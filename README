
    Building Open HA Cluster Agents

The majority of Open High Availability Cluster (OHAC) code is released under the Common Development and Distribution License (CDDL) Version 1.0.

Source based on existing open source projects will continue to be available under their current licenses. Some binary components are covered under the OpenSolaris Binary License and some are covered under other open source licenses. Specific download pages provide license information associated with the available component pieces.
Building From Source
To build from source, you need a system installed with:

Solaris Express (either Community Edition or Developer Edition).
A C/C++ compiler collection. You can use the Sun Studio Compilers (included in Solaris Express Developer Edition), or the GNU Compiler Collection (included in Solaris Express).
You also need to download and install the following build tools:
The ON Specific Build Tools (SUNWonbld.PLATFORM.tar.bz2)
The SUNWonbld package is a pkgadd-format directory that has been archived using tar and bzip2. Unpack it in a temporary directory and use pkgadd(1M) to install it. For example:
# cd /var/tmp
# bzcat /path/SUNWonbld.PLATFORM.tar.bz2 | tar xf -
# pkgadd -d onbld SUNWonbld

The OHAC Specific Build Tools (ohac-tools-DATE.PLATFORM.tar.bz2)
The ohac-tools archive contains the build tools that are necessary for building OHAC.
To install the OHAC build tools:
# cd /opt
# bzcat /path/ohac-tools-DATE.PLATFORM.tar.bz2 | tar xf -

    Building Agents
To build the OHAC Agents, you also need to download and install the following archives:
The OHAC Core Reference Proto (ohac-ref-proto-DATE.PLATFORM.tar.bz2)
The ohac-ref-proto archive contains the Core development headers and libraries for OHAC.
The OHAC Agents Closed Binaries (ohacds-closed-bins-DATE.PLATFORM.tar.bz2)
The ohacds-closed-bins archive contains the Agent binaries built from closed source.
The OHAC Agents External Packages (ohacds-ext-pkgs-DATE.PLATFORM.tar.bz2)
The ohacds-ext-pkgs archive contains prebuilt packages and product deliverables from other groups, such as Documentation and Globalization.

    To install the OHAC archives:
# cd /opt
# bzcat /path/ohac-ref-proto-DATE.PLATFORM.tar.bz2 | tar xf -
# bzcat /path/ohacds-closed-bins-DATE.PLATFORM.tar.bz2 | tar xf -
# bzcat /path/ohacds-ext-pkgs-DATE.PLATFORM.tar.bz2 | tar xf -

And finally, you need to download and install the source archive:
The OHAC Agents Source Code (ohacds-src-DATE.tar.bz2)
You can install the source code wherever you like. This example assumes the archive is extracted to /builds:
$ cd /builds
$ bzcat /path/ohacds-src-DATE.tar.bz2 | tar xf -
To build the agent's source, run nbuild from within the source tree. This example creates a complete build of both non-debug and debug using the Sun Studio Compiler, and assumes all the compilers, tools and archives were installed in their default locations:
$ cd /builds/ohacds
$ /opt/scbld/bin/nbuild -Da
Alternatively, to build the agents source using the GNU Compiler Collection (gcc), run nbuild with two additional options:
$ cd /builds/ohacds
$ /opt/scbld/bin/nbuild -Da CW_NO_SHADOW=# __GNUC=
The resulting build is located in /builds/ohacds+5.11+PLATFORM where you can find:
log/log.TIMEDATE.NUM - log files for this build
packages/PLATFORM/Sol_11 - debug packages
packages/PLATFORM/Sol_11-nd - non-debug packages 

    Installing Agent Packages
Use pkgadd(1M) to install the agents. This example shows how to install the non-debug x86 NFS agent:
# pkgadd -d /builds/ohacds+5.11+i386/packages/i386/Sol_11-nd SUNWscnfs
