#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at <packagename>/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)Makefile.com 1.11	09/05/14 SMI"
#

include $(SRC)/Makefile.master

# generic definitions
MANIFESTS :sh= (cd manifests; print *.mf)
PKGS= $(MANIFESTS:%.mf=%)

FILES = $(INSTALL_SCRIPTS_DIR)

CLEANFILES= $(FILES)

# group- and loose-package "signature" files
# these definitions must be included *before* ?P_SIGNATURE_FILE is
# defined in package makefile
GP_SIGNATURE_DIR = $(VROOT)/usr/cluster/lib/scadmin/ips/sig-gp
LP_SIGNATURE_DIR = $(VROOT)/usr/cluster/lib/scadmin/ips/sig-pkg

INSTALL_SCRIPTS_DIR = $(VROOT)/usr/cluster/lib/scadmin/ips/install-scripts
INSTALL_SCRIPT_FILES = $(SRC)/pkg/scripts
LIC_FILES = $(SRC)/pkg/license_files
