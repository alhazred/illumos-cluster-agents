#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)Makefile.cmd	1.37	07/08/15 SMI"
#
# usr/src/cmd/Makefile.cmd
#
# Definitions common to command source.
#
include $(SRC)/Makefile.master

# Default LINTFILES rule
LINTFILES=	$(PROG:%=%.ln) $(DAEMON:%=%.ln) $(OBJECTS:%.o=%.ln)

LDLIBS =	$(LDLIBS.cmd)
$(POST_S9_BUILD)LDLIBS += -lscf
LDFLAGS.cmd =	$(STRIPFLAG) $(ENVLDFLAGS1) $(ENVLDFLAGS2) $(ENVLDFLAGS3)
LDFLAGS =	$(LDFLAGS.cmd)

# Default text domain
TEXT_DOMAIN=	SUNW_SC_CMD
PIFILES=	$(PROG:%=%.pi)
POFILE=		$(PROG:%=%.po)

CLOBBERFILES += $(LINTFILES)

ROOTOPTBIN=		$(VROOT)/opt/$(PKGNAME)/bin
ROOTOPTETC=		$(VROOT)/opt/$(PKGNAME)/etc
ROOTOPTLIB=		$(VROOT)/opt/$(PKGNAME)/lib
ROOTOPTUTIL=		$(VROOT)/opt/$(PKGNAME)/util
ROOTOPTBINPROG=		$(PROG:%=$(ROOTOPTBIN)/%)
ROOTOPTETCPROG=		$(PROG:%=$(ROOTOPTETC)/%)
ROOTOPTETCCONF=		$(CONF:%=$(ROOTOPTETC)/%)
ROOTOPTETCRTR=		$(RTRFILE:%=$(ROOTOPTETC)/%)
ROOTOPTLIBPROG=		$(PROG:%=$(ROOTOPTLIB)/%)
ROOTOPTLIBKSHPROG=	$(LIBPROG:%=$(ROOTOPTLIB)/%)
ROOTOPTUTILPROG=	$(UTILPROG:%=$(ROOTOPTUTIL)/%)
