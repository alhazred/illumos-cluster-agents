/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * hadb_ma.c - Common utilities for hadb
 */

#pragma ident	"@(#)hadb_ma.c	1.9	07/06/06 SMI"

/*
 * This utility has the methods for performing the validation, starting and
 * stopping the data service and the fault monitor. It also contains the
 * method to probe the health of the data service. The probe just returns
 * either success or failure. Action is taken based on this returned value in
 * the method found in the file hadb_ma_probe.c
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <libgen.h>
#include <libintl.h>
#include <sys/stat.h>
#include <sys/errno.h>
#include <regex.h>
#include <sys/utsname.h>
#include <scha_tags.h>
#include <sys/wait.h>
#include <scha.h>
#include <rgm/libdsdev.h>
#include "hadb_ma.h"
#include "../../common/ds_common.h"
#include "hadb_ma_util.h"


#define	SVC_SMOOTH_PCT		80

/*
 * We need to wait for SVC_WAIT_TIME ( 5 secs) for pmf to send the failure
 * message before probing the service
 */

#define	SVC_WAIT_TIME		5

/*
 * The initial timeout allowed  for the sap enqueue dataservice to
 * be fully up and running. We will wait for for 2% (SVC_WAIT_PCT)
 * of the start_timeout time before probing the service.
 */
#define	SVC_WAIT_PCT		2

/*
 * svc_validate():
 *
 * Do hadb ma specific validation of the resource configuration. Called by
 * start/validate/update/monitor methods. Return 0 on success, > 0 on
 * failures.
 *
 * Note that we don't do any HAStoragePlus validation since the typical config
 * will have the binaries on local filesystems, and the data and
 * configuration is replicated to local filesystems by HADB itself.  We do
 * check for binaries and configuration files on all nodes so if they are
 * unavailable on a node for any reason the validate will fail.
 *
 * Perform the following checks:
 *
 *     1)desired number of primaries is equal to maximum number primaries.
 *     2)ma user exists
 *     3)hadb root exists and it's an absolute path
 *     4)management agent binary exists with correct permission
 *     5)hadbm binary exists with correct permission
 *     6)mgt.cfg file exists with correct permission
 */

int
svc_validate(scds_handle_t scds_handle, hadb_ma_extprops_t *hadbmaxprops,
		boolean_t print_messages)
{
	int		err;
	int		rc = 0;
	int		desired_primaries;
	int		maximum_primaries;
	mode_t 		mode;
	scha_rgmode_t	rg_mode;

	scds_syslog_debug(DBG_LEVEL_HIGH, "In svc_validate()....");

	/*
	* Check if the Resource Group is in scalable mode
	*/
	rg_mode = scds_get_rg_rg_mode(scds_handle);
	if (rg_mode != RGMODE_SCALABLE) {
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		scds_syslog(LOG_ERR, "HADB MA resource must be created "
				"in a Multi Master Resource Group");
		if (print_messages) {
			(void) fprintf(stderr, gettext("HADB MA resource "
					"must be created "
					"in a Multi Master Resource Group\n"));
		}
		rc = 1;
		goto finished;
	}

	/*
	 * Get desired number of primaries and the maximum number of
	 * primaries.
	 */

	desired_primaries = scds_get_rg_desired_primaries(scds_handle);
	maximum_primaries = scds_get_rg_maximum_primaries(scds_handle);

	/*
	 * Check that desired and maximum primaries are equal. This check
	 * will validate the muli-master configuration
	 */

	if (desired_primaries != maximum_primaries) {
		scds_syslog(LOG_ERR, "Desired_primaries must equal "
			    "Maximum_primaries.");
		if (print_messages) {
			(void) fprintf(stderr, gettext("Desired_primaries "
						"must equal "
						"Maximum_primaries.\n"));
		}
		rc = 1;
		goto finished;
	}
	/* Check if ma user id exists */
	err = validate_ma_user(hadbmaxprops, print_messages);
	if (err != 0) {
		rc = 1;
		goto finished;
	}
	/* Check that hadb root is set properly */
	if (strcmp(hadbmaxprops->hadb_ma_root, "") == 0) {
		scds_syslog(LOG_ERR, "Property %s is not set.", "HADB_ROOT");
		if (print_messages) {
			(void) fprintf(stderr, gettext("Property %s is not "
							"set.\n"), "HADB_ROOT");
		}
		rc = 1;
		goto finished;
	}
	/* Make sure hadb root is an absolute path. */
	if (hadbmaxprops->hadb_ma_root[0] != '/') {
		scds_syslog(LOG_ERR,
			    "%s is not an absolute path.", "HADB_ROOT");
		if (print_messages) {
			(void) fprintf(stderr, gettext("%s is not "
							"an absolute path.\n"),
							"HADB_ROOT");
		}
		rc = 1;
		goto finished;
	}

	/* Get mode for hadb root for ma user */
	if (ds_validate_file_owner
		(B_FALSE, hadbmaxprops->ma_userid, -1, "%s",
		hadbmaxprops->hadb_ma_root) == SCHA_ERR_NOERR) {

		mode = S_IRUSR | S_IXUSR;
	}
	else
	if (ds_validate_file_owner
		(B_FALSE, -1, hadbmaxprops->ma_grpid, "%s",
		hadbmaxprops->hadb_ma_root) == SCHA_ERR_NOERR) {

		mode = S_IRGRP | S_IXGRP;
	}
	else
	{
		mode = S_IROTH | S_IXOTH;
	}

	/* Make sure hadb root directory exists */
	err = ds_validate_file(print_messages, mode | S_IFDIR,
				"%s", hadbmaxprops->hadb_ma_root);
	if (err != SCHA_ERR_NOERR) {
		rc = 1;
		goto finished;
	}

	/* Get mode for hadb ma exe for ma user */
	if (ds_validate_file_owner
		(B_FALSE, hadbmaxprops->ma_userid, -1, "%s/bin/ma",
		hadbmaxprops->hadb_ma_root) == SCHA_ERR_NOERR) {

		mode = S_IXUSR;
	}
	else
	if (ds_validate_file_owner
		(B_FALSE, -1, hadbmaxprops->ma_grpid, "%s/bin/ma",
		hadbmaxprops->hadb_ma_root) == SCHA_ERR_NOERR) {

		mode = S_IXGRP;
	}
	else
	{
		mode = S_IXOTH;
	}

	/* Check ma executable exists with correct permissions. */
	err = ds_validate_file(print_messages, mode | S_IFREG,
				"%s/bin/ma", hadbmaxprops->hadb_ma_root);
	if (err != SCHA_ERR_NOERR) {
		rc = 1;
		goto finished;
	}

	/* Get mode for hadb hadbm exe for ma user */
	if (ds_validate_file_owner
		(B_FALSE, hadbmaxprops->ma_userid, -1, "%s/bin/hadbm",
		hadbmaxprops->hadb_ma_root) == SCHA_ERR_NOERR) {

		mode = S_IXUSR;
	}
	else
	if (ds_validate_file_owner
		(B_FALSE, -1, hadbmaxprops->ma_grpid, "%s/bin/hadbm",
		hadbmaxprops->hadb_ma_root) == SCHA_ERR_NOERR) {

		mode = S_IXGRP;
	}
	else
	{
		mode = S_IXOTH;
	}

	/* Check hadbm executable exists with correct permissions. */
	err = ds_validate_file(print_messages, mode | S_IFREG,
				"%s/bin/hadbm", hadbmaxprops->hadb_ma_root);
	if (err != SCHA_ERR_NOERR) {
		rc = 1;
		goto finished;
	}

	/* Get mode for hadb ma-initd script for ma user */
	if (ds_validate_file_owner
		(B_FALSE, hadbmaxprops->ma_userid, -1, "%s",
		hadbmaxprops->hadb_ma_start) == SCHA_ERR_NOERR) {

		mode = S_IXUSR;
	}
	else
	if (ds_validate_file_owner
		(B_FALSE, -1, hadbmaxprops->ma_grpid, "%s",
		hadbmaxprops->hadb_ma_start) == SCHA_ERR_NOERR) {

		mode = S_IXGRP;
	}
	else
	{
		mode = S_IXOTH;
	}

	/* Check ma-initd script exist with correct permissions. */
	err = ds_validate_file(print_messages, mode | S_IFREG,
				"%s", hadbmaxprops->hadb_ma_start);
	if (err != SCHA_ERR_NOERR) {
		rc = 1;
		goto finished;
	}

	/* Get mode for hadb mgt.cfg config for ma user */
	if (ds_validate_file_owner
		(B_FALSE, hadbmaxprops->ma_userid, -1, "%s",
		hadbmaxprops->hadb_ma_cfg) == SCHA_ERR_NOERR) {
		mode = S_IRUSR;
	}
	else
	if (ds_validate_file_owner
		(B_FALSE, -1, hadbmaxprops->ma_grpid, "%s",
		hadbmaxprops->hadb_ma_cfg) == SCHA_ERR_NOERR) {

		mode = S_IRGRP;
	}
	else
	{
		mode = S_IROTH;
	}

	/* Check mgt.cfg config exist with correct permissions. */
	err = ds_validate_file(print_messages, mode | S_IFREG,
				"%s", hadbmaxprops->hadb_ma_cfg);
	if (err != SCHA_ERR_NOERR) {
		rc = 1;
		goto finished;
	}

	/* Check if hadbm admin password file is set */
	if (strcmp(hadbmaxprops->hadbm_passwordfile, "") != 0) {

		/* Get mode for hadbm admin password file for hadbm */
		if (ds_validate_file_owner
			(B_FALSE, hadbmaxprops->ma_userid, -1, "%s",
			hadbmaxprops->hadbm_passwordfile) == SCHA_ERR_NOERR) {
			mode = S_IRUSR;
		}
		else
		if (ds_validate_file_owner
			(B_FALSE, -1, hadbmaxprops->ma_grpid, "%s",
			hadbmaxprops->hadbm_passwordfile) == SCHA_ERR_NOERR) {

			mode = S_IRGRP;
		}
		else
		{
			mode = S_IROTH;
		}

		/*
		 * Check hadbm admin password file exist with
		 * correct permissions.
		 */
		err = ds_validate_file(print_messages, mode | S_IFREG,
				"%s", hadbmaxprops->hadbm_passwordfile);
		if (err != SCHA_ERR_NOERR) {
			rc = 1;
		goto finished;
		}
	}
	else
	{
	/*
	 * Warn the user, if HADB mgmt domain administrative password
	 * is not set.
	 */
		/*
		 * SCMSGS
		 * @explanation
		 * While validating the extension properties, The
		 * HADBM_PASSWORDFILE extension property is found not to be
		 * set.
		 * @user_action
		 * This is an informational message, The user has to provide
		 * the complete path to the hadbm admin password file if the
		 * domain is created with a password.
		 */
		scds_syslog(LOG_NOTICE,
			"The %s extension property is not set.",
			MA_EXT_HADBM_PASSWD);
	}

	/* Retrive the port number from mgt.cfg */
	err = get_ma_server_port(hadbmaxprops->hadb_ma_cfg,
				&(hadbmaxprops->hadb_ma_port), B_TRUE);
	if (err != 0) {
		/* set default value */
		hadbmaxprops->hadb_ma_port = HADB_MA_DEF_PORT;
		/*
		 * SCMSGS
		 * @explanation
		 * While parsing the mgt.cfg configuration file ,the ma server
		 * port field was not found or an error occured in parsing the
		 * port field within the file.
		 * @user_action
		 * This is an informational message, no user action is needed.
		 * The ma server will start up with default port.
		 */
		scds_syslog(LOG_INFO,
			"default port:<%d> will be used to probe ma",
			hadbmaxprops->hadb_ma_port);

	}
	else
	{
		scds_syslog_debug(DBG_LEVEL_HIGH, "Got Port no %d from %s",
				hadbmaxprops->hadb_ma_port,
				hadbmaxprops->hadb_ma_cfg);
	}

	/* All validation checks were successful */
finished:

	scds_syslog_debug(DBG_LEVEL_HIGH, "validation status is <%d>", rc);

	return (rc);		/* return result of validation */
}

/*
 * svc_start():
 *
 * Start the hadb management agent using ma-initd script for this local system
 *
 * The management agent will be started under pmf control, the child monitor
 * level is got from RTR file.
 *
 * Return    0     if ma is started up successfully
 *           1     if ma is running already or system error
 *      call scha_control with SCHA_GIVEOVER tag to bailout and
 * return 1 if ma is already running outside of SC
 *
 */

int
svc_start(scds_handle_t scds_handle, hadb_ma_extprops_t *hadbmaxprops)
{
	int		rc = 0;
	char		*cmd;
	int		child_mon_level = -1;
	scha_extprop_value_t *child_mon_level_prop = NULL;
	int		ma_stat;

	/*
	 * If ma is not running locally go ahead and start ma
	 *
	 * If ma is already running on this node, call scha_control with tag
	 * SCHA_GIVEOVER, so that the start method will abort there will be
	 * no failovers as the configuration will be multi-mastered. The
	 * start method will return non-zero.
	 */

	ma_stat = is_process_running(MA_PROCESS, B_TRUE);

	if (ma_stat == 1) {

		/*
		 * If ma is already running, log notice, call scha_control
		 * with tag SCHA_GIVEOVER. return 1,so the start method will
		 * abort.
		 */
		/*
		 * SCMSGS
		 * @explanation
		 * The specified application is already running on the node
		 * outside of Sun Cluster software. The attempt to start it up
		 * under Sun Cluster software will be aborted.
		 * @user_action
		 * No user action is needed.
		 */
		scds_syslog(LOG_ERR,
			    "%s is already running on this node outside of Sun "
			    "Cluster. The start of %s from Sun Cluster will "
			    "be aborted.", APP_NAME, APP_NAME);

		rc = scha_control(SCHA_IGNORE_FAILED_START,
				scds_get_resource_group_name(scds_handle),
				scds_get_resource_name(scds_handle));
		if (rc == SCHA_ERR_TAG) {
			scds_syslog_debug(DBG_LEVEL_HIGH, "Invalid scha_control"
				" tag SCHA_IGNORE_FAILED_START. api-version "
				"running is older than 5.");
		}
		/* fail, bail out */
		return (2);
	}
	/*
	 * Build hadb ma start command. <ma-initd start>
	 */
	cmd = ds_string_format("%s start >/dev/null 2>&1",
				hadbmaxprops->hadb_ma_start);
	if (cmd == NULL) {
		rc = 1;
		ds_internal_error("unable to create the start command");
		goto finished;
	}
	scds_syslog_debug(DBG_LEVEL_HIGH, "The command used to start "
			"MA is %s", cmd);

	/* get child_mon_level */
	if (scds_get_ext_property(scds_handle, "Child_mon_level",
				SCHA_PTYPE_INT,
				&child_mon_level_prop) == SCHA_ERR_NOERR) {

		child_mon_level = child_mon_level_prop->val.val_int;
		/*
		 * SCMSGS
		 * @explanation
		 * Resource property Child_mon_level is set to the given
		 * value.
		 * @user_action
		 * No user action is needed.
		 */
		scds_syslog(LOG_INFO,
			    "Extension property <Child_mon_level> has a "
			    "value of <%d>", child_mon_level);
	}
	else
		/*
		 * SCMSGS
		 * @explanation
		 * Property Child_mon_level may not be defined in RTR file.
		 * Use the default value of -1.
		 * @user_action
		 * No user action is needed.
		 */
		scds_syslog(LOG_INFO,
			"Either extension property <Child_mon_level> is not "
			"defined, or an error occurred while retrieving this "
			"property. The default value of -1 is being used.");

	scds_syslog_debug(DBG_LEVEL_HIGH, "child mon level = <%d>",
				child_mon_level);

	/* Suppress lint error about lack of _errno() prototype */
	if ((rc = setegid(hadbmaxprops->ma_grpid)) != 0) {
		ds_internal_error("setegid: %d %s", hadbmaxprops->ma_grpid,
				strerror(errno));	/*lint !e746 */
		goto finished;
	}

	scds_syslog_debug(DBG_LEVEL_HIGH, "after setegid");

	if ((rc = seteuid(hadbmaxprops->ma_userid)) != 0) {
		ds_internal_error("seteuid: %d %s", hadbmaxprops->ma_userid,
				strerror(errno));	/*lint !e746 */
		goto finished;
	}

	scds_syslog_debug(DBG_LEVEL_HIGH, " after seteuid");

	/* Start hadb ma under pmf control */
	rc = scds_pmf_start(scds_handle, SCDS_PMF_TYPE_SVC, 0, cmd,
			    child_mon_level);

	if (rc == SCHA_ERR_NOERR) {
		scds_syslog(LOG_INFO,
			    "Start of %s completed successfully.", cmd);
	}
	else
	{
		scds_syslog(LOG_ERR, "Failed to start %s.", cmd);
	}

	if (seteuid(0) != 0) {
		ds_internal_error("seteuid(0): %d %s", 0,
				strerror(errno));	/*lint !e746 */
		rc = 1;
	}

	if (setegid(0) != 0) {
		ds_internal_error("setegid(0): %d %s", 0,
				strerror(errno));	/*lint !e746 */
		rc = 1;
	}

	scds_syslog_debug(DBG_LEVEL_HIGH, "after reset uid=%d euid=%d",
		getuid(), geteuid());

finished:

	free(cmd);

	return (rc);		/* return Success/failure status */

}

/*
 * svc_stop():
 *
 * Stop the hadb ma process Return 0 on success, > 0 on failures.
 *
 */
int
svc_stop(scds_handle_t scds_handle, hadb_ma_extprops_t *hadbmaxprops)
{
	int		rc = 0, cmd_exit_code = 0;
	int		stop_smooth_timeout;
	char		*cmd = NULL;

	scds_syslog(LOG_NOTICE, "Stopping %s.", APP_NAME);

	stop_smooth_timeout = (scds_get_rs_stop_timeout(scds_handle)
				* SVC_SMOOTH_PCT) / 100;

	/*
	 * First take the command out of PMF monitoring, so that it doesn't
	 * keep restarting it.
	 */
	rc = scds_pmf_stop_monitoring(scds_handle, SCDS_PMF_TYPE_SVC, 0);

	if (rc != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * An error was encounterd while taking the resource out of
		 * PMF's control while stopping the resource. The resource
		 * will be stopped by sending it SIGKILL.
		 * @user_action
		 * This message is informational; no user action is needed.
		 */
		scds_syslog(LOG_ERR,
			    "Failed to take the resource out of PMF "
			    "control. Sending SIGKILL now.");

		goto send_kill;
	}
	/*
	 * form the command to stop MA gracefully: <ma-initd stop>
	 */
	cmd = ds_string_format("%s stop >/dev/null 2>&1",
				hadbmaxprops->hadb_ma_stop);

	if (cmd == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * The STOP method was not able to construct the applications
		 * stop command. The STOP method will send SIGKILL to stop the
		 * application.
		 * @user_action
		 * Other messages will indicate what the underlying problem
		 * was such as no memory or a bad configuration.
		 */
		scds_syslog(LOG_ERR, "Unable to compose %s path. "
			    "Sending SIGKILL now.", "ma-initd stop");
		goto send_kill;
	}
	scds_syslog_debug(DBG_LEVEL_HIGH,
				"%s command will be used to stop ma", cmd);

	if ((rc = setegid(hadbmaxprops->ma_grpid)) != 0) {
		ds_internal_error("for %s setegid: %d %s",
				hadbmaxprops->ma_grpid,
				strerror(errno));	/*lint !e746 */
		goto finished;
	}

	scds_syslog_debug(DBG_LEVEL_HIGH, "after setegid");

	if ((rc = seteuid(hadbmaxprops->ma_userid)) != 0) {
		ds_internal_error("seteuid: %d %s",
				hadbmaxprops->ma_userid,
				strerror(errno));	/*lint !e746 */
		goto finished;
	}

	scds_syslog_debug(DBG_LEVEL_HIGH, " after seteuid");

	rc = scds_timerun(scds_handle, cmd, stop_smooth_timeout,
				SIGKILL, &cmd_exit_code);

	if (rc != 0 || cmd_exit_code != 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * The stop command was unable to stop the application. The
		 * STOP method will now stop the application by sending it
		 * SIGKILL.
		 * @user_action
		 * Look at application and system logs for the cause of the
		 * failure.
		 */
		scds_syslog(LOG_ERR,
			    "The stop command <%s> failed to stop the "
			    "application. Will now use SIGKILL to stop the "
			    "application.", cmd);
	}
	if (seteuid(0) != 0) {
		ds_internal_error("seteuid(0): %d %s", 0,
				strerror(errno));	/*lint !e746 */
		rc = 1;
	}

	if (setegid(0) != 0) {
		ds_internal_error("setegid(0): %d %s", 0,
				strerror(errno));	/*lint !e746 */
		rc = 1;
	}

	scds_syslog_debug(DBG_LEVEL_HIGH, "after reset uid=%d euid=%d",
		getuid(), geteuid());


	free(cmd);
	/*
	 * Regardless of whether the command succeeded or not we send KILL
	 * signal to the pmf tag. This will ensure that the process tree goes
	 * away if it still exists. If it doesn't exist by then, we return
	 * NOERR.
	 */

send_kill:
	/*
	 * Since all else failed, send SIGKILL to stop the application.
	 * Notice that this call will return with success, even if the tag
	 * does not exist by now.
	 */
	if ((rc = scds_pmf_stop(scds_handle, SCDS_PMF_TYPE_SVC, 0,
				SIGKILL, -1)) != SCHA_ERR_NOERR) {

		/*
		 * Failed to stop the application even with SIGKILL, bail out
		 * now.
		 */
		scds_syslog(LOG_ERR,
			    "Failed to stop the application with SIGKILL. "
			    "Returning with failure from stop method.");

		goto finished;
	}
finished:

	scds_syslog_debug(DBG_LEVEL_HIGH, "after scds_pmf_stop, rc = <%d>", rc);

	if (rc == SCHA_ERR_NOERR) {
		scds_syslog(LOG_INFO, "Successfully stopped the application");

	}
	return (rc);		/* Successfully stopped */
}


/*
 * svc_wait():
 *
 * wait for the data service to start up fully and make sure it is running
 * healthy
 */

int
svc_wait(scds_handle_t scds_handle, hadb_ma_extprops_t *hadbmaxprops)
{
	int		svc_start_timeout, probe_timeout;
	int		rc = 0;

	scds_syslog_debug(DBG_LEVEL_HIGH, "in svc_wait()... ");

	/* Get the Start timeout */
	svc_start_timeout = scds_get_rs_start_timeout(scds_handle);

	/*
	 * sleep for SVC_WAIT_PCT percentage of start_timeout time
	 * before actually probing the dataservice. This is to allow
	 * the dataservice to be fully up in order to reply to the
	 * probe. NOTE: the value for SVC_WAIT_PCT could be different
	 * for different dataservices.
	 *
	 */
	scds_syslog_debug(DBG_LEVEL_HIGH, "initial wait ... before probe");

	if (scds_svc_wait(scds_handle, (svc_start_timeout * SVC_WAIT_PCT) / 100)
	    != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		scds_syslog(LOG_ERR, "Failed to start service.");
		return (1);
	}

	scds_syslog_debug(DBG_LEVEL_HIGH, "after inital wait, probe now");

	/* Get Probe timeout value */
	probe_timeout = scds_get_ext_probe_timeout(scds_handle);

	do {
		rc = svc_probe(scds_handle, hadbmaxprops, probe_timeout);

		if (rc == SCHA_ERR_NOERR || rc == 1) {
			scds_syslog_debug(DBG_LEVEL_HIGH, "Service is online.");
			return (0);
		}
		scds_syslog_debug(DBG_LEVEL_HIGH,
				"Waiting for %s to come up.", APP_NAME);

		/* wait for 5 seconds if probe isn't there yet! */
		rc = scds_svc_wait(scds_handle, SVC_WAIT_TIME);
		if (rc != SCHA_ERR_NOERR) {
			scds_syslog(LOG_ERR, "Failed to start service.");
			return (1);
		}
		/* we rely on rgm to timeout */
	} while (1);
}

/*
 * svc_probe(): Do data service specific probing. Return a float value
 * between 0 (success) and 100(complete failure).
 *
 * The probe checks the health check using hadbm utility, it sends a dummy
 * request to the management on the local host, if the ma is not responding
 * flag it as failure
 *
 * All other system error, treat them as 1/2 of complete failure.
 */

int
svc_probe(scds_handle_t scds_handle, hadb_ma_extprops_t *hadbmaxprops,
		int probe_timeout)
{
	char		*probe_cmd;
	int		cmd_exit_status;
	scha_err_t		rc = SCHA_ERR_NOERR;
	int		probe_status = 0;

	scds_syslog_debug(DBG_LEVEL_HIGH, "in svc_probe()... ");

	/*
	 * hadbm cli is used to probe the aliveness of MA, The logic
	 * is to execute hadbm listdomain to verify if MA is ready to
	 * handle client requests or not. The HADBM management domain
	 * can be created with or w/o an administrator password.
	 * Based on the authentication need, the user has to set the
	 * extension property HADBM_PASSWORDFILE pointing to the abs
	 * path of the password file, If the property is set then it is
	 * used by the hadbm cli in the ma_status script for authentication.
	 */
	/* if hadbm admin password file is not set */
	if (strcmp(hadbmaxprops->hadbm_passwordfile, "") == 0) {
		/*
		 * ma_status -hadb root -port no
		 */
		probe_cmd = ds_string_format("%s/%s %s %d",
					scds_get_rt_rt_basedir(scds_handle),
					MA_STATUS_SCRIPT,
					hadbmaxprops->hadb_ma_root,
					hadbmaxprops->hadb_ma_port);
	}
	else
	{
		/*
		 * ma_status -hadb root -port no hadbm passwordfile
		 */
		probe_cmd = ds_string_format("%s/%s %s %d %s",
					scds_get_rt_rt_basedir(scds_handle),
					MA_STATUS_SCRIPT,
					hadbmaxprops->hadb_ma_root,
					hadbmaxprops->hadb_ma_port,
					hadbmaxprops->hadbm_passwordfile);
	}

	if (probe_cmd == NULL) {
		ds_internal_error("Unable to allocate string for "
					"probe command.");
		return (SCDS_PROBE_COMPLETE_FAILURE / 2);
	}
	scds_syslog_debug(DBG_LEVEL_HIGH,
				"svc_probe() calling %s with timeout %d",
				probe_cmd, probe_timeout);

	/* execute the health check cmd under scds_timerun */
	rc = scds_timerun(scds_handle, probe_cmd,
				probe_timeout, SIGKILL, &cmd_exit_status);
	switch (rc) {
	case SCHA_ERR_TIMEOUT:
		/*
		 * SCMSGS
		 * @explanation
		 * Probe of the data service timed out.
		 * @user_action
		 * No user action needed.
		 */
		scds_syslog(LOG_ERR,
			    "The probe command <%s> timed out", probe_cmd);
		probe_status = SCDS_PROBE_COMPLETE_FAILURE / 2;
		goto finished;
	case SCHA_ERR_INTERNAL:
		/*
		 * SCMSGS
		 * @explanation
		 * An internal error has occurred.
		 * @user_action
		 * Save a copy of the /var/adm/messages files on all nodes.
		 * Contact your authorized Sun service provider for assistance
		 * in diagnosing the problem.
		 */
		scds_syslog(LOG_ERR,
			    "INTERNAL ERROR: error occurred while "
			    "launching the probe command <%s>",
			    probe_cmd);
		probe_status = SCDS_PROBE_COMPLETE_FAILURE / 2;
		goto finished;
	case SCHA_ERR_INVAL:
		/*
		 * SCMSGS
		 * @explanation
		 * Failure in executing the command.
		 * @user_action
		 * Check the syslog message for the command description. Check
		 * whether the system is low in memory or the process table is
		 * full and take appropriate action. Make sure that the
		 * executable exists.
		 */
		scds_syslog(LOG_ERR,
			    "Cannot execute %s: %s.", probe_cmd,
			    cmd_exit_status);
		probe_status = SCDS_PROBE_COMPLETE_FAILURE / 2;
		/* fall through */
	case SCHA_ERR_NOERR:
		scds_syslog_debug(DBG_LEVEL_HIGH,
				"ma status returned %d", cmd_exit_status);
		break;
	default:
		/*
		 * SCMSGS
		 * @explanation
		 * The data service detected an internal error.
		 * @user_action
		 * Informational message. No user action is needed.
		 */
		scds_syslog(LOG_ERR,
			    "Unhandled error from probe command %s: %d",
			    probe_cmd, rc);
		goto finished;
	}

	if (cmd_exit_status == 0) {
		probe_status = 0;
	} else if (cmd_exit_status == 2) {
		/*
		 * We set the probe to 1 to allow the RGM to restart
		 * the resource after a large amount of time. This will
		 * allow sufficient time for MA's to recover from
		 * repository sync operations. The resource is set to
		 * degraded state and the syslog messages will indicate
		 * reason for the degraded state.
		 */
		probe_status = 1;
	}
	else
	{
		scds_syslog_debug(DBG_LEVEL_HIGH,
			    "Probe for ma returned %d", cmd_exit_status);
		probe_status = SCDS_PROBE_COMPLETE_FAILURE;
	}

finished:
	free(probe_cmd);
	return (probe_status);
}

/*
 * This function starts the fault monitor for a hadb ma resource. This is
 * done by starting the probe under PMF. The PMF tag is derived as
 * <RG-name,RS-name,instance_number.mon>. The restart option of PMF is used
 * but not the "infinite restart". Instead interval/retry_time is obtained
 * from the RTR file.
 */

int
mon_start(scds_handle_t scds_handle)
{
	scha_err_t	err;

	scds_syslog_debug(DBG_LEVEL_HIGH,
			"Calling MONITOR_START method for resource <%s>.",
			scds_get_resource_name(scds_handle));

	/*
	 * The probe hadb_probe is assumed to be available in the same
	 * subdirectory where the other callback methods for the RT are
	 * installed. The last parameter to scds_pmf_start denotes the child
	 * monitor level. Since we are starting the probe under PMF we need
	 * to monitor the probe process only and hence we are using a value
	 * of 0.
	 */

	err = scds_pmf_start(scds_handle, SCDS_PMF_TYPE_MON,
				SCDS_PMF_SINGLE_INSTANCE, "hadb_ma_probe", 0);

	if (err != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to start fault monitor.");
		return (err);
	}
	scds_syslog(LOG_INFO, "Started the fault monitor.");

	return (SCHA_ERR_NOERR);	/* Successfully started Monitor */
}


/*
 * This function stops the fault monitor for a hadb ma resource. This is done
 * via PMF. The PMF tag for the fault monitor is constructed based on
 * <RG-name_RS-name,instance_number.mon>.
 */

int
mon_stop(scds_handle_t scds_handle)
{
	scha_err_t	err;

	scds_syslog_debug(DBG_LEVEL_HIGH, "Calling scds_pmf_stop method");

	err = scds_pmf_stop(scds_handle, SCDS_PMF_TYPE_MON,
			    SCDS_PMF_SINGLE_INSTANCE, SIGKILL,
			    FINAL_STOP_TIMEOUT);

	if (err != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to stop fault monitor.");
		return (err);
	}
	scds_syslog(LOG_INFO, "Stopped the fault monitor.");

	return (SCHA_ERR_NOERR);	/* Successfully stopped monitor */
}

/*
 * hadb_ma_get_extensions()
 *
 * Get the extension properties set for hadb ma resource and save them in the
 * hadbmaxprops structure
 *
 * Return 0 on success , 1 otherwise
 */

int
hadb_ma_get_extensions(scds_handle_t scds_handle,
			hadb_ma_extprops_t *hadbmaxprops,
			boolean_t print_messages)
{
	int		err;

	/* Start filling in the hadbmaxprops struct */

	/* Get hadb install directory */
	err = get_string_ext_property(scds_handle, MA_EXT_HADBROOT,
				&hadbmaxprops->hadb_ma_root, print_messages);
	if (err != SCHA_ERR_NOERR) {
		/* Message already logged and printed. */
		return (1);
	}
	/* Get hadb ma ma-initd script: start */
	err = get_string_ext_property(scds_handle, MA_EXT_START,
				&hadbmaxprops->hadb_ma_start, print_messages);
	if (err != SCHA_ERR_NOERR) {
		/* Message already logged and printed. */
		return (1);
	}
	/* Get hadb ma ma-initd script: stop */
	err = get_string_ext_property(scds_handle, MA_EXT_STOP,
				&hadbmaxprops->hadb_ma_stop, print_messages);
	if (err != SCHA_ERR_NOERR) {
		/* Message already logged and printed. */
		return (1);
	}
	/* Get hadb ma user */
	err = get_string_ext_property(scds_handle, MA_EXT_USER,
				&hadbmaxprops->hadb_ma_user, print_messages);
	if (err != SCHA_ERR_NOERR) {
		/* Message already logged and printed. */
		return (1);
	}
	/* Get hadb ma CFG file */
	err = get_string_ext_property(scds_handle, MA_EXT_CFG,
				&hadbmaxprops->hadb_ma_cfg, print_messages);
	if (err != SCHA_ERR_NOERR) {
		/* Message already logged and printed. */
		return (1);
	}
	/* Get hadbm admin password file */
	err = get_string_ext_property(scds_handle, MA_EXT_HADBM_PASSWD,
				&hadbmaxprops->hadbm_passwordfile,
				print_messages);
	if (err != SCHA_ERR_NOERR) {
		/* Message already logged and printed. */
		return (1);
	}
	scds_syslog_debug(DBG_LEVEL_HIGH, "hadb root <%s> ma initd <%s>"
					" ma uid <%s> ma cfg <%s>"
					" hadbm admin password file <%s>",
				hadbmaxprops->hadb_ma_root,
				hadbmaxprops->hadb_ma_start,
				hadbmaxprops->hadb_ma_user,
				hadbmaxprops->hadb_ma_cfg,
				hadbmaxprops->hadbm_passwordfile);

	return (0);
}

/*
 * This function validates the ma user, retrieves the uid, gid of the user
 *
 * Return 0 if succeed, 1 otherwise.
 */
int
validate_ma_user(hadb_ma_extprops_t *hadbmaxprops, boolean_t print_errors)
{
	struct passwd  *ma_usr_pwd = NULL;
	struct passwd   pwd;
	char		buf[SCDS_ARRAY_SIZE];

	/*
	 * MT safe routine getpwnam_r instead of getpwnam(). Resulting
	 * pointer is the same as the pointer to pwd.
	 */

	/* get user information from ma user, need to validate user */
	ma_usr_pwd = getpwnam_r(hadbmaxprops->hadb_ma_user, &pwd, buf,
				sizeof (buf));
	if (ma_usr_pwd == NULL) {
		if (errno == ERANGE) {
			ds_internal_error("Data buffer for user %s "
						"is not big enough",
						hadbmaxprops->hadb_ma_user);
			if (print_errors) {
				(void) fprintf(stderr,
						gettext
						("Data buffer for user %s "
						"is not big enough.\n"),
						hadbmaxprops->hadb_ma_user);
			}
		}
		else
		{
			/*
			 * SCMSGS
			 * @explanation
			 * Failed to retrieve information for the specified HADB
			 * MA user
			 * @user_action
			 * Check if the proper HADB MA User ID is set
			 * or check if this user exists on all the nodes of
			 * the cluster.
			 */
			scds_syslog(LOG_ERR,
				    "Failed to retrieve information for "
				    "user %s.", hadbmaxprops->hadb_ma_user);
			if (print_errors) {
				(void) fprintf(stderr,
						gettext
						("Failed to retrieve user "
						"information for <%s>.\n"),
						hadbmaxprops->hadb_ma_user);
			}
		}
		return (1);	/* user not found, return failure */
	}
	hadbmaxprops->ma_userid = pwd.pw_uid;	/* save ma uid */
	hadbmaxprops->ma_grpid = pwd.pw_gid;	/* save ma gid */
	return (0);		/* user found, return success */
}
