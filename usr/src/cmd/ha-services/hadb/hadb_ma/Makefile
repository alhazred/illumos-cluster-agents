#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)Makefile	1.6	07/06/06 SMI"
#
# cmd/ha-services/hadb/hadb_ma/Makefile

DS_COMMON = $(SRC)/cmd/ha-services/common

DS_COMMON_SRCS = $(DS_COMMON)/ds_utils.c \
        $(DS_COMMON)/ds_http.c

LIBDS_COMMON_ARCH = $(MACH)

SRCS =	hadb_ma_svc_start.c \
        hadb_ma_svc_stop.c \
        hadb_ma_validate.c \
        hadb_ma_update.c \
        hadb_ma_monitor_start.c \
        hadb_ma_monitor_stop.c \
        hadb_ma_monitor_check.c \
        hadb_ma_probe.c

SHCRIPTS = ma_status.sh

SHPROG = $(SHCRIPTS:%.sh=%)

CPROG = $(SRCS:%.c=%)

PROG = $(CPROG) $(SHPROG)

OBJS = $(SRCS:%.c=%.o)

COMMON_SRCS = hadb_ma.c \
	hadb_ma_util.c

COMMON_OBJS = $(COMMON_SRCS:%.c=%.o)

OBJECTS = $(OBJS) $(COMMON_OBJS)

LINT_SRCS = $(SRCS) $(COMMON_SRCS)

include ../../../Makefile.cmd

# Packaging
PKGNAME = SUNWschadb/hadb_ma
RTRFILE = SUNW.hadb_ma

TEXT_DOMAIN = SUNW_SC_HADB
MESSAGE_DIR = /opt/$(PKGNAME)/lib/locale
PIFILES = $(SRCS:%.c=%.pi) $(COMMON_SRCS:%.c=%.pi) \
        $(DS_COMMON_SRCS:%.c=%.pi)
POFILE = hadb_ma-ds.po

LDLIBS	+= -ldsdev -lscha
LDLIBS  += -L$(DS_COMMON)/$(LIBDS_COMMON_ARCH) -lds_common

COPTFLAG += -D_REENTRANT

LINTFILES= $(LINT_SRCS:%.c=%.ln)

.KEEP_STATE:

all: $(COMMON_OBJS) $(PROG) $(ROOTOPTETCRTR)

.PARALLEL: $(PROG)

install: all $(ROOTOPTBINPROG)

include ../../../Makefile.targ

$(SRCS:%.c=%): $(COMMON_OBJS) $$(@:%=%.o)
	$(LINK.c) -o $@ $(@:%=%.c) $(COMMON_OBJS) $(LDFLAGS) $(LDLIBS)
	$(POST_PROCESS)

$(SHCRIPTS:%.sh=%): $(SHCRIPTS)
	-cp  $(@:%=%.sh) $@
	chmod +x $@

clean:
	$(RM) $(PROG) $(OBJS) $(COMMON_OBJS)
