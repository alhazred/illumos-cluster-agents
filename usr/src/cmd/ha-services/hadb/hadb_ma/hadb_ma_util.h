/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2004,2005 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_HADB_MA_UTIL_H
#define	_HADB_MA_UTIL_H

#pragma ident	"@(#)hadb_ma_util.h	1.5	07/06/06 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#include <sys/wait.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pwd.h>
#include <sys/types.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <sys/time.h>
#include <libgen.h>
#include <scha.h>
#include <libdsdev.h>
#include <sys/param.h>
#include <sys/fcntl.h>
#include <rgm/libdsdev.h>
#include <fcntl.h>
#include <sys/procfs.h>

scha_err_t get_string_ext_property(scds_handle_t scds_handle,
	char *prop_name, char **stringval, boolean_t print_errors);

size_t strmin(char *s1, char *s2);

int
is_process_running(char *ps_name, boolean_t print_errors);

int
get_ma_server_port(char *ma_config_file, int *portno, boolean_t print_errors);

#ifdef __cplusplus
}
#endif

#endif	/* _HADB_MA_UTIL_H */
