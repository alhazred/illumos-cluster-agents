/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2004,2005 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * hadb_ma_svc_start.c - Start method for hadb ma
 */

#pragma ident	"@(#)hadb_ma_svc_start.c	1.5	07/06/06 SMI"

#include <unistd.h>
#include <rgm/libdsdev.h>
#include "hadb_ma.h"

/*
 * The start method for hadb. Does some sanity checks on the resource
 * settings then starts the hadb ma under PMF with an action script.
 */

int
main(int argc, char *argv[])
{
	scds_handle_t   scds_handle;
	char		*rgname, *rsname;
	int		rc = 0;
	hadb_ma_extprops_t hadbmaxprops;

	/*
	 * Process all the arguments that have been passed to us from RGM and
	 * do some initialization for syslog
	 */

	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR) {
		return (1);
	}
	rsname = (char *)scds_get_resource_name(scds_handle);
	rgname = (char *)scds_get_resource_group_name(scds_handle);

	/*
	 * Populate the ma extension property information
	 */

	if (hadb_ma_get_extensions(scds_handle, &hadbmaxprops, B_TRUE) != 0) {
		scds_syslog(LOG_ERR,
			    "Failed to retrieve extension properties.");
		rc = 1;
		goto finished;
	}
	/* Validate the configuration and if there is an error return back */
	rc = svc_validate(scds_handle, &hadbmaxprops, B_FALSE);
	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to validate configuration.");
		goto finished;
	}
	/*
	 * Start the data service, if it fails return with an error
	 *
	 * svc_start() will start the hadb ma under pmf control
	 */

	rc = svc_start(scds_handle, &hadbmaxprops);
	if (rc != 0) {
		goto finished;
	}

	/*
	 * Wait for the service to start up fully after the svc_wait succeeds
	 * update the resource status message.
	 */

	scds_syslog_debug(DBG_LEVEL_HIGH,
			"Done with svc_start, calling svc_wait.");

	rc = svc_wait(scds_handle, &hadbmaxprops);

	scds_syslog_debug(DBG_LEVEL_HIGH,
			"Done with svc_wait which returned <%d>.", rc);

finished:
	if (rc == 0)
		(void) scha_resource_setstatus(rsname, rgname,
						SCHA_RSSTATUS_OK,
						"Successfully started hadb "
						"ma");
	else if (rc == 2)
		(void) scha_resource_setstatus(rsname, rgname,
						SCHA_RSSTATUS_FAULTED,
						"Offline - MA running outside "
						"Sun Cluster");
	else
		(void) scha_resource_setstatus(rsname, rgname,
						SCHA_RSSTATUS_FAULTED,
						"Failed to start hadb "
						"ma");

	/* Free up the Environment resources that were allocated */
	scds_close(&scds_handle);

	return (rc);
}
