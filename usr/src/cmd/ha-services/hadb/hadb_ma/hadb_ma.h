/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2004,2005 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_HADB_MA_H
#define	_HADB_MA_H

#pragma ident	"@(#)hadb_ma.h	1.5	07/06/06 SMI"

#include <limits.h>
#include "hadb_ma_util.h"

#ifdef __cplusplus
extern "C" {
#endif

	/* Debug levels for error messages */
#define	DBG_LEVEL_HIGH		9
#define	DBG_LEVEL_MED		5
#define	DBG_LEVEL_LOW		1

#define	SCDS_CMD_SIZE		(8 * 1024)

#define	SCDS_ARRAY_SIZE		1024

/*
 * Application name defined for use in scds_syslog messages. Using
 * this allows identical messages to be defined in more than one data
 * service, hashing to one message explanation.
 */

#define	APP_NAME	"HADB MA"

/*
 * How long to wait for final stop attempt.  SC3.1 supports passing
 * -1 to mean wait forever.  In SC3.0 we need to settle for INT_MAX
 * or approximately 68 years.
 *
 * "Forever" is of course until RGM times out the stop method.
 */

#define	FINAL_STOP_TIMEOUT	INT_MAX

#define	MA_PROCESS		"ma"

#define	HADB_MA_DEF_PORT	1862

#define	MA_STATUS_SCRIPT	"ma_status"

/* hadb ma extensions */

#define	MA_EXT_HADBROOT		"HADB_ROOT"
#define	MA_EXT_START		"HADB_MA_START"
#define	MA_EXT_STOP		"HADB_MA_START"
#define	MA_EXT_USER		"HADB_MA_USER"
#define	MA_EXT_CFG		"HADB_MA_CFG"
#define	MA_EXT_HADBM_PASSWD	"HADBM_PASSWORDFILE"

/*
 * Information on hadb management agent. extension property
 */
typedef struct hadb_ma_extprops {
	char 		*hadb_ma_root;	/* hadb root directory */
	char		*hadb_ma_start;	/* hadb ma start script */
	char		*hadb_ma_stop;	/* hadb ma stop script */
	char		*hadb_ma_user;	/* User id to launch ma */
	char		*hadb_ma_cfg;	/* hadb ma config file */
	/* just set in validate, no real extensions */
	uid_t		ma_userid;	/* ma uid */
	gid_t		ma_grpid;	/* ma gid */
	int 		hadb_ma_port;   /* ma sever port */
	char		*hadbm_passwordfile;    /* hadbm admin password */
}hadb_ma_extprops_t;


/* Function to fill in hadb ma specific properties */
int hadb_ma_get_extensions(scds_handle_t handle,
	hadb_ma_extprops_t *hadbmaxprops, boolean_t print_messages);

/* Validation and update */
int svc_validate(scds_handle_t handle, hadb_ma_extprops_t *hadbmaxprops,
	boolean_t print_messages);
int svc_update(scds_handle_t scds_handle, hadb_ma_extprops_t *hadbmaxprops);

/* Basic start and stop functionality */
int svc_start(scds_handle_t handle, hadb_ma_extprops_t *hadbmaxprops);
int svc_stop(scds_handle_t handle, hadb_ma_extprops_t *hadbmaxprops);

/* Fault monitor functions */
int mon_start(scds_handle_t handle);
int mon_stop(scds_handle_t handle);

/* Probe related functions */
int svc_probe(scds_handle_t handle, hadb_ma_extprops_t *hadbmaxprops,
	int probetimeout);

int svc_wait(scds_handle_t handle, hadb_ma_extprops_t *hadbmaxprops);

int validate_ma_user(hadb_ma_extprops_t *hadbmaxprops,
	boolean_t print_errors);

#ifdef __cplusplus
}
#endif

#endif	/* _HADB_MA_H */
