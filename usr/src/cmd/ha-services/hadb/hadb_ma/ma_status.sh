#!/bin/sh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#ident	"@(#)ma_status.sh	1.7	07/06/06 SMI"
#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#
# This script probes the ma. 
# Usage: ma_status HADBROOT PORTNO
#
# hadbm is used to probe the aliveness of MA,it requires a list
# of hosts (and a port) for the management agent it  connects to. 
# since the probing is for the ma running on local host , the 
# url contains local host in it's list.
# we pass a dummy command and grep for the error code Error:22006
# 22006 means None of the agents specified in url{0} could be reached.
#
# Error 22012: The management agent at host localhost is not ready to
# execute the operation, since it is about to do repository recovery.
# Please make sure that a majority of the management agents in the
# domain are running, and retry the operation later.
#
# Return:
#        1 if ma is not running , 2, if ma is in waiting state, 
#	 0 if ma is running and able to accept client requests

PATH=/bin:/usr/bin:/usr/cluster/bin:/usr/sbin:/usr/cluster/lib/sc:$PATH;
export PATH
syslog_tag="SC[SUNW.hadb_ma,ma_status]"

if [ $# -lt 2 -o $# -gt 4 ]
then
	# SCMSGS
	# @explanation
	# Need explanation of this message!
	# @user_action
	# Need a user action for this message.
	scds_syslog -p error -t $syslog_tag -m \
		"%s %s -or- %s" \
		"usage: " \
		"$0 <hadb root> <port no>" \
		"$0 <hadb root> <port no> <hadbm passwordfile>"

	exit 1
fi

HADB_ROOT=$1
PORT_NO=$2
#
# 2>&1 associate fd (2) with fd (1)
# we are looking for errors on stderr
#

# If the error as shown below we decide the MA is not ready for client
# requests:

# "hadbm:Error 22006: The agents localhost:portno could not be reached."
#
# "hadbm:Error 22012: The management agent at host localhost is not ready to
# execute the operation, since it is about to do repository recovery.
# Please make sure that a majority of the management agents in the
# domain are running, and retry the operation later.


ErrStr=""
if [ $#  -eq 2 ]
then
	# No admin authentication
	ErrStr=`$HADB_ROOT/bin/hadbm list --agent=localhost:$PORT_NO 2>&1`
else
	HADBM_PASSWORD_FILE=$3
	ErrStr=`$HADB_ROOT/bin/hadbm list --agent=localhost:$PORT_NO \
				--adminpasswordfile=$HADBM_PASSWORD_FILE 2>&1`
fi

echo $ErrStr | /usr/bin/grep 22006: >/dev/null
rc=$?
if [ $rc -ne 1 ]
then
	# SCMSGS
	# @explanation
	# Could not reach the Management agent at localhost
	# @user_action
	# Look for other syslog error messages on the same node. Save
	# a copy of the /var/adm/messages files on all nodes, and
	# report the problem to your authorized Sun service provider.
	scds_syslog -p error -t $syslog_tag -m \
		"Failed to reach the agents localhost:%s" $PORT_NO 
        exit 1
else
	echo $ErrStr | /usr/bin/grep 22012: >/dev/null
	rc=$?
	if [ $rc -ne 1 ]
	then
		# SCMSGS
		# @explanation
		# The management agent at host localhost is not ready
		# to execute the operation, since it is about to do
		# repository recovery.
		# @user_action
		# Make sure that a majority of the management agents in
		# the domain are running.
		scds_syslog -p notice -t $syslog_tag -m \
			"Local HADB Management Agent is Waiting for other MAs to Join"
		exit 2
	else
        	exit 0
	fi
fi
