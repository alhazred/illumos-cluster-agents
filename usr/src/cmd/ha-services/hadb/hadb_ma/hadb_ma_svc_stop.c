/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * hadb_ma_svc_stop.c - Stop method for hadb ma
 */

#pragma ident	"@(#)hadb_ma_svc_stop.c	1.5	07/06/06 SMI"

#include <rgm/libdsdev.h>
#include "hadb_ma.h"

/*
 * Stops the hadb ma process using PMF
 */

int
main(int argc, char *argv[])
{
	scds_handle_t	scds_handle;
	char		*rgname, *rsname;
	int		rc;
	hadb_ma_extprops_t hadbmaxprops;

	/* Process the arguments passed by RGM and initalize syslog */
	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR) {
		return (1);
	}
	rsname = (char *)scds_get_resource_name(scds_handle);
	rgname = (char *)scds_get_resource_group_name(scds_handle);

	/* Get the hadb ma extension properties */

	if (hadb_ma_get_extensions(scds_handle, &hadbmaxprops, B_TRUE) != 0) {
		scds_syslog(LOG_ERR,
			    "Failed to retrieve extension properties.");
		rc = 1;
		goto finished;
	}

	/* Retrieve ma user credentials */
	rc = validate_ma_user(&hadbmaxprops, B_FALSE);
	if (rc != 0) {
		rc = 1;
		goto finished;
	}

	rc = svc_stop(scds_handle, &hadbmaxprops);

finished:
	if (rc != 0)
		(void) scha_resource_setstatus(rsname, rgname,
						SCHA_RSSTATUS_FAULTED,
						"Failed to stop the hadb "
						"ma.");
	else
		(void) scha_resource_setstatus(rsname, rgname,
						SCHA_RSSTATUS_OFFLINE,
						"Successfully stopped the hadb "
						"ma.");

	/* Free up all the memory allocated by scds_initialize */
	scds_close(&scds_handle);

	/* Return the result of svc_stop method */
	return (rc);
}
