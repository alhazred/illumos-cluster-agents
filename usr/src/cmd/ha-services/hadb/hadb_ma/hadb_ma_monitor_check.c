/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * hadb_ma_monitor_check.c - Monitor Check method for hadb ma
 */

#pragma ident	"@(#)hadb_ma_monitor_check.c	1.5	07/06/06 SMI"

#include <rgm/libdsdev.h>
#include "hadb_ma.h"

/*
 * just make a simple validate check on the service
 */

int
main(int argc, char *argv[])
{
	scds_handle_t	scds_handle;
	int		rc;
	hadb_ma_extprops_t hadbmaxprops;

	/* Process the arguments passed by RGM and initialize syslog */
	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR) {
		return (1);
	}
	/*
	 * Populate the ma extension property information
	 */

	if (hadb_ma_get_extensions(scds_handle, &hadbmaxprops, B_TRUE) != 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * An internal error occurred in the rgmd while checking a
		 * cluster property.
		 * @user_action
		 * Serious error. The RTR file may be corrupted. Reload the
		 * package for HA-HADB MA. If problem persists save a copy of
		 * the /var/adm/messages files on all nodes and contact the
		 * Sun Cluster HA developer.
		 */
		scds_syslog(LOG_ERR,
			    "Failed to retrieve extension properties.");
		rc = 1;
		goto finished;
	}
	rc = svc_validate(scds_handle, &hadbmaxprops, B_FALSE);
	scds_syslog_debug(DBG_LEVEL_HIGH,
			"monitor_check method "
			"was called and returned <%d>.", rc);
finished:
	/* Free up all the memory allocated by scds_initialize */
	scds_close(&scds_handle);

	/* Return the result of validate method run as part of monitor check */
	return (rc);
}
