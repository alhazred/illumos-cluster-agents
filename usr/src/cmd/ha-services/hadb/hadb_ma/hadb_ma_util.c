/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * hadb_ma_util.c - Generic data service utilities for hadb
 */

#pragma ident	"@(#)hadb_ma_util.c	1.7	07/06/06 SMI"

/*
 * Contains utility functions for more easily retrieving extenstion
 * properties, and wrappers around a number of SCHA routines that do not have
 * DSDL counterparts.
 *
 * Interface notes:
 *
 * - These routines log and (possibly) print relevant error messages so callers
 * can concentrate on the default, non-error cases.
 *
 * - Most of the returns are scha_err_t to keep the interfaces consistent even
 * when returning the actual result would be marginally better.
 *
 * - Printing of error messages is controlled by print_errors().  By default
 * error messages are not printed, but if print_errors(B_TRUE) is called
 * error messages will be printed to stderr.  It's done this way rather then
 * sticking a print flag at the end of each parameter list to keep the
 * interfaces a little less cluttered.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <unistd.h>
#include <libintl.h>
#include <sys/stat.h>
#include <sys/utsname.h>
#include <sys/types.h>
#include <sys/statvfs.h>
#include <scha.h>
#include <unistd.h>
#include <regex.h>
#include <ctype.h>
#include <rgm/libdsdev.h>
#include "hadb_ma.h"
#include "hadb_ma_util.h"
#include "../../common/ds_common.h"

/*
 * Retrieve a string extension property.
 * On success return SCHA_ERR_NOERR.  The returned string value may
 * be NULL on success, it will always be NULL if an error occured.
 *
 * The caller is responsible for freeing the returned string which is duplicated
 * from the underlying extension property.
 *
 * Please note the underlying extension property is not freed.  This is a memory
 * leak but freeing it caused a problem on a previous version of cluster.
 * This is normally OK since methods are short lived so the leak is
 * manageable.  The exception is the probe - be careful not to call this
 * function in the probe's loop.
 */

scha_err_t
get_string_ext_property(scds_handle_t scds_handle, char *prop_name,
			char **stringval, boolean_t print_errors)
{
	int		rc;
	scha_extprop_value_t *extprop = NULL;

	rc = scds_get_ext_property(scds_handle, prop_name,
				SCHA_PTYPE_STRING, &extprop);

	if (rc != SCHA_ERR_NOERR || extprop == NULL) {
		scds_syslog(LOG_ERR,
			    "Failed to retrieve the property %s: %s.",
			    prop_name, scds_error_string(rc));
		if (print_errors)
			(void) fprintf(stderr,
					gettext
					("Failed to retrieve the property "
					"%s: %s."), prop_name,
					scds_error_string(rc));

		*stringval = NULL;
		if (rc == SCHA_ERR_NOERR)	/* extprop is NULL */
			return (SCHA_ERR_INVAL);
		else
			return (rc);
	}
	if (extprop->val.val_str == NULL) {
		scds_syslog(LOG_INFO,
			    "Extension property <%s> has a value of <%s>",
			    prop_name, "NULL");
		*stringval = NULL;
	}
	else
	{
		*stringval = strdup(extprop->val.val_str);
		if (*stringval == NULL) {
			rc = SCHA_ERR_NOMEM;
			scds_syslog(LOG_ERR, "Out of memory.");
			if (print_errors)
				(void) fprintf(stderr,
						gettext("Out of memory."));
		}
		else
			scds_syslog(LOG_INFO,
				    "Extension property <%s> has a value of "
				    "<%s>", prop_name, *stringval);
	}

	/*
	 * Freeing extprop would cause a problem on a previous version of
	 * cluster so don't.  Because of this don't run this function in
	 * loop.
	 */
	return (rc);
}

/*
 * strmin()
 * Evaluate the length of strings s1 & s2 and return the minimum length
 *
 * Return values: minimum length of strings s1 & s2
 */

size_t
strmin(char *s1, char *s2)
{
	size_t		x;
	size_t		y;

	/* compute string length */
	x = strlen(s1);
	y = strlen(s2);
	/* return minimum */
	return (((x) < (y) ? (x) : (y)) - 1);
}

/*
 * is_process_running()
 * Check if MA is running outside SC agent control
 * Go thru procfs interface to see if it is running
 *
 * Return values: 0 if MA is not running
 *                1: if MA is running
 */

int
is_process_running(char *ps_name, boolean_t print_errors)
{
	int		rc = 0;
	int		exit_status;
	char		cmd[2048];
	FILE		*fp;
	pid_t		pid;

	scds_syslog_debug(DBG_LEVEL_HIGH, "in is_process_running()...");

	if (os_newer_than_s10()) {
		(void) sprintf(cmd, "/usr/bin/pgrep -x -z global "
				    "\\^%s ", ps_name);
	} else {
		(void) sprintf(cmd, "/usr/bin/pgrep -x "
				    "\\^%s ", ps_name);
	}

	fp = popen(cmd, "r");
	if (fp == NULL) {
		scds_syslog(LOG_ERR,
		    "Cannot execute %s: %s.",
		    cmd, strerror(errno));  /*lint !e746 */
		return (1);
	}

	fscanf(fp, "%d", &pid);

	exit_status = pclose(fp);
	if (WIFEXITED((uint_t)exit_status)) {
		exit_status = WEXITSTATUS((uint_t)exit_status);
		scds_syslog_debug(DBG_LEVEL_HIGH,
				"%s exited with status %d",
				cmd, exit_status);
	} else {
		rc = -1;
		scds_syslog_debug(DBG_LEVEL_HIGH,
				"%s returned with status %d",
				cmd, rc);
	}

	if (exit_status == 0) {
		scds_syslog_debug(DBG_LEVEL_HIGH,
				"%s is running = <%d> ",
				ps_name, pid);
		if (print_errors) {
			(void) fprintf(stderr, gettext
				("HADB :MA %d is already running outside "
				" Suncluster\n"), pid);
		}
		rc = 1;
	} else {
		scds_syslog_debug(DBG_LEVEL_HIGH, "MA is not running ");
	}

	return (rc);	/* return process found/not found status */
}

/*
 * get_ma_server_port()
 *
 * retrieve port number from config file.
 *
 * Returns: 0, if port number is retrieved properly, 1 otherwise ma server jmxmp
 * port is saved in portno
 *
 * errno is left intact so it can be checked for more info if the library
 * routine fails.
 */

int
get_ma_server_port(char *ma_config_file, int *portno, boolean_t print_errors)
{
	FILE		*in;
	regex_t		reg;
	/* MA server communication port search pattern  */
	char		*pattern = "^[^#].*[^#].*jmxmp\\.port.*[0-9]";
	char		*s;
	char		line[SCDS_ARRAY_SIZE];
	int		err = 0;
	int		rc;

	scds_syslog_debug(DBG_LEVEL_HIGH, "in get_ma_server_port()...");

	/*
	 * compile the regular  expression pointed to by the pattern argument
	 */
	err = regcomp(&reg, pattern, REG_EXTENDED | REG_NOSUB);
	if (err != 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * An internal error has occurred.
		 * @user_action
		 * Save a copy of the /var/adm/messages files on all nodes.
		 * Contact your authorized Sun service provider for assistance
		 * in diagnosing the problem.
		 */
		scds_syslog(LOG_ERR,
			    "Regular expression compile error %s: %s",
			    pattern, strerror(errno));	/*lint !e746 */
		if (print_errors) {
			(void) fprintf(stderr,
				gettext
				("Regular expression compile error %s: %s\n"),
				pattern, strerror(errno));	/*lint !e746 */
		}
		return (1);
	}
	/* open mgt.cfg file to read port number */
	in = fopen(ma_config_file, "r");
	if (in == NULL) {
		scds_syslog(LOG_ERR,
			    "Unable to open %s: %s.",
			    ma_config_file, strerror(errno));	/*lint !e746 */

		if (print_errors) {
			(void) fprintf(stderr,
					gettext("Unable to open %s: %s."),
					ma_config_file,
					strerror(errno));	/*lint !e746 */
		}
		regfree(&reg);
		return (1);
	}
	rc = 1;
	while (!feof(in)) {	/* while EOF not encountered */
		s = fgets(line, sizeof (line), in);	/* read a line */
		if (s == NULL || feof(in)) {	/* is EOF */
			break;	/* stop and bail out */
		}
		err = regexec(&reg, s, (size_t)0, NULL, 0);
		if (err == 0) {	/* match found */
			while (*s != '\0' && !isdigit(*s))
				s++;
			if (*s != '\0') {
				/* save port no */
				err = sscanf(s, "%d", portno);
				if (err != -1) {
					rc = 0;
					scds_syslog_debug(DBG_LEVEL_HIGH,
					    "port number from mgt.cfg is <%d>",
					    *portno);
				}
			}
			break;
		}
	}

	regfree(&reg);		/* free pattern */
	err = fclose(in);	/* close file */
	scds_syslog_debug(DBG_LEVEL_HIGH, "Port retrival status <%d>", rc);
	return (rc);
}
