/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2003 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * hadb_svc_stop.c - Stop method for hadb
 */

#pragma ident	"@(#)hadb_svc_stop.c	1.4	07/06/06 SMI"

#include <unistd.h>
#include <rgm/libdsdev.h>
#include "hadb.h"

/*
 * Stops the hadb process using PMF
 */

int
main(int argc, char *argv[])
{
	scds_handle_t	scds_handle;
	int		rc;
	char		*rgname, *rsname;

	/* Process the arguments passed by RGM and initalize syslog */
	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR) {
		return (1);
	}

	rsname = (char *)scds_get_resource_name(scds_handle);
	rgname = (char *)scds_get_resource_group_name(scds_handle);

	/*
	 * Sleep for a few moments so that RGM has a chance to update
	 * the RG status on all the nodes in the RG so that all the
	 * stopping SC nodes have a consistent picture of what is
	 * happening.
	 *
	 * This prevents node A from getting status first before RGM
	 * updates that node B is also stopping leading node A to
	 * think that node B is not stopping.
	 */

	(void) sleep(PRE_RG_SETTLE_TIME);

	rc = get_hadb_config_info(scds_handle, 0);
	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Error retrieving configuration "
			"information.");
		return (1);
	}

	/*
	 * It's not clear if this is needed or not.  The concern is
	 * that the stop method on node A could run fast enough and
	 * exit before node B gets the status node A so that when B
	 * finally gets the status for A it thinks A is stopped.
	 *
	 * It's unlikely that this would happen but this makes it a
	 * little "safer" - sleeping to get everyone who is stopping
	 * on the same page is a poor substitute for real
	 * synchronization
	 */

	(void) sleep(POST_RG_SETTLE_TIME);

	rc = svc_stop(scds_handle);

	if (rc != SCHA_ERR_NOERR)
		(void) scha_resource_setstatus(rsname, rgname,
			SCHA_RSSTATUS_FAULTED,
			"Failed to stop Sun ONE HADB.");
	else
		(void) scha_resource_setstatus(rsname, rgname,
			SCHA_RSSTATUS_OFFLINE,
			"Successfully stopped Sun ONE HADB.");

	/* Free up all the memory allocated by scds_initialize */
	scds_close(&scds_handle);

	/* Return the result of svc_stop method */
	return (rc);
}
