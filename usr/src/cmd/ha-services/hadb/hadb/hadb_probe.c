/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * hadb_probe.c - Probe for hadb
 */

#pragma ident	"@(#)hadb_probe.c	1.6	07/06/06 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <rgm/libdsdev.h>
#include "../../common/ds_common.h"
#include "hadb.h"

/*
 * The probe for HADB is unusual compared to the typical probes for
 * other data services.
 *
 *
 * The big difference is that we may be starting the HADB database
 * from the probe.  This is done because we may not be able to start
 * the database at the time that the start method is run.
 *
 * Take for example a 4 HADB node site that is running across 4 SC
 * nodes.  The database will not be able to be started until at least
 * two of the SC nodes are running the resource.  So if only one SC
 * node starts at a time we don't want to fail the start method simply
 * because not enough nodes are online yet.  We will allow the start
 * method to complete successfully and and allow the database to be
 * started later once the required SC nodes are online.
 *
 * An additional complication is that the database start command can
 * not be run by just any SC node.  It has to be run from the SC node
 * that has the stopstate file which will be on the node that ran the
 * database stop command.  So in the above four node example if node 1
 * has the stopstate file and it's the first one up, when node 2 comes
 * online it can't simply run the start command in it's start method,
 * it has to be run on node 1 and it's start method is long finished,
 * so we have the probe run the start command when conditions allow
 * it.
 *
 *
 * Another difference is we don't rely on RGM restarting the resource
 * by calling scds_fm_action.  In the probe we will handle restarting
 * individual HADB nodes.  We do this for two reasons 1) multiple HADB
 * nodes may be running on the SC nodes, we don't necessarily want to
 * restart all of them because one is having a problem 2) "failing
 * over" does not make sense for HADB.
 *
 *
 * The only probing we do of HADB is by running "hadbm status" and
 * "hadbm status --nodes".  We can't do a more active probe such as
 * executing a database query because an HADB node will silently
 * ignore new connection requests after it has reached it's limit.
 *
 * Future enhancements of the HADB managment framework may allow more
 * active probing, however the hadbm status commands seem robust and
 * accurate.
 */

int
main(int argc, char *argv[])
{
	scds_handle_t	scds_handle;
	scha_err_t 	rc;
	int		interval;

	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR)
		return (1);

	/*
	 * How long to sleep between probes.
	 */
	interval = scds_get_rs_thorough_probe_interval(scds_handle);

	/*
	 * Sleep before we get the configuration and status
	 * information of the resource for the first time.
	 */
	(void) scds_fm_sleep(scds_handle, interval);

	rc = get_hadb_config_info(scds_handle, B_FALSE);
	if (rc != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * An error was encountered while running hadbm status.
		 * @user_action
		 * Examine other syslog messages occurring around the same
		 * time on the same node, to see if the source of the problem
		 * can be identified. Try running the hadbm status command
		 * manually for the HADB database.
		 */
		scds_syslog(LOG_ERR, "Error retrieving configuration "
			"information.");
			return (1);
	}

	for (;;) {
		/*
		 * If all the HADB nodes are running, then clear the
		 * startnode_error flag. This is a very sub-optimal
		 * way of erasing the startnode_error flag, just in
		 * case, we see crazy behaviour due to our agent's hadbm
		 * usage: 4949663.
		 *
		 * Moreover, the primary driver of agent logic is
		 * the status of the HADB database. Previously, in
		 * update_hadb_config_info() hadb status is first found
		 * and then followed by db status. This could lead to
		 * situations like in 4949663. Changing the order will
		 * help us in such situations and that coupled with
		 * below check will atleast make the messages transient.
		 *
		 * Optimal way: Sorry, not any sooner. 4915984's closed.
		 */

		if (all_local_nodes_running(scds_handle))
			site_info.startnode_error = B_FALSE;

		/*
		 * If the database is running, check to see if any
		 * local HADB nodes are not running.
		 *
		 * If they are not running and the database is running
		 * then ask svc_start() to start any non running local
		 * HADB nodes.
		 */

		if (database_running(scds_handle) &&
		    !all_local_nodes_running(scds_handle)) {
			rc = svc_start(scds_handle);
			if (rc != SCHA_ERR_NOERR) {
				/*
				 * Error starting one or more of the
				 * local HADB nodes.  Note that if we
				 * did not start nodes because we're
				 * not ready that was not counted as
				 * an error.
				 *
				 * Set the startnode error flag so
				 * that the resource status will get
				 * updated to reflect the error down
				 * below..
				 */

				site_info.startnode_error = B_TRUE;
			} else {
				site_info.startnode_error = B_FALSE;
			}
		}

		/*
		 * If the database is not running attempt to start it.
		 * Don't bother checking what the return is, if we
		 * can't start it the messages are already logged and
		 * that resource status will be updated in a moment.
		 */

		if (!database_running(scds_handle)) {
			(void) svc_start_database(scds_handle);

			/*
			 * If we attempted to start the database
			 * update the database status.
			 */

			rc = get_hadb_status(scds_handle,
				&(site_info.status));
			if (rc != SCHA_ERR_NOERR)
				scds_syslog(LOG_ERR, "Unable to determine "
					"database status.");
		}

		/*
		 * We may have started hadb nodes or even the
		 * database.  Update the resource status with the
		 * latest information.
		 */

		update_resource_status(scds_handle);

		/*
		 * Sleep for a duration of thorough_probe_interval
		 * between successive probes.
		 */
		(void) scds_fm_sleep(scds_handle, interval);

		/*
		 * Update the config/status information before
		 * checking to see if we need to do an autorecovery
		 * and going through the probe loop again.
		 */

		rc = update_hadb_config_info(scds_handle, B_FALSE);
		if (rc != SCHA_ERR_NOERR) {
			scds_syslog(LOG_ERR, "Error retrieving configuration "
				"information.");
		}

		/*
		 * If database is stopped check to see if we should do
		 * a autorecovery.
		 */

		if (strcmp(site_info.status, DB_STOPPED) == 0) {
			switch (ready_for_autorecovery(scds_handle)) {
			case AUTORECOVER_YES:
				/*
				 * SCMSGS
				 * @explanation
				 * All the Sun Cluster nodes able to run the
				 * HADB resource are running the resource, but
				 * the database is unable to be started. The
				 * database will be reinitialized by running
				 * hadbm clear and then the command, if any,
				 * specified by the auto_recovery_command
				 * extension property.
				 * @user_action
				 * This is an informational message, no user
				 * action is needed.
				 */
				scds_syslog(LOG_NOTICE, "Starting "
					"auto recovery of HADB database.");

				rc = autorecover(scds_handle);
				if (rc == SCHA_ERR_NOERR) {
					/*
					 * SCMSGS
					 * @explanation
					 * The database was successfully
					 * reinitialized.
					 * @user_action
					 * This is an informational message,
					 * no user action is needed.
					 */
					scds_syslog(LOG_NOTICE, "Successful "
						"auto recovery of HADB "
						"database.");
					site_info.auto_recover_failed =
						B_FALSE;
				} else {
					/*
					 * SCMSGS
					 * @explanation
					 * Need explanation of this message!
					 * @user_action
					 * Examine other syslog messages
					 * occurring around the same time on
					 * the same node, to see if the source
					 * of the problem can be identified.
					 */
					scds_syslog(LOG_ERR, "Auto recovery "
						"of HADB database failed.");
					site_info.auto_recover_failed =
						B_TRUE;
				}

				/*
				 * Regardless what happened, update
				 * the current config info and the
				 * update the resource status before
				 * going back to the top of the loop.
				 */

				rc = update_hadb_config_info(scds_handle,
					B_FALSE);
				if (rc != SCHA_ERR_NOERR) {
					scds_syslog(LOG_ERR, "Error "
						"retrieving configuration "
						"information.");
				}
				update_resource_status(scds_handle);

				break;
			case AUTORECOVER_READY:
				/*
				 * Ready means that all conditions for
				 * autorecovery were met except that
				 * the user has not requested it.
				 * We'll log a message so that the
				 * user will know that maybe they want
				 * to set auto_recovery to true.
				 */
				/*
				 * SCMSGS
				 * @explanation
				 * All the Sun Cluster nodes able to run the
				 * HADB resource are running the resource, but
				 * the database is unable to be started. If
				 * the auto_recovery extension property was
				 * set to true the resource would attempt to
				 * start the database by running hadbm clear
				 * and the command, if any, specified in the
				 * auto_recovery_command extension property.
				 * @user_action
				 * The database must be manually recovered, or
				 * if autorecovery is desired the
				 * auto_recovery extension property can be set
				 * to true and auto_recovery_command can
				 * optionally also be set.
				 */
				scds_syslog(LOG_NOTICE, "Database is ready "
					"for auto recovery but the "
					"Auto_recovery property is false. ");
				break;
			case AUTORECOVER_NO:
				/* do nothing */
				break;
			default:
				ds_internal_error("Unknown return from "
					"ready_for_autorecovery");
				break;
			}
		}


	} 	/* Keep probing forever */
}
