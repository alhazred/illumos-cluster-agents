/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * hadb_util.c - Generic data service utilities for hadb
 */

#pragma ident	"@(#)hadb_util.c	1.7	07/06/06 SMI"

/*
 * Contains utility functions for more easily retrieving extenstion
 * properties, and wrappers around a number of SCHA routines that do
 * not have DSDL counterparts.
 *
 * Interface notes:
 *
 * - These routines log and (possibly) print relevant error messages
 * so callers can concentrate on the default, non-error cases.
 *
 * - Most of the returns are scha_err_t to keep the interfaces
 * consistent even when returning the actual result would be
 * marginally better.
 *
 * - Printing of error messages is controlled by print_errors().  By
 * default error messages are not printed, but if print_errors(B_TRUE)
 * is called error messages will be printed to stderr.  It's done
 * this way rather then sticking a print flag at the end of each
 * parameter list to keep the interfaces a little less cluttered.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <libintl.h>
#include <sys/stat.h>
#include <sys/utsname.h>
#include <sys/types.h>
#include <sys/statvfs.h>
#include <scha.h>
#include <rgm/libdsdev.h>
#include "hadb.h"
#include "../../common/ds_common.h"
#include "hadb_util.h"

/*
 * Map RG states to printable strings
 */
typedef struct rgstatename {
	scha_rgstate_t	state;
	char		*statename;
} rgstatename_t;

/*
 * For error conditions for unknown RG and RS states
 */
#define	STATE_NOT_FOUND "state not found"

/*
 * defines RG state
 */
rgstatename_t rgstatenames[] = {
	{ SCHA_RGSTATE_UNMANAGED,		"Unmanaged" },
	{ SCHA_RGSTATE_ONLINE,			"Online" },
	{ SCHA_RGSTATE_OFFLINE,			"Offline" },
	{ SCHA_RGSTATE_PENDING_ONLINE,		"Pending Online" },
	{ SCHA_RGSTATE_PENDING_OFFLINE,		"Pending Offline" },
	{ SCHA_RGSTATE_ERROR_STOP_FAILED,	"Error: Stop Failed" },
	{ SCHA_RGSTATE_ONLINE_FAULTED,		"Online: Faulted"}
};

/*
 * Number of RG entries.
 */
#define	RG_ENTRIES	(sizeof (rgstatenames) / sizeof (rgstatenames[0]))

/*
 * Map RS states to printable strings
 */
typedef struct rsstatename {
	scha_rsstate_t	state;
	char		*statename;
} rsstatename_t;

/*
 * defines RS state
 *
 * Detached is marked reserved for future use in scha_types.h.
 */
rsstatename_t rsstatenames[] = {
	{ SCHA_RSSTATE_ONLINE,			"Online" },
	{ SCHA_RSSTATE_OFFLINE,			"Offline" },
	{ SCHA_RSSTATE_START_FAILED,		"Start Failed" },
	{ SCHA_RSSTATE_STOP_FAILED,		"Stop Failed" },
	{ SCHA_RSSTATE_MONITOR_FAILED,		"Monitor Failed" },
	{ SCHA_RSSTATE_ONLINE_NOT_MONITORED,	"Online: Not Monitored" },
	{ SCHA_RSSTATE_STARTING,		"Starting"},
	{ SCHA_RSSTATE_STOPPING,		"Stopping"},
	{ SCHA_RSSTATE_DETACHED,		"Detached"},
};

/*
 * Number of RS entries.
 */
#define	RS_ENTRIES	(sizeof (rsstatenames) / sizeof (rsstatenames[0]))

scha_err_t copy_uint_array(scha_uint_array_t **dst, scha_uint_array_t *src);

/*
 * If print_errors is true appropriate error messages will be printed
 * to stderr.
 */

static boolean_t print_errors = B_FALSE;

/*
 * Set the print errors flag to either print error messages, or not.
 * Not printing errors is the default.
 */

void
set_print_errors(boolean_t print)
{
	print_errors = print;
}

/*
 * Retrieve a string extension property.  On success return
 * SCHA_ERR_NOERR.  The returned string value may be NULL on success,
 * it will always be NULL if an error occured.
 *
 * The caller is responsible for freeing the returned string which is
 * duplicated from the underlying extension property.
 *
 * Please note the underlying extension property is not freed.  This
 * is a memory leak but freeing them caused a problem in the previous
 * release of cluster. This is normally OK since methods are short
 * lived so the leak is manageable. The exception is the probe -
 * be careful not to call this function in the probe's loop.
 */

scha_err_t
get_string_ext_property(scds_handle_t scds_handle, char *prop_name,
	char **stringval)
{
	int			rc;
	scha_extprop_value_t	*extprop = NULL;

	rc = scds_get_ext_property(scds_handle, prop_name,
		SCHA_PTYPE_STRING, &extprop);

	if (rc != SCHA_ERR_NOERR || extprop == NULL) {
		scds_syslog(LOG_ERR,
			"Failed to retrieve the property %s: %s.",
			prop_name, scds_error_string(rc));
		if (print_errors)
			(void) fprintf(stderr,
				gettext("Failed to retrieve the property "
					"%s: %s."), prop_name,
					scds_error_string(rc));

		*stringval = NULL;
		if (rc == SCHA_ERR_NOERR) /* extprop is NULL */
			return (SCHA_ERR_INVAL);
		else
			return (rc);
	}

	if (extprop->val.val_str == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * The property is set to the indicated value.
		 * @user_action
		 * This message is informational; no user action is needed.
		 */
		scds_syslog(LOG_INFO,
			"Extension property <%s> has a value of <%s>",
			prop_name, "NULL");
		*stringval = NULL;
	} else {
		*stringval = strdup(extprop->val.val_str);
		if (*stringval == NULL) {
			rc = SCHA_ERR_NOMEM;
			scds_syslog(LOG_ERR, "Out of memory.");
			if (print_errors)
				(void) fprintf(stderr,
					gettext("Out of memory."));
		} else
			scds_syslog(LOG_INFO,
				"Extension property <%s> has a value of "
				"<%s>", prop_name, *stringval);
	}
	/*
	* Freeing extprop caused a problem on a previous version of
	* cluster so don't.  Because of this don't run this
	* function in loop.
	*/
	return (rc);
}

/*
 * Similiar to get_string_ext_property, but instead fetch a boolean.
 */

scha_err_t
get_boolean_ext_property(scds_handle_t scds_handle, char *prop_name,
	boolean_t *boolval)
{
	int			rc;
	scha_extprop_value_t	*extprop = NULL;

	rc = scds_get_ext_property(scds_handle, prop_name,
		SCHA_PTYPE_BOOLEAN, &extprop);

	if (rc != SCHA_ERR_NOERR || extprop == NULL) {
		scds_syslog(LOG_ERR,
			"Failed to retrieve the property %s: %s.",
			prop_name, scds_error_string(rc));
		if (print_errors)
			(void) fprintf(stderr,
				gettext("Failed to retrieve the property "
					"%s: %s."), prop_name,
				scds_error_string(rc));

		/*
		 * In case the error return is ignored set the boolean
		 * to something hopefully benign.
		 */
		*boolval = B_FALSE;
		if (rc == SCHA_ERR_NOERR) /* extprop is NULL */
			return (SCHA_ERR_NOERR);
		else
			return (rc);
	}

	*boolval = extprop->val.val_boolean;

	scds_syslog(LOG_INFO,
		"Extension property <%s> has a value of <%s>",
		prop_name, ((*boolval == B_FALSE) ? "B_FALSE" : "B_TRUE"));

	return (SCHA_ERR_NOERR);
}

/*
 * Get the RG state on the given node.
 *
 * Returns SCHA_ERR_NOERR on success.
 *
 * Use the RG state because a scha_resourcegroup_get call is
 * guaranteed to show the RG in PENDING_OFFLINE state when the Stop
 * method executes.  However, a scha_resource_get call on a resource
 * might /not/ show that the resource has begun to stop; it might
 * still show as being ONLINE, in the case that the node is heavily
 * loaded and hasn't had a chance to run its state machine yet.  To
 * avoid this problem, the HADB Stop method should use the RG state
 * (rather than the resource state) to decide whether HADB is being
 * stopped on the other node.
 */

scha_err_t
get_rg_state_node(scds_handle_t handle, char *node, scha_rgstate_t *pstate)
{
	scha_err_t		rc;
	scha_resourcegroup_t	rghandle = NULL;
	const char		*rgname;

	/*
	 * We can not access the resource group handle via the DSDL
	 * handle so we get the resource group name, and then open a
	 * new RG handle.
	 *
	 * Checking the return for rgname is perhaps paranoid since it
	 * should be NULL only if the scds_handle_t is uninitialized.
	 */

	rgname = scds_get_resource_group_name(handle);
	if (rgname == NULL) {
		ds_internal_error("Resource group name is null.");
		if (print_errors)
			(void) fprintf(stderr,
				gettext("INTERNAL ERROR: "
					"Resource group name is null."));
		return (SCHA_ERR_NOMEM);
	}

	scds_syslog_debug(DBG_LEVEL_HIGH, "Getting RG_STATE_NODE for %s",
		node);

retry:
	rc = scha_resourcegroup_open(rgname, &rghandle);
	if (rc != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * An API operation on the resource group has failed.
		 * @user_action
		 * For the resource group name, check the syslog tag. For more
		 * details, check the syslog messages from other components.
		 * If the error persists, reboot the node.
		 */
		scds_syslog(LOG_ERR, "Failed to retrieve the resource group "
			"handle: %s.", scds_error_string(rc));
		if (print_errors)
			(void) fprintf(stderr,
				gettext("Failed to retrieve the resource "
					"group handle: %s."),
				scds_error_string(rc));
		return (rc);
	}

	rc = scha_resourcegroup_get(rghandle, SCHA_RG_STATE_NODE, node,
		pstate);
	if (rc == SCHA_ERR_SEQID) {
		/* Close the handle and retry. */
		/*
		 * SCMSGS
		 * @explanation
		 * An update to resource configuration occured while resource
		 * properties were being retrieved.
		 * @user_action
		 * This is an informational message, no user action is needed.
		 */
		scds_syslog(LOG_INFO, "Retrying retrieve of resource "
			"information: %s.", scds_error_string(rc));
		if (print_errors)
			(void) fprintf(stderr,
				gettext("Retrying retrieve of resource "
					"information: %s."),
				scds_error_string(rc));
		(void) scha_resource_close(rghandle);
		rghandle = NULL;
		(void) sleep(1);
		goto retry;
	} else if (rc != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * An attempt to retrieve a resource group property has
		 * failed.
		 * @user_action
		 * If the failure is cased by insufficient memory, reboot. If
		 * the problem recurs after rebooting, consider increasing
		 * swap space by configuring additional swap devices. If the
		 * failure is caused by an API call, check the syslog messages
		 * for the possible cause.
		 */
		scds_syslog(LOG_ERR, "Failed to retrieve the resource group "
			"property %s: %s.", SCHA_RG_STATE_NODE,
			scds_error_string(rc));
		goto close;
	}

	scds_syslog_debug(DBG_LEVEL_HIGH, "Resource group %s state on node "
		"%s is %s.", rgname, node, rgstate_to_string(*pstate));

close:
	(void) scha_resourcegroup_close(rghandle);

	return (rc);
}

/*
 * Get the resource state on the given node.
 *
 * Returns SCHA_ERR_NOERR on success.
 *
 * We typically in the HADB agent depend on the RG state to determine
 * what to do for the reasons outlined above in the comments for
 * get_rg_state_node.  However if the resource is being disabled the
 * RG state will not change so we fall back to resource state.
 */

scha_err_t
get_rs_state_node(scds_handle_t handle, char *node, scha_rsstate_t *pstate)
{
	scha_err_t	rc;
	scha_resource_t	rshandle = NULL;
	const char	*rgname;
	const char	*rsname;

	/*
	 * We can not access the resource group handle via the DSDL
	 * handle so we get the resource group and resource name, and
	 * then open a new RS handle.
	 *
	 * Checking the return for r[gs]name is perhaps paranoid since
	 * it should be NULL only if the scds_handle_t is
	 * uninitialized.
	 */

	rgname = scds_get_resource_group_name(handle);
	if (rgname == NULL) {
		ds_internal_error("Resource group name is null.");
		if (print_errors)
			(void) fprintf(stderr,
				gettext("INTERNAL ERROR: "
					"Resource group name is null."));
		return (SCHA_ERR_NOMEM);
	}

	rsname = scds_get_resource_name(handle);
	if (rgname == NULL) {
		ds_internal_error("Resource name is null.");
		if (print_errors)
			(void) fprintf(stderr,
				gettext("INTERNAL ERROR: "
					"Resource name is null."));
		return (SCHA_ERR_NOMEM);
	}

	scds_syslog_debug(DBG_LEVEL_HIGH, "Getting RESOURCE_STATE_NODE for "
		"%s", node);

retry:
	rc = scha_resource_open(rsname, rgname, &rshandle);
	if (rc != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * An API operation on the resource has failed.
		 * @user_action
		 * For the resource name, check the syslog tag. For more
		 * details, check the syslog messages from other components.
		 * If the error persists, reboot the node.
		 */
		scds_syslog(LOG_ERR, "Failed to retrieve the resource "
			"handle: %s.", scds_error_string(rc));
		if (print_errors)
			(void) fprintf(stderr,
				gettext("Failed to retrieve the resource "
					"handle: %s."),
				scds_error_string(rc));
		return (rc);
	}

	rc = scha_resource_get(rshandle, SCHA_RESOURCE_STATE_NODE, node,
		pstate);
	if (rc == SCHA_ERR_SEQID) {
		/* Close the handle and retry. */
		scds_syslog(LOG_INFO, "Retrying retrieve of resource "
			"information: %s.", scds_error_string(rc));
		if (print_errors)
			(void) fprintf(stderr,
				gettext("Retrying retrieve of resource "
					"information: %s."),
				scds_error_string(rc));
		(void) scha_resource_close(rshandle);
		rshandle = NULL;
		(void) sleep(1);
		goto retry;
	} else if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to retrieve the resource "
			"property %s: %s.", SCHA_RESOURCE_STATE_NODE,
			scds_error_string(rc));
		goto close;
	}

	scds_syslog_debug(DBG_LEVEL_HIGH, "Resource %s state on node "
		"%s is %s.", rsname, node, rsstate_to_string(*pstate));

close:
	(void) scha_resource_close(rshandle);

	return (rc);
}

/*
 * Get the list of nodeids for all the cluster nodes.
 *
 * The caller is responsible for releasing the uint_t array that is
 * returned.
 *
 * Returns SCHA_ERR_NOERR on success.
 */

scha_err_t
get_all_nodeids(scds_handle_t handle, scha_uint_array_t **pall_nodeids)
{
	/*
	 * Lint suppression.  The handle argument is not needed but
	 * keeping it keeps the interface consistent with the rest of
	 * the routines in this file.
	 */

	scha_err_t		rc;
	scha_cluster_t		clhandle = NULL;
	scha_uint_array_t	*copy;

	scds_syslog_debug(DBG_LEVEL_HIGH, "Getting ALL_NODEIDS.");

retry:
	rc = scha_cluster_open(&clhandle);
	if (rc != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * An API operation has failed while retrieving the cluster
		 * information.
		 * @user_action
		 * This may be solved by rebooting the node. For more details
		 * about API failure, check the messages from other
		 * components.
		 */
		scds_syslog(LOG_ERR, "Failed to retrieve the cluster "
			"handle: %s.", scds_error_string(rc));
		if (print_errors)
			(void) fprintf(stderr,
				gettext("Failed to retrieve the cluster "
					"handle: %s."),
				scds_error_string(rc));
		return (rc);
	}

	rc = scha_cluster_get(clhandle, SCHA_ALL_NODEIDS,
		pall_nodeids);
	if (rc == SCHA_ERR_SEQID) {
		/* Close the handle and retry. */
		/*
		 * SCMSGS
		 * @explanation
		 * An update to cluster configuration occured while cluster
		 * properties were being retrieved
		 * @user_action
		 * This is an informational message, no user action is needed.
		 */
		scds_syslog(LOG_INFO, "Retrying retrieve of cluster "
			"information: %s.", scds_error_string(rc));
		if (print_errors)
			(void) fprintf(stderr,
				gettext("Retrying retrieve of cluster "
					"information: %s."),
				scds_error_string(rc));
		(void) scha_cluster_close(clhandle);
		clhandle = NULL;
		(void) sleep(1);
		goto retry;
	} else if (rc != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * The query for a property failed. The reason for the failure
		 * is given in the message.
		 * @user_action
		 * Save a copy of the /var/adm/messages files on all nodes.
		 * Contact your authorized Sun service provider for assistance
		 * in diagnosing the problem.
		 */
		scds_syslog(LOG_ERR, "Failed to retrieve the cluster "
			"property %s: %s.", SCHA_ALL_NODEIDS,
			scds_error_string(rc));
		if (print_errors)
			(void) fprintf(stderr,
				gettext("Failed to retrieve the cluster "
					"property %s: %s."), SCHA_ALL_NODEIDS,
				scds_error_string(rc));
		goto close;
	}

	scds_syslog_debug(DBG_LEVEL_HIGH, "Retrieved %d node ids.",
		(*pall_nodeids)->array_cnt);

	/*
	 * Need to copy the uint array since the close invalidates the
	 * original.
	 */

	rc = copy_uint_array(&copy, *pall_nodeids);
	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Out of memory.");
		if (print_errors)
			(void) fprintf(stderr, gettext("Out of memory."));

		goto close;
	}

	*pall_nodeids = copy;

close:
	(void) scha_cluster_close(clhandle);

	return (rc);
	/* Suppress handle not referenced. */
} /*lint !e715 */

/*
 * Retrieve the cluster nodename for a given nodeid.
 *
 * If no error SCHA_ERR_NOERR is returned, and a pointer to the
 * nodename is returned in nodename.  The caller is responsible for
 * releasing the memory using free(3C).
 */

scha_err_t
get_nodename_from_id(scds_handle_t handle, uint_t nodeid,
	char **nodename)
{
	/*
	 * The scds_handle_t is not needed but is there for interface
	 * consistency.
	 */

	scha_err_t		rc;
	scha_cluster_t		clhandle = NULL;
	char			*orignodename;

	scds_syslog_debug(DBG_LEVEL_HIGH, "Getting NODENAME_NODEID for %u",
		nodeid);

retry:
	rc = scha_cluster_open(&clhandle);
	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to retrieve the cluster "
			"handle: %s.", scds_error_string(rc));
		if (print_errors)
			(void) fprintf(stderr,
				gettext("Failed to retrieve the cluster "
					"handle: %s."),
				scds_error_string(rc));
		return (rc);
	}

	rc = scha_cluster_get(clhandle, SCHA_NODENAME_NODEID, nodeid,
		&orignodename);
	if (rc == SCHA_ERR_SEQID) {
		/* Close the handle and retry. */
		scds_syslog(LOG_INFO, "Retrying retrieve of resource "
			"information: %s.", scds_error_string(rc));
		(void) scha_cluster_close(clhandle);
		clhandle = NULL;
		(void) sleep(1);
		goto retry;
	} else if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to retrieve the cluster "
			"property %s: %s.", SCHA_NODENAME_NODEID,
			scds_error_string(rc));
		if (print_errors)
			(void) fprintf(stderr,
				gettext("Failed to retrieve the cluster "
					"property %s: %s."),
				SCHA_NODENAME_NODEID, scds_error_string(rc));
		goto close;
	}

	/*
	 * Need to copy the nodename, since the original storage will
	 * be released when we close the handle.
	 */

	*nodename = strdup(orignodename);
	if (*nodename == NULL) {
		scds_syslog(LOG_ERR, "Out of memory.");
		if (print_errors)
			(void) fprintf(stderr, gettext("Out of memory."));
		return (SCHA_ERR_NOMEM);
	}

	scds_syslog_debug(DBG_LEVEL_HIGH, "Nodename for node %u is %s.",
		nodeid, *nodename);

close:
	(void) scha_cluster_close(clhandle);

	return (rc);

	/* Suppress handle not used. */
} /*lint !e715 */

/*
 * Retrieve the cluster private link hostname for a given nodename.
 *
 * If no error SCHA_ERR_NOERR is returned, and a pointer to the
 * nodename is returned in nodename.  The caller is responsible for
 * releasing the memory using free(3C).
 */

scha_err_t
get_privname_from_nodename(scds_handle_t handle, char *nodename,
	char **privname)
{
	/*
	 * The scds_handle_t is not needed but is there for interface
	 * consistency.
	 */

	scha_err_t		rc;
	scha_cluster_t		clhandle = NULL;
	char			*origprivname;

	scds_syslog_debug(DBG_LEVEL_HIGH, "Getting PRIVATELINK_HOSTNAME_NODE "
		"for %s", nodename);

retry:
	rc = scha_cluster_open(&clhandle);
	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to retrieve the cluster "
			"handle: %s.", scds_error_string(rc));
		if (print_errors)
			(void) fprintf(stderr,
				gettext("Failed to retrieve the cluster "
					"handle: %s."),
				scds_error_string(rc));
		return (rc);
	}

	rc = scha_cluster_get(clhandle, SCHA_PRIVATELINK_HOSTNAME_NODE,
		nodename, &origprivname);
	if (rc == SCHA_ERR_SEQID) {
		/* Close the handle and retry. */
		scds_syslog(LOG_INFO, "Retrying retrieve of resource "
			"information: %s.", scds_error_string(rc));
		(void) scha_cluster_close(clhandle);
		clhandle = NULL;
		(void) sleep(1);
		goto retry;
	} else if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to retrieve the cluster "
			"property %s: %s.", SCHA_PRIVATELINK_HOSTNAME_NODE,
			scds_error_string(rc));
		if (print_errors)
			(void) fprintf(stderr,
				gettext("Failed to retrieve the cluster "
					"property %s: %s."),
				SCHA_PRIVATELINK_HOSTNAME_NODE,
				scds_error_string(rc));
		goto close;
	}

	/*
	 * Need to copy the nodename, since the original storage will
	 * be released when we close the handle.
	 */

	*privname = strdup(origprivname);
	if (*nodename == NULL) {
		scds_syslog(LOG_ERR, "Out of memory.");
		if (print_errors)
			(void) fprintf(stderr, gettext("Out of memory."));
		return (SCHA_ERR_NOMEM);
	}

	scds_syslog_debug(DBG_LEVEL_HIGH, "Privname for node %s is %s.",
		nodename, *privname);

close:
	(void) scha_cluster_close(clhandle);

	return (rc);

	/* Suppress handle not used. */
} /*lint !e715 */

/*
 * Get the local SC node id.
 */
scha_err_t
get_local_nodeid(scds_handle_t handle, uint_t *pnodeid)
{
	/*
	 * The scds_handle_t is not needed but is there for interface
	 * consistency.
	 */

	scha_err_t		rc;
	scha_cluster_t		clhandle = NULL;

	scds_syslog_debug(DBG_LEVEL_HIGH, "Getting NODEID_LOCAL.");

retry:
	rc = scha_cluster_open(&clhandle);
	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to retrieve the cluster "
			"handle: %s.", scds_error_string(rc));
		if (print_errors)
			(void) fprintf(stderr,
				gettext("Failed to retrieve the cluster "
					"handle: %s."),
				scds_error_string(rc));
		return (rc);
	}

	rc = scha_cluster_get(clhandle, SCHA_NODEID_LOCAL, pnodeid);
	if (rc == SCHA_ERR_SEQID) {
		/* Close the handle and retry. */
		scds_syslog(LOG_INFO, "Retrying retrieve of resource "
			"information: %s.", scds_error_string(rc));
		(void) scha_cluster_close(clhandle);
		clhandle = NULL;
		(void) sleep(1);
		goto retry;
	} else if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to retrieve the cluster "
			"property %s: %s.", SCHA_NODEID_LOCAL,
			scds_error_string(rc));
		if (print_errors)
			(void) fprintf(stderr,
				gettext("Failed to retrieve the cluster "
					"property %s: %s."),
				SCHA_NODEID_LOCAL,
				scds_error_string(rc));
		goto close;
	}

	scds_syslog_debug(DBG_LEVEL_HIGH, "Nodeid for local node is %d.",
		*pnodeid);

close:
	(void) scha_cluster_close(clhandle);

	return (rc);

	/* Suppress handle not used. */
} /*lint !e715 */

/*
 * Map a RG state to a string so we can print pretty messages.
 *
 * The returned string is static so don't free(3) it!
 *
 */

const char *
rgstate_to_string(scha_rgstate_t state)
{
	uint_t i;

	for (i = 0; i < RG_ENTRIES; i++) {
		if (rgstatenames[i].state == state)
			return (rgstatenames[i].statename);
	}

	ds_internal_error("Unknown resource group state: %d", state);

	return (STATE_NOT_FOUND);
}

/*
 * Map a RS state to a string so we can print pretty messages.
 *
 * The returned string is static so don't free(3) it!
 */

const char *
rsstate_to_string(scha_rsstate_t state)
{
	uint_t i;

	for (i = 0; i < RS_ENTRIES; i++) {
		if (rsstatenames[i].state == state)
			return (rsstatenames[i].statename);
	}

	ds_internal_error("Unknown resource state: %d", state);

	return (STATE_NOT_FOUND);
}

/*
 * Copy a uint_array_t from src to dst.
 *
 * Returns SCHA_ERR_NOERR if successful, otherwise return
 * SCHA_ERR_NOMEM.
 */

scha_err_t
copy_uint_array(scha_uint_array_t **dst, scha_uint_array_t *src)
{
	uint_t	i;

	*dst = malloc(sizeof (scha_uint_array_t));
	if (*dst == NULL) {
		return (SCHA_ERR_NOMEM);
	}

	(*dst)->uint_array = malloc(sizeof (uint_t) * src->array_cnt);
	if ((*dst)->uint_array == NULL) {
		free(*dst);
		return (SCHA_ERR_NOMEM);
	}

	(*dst)->array_cnt = src->array_cnt;
	for (i = 0; i < src->array_cnt; i++)
		(*dst)->uint_array[i] = src->uint_array[i];

	return (SCHA_ERR_NOERR);
}

/*
 * file_exists()
 *
 * Check if a file exists quietly.  Use instead of ds_validate_file()
 * if it's OK that the file does not exist, ds_validate_file() would
 * log error messages even if it's not an error for the file not to
 * exist.
 *
 * Returns:
 *	SCHA_ERR_NOERR		file exists
 *	SCHA_ERR_NOMEM		no memory to do checks
 *	SCHA_ERR_ACCESS		file doesn't exist
 *
 * errno is left intact so it can be checked for more info if the file
 * is unable to be accessed.
 *
 * file_format is treated as a printf format string so file and any
 * additional arguments are used to build up the file name.  For
 * example to check to see if Confdir_list[0]/bin/startserv exists
 *
 * rc = file_exists("%s/bin/startserv", confdirs->str_array[0]);
 */

scha_err_t
file_exists(const char *file_format, ...)
{
	char		*file;
	va_list		ap;
	struct statvfs	vfsbuf;

	/* Suppress Undeclared identifier (__builtin_va_alist) */
	va_start(ap, file_format);   /*lint !e40 */
	file = ds_string_format_v(file_format, ap);
	va_end(ap);

	if (file == NULL) {
		/* message already logged */
		return (SCHA_ERR_NOMEM);
	}

	if (statvfs(file, &vfsbuf) != 0) { /* file does not exist */
		free(file);
		return (SCHA_ERR_ACCESS);
	}

	free(file);
	return (SCHA_ERR_NOERR);
}
