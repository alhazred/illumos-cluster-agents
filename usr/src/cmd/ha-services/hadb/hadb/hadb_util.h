/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2003 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_HADB_UTIL_H
#define	_HADB_UTIL_H

#pragma ident	"@(#)hadb_util.h	1.5	07/06/06 SMI"

#ifdef __cplusplus
extern "C" {
#endif

scha_err_t get_string_ext_property(scds_handle_t scds_handle,
	char *prop_name, char **stringval);
scha_err_t get_boolean_ext_property(scds_handle_t scds_handle,
	char *prop_name, boolean_t *boolval);

scha_err_t get_rg_state_node(scds_handle_t handle, char *node,
	scha_rgstate_t *pstate);

const char *rgstate_to_string(scha_rgstate_t state);

scha_err_t get_rs_state_node(scds_handle_t handle, char *node,
	scha_rsstate_t *pstate);

const char *rsstate_to_string(scha_rsstate_t state);

scha_err_t get_all_nodeids(scds_handle_t handle,
	scha_uint_array_t **pall_nodeids);

scha_err_t get_nodename_from_id(scds_handle_t handle, uint_t nodeid,
	char **nodename);

scha_err_t get_privname_from_nodename(scds_handle_t handle, char *nodename,
	char **privname);

scha_err_t get_local_nodeid(scds_handle_t handle, uint_t *pnodeid);

void set_print_errors(boolean_t print);

scha_err_t file_exists(const char *file_format, ...);

#ifdef __cplusplus
}
#endif

#endif /* _HADB_UTIL_H */
