/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2003 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * hadb_monitor_stop.c - Monitor Stop method for hadb
 */

#pragma ident	"@(#)hadb_monitor_stop.c	1.4	07/06/06 SMI"

#include <rgm/libdsdev.h>
#include "hadb.h"

/*
 * This method stops the fault monitor for a hadb resource.
 * This is done via PMF. The PMF tag for the fault monitor is
 * constructed based on RG-name_RS-name.mon.
 */

int
main(int argc, char *argv[])
{

	scds_handle_t   scds_handle;
	int 	rc;

	/* Process arguments passed by RGM and initialize syslog */
	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR) {
		return (1);
	}
	rc = mon_stop(scds_handle);

	/* Free up all the memory allocated by scds_initialize */
	scds_close(&scds_handle);

	/* Return the result of monitor stop method */
	return (rc);
}
