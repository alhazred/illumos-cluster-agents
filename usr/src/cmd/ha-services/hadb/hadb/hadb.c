/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * hadb.c - Common utilities for hadb
 */

#pragma ident	"@(#)hadb.c	1.12	07/06/06 SMI"

/*
 * This utility has the methods for performing the validation,
 * starting and stopping the data service and the fault monitor. It
 * also contains the method to probe the health of the data service.
 * The probe just returns either success or failure. Action is taken
 * based on this returned value in the method found in the file
 * hadb_probe.c
 */

#include <ctype.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <libgen.h>
#include <libintl.h>
#include <sys/stat.h>
#include <sys/utsname.h>
#include <sys/wait.h>
#include <scha.h>
#include <rgm/libdsdev.h>
#include "hadb.h"
#include "../../common/ds_common.h"
#include "hadb_util.h"

/*
 * The percentage of the stop timeout to wait to see if the database
 * is stopped.  If the database is not stopped after this amount of
 * time a new SC node to run the stop command will be selected.
 */

#define	SVC_STOPWAIT_PCT	10

/*
 * The maximum length of a line in the output of the hadbm status
 * command.
 */

#define	STATUS_LINE_LEN		1024

/*
 * The global HADB and resource configuration.
 */

boolean_t site_info_initialized = B_FALSE;

struct sc_node_info sc_node[MAX_SC_NODES+1];
struct site_info site_info;

scha_err_t update_rs_state_info(scds_handle_t scds_handle);

scha_err_t parse_node_status_line(char *line, int *node, char **hostname,
	int *port, char **noderole, char **nodestate, int *mirror);
scha_err_t add_hadb_node_to_config(int node, char *hostname, char *nodestate,
	int mirror, boolean_t print_messages);

void print_hadb_config_info(void);

scha_err_t stop_database(scds_handle_t scds_handle, int nodeid);
scha_err_t stop_local_nodes(scds_handle_t scds_handle);
scha_err_t start_local_nodes(scds_handle_t scds_handle);

scha_err_t run_hadb_stop_command(scds_handle_t scds_handle);
scha_err_t run_hadb_start_command(scds_handle_t scds_handle);
scha_err_t run_hadb_startnode_command(scds_handle_t scds_handle, int node);
scha_err_t run_hadb_stopnode_command(scds_handle_t scds_handle, int node);

int pick_stop_node(int nodeid);
boolean_t node_still_stopping(scds_handle_t scds_handle, int i);

int get_sc_node_by_hadb_node(int hadbnode);

scha_err_t check_stopstate(void);
scha_err_t split_stopstate_line(char *line, int *phys, int *logical,
	char **role, char **state);

/*
 * svc_validate():
 *
 * Do hadb specific validation of the resource configuration.
 *
 * Note that we don't do any HAStoragePlus validation since the
 * typical config will have the binaries on local filesystems, and the
 * data and configuration is replicated to local filesystems by HADB
 * itself.  We do check for binaries and configuration files on all
 * nodes so if they are unavailable on a node for any reason the
 * validate will fail.
 *
 */

int
svc_validate(scds_handle_t scds_handle, boolean_t print_messages)
{
	int	err;
	int	rc = 0;

	scha_str_array_t	*confdirs = NULL;
	const scha_str_array_t	*nodelist = NULL;
	char			*db_name = NULL;
	char			*hadb_root = NULL;
	boolean_t		auto_recover;
	char			*auto_recover_command;
	char			*dbpasswordfile;
	int			desired_primaries;
	int			maximum_primaries;
	char			*copy, *bname;
	struct stat statbuf;

	/*
	 * Control wether the utility routines will print errors to
	 * stderr for us.
	 */

	set_print_errors(print_messages);

	/*
	 * Get the nodelist, desired number of primaries and the
	 * maximum number of primaries.
	 *
	 * Check that number of nodes in nodelist, desired and maximum
	 * primaries all equal the same number, greater then two and
	 * an even number.
	 */

	nodelist = scds_get_rg_nodelist(scds_handle);

	/* Return an error if there is no nodelist prop */
	if (nodelist == NULL || nodelist->array_cnt == 0) {
		scds_syslog(LOG_ERR, "Property %s is not set.",
			"Nodelist");
		if (print_messages) {
			(void) fprintf(stderr, gettext("Property %s is not "
						"set.\n"), "Nodelist");
		}
		rc = 1;
		goto finished;
	}

	if (nodelist->array_cnt < 2) {
		/*
		 * SCMSGS
		 * @explanation
		 * The nodelist resource group property must contain two or
		 * more Sun Cluster nodes.
		 * @user_action
		 * Recreate the resource group with a nodelist that contains
		 * two or more Sun Cluster nodes.
		 */
		scds_syslog(LOG_ERR,
			"Nodelist must contain two or more nodes.");
		if (print_messages) {
			(void) fprintf(stderr, gettext("Nodelist must "
					"contain two or more nodes.\n"));
		}
		rc = 1;
		goto finished;
	}

	if (nodelist->array_cnt % 2 != 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * The HADB resource must be configured to run on an even
		 * number of Sun Cluster nodes.
		 * @user_action
		 * Recreate the resource group and specify an even number of
		 * Sun Cluster nodes in the nodelist.
		 */
		scds_syslog(LOG_ERR,
			"Nodelist must contain an even number of nodes.");
		if (print_messages) {
			(void) fprintf(stderr, gettext("Nodelist must "
					"contain an even number of "
					"nodes.\n"));
		}
		rc = 1;
		goto finished;
	}

	desired_primaries = scds_get_rg_desired_primaries(scds_handle);
	maximum_primaries = scds_get_rg_maximum_primaries(scds_handle);

	if (desired_primaries != maximum_primaries) {
		/*
		 * SCMSGS
		 * @explanation
		 * The resource group properties desired_primaries and
		 * maximum_primaries must be equal.
		 * @user_action
		 * Set the desired and maximum primaries to be equal.
		 */
		scds_syslog(LOG_ERR, "Desired_primaries must equal "
			"Maximum_primaries.");
		if (print_messages) {
			(void) fprintf(stderr, gettext("Desired_primaries "
						"must equal "
						"Maximum_primaries.\n"));
		}
		rc = 1;
		goto finished;
	}

	if ((int)nodelist->array_cnt != desired_primaries) {
		/*
		 * SCMSGS
		 * @explanation
		 * The resource group properties desired_primaries and
		 * maximum_primaries must be equal and they must equal the
		 * number of Sun Cluster nodes in the nodelist of the resource
		 * group.
		 * @user_action
		 * Set the desired and maximum primaries to be equal and to
		 * the number of Sun Cluster nodes in the nodelist.
		 */
		scds_syslog(LOG_ERR, "Desired_primaries must equal the "
			"number of nodes in Nodelist.");
		if (print_messages) {
			(void) fprintf(stderr, "Desired_primaries must "
				"equal the number of nodes in Nodelist.\n");
		}
		rc = 1;
		goto finished;
	}

	/* Get Confdir_list extension property */
	confdirs = scds_get_ext_confdir_list(scds_handle);

	/* Return an error if there is no config_dirs x prop */
	if (confdirs == NULL || confdirs->array_cnt == 0) {
		scds_syslog(LOG_ERR, "Property %s is not set.",
			"Confdir_list");
		if (print_messages) {
			(void) fprintf(stderr, gettext("Property %s is not "
						"set.\n"), "Confdir_list");
		}
		return (1);
	}

	if (confdirs->array_cnt > 1) {
		/*
		 * SCMSGS
		 * @explanation
		 * One and only value may be specified in the specified
		 * extension property.
		 * @user_action
		 * Specify only one value for the specified extension
		 * property.
		 */
		scds_syslog(LOG_ERR,
			"%s data services must have exactly "
			"one value for extension property %s.",
			APP_NAME, "Confdir_list");

		if (print_messages) {
			(void) fprintf(stderr, gettext("%s data "
					"services must have exactly "
					"one value for extension "
					"property %s.\n"),
				APP_NAME, "Confdir_list");
		}
		return (1);
	}

	/* Check that Confdir_list[0] is an absolute path. */
	if (confdirs->str_array[0][0] != '/') {
		/*
		 * SCMSGS
		 * @explanation
		 * The extension property listed is not an absolute path.
		 * @user_action
		 * Make sure the path starts with "/".
		 */
		scds_syslog(LOG_ERR,
			"%s is not an absolute path.", "Confdir_list");
		if (print_messages) {
			(void) fprintf(stderr, gettext("%s is not "
					"an absolute path.\n"),
				"Confdir_list");
		}
		return (1);
	}

	/*
	 * Check that the files cfg and def exist and are readable.
	 */

	err = ds_validate_file(print_messages, S_IRUSR,	"%s/cfg",
		confdirs->str_array[0]);
	if (err != SCHA_ERR_NOERR) {
		rc = 1;
		goto finished;
	}

	err = ds_validate_file(print_messages, S_IRUSR,	"%s/def",
		confdirs->str_array[0]);
	if (err != SCHA_ERR_NOERR) {
		rc = 1;
		goto finished;
	}

	/*
	 * Get and check the HADB_ROOT extension property.
	 */

	err = get_string_ext_property(scds_handle, "HADB_ROOT", &hadb_root);
	if (err != SCHA_ERR_NOERR) {
		/* Message already logged and printed. */
		rc = 1;
		goto finished;
	}

	if (strcmp(hadb_root, "") == 0) {
		scds_syslog(LOG_ERR, "Property %s is not set.",
			"HADB_ROOT");
		if (print_messages) {
			(void) fprintf(stderr, gettext("Property %s is not "
						"set.\n"), "HADB_ROOT");
		}
		rc = 1;
		goto finished;
	}

	/* Check that HADB_ROOT is an absolute path. */
	if (hadb_root[0] != '/') {
		scds_syslog(LOG_ERR,
			"%s is not an absolute path.", "HADB_ROOT");
		if (print_messages) {
			(void) fprintf(stderr, gettext("%s is not "
					"an absolute path.\n"),
				"HADB_ROOT");
		}
		return (1);
	}

	err = ds_validate_file(print_messages, S_IXUSR,
		"%s/lib/server/clu_nsup_srv", hadb_root);
	if (err != SCHA_ERR_NOERR) {
		rc = 1;
		goto finished;
	}

	err = ds_validate_file(print_messages, S_IXUSR,	"%s/bin/hadbm",
		hadb_root);
	if (err != SCHA_ERR_NOERR) {
		rc = 1;
		goto finished;
	}

	/*
	 * Get and check the DB_Name extension property.
	 */

	err = get_string_ext_property(scds_handle, "DB_Name", &db_name);
	if (err != SCHA_ERR_NOERR) {
		/* Message already logged and printed. */
		rc = 1;
		goto finished;
	}

	if (strcmp(db_name, "") == 0) {
		scds_syslog(LOG_ERR, "Property %s is not set.",
			"db_name");
		if (print_messages) {
			(void) fprintf(stderr, gettext("Property %s is not "
						"set.\n"), "db_name");
		}
		rc = 1;
		goto finished;
	}

	/*
	 * The end of the configuration directory should match the
	 * database name.  For example:
	 *
	 * Confdir = /etc/opt/SUNWhadb/dbdef/db1
	 * DB_name = db1
	 */

	/*
	 * First make a copy of confdir_list[0] because basename()
	 * will modify it.
	 */

	copy = strdup(confdirs->str_array[0]);
	if (copy == NULL)
		return (SCHA_ERR_NOMEM);

	bname = basename(copy);

	if (strcasecmp(db_name, bname) != 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * The db_name extenstion property does not match the database
		 * name in the confdir_list path.
		 * @user_action
		 * Check to see if the db_name and confdir_list extension
		 * properties are correct.
		 */
		scds_syslog(LOG_ERR, "DB_name, %s, does not match last "
			"component of Confdir_list, %s.", db_name,
			confdirs->str_array[0]);
		if (print_messages)
			(void) fprintf(stderr,
				gettext("DB_name, %s, does not match "
					"last component of Confdir_list, "
					"%s.\n"),
				db_name, confdirs->str_array[0]);
	}

	/*
	 * Check to see if we can get Auto_recovery to true.
	 */

	err = get_boolean_ext_property(scds_handle, "Auto_recovery",
		&auto_recover);
	if (err != SCHA_ERR_NOERR) {
		/* Message already logged and printed. */
		rc = 1;
		goto finished;
	}

	/*
	 * If Auto_recovery is true we need to validate
	 * Auto_recovery_command and DB_password_file.
	 */

	if (auto_recover) {
		err = get_string_ext_property(scds_handle,
			"Auto_recovery_command", &auto_recover_command);
		if (err != SCHA_ERR_NOERR) {
			/* Message already logged and printed */
			rc = 1;
			goto finished;
		}

		/*
		 * Allow the auto recover command to be empty.
		 * However if it set check to see that it is
		 * an absolute path and that it is executable.
		 */
		if (auto_recover_command[0] != '\0') {
			if (auto_recover_command[0] != '/') {
				scds_syslog(LOG_ERR,
					"%s is not an absolute path.",
					"Auto_recovery_command");
				if (print_messages) {
					(void) fprintf(stderr,
						gettext("%s is not an "
							"absolute path.\n"),
						"Auto_recovery_command");
				}
				rc = 1;
				goto finished;
			}

			err = ds_validate_file(print_messages, S_IXUSR,
				auto_recover_command);
			if (err != SCHA_ERR_NOERR) {
				rc = 1;
				goto finished;
			}

			/*
			 * The file is already stat'd in ds_validate_file. This
			 * is used by DS common API. Its risky to change the
			 * func. This is redundant but, better to enforce via
			 * another stat().
			 */
			if (stat(auto_recover_command, &statbuf) != 0) {
				/* Lint info: ___errno(): no prototype */
				scds_syslog(LOG_ERR,
					"Cannot access file: %s (%s)",
					auto_recover_command,
					strerror(errno)); /*lint !e746 */
				if (print_messages) {
				    (void) fprintf(stderr,
					gettext("Cannot access file: %s (%s)"
						" \n"), auto_recover_command,
						strerror(errno));
				}
				rc = 1;
				goto finished;
			}

			/* Check if its a regular file */
			if (!S_ISREG(statbuf.st_mode)) {
				/*
				 * SCMSGS
				 * @explanation
				 * The Auto_recovery_command extension
				 * property needs to be a regular file.
				 * @user_action
				 * Give appropriate file name to the extension
				 * property. Make sure that the path name does
				 * not point to a directory or any other file
				 * type. Moreover, the Auto_recovery_command
				 * could be a link to a file also.
				 */
				scds_syslog(LOG_ERR,
					"Auto_recovery_command should be a"
					" regular file: %s",
					auto_recover_command);
				if (print_messages) {
					(void) fprintf(stderr,
						gettext("Auto_recovery_command"
						" should be a regular file: "
						"%s\n"), auto_recover_command);
				}
				rc = 1;
				goto finished;
			}
		}

		/*
		 * If Auto_recovery is true we must have the
		 * dbpasswordfile and it must be an absolute path and
		 * readable.
		 */
		err = get_string_ext_property(scds_handle,
			"DB_password_file", &dbpasswordfile);
		if (err != SCHA_ERR_NOERR) {
			/* Message already logged and printed */
			rc = 1;
			goto finished;
		}

		if (dbpasswordfile[0] == '\0') {
			/*
			 * SCMSGS
			 * @explanation
			 * When auto_recovery is set to true the
			 * db_password_file extension property must be set to
			 * a file that can be passed as the value to the hadbm
			 * command's --dbpasswordfile argument.
			 * @user_action
			 * Retry the resource creation and provide a value for
			 * the db_password_file extension property.
			 */
			scds_syslog(LOG_ERR, "DB_password_file must be set "
				"when Auto_recovery is set to true.");
			rc = 1;
			goto finished;
		}

		if (dbpasswordfile[0] != '/') {
			scds_syslog(LOG_ERR,
				"%s is not an absolute path.",
				"DB_password_file");
			if (print_messages) {
				(void) fprintf(stderr,
					gettext("%s is not an absolute "
						"path.\n"),
					"DB_password_file");
			}
			rc = 1;
			goto finished;
		}

		err = ds_validate_file(print_messages, S_IRUSR,
			dbpasswordfile);
		if (err != SCHA_ERR_NOERR) {
			rc = 1;
			goto finished;
		}

		/*
		 * The file is already stat'd in ds_validate_file. This is
		 * used by DS common API. Its risky to change the func.
		 * This is redundant but, better to enforce via another stat().
		 */
		if (stat(dbpasswordfile, &statbuf) != 0) {
			/* Lint info: ___errno(): no prototype */
			scds_syslog(LOG_ERR,
				"Cannot access file: %s (%s)", dbpasswordfile,
				strerror(errno)); /*lint !e746 */
			if (print_messages) {
				(void) fprintf(stderr,
				    gettext("Cannot access file: %s (%s)\n"),
				    dbpasswordfile, strerror(errno));
			}
			rc = 1;
			goto finished;
		}

		/* Check if its a regular file */
		if (!S_ISREG(statbuf.st_mode)) {
			/*
			 * SCMSGS
			 * @explanation
			 * The DB_password_file extension property needs to be
			 * a regular file.
			 * @user_action
			 * Give appropriate file name to the extension
			 * property. Make sure that the path name does not
			 * point to a directory or any other file type.
			 * Moreover, the DB_password_file could be a link to a
			 * file also.
			 */
			scds_syslog(LOG_ERR,
				"DB_password_file should be a regular file: %s",
				dbpasswordfile);
			if (print_messages) {
				(void) fprintf(stderr,
					gettext("DB_password_file should be a "
					"regular file: %s\n"), dbpasswordfile);
			}
			rc = 1;
			goto finished;
		}
	}

	/*
	 * All the routine sanity checks have succeeded.  We will now
	 * gather the HADB configuration and status info.  And do what
	 * validation we can of that.
	 *
	 * This is heavyweight so we do it only during an actual
	 * VALIDATE method, not when we're running svc_validate() as
	 * part of a START method which we determine by looking at
	 * print_messages which is only true during a VALIDATE method.
	 */

	if (!print_messages)
		goto finished;

	err = get_hadb_config_info(scds_handle, print_messages);
	if (err != SCHA_ERR_NOERR) {
		/* msgs logged and printed */
		rc = 1;
		goto finished;
	}

	/*
	 * The validation we're interested in is that the hostnames
	 * for the HADB nodes match the interconnect hostnames.
	 * get_hadb_config_info() checks for that then logs and print
	 * messages for us.  So we now done.
	 */

	/* All validation checks were successful */

finished:

	/*
	 * We do not free any of the properties allocated because doing
	 * so caused a problem on a previous version of cluster.
	 *
	 * hadb_svc_start is a short lived process and the properties
	 * are not allocated repeatedly so this does not pose a
	 * serious memory leak problem.
	 */

	free(hadb_root);
	free(db_name);

	return (rc);		/* return result of validation */
}

/*
 * svc_start():
 *
 * Start the nodes that are configured for this local system if they
 * are not already running and the database is running.
 *
 * They may be running already because a hadbm start from another node
 * may have started them.
 *
 * get_hadb_config_info() needs to be run before svc_start().
 */

int
svc_start(scds_handle_t scds_handle)
{
	int	rc = 0;

	if (!site_info_initialized) {
		ds_internal_error("Site info has not been initialized.");
		return (SCHA_ERR_INTERNAL);
	}

	print_hadb_config_info();

	/*
	 * Check to see if the database is running.  If it is start
	 * the local HADB nodes so we can join the running HADB site.
	 *
	 * start_local_nodes() will figure out if the HADB nodes
	 * actually need to be started and log the appropriate
	 * messages.
	 */

	if (database_running(scds_handle)) {
		rc = start_local_nodes(scds_handle);
		return (rc);
	} else {
		/*
		 * SCMSGS
		 * @explanation
		 * The HADB database has not been started so the local HADB
		 * nodes can not be started.
		 * @user_action
		 * Online the HADB resource on more Sun Cluster nodes so that
		 * the database can be started.
		 */
		scds_syslog(LOG_NOTICE, "Not ready to start local HADB "
			"nodes.");
		return (SCHA_ERR_NOERR);
	}
}

/*
 * Called from svc_start() if the database is running and local HADB
 * nodes need to be started.  Any local HADB nodes not in the stopped
 * state will be ignored.  The ones in the stopped state will be
 * started.
 */

scha_err_t
start_local_nodes(scds_handle_t scds_handle)
{
	int		i = site_info.scnodeid;
	int		j;
	scha_err_t	rc;

	for (j = 0; j < MAX_HADB_NODES_PER_SYS; j++) {
		if (sc_node[i].hadb_node[j].nodeno < 0)
			break;

		if (strcmp(sc_node[i].hadb_node[j].nodestate, NODE_STOPPED)
			== 0) {
			rc = run_hadb_startnode_command(scds_handle,
				sc_node[i].hadb_node[j].nodeno);
			if (rc != SCHA_ERR_NOERR) {
				/* msg already logged */
				return (rc);
			}
		} else {
			scds_syslog_debug(DBG_LEVEL_MED, "Not starting "
				"HADB node %d which is in state %s",
				sc_node[i].hadb_node[j].nodeno,
				sc_node[i].hadb_node[j].nodestate);
		}
	}

	return (SCHA_ERR_NOERR);
}

/*
 * Run the startnode command for the given node.
 */

scha_err_t
run_hadb_startnode_command(scds_handle_t scds_handle, int node)
{
	char		*cmd;
	scha_err_t	rc;
	int		err;

	/*
	 * We redirect stderr because the hadbm script runs stty which
	 * when stdout is not a tty will print: "stty: : No such
	 * device or address" which ends up on the console twice every
	 * probe cycle.  A bug has been filed against hadbm.
	 */
	cmd = ds_string_format("%s startnode %d %s 2>/dev/null",
		site_info.hadbm_cmd, node, site_info.dbname);
	if (cmd == NULL)
		return (SCHA_ERR_NOMEM);

	scds_syslog_debug(DBG_LEVEL_HIGH, "Running nodestart command: %s.",
		cmd);

	err = system(cmd);
	if (err == -1) {
		/* If system() itself had a failure. */

		/*
		 * Lint: call to ___errno() not made in the presence
		 * of a prototype
		 */
		/*
		 * SCMSGS
		 * @explanation
		 * The resource was unable to successfully run the hadbm start
		 * command either because it was unable to execute the
		 * program, or the hadbm command received a signal.
		 * @user_action
		 * This might be the result of a lack of system resources.
		 * Check whether the system is low in memory and take
		 * appropriate action.
		 */
		scds_syslog(LOG_ERR, "Start of HADB node %d did not "
			"complete: %s.", node,
			strerror(errno)); /*lint !e746 */
		rc = SCHA_ERR_INVAL; /* as good as any other */
		goto finish;

	} else if (WIFSIGNALED((uint_t)err)) {
		/* If we didn't complete due to being signaled... */

		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		scds_syslog(LOG_ERR, "Start of HADB node %d did not "
			"complete.", node);
		rc = SCHA_ERR_INVAL;
		goto finish;

	} else if (WEXITSTATUS((uint_t)err) != 0) {
		/* If hadbm did not succeed. */

		/*
		 * SCMSGS
		 * @explanation
		 * The resource encountered an error trying to start the HADB
		 * node.
		 * @user_action
		 * Examine other syslog messages occurring around the same
		 * time on the same node, to see if the source of the problem
		 * can be identified.
		 */
		scds_syslog(LOG_ERR, "Start of HADB node %d failed with "
			"exit code %d.", node, WEXITSTATUS((uint_t)err));
		rc = SCHA_ERR_INVAL;
		goto finish;

	} else {
		/* Success. */
		/*
		 * SCMSGS
		 * @explanation
		 * The HADB resource successfully started the specified node.
		 * @user_action
		 * This is an informational message, no user action is needed.
		 */
		scds_syslog(LOG_NOTICE, "Start of HADB node %d completed "
			"successfully.", node);

		rc = SCHA_ERR_NOERR;
	}

finish:
	free(cmd);

	return (rc);
	/* Lint: scds_handle not referenced */
} /*lint !e715 */

/*
 * Attempt to start the HADB database.
 *
 * If the database is already running return SCHA_ERR_NOERR.
 *
 * If the stopstate file does not exist on the localnode return
 * SCHA_ERR_STATE.
 *
 * If the database can be started return success SCHA_ERR_NOERR.
 *
 * If the database can not be started because enugh SC nodes are not
 * up return SCHA_ERR_DEPEND.
 *
 * If there is an error starting the database return SCHA_ERR_INVAL.
 *
 * Other errors will return SCHA_ERR_INTERNAL.
 */

int
svc_start_database(scds_handle_t scds_handle)
{
	scha_err_t	rc;

	if (!site_info_initialized) {
		ds_internal_error("Site info has not been initialized.");
		return (SCHA_ERR_INTERNAL);
	}

	/* If the DB is already up just return true. */

	if (database_running(scds_handle)) {
		/*
		 * SCMSGS
		 * @explanation
		 * The database is running and does not need to be started.
		 * @user_action
		 * This is an informational message, no user action is needed.
		 */
		scds_syslog(LOG_INFO, "Database is already running.");
		return (SCHA_ERR_NOERR);
	}

	/*
	 * Database is not up.
	 *
	 * Check to see if we are the owner of the stopstate file.  If
	 * we are the owner check to see if the SC nodes that need to
	 * be online are either online or pending online.
	 *
	 * Check to see if we have the stopstate file on the local
	 * node.  If so attempt to start the database.
	 *
	 * Can't use ds_validate_file because it's not an error if the
	 * stopstate file does not exist, but ds_validate_file will
	 * log an error message at LOG_ERR if the file does not exist.
	 */

	rc = file_exists("%s/stopstate", site_info.confdir);
	if (rc != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * The stopstate file is used by HADB to start the database.
		 * This file will exists on the Sun Cluster node that last
		 * stopped the HADB database. That Sun Cluster node will start
		 * the database, and the others will wait for that node to
		 * start the database.
		 * @user_action
		 * This is an informational message, no user action is needed.
		 */
		scds_syslog(LOG_NOTICE, "stopstate file does not "
			"exist on this node.");
		return (SCHA_ERR_STATE);
	}

	scds_syslog_debug(DBG_LEVEL_HIGH, "stopstate file exists on this "
		"node, continuing with database start.");

	/*
	 * check_stopstate() will examine the stopstate file and
	 * determine if we are ready to start the database based on
	 * the state contained in the stopstate file.
	 */

	rc = check_stopstate();
	if (rc == SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * All the Sun Cluster nodes needed to start the database are
		 * running the resource.
		 * @user_action
		 * This is an informational message, no user action is needed.
		 */
		scds_syslog(LOG_NOTICE, "Needed Sun Cluster nodes are "
			"online, continuing with database start.");
	} else if (rc == SCHA_ERR_DEPEND) {
		/* Specific nodes already logged at notice level. */
		scds_syslog_debug(DBG_LEVEL_HIGH, "Waiting for more nodes "
			"to join.");
		return (SCHA_ERR_DEPEND);
	} else {
		/*
		 * SCMSGS
		 * @explanation
		 * The stopstate file could not be opened and read.
		 * @user_action
		 * Examine other syslog messages occurring around the same
		 * time on the same node, to see if the source of the problem
		 * can be identified.
		 */
		scds_syslog(LOG_ERR, "Error reading stopstate file.");
		return (rc);
	}

	rc = run_hadb_start_command(scds_handle);
	if (rc == SCHA_ERR_NOERR)
		return (SCHA_ERR_NOERR);
	else
		return (SCHA_ERR_INVAL);
}

/*
 * If svc_start_database has determined we are ready to to actually
 * start the database run_hadb_start_command() will handle it.
 */

scha_err_t
run_hadb_start_command(scds_handle_t scds_handle)
{
	scha_err_t	rc;
	int		err;
	char		*cmd;

	/*
	 * We redirect stderr because the hadbm script runs stty which
	 * when stdout is not a tty will print: "stty: : No such
	 * device or address" which ends up on the console twice every
	 * probe cycle.  A bug has been filed against hadbm.
	 */
	cmd = ds_string_format("%s start %s 2>/dev/null",
		site_info.hadbm_cmd, site_info.dbname);
	if (cmd == NULL)
		return (SCHA_ERR_NOMEM);

	scds_syslog_debug(DBG_LEVEL_HIGH, "Running start command %s.", cmd);

	err = system(cmd);
	if (err == -1) {
		/* If system() itself had a failure. */

		/*
		 * SCMSGS
		 * @explanation
		 * The resource was unable to successfully run the hadbm start
		 * command either because it was unable to execute the
		 * program, or the hadbm command received a signal.
		 * @user_action
		 * This might be the result of a lack of system resources.
		 * Check whether the system is low in memory and take
		 * appropriate action.
		 */
		scds_syslog(LOG_ERR, "Start of HADB database did not "
			"complete: %s.", strerror(errno));
		rc = SCHA_ERR_INVAL; /* as good as any other */
		goto finish;

	} else if (WIFSIGNALED((uint_t)err)) {
		/* If we didn't complete due to being signaled... */

		/*
		 * SCMSGS
		 * @explanation
		 * The resource was unable to successfully run the hadbm start
		 * command either because it was unable to execute the
		 * program, or the hadbm command received a signal.
		 * @user_action
		 * This might be the result of a lack of system resources.
		 * Check whether the system is low in memory and take
		 * appropriate action.
		 */
		scds_syslog(LOG_ERR, "Start of HADB database did not "
			"complete.");
		rc = SCHA_ERR_INVAL;
		goto finish;

	} else if (WEXITSTATUS((uint_t)err) != 0) {
		/* If hadbm did not succeed. */

		/*
		 * SCMSGS
		 * @explanation
		 * The resource encountered an error trying to start the HADB
		 * database.
		 * @user_action
		 * Examine other syslog messages occurring around the same
		 * time on the same node, to see if the source of the problem
		 * can be identified.
		 */
		scds_syslog(LOG_ERR, "Start of HADB database failed with "
			"exit code %d.", WEXITSTATUS((uint_t)err));
		rc = SCHA_ERR_INVAL;
		goto finish;

	} else {
		/* Success. */
		/*
		 * SCMSGS
		 * @explanation
		 * The resource was able to successfully start the HADB
		 * database.
		 * @user_action
		 * This is an informational message, no user action is needed.
		 */
		scds_syslog(LOG_NOTICE, "Start of HADB database completed "
			"successfully.");

		rc = SCHA_ERR_NOERR;
	}

finish:
	free(cmd);

	return (rc);
} /*lint !e715 */ /* scds_handle not referenced */

/*
 * svc_stop():
 *
 * Determine if just the local HADB nodes need to be stopped or if the
 * entire database needs to be stopped.  The entire database will need
 * to be stopped if as the result of the current reconfiguration there
 * will not be enough SC nodes running the resource to support the
 * database.
 *
 * Note that we don't attempt last ditch efforts of shutting down HADB
 * processes by sending a TERM or KILL signal.  We can't even if we
 * wanted to since the processes are not running under PMF, and the
 * command name and arguments are longer then the 80 (PRARGSZ)
 * characters ps will print, and the information to identify to which
 * DB the process belongs to is at the end of the command line, beyond
 * 80 characters.
 *
 * NB: get_hadb_config_info() needs to be run before svc_stop().
 *
 * Return 0 on success, > 0 on failures.
 */

int
svc_stop(scds_handle_t scds_handle)
{
	int	rc = 0;
	/* int	stop_smooth_timeout; */

	scds_syslog(LOG_NOTICE, "Stopping %s.", APP_NAME);

	if (!site_info_initialized) {
		ds_internal_error("Site info has not been initialized.");
		return (SCHA_ERR_INTERNAL);
	}

	/*
	 * Check to see if we are being disabled.  If so shutdown the
	 * database.
	 *
	 * If more then one of the SC nodes will be offline we
	 * shutdown the database.  It might be possible to instead
	 * make sure that after shutting down SC nodes that we would
	 * still have a complete DRU online, but that could be tricky.
	 *
	 * Speaking with some of the HADB engineers they agree that
	 * this is the best alternative for the time being and that
	 * the future managment framework may allow us to be more
	 * sophisticated.
	 */

	if (site_info.disabled ||
	    site_info.n_rg_nodes - site_info.n_rg_online > 1) {

		if (site_info.disabled)
			/*
			 * SCMSGS
			 * @explanation
			 * The HADB database will be stopped because the
			 * resource is being disabled.
			 * @user_action
			 * This is an informational message, no user action is
			 * needed.
			 */
			scds_syslog(LOG_NOTICE, "Resource is disabled, "
				"stopping database.");
		else
			/*
			 * SCMSGS
			 * @explanation
			 * When the resource is stopped on a Sun Cluster node
			 * and the resource will be offline on more then one
			 * node the entire database will be stopped.
			 * @user_action
			 * This is an informational message, no user action is
			 * needed.
			 */
			scds_syslog(LOG_NOTICE, "More than one node will be "
				"offline, stopping database.");


		rc = update_rs_state_info(scds_handle);
		if (rc != SCHA_ERR_NOERR) {
			/* error already logged */
			return (rc);
		}

		rc = stop_database(scds_handle, 1);
		if (rc != SCHA_ERR_NOERR) {
			/*
			 * SCMSGS
			 * @explanation
			 * The HADB agent encountered an error trying to stop
			 * the database.
			 * @user_action
			 * Examine other syslog messages occurring around the
			 * same time on the same node, to see if the source of
			 * the problem can be identified.
			 */
			scds_syslog(LOG_ERR, "Unable to stop database.");
			return (rc);
		}

		/*
		 * SCMSGS
		 * @explanation
		 * The resource was able to successfully shutdown the the HADB
		 * database.
		 * @user_action
		 * This is an informational message, no user action is needed.
		 */
		scds_syslog(LOG_NOTICE, "Successfully stopped the database.");

	} else {
		/*
		 * SCMSGS
		 * @explanation
		 * The resource will be offline on only one node so the
		 * database can continue to run.
		 * @user_action
		 * This is an informational message, no user action is needed.
		 */
		scds_syslog(LOG_NOTICE, "Only one Sun Cluster node will be "
			"offline, stopping node.");

		rc = stop_local_nodes(scds_handle);
		if (rc != SCHA_ERR_NOERR) {
			/*
			 * SCMSGS
			 * @explanation
			 * The resource was unable to successfully run the
			 * hadbm stopnode command either because it was unable
			 * to execute the program, or the hadbm command
			 * received a signal.
			 * @user_action
			 * This might be the result of a lack of system
			 * resources. Check whether the system is low in
			 * memory and take appropriate action.
			 */
			scds_syslog(LOG_ERR, "Unable to stop HADB nodes on "
				"%s", site_info.sysname);
			return (rc);
		}

		/*
		 * SCMSGS
		 * @explanation
		 * The resource was able to successfully stop the HADB nodes
		 * running on the local Sun Cluster node.
		 * @user_action
		 * This is an informational message, no user action is needed.
		 */
		scds_syslog(LOG_NOTICE, "Successfully stopped the local HADB "
			"nodes.");
	}

	return (SCHA_ERR_NOERR); /* Successfully stopped */
}

/*
 * Stop the database.  Routine will pick a node that will run the stop
 * command.  The nodeid provided is the lowest numbered node that will
 * be considered for stopping the database.
 *
 * If the disabled flag is set that the RG is not going offline, but
 * instead the resource is being disabled.
 */

scha_err_t
stop_database(scds_handle_t scds_handle, int nodeid)
{
	int		i;
	int		rc;
	int		stop_timeout;
	unsigned int	stop_wait;
	boolean_t	do_stop = B_FALSE;

	/*
	 * If the database is already stopped simply return success.
	 */

	if (strcmp(site_info.status, DB_STOPPED) == 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * The database does not need to be stopped.
		 * @user_action
		 * This is an informational message, no user action is needed.
		 */
		scds_syslog(LOG_INFO, "Database is already stopped.");
		return (SCHA_ERR_NOERR);
	}

	/*
	 * Find the SC node that will run the database stop command.
	 */

	i = pick_stop_node(nodeid);

	/* Check to see if we have a valid node that we picked */
	if (i < 0 || i > MAX_SC_NODES) {
		ds_internal_error("Unable to find a node pending offline "
			"to run hadbm stop.");
		return (SCHA_ERR_INVAL);
	}
	if (sc_node[i].sysname == NULL) {
		ds_internal_error("Nodeid %d is not part of the cluster.", i);
		return (SCHA_ERR_INTERNAL);
	}
	if (sc_node[i].hadb == B_FALSE) {
		ds_internal_error("Node %s (%d) is not in the resource "
			"groups nodelist.", sc_node[i].sysname, i);
		return (SCHA_ERR_INTERNAL);
	}

	/*
	 * OK, it appears we have a valid node selected to run the
	 * stop command.
	 */

	if (i == site_info.scnodeid) {
		/*
		 * We are first node in nodelist that
		 * is pending offline.
		 *
		 * We are reponsible for running the
		 * database stop command.
		 */
		do_stop = B_TRUE;
	} else {
		/*
		 * Some other node will run the stop
		 * command.
		 */
		do_stop = B_FALSE;
	}

	if (do_stop) {
		/*
		 * SCMSGS
		 * @explanation
		 * The HADB database is being stopped by the hadbm command.
		 * @user_action
		 * This is an informational message, no user action is needed.
		 */
		scds_syslog(LOG_NOTICE, "Running hadbm stop.");

		rc = run_hadb_stop_command(scds_handle);

		return (rc);
	}

	/*
	 * OK, we are not the node that will be shutting down the
	 * database.
	 *
	 * We will try to recover from a failure by the stopping node.
	 * Wait some percentage of stop timeout.  If the database is
	 * still running but the node we selected to stop the database
	 * is offline after sleeping we'll try again.
	 */

	do {
		/*
		 * SCMSGS
		 * @explanation
		 * When the database is being stopped only one node can run
		 * the stop command. The other nodes will just wait for the
		 * database to finish stopping.
		 * @user_action
		 * This is an informational message, no user action is needed.
		 */
		scds_syslog(LOG_NOTICE, "Waiting for %s to "
			"run stop command.",
			sc_node[i].sysname);

		stop_timeout = scds_get_rs_stop_timeout(scds_handle);
		stop_wait = (SVC_STOPWAIT_PCT * (unsigned int) stop_timeout)
			/ 100;

		(void) sleep(stop_wait);

		/*
		 * Check to see if the SC node we picked, i, to stop
		 * the database is still busy stopping the database.
		 */

		if (node_still_stopping(scds_handle, i)) {
			/*
			 * SC Node i is still working on stopping the
			 * database, skip the rest of the loop and go
			 * back to the top to sleep a little while
			 * longer.
			 */
			continue;
		}

		/*
		 * Node i is no longer still stopping, get the
		 * database status, if it is not stopped node i
		 * failed, pick a new node to try again.
		 */

		rc = update_hadb_config_info(scds_handle, B_FALSE);
		if (rc != SCHA_ERR_NOERR)
			return (rc);

		if (database_running(scds_handle)) {
			/*
			 * SCMSGS
			 * @explanation
			 * The database is still running but the resource on
			 * the Sun Cluster node that was selected to run the
			 * hadbm stop command is now offline, possibly because
			 * of an error while running the hadbm command. A
			 * different Sun Cluster node will be selected to run
			 * the hadbm stop command again.
			 * @user_action
			 * This is an informational message, no user action is
			 * needed.
			 */
			scds_syslog(LOG_ERR, "Database is still running "
				"after %d seconds.  Selecting new Sun "
				"Cluster node to run the stop command.",
				stop_wait);

			/*
			 * Just recurse, making sure that at least the
			 * next highest nodeid is selected.
			 */

			return (stop_database(scds_handle, i+1));
		}

		/*
		 * Database is not running: return success.
		 */

		return (SCHA_ERR_NOERR);
	} while (1);
}

boolean_t
node_still_stopping(scds_handle_t scds_handle, int i)
{
	scha_err_t	rc;

	/*
	 * Get the updated situation.
	 *
	 * If the RG is being offlined:
	 *
	 *   Get the state of node i, if the SC node that was picked
	 *   to stop the database (i) is still pending offline assume
	 *   that it is still working on stopping the database.
	 *
	 *   The reason to only get the RG state of the "stopper" node
	 *   is that if that stopper node crashed RGM will try to
	 *   start the resource on another node and the RG state for
	 *   the node being failed over to will have it's RG state set
	 *   to starting even though it is still running the STOP
	 *   method and is a candidate for for running the hadbm stop
	 *   command.
	 *
	 * If the resource is being disabled:
	 *
	 *   Get the resource state on node i, if it still in a non
	 *   offline state assume that it is still working on stopping
	 *   the database.
	 */

	if (site_info.disabled) {
		rc = get_rs_state_node(scds_handle,
			sc_node[i].sysname, &(sc_node[i].rsstate));
		if (rc != SCHA_ERR_NOERR) {
			/*
			 * If we encounter an error getting the
			 * resource state we return that the node is
			 * still working and hope that when we check
			 * again we'll be able to get the resource
			 * state.
			 */
			return (B_TRUE);
		}

		if (sc_node[i].rsstate == SCHA_RSSTATE_ONLINE ||
		    sc_node[i].rsstate ==
			SCHA_RSSTATE_MONITOR_FAILED ||
		    sc_node[i].rsstate ==
			SCHA_RSSTATE_ONLINE_NOT_MONITORED ||
		    sc_node[i].rsstate ==
			SCHA_RSSTATE_STOPPING)
			return (B_TRUE);
		else
			return (B_FALSE);
	} else {
		rc = get_rg_state_node(scds_handle,
			sc_node[i].sysname, &(sc_node[i].rgstate));
		if (rc != SCHA_ERR_NOERR) {
			/*
			 * If we encounter an error getting the RG
			 * state we return that the node is still
			 * working and hope that when we check again
			 * we'll be able to get the RG state.
			 */
			return (B_TRUE);
		}

		if (sc_node[i].rgstate == SCHA_RGSTATE_PENDING_OFFLINE)
			return (B_TRUE);
		else
			return (B_FALSE);
	}
}

/*
 * Pick a SC node to do the stop.
 *
 * This is arbitrary so we pick the lowest SC nodeid that is part of
 * the RG's nodelist that is pending offline, or in the case were the
 * resource is being disabled we pick the lowest node in the nodelist
 * that is not offline.
 */

int
pick_stop_node(int nodeid)
{
	int i;

	for (i = nodeid; i <= MAX_SC_NODES; i++) {

		/* Skip nodes not part of this RG or cluster */
		if (sc_node[i].sysname == NULL ||
			sc_node[i].hadb == B_FALSE) {
			continue;
		}

		if (site_info.disabled) {
			/*
			 * When the resource is being disabled all the
			 * nodes running the resource will run the
			 * stop method so we need to find a node that
			 * is not allready offline.
			 */

			if (sc_node[i].rsstate == SCHA_RSSTATE_ONLINE ||
			    sc_node[i].rsstate ==
				SCHA_RSSTATE_MONITOR_FAILED ||
			    sc_node[i].rsstate ==
				SCHA_RSSTATE_ONLINE_NOT_MONITORED ||
			    sc_node[i].rsstate ==
				SCHA_RSSTATE_STOPPING)
				return (i);
		} else {
			if (sc_node[i].rgstate ==
				SCHA_RGSTATE_PENDING_OFFLINE)
				return (i);
		}

	}

	/*
	 * We didn't find a node that can stop database.
	 *
	 * This is an internal error because at the very least, this
	 * node running should have been able to run the hadbm stop
	 * command.
	 */

	return (-1);
}

/*
 * Called by stop_database() to run the actual hadbm stop command to
 * bringdown the database.
 */

scha_err_t
run_hadb_stop_command(scds_handle_t scds_handle)
{
	scha_err_t	rc;
	int		err;
	char		*cmd;

	/*
	 * --yes prevents hadbm from asking for confirmation to stop
	 * the database.
	 */
	cmd = ds_string_format("%s stop --yes %s 2>/dev/null",
		site_info.hadbm_cmd, site_info.dbname);
	if (cmd == NULL)
		return (SCHA_ERR_NOMEM);

	scds_syslog_debug(DBG_LEVEL_HIGH, "Running stop command %s.", cmd);

	err = system(cmd);
	if (err == -1) {
		/* If system() itself had a failure. */

		/*
		 * SCMSGS
		 * @explanation
		 * The resource was unable to successfully run the hadbm stop
		 * command either because it was unable to execute the
		 * program, or the hadbm command received a signal.
		 * @user_action
		 * This might be the result of a lack of system resources.
		 * Check whether the system is low in memory and take
		 * appropriate action.
		 */
		scds_syslog(LOG_ERR, "Stop of HADB database did not "
			"complete: %s.", strerror(errno));
		rc = SCHA_ERR_INVAL; /* as good as any other */
		goto finish;

	} else if (WIFSIGNALED((uint_t)err)) {
		/* If we didn't complete due to being signaled... */

		/*
		 * SCMSGS
		 * @explanation
		 * The resource was unable to successfully run the hadbm stop
		 * command either because it was unable to execute the
		 * program, or the hadbm command received a signal.
		 * @user_action
		 * This might be the result of a lack of system resources.
		 * Check whether the system is low in memory and take
		 * appropriate action.
		 */
		scds_syslog(LOG_ERR, "Stop of HADB database did not "
			"complete.");
		rc = SCHA_ERR_INVAL;
		goto finish;

	} else if (WEXITSTATUS((uint_t)err) != 0) {
		/* If hadbm did not succeed. */

		/*
		 * SCMSGS
		 * @explanation
		 * The resource encountered an error trying to stop the HADB
		 * database.
		 * @user_action
		 * Examine other syslog messages occurring around the same
		 * time on the same node, to see if the source of the problem
		 * can be identified.
		 */
		scds_syslog(LOG_ERR, "Stop of HADB database failed with "
			"exit code %d.", WEXITSTATUS((uint_t)err));
		rc = SCHA_ERR_INVAL;
		goto finish;

	} else {
		/* Success. */
		/*
		 * SCMSGS
		 * @explanation
		 * The resource was able to successfully stop the HADB
		 * database.
		 * @user_action
		 * This is an informational message, no user action is needed.
		 */
		scds_syslog(LOG_NOTICE, "Stop of HADB database completed "
			"successfully.");

		rc = SCHA_ERR_NOERR;
	}

finish:
	free(cmd);

	return (rc);
} /*lint !e715 */ /* scds_handle not referenced */

/*
 * Stop all the local HADB nodes running on this SC node.
 *
 * Return:
 *
 * SCHA_ERR_NOERR if successful.
 *
 * SCHA_ERR_STATE if any of the nodes that we want to stop is not
 * running (i.e. it's already stopped or is recovering).
 *
 * SCHA_ERR_INVAL is there is an error stopping a node.
 *
 * SCHA_ERR_RG if we run into both the STATE and INVAL conditions.
 */

scha_err_t
stop_local_nodes(scds_handle_t scds_handle)
{
	int		i, j;
	scha_err_t	rc;
	boolean_t	error_stopping = B_FALSE;

	i = site_info.scnodeid;

	for (j = 0; j < MAX_HADB_NODES_PER_SYS; j++) {
		if (sc_node[i].hadb_node[j].nodeno < 0)
			break;

		if (strcmp(sc_node[i].hadb_node[j].nodestate, NODE_STOPPED)
			!= 0) {
			rc = run_hadb_stopnode_command(scds_handle,
				sc_node[i].hadb_node[j].nodeno);
			if (rc != SCHA_ERR_NOERR) {
				/*
				 * msg already logged
				 *
				 * Remember that we had an error, but
				 * we don't want to return yet because
				 * we don't want this error to prevent
				 * us from stopping other local nodes.
				 */

				error_stopping = B_TRUE;
			}
		} else {
			/*
			 * If one of the local HADB nodes is not
			 * running somethis up.  It's not an error in
			 * the sense that we should return a failure
			 * but we should complain loudly.
			 */

			/*
			 * SCMSGS
			 * @explanation
			 * The specified HADB node is already in a non running
			 * state and does not need to be stopped.
			 * @user_action
			 * This is an informational message, no user action is
			 * needed.
			 */
			scds_syslog(LOG_ERR, "Not stopping "
				"HADB node %d which is in state %s",
				sc_node[i].hadb_node[j].nodeno,
				sc_node[i].hadb_node[j].nodestate);
		}
	}

	if (error_stopping)
		return (SCHA_ERR_INVAL);
	else
		return (SCHA_ERR_NOERR);
}

/*
 * Run the stopnode command for the given HADB node.
 */

scha_err_t
run_hadb_stopnode_command(scds_handle_t scds_handle, int node)
{
	char		*cmd;
	scha_err_t	rc;
	int		err, exit_code;
	boolean_t	repair;

	/*
	 * The command we're going to run is:
	 *
	 *	hadbm stopnode [--no-repair] --yes NODE DB
	 *
	 * If the DB is HAFaultTolerant that means each DRU has an
	 * available spare and it will activate a spare when a active
	 * node is stopped.
	 *
	 * If the DB is not HAFaultTolerant (HAFT) we run with the
	 * --no-repair flag since there are no spares to activate.
	 *
	 * We'll check if the DB is only Operational, if it is we
	 * really should be stopping the database.  We'll log and
	 * return an error.
	 *
	 * Note that we don't have a fallback if the stopnode fails -
	 * we can't identify what processes belong to a given node and
	 * the --force option doesn't do anything other then allow us
	 * to stop already stopped nodes, from the hadbm
	 * specification:
	 *
	 * If --force option is false(default)
	 *	The node that is to be stopped should not be in
	 *	stopped state.
	 * If --force option is true
	 *	The node that is to be stopped can be in any state
	 *	including stopped.
	 *
	 * We already check if the node is already stopped and skip
	 * stopping it if it is already stopped.
	 */

	if (strcmp(site_info.status, DB_OPERATIONAL) == 0) {
		ds_internal_error("Database is in the operational state.  "
			"Cannot stop individual HADB nodes.");
		print_hadb_config_info();
		return (SCHA_ERR_INTERNAL);
	}

	/*
	 * As explained above, if the DB is not HAFT we need to supply
	 * the no-repair flag.
	 *
	 * We redirect stderr because the hadbm script runs stty which
	 * when stdout is not a tty will print: "stty: : No such
	 * device or address" which ends up on the console twice every
	 * probe cycle.  A bug has been filed against hadbm.
	 */
	if (strcmp(site_info.status, DB_HA_FAULT_TOLERANT) == 0) {
		cmd = ds_string_format("%s stopnode --yes %d %s 2>/dev/null",
			site_info.hadbm_cmd, node, site_info.dbname);
		repair = B_TRUE;
	} else {
		cmd = ds_string_format("%s stopnode --no-repair --yes %d %s "
			"2>/dev/null",
			site_info.hadbm_cmd, node, site_info.dbname);
		repair = B_FALSE;
	}

	if (cmd == NULL)
		return (SCHA_ERR_NOMEM);

	scds_syslog_debug(DBG_LEVEL_HIGH, "Running nodestop command: %s.",
		cmd);

	err = system(cmd);
	exit_code = WEXITSTATUS((uint_t)err);
	if (err == -1) {
		/* If system() itself had a failure. */

		/*
		 * SCMSGS
		 * @explanation
		 * The resource was unable to successfully run the hadbm stop
		 * command either because it was unable to execute the
		 * program, or the hadbm command received a signal.
		 * @user_action
		 * This might be the result of a lack of system resources.
		 * Check whether the system is low in memory and take
		 * appropriate action.
		 */
		scds_syslog(LOG_ERR, "Stop of HADB node %d did not "
			"complete: %s.", node, strerror(errno));
		rc = SCHA_ERR_INVAL; /* as good as any other */
		goto finish;

	} else if (WIFSIGNALED((uint_t)err)) {
		/* If we didn't complete due to being signaled... */

		/*
		 * SCMSGS
		 * @explanation
		 * The resource was unable to successfully run the hadbm
		 * startnode command either because it was unable to execute
		 * the program, or the hadbm command received a signal.
		 * @user_action
		 * This might be the result of a lack of system resources.
		 * Check whether the system is low in memory and take
		 * appropriate action.
		 */
		scds_syslog(LOG_ERR, "Stop of HADB node %d did not "
			"complete.", node);
		rc = SCHA_ERR_INVAL;
		goto finish;

	} else if (repair && exit_code != 0) {
		/* If hadbm did not succeed. */

		/*
		 * SCMSGS
		 * @explanation
		 * The resource encountered an error trying to stop the HADB
		 * node.
		 * @user_action
		 * Examine other syslog messages occurring around the same
		 * time on the same node, to see if the source of the problem
		 * can be identified.
		 */
		scds_syslog(LOG_ERR, "Stop of HADB node %d failed with "
			"exit code %d.", node, WEXITSTATUS((uint_t)err));
		rc = SCHA_ERR_INVAL;
		goto finish;

	} else if (!repair && !(exit_code == 0 || exit_code == 1)) {
		/*
		 * no-repair was specified and hadbm did not succeed.
		 * If no-repair was specified hadbm will return 1, we
		 * also allow it to return the normal success code of
		 * 0 for future proofness.
		 */

		scds_syslog(LOG_ERR, "Stop of HADB node %d failed with "
			"exit code %d.", node, WEXITSTATUS((uint_t)err));
		rc = SCHA_ERR_INVAL;
		goto finish;

	} else {
		/* Success. */
		/*
		 * SCMSGS
		 * @explanation
		 * The resource was able to successfully stop the specified
		 * HADB node.
		 * @user_action
		 * This is an informational message, no user action is needed.
		 */
		scds_syslog(LOG_NOTICE, "Stop of HADB node %d completed "
			"successfully.", node);

		rc = SCHA_ERR_NOERR;
	}

finish:
	free(cmd);

	return (rc);
} /*lint !e715 */ /* scds_handle not referenced */

/*
 * This function starts the fault monitor for a hadb resource.  This
 * is done by starting the probe under PMF. The PMF tag is derived as
 * <RG-name,RS-name,instance_number.mon>. The restart option of PMF is
 * used but not the "infinite restart". Instead interval/retry_time is
 * obtained from the RTR file.
 */

int
mon_start(scds_handle_t scds_handle)
{
	scha_err_t	err;

	scds_syslog_debug(DBG_LEVEL_HIGH,
	    "Calling MONITOR_START method for resource <%s>.",
	    scds_get_resource_name(scds_handle));

	/*
	 * The probe hadb_probe is assumed to be available in the same
	 * subdirectory where the other callback methods for the RT
	 * are installed. The last parameter to scds_pmf_start denotes
	 * the child monitor level. Since we are starting the probe
	 * under PMF we need to monitor the probe process only and
	 * hence we are using a value of 0.
	 */

	err = scds_pmf_start(scds_handle, SCDS_PMF_TYPE_MON,
		SCDS_PMF_SINGLE_INSTANCE, "hadb_probe", 0);

	if (err != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to start fault monitor.");
		return (err);
	}

	scds_syslog(LOG_INFO, "Started the fault monitor.");

	return (SCHA_ERR_NOERR); /* Successfully started Monitor */
}


/*
 * This function stops the fault monitor for a hadb resource.
 * This is done via PMF. The PMF tag for the fault monitor is
 * constructed based on <RG-name_RS-name,instance_number.mon>.
 */

int
mon_stop(scds_handle_t scds_handle)
{
	scha_err_t	err;

	scds_syslog_debug(DBG_LEVEL_HIGH, "Calling scds_pmf_stop method");

	err = scds_pmf_stop(scds_handle, SCDS_PMF_TYPE_MON,
		SCDS_PMF_SINGLE_INSTANCE, SIGKILL,
		FINAL_STOP_TIMEOUT);

	if (err != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to stop fault monitor.");
		return (err);
	}

	scds_syslog(LOG_INFO, "Stopped the fault monitor.");

	return (SCHA_ERR_NOERR); /* Successfully stopped monitor */
}

/*
 * Populate the global site_info and sc_node data structures with
 * configuration information of the HADB site and resource.  Failures
 * are logged to syslog and printed to stderr if print is set to true.
 */

scha_err_t
get_hadb_config_info(scds_handle_t scds_handle, int print)
{
	const scha_str_array_t	*nodelist;
	scha_str_array_t	*confdirs;
	scha_uint_array_t	*nodeids = NULL;
	scha_err_t		rc;
	uint_t			i, j;
	uint_t			n;

	set_print_errors(print);

	/*
	 * Initialize the global variables to default values.  SC Node
	 * IDs range from 1 to 64.  Index 0 is initialized to NULL,
	 * but afterwards should not be used.
	 */

	for (i = 0; i <= MAX_SC_NODES; i++) {
		sc_node[i].sysname = NULL;
		sc_node[i].privname = NULL;
		sc_node[i].hadb = B_FALSE;

		/*
		 * Initialize each SC nodes HADB nodes to default
		 * values.
		 */

		for (j = 0; j < MAX_HADB_NODES_PER_SYS; j++) {
			sc_node[i].hadb_node[j].nodeno = -1;
			sc_node[i].hadb_node[j].mirror = -1;
		}
	}

	site_info.sysname = NULL;
	site_info.scnodeid = -1;
	site_info.n_rg_nodes = 0;
	site_info.n_rg_online = 0;
	site_info.n_rg_pending_online = 0;
	site_info.auto_recover_failed = B_FALSE;
	site_info.startnode_error = B_FALSE;

	/*
	 * Get HADB_ROOT and construct the commands we need.
	 */

	rc = get_string_ext_property(scds_handle, "HADB_ROOT",
		&(site_info.hadb_root));
	if (rc != SCHA_ERR_NOERR) {
		/* Message already logged and printed */
		return (rc);
	}

	site_info.hadbm_cmd = ds_string_format("%s/bin/hadbm",
		site_info.hadb_root);
	if (site_info.hadbm_cmd == NULL) {
		if (print)
			(void) fprintf(stderr, gettext("Out of memory.\n"));
		return (SCHA_ERR_NOMEM);
	}

	/*
	 * Get the database name.
	 */

	rc = get_string_ext_property(scds_handle, "DB_Name",
		&site_info.dbname);
	if (rc != SCHA_ERR_NOERR) {
		/* Message already logged and printed. */
		return (rc);
	}

	/*
	 * Get Confdir_list extension property
	 */

	confdirs = scds_get_ext_confdir_list(scds_handle);

	/* Return an error if there is no config_dirs x prop */
	if (confdirs == NULL || confdirs->array_cnt == 0) {
		scds_syslog(LOG_ERR, "Property %s is not set.",
			"Confdir_list");
		if (print) {
			(void) fprintf(stderr, gettext("Property %s is not "
						"set.\n"), "Confdir_list");
		}
		return (1);
	}

	site_info.confdir = confdirs->str_array[0];

	/*
	 * Get the local nodename.
	 */

	rc = scha_cluster_getnodename(&(site_info.sysname));
	if (rc != SCHA_ERR_NOERR) {
		ds_internal_error("%s.", scds_error_string(rc));
		if (print)
			(void) fprintf(stderr,
				gettext("INTERNAL ERROR: %s.\n"),
				scds_error_string(rc));
		return (rc);
	}

	/*
	 * Get the local node id.
	 */

	rc = get_local_nodeid(scds_handle, &n);
	if (rc != SCHA_ERR_NOERR) {
		ds_internal_error("%s.", scds_error_string(rc));
		if (print)
			(void) fprintf(stderr,
				gettext("INTERNAL ERROR: %s.\n"),
				scds_error_string(rc));
		return (rc);
	}
	site_info.scnodeid = (int)n;

	/*
	 * Get the list of all the node ids.  We index
	 * sc_node[] by the SC node id.
	 */

	rc = get_all_nodeids(scds_handle, &nodeids);
	if (rc != SCHA_ERR_NOERR) {
		/* msg logged and printed */
		return (rc);
	}

	if (nodeids == NULL || nodeids->array_cnt < 1) {
		ds_internal_error("Node id list is empty.");
		if (print)
			(void) fprintf(stderr, gettext("INTERNAL ERROR: "
					"Node id list is empty.\n"));

		return (SCHA_ERR_INTERNAL);
	}

	/*
	 * For each SC node id get it's name and also it's private
	 * interconnect hostname.
	 */

	for (i = 0; i < nodeids->array_cnt; i++) {
		uint_t	id = (nodeids->uint_array)[i];
		char	*name;

		rc = get_nodename_from_id(scds_handle, id, &name);
		if (rc != SCHA_ERR_NOERR) {
			/* msg logged and printed */
			return (rc);
		}

		sc_node[id].sysname = name;

		rc = get_privname_from_nodename(scds_handle,
			sc_node[id].sysname, &name);
		if (rc != SCHA_ERR_NOERR) {
			/* msg logged and printed */
			return (rc);
		}

		sc_node[id].privname = name;
	}

	/*
	 * Get the nodelist for the RG.  This may differ from the
	 * nodelist of the entire SC cluster.
	 */

	scds_syslog_debug(DBG_LEVEL_HIGH, "Getting Nodelist.");

	nodelist = scds_get_rg_nodelist(scds_handle);

	/*
	 * array_cnt should technically match our validation
	 * constraints (even number, match max and desired primaries,
	 * etc), but that has already been checked by svc_validate()
	 * at resource creation and start times and we don't want to
	 * duplicate the code in multiple places.
	 */

	if (nodelist == NULL || nodelist->array_cnt < 1) {
		ds_internal_error("Nodelist is empty.");
		if (print)
			(void) fprintf(stderr, gettext("INTERNAL ERROR: "
					"Nodelist is empty.\n"));

		return (SCHA_ERR_INTERNAL);
	}

	/*
	 * If array_cnt is greater then MAX_SC_NODES (which is, as of
	 * 2003, 64) then something is wrong.  Sun Cluster assumes a
	 * maximum of 64 nodes.
	 */

	if (nodelist->array_cnt > MAX_SC_NODES) {
		ds_internal_error("Invalid number of nodes in nodelist.");
		if (print)
			(void) fprintf(stderr, gettext("INTERNAL ERROR: "
					"Invalid number of nodes in "
					"nodelist.\n"));

		return (SCHA_ERR_INTERNAL);
	}

	site_info.n_rg_nodes = nodelist->array_cnt;

	/*
	 * Go through the RG nodelist and match up the node names with
	 * the names in sc_node.  When we find matches set the
	 * hadb flag to true to indicate that that SC node is part of
	 * the HADB site.
	 */

	for (i = 0; i < nodelist->array_cnt; i++) {

		for (j = 1; j <= MAX_SC_NODES; j++) {
			if (sc_node[j].sysname == NULL)
				continue;

			if (strcasecmp(nodelist->str_array[i],
				    sc_node[j].sysname) == 0) {

				/* Found a match */

				sc_node[j].hadb = B_TRUE;

				break;
			}
		}

		if (j > MAX_SC_NODES) {
			/* A match was not found */

			ds_internal_error("Nodelist includes non-existent "
				"cluster node: %s.", nodelist->str_array[i]);
			if (print)
				(void) fprintf(stderr,
					gettext("Nodelist includes "
						"non-existent cluster "
						"node: %s.\n"),
					nodelist->str_array[i]);

			return (SCHA_ERR_INVAL);
		}
	}

	rc = get_hadb_node_info(scds_handle, print);
	if (rc != SCHA_ERR_NOERR)
		return (rc);

	rc = get_hadb_status(scds_handle, &(site_info.status));
	if (rc != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * An error occured while trying to read the output of the
		 * hadbm status command.
		 * @user_action
		 * Examine other syslog messages occurring around the same
		 * time on the same node, to see if the source of the problem
		 * can be identified.
		 */
		scds_syslog(LOG_ERR, "Unable to determine database status.");
		return (rc);
	}

	/*
	 * Get the state of the RG on each of the nodes.
	 */

	for (i = 1; i <= MAX_SC_NODES; i++) {
		if (sc_node[i].sysname == NULL)
			continue; /* skip non-existent node */

		rc = get_rg_state_node(scds_handle, sc_node[i].sysname,
			&(sc_node[i].rgstate));
		if (rc != SCHA_ERR_NOERR)
			return (rc);

		if (sc_node[i].hadb &&
		    sc_node[i].rgstate == SCHA_RGSTATE_ONLINE)
			site_info.n_rg_online++;

		if (sc_node[i].hadb &&
		    sc_node[i].rgstate == SCHA_RGSTATE_PENDING_ONLINE)
			site_info.n_rg_pending_online++;
	}

	/*
	 * Check to see if the resource is being disabled.  This will
	 * only be true when the STOP method is being called when the
	 * resource has been disabled.
	 *
	 * We'll wait to get the resource state because we only need
	 * it if we're being disabled.  We also wait to get it because
	 * the resource state is not updated atomically across all the
	 * nodes.  Waiting until we actually need it should allow the
	 * state to "settle" to a consistent state across all the
	 * nodes.
	 */

	if (scds_get_rs_on_off_switch(scds_handle) == SCHA_SWITCH_DISABLED)
		site_info.disabled = B_TRUE;
	else
		site_info.disabled = B_FALSE;

	/*
	 * Set the flag indicating that we have initialized the
	 * configuration information.
	 */
	site_info_initialized = B_TRUE;

	/*
	 * Print the hadb config/status info to debug, doing it here
	 * insures that each method will print the info.
	 */

	print_hadb_config_info();

	return (SCHA_ERR_NOERR);
}

/*
 * Get the HADB node information.
 *
 * Returns SCHA_ERR_NOERR in case of success.
 */

scha_err_t
get_hadb_node_info(scds_handle_t scds_handle, int print)
{
	FILE	*in;
	char	*cmd;
	char	line[STATUS_LINE_LEN];
	char	*s;

	int	node, port, mirror;
	char	*hostname, *noderole, *nodestate;
	int	hadbmerr = 0;
	int	firstln = 1; /* counter to identify the first line */

	scha_err_t	rc = SCHA_ERR_NOERR;

	/*
	 * --quiet suppresses the top one line of header info output
	 * from the command.
	 *
	 * We redirect stderr because the hadbm script runs stty which
	 * when stdout is not a tty will print: "stty: : No such
	 * device or address" which ends up on the console twice every
	 * probe cycle.  A bug has been filed against hadbm.
	 */
	cmd = ds_string_format("%s status --quiet --nodes %s 2>/dev/null",
		site_info.hadbm_cmd, site_info.dbname);
	if (cmd == NULL) {
		if (print)
			(void) fprintf(stderr, gettext("Out of memory.\n"));
		return (SCHA_ERR_NOMEM);
	}

	in = popen(cmd, "r");
	if (in == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * The specified command was unable to run because of the
		 * specified reason.
		 * @user_action
		 * Examine other syslog messages occurring around the same
		 * time on the same node, to see if the source of the problem
		 * can be identified.
		 */
		scds_syslog(LOG_ERR, "Unable to to run %s: %s.", cmd,
			strerror(errno));
		if (print)
			(void) fprintf(stderr,
				gettext("Unable to run %s: %s.\n"), cmd,
				strerror(errno));

		rc = SCHA_ERR_INVAL;
		goto finish;
	}

	while (!feof(in)) {
		s = fgets(line, sizeof (line), in);
		if (s == NULL && !feof(in)) {
			/*
			 * SCMSGS
			 * @explanation
			 * An error was encountered while reading the output
			 * from the specified hadbm command.
			 * @user_action
			 * Examine other syslog messages occurring around the
			 * same time on the same node, to see if the source of
			 * the problem can be identified.
			 */
			scds_syslog(LOG_ERR, "Error reading line from "
				"hadbm command: %s.", strerror(errno));
			rc = SCHA_ERR_INVAL;
			goto finish;
		}

		/*
		 * Check for feof(). feof() bit/byte will be set only if
		 * it's  encountered during an operation. A feof() will
		 * not consciously do a read and report it. In our case,
		 * if eof comes just after a newline, we will end up
		 * parsing an extra same line, as while(!feof(in)) wont'
		 * catch it. Also, only (s == NULL) should have been
		 * checked above, but, don't know why ?? Let's break now.
		 * We should be OK quitting, because the output from
		 * hadbm is structured and bound to end with newline first.
		 */
		if (feof(in))
			break;

		/*
		 * Parse first output line for "Error:". This is to
		 * make user's life easier when errors from 'hadbm'
		 * wrapper script are encountered. The errors seem
		 * to follow a pattern - start with "Error:" on the
		 * very first line.
		 */
		if (firstln && strstr(line, "Error:")) {
			hadbmerr = 1; /* remember this event */
			rc = SCHA_ERR_INVAL;
			/*
			 * SCMSGS
			 * @explanation
			 * Printed whenever hadbm command execution fails to
			 * take off.
			 * @user_action
			 * Make sure that java executable present in /usr/bin
			 * is linked to appropriate version of j2se needed by
			 * hadbm. Also, depending upon the subsequesnt
			 * messages printed, take appropriate actions. The
			 * messages following this message are printed as is
			 * from hadbm wrapper script.
			 */
			scds_syslog(LOG_ERR, "Error reported from"
					" command: %s.", cmd);
			if (print)
				(void) fprintf(stderr,
					gettext("Error reported from"
						" command: %s.\n"), cmd);
		}

		/* Error output could be more than one line */
		if (hadbmerr) {
			scds_syslog(LOG_ERR, "%s", line);
			if (print)
				(void) fprintf(stderr, gettext("%s"),
					gettext(line));
			continue;
			/* Don't have to further parse the lines. */
		}

		if (firstln)
			firstln = 0;

		/*
		 * Break out the individual fields of the line.
		 */

		rc = parse_node_status_line(line, &node, &hostname, &port,
			&noderole, &nodestate, &mirror);
		if (rc != SCHA_ERR_NOERR)
			goto finish;

		scds_syslog_debug(DBG_LEVEL_HIGH, "HADB Status Line parsed "
			"to node: %d, hostname: %s, port: %d, noderole: %s, "
			"nodestate: %s, mirror: %d", node,
			hostname, port, noderole, nodestate, mirror);

		rc = add_hadb_node_to_config(node, hostname, nodestate,
			mirror, print);
		if (rc != SCHA_ERR_NOERR)
			goto finish;
	}

finish:
	(void) pclose(in);
	free(cmd);

	return (rc);
} /*lint !e715 */ /* scds_handle not referenced */

/*
 * Get the HADB database state.
 *
 * Returns SCHA_ERR_NOERR in case of success.
 */

scha_err_t
get_hadb_status(scds_handle_t scds_handle, char **status)
{
	FILE	*in = NULL;
	char	*cmd;
	char	line[STATUS_LINE_LEN];
	char	*p;

	scha_err_t	rc = SCHA_ERR_NOERR;

	/* Set the status to SC unknown just in case */
	*status = DB_SC_UNKNOWN;

	/*
	 * --quiet suppresses the top one line of header output.
	 *
	 * We redirect stderr because the hadbm script runs stty which
	 * when stdout is not a tty will print: "stty: : No such
	 * device or address" which ends up on the console twice every
	 * probe cycle.  A bug has been filed against hadbm.
	 */
	cmd = ds_string_format("%s status --quiet %s 2>/dev/null",
		site_info.hadbm_cmd, site_info.dbname);
	if (cmd == NULL) {
		*status = DB_SC_ERROR;
		rc = SCHA_ERR_NOMEM;
		goto finish;
	}

	in = popen(cmd, "r");
	if (in == NULL) {
		scds_syslog(LOG_ERR, "Unable to to run %s: %s.", cmd,
			strerror(errno));
		*status = DB_SC_ERROR;
		rc = SCHA_ERR_INVAL;
		goto finish;
	}

	/*
	 * The command and it's output is:
	 *
	 * # hadbm status db1
	 * db1 Operational
	 */

	if (!feof(in)) {
		if (fgets(line, sizeof (line), in) == NULL) {
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Examine other syslog messages occurring around the
			 * same time on the same node, to see if the source of
			 * the problem can be identified.
			 */
			scds_syslog(LOG_ERR, "Error while reading output "
				"of %s.", cmd);
			*status = DB_SC_ERROR;
			rc = SCHA_ERR_STATE;
			goto finish;
		}

		/*
		 * fgets() includes the newline, terminate it if it
		 * is there.
		 */

		for (p = line; *p != '\0'; p++)
			if (*p == '\n') {
				*p = '\0';
				break;
			}

		/*
		 * Skip over the database name until we hit
		 * whitespace
		 */
		for (p = line; *p != '\0' && !isspace(*p); p++)
			/* empty */;

		/* Skip over the whitespace */

		for (; isspace(*p); p++)
			/* empty */;

		scds_syslog_debug(DBG_LEVEL_HIGH, "HADBM Status [%s].", p);

		if (strcasecmp(DB_HA_FAULT_TOLERANT, p) == 0)
			*status = DB_HA_FAULT_TOLERANT;

		else if (strcasecmp(DB_FAULT_TOLERANT, p) == 0)
			*status = DB_FAULT_TOLERANT;

		else if (strcasecmp(DB_OPERATIONAL, p) == 0)
			*status = DB_OPERATIONAL;

		else if (strcasecmp(DB_STOPPED, p) == 0)
			*status = DB_STOPPED;

		else if (strcasecmp(DB_NON_OPERATIONAL, p) == 0)
			*status = DB_NON_OPERATIONAL;

		else if (strcasecmp(DB_UNKNOWN, p) == 0)
			*status = DB_UNKNOWN;

		else {
			ds_internal_error("Unknown database status: %s", p);
			rc = SCHA_ERR_STATE;
			*status = DB_SC_UNKNOWN;
		}
	} else {
		rc = SCHA_ERR_INVAL;
		*status = DB_SC_ERROR;
	}

finish:
	scds_syslog_debug(DBG_LEVEL_HIGH, "Database status is %s.", *status);

	if (in != NULL)
		(void) pclose(in);
	free(cmd);

	return (rc);
} /*lint !e715 */ /* scds_handle not referenced */

/*
 * Add the HADB node for hostname to the global HADB config info.  If
 * the node is already in the configuration update it with any new
 * information.
 *
 * Return SCHA_ERR_NOERR on success.
 */

scha_err_t
add_hadb_node_to_config(int node, char *hostname, char *nodestate,
	int mirror, boolean_t print_messages)
{
	int	i, j;

	/*
	 * Get nodestate to point to the static strings defined in the
	 * NODE_STARTING, NODE_STOPPED, ... macros.
	 */
	if (strcasecmp(nodestate, NODE_STARTING) == 0)
		nodestate = NODE_STARTING;

	else if (strcasecmp(nodestate, NODE_RUNNING) == 0)
		nodestate = NODE_RUNNING;

	else if (strcasecmp(nodestate, NODE_STOPPED) == 0)
		nodestate = NODE_STOPPED;

	else if (strcasecmp(nodestate, NODE_RECOVERING) == 0)
		nodestate = NODE_RECOVERING;

	else if (strcasecmp(nodestate, NODE_REPAIRING) == 0)
		nodestate = NODE_REPAIRING;

	else if (strcasecmp(nodestate, NODE_WAITING) == 0)
		nodestate = NODE_WAITING;

	else if (strcasecmp(nodestate, NODE_STOPPING) == 0)
		nodestate = NODE_STOPPING;

	else {
		ds_internal_error("Unknown node status: %s.", nodestate);
		if (print_messages)
			(void) fprintf(stderr,
				gettext("INTERNAL ERROR: Unknown node status "
					"%s.\n"), nodestate);

		nodestate = NODE_SC_UNKNOWN;
	}

	for (i = 1; i <= MAX_SC_NODES; i++) {

		/* Skip non-existent nodes */
		if (sc_node[i].sysname == NULL)
			continue;

		if (strcasecmp(hostname, sc_node[i].privname) == 0) {

			for (j = 0; j < MAX_HADB_NODES_PER_SYS; j++) {
				if (sc_node[i].hadb_node[j].nodeno == -1) {
					/*
					 * Found empty slot - do the
					 * following struct/pointer
					 * stuff to keep the code from
					 * marching off the right hand
					 * side.
					 */

					struct hadb_node_info *n =
						&sc_node[i].hadb_node[j];

					n->nodeno = node;
					n->mirror = mirror;
					n->nodestate = nodestate;

					break;
				}
				if (sc_node[i].hadb_node[j].nodeno == node) {
					/*
					 * node is already in the
					 * configuration.  Update it.
					 */

					struct hadb_node_info *n =
						&sc_node[i].hadb_node[j];

					n->mirror = mirror;
					n->nodestate = nodestate;

					break;
				}
			}

			if (j >= MAX_HADB_NODES_PER_SYS) {
				/* No available slots left */
				ds_internal_error("Max HADB nodes exceeded");
				if (print_messages)
					(void) fprintf(stderr,
						gettext("INTERNAL ERROR: "
							"Max HADB nodes "
							"exceeded.\n"));
			}

			break;
		}
	}

	if (i > MAX_SC_NODES) {	/* No match found */
		/*
		 * SCMSGS
		 * @explanation
		 * The HADB database must be created using the hostnames of
		 * the Sun Cluster private interconnect, the hostname for the
		 * specified HADB node is not a private cluster hostname.
		 * @user_action
		 * Recreate the HADB and specify cluster private interconnect
		 * hostnames.
		 */
		scds_syslog(LOG_ERR, "HADB node %d for host %s "
			"does not match any Sun Cluster interconnect "
			"hostname.", node, hostname);
		if (print_messages)
			(void) fprintf(stderr,
				gettext("HADB node %d for host %s "
					"does not match any Sun Cluster "
					"interconnect hostname.\n"),
				node, hostname);

		return (SCHA_ERR_INVAL);
	}

	return (SCHA_ERR_NOERR);
}

/*
 * Break out the individual fields of the line.  The caller is
 * responsible for releasing (via free(3)) the string arguments after
 * a successful call.
 *
 * Returns SCHA_ERR_NOERR on success.
 *
 * The output from hadbm looks like:
 *
 * NodeNo HostName Port NodeRole NodeState MirrorNode
 * 0 clusternode1-priv 15100 Other Stopped n/a
 * 1 clusternode2-priv 15120 Other Stopped n/a
 */
scha_err_t
parse_node_status_line(char *line, int *node, char **hostname, int *port,
	char **noderole, char **nodestate, int *mirror)
{
	char *tok;
	char *copy;

	/* Have strtok() trash a copy of the line */

	if ((copy = strdup(line)) == NULL) {
		scds_syslog(LOG_ERR, "Out of memory.");
		return (SCHA_ERR_NOMEM);
	}

	/*
	 * Initialize all the strings to NULL so in case of an error
	 * we can just free them all.
	 */

	*hostname = *noderole = *nodestate = NULL;

	/* Get the node number */

	if ((tok = strtok(copy, " \t")) == NULL)
		goto error;
	*node = atoi(tok);
	if (*node < 0)		/* make sure it makes sense */
		goto error;

	/* Get hostname */

	if ((tok = strtok(NULL, " \t")) == NULL)
		goto error;
	if ((*hostname = strdup(tok)) == NULL)
		goto error;

	/* Get port */

	if ((tok = strtok(NULL, " \t")) == NULL)
		goto error;
	*port = atoi(tok);
	if (*port < 0)		/* make sure port makes sense */
		goto error;

	/* Get noderole */

	if ((tok = strtok(NULL, " \t")) == NULL)
		goto error;
	if ((*noderole = strdup(tok)) == NULL)
		goto error;

	/* Get nodestate */

	if ((tok = strtok(NULL, " \t")) == NULL)
		goto error;
	if ((*nodestate = strdup(tok)) == NULL)
		goto error;

	/*
	 * Get mirror node of this HADB node, it might be a number or
	 * "n/a"
	 */

	if ((tok = strtok(NULL, " \t")) == NULL)
		goto error;
	if (isdigit(tok[0])) {
		*mirror = atoi(tok);
		if (*mirror < 0)
			goto error;
	} else {		/* it's "n/a" */
		*mirror = -1;
	}

	free(copy);
	return (SCHA_ERR_NOERR);

error:
	free(*hostname);
	free(*noderole);
	free(*nodestate);

	/*
	 * SCMSGS
	 * @explanation
	 * The resource had an error while parsing the specified line of
	 * output from the hadbm status --nodes command.
	 * @user_action
	 * Examine other syslog messages occurring around the same time on the
	 * same node, to see if the source of the problem can be identified.
	 * Otherwise contact your authorized Sun service provider to determine
	 * whether a workaround or patch is available.
	 */
	scds_syslog(LOG_ERR, "Error parsing node status line (%s).", line);
	free(copy);

	return (SCHA_ERR_INVAL);
}

/*
 * Update the existing hadb config with the current RG state.
 */

scha_err_t
update_hadb_config_info(scds_handle_t scds_handle, int print)
{
	int	rc;

	if (!site_info_initialized) {
		ds_internal_error("Site info has not been initialized.");
		return (SCHA_ERR_NOERR);
	}

	/*
	 * Get the overall database status.
	 * Get the node info first. Database status being
	 * the primary driver of the logic, it's better to
	 * get it later.
	 */
	rc = get_hadb_node_info(scds_handle, print);
	if (rc != SCHA_ERR_NOERR)
		return (rc);

	rc = get_hadb_status(scds_handle, &(site_info.status));
	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Unable to determine database status.");
		return (rc);
	}

	/*
	 * If the database is running we don't need to get the RG
	 * state for the nodes since we don't have to make a decision
	 * on wether we need to start the database - it's already
	 * running.
	 *
	 * This reduces the demands that we place on the RGM president
	 * node.
	 */

	if (!database_running(scds_handle)) {
		rc = update_rg_state_info(scds_handle);
		if (rc != SCHA_ERR_NOERR) {
			/*
			 * SCMSGS
			 * @explanation
			 * A critical method was unable to determine the
			 * status of the specified resource group.
			 * @user_action
			 * Please examine other messages in the
			 * /var/adm/messages file to determine the cause of
			 * this problem. Also verify if the resource group is
			 * available or not. If not available, start the
			 * service or resource and retry the operation which
			 * failed.
			 */
			scds_syslog(LOG_ERR, "Unable to determine resource "
				"group status.");
			return (rc);
		}
	} else
		scds_syslog_debug(DBG_LEVEL_HIGH, "Skipping update of the "
			"RG state.");

	return (SCHA_ERR_NOERR);
}

/*
 * Update the global status info with the RG state of all of our SC
 * nodes.
 */

scha_err_t
update_rg_state_info(scds_handle_t scds_handle)
{
	int		i;
	scha_err_t	rc;

	/* Get a fresh count of nodes online */
	site_info.n_rg_online = 0;
	site_info.n_rg_pending_online = 0;

	/*
	 * Get the state of the RG on each of the nodes.
	 */

	for (i = 1; i <= MAX_SC_NODES; i++) {
		if (sc_node[i].sysname == NULL)
			continue; /* skip non-existent node */

		rc = get_rg_state_node(scds_handle,
			sc_node[i].sysname, &(sc_node[i].rgstate));
		if (rc != SCHA_ERR_NOERR)
			return (rc);

		if (sc_node[i].hadb &&
			sc_node[i].rgstate == SCHA_RGSTATE_ONLINE)
			site_info.n_rg_online++;

		if (sc_node[i].hadb && sc_node[i].rgstate ==
			SCHA_RGSTATE_PENDING_ONLINE)
			site_info.n_rg_online++;
	}

	return (SCHA_ERR_NOERR);
}

/*
 * Update the global status info with the resource state of all of our
 * SC nodes.
 */

scha_err_t
update_rs_state_info(scds_handle_t scds_handle)
{
	int		i;
	scha_err_t	rc;

	/*
	 * Get the state of the resource on each of the nodes.
	 */

	for (i = 1; i <= MAX_SC_NODES; i++) {
		if (sc_node[i].sysname == NULL)
			continue; /* skip non-existent node */

		rc = get_rs_state_node(scds_handle,
			sc_node[i].sysname, &(sc_node[i].rsstate));
		if (rc != SCHA_ERR_NOERR)
			return (rc);
	}

	return (SCHA_ERR_NOERR);
}

/*
 * Print the HADB configuration to syslog.
 */

void
print_hadb_config_info(void)
{
	int	i, j;

	scds_syslog_debug(DBG_LEVEL_HIGH, "Database status: %s",
		site_info.status);
	scds_syslog_debug(DBG_LEVEL_HIGH, "Local system: %s (node %d).",
		site_info.sysname, site_info.scnodeid);
	scds_syslog_debug(DBG_LEVEL_HIGH, "Total SC systems in RG: %d.",
		site_info.n_rg_nodes);
	scds_syslog_debug(DBG_LEVEL_HIGH, "Total SC systems in RG online: "
		"%d.", site_info.n_rg_online);

	for (i = 1; i <= MAX_SC_NODES; i++) {
		/* Skip any non-existent nodes */
		if (sc_node[i].sysname == NULL)
			continue;

		scds_syslog_debug(DBG_LEVEL_HIGH, "%s", sc_node[i].sysname);
		scds_syslog_debug(DBG_LEVEL_HIGH, "%s", sc_node[i].privname);
		scds_syslog_debug(DBG_LEVEL_HIGH, "  SC HADB node: %s",
			(sc_node[i].hadb) ? "TRUE" : "FALSE");
		scds_syslog_debug(DBG_LEVEL_HIGH, "  %s",
			rgstate_to_string(sc_node[i].rgstate));

		for (j = 0; j < MAX_HADB_NODES_PER_SYS; j++) {
			/*
			 * The first hadb node with nodeno == -1
			 * indicates the end of hadb nodes for this
			 * system.
			 */

			if (sc_node[i].hadb_node[j].nodeno == -1)
				break;

			scds_syslog_debug(DBG_LEVEL_HIGH, "  HADB node %d "
				"(mirror %d) is %s.",
				sc_node[i].hadb_node[j].nodeno,
				sc_node[i].hadb_node[j].mirror,
				sc_node[i].hadb_node[j].nodestate);
		}
	}
}

/*
 * Update the the local resource status from current hadb config info.
 *
 * We centralize all of the intelligence of "translating" the current
 * state of HADB and the resource here.  This also allows us to
 * remember what the last status and status string were and if they
 * are not changing then we can skip sending an update to RGM.
 */

void
update_resource_status(scds_handle_t scds_handle)
{
	char	*rsname = (char *)scds_get_resource_name(scds_handle);
	char	*rgname = (char *)scds_get_resource_group_name(scds_handle);

	static scha_rsstatus_t	prev_status = -1;
	static char		*prev_status_msg = "";

	scha_rsstatus_t	status;
	char		*status_msg;

	if (site_info.auto_recover_failed) {
		status = SCHA_RSSTATUS_FAULTED;
		status_msg = "Autorecovery of Sun ONE HADB Database failed.";
		goto set;
	}

	if (site_info.startnode_error) {
		status = SCHA_RSSTATUS_DEGRADED;
		status_msg = "Error starting one or more Sun ONE HADB nodes.";
		goto set;
	}

	if (database_running(scds_handle)) {
		status = SCHA_RSSTATUS_OK;
		status_msg = "Successfully started Sun ONE HADB Database.";
		goto set;
	}

	/*
	 * If the database is not running and a auto recovery did not
	 * fail we are waiting for the resource to start on enough
	 * nodes so that the database can be started.  If the
	 * stopstate file exists on this node we print a different
	 * message then the other nodes.
	 */

	if (file_exists("%s/stopstate", site_info.confdir) ==
	    SCHA_ERR_NOERR) {
		status = SCHA_RSSTATUS_DEGRADED;
		status_msg = "Waiting for nodes to start resource.";
	} else {
		status = SCHA_RSSTATUS_DEGRADED;
		status_msg = "Waiting for Sun ONE HADB database to start.";
	}

set:
	/*
	 * Check to see if the computed status and status message are
	 * different from the previous status and message.
	 *
	 * If they are different set the status to the new values, and
	 * remember the new values.
	 */

	if (status != prev_status ||
	    strcmp(status_msg, prev_status_msg) != 0) {

		(void) scha_resource_setstatus(rsname, rgname, status,
			status_msg);

		prev_status = status;
		prev_status_msg = status_msg;
	}
}

/*
 * Get the current time as expressed in seconds.
 */

int
gettime(void)
{
	/*
	 * gethrtime() returns the time in nanoseconds.  Convert it to
	 * seconds to make it compatible with our second based
	 * timeouts.
	 */

	return (int)(gethrtime() / 1e9);
}

/*
 * Is the database running according to the current HADB config info?
 */

boolean_t
database_running(scds_handle_t scds_handle)
{
	if (strcmp(site_info.status, DB_FAULT_TOLERANT) == 0 ||
	    strcmp(site_info.status, DB_OPERATIONAL) == 0 ||
	    strcmp(site_info.status, DB_HA_FAULT_TOLERANT) == 0)
		return (B_TRUE);
	else
		return (B_FALSE);
} /*lint !e715 */ /* scds_handle not referenced */

/*
 * Are all the local HADB nodes running according to the current HADB
 * config info?
 *
 * We consider every state other then stopped to be a "running" state.
 */

boolean_t
all_local_nodes_running(scds_handle_t scds_handle)
{
	int	i = site_info.scnodeid;
	int	j;

	for (j = 0; j < MAX_HADB_NODES_PER_SYS; j++) {
		if (sc_node[i].hadb_node[j].nodeno < 0)
			break;

		if (strcmp(sc_node[i].hadb_node[j].nodestate, NODE_STOPPED)
		    == 0)
			return (B_FALSE);
	}

	return (B_TRUE);

} /*lint !e715 */ /* scds_handle not referenced */

/*
 * Check to see that HADB mirror nodes are on different SC nodes.  If
 * they are not on different nodes complain loudly.
 *
 * The algorithm here is O(n^2) where n is the number of HADB nodes,
 * but n wil usually be relatively small (1-10 HADB nodes per SC
 * node).  In any case, the overhead of this routine will be tiny
 * compared to the overhead of running hadbm commands elsewhere.
 */

scha_err_t
validate_hadb_node_config(scds_handle_t scds_handle)
{
	int		i, j;
	int		scnodeid;
	scha_err_t	ok = SCHA_ERR_NOERR;

	for (i = 1; i <= MAX_SC_NODES; i++) {
		if (sc_node[i].sysname == NULL)
			continue;

		for (j = 0; j < MAX_HADB_NODES_PER_SYS; j++) {
			if (sc_node[i].hadb_node[j].nodeno < 0)
				break;

			if (sc_node[i].hadb_node[j].mirror < 0)
				continue;

			scnodeid = get_sc_node_by_hadb_node(
				sc_node[i].hadb_node[j].mirror);
			if (scnodeid < 0) {
				/*
				 * SCMSGS
				 * @explanation
				 * The HADB database must be created using the
				 * hostnames of the Sun Cluster private
				 * interconnect, the hostname for the
				 * specified HADB node is not a private
				 * cluster hostname.
				 * @user_action
				 * Recreate the HADB and specify cluster
				 * private interconnect hostnames.
				 */
				scds_syslog(LOG_ERR, "Unable to determine "
					"Sun Cluster node for HADB node %d.",
					sc_node[i].hadb_node[j].mirror);
				continue;
			}

			if (strcasecmp(sc_node[scnodeid].sysname,
				    sc_node[i].sysname) == 0) {
				/*
				 * SCMSGS
				 * @explanation
				 * The specified HADB nodes are mirror nodes
				 * of each and they are located on the same
				 * Sun Cluster node. This is a single point of
				 * failure because failure of the Sun Cluster
				 * node would cause both HADB nodes, which
				 * mirror each other, to fail.
				 * @user_action
				 * Recreate the HADB database with mirror HADB
				 * nodes on seperate Sun Cluster nodes.
				 */
				scds_syslog(LOG_ERR, "HADB mirror nodes %d "
					"and %d are on the same Sun Cluster "
					"node: %s.",
					sc_node[i].hadb_node[j].nodeno,
					sc_node[i].hadb_node[j].mirror,
					sc_node[i].sysname);
				ok = SCHA_ERR_STATE;
			}
		}
	}

	return (ok);
} /*lint !e715 */ /* scds_handle not referenced */

/*
 * For the given hadb node find the SC node that it is on.  If this
 * can not be determined a -1 is returned.
 */

int
get_sc_node_by_hadb_node(int hadbnode)
{
	int	i, j;
	int	scnodeid = -1;

	for (i = 1; i <= MAX_SC_NODES; i++) {
		if (sc_node[i].sysname == NULL)
			continue;

		for (j = 0; j < MAX_HADB_NODES_PER_SYS; j++) {
			if (sc_node[i].hadb_node[j].nodeno < 0)
				break;

			if (sc_node[i].hadb_node[j].nodeno == hadbnode)
				return (i);
		}
	}

	return (scnodeid);
}

/*
 * Check the stopstate file and determine if we are ready to start the
 * database.
 *
 * Returns:
 *
 * SCHA_ERR_NOERR	Yes, start database
 * SCHA_ERR_DEPEND	No, not ready
 * SCHA_ERR_INVAL	No, some error occured
 * SCHA_ERR_NOMEM	No, out of memory
 *
 * The format of the stopstate file is:
 *
 * physnode:logicalnode:role:state
 *
 * Example:
 *
 * 0:0:active:running
 * 1:1:active:running
 * 2:2:active:running
 * 3:3:offline:stopped
 *
 */

scha_err_t
check_stopstate(void)
{
	FILE		*in;
	char		*stopstate;
	char		line[SCDS_ARRAY_SIZE];
	int		lineno = 0;
	scha_err_t	rc, err;
	int		phys, logical;
	char		*role = NULL, *state = NULL;
	int		scnode;
	char		*s;

	stopstate = ds_string_format("%s/stopstate", site_info.confdir);
	if (stopstate == NULL)
		return (SCHA_ERR_NOMEM);

	in = fopen(stopstate, "r");
	if (in == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		scds_syslog(LOG_ERR, "Unable to open %s: %s.", stopstate,
			strerror(errno));
		rc = SCHA_ERR_INVAL;
		goto finish;
	}

	/*
	 * Assume that we'll find all the SC nodes that we need are
	 * online.  If we find otherwise we'll set rc to
	 * SCHA_ERR_DEPEND.
	 */

	rc = SCHA_ERR_NOERR;

	while (!feof(in)) {
		lineno++;
		s = fgets(line, SCDS_ARRAY_SIZE, in);
		if (s == NULL && !feof(in)) {
			/*
			 * SCMSGS
			 * @explanation
			 * There was an error reading from the stopstate file
			 * at the specified line.
			 * @user_action
			 * Examine other syslog messages occurring around the
			 * same time on the same node, to see if the source of
			 * the problem can be identified. Check to see if
			 * their is a problem with the stopstate file.
			 */
			scds_syslog(LOG_ERR, "Error reading line %d from "
				"stopstate file: %s.", lineno,
				strerror(errno));
			rc = SCHA_ERR_INVAL;
			goto finish;
		}

		err = split_stopstate_line(line, &phys, &logical, &role,
			&state);
		if (err != SCHA_ERR_NOERR) {
			/*
			 * SCMSGS
			 * @explanation
			 * An error was encountered on the specified line
			 * while parsing the stopstate file.
			 * @user_action
			 * Examine the stopstate file to determine if it is
			 * corrupted. Also examine other syslog messages
			 * occurring around the same time on the same node, to
			 * see if the source of the problem can be identified.
			 */
			scds_syslog(LOG_ERR, "Error parsing stopstate file "
				"at line %d: %s.", lineno, line);
			rc = SCHA_ERR_INVAL;
			goto finish;
		}

		if (strcasecmp(state, "running") != 0) {
			/*
			 * If the physical node was not running we
			 * don't care if the resource that would start
			 * is running or not so skip it.
			 */
			continue;
		}

		scds_syslog_debug(DBG_LEVEL_HIGH, "HADB node %d was "
			"running, checking to see if associated "
			"SC node is running resource.", phys);

		scnode = get_sc_node_by_hadb_node(phys);
		if (scnode < 0) {
			scds_syslog(LOG_ERR, "Unable to determine "
				"Sun Cluster node for HADB node %d.", phys);
			rc = SCHA_ERR_INVAL;
			goto finish;
		}

		/*
		 * If the SC node is not pending or already online set
		 * rc to SCHA_ERR_DEPEND indicating that we are
		 * waiting for more nodes to come online and start
		 * running the resource.  We don't quit the loop early
		 * so that we can print messages for all the SC nodes
		 * that are needed.  This will cause us to print
		 * messages multiple times for the same SC node if it
		 * has multiple HADB nodes configured on it.
		 */
		if (sc_node[scnode].rgstate == SCHA_RGSTATE_ONLINE)
			scds_syslog_debug(DBG_LEVEL_HIGH,
				"SC node %d (HADB node %d) is online.",
				scnode, phys);

		else if (sc_node[scnode].rgstate ==
			SCHA_RGSTATE_PENDING_ONLINE)
			scds_syslog_debug(DBG_LEVEL_HIGH,
				"SC node %d (HADB node %d) is pending "
				"online.", scnode, phys);

		else {
			rc = SCHA_ERR_DEPEND;
			/*
			 * SCMSGS
			 * @explanation
			 * The specified Sun Cluster node must be running the
			 * HADB resource before the database can be started.
			 * @user_action
			 * Bring the HADB resource online on the specified Sun
			 * Cluster node.
			 */
			scds_syslog(LOG_NOTICE, "Will not start database.  "
				"Waiting for %s (HADB node %d) to start "
				"resource.", sc_node[scnode].sysname,
				phys);
		}

		free(role);
		free(state);
		role = NULL;	/* make sure we don't free() twice */
		state = NULL;
	}

	/*
	 * If we get here it means we successfully parsed though the
	 * stopstate file.  rc will be either SCHA_ERR_NOERR or
	 * SCHA_ERR_DEPEND.
	 *
	 * Follow through into finish: below.
	 */

finish:
	free(stopstate);
	free(role);
	free(state);

	if (in != NULL)
		(void) fclose(in);

	return (rc);
}

/*
 * Split a stopstate file line.
 *
 * Returns:
 *
 * SCHA_ERR_NOERR	No errors
 * SCHA_ERR_INVAL	Error
 *
 * The caller must free(3c) the strings role and state when done with
 * them.
 */
scha_err_t
split_stopstate_line(char *line, int *phys, int *logical, char **role,
	char **state)
{
	char	*tok;
	char	*copy;

	/* Setting to NULL lets us just free them in case of error */

	*role = NULL;
	*state = NULL;

	/*
	 * Copy the input so that we don't trash it by strtok()ing it.
	 */
	copy = strdup(line);
	if (copy == NULL)
		goto error;

	/* Get the physical node number */

	if ((tok = strtok(copy, ":")) == NULL)
		goto error;
	*phys = atoi(tok);
	if (*phys < 0)		/* make sure it makes sense */
		goto error;

	/* Get the logical node number */

	if ((tok = strtok(NULL, ":")) == NULL)
		goto error;
	*logical = atoi(tok);
	if (*logical < 0)	/* make sure it makes sense */
		goto error;

	/* Get noderole */

	if ((tok = strtok(NULL, ":")) == NULL)
		goto error;
	if ((*role = strdup(tok)) == NULL)
		goto error;

	/* Get nodestate */

	if ((tok = strtok(NULL, ":\n")) == NULL)
		goto error;
	if ((*state = strdup(tok)) == NULL)
		goto error;

	return (SCHA_ERR_NOERR);

error:
	free(*role);
	free(*state);
	*role = NULL;
	*state = NULL;
	return (SCHA_ERR_INVAL);
}

/*
 * Determine if it is time for autorecovery.
 *
 * The conditions for autorecovery are:
 *
 * 0 - We are the lowest numbered SC node configured in the RG's
 *     nodelist.
 * 1 - All SC nodes in the nodelist are online (this includes online:
 *     faulted) with the resource.
 * 2 - The database is stopped.
 * 3 - We've had 1 and 2 for start timeout seconds.
 * 4 - And finally Auto_recovery has been set to true.
 *
 * 0) is not really a condition, but we want only one node to do the
 * auto recovery.
 *
 * 1) means that if the database was able to be started somewhere it
 * would have been.
 *
 * 2) it doesn't make sense to recover the database if the database was
 * up now would it?
 *
 * 3) we want to make sure that someone (whoever has the stopstate
 * file) has had a chance to start the database.
 *
 * 4) since the auto recovery will wipe out the database we want to
 * make sure the user has specifically asked for it.  For common HADB
 * applications such as limited lifetime HTTP session state
 * persistence were the data is not particularly valuable it is often
 * the case that throwing out the old data is thinkable.
 *
 * We will return AUTORECOVER_YES if we should autorecover,
 * AUTORECOVER_NO if not, and AUTORECOVER_READY is we should but the
 * user has not asked for us to nuke his/her/it's data.  Ready allows
 * us to communicate to the caller that it may want to suggest turning
 * auto_recovery on.
 *
 * Note that the first time we are called we don't know if condition 3
 * has been met.  If conditions 1 and 2 are met we will remember that
 * and if we are called again and all three conditions are met we'll
 * return YES or READY.
 */

enum autorecovery_response
ready_for_autorecovery(scds_handle_t scds_handle)
{
	/*
	 * -1 indicates that we haven't started tracking condition 3
	 * yet.
	 */
	static int	start = -1;
	int		now, timeout;
	int		i;
	scha_err_t	rc;
	boolean_t	auto_recover;

	/*
	 * Are we the lowest numbered node?
	 */
	for (i = 1; i <= MAX_SC_NODES; i++) {
		/* Skip nodes not configured for the resource. */
		if (!sc_node[i].hadb)
			continue;

		if (strcasecmp(site_info.sysname, sc_node[i].sysname) != 0) {
			/* We are not the lowest numbered SC node. */
			scds_syslog_debug(DBG_LEVEL_LOW, "%s is not the "
				"lowest numbered node.  "
				"Will not attempt auto recovery.",
				site_info.sysname);
			/* Reset the clock in case it was started. */
			start = -1;
			return (AUTORECOVER_NO);
		} else {
			scds_syslog_debug(DBG_LEVEL_LOW, "%s is the lowest "
				"numbered node.  Continuing with auto "
				"recovery checks.", site_info.sysname);
			break;
		}
	}

	/*
	 * Is the database stopped?
	 */
	if (strcmp(site_info.status, DB_STOPPED) != 0) {
		scds_syslog_debug(DBG_LEVEL_LOW, "Database is not stopped.  "
			"Will not attempt auto recovery.");
		/* Reset the clock */
		start = -1;
		return (AUTORECOVER_NO);
	}

	/*
	 * OK, database is stopped, are all the nodes online?
	 */
	for (i = 1; i <= MAX_SC_NODES; i++) {
		/* Skip nodes not configured for the resource. */
		if (!sc_node[i].hadb)
			continue;

		if (!(sc_node[i].rgstate == SCHA_RGSTATE_ONLINE ||
		    sc_node[i].rgstate == SCHA_RGSTATE_ONLINE_FAULTED)) {
			/* Found a node not running the resource. */
			scds_syslog_debug(DBG_LEVEL_LOW, "%s is not online.  "
				"Will not attempt auto recovery.",
				sc_node[i].sysname);
			/* Reset the clock in case it was started. */
			start = -1;
			return (AUTORECOVER_NO);
		}
	}

	/*
	 * OK, got through all the nodes in the resource and they are
	 * all online, check to see if we have previously started the
	 * clock.
	 */
	if (start == -1) {
		scds_syslog_debug(DBG_LEVEL_LOW, "Starting the auto recovery "
			"clock.  Will not attempt auto recovery yet.");
		start = gettime();
		return (AUTORECOVER_NO);
	}

	/*
	 * Clock was started, is it time?
	 */
	now = gettime();
	timeout = scds_get_rs_start_timeout(scds_handle);

	if (now < start + timeout) {
		scds_syslog_debug(DBG_LEVEL_LOW, "%d seconds until start "
			"timeout elapses.  Will not attempt auto recovery "
			"yet.", start + timeout - now);
		return (AUTORECOVER_NO);
	}

	/*
	 * John Connor. It is time.
	 *
	 * Check to see if the user has asked for auto recovery.
	 *
	 * We check this last so that we can tell if we would be ready
	 * to recover, but the user has asked not to.  This allows the
	 * caller to suggest to the user that they can turn on auto
	 * recovery.
	 */
	rc = get_boolean_ext_property(scds_handle, "Auto_recovery",
		&auto_recover);
	if (rc != SCHA_ERR_NOERR) {
		/*
		 * Do not reset the clock, maybe a future retrieval
		 * will succeed?
		 *
		 * Message already logged.
		 */
		return (AUTORECOVER_NO);
	}

	if (auto_recover == B_FALSE) {
		scds_syslog_debug(DBG_LEVEL_LOW, "User has not requested "
			"auto recovery.  Will not attempt auto recovery");
		return (AUTORECOVER_READY);
	}

	/*
	 * If we get here it's time to attempt auto recovery."
	 */
	scds_syslog_debug(DBG_LEVEL_LOW, "All conditions met for auto "
		"recovery.");

	return (AUTORECOVER_YES);
}

scha_err_t
autorecover(scds_handle_t scds_handle)
{
	char		*cmd = NULL, *dbpasswordfile;
	scha_err_t	rc;
	int		err;

	/*
	 * Usually the dbpasswordfile is not needed so we need to get
	 * it ourselves.
	 */
	rc = get_string_ext_property(scds_handle,
		"DB_password_file", &dbpasswordfile);
	if (rc != SCHA_ERR_NOERR) {
		/* Message already logged */
		goto finish;
	}

	/*
	 * We redirect stderr because the hadbm script runs stty which
	 * when stdout is not a tty will print: "stty: : No such
	 * device or address" which ends up on the console twice every
	 * probe cycle.  A bug has been filed against hadbm.
	 */
	cmd = ds_string_format("%s clear --fast --yes --dbpasswordfile=%s %s"
		" 2>/dev/null",
		site_info.hadbm_cmd, dbpasswordfile, site_info.dbname);
	if (cmd == NULL) {
		rc = SCHA_ERR_NOMEM;
		goto finish;
	}

	/*
	 * Run the clear command.
	 */

	err = system(cmd);

	/* If system() itself had a failure. */
	if (err == -1) {
		scds_syslog(LOG_ERR, "Cannot Execute %s: %s.", cmd,
			strerror(errno));
		rc = SCHA_ERR_INVAL; /* as good as any other */
		goto finish;
	}

	/* If we didn't complete due to being signaled... */
	if (WIFSIGNALED((uint_t)err)) {
		scds_syslog(LOG_ERR, "%s failed to complete.", cmd);
		rc = SCHA_ERR_INVAL;
		goto finish;
	}

	/* If hadbm did not succeed. */
	if (WEXITSTATUS((uint_t)err) != 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * The specified command returned the specified exit code.
		 * @user_action
		 * Examine other syslog messages occurring around the same
		 * time on the same node, to see if the source of the problem
		 * can be identified.
		 */
		scds_syslog(LOG_ERR, "%s failed with exit code %d.", cmd,
			WEXITSTATUS((uint_t)err));
		rc = SCHA_ERR_INVAL;
		goto finish;
	}

	/*
	 * hadbm clear succeeded, check to see if an
	 * auto_recovery_command has been set and if so run it.
	 *
	 * Usually the auto_recovery_command is not needed so we need
	 * to get it ourselves.
	 */
	free(cmd);
	rc = get_string_ext_property(scds_handle,
		"Auto_recovery_command", &cmd);
	if (rc != SCHA_ERR_NOERR) {
		/* Message already logged */
		goto finish;
	}

	if (cmd == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * NULL value was specified for the extension property of the
		 * resource.
		 * @user_action
		 * For the property name check the syslog message. Any of the
		 * following situations might have occurred. Different user
		 * action is needed for these different scenarios. 1) If a new
		 * resource is created or updated, check whether the value of
		 * the extension property is empty. If it is, provide valid
		 * value using scrgadm(1M). 2) For all other cases, treat it
		 * as an Internal error. Contact your authorized Sun service
		 * provider.
		 */
		scds_syslog(LOG_ERR, "NULL value returned for the extension "
			"property %s.", "Auto_recovery_command");
		rc = SCHA_ERR_INVAL;
		goto finish;
	}

	/* If auto_recovery_command is not set skip this step. */
	if (cmd[0] == '\0') {
		/*
		 * SCMSGS
		 * @explanation
		 * Auto recovery was performed and the database was
		 * reinitialized by running the hadbm clear command. However
		 * the extension property auto_recovery_command was not set so
		 * no further recovery was attempted.
		 * @user_action
		 * HADB is up with a fresh database. Session stores will need
		 * to be recreated. In the future the auto_recovery_command
		 * extension property can be set to a command that can do this
		 * whenever autorecovery is performed.
		 */
		scds_syslog(LOG_INFO, "Auto_recovery_command is not set.");
		rc = SCHA_ERR_NOERR;
		goto finish;
	}

	/*
	 * Run the auto recovery command.
	 */

	err = system(cmd);

	/* If system() itself had a failure. */
	if (err == -1) {
		scds_syslog(LOG_ERR, "Cannot Execute %s: %s.", cmd,
			strerror(errno));
		rc = SCHA_ERR_INVAL; /* as good as any other */
		goto finish;
	}

	/* If we didn't complete due to being signaled... */
	if (WIFSIGNALED((uint_t)err)) {
		scds_syslog(LOG_ERR, "%s failed to complete.", cmd);
		rc = SCHA_ERR_INVAL;
		goto finish;
	}

	/* If hadbm did not succeed. */
	if (WEXITSTATUS((uint_t)err) != 0) {
		scds_syslog(LOG_ERR, "%s failed with exit code %d.", cmd,
			WEXITSTATUS((uint_t)err));
		rc = SCHA_ERR_INVAL;
		goto finish;
	}

finish:
	free(dbpasswordfile);
	free(cmd);

	return (rc);
}
