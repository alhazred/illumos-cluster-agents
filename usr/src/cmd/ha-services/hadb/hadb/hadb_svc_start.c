/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * hadb_svc_start.c - Start method for hadb
 */

#pragma ident	"@(#)hadb_svc_start.c	1.6	07/06/06 SMI"

#include <unistd.h>
#include <rgm/libdsdev.h>
#include "hadb.h"

/*
 * The start method for hadb. Does some sanity checks on
 * the resource settings then starts the hadb under PMF with
 * an action script.
 */

int
main(int argc, char *argv[])
{
	scds_handle_t	scds_handle;
	int		rc;
	char		*rgname, *rsname;

	/*
	 * Process all the arguments that have been passed to us from RGM
	 * and do some initialization for syslog
	 */

	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR) {
		return (1);
	}

	rsname = (char *)scds_get_resource_name(scds_handle);
	rgname = (char *)scds_get_resource_group_name(scds_handle);

	/*
	 * SCMSGS
	 * @explanation
	 * Sun Cluster is starting the specified application.
	 * @user_action
	 * This is an informational message, no user action is needed.
	 */
	scds_syslog(LOG_NOTICE, "Starting %s.", APP_NAME);

	/* Validate the configuration and if there is an error return back */
	rc = svc_validate(scds_handle, B_FALSE);

	if (rc != 0) {
		scds_syslog(LOG_ERR,
			"Failed to validate configuration.");
		goto error;
	}

	/*
	 * Populate the configuration information
	 */

	rc = get_hadb_config_info(scds_handle, B_FALSE);
	if (rc != SCHA_ERR_NOERR)
		goto error;

	/*
	 * Start the data service, if it fails return with an error
	 *
	 * svc_start() will start the local HADB nodes if the database
	 * is already running, but the local nodes are not running
	 * already.
	 */

	rc = svc_start(scds_handle);

	if (rc != 0) {
		goto error;
	}

	/*
	 * The individual local HADB may have been started, but we
	 * don't know yet if the database is running yet.
	 *
	 * Set the status to degraded for the moment.
	 */

	(void) scha_resource_setstatus(rsname, rgname,
		SCHA_RSSTATUS_DEGRADED,
		"Waiting for database to start.");

	/*
	 * Attempt to start the database.
	 *
	 * If the database is not able to be started right now do not
	 * count it as an error, not enough SC nodes may be running
	 * right now to support the HADB site.
	 *
	 * Even if we encountered a genuine error we want to ignore it
	 * and allow the resource to start in a degraded state, the
	 * probe may be able to start the database.
	 *
	 * For example if the database start was attempted but the
	 * hadbm command dies for some reason the stopstate file will
	 * still exist and a future start attempt will succeed.
	 */

	(void) svc_start_database(scds_handle);

	/*
	 * At this point the database may have been started as part of
	 * the current reconfiguration or it may not because enough SC
	 * nodes may not be online.
	 *
	 * If the database was started and it was started on this node
	 * the database is up because the start command does not
	 * return until the database is up (assuming the command
	 * succeeded).  We'll update the HADB status in a moment and
	 * based on that we'll update the resource status message.
	 *
	 * If the database is starting but not on this node because we
	 * were not the owner of the stopstate file we returned from
	 * svc_start_database() right away.  We could wait in the
	 * start method for the database to start, but we don't want
	 * to do this because we don't have a way to know if the
	 * database is actually starting, to do that we would need to
	 * know the contents of the stopstate file.
	 *
	 * So we could end up waiting for a database that will not be
	 * starting in the current reconfiguration.  By waiting in
	 * that case we would just prolong the reconfiguration for no
	 * reason, preventing other SC nodes from starting the
	 * resource since RGM will not allow another reconfig while
	 * this one is still running.
	 *
	 * So instead of waiting on these "non starter" SC nodes we'll
	 * let hadb_probe update the resource status message.
	 *
	 * Next thing to do is to update the config/status information
	 * and then use that information to update the resource
	 * status.
	 */

	/*
	 * SCMSGS
	 * @explanation
	 * The HADB START method is updating it's configuration information.
	 * @user_action
	 * This is an informational message, no user action is needed.
	 */
	scds_syslog(LOG_INFO, "Updating configuration information.");
	(void) update_hadb_config_info(scds_handle, B_FALSE);
	update_resource_status(scds_handle);

	/*
	 * Now that the database may be up we can validate that the
	 * user did not put mirror nodes on the same SC node.
	 *
	 * This will look at the updated status information gathered
	 * by the update_hadb_config_info().
	 */
	if (database_running(scds_handle))
		(void) validate_hadb_node_config(scds_handle);

	/* Free up the Environment resources that were allocated */
	scds_close(&scds_handle);

	return (0);

error:
	(void) scha_resource_setstatus(rsname, rgname,
		SCHA_RSSTATUS_FAULTED,
		"Failed to start Sun ONE HADB.");

	/* Free up the Environment resources that were allocated */
	scds_close(&scds_handle);

	return (rc);
}
