/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2003 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_HADB_H
#define	_HADB_H

#pragma ident	"@(#)hadb.h	1.5	07/06/06 SMI"

#include <limits.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Debug levels for error messages */
#define	DBG_LEVEL_HIGH		9
#define	DBG_LEVEL_MED		5
#define	DBG_LEVEL_LOW		1

#define	SCDS_CMD_SIZE		(8 * 1024)

#define	SCDS_ARRAY_SIZE		1024

/*
 * Application name defined for use in scds_syslog messages.
 * Using this allows identical messages to be defined in more
 * than one data service, hashing to one message explanation.
 */

#define	APP_NAME	"Sun ONE HADB"

/*
 * The maximum number of Sun Cluster nodes.
 */

#define	MAX_SC_NODES		64

/*
 * The maximum number of HADB nodes per system.
 *
 * This is arbitrary, though you would be unlikely to have more HADB
 * nodes per system then you had CPUs.  (see also: C Programmer's
 * Disease)
 */

#define	MAX_HADB_NODES_PER_SYS	100

/*
 * How long to wait for final stop attempt.  SC3.1 supports passing -1
 * to mean wait forever.  In SC3.0 we need to settle for INT_MAX or
 * approximately 68 years.
 *
 * "Forever" is of course until RGM times out the stop method.
 */

#define	FINAL_STOP_TIMEOUT	INT_MAX

/*
 * How many seconds to wait after the start method has started the
 * HADB nodes before attempting to start the database.
 *
 * This is for when the resource is being started on all the SC nodes
 * at the same time.  Pausing will allow slow systems to "catch up"
 * with the others.
 *
 * If the pause isn't sufficient the probe will also try to start the
 * database if we are unable to do so from the start method.
 */

#define	PAUSE_FOR_NODE_ONLINE	3

/*
 * The percentage of the start timeout that we will wait (in
 * svc_wait()) for the database to come online.
 *
 * For example with this set to 80, if we get within 20% of timing out
 * in the start method we will stop waiting for the database to come
 * online.
 */

#define	WAIT_FOR_DB_ONLINE_PCT	80

/*
 * Database states.  These are the strings printed by hadbm status.
 *
 * DB_SC_UNKNOWN is the state if the state printed by hadbm is not
 * recognized.  This is different from DB_UNKNOWN which is a state
 * that the DB can actually be in.
 *
 * DB_SC_ERROR is the state if we encounter an error while trying to
 * determine the state.
 *
 * DB_SC_UNKNOWN and DB_SC_ERROR strings begin with a underscore to
 * differentiate them from legitimate state strings that hadbm might
 * print.
 *
 * HAFaultTolerant is different from FaultTolerant in that HA means
 * that each DRU has an available spare.
 *
 * The DB and node states are specified in:
 * HADB Admin CLI Functional Specification: Version 0.4, 2003-07-03.
 */

#define	DB_SC_UNKNOWN		"_SC_Unknown"
#define	DB_SC_ERROR		"_SC_Error"
#define	DB_HA_FAULT_TOLERANT	"HAFaultTolerant"
#define	DB_FAULT_TOLERANT	"FaultTolerant"
#define	DB_STOPPED		"Stopped"
#define	DB_OPERATIONAL		"Operational"
#define	DB_NON_OPERATIONAL	"NonOperational"
#define	DB_UNKNOWN		"Unknown"

/*
 * Node states, as printed by hadbm status --nodes.
 */

#define	NODE_SC_UNKNOWN		"_SC_Unknown"
#define	NODE_SC_ERROR		"_SC_Error"
#define	NODE_WAITING		"Waiting"
#define	NODE_RUNNING		"Running"
#define	NODE_STOPPING		"Stopping"
#define	NODE_STOPPED		"Stopped"
#define	NODE_RECOVERING		"Recovering"
#define	NODE_REPAIRING		"Repairing"
#define	NODE_STARTING		"Starting"

/*
 * Return possibilities from ready_for_autorecovery().  Ready means
 * that the conditions are right for autorecovery but the
 * Auto_recovery property is false.
 */

enum autorecovery_response {
	AUTORECOVER_YES,
	AUTORECOVER_READY,
	AUTORECOVER_NO
};

/*
 * "Settle" times for starting and stopping so that the stop or start
 * methods running at the same time get a consistent view of what the
 * other nodes in the RG are doing.
 *
 * Without these settling times a scswitch -F might cause node 1 might
 * see:
 *
 *	pdixon1		Pending Offline
 *	pdixon2		Offline
 *
 * and node 2 sees:
 *
 *	pdixon1		Pending Offline
 *	pdixon2		Pending Offline
 *
 * With the settle times all nodes will see "Pending Offline" for all
 * nodes.
 *
 * The stop_timeout property setting needs to take these sleep times
 * into account.
 */

#define	PRE_RG_SETTLE_TIME	1
#define	POST_RG_SETTLE_TIME	1

/*
 * The time in seconds when the method or current process started.
 * Initialized by a call to gettime() (see below) at the start of the
 * method.
 */

extern int start_time;

/*
 * Information on individual hadb nodes.  Each SC node may be running
 * multiple HADB nodes for the same site.
 */

struct hadb_node_info {
	int	nodeno;		/* >= 0 if it exists */
	int	mirror;		/* mirror node, == -1 if not known */
	char	*nodestate;
};

/*
 * Struct for the global sc_node_info array.  We index by the SC node
 * ids which are 1 based.  Just ignore index 0 (zero).  Nodes that do
 * not exist will have a sysname equal to NULL.
 *
 * The resource state is only populated while stopping the resource,
 * and then only when it's stopping because the resource is being
 * disabled.
 */

struct sc_node_info {
	char		*sysname;
	char		*privname; /* clusternode[1234...]-priv */
	boolean_t	hadb;	/* true if a potential master */
	scha_rgstate_t	rgstate;
	scha_rsstate_t	rsstate;

	struct hadb_node_info	hadb_node[MAX_HADB_NODES_PER_SYS];
};

struct site_info {
	char		*status; /* The database status */

	/*
	 * The disabled flag will be set to true during the STOP
	 * method if the resource is being disabled.
	 */
	boolean_t	disabled;

	/*
	 * If an auto recovery is attempted but it fails this flag
	 * will be set to true.
	 */
	boolean_t	auto_recover_failed;

	/*
	 * If an individual HADB node startup encounters an error this
	 * flag will be set to true.  If it is later started
	 * successfully this flag will be reset to false.
	 */
	boolean_t startnode_error;

	char		*sysname;
	int		scnodeid;

	uint_t		n_rg_nodes;
	uint_t		n_rg_online;
	uint_t		n_rg_pending_online;

	char		*hadb_root;
	char		*dbname;
	char		*confdir;

	char		*hadbm_cmd;
};

extern struct sc_node_info sc_node[MAX_SC_NODES+1];
extern struct site_info site_info;

/*
 * gettime() returns the current time. Time is expressed as seconds
 * since some arbitrary time in the past; it is not correlated in any
 * way to the time of day, and thus is not subject to resetting or
 * drifting by way of adjtime(2) or settimeofday(3C).
 */

int gettime(void);

int svc_validate(scds_handle_t scds_handle, boolean_t print_messages);

int svc_start(scds_handle_t scds_handle);

int svc_start_database(scds_handle_t scds_handle);

int svc_stop(scds_handle_t scds_handle);

int svc_wait(scds_handle_t scds_handle);

int mon_start(scds_handle_t scds_handle);

int mon_stop(scds_handle_t scds_handle);

/*
 * HADB utility routines.
 */

scha_err_t get_hadb_node_info(scds_handle_t scds_handle, int print);
scha_err_t update_hadb_config_info(scds_handle_t scds_handle, int print);

scha_err_t get_hadb_config_info(scds_handle_t handle, int print);
scha_err_t get_hadb_status(scds_handle_t scds_handle, char **status);

scha_err_t update_rg_state_info(scds_handle_t scds_handle);

scha_err_t validate_hadb_node_config(scds_handle_t scds_handle);

boolean_t database_running(scds_handle_t scds_handle);
boolean_t all_local_nodes_running(scds_handle_t scds_handle);

void update_resource_status(scds_handle_t handle);

enum autorecovery_response ready_for_autorecovery(scds_handle_t scds_handle);
scha_err_t autorecover(scds_handle_t scds_handle);

#ifdef __cplusplus
}
#endif

#endif /* _HADB_H */
