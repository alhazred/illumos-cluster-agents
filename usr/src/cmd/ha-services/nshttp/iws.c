/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * iws.c - Common utilities for highly available iws
 */

#pragma ident	"@(#)iws.c	1.60	08/06/17 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <unistd.h>
#include <errno.h>
#include <sys/stat.h>
#include <libintl.h>
#include <rgm/libdsdev.h>
#include <ds_common.h>
#include "iws.h"

/*
 * The initial timeout allowed  for the dataservice to
 * be fully up and running.
 */
#define	SVC_WAIT_PCT		2

/*
 * We need to use 95% of probe_timeout to connect to the port and the
 * remaining time is used to disconnect from port in the svc_probe function.
 */
#define	SVC_CONNECT_TIMEOUT_PCT		95

/*
 * We need to wait for SVC_WAIT_TIME ( 5 secs) for pmf
 * to send the failure message before probing the service
 */

#define	SVC_WAIT_TIME		5

/*
 * This value will be used as disconnect timeout, if there is no
 * time left from the probe_timeout.
 */

#define	SVC_DISCONNECT_TIMEOUT_SECONDS		2

static scha_err_t scds_ns_is_secure(char *filename, boolean_t *rc);
static int remove_pidlog(const char *magnus_filename);
static int validate_monitor_uri_list(scds_handle_t scds_handle,
    scds_net_resource_list_t *snrlp, boolean_t print_messages);
static int validate_server_url(char *uri, scds_net_resource_list_t *snrlp,
    boolean_t print_messages);
boolean_t pre_webserver7[SCDS_ARRAY_SIZE];
/*
 * svc_validate():
 * Do iws specific validation of the resource configration.
 * Called by start/validate/update/monitor methods.
 * Return 0 on success, > 0 on failures.
 *
 * svc_validate will check for the following
 * 1. Confdir_list
 * 2. Port_list
 * 3. Extension properties
 * 4. Logical hostname resources
 * 5. Executable permissions, if filesystem is mounted on this node
 * 6. Keypass file
 */

int
svc_validate(scds_handle_t scds_handle, boolean_t print_messages)
{
	char	iws_dir[SCDS_ARRAY_SIZE];
	char	iws_config[SCDS_ARRAY_SIZE];
	char	iws_httpd[SCDS_ARRAY_SIZE];
	int	rc = 0, i = 0, err = SCHA_ERR_NOERR;
	scha_str_array_t *confdirs = NULL;
	scds_net_resource_list_t *snrlp = NULL;
	scds_port_list_t *portlist = NULL;
	boolean_t *se_secure = NULL;
	struct stat statbuf;
	scds_hasp_status_t hasp_status;
	int check_executable;

	/* Get Confdir_list extension property */
	confdirs = scds_get_ext_confdir_list(scds_handle);

	/* Return an error if there are no config_dirs x prop */
	if (confdirs == NULL || confdirs->array_cnt == 0) {
		scds_syslog(LOG_ERR, "Property %s is not set.", "Confdir_list");
		if (print_messages) {
			(void) fprintf(stderr, gettext("Property %s is not "
				"set.\n"), "Confdir_list");
		}
		return (1);
	}

	if (scds_get_rs_scalable(scds_handle) == B_FALSE) {
		/* failover case, must have exactly one confdir */
		if (confdirs->array_cnt != 1) {
			/*
			 * SCMSGS
			 * @explanation
			 * Failover data services must have one and only one
			 * value for Confdir_list.
			 * @user_action
			 * Create a failover resource group for each
			 * configuration file.
			 */
			scds_syslog(LOG_ERR,
				"Failover %s data services must have exactly "
				"one value for extension property %s.",
				APP_NAME, "Confdir_list");
			if (print_messages) {
				(void) fprintf(stderr, gettext("Failover %s "
					"data services must have exactly one "
					"value for extension property %s.\n"),
					APP_NAME, "Confdir_list");
			}
			return (1);
		}
	}

	/* Check that Confdir_list[0] is an absolute path. */
	if (confdirs->str_array[0][0] != '/') {
		scds_syslog(LOG_ERR,
			"Confdir_list must be an absolute path.");
		if (print_messages) {
			(void) fprintf(stderr, gettext("Confdir_list must "
				"be an absolute path."));
		}
		return (1);
	}

	/* Network aware service should have at least one port specified */

	err = scds_get_port_list(scds_handle, &portlist);
	if (err != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
			"Failed to retrieve the property %s: %s.",
			"Port_list", scds_error_string(err));
		if (print_messages) {
			(void) fprintf(stderr, gettext("Failed to retrieve "
				"the property %s: %s.\n"), "Port_list",
				gettext(scds_error_string(err)));
		}
		goto finished_validate;
	}

	if (portlist == NULL || portlist->num_ports < 1) {
		scds_syslog(LOG_ERR, "Property %s is not set.", "Port_list");
		if (print_messages) {
			(void) fprintf(stderr, gettext("Property %s is not "
				"set.\n"), "Port_list");
		}
		err = 1;
		goto finished_validate;
	}

	/* Check if Confdir_list and Port_list are 1-1 mapping */
	if (confdirs->array_cnt != (unsigned int) portlist->num_ports) {
		/*
		 * SCMSGS
		 * @explanation
		 * The Confdir_list and Port_list properties must contain the
		 * same number of entries, thus maintaining a 1-1 mapping
		 * between the two.
		 * @user_action
		 * Using the appropriate scrgadm command, configure this
		 * resource to contain the same number of entries in the
		 * Confdir_list and the Port_list properties.
		 */
		scds_syslog(LOG_ERR,
			"Elements in Confdir_list and Port_list must be "
			"1-1 mapping.");
		if (print_messages) {
			(void) fprintf(stderr, gettext("Elements in "
				"Confdir_list and Port_list must be 1-1 "
				"mapping.\n"));
		}
		err = 1;
		goto finished_validate;
	}

	/* Check to make sure other important extension props are set */
	if (scds_get_ext_monitor_retry_count(scds_handle) <= 0) {
		scds_syslog(LOG_ERR,
			"Property %s is not set.",
			"Monitor_retry_count");
		if (print_messages) {
			(void) fprintf(stderr, gettext("Property %s is not "
				"set.\n"), "Monitor_retry_count");
		}
		err = 1;
		goto finished_validate;
	}
	if (scds_get_ext_monitor_retry_interval(scds_handle) <= 0) {
		scds_syslog(LOG_ERR,
			"Property %s is not set.",
			"Monitor_retry_interval");
		if (print_messages) {
			(void) fprintf(stderr, gettext("Property %s is not "
				"set.\n"), "Monitor_retry_interval");
		}
		err = 1;
		goto finished_validate;
	}
	if (scds_get_ext_probe_timeout(scds_handle) <= 0) {
		scds_syslog(LOG_ERR,
			"Property %s is not set.",
			"Probe_timeout");
		if (print_messages) {
			(void) fprintf(stderr, gettext("Property %s is not "
				"set.\n"), "Probe_timeout");
		}
		err = 1;
		goto finished_validate;
	}

	/*
	 * Return an error if there is an error when trying to get the
	 * available network address resources for this resource
	 */
	if ((err = scds_get_rs_hostnames(scds_handle, &snrlp))
		!= SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
			"Error in trying to access the configured network "
			"resources : %s.", scds_error_string(err));
		if (print_messages) {
			(void) fprintf(stderr, gettext("Error in trying to "
				"access the configured network resources "
				": %s.\n"), gettext(scds_error_string(err)));
		}
		goto finished_validate;
	}

	/* Return an error if there are no network address resources */
	if (snrlp == NULL || snrlp->num_netresources == 0) {
		scds_syslog(LOG_ERR,
			"No network address resource in resource group.");
		if (print_messages) {
			(void) fprintf(stderr, gettext("No network address "
				"resource in resource group.\n"));
		}
		err = 1;
		goto finished_validate;
	}

	/* check for HAStoragePlus resources */
	rc = scds_hasp_check(scds_handle, &hasp_status);
	if (rc != SCHA_ERR_NOERR) {
		/* scha_hasp_check() logs everytime it fails */
		if (print_messages) {
			(void) fprintf(stderr, gettext("INTERNAL ERROR: %s.\n"),
				gettext("scds_hasp_check failed"));
		}
		err = 1;
		goto finished_validate;
	}

	if (hasp_status == SCDS_HASP_NOT_ONLINE) {
		scds_syslog(LOG_ERR, "Resource depends on a "
			"SUNW.HAStoragePlus type resource that is "
			"not online anywhere.");
		if (print_messages) {
			(void) fprintf(stderr, gettext("Resource depends on a "
				"SUNW.HAStoragePlus type resource that is "
				"not online anywhere.\n"));
		}
		err = 1;
		goto finished_validate;
	} else if (hasp_status == SCDS_HASP_ERR_CONFIG) {
		/* problem syslogged by scds_hasp_check */
		if (print_messages) {
			(void) fprintf(stderr, gettext("This resource depends "
				"on a HAStoragePlus resouce that is in a "
				"different Resource Group. This configuration "
				"is not supported.\n"));
		}
		err = 1;
		goto finished_validate;
	}

	for (i = 0; i < (int)confdirs->array_cnt; i++) {
		/*
		 * XXX Check to see if the iws-stats URL mapping has been
		 * enabled in the configuration file. We prefer to have it.
		 * Make sure that the PORT setting in the resource settings
		 * look OK as per the config file. Check if SSL has been enabled
		 */

		/*
		 * Construct the pathname to the iws start script
		 * The following code change is to accomodate the new
		 * directory structure that has been introduced in
		 * the Sun One Web Server 7.0 version. This checks
		 * to see if the start script exists in the instance directory
		 * else if startserv script is present in instance/bin directory
		 * in order to check the version.
		 */
		pre_webserver7[i] = B_TRUE;
		(void) sprintf(iws_httpd, "%s/start",
				confdirs->str_array[i]);
		/*
		 * Check to see if the version is 6.1. If stat fails,
		 * it could be because the files are not accessible.This
		 * could be because either the hasp resource not
		 * online on the local node or 7.0 version is installed.
		 * If hasp resource is not online on the
		 * local node,do not check for any validations.
		 * Else, it could be because 7.0 web server version is
		 * installed.
		 * Throw an error message in case
		 * the files are not existing.
		 */
		if (stat(iws_httpd, &statbuf) != 0) {
			if ((hasp_status == SCDS_HASP_ONLINE_NOT_LOCAL) &&
				(errno == ENOENT)) {	/* lint !e746 */
				check_executable = 0;
			} else {
			    (void) sprintf(iws_httpd, "%s/bin/startserv",
				confdirs->str_array[i]);
				if (stat(iws_httpd, &statbuf) != 0) {
					rc = errno;
					/*
					 * SCMSGS
					 * @explanation
					 * Need explanation of this message!
					 * @user_action
					 * Need a user action for this
					 * message.
					 */
					scds_syslog(LOG_ERR,
						"Cannot access "
						"start script %s "
						"or %s/start: %s",
						iws_httpd,
						confdirs->str_array[i],
						strerror(rc));
					if (print_messages) {
						(void) fprintf(stderr,
						gettext("Cannot access start "
						"script %s or %s/start: %s\n"),
						iws_httpd,
						confdirs->str_array[i],
						gettext(strerror(rc)));
					}
					err = 1;
					goto finished_validate;
				}
			pre_webserver7[i] = B_FALSE;
			}
		}

		if (check_executable) {
			/* check that the binary is executable */
			if ((statbuf.st_mode & S_IXUSR) != S_IXUSR) {
				scds_syslog(LOG_ERR,
					"Incorrect permissions set for %s.",
					iws_httpd);
				if (print_messages) {
					(void) fprintf(stderr,
						gettext("Incorrect permissions "
						"set for %s.\n"), iws_httpd);
				}
				err = 1;
				goto finished_validate;
			}
		}

		rc = snprintf(iws_dir, SCDS_ARRAY_SIZE,
			"%s/config", confdirs->str_array[i]);

		if (rc == -1) {
			scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
				"String handling error creating path to "
				"configuration file. "
				"The path may be too long");
			if (print_messages) {
				(void) fprintf(stderr, gettext("INTERNAL "
					"ERROR: %s.\n"), gettext("String "
					"handling error creating path to "
					"configuration file. "
					"The path may be too long"));
			}
			err = 1;
			goto finished_validate;
		}

		/*
		 * Check to see if the iws configuration file is in the
		 * right place
		 */
		rc = snprintf(iws_config, sizeof (iws_config),
			"%s/magnus.conf", iws_dir);
		if (rc == -1) {
			scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
				"String handling error creating path to "
				"configuration file. "
				"The path may be too long");
			if (print_messages) {
				(void) fprintf(stderr, gettext("INTERNAL "
					"ERROR: %s.\n"), gettext("String "
					"handling error creating path to "
					"configuration file. "
					"The path may be too long"));
			}
			err = 1;
			goto finished_validate;
		}

		if (stat(iws_config, &statbuf) != 0) {
			if ((hasp_status == SCDS_HASP_ONLINE_NOT_LOCAL) &&
				(errno == ENOENT)) {
				/* not all config files are available here */
				break;
			} else {
				rc = errno;
				scds_syslog(LOG_ERR,
				"File %s is not readable: %s.", iws_config,
					strerror(rc));
				if (print_messages) {
					(void) fprintf(stderr, gettext("File "
						"%s is not readable: %s.\n"),
						iws_config,
						gettext(strerror(rc)));
				}
				err = 1;
				goto finished_validate;
			}
		}
	}

	/* do not check for keypass file if we broke out of the loop */
	if (i == (int)confdirs->array_cnt) {
		if (get_secure_ports(scds_handle, &se_secure)
			!= SCHA_ERR_NOERR) {
			err = 1;
			goto finished_validate;
		}

		for (i = 0; i < (int)confdirs->array_cnt; i++) {
			/* Verify that password file is readable */
			if (se_secure[i]) {
				rc = snprintf(iws_config, SCDS_ARRAY_SIZE,
					"%s/keypass", confdirs->str_array[i]);
				if (rc == -1) {
					scds_syslog(LOG_ERR,
						"INTERNAL ERROR: %s.",
						"String handling error "
						"creating path to keypass "
						"file. "
						"The path may be too long");
					if (print_messages) {
						(void) fprintf(stderr,
							gettext("INTERNAL "
							"ERROR: %s.\n"),
							gettext("String "
							"handling error "
							"creating path to "
							"keypass file. The "
							"path may be too "
							"long"));
					}
					err = 1;
					goto finished_validate;
				}
				if (stat(iws_config, &statbuf) != 0) {
					rc = errno;
					/*
					 * SCMSGS
					 * @explanation
					 * For the secure server to run, a
					 * password file named keypass is
					 * required. This file could not be
					 * read, which resulted in an error
					 * when trying to start the Data
					 * Service.
					 * @user_action
					 * Create the keypass file and place
					 * it under the Confdir_list path for
					 * this resource. Make sure that the
					 * file is readable.
					 */
					scds_syslog(LOG_ERR,
					"Password file %s is not readable: %s",
					iws_config, strerror(rc));
					if (print_messages) {
						(void) fprintf(stderr,
							gettext("Password file "
							"%s is not readable: "
							"%s\n"), iws_config,
							gettext(strerror(rc)));
					}
					err = 1;
					goto finished_validate;
				}
			}
		}
	}

	/* Make sure that URIs provided (if any) look OK */
	if (validate_monitor_uri_list(scds_handle, snrlp, print_messages) !=
	    0) {
		err = 1;
		goto finished_validate;
	}

finished_validate:
	if (snrlp)
		scds_free_net_list(snrlp);
	if (portlist)
		scds_free_port_list(portlist);
	if (se_secure)
		free(se_secure);

	return (err);
}


/*
 * svc_start():
 */

int
svc_start(scds_handle_t scds_handle, boolean_t *issecure)
{
	char	iws_httpd[SCDS_ARRAY_SIZE];
	char	iws_passwd[SCDS_ARRAY_SIZE];
	char	cmd[SCDS_ARRAY_SIZE];
	char	magnus_file[SCDS_ARRAY_SIZE];
	char	*rsname = NULL, *rgname = NULL;
	char	msg[SCDS_ARRAY_SIZE];
	int	err = SCHA_ERR_NOERR, indx, rc;
	struct stat statbuf;
	scha_str_array_t *confdirs = NULL;

	scds_syslog_debug(DBG_LEVEL_HIGH,
		"Calling START method for resource %s.",
		scds_get_resource_name(scds_handle));

	/* Get resource and resource group names */
	rsname = (char *)scds_get_resource_name(scds_handle);
	rgname = (char *)scds_get_resource_group_name(scds_handle);

	/* Initialize message */
	(void) sprintf(msg, "Failed to start %s.", APP_NAME);

	/* Get Confdir_list extension property */
	confdirs = scds_get_ext_confdir_list(scds_handle);

	/* Return an error if there are no config_dirs x prop */
	if (confdirs == NULL || confdirs->array_cnt == 0) {
		scds_syslog(LOG_ERR,
			"Property %s is not set.",
			"Confdir_list");
		return (1);
	}

	for (indx = 0; indx < (int)confdirs->array_cnt; indx++) {

		/*
		 * The following code change has been made to accomodate
		 * the new directory structure that has been
		 * introduced in Sun One Web Server 7.0 version. This checks
		 * if start script is present in the instance directory or
		 * instance/bin directory to check the version.
		 */
		if (pre_webserver7[indx])
			(void) sprintf(iws_httpd, "%s/start",
				confdirs->str_array[indx]);
		else
			(void) sprintf(iws_httpd, "%s/bin/startserv",
				confdirs->str_array[indx]);

		if (issecure[indx] == B_TRUE) {
		/*
		 * 4234453 -- must provide password in secure mode.
		 * The current implementation assumes there is a file
		 * that stores the password located at <config-dir>/keypass
		 * and that file is used for stdin when starting the http
		 * server in secure mode.
		 */
			(void) sprintf(iws_passwd, "%s/keypass",
				confdirs->str_array[indx]);

			/* Must use extra layer of shell to redirect stdin */
			(void) sprintf(cmd,
				"/bin/sh -c \"%s < %s 2>/dev/null\"",
				iws_httpd, iws_passwd);

			scds_syslog_debug(DBG_LEVEL_LOW,
				"Starting server in secure mode "
				"with command %s\n", cmd);
		} else {
			/* Now construct the command to start iws */
			/*
			 * The string in the sprintf below is:
			 * /bin/sh -c "%s 2>/dev/null"
			 * but the \042 is used in place of the double quotes
			 * to pass cstyle checking.
			 */
			(void) sprintf(cmd, "/bin/sh -c \042%s 2>/dev/null\042",
				iws_httpd);

			scds_syslog_debug(DBG_LEVEL_LOW,
				"Starting server with command %s\n",
				cmd);
		}

		/*
		 * Remove the PidLog file if need be.
		 *
		 * Problems with finding magnus.conf or removing the
		 * PidLog are logged but otherwise ignored.  We just
		 * let the web server manage the best that it can
		 * because bubbling up the error when we're starting
		 * several instances from Confdir_list is difficult to
		 * do cleanly and consistently with how we deal with
		 * scds_pmf_start() problems.
		 *
		 * The following modification has been done for backward
		 * compatibility of Web Server6.1.Since the pid file location
		 * in Web Server 7.0 version is no longer available in
		 * magnus.conf file,we will leave it for the
		 * web server to handle.
		 */
		if (pre_webserver7[indx]) {
		/*
		 * If the webserver version is prior to 7.0,
		 * then search for magnus file.
		 */
			rc = snprintf(magnus_file, sizeof (magnus_file),
				"%s/config/magnus.conf",
				confdirs->str_array[indx]);
			if (rc == -1) {
				scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
					"String handling error creating "
					"path to magnus.conf. "
					"The path may be too long");
			} else
				(void) remove_pidlog(magnus_file);

		}

		err = scds_pmf_start(scds_handle, SCDS_PMF_TYPE_SVC,
			indx, cmd, -1);
		if (err == SCHA_ERR_NOERR) {
			(void) scha_resource_setstatus(rsname, rgname,
				SCHA_RSSTATUS_OK,
				"Completed successfully.");
		} else {
			(void) scha_resource_setstatus(rsname, rgname,
				SCHA_RSSTATUS_FAULTED, msg);
		}

	} /* for loop */

	return (err);
}


/*
 * iws_svc_start() calls svc_wait() just after it calls svc_start()
 * and before it returns.  svc_start() starts up the application (Sun Java
 * System web server), but does not wait for the application to complete
 * coming up before returning.
 *
 * The RGM framework specifies that the START method should not return until
 * the application is up.  svc_wait() verifies that the application is
 * up before it returns.  It does this by probing the application across all
 * its port/ip combinations.  When the probing is successful, svc_wait()
 * returns immediately indicating success.  When the probing is unsuccessful
 * the START method may eventually time out.
 *
 * To avoid probing too early, we wait for SVC_WAIT_PCT percentage of the
 * start method timeout value before starting to probe.
 *
 * Returns: 0=probing succeeded, application is up
 *	  1=unsuccessful probe, application wasn't
 *		determined to be up.
 */

int
svc_wait(scds_handle_t scds_handle, boolean_t *se_secure)
{
	int		svc_start_timeout, probe_timeout,
			probe_result, i;
	scha_err_t	err = SCHA_ERR_NOERR;
	scds_netaddr_list_t *netaddr = NULL;
	scha_extprop_value_t *urilist = NULL;
	scha_str_array_t *uris = NULL;

	/* obtain the network resource to use for probing */
	err = scds_get_netaddr_list(scds_handle, &netaddr);
	if (err != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
			"Error in trying to access the "
			"configured network resources : %s.",
			scds_error_string(err));
		return (1);
	}

	/* Return an error if there are no network resources */
	if (netaddr == NULL || netaddr->num_netaddrs == 0) {
		scds_syslog(LOG_ERR,
			"No network address resource in resource group.");
		return (1);
	}

	/* Retrieve the list of uris that we were told to probe */
	err = scds_get_ext_property(scds_handle, "Monitor_Uri_List",
			SCHA_PTYPE_STRINGARRAY, &urilist);
	if ((err != SCHA_ERR_NOERR) && (err != SCHA_ERR_PROP)) {
		/* failed with something other than SCHA_ERR_PROP */
		scds_syslog(LOG_ERR,
				"Failed to retrieve the "
				"property %s: %s.",
				"Monitor_Uri_List", scds_error_string(err));
		scds_free_netaddr_list(netaddr);
		return (1);
	}
	if (urilist != NULL) {
		uris = urilist->val.val_strarray;
	}

	/* Get the Start method timeout and the Probe timeout value */
	svc_start_timeout = scds_get_rs_start_timeout(scds_handle);
	probe_timeout = scds_get_ext_probe_timeout(scds_handle);

	/*
	 * sleep for SVC_WAIT_PCT percentage of start_timeout time
	 * before actually probing the dataservice. This is to allow
	 * the dataservice to be fully up inorder to reply to the
	 * probe.
	 */
	if (scds_svc_wait(scds_handle,
		(svc_start_timeout * SVC_WAIT_PCT)/100)
		!= SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to start %s.", APP_NAME);
		scds_free_ext_property(urilist);
		scds_free_netaddr_list(netaddr);
		return (1);
	}

	do {
		scds_syslog_debug(DBG_LEVEL_HIGH,
			"Probing service, t = %d\n", time(NULL));

		scds_syslog(LOG_NOTICE, "Waiting for %s to come up.",
				"Web Server");

		/*
		 * Probe the data service on the logicalhostnames
		 * and the ports.
		 */
		probe_result = 0;
		for (i = 0; (i < netaddr->num_netaddrs) && (probe_result == 0);
		    i++) {
			scds_syslog_debug(DBG_LEVEL_LOW,
				"Performing %s probe of port %d.",
				se_secure[i]?"secure":"non-SSL",
				netaddr->netaddrs[i].port_proto.port);
			probe_result = svc_probe(scds_handle,
				netaddr->netaddrs[i].hostname,
				netaddr->netaddrs[i].port_proto.port,
				se_secure[i], probe_timeout,
				B_FALSE);
		}
		/* probe all uris (if any) that were supplied */
		if (uris) {
			for (i = 0; (i < (int)uris->array_cnt) &&
			    (probe_result == 0); i++) {
				probe_result += probe_uri(scds_handle,
				    uris->str_array[i], probe_timeout, B_TRUE);
			}
		}

		if (probe_result == SCHA_ERR_NOERR) {
			/* Success. Free up resources and return */
			scds_free_ext_property(urilist);
			scds_free_netaddr_list(netaddr);
			return (SCHA_ERR_NOERR);
		}

		/*
		 * Dataservice is still trying to come up. Sleep for a while
		 * before probing again.
		 */
		if (scds_svc_wait(scds_handle, SVC_WAIT_TIME)
			!= SCHA_ERR_NOERR) {
			scds_syslog(LOG_ERR,
				"Failed to start %s.", APP_NAME);
			scds_free_ext_property(urilist);
			scds_free_netaddr_list(netaddr);
			return (1);
		}

	/* We rely on RGM to timeout and terminate the program */
	} while (1);
}


/*
 * Stops the iws process using PMF
 * Return 0 on success, > 0 on failures.
 */

int
svc_stop(scds_handle_t scds_handle)
{
	int	rc = SCHA_ERR_NOERR, indx;
	char	*rsname = NULL, *rgname = NULL;
	char	msg_ok[SCDS_ARRAY_SIZE];
	char	msg_fail[SCDS_ARRAY_SIZE];
	scha_str_array_t *confdirs = NULL;
	struct stat statbuf;
	char stopcmd[SCDS_ARRAY_SIZE];
	int code;

	scds_syslog_debug(DBG_LEVEL_HIGH,
		"Calling STOP method for resource %s.",
		scds_get_resource_name(scds_handle));

	/* Get Confdir_list extension property */
	confdirs = scds_get_ext_confdir_list(scds_handle);

	/* Return an error if there are no config_dirs x prop */
	if (confdirs == NULL || confdirs->array_cnt == 0) {
		scds_syslog(LOG_ERR,
			"Property %s is not set.",
			"Confdir_list");
		return (1);
	}

	/* Get resource and resource group names */
	rsname = (char *)scds_get_resource_name(scds_handle);
	rgname = (char *)scds_get_resource_group_name(scds_handle);

	/* Initialize messages */
	(void) sprintf(msg_ok, "Successfully stopped %s.", APP_NAME);
	(void) sprintf(msg_fail, "Failed to stop %s.", APP_NAME);

	for (indx = 0; indx < (int)confdirs->array_cnt; indx++) {
		/*
		 * We prefer using the stop script supplied with iws
		 * but if its not available, then we silently fall back
		 * to sending a SIGTERM to the process tree. There is
		 * a possibility that SIGTERM will cause iws to terminate
		 * with a core dump.
		 *
		 * Note that we also fall back to signals if we
		 * cant take the process tree out of pmf monitoring.
		 * The following code change has been done to accomodate
		 * the new directory structure that has
		 * been introduced in Sun One Web Server 7.0 version.
		 * This checks to see if stop script is present in the
		 * instance directory else if it's present in the
		 * instance/bin directory.
		 */
		(void) snprintf(stopcmd, sizeof (stopcmd), "%s/stop",
			confdirs->str_array[indx]);
		if (stat(stopcmd, &statbuf) != 0) {
			(void) snprintf(stopcmd, sizeof (stopcmd),
				"%s/bin/stopserv",
				confdirs->str_array[indx]);
		}
			scds_syslog_debug(DBG_LEVEL_LOW, "The command "
				" used to stop Web Server is"
				"%s", stopcmd);


		if ((access(stopcmd, X_OK) == 0) &&
		    (scds_pmf_stop_monitoring(scds_handle, SCDS_PMF_TYPE_SVC,
		    indx) == SCHA_ERR_NOERR)) {
			scds_syslog_debug(DBG_LEVEL_HIGH, "Using %s to stop "
			    "the resource", stopcmd);
			code = -1;

			rc = scds_timerun(scds_handle, stopcmd,
			    (scds_get_rs_stop_timeout(scds_handle) * 8) / 10,
			    -1, &code);

			/*
			 * If timerun fails or the exit code from the
			 * script is not 0, we send a kill to the process
			 * tree. pmf_stop_monitoring actually means
			 * stop_restarting, not stop_monitoring.
			 */
			if (rc != SCHA_ERR_NOERR) {
				/*
				 * SCMSGS
				 * @explanation
				 * An attempt to stop the application failed
				 * with the failure specified in the message.
				 * @user_action
				 * Save the syslog and contact your authorized
				 * Sun service provider.
				 */
				scds_syslog(LOG_ERR, "Failed to stop the "
				    "application using %s: %s", stopcmd,
				    scds_error_string(rc));
			} else if (code != 0) {
				/*
				 * SCMSGS
				 * @explanation
				 * A command was run to stop the application
				 * but it failed with an error. Both the
				 * command and the error code are specified in
				 * the message.
				 * @user_action
				 * Save the syslog and contact your authorized
				 * Sun service provider.
				 */
				scds_syslog(LOG_ERR, "%s failed to stop the "
				    "application and returned with %d",
				    stopcmd, code);

				/*
				 * Treat a script failure as internal error.
				 * This makes sure we log a message about
				 * sending KILL later on.
				 */
				rc = SCHA_ERR_INTERNAL;
			}
		} else {

			/* stop script not available, lets use signals */
			scds_syslog_debug(DBG_LEVEL_HIGH, "Using SIGTERM to "
			    "stop the resource");
			rc = scds_pmf_stop(scds_handle, SCDS_PMF_TYPE_SVC, indx,
				SIGTERM, scds_get_rs_stop_timeout(scds_handle));
		}

		/* rc now has the return code from scds_timerun or pmf_stop */
		if (rc != SCHA_ERR_NOERR) {
			/*
			 * SCMSGS
			 * @explanation
			 * An attempt to stop the application did not succeed.
			 * A KILL signal will now be delivered to the
			 * application in order to stop it forcibly.
			 * @user_action
			 * No action is required. This is an informational
			 * message only.
			 */
			scds_syslog(LOG_ERR, "Failed to stop the application "
				"cleanly. Will try to stop using SIGKILL");
		}

		/*
		 * No matter what has happened till now, we send a kill
		 * to the entire tree. pmf_stop returns NOERR for tags that
		 * are already gone, so its harmless in case the webserver
		 * has shut down already.
		 */
		rc = scds_pmf_stop(scds_handle, SCDS_PMF_TYPE_SVC, indx,
		    SIGKILL, -1);
		if (rc == SCHA_ERR_NOERR) {
			(void) scha_resource_setstatus(rsname, rgname,
			    SCHA_RSSTATUS_OFFLINE, msg_ok);
		} else {
			(void) scha_resource_setstatus(rsname, rgname,
			    SCHA_RSSTATUS_FAULTED, msg_fail);
		}

	} /* for loop */

	return (rc);
}


/*
 * This function starts the fault monitor for a iws resource.
 * This is done by starting the probe under PMF. The PMF tag
 * is derived as <RG-name,RS-name,instance_number.mon>. The restart
 * option of PMF is used but not the "infinite restart". Instead
 * interval/retry_time is obtained from the RTR file.
 */

int
mon_start(scds_handle_t scds_handle)
{
	scds_syslog_debug(DBG_LEVEL_HIGH,
		"Calling MONITOR_START method for resource %s.",
		scds_get_resource_name(scds_handle));

	/*
	 * The probe iws_probe is assumed to be available in the same
	 * subdirectory where the other callback methods for the RT are
	 * installed. The last parameter to scds_pmf_start denotes the
	 * child monitor level. Since we are starting the probe under PMF
	 * we need to monitor the probe process only and hence we are using
	 * a value of 0.
	 */
	if (scds_pmf_start(scds_handle, SCDS_PMF_TYPE_MON,
		SCDS_PMF_SINGLE_INSTANCE, "iws_probe", 0)
		!= SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
			"Failed to start fault monitor.");
		return (1);
	}

	scds_syslog(LOG_INFO, "Started the fault monitor.");

	return (SCHA_ERR_NOERR);
}


/*
 * This function stops the fault monitor for a iws resource.
 * This is done via PMF. The PMF tag for the fault monitor is
 * constructed based on <RG-name_RS-name,instance_number.mon>.
 */

int
mon_stop(scds_handle_t scds_handle)
{
	scds_syslog_debug(DBG_LEVEL_HIGH,
		"Calling MONITOR_STOP method for resource %s.",
		scds_get_resource_name(scds_handle));

	if (scds_pmf_stop(scds_handle, SCDS_PMF_TYPE_MON,
		SCDS_PMF_SINGLE_INSTANCE, SIGKILL,
		scds_get_rs_monitor_stop_timeout(scds_handle))
		!= SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
			"Failed to stop fault monitor.");
		return (1);
	}

	scds_syslog(LOG_INFO, "Stopped the fault monitor.");

	return (SCHA_ERR_NOERR);
}


/*
 * svc_probe(): Do data service specific probing. Return a value
 * between 0 (success) and 100(complete failure).
 *
 * The probe does a simple socket connection to the iws server on the
 * specified port which is configured as the resource extension property
 * (Port_list) and pings the dataservice. If the probe fails to connect to
 * the port, we return a value of 100 indicating that there is a total
 * failure. If the connection goes through and the disconnect to the port
 * fails, then a value of 50 is returned indicating a partial failure.
 */

int
svc_probe(scds_handle_t scds_handle, char *hostname, int port,
	boolean_t issecure, int timeout, boolean_t arg_syslog_msgs)
{
	ulong_t	t1, t2;
	int	sock, rc = 0, retval = 0, time_used, time_remaining;
	size_t	size = 0;
	char	buf[SCDS_ARRAY_SIZE];
	long	connect_timeout;


	/*
	 * Probe the dataservice by doing a socket connection to the port
	 * specified in the port_list property to the host that is
	 * serving the iws dataservice. If the iws service which is
	 * configured to listen on the specified port, replies to the
	 * connection, then the probe is successfull. Else we will wait for
	 * a time period set in probe_timeout property before concluding
	 * that the probe failed.
	 */

	/*
	 * Use the SVC_CONNECT_TIMEOUT_PCT percentage of timeout
	 * to connect to the port
	 */
	connect_timeout = (SVC_CONNECT_TIMEOUT_PCT * timeout)/100;
	t1 = (ulong_t)(gethrtime()/1E9);

	/*
	 * the probe makes a connection to the specified hostname and port.
	 * The connection is timed for 95% of the actual probe_timeout.
	 */
	rc = scds_fm_tcp_connect(scds_handle, &sock, hostname, port,
		connect_timeout);
	if (rc) {
		scds_syslog(LOG_ERR,
			"Failed to connect to host %s and "
			"port %d: %s.",
			hostname, port, strerror(errno));
		/* this is a complete failure */
		return (SCDS_PROBE_COMPLETE_FAILURE);
	} else {
		scds_syslog_debug(DBG_LEVEL_LOW,
			"Successful connection to server %s "
			"port %d for resource %s.",
			hostname, port,
			scds_get_resource_name(scds_handle));
	}

	t2 = (ulong_t)(gethrtime()/1E9);

	/*
	 * Compute the actual time it took to connect. This should be less than
	 * or equal to connect_timeout, the time allocated to connect.
	 * If the connect uses all the time that is allocated for it,
	 * then the remaining value from the probe_timeout that is passed to
	 * this function will be used as disconnect timeout. Otherwise, the
	 * the remaining time from the connect call will also be added to
	 * the disconnect timeout.
	 *
	 */

	time_used = (int)(t2 - t1);

	/*
	 * Use the remaining time(timeout - time_took_to_connect) to disconnect
	 */

	time_remaining = timeout - time_used;

	/*
	 * If all the time is used up, use a small hardcoded timeout
	 * to still try to disconnect. This will avoid the fd leak.
	 */
	if (time_remaining <= 0) {
		scds_syslog_debug(DBG_LEVEL_LOW,
			"svc_probe used entire timeout of "
			"%d seconds during connect operation "
			"and exceeded the timeout by %d seconds. "
			"Attempting disconnect with timeout %d",
			connect_timeout,
			abs(time_remaining),
			SVC_DISCONNECT_TIMEOUT_SECONDS);

		time_remaining = SVC_DISCONNECT_TIMEOUT_SECONDS;
	} else {

		if (issecure) {
			goto finished;
		}

		/* Generic HTML/1.0 HEAD check */
		(void) strcpy(buf, "HEAD / HTTP/1.0\n\n");

		size = strlen(buf);
		if (scds_fm_tcp_write(scds_handle, sock,
			buf, &size, time_remaining) < 0) {
			/*
			 * write()s should never fail unless the server
			 * (iws) has closed its end of the socket.
			 * That sounds like a serious problem. Hence 1
			 * as probe result.
			 */
			retval = SCDS_PROBE_COMPLETE_FAILURE;
			if (arg_syslog_msgs) {
				scds_syslog(LOG_ERR,
					"Failed to communicate with "
					"server %s port %d: %s.",
					hostname, port, strerror(errno));
			}

			goto finished;
		}

		/*
		 * Data sent to us by server may span several packets,
		 * hence must do things in a loop().
		 */
		do {
			t2 = (ulong_t)(gethrtime()/1E9);
			time_used = (int)(t2 - t1);
			time_remaining = timeout - time_used;
			if (time_remaining < 1) {
				/*
				 * Be more lenient if the read()s
				 * timeout or * fail, maybe a temporary
				 * problem. Two successive "partial"
				 * failures like these though, ought to
				 * make us restart the server, at least.
				 * If not failover... Hence the "1/2" as
				 * the probe result.
				 */
				retval = retval +
					SCDS_PROBE_COMPLETE_FAILURE/2;

				if (retval >= SCDS_PROBE_COMPLETE_FAILURE) {
					retval = SCDS_PROBE_COMPLETE_FAILURE;
					goto finished;
				}
				continue;
			}

			scds_syslog_debug(DBG_LEVEL_HIGH,
				"Waiting to receive data from the server for "
				"resource %s.",
				scds_get_resource_name(scds_handle));

			size = sizeof (buf) - 1;
			if ((scds_fm_tcp_read(scds_handle, sock, buf,
				&size, time_remaining)) != SCHA_ERR_NOERR) {
					retval = retval +
						SCDS_PROBE_COMPLETE_FAILURE/2;

				if (arg_syslog_msgs) {
					scds_syslog(LOG_ERR,
						"Failed to communicate with "
						"server %s port %d: %s.",
						hostname, port,
						strerror(errno));
				}

				if (retval >= SCDS_PROBE_COMPLETE_FAILURE) {
					retval = SCDS_PROBE_COMPLETE_FAILURE;
					goto finished;
				}
				continue;
			}
			buf[size] = '\0';

			scds_syslog_debug(DBG_LEVEL_HIGH,
				"Received %d bytes from the server "
				"for resource %s.",
				size, scds_get_resource_name(scds_handle));

		} while (size == (int)sizeof (buf) - 1);
	}

finished:
	/*
	 * Return partial failure in case of disconnection failure.
	 * Reason: The connect call is successful, which means
	 * the application is alive. A disconnection failure
	 * could happen due to a hung application or heavy load.
	 * If it is the later case, don't declare the application
	 * as dead by returning complete failure. Instead, declare
	 * it as partial failure. If this situation persists, the
	 * disconnect call will fail again and the application will be
	 * restarted.
	 */
	rc = scds_fm_tcp_disconnect(scds_handle, sock, time_remaining);
	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
			"Failed to disconnect from host %s "
			"and port %d.", hostname, port);
		/* this is a partial failure */
		if (retval < SCDS_PROBE_COMPLETE_FAILURE/2)
			retval = (SCDS_PROBE_COMPLETE_FAILURE/2);

		/* return the greater of above or previous value */
		return (retval);
	}

	t2 = (ulong_t)(gethrtime()/1E9);
	time_used = (int)(t2 - t1);
	time_remaining = timeout - time_used;

	/*
	 * If there is no time left, return
	 * SCDS_PROBE_COMPLETE_FAILURE/2 instead. This will make sure
	 * that if this timeout persists, server will be restarted.
	 */
	if (time_remaining <= 0) {
		scds_syslog(LOG_ERR, "Probe timed out.");

		retval += (SCDS_PROBE_COMPLETE_FAILURE/2);
		if (retval > SCDS_PROBE_COMPLETE_FAILURE)
			retval = SCDS_PROBE_COMPLETE_FAILURE;
	}

	return (retval);
}


/*
 * Get the secure status for each ports, and return in an array of boolean_t.
 * scds_handle passed in.
 * se_secure array must be freed when done.
 * Returns SCHA_ERR_NOERR or 1.
 */

int
get_secure_ports(scds_handle_t scds_handle, boolean_t **se_securep)
{
	scds_port_list_t *portlist = NULL;
	scha_str_array_t *confdirs = NULL;
	char magnus_file[SCDS_ARRAY_SIZE];
	boolean_t *se_secure;
	char testfile[SCDS_ARRAY_SIZE];
	int se_nport, rc, i;

	rc = scds_get_port_list(scds_handle, &portlist);
	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
			"Failed to retrieve the property %s: %s.",
			"Port_list", scds_error_string(rc));
		return (1);
	} else {
		se_nport = portlist->num_ports;
		scds_free_port_list(portlist);
	}
	se_secure =
		(boolean_t *)calloc((size_t)se_nport, sizeof (boolean_t));
	if (se_secure == NULL) {
		scds_syslog(LOG_ERR, "Out of memory.");
		return (1);
	}

	/* Get Confdir_list extension property */
	confdirs = scds_get_ext_confdir_list(scds_handle);

	/* Return an error if there are no config_dirs x prop */
	if (confdirs == NULL || confdirs->array_cnt == 0) {
		scds_syslog(LOG_ERR,
			"Property %s is not set.",
			"Confdir_list");
		return (1);
	}

	/* Find out which ports are secure */
	for (i = 0; i < se_nport; i++) {
		if (pre_webserver7[i]) {
			rc = snprintf(magnus_file, sizeof (magnus_file),
				"%s/config/magnus.conf",
				confdirs->str_array[i]);

			if (rc == -1) {
				scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
					"String handling error creating "
					"path to magnus.conf. "
					"The path may be too long");
				return (1);
			}

			if (scds_ns_is_secure(magnus_file, &se_secure[i])) {
				/*
				 * SCMSGS
				 * @explanation
				 * While parsing the Netscape configuration
				 * file an error occured in while either
				 * reading the file, or one of the fields
				 * within the file.
				 * @user_action
				 * Make sure that the appropriate
				 * configuration file is located in its
				 * default location with respect to the
				 * Confdir_list property.
				 */
				scds_syslog(LOG_ERR,
					"Unable to parse configuration file.");
				return (1);
			}

		} else {

		/*
		 * The keyword "Security On/Off" has been removed
		 * from magnus.conf file in Sun Java System Web
		 * Server 7.0 version. The following code checks
		 * if keypass file is present in <config-dir>/keypass
		 * and sets the issecure flag to true/false accordingly.
		 */
			/* If keypass exists, set issecure on */
			(void) snprintf(testfile, sizeof (testfile),
				"%s/keypass", confdirs->str_array[i]);
			if (access(testfile, F_OK) == 0) {
				se_secure[i] = B_TRUE;
			scds_syslog_debug(DBG_LEVEL_HIGH,
				"Sun Java System Web Server configured in"
				"<%s> is SSL enabled\n",
				confdirs->str_array[i]);
			} else {
				/* keypass was not found */
				se_secure[i] = B_FALSE;
			scds_syslog_debug(DBG_LEVEL_HIGH,
				"Sun Java System Web Server configured in"
				"<%s> is not SSL enabled\n",
				confdirs->str_array[i]);
			}
		}
	}

	*se_securep = se_secure;

	return (SCHA_ERR_NOERR);
}

/*
 * ns_is_secure()
 * Private function to parse a Sun Java System Web Server magnus.conf file
 * for Secure keyword.
 * Return value is in rc.
 */

static scha_err_t
scds_ns_is_secure(char *filename, boolean_t *rc)
{
	char val[SCDS_ARRAY_SIZE];

	*rc = B_FALSE;

	/*
	 * Look for the "Security" Keyword
	 * If the line "Security off" appears in the given file, then
	 * this iws service is a non-SSL type service.
	 */
	if (ds_get_magnus_value(filename, "Security", val) == 0) {
		if (strcasecmp(val, "off") == 0) {
			*rc = B_FALSE;
			scds_syslog_debug(DBG_LEVEL_HIGH,
				"Sun Java System Web Server service with "
				"config file <%s> is not SSL enabled\n",
				filename);
		} else {
			*rc = B_TRUE;
			scds_syslog_debug(DBG_LEVEL_HIGH,
				"Sun Java System Web Server service with "
				"config file <%s> is secure\n", filename);
		}

		return (SCHA_ERR_NOERR);
	}

	scds_syslog_debug(DBG_LEVEL_HIGH,
		"Sun Java System Web Server service with config file <%s>"
		" is not SSL enabled\n", filename);

	return (SCHA_ERR_NOERR);
}

/*
 * remove_pidlog()
 *
 * Remove the PidLog file as specified in magnus_filename.
 *
 * Returns 1 on error, otherwise 0.
 */
static int
remove_pidlog(const char *magnus_filename)
{
	char	pidfile[MAXPATHLEN+1];

	if (ds_get_magnus_value(magnus_filename, "PidLog", pidfile) == 0) {

		/*
		 * If the return from get_magnus_filename() is non
		 * zero there was some issue, either there was no
		 * PidLog entry or the PidLog value was larger then
		 * MAXPATHLEN.
		 *
		 * In either case we don't want to unlink whatever was
		 * returned in pidfile.  Just skip and let the web
		 * server sort it out.
		 */

		if (unlink(pidfile) == 0) {
			scds_syslog_debug(DBG_LEVEL_HIGH,
				"Removed PidLog at %s\n", pidfile);
		} else {
			/*
			 * It's not an error if the PidLog is not
			 * there.
			 */
			if (errno != ENOENT) {
				/*
				 * SCMSGS
				 * @explanation
				 * The data service was not able to delete the
				 * specified PidLog file.
				 * @user_action
				 * Delete the PidLog file manually and start
				 * the the resource group.
				 */
				scds_syslog(LOG_ERR,
					"Error deleting PidLog <%s> "
					"(%s) "
					"for Sun Java System Web Server "
					"service with config file <%s>.\n",
					pidfile, strerror(errno),
					magnus_filename);
				return (1);
			}
		}
	}

	return (0);
}

/*
 * This function validates the monitor_uri_list extension property.
 * This function extracts the extension property and calls the function
 * validate_server_url for each uri.
 * It is OK for the property not to exist at all or have a null value.
 */

int
validate_monitor_uri_list(scds_handle_t scds_handle,
    scds_net_resource_list_t *snrlp, boolean_t print_messages)
{
	int		i, rc;
	scha_extprop_value_t	*prop = NULL;
	scha_str_array_t		*uris = NULL;

	scds_syslog_debug(DBG_LEVEL_HIGH, "In validate_monitor_uri_list()");

	/* Get the monitor uri list */
	rc = scds_get_ext_property(scds_handle, "Monitor_Uri_List",
			SCHA_PTYPE_STRINGARRAY, &prop);
	if (rc == SCHA_ERR_PROP) {
		scds_syslog_debug(DBG_LEVEL_HIGH, "Monitor_Uri_List is an "
		    "invalid property");
		return (0); /* nothing to do, property doesnt exist */
	}

	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
				"Failed to retrieve the "
				"property %s: %s.",
				"Monitor_Uri_List", scds_error_string(rc));
		if (print_messages) {
			(void) fprintf(stderr, gettext("Failed to retrieve "
					"the extension "
					"property %s: %s.\n"),
					"Monitor_Uri_List",
					scds_error_string(rc));
		}
		return (1);
	} else {
		uris = prop->val.val_strarray;
	}

	if ((prop == NULL) || prop->val.val_strarray->array_cnt < 1) {
		/* no value specified for the property, thats fine */
		scds_syslog_debug(DBG_LEVEL_HIGH, "No value specified for "
		    "Monitor_Uri_List");
		if (prop)
			scds_free_ext_property(prop);
		return (0);
	}

	for (i = 0; i < (int)uris->array_cnt; i++) {
		char *uri = uris->str_array[i];

		scds_syslog_debug(DBG_LEVEL_HIGH, "Validating uri %s", uri);

		if (validate_server_url(uri, snrlp, print_messages) != 0) {
			scds_syslog(LOG_ERR,
				"Validation of URI %s failed", uri);
			if (print_messages) {
				(void) fprintf(stderr, gettext("Validation of "
						"URI %s failed\n"), uri);
			}
			scds_free_ext_property(prop);
			return (1);
		}
	}

	scds_free_ext_property(prop);

	scds_syslog_debug(DBG_LEVEL_HIGH, "Successfully validated the "
				"monitor_uri_list");
	return (0);
}

/*
 * Function to validate the uri/url. This function makes sure that
 * the url/uri uses only http protocol and not any other protocol.
 */

int
validate_server_url(char *uri, scds_net_resource_list_t *snrlp,
    boolean_t print_messages)
{
	char    *scheme, *host, *port, *path;
	int		rc, i, j;

	scds_syslog_debug(DBG_LEVEL_HIGH, "In validate_server_url()");

	/*
	 * Wont happen, has already been checked in svc_validate.
	 * But better safe than sorry
	 */
	if (snrlp == NULL || snrlp->num_netresources == 0) {
		scds_syslog(LOG_ERR,
			"No network address resource in resource group.");
		if (print_messages) {
			(void) fprintf(stderr, gettext("No network address "
				"resource in resource group.\n"));
		}
		return (1);
	}

	rc = ds_parse_simple_uri(uri, &scheme, &host, &port,
			&path);

	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Error parsing URI: %s", uri);
		if (print_messages) {
			(void) fprintf(stderr,
					gettext("Error parsing URI: %s\n"),
					uri);
		}
		rc = 1;
		goto finished;
	}

	if ((strcasecmp(scheme, "http")) != 0) {
		scds_syslog(LOG_ERR,
			"URI (%s) must be an absolute http URI.", uri);
		if (print_messages) {
			(void) fprintf(stderr, gettext("URI <%s> must "
					"be an absolute "
					"http URI.\n"), uri);
		}
		rc = 1;
		goto finished;
	}

	for (i = 0; i < snrlp->num_netresources; i++) {
		for (j = 0; j < snrlp->netresources[i].num_hostnames; j++) {
			if (strcasecmp(host,
			    snrlp->netresources[i].hostnames[j]) == 0) {
				scds_syslog_debug(DBG_LEVEL_HIGH,
					"URI validation: match "
					"on %s [%d][%d]", host, i, j);
				rc = 0;
				goto finished;
			}
		}
	}

	/*
	 * If we get here we did not find a match for the hostname in
	 * any of the network resource hostnames.  Log and if
	 * requested print a message and set rc to SCHA_ERR_INVAL for
	 * the return.
	 */

	scds_syslog(LOG_ERR, "The hostname in %s is not a network address "
		"resource in this resource group.", uri);
	if (print_messages)
		(void) fprintf(stderr, gettext("The hostname in %s is not "
				"a network address resource in this "
				"resource group.\n"), uri);
	rc = 1;

finished:
	free(scheme);
	free(host);
	free(port);
	free(path);

	return (rc);
}


int
probe_uri(scds_handle_t scds_handle, char *uri, int timeout,
    boolean_t log_messages)
{
	int		rc;
	int		status;
	int		failure_code = 0;

	rc = ds_http_get_status(scds_handle, uri, &status, timeout,
			log_messages);
	scds_syslog_debug(DBG_LEVEL_HIGH, "ds_http_get_status returned "
				"rc = %d, status = %d for uri %s",
				rc, status, uri);

	switch (rc) {
	case SCHA_ERR_NOERR :
		if (status == 500) {
			failure_code = SCDS_PROBE_COMPLETE_FAILURE;
			scds_syslog(LOG_ERR, "Probe failed, HTTP GET Response "
			    "Code for %s is %d.", uri, status);
		}
		break;
	case SCHA_ERR_NOMEM:
	case SCHA_ERR_INVAL:
	case SCHA_ERR_INTERNAL:
		failure_code = 0;
		break;
	case SCHA_ERR_TIMEOUT:
		failure_code = SCDS_PROBE_COMPLETE_FAILURE / 2;
		break;
	case SCHA_ERR_STATE:
		/* Connection refused */
		failure_code = SCDS_PROBE_COMPLETE_FAILURE;
		break;
	default:
		failure_code = 0;
		break;
	}

	return (failure_code);
}
