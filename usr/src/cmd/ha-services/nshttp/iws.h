/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_NSHTTP_H
#define	_NSHTTP_H

#pragma ident	"@(#)iws.h	1.26	08/06/17 SMI"

#include <rgm/libdsdev.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Debug levels for error messages */
#define	DBG_LEVEL_HIGH		9
#define	DBG_LEVEL_MED		5
#define	DBG_LEVEL_LOW		1

#define	SCDS_CMD_SIZE		(8*1024)

#define	SCDS_ARRAY_SIZE		1024

/*
 * Application name defined for use in scds_syslog messages.
 * Using this allows identical messages to be defined in more
 * than one data service, hashing to one message explanation.
 */
#define	APP_NAME		"Sun Java System Web Server"


/*
 * svc_validate():
 * Do iws specific validation of the resource configration.
 * Called by start/validate/update/monitor methods.
 * Return 0 on success, > 0 on failures.
 */
int svc_validate(scds_handle_t scds_handle, boolean_t print_messages);

/*
 * svc_start():
 * creates a pmftag under which iws can be started
 * constructs the command and starts iws
 * The issecure flag causes a password to be given
 * when iws is started up in secure mode.
 */
int svc_start(scds_handle_t scds_handle, boolean_t *issecure);

int svc_stop(scds_handle_t scds_handle);

int svc_wait(scds_handle_t scds_handle, boolean_t *issecure);

int mon_start(scds_handle_t scds_handle);

int mon_stop(scds_handle_t scds_handle);

int svc_probe(scds_handle_t scds_handle, char *hostname, int port,
	boolean_t issecure, int timeout, boolean_t arg_syslog_msgs);

/*
 * Determine which ports are secure.
 */
int get_secure_ports(scds_handle_t scds_handle, boolean_t **se_securep);

int probe_uri(scds_handle_t scds_handle, char *uri, int timeout,
    boolean_t log_messages);

#ifdef __cplusplus
}
#endif

#endif /* _NSHTTP_H */
