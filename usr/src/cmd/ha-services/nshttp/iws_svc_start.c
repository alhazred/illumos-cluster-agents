/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 1998-2002 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * iws_svc_start.c - Start method for highly available iws
 */

#pragma ident	"@(#)iws_svc_start.c	1.29	07/06/06 SMI"

#include <rgm/libdsdev.h>
#include "iws.h"

/*
 * The start method for iws. Does some sanity checks on
 * the resource settings then starts the iws under PMF with
 * an action script.
 */

int
main(int argc, char *argv[])
{
	scds_handle_t	scds_handle;
	boolean_t	*se_secure;
	int		rc;

	/*
	 * Process all the arguments that have been passed to us from RGM
	 * and do some initialization for syslog
	 */

	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR) {
		return (1);
	}

	/* Validate the configuration and if there is an error return back */
	rc = svc_validate(scds_handle, B_FALSE);
	if (rc != 0) {
		scds_syslog(LOG_ERR,
			"Failed to validate configuration.");
		goto finished;
	}

	rc = get_secure_ports(scds_handle, &se_secure);
	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
			"Failed to start service.");
		goto finished;
	}

	/* Start the data service, if it fails return with an error */
	rc = svc_start(scds_handle, se_secure);
	if (rc != SCHA_ERR_NOERR) {
		goto finished;
	}

	scds_syslog_debug(DBG_LEVEL_HIGH,
		"Done with svc_start, calling svc_wait.");

	/* Wait for the service to start up fully */
	rc = svc_wait(scds_handle, se_secure);
	scds_syslog_debug(DBG_LEVEL_HIGH,
		"Done with svc_wait which returned <%d>.", rc);

finished:
	/* Free up the Environment resources that were allocated */
	scds_close(&scds_handle);

	return (rc);
}
