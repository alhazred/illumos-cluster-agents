/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 1998-2002 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * iws_monitor_check.c - Monitor Check method for highly available iws
 */

#pragma ident	"@(#)iws_monitor_check.c	1.20	07/06/06 SMI"

#include <rgm/libdsdev.h>
#include "iws.h"

/*
 * just make a simple validate check on the service
 */

int
main(int argc, char *argv[])
{
	scds_handle_t		scds_handle;
	int			rc;

	/*
	 * Process all the arguments that have been passed to us from RGM
	 * and do some initialization for syslog
	 */

	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR) {
		return (1);
	}

	rc =  svc_validate(scds_handle, B_FALSE);
	scds_syslog_debug(DBG_LEVEL_HIGH,
		"monitor_check method for resource %s "
		"was called and returned %d.",
		scds_get_resource_name(scds_handle), rc);

	/* Free up all the memory allocated by scds_initialize */
	scds_close(&scds_handle);

	/* Return the result of validate method run as part of monitor check */
	return (rc);
}
