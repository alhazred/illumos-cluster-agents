/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#pragma ident	"@(#)jsas-na_probe.c	1.8	07/06/06 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <unistd.h>
#include <rgm/libdsdev.h>
#include "jsas-na.h"

int
main(int argc, char *argv[])
{
	scds_handle_t	scds_handle;
	scha_err_t 	rc, err;

	int		timeout, i;
	int		probe_result = 0;
	hrtime_t	ht1, ht2;
	long		dt;

	scds_pmf_status_t	status;

	as_extn_props_t		asextnprops;

	scds_syslog_debug(DBG_LEVEL_HIGH,
				"In jsas-na_probe main");

	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR)
		return (1);

	/* Get the App Server extension properties */

	if (get_as_extn_props(scds_handle, &asextnprops, B_FALSE) != 0) {
		/* Error message already logged */
		return (1);
	}

	/*
	 * Get the timeout from the extension props. This means that
	 * each probe iteration will get a full timeout on each network
	 * resource without chopping up the timeout between all of the
	 * network resources configured for this resource.
	 */
	timeout = scds_get_ext_probe_timeout(scds_handle);

	for (;;) {

		/*
		 * sleep for a duration of thorough_probe_interval between
		 * successive probes.
		 */
		(void) scds_fm_sleep(scds_handle,
			scds_get_rs_thorough_probe_interval(scds_handle));

		ht1 = gethrtime(); /* Latch probe start time */

		/*
		 * Probe all the Node Agents. Action will be taken
		 * as soon as a failure is detected. After Failure is
		 * is detected the remaining Node Agents are not checked.
		 * Action is taken immediately.
		 */

		for (i = 0; i < asextnprops.number_of_na; i++) {

			probe_result = 0;

			rc = get_na_status(scds_handle, timeout,
					&asextnprops, i);

			if (rc == DAS_NOT_RUNNING) {
				scds_syslog_debug(DBG_LEVEL_HIGH,
					"get_na_status failed and "
					"returned %d. The Admin Server "
					"could be down.", rc);

				/*
				 * SCMSGS
				 * @explanation
				 * This error is from the Node Agents Probe.
				 * Before checking for the status of the Node
				 * Agents, the probe makes sure the Domain
				 * Admin Server is accessible. This error
				 * message indicates that the Domain Admin
				 * Server is not UP and the probe cannot get
				 * the status of the Node Agents.
				 * @user_action
				 * If the Domain Admin Server is also a
				 * suncluster resource then set the resource
				 * dependency between the Node Agents resource
				 * and the Domain Admin Server resource. If
				 * the Domain Admin Server is not a sun
				 * cluster resource then make sure it is
				 * Running.
				 */
				scds_syslog(LOG_ERR,
					"The asadmin list-node-agents command "
					"failed to run. Unable to communicate "
					"with the Domain Admin Server %s on "
					"port %d. Probe Cannot determine the "
					"status of the Node Agent %s.",
					asextnprops.admin_host,
					asextnprops.admin_port,
					asextnprops.na_list[i]);
				/*
				 * Take No action. Just continue to throw
				 * the message till DAS is available
				 */
				continue;
			}

			if (rc == NA_NOT_RUNNING) {
				scds_syslog_debug(DBG_LEVEL_HIGH,
					"get_na_status failed and "
					"returned %d", rc);

				scds_syslog_debug(DBG_LEVEL_HIGH,
					"get_na_status says Node Agent %s "
					"is NOT running",
					asextnprops.na_list[i]);
				/*
				 * If we are here then the asadmin
				 * list-node-agents lists the status
				 * of the Node Agent as "not running"
				 */
				probe_result = SCDS_PROBE_COMPLETE_FAILURE;
				break;
			}

			if (rc == NO_MEMORY) {
				/*
				 * Out of Memory error.
				 * Message already logged.
				 */

				probe_result = SCDS_PROBE_COMPLETE_FAILURE/10;
				break;
			}

			scds_syslog_debug(DBG_LEVEL_HIGH,
				"get_na_status says Node Agent %s "
				"is running", asextnprops.na_list[i]);

			err = scds_pmf_get_status(scds_handle,
				SCDS_PMF_TYPE_SVC, i, &status);

			if (err != SCHA_ERR_NOERR) {
				scds_syslog(LOG_ERR,
					"Failed to retrieve process monitor "
					"facility tag.");
				probe_result = SCDS_PROBE_COMPLETE_FAILURE;
				break;
			}

			/* Check if the dataservice is still up and running */
			if (status != SCDS_PMF_MONITORED) {
				scds_syslog(LOG_ERR,
					"Node Agent %s failed to stay up. ",
					asextnprops.na_list[i]);
				probe_result = SCDS_PROBE_COMPLETE_FAILURE;
				break;
			}


		} /* End Node Agents for loop */

		ht2 = gethrtime();

		/* Convert to milliseconds */
		dt = (long)((ht2 - ht1) / 1e6);

		/*
		 * Compute failure history and take action if needed
		 */
		(void) scds_fm_action(scds_handle, probe_result, dt);


	} 	/* Keep probing forever */
}
