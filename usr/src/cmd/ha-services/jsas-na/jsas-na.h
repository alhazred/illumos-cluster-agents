/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2004 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_S1AS_H
#define	_S1AS_H

#pragma ident	"@(#)jsas-na.h	1.7	07/06/06 SMI"

#include <limits.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Debug levels for error messages */
#define	DBG_LEVEL_HIGH		9
#define	DBG_LEVEL_MED		5
#define	DBG_LEVEL_LOW		1

#define	SCDS_CMD_SIZE		(8 * 1024)

#define	SCDS_ARRAY_SIZE		1024

/* Extension properties */
#define	ADMINUSER	"Adminuser"
#define	PASSWORDFILE	"Passwordfile"
#define	AGENTDIR	"Agentdir"
#define	ADMINHOST	"Adminhost"
#define	ADMINPORT	"Adminport"

/* Error code returned from the jsas-na_utils.sh script */

#define	NA_RUNNING		0
#define	DAS_NOT_RUNNING		1
#define	NA_NOT_RUNNING		2
#define	NO_MEMORY		3
/*
 * Application name defined for use in scds_syslog messages.
 * Using this allows identical messages to be defined in more
 * than one data service, hashing to one message explanation.
 */
#define	APP_NAME	"Sun Java Systems Application Server"

/*
 * How long to wait for final stop attempt.  SC3.1 supports passing -1
 * to mean wait forever.  In SC3.0 we need to settle for INT_MAX or
 * approximately 68 years.
 *
 * "Forever" is of course until RGM times out the stop method.
 */
#define	FINAL_STOP_TIMEOUT	INT_MAX

/* structure to hold the JSAS extension properties */

typedef struct as_extension_props
{
	char	*install_dir;
	char	*admin_user;
	char	*password_file;
	char	*agentdir;
	char	*admin_host;
	int	number_of_na;
	int	admin_port;
	char	na_list[1024][1024];
}as_extn_props_t;

int svc_validate(as_extn_props_t *asextnprops,
		boolean_t print_messages);

int svc_start(scds_handle_t scds_handle, as_extn_props_t *asextnprops);

int svc_stop(scds_handle_t scds_handle, as_extn_props_t *asextnprops);

int svc_wait(scds_handle_t scds_handle, as_extn_props_t *asextnprops);

int mon_start(scds_handle_t scds_handle);

int mon_stop(scds_handle_t scds_handle);

int svc_probe(scds_handle_t scds_handle, char *hostname, int port,
	time_t timeout);

int svc_probe_uri(scds_handle_t scds_handle, char *uri, time_t timeout);

int get_as_extn_props(scds_handle_t scds_handle,
	as_extn_props_t *asextnprops, boolean_t print_messages);

int get_na_status(scds_handle_t scds_handle, int timeout,
	as_extn_props_t *asextnprops, int i);

int get_node_agents_list(scds_handle_t scds_handle,
	as_extn_props_t *asextnprops, boolean_t print_messages);

#ifdef __cplusplus
}
#endif

#endif /* _S1AS_H */
