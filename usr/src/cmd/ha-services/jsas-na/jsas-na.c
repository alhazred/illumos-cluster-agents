/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * jsas-na.c - Common utilities for jsas Node Agents
 */

#pragma ident	"@(#)jsas-na.c	1.9	07/08/07 SMI"

/*
 * This utility has the methods for performing the validation,
 * starting and stopping the data service and the fault monitor. It
 * also contains the method to probe the health of the data service.
 * The probe just returns either success or failure. Action is taken
 * based on this returned value in the method found in the file
 * jsas_probe.c
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <fcntl.h>
#include <libgen.h>
#include <unistd.h>
#include <libintl.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/statvfs.h>
#include <scha.h>
#include <rgm/libdsdev.h>
#include <dlfcn.h>
#include "jsas-na.h"
#include "../common/ds_common.h"

/*
 * The initial timeout allowed  for the jsas dataservice to
 * be fully up and running. We will wait for 10 % (SVC_WAIT_PCT)
 * of the start_timeout time before probing the service.
 *
 * This will with the default setting of 300 seconds for start timeout
 * give us a wait time of 30 seconds, about how long S1AS takes to
 * start up on a E3500.
 */
#define	SVC_WAIT_PCT		3

/*
 * Wait for SVC_WAIT_TIME between probes while waiting for service to
 * start.
 */

#define	SVC_WAIT_TIME		8

#define	SVC_SMOOTH_PCT		80

#define	ASADMIN_COMMAND_PCT		10
boolean_t pre_glassfish;

/*
 * svc_validate():
 *
 * Do jsas Node Agent specific validation of the
 * resource configuration. Most of the Validation
 * is done while retrieving the Extension Properties.
 *
 */

int
svc_validate(as_extn_props_t *asextnprops,
		boolean_t print_messages)
{
	int	err;
	char    asadmin_bin[SCDS_ARRAY_SIZE];
	struct statvfs			vfsbuf;
	struct stat			statbuf;

	/*
	 * Validate the Install directory
	 */

	err = ds_validate_file(print_messages, S_IFDIR,
			asextnprops->install_dir);
	if (err != SCHA_ERR_NOERR) {
		return (1); /* Message logged in ds_validate_file */
	}

	scds_syslog_debug(DBG_LEVEL_HIGH,
		"The Installation directory %s is validated",
		asextnprops->install_dir);

	/*
	 * Check and Make sure the asadmin command is executable and
	 * readable.
	 */
	pre_glassfish = B_TRUE;
	(void) sprintf(asadmin_bin, "%s/appserver/bin/asadmin",
		asextnprops->install_dir);
		/*
		* Check to see if the version is pre-9.1. If stat fails,
		* it could be application server 9.1. Else it could be
		* Application server 8.*.
		*/
		if (stat(asadmin_bin, &statbuf) != 0) {
			pre_glassfish = B_FALSE;
			(void) sprintf(asadmin_bin, "%s/bin/asadmin",
				asextnprops->install_dir);
		}
	err = ds_validate_file(print_messages, S_IXUSR | S_IRUSR,
		asadmin_bin);
	if (err != SCHA_ERR_NOERR) {
		return (1);
	}

	scds_syslog_debug(DBG_LEVEL_HIGH,
		"%s is validated", asadmin_bin);

	/* Validate the admin password file */
	err = ds_validate_file(print_messages, S_IFREG | S_IXUSR |
		S_IRUSR, asextnprops->password_file);
	if (err != SCHA_ERR_NOERR) {
		return (1);
	}

	scds_syslog_debug(DBG_LEVEL_HIGH,
		"%s is validated", asextnprops->password_file);

	/* All validation checks were successful */

	return (0);		/* return result of validation */
}

/*
 * svc_start():
 */
int
svc_start(scds_handle_t scds_handle, as_extn_props_t *asextnprops)
{

	int	rc = 0, i;
	char	*cmd = NULL;
	int	child_mon_level = -1;

	scds_syslog_debug(DBG_LEVEL_HIGH, "In svc_start() function");

	/* Start each Node agent using PMF */

	for (i = 0; i < asextnprops->number_of_na; i++) {

		scds_syslog_debug(DBG_LEVEL_HIGH,
			"Starting the %d node agent %s ",
			i, asextnprops->na_list[i]);
	if (pre_glassfish) {
		cmd = ds_string_format("%s/appserver/bin/asadmin "
			"start-node-agent --agentdir %s --user %s "
			"--passwordfile %s %s",
			asextnprops->install_dir,
			asextnprops->agentdir,
			asextnprops->admin_user,
			asextnprops->password_file,
			asextnprops->na_list[i]);
	} else {
		cmd = ds_string_format("%s/bin/asadmin "
			"start-node-agent --agentdir %s --user %s "
			"--passwordfile %s %s",
			asextnprops->install_dir,
			asextnprops->agentdir,
			asextnprops->admin_user,
			asextnprops->password_file,
			asextnprops->na_list[i]);
	}

		if (cmd == NULL) {
			rc = 1;
			ds_internal_error("unable to create the start command");
			goto finished;
		}
		scds_syslog_debug(DBG_LEVEL_HIGH, "The command used to start "
			"the Node Agent %s is %s", asextnprops->na_list[i],
			cmd);

		/*
		 * All the App Server instances started by the Node Agent
		 * are also put under the PMF control (child mon level -1).
		 * Originally only the Node Agent was supposed to be put under
		 * the PMF control (Child Mon Level 4). But the Sun Cluster
		 * Data Service restart by RGM doesnt work well with that
		 * functionality.
		 * If the probe detects that a Node agent is down
		 * we would want to ideally re-start just the Node Agent and not
		 * all the App Server instances. But RGM calls the stop method
		 * and then calls the start method. So, we need to tell the
		 * STOP method Not to stop all the app server. We could have
		 * done that by creating a resource name flag in some directory
		 * (/var/run/cluster) and then the stop method will check if the
		 * file exists. If it exists then stopping all the instances
		 * using "asadmin stop-node-agent" will be skipped and just the
		 * scds_pmf_stop() will be called. But, if the Node Agent fails
		 * retry_count times within the re-try interval the probe method
		 * will initiate a Failover. The file would have ben still
		 * created because the Probe detected the faillure of NA. The
		 * STOP method will not call "asadmin stop-node-agent" because
		 * it detected the file created by the Probe. We need a scds
		 * call to tell the STOP method if it is called due to a local
		 * restart or due to a Failover or due to switchover. Based on
		 * that information the STOP method can take appropriate action.
		 * The Other possibility was to split the Node Agent stopping in
		 * STOP method and the App server instances stopping in
		 * POSTNET-STOP method because the POSTNET_STOP is called when
		 * the whole RG is going down on the same node or being switched
		 * to another node.But the problem is that the Node Agent should
		 * be available to stop all the appserver instances. So, that
		 * approach also doesnt work.
		 * If you want detailed history check out the code and other
		 * comments (very very elaborate comments ) in SCCS version
		 * 1.0 to 1.3 of this file
		 */
		rc = scds_pmf_start(scds_handle, SCDS_PMF_TYPE_SVC, i, cmd,
				child_mon_level);

		if (rc == SCHA_ERR_NOERR) {
			/*
			 * SCMSGS
			 * @explanation
			 * This is an informational message. The Start method
			 * is starting the Node Agent and all the Application
			 * Server Instances under PMF.
			 * @user_action
			 * None.
			 */
			scds_syslog(LOG_NOTICE,
				"Starting the Node Agent %s "
				"and all its Application Server instances "
				"under PMF", asextnprops->na_list[i]);
		} else {
			/* We fail even if we cannot start one NA */
			/*
			 * SCMSGS
			 * @explanation
			 * The Start method failed to start the Node Agent.
			 * @user_action
			 * Try starting the Node Agent manually using the
			 * asadmin command listed in the error message. If the
			 * Node Agent fails to start, check your configuration
			 * and try again. If Node Agent starts properly when
			 * started manually but the Sun Cluster agent cannot
			 * start it, report the problem.
			 */
			scds_syslog(LOG_ERR,
				"Failed to start the Node Agent %s "
				"using the command %s.",
				asextnprops->na_list[i], cmd);
			free(cmd);
			goto finished;
		}
		free(cmd);
	} /* End of For loop */

finished:
	return (rc); /* return Success/failure status */
}


/*
 * svc_stop():
 *
 * Stop the jsas server
 * Return 0 on success, > 0 on failures.
 *
 */
int
svc_stop(scds_handle_t scds_handle, as_extn_props_t *asextnprops)
{
	int	rc = 0, cmd_exit_code = 0, i;
	char	*command = NULL;
	int	stop_smooth_timeout;
	int	overall_stop_status = 0;
	char    asadmin_bin[SCDS_ARRAY_SIZE];
	struct stat	statbuf;

	scds_syslog(LOG_NOTICE, "Stopping %s.", "JSAS Node Agents");

	stop_smooth_timeout = (scds_get_rs_stop_timeout(scds_handle)
		* SVC_SMOOTH_PCT) / 100;

	for (i = 0; i < asextnprops->number_of_na; i++) {

		pre_glassfish = B_TRUE;
		(void) sprintf(asadmin_bin, "%s/appserver/bin/asadmin",
			asextnprops->install_dir);
		if (stat(asadmin_bin, &statbuf) != 0) {
			pre_glassfish = B_FALSE;
			(void) sprintf(asadmin_bin, "%s/bin/asadmin",
				asextnprops->install_dir);
		}
	if (pre_glassfish) {
		command = ds_string_format("%s/appserver/bin/asadmin "
			"stop-node-agent --agentdir %s %s",
			asextnprops->install_dir,
			asextnprops->agentdir,
			asextnprops->na_list[i]);
	} else {
		command = ds_string_format("%s/bin/asadmin "
			"stop-node-agent --agentdir %s %s",
			asextnprops->install_dir,
			asextnprops->agentdir,
			asextnprops->na_list[i]);
	}

		if (command == NULL) {
			scds_syslog(LOG_ERR, "Unable to compose %s path. "
				"Sending SIGKILL now.", "asadmin stop-domain");
			goto send_kill;
		}

		/*
		 * First take the command out of PMF monitoring, so that it
		 * doesn't keep restarting it.
		 */
		if (scds_pmf_stop_monitoring(scds_handle,
			SCDS_PMF_TYPE_SVC, i) == SCHA_ERR_NOERR) {

			/*
			 * Build the NA stop command and run it under a
			 * timeout to attempt a smooth shutdown of the
			 * appserver.
			 */

			scds_syslog_debug(DBG_LEVEL_HIGH,
				"%s command will be used to stop the "
				"Node Agent %d",
				command, i);

			rc = scds_timerun(scds_handle, command,
				stop_smooth_timeout,
				SIGKILL, &cmd_exit_code);

			if (rc != 0 || cmd_exit_code != 0) {
				/*
				 * SCMSGS
				 * @explanation
				 * This is an informational message. The stop
				 * method first tries to stop the Node Agents
				 * and the Application Server instances using
				 * the "asadmin stop-node-agent" command. The
				 * error message indicates that this command
				 * failed. The command fails if the Node Agent
				 * is already stopped. The Stop Method will
				 * send SIGKILL to all the processes using PMF
				 * to make sure all the processes are stopped.
				 * @user_action
				 * None.
				 */
				scds_syslog(LOG_NOTICE,
					"The stop command <%s> failed to stop "
					"the application. Will now use SIGKILL "
					"to stop the Node Agent and all the "
					"server instances.",  command);

			}

		}

		/*
		 * Regardless of whether the command succeeded or not we send
		 * KILL signal to the pmf tag. This will ensure that the
		 * process tree goes away if it still exists. If it doesn't
		 * exist by then, we return NOERR.
		 */


send_kill:
		/*
		 * Send SIGKILL to stop the application.  Notice that this
		 * call will return with success, even if the tag does
		 * not exist by now.
		 *
		 * We send -1 to scds_pmf_stop as timeout which means we will
		 * try as long as we can till we are timed out by RGM.
		 */
		scds_syslog_debug(DBG_LEVEL_HIGH,
		    "Stopping the NA and all its children by sending SIGKILL "
		    "to PMF");
		if ((rc = scds_pmf_stop(scds_handle, SCDS_PMF_TYPE_SVC, i,
			SIGKILL, -1)) != SCHA_ERR_NOERR) {

			/*
			 * Failed to stop the application even with SIGKILL,
			 * Throw an error message indicating which Node Agent
			 * Failed to Stop. We will set the overall_exit_status
			 * to 1 and continue to stop other Node Agents.
			 * At the end we will return the overall_exit_status.
			 */

			/*
			 * SCMSGS
			 * @explanation
			 * The Stop method failed to stop the Node Agent and
			 * the Application Server instances even with SIGKILL.
			 * @user_action
			 * Manually stop or kill the Node Agent and
			 * Application Server instances.
			 */
			scds_syslog(LOG_ERR,
				"Failed to stop the Node Agent %s and the "
				"server instances with SIGKILL.",
				asextnprops->na_list[i]);
			overall_stop_status = 1;
			/*
			 * We will continue to stop the other Node Agents
			 * eventhough we are returning 1 at the end. I.e we
			 * will atleast try to stop as many as possible.
			 */
			continue;
		}

		if (rc == SCHA_ERR_NOERR) {
			/*
			 * SCMSGS
			 * @explanation
			 * This is an informational message indicating that
			 * the Node Agent and all its Application Server
			 * instances are stopped.
			 * @user_action
			 * None.
			 */
			scds_syslog(LOG_NOTICE,
				"Successfully stopped the Node Agent %s "
				"and the server instances.",
				asextnprops->na_list[i]);
		}

	} /* End for loop */

	free(command);
	return (overall_stop_status);
}

/*
 * svc_wait():
 *
 * wait for the data service to start up fully and make sure it is running
 * healthy
 */

int
svc_wait(scds_handle_t scds_handle, as_extn_props_t *asextnprops)
{
	int rc, svc_start_timeout, probe_timeout;
	int			asadmin_command_timeout;
	int			number_of_na_running;
	scds_pmf_status_t	status;
	scha_err_t		err, i;

	scds_syslog_debug(DBG_LEVEL_HIGH, "In svc_wait()");

	/*
	 * Get the Start method timeout and the Probe timeout value
	 */
	svc_start_timeout = scds_get_rs_start_timeout(scds_handle);
	probe_timeout = scds_get_ext_probe_timeout(scds_handle);

	asadmin_command_timeout = (svc_start_timeout * ASADMIN_COMMAND_PCT)/100;

	scds_syslog_debug(DBG_LEVEL_HIGH, "The value of "
		"asadmin_command_timeout is %d", asadmin_command_timeout);

	/*
	 * SCMSGS
	 * @explanation
	 * Waiting for the application to startup.
	 * @user_action
	 * This message is informational; no user action is needed.
	 */
	scds_syslog(LOG_NOTICE, "Waiting for %s to startup", APP_NAME);
	/*
	 * sleep for SVC_WAIT_PCT percentage of start_timeout time
	 * before actually probing the dataservice. This is to allow
	 * the dataservice to be fully up in order to reply to the
	 * probe. NOTE: the value for SVC_WAIT_PCT could be different
	 * for different dataservices.
	 */
	if (scds_svc_wait(scds_handle, (svc_start_timeout * SVC_WAIT_PCT)/100)
		!= SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to start service.");
		return (1);
	}

	do {

		scds_syslog_debug(DBG_LEVEL_HIGH,
			"In svc_wait() inside the while "
			"loop checking for NA status");

		number_of_na_running = 0;

		for (i = 0; i < asextnprops->number_of_na; i++) {

			rc = get_na_status(scds_handle, probe_timeout,
					asextnprops, i);
			if (rc == DAS_NOT_RUNNING) {

				scds_syslog_debug(DBG_LEVEL_HIGH,
					"get_na_status failed and "
					"returned %d. The Admin Server "
					"could be down.", rc);

				/*
				 * SCMSGS
				 * @explanation
				 * This error message is from the start
				 * method. After starting the Node Agents, the
				 * start method probes for the Node Agent
				 * using the asadmin list-node-agents command
				 * and makes sure it is UP before declaring
				 * the resource online. The asadmin command
				 * failed because the Domain Admin Server is
				 * unreachable. The Start method will just
				 * verify that the Node Agents PID is
				 * available.
				 * @user_action
				 * If the Domain Admin Server is also a Sun
				 * Cluster resource then set the resource
				 * dependency between the Node Agents resource
				 * and the Domain Admin Server resource. If
				 * the Domain Admin Server is not a Sun
				 * Cluster resource then make sure it is
				 * Running.
				 */
				scds_syslog(LOG_NOTICE,
					"The asadmin list-node-agents failed "
					"to run. Unable to communicate with "
					"the Domain Admin Server %s on port "
					"%d. Probe will just check for Node "
					"Agent <%s> PID.",
					asextnprops->admin_host,
					asextnprops->admin_port,
					asextnprops->na_list[i]);
				/*
				 * We could be here if the Admin Server is down
				 * So we depend on PMF to make sure the PID
				 * still exists.
				 */
				goto check_pmf_status;
			}

			if (rc == NA_NOT_RUNNING) {

				scds_syslog_debug(DBG_LEVEL_HIGH,
					"get_na_status says Node Agent %s "
					"is still NOT up",
					asextnprops->na_list[i]);
				/*
				 * If we are here then the asadmin
				 * list-node-agents lists the status
				 * of the Node Agent as "not running".
				 */
				continue; /* continue to wait */
			}

			if (rc == NO_MEMORY) {
				/*
				 * Out of Memory error.
				 * Message already logged.
				 */

				return (1);
			}

			scds_syslog_debug(DBG_LEVEL_HIGH,
				"get_na_status says Node Agent %s "
				"is running", asextnprops->na_list[i]);

check_pmf_status:
			err = scds_pmf_get_status(scds_handle,
				SCDS_PMF_TYPE_SVC, i, &status);

			if (err == SCHA_ERR_NOERR) {
				scds_syslog_debug(DBG_LEVEL_HIGH,
					"scds_pmf_get_status returns success "
					"for Node agent %s",
					asextnprops->na_list[i]);
				++number_of_na_running;
				continue;
			}

			if (err != SCHA_ERR_NOERR) {

				/* We fail even if one NA fails */
				/*
				 * SCMSGS
				 * @explanation
				 * Failed to create the tag that is used to
				 * register with the process monitor facility.
				 * @user_action
				 * Check the syslog messages that occurred
				 * just before this message. In case of
				 * internal error, save the /var/adm/messages
				 * file and contact authorized Sun service
				 * provider.
				 */
				scds_syslog(LOG_ERR,
					"Failed to retrieve process monitor "
					"facility tag.");
				return (1);
			}
			/* Check if the dataservice is still up and running */
			if (status != SCDS_PMF_MONITORED) {
				/* We fail even if one NA fails */
				/*
				 * SCMSGS
				 * @explanation
				 * This is an informational message. The Probe
				 * detected that the Node Agent is down. The
				 * Probe will either restart the Node Agents
				 * and all the Application Server instances or
				 * Failover to another node.
				 * @user_action
				 * No User action needed.
				 */
				scds_syslog(LOG_ERR,
					"Node Agent %s failed to stay up. ",
					asextnprops->na_list[i]);
				return (1);
			}


		} /* End for loop */

		scds_syslog_debug(DBG_LEVEL_HIGH, "There are %d Node Agents "
			"configured in this resource and so far %d Node Agents "
			"are UP", asextnprops->number_of_na,
			number_of_na_running);

		if (number_of_na_running == asextnprops->number_of_na) {
			scds_syslog_debug(DBG_LEVEL_HIGH,
				"All the %d configured Node Agents are "
				"running", number_of_na_running);
			return (0);
		}

		/*
		 * Dataservice is still trying to come up. Sleep for a while
		 * before probing again.
		 */
		scds_syslog(LOG_NOTICE, "Waiting for %s to startup",
			"Node Agents");

		if (scds_svc_wait(scds_handle, SVC_WAIT_TIME)
			!= SCHA_ERR_NOERR) {
			scds_syslog(LOG_ERR, "Failed to start service.");
			return (1);
		}

	/* We rely on RGM to timeout and terminate the program */
	} while (1);
}

/*
 * This function starts the fault monitor for a jsas resource.  This
 * is done by starting the probe under PMF. The PMF tag is derived as
 * <RG-name,RS-name,instance_number.mon>. The restart option of PMF is
 * used but not the "infinite restart". Instead interval/retry_time is
 * obtained from the RTR file.
 */

int
mon_start(scds_handle_t scds_handle)
{
	scha_err_t	err;

	scds_syslog_debug(DBG_LEVEL_HIGH,
	    "Calling MONITOR_START method for resource <%s>.",
	    scds_get_resource_name(scds_handle));

	/*
	 * The probe jsas_probe is assumed to be available in the same
	 * subdirectory where the other callback methods for the RT
	 * are installed. The last parameter to scds_pmf_start denotes
	 * the child monitor level. Since we are starting the probe
	 * under PMF we need to monitor the probe process only and
	 * hence we are using a value of 0.
	 */

	err = scds_pmf_start(scds_handle, SCDS_PMF_TYPE_MON,
		SCDS_PMF_SINGLE_INSTANCE, "jsas-na_probe", 0);

	if (err != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to start fault monitor.");
		return (err);
	}

	scds_syslog(LOG_INFO, "Started the fault monitor.");

	return (SCHA_ERR_NOERR); /* Successfully started Monitor */
}

/*
 * This function stops the fault monitor for a jsas resource.
 * This is done via PMF. The PMF tag for the fault monitor is
 * constructed based on <RG-name_RS-name,instance_number.mon>.
 */

int
mon_stop(scds_handle_t scds_handle)
{
	scha_err_t	err;

	scds_syslog_debug(DBG_LEVEL_HIGH, "Calling scds_pmf_stop method");

	err = scds_pmf_stop(scds_handle, SCDS_PMF_TYPE_MON,
		SCDS_PMF_SINGLE_INSTANCE, SIGKILL,
		FINAL_STOP_TIMEOUT);

	if (err != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to stop fault monitor.");
		return (err);
	}

	scds_syslog(LOG_INFO, "Stopped the fault monitor.");

	return (SCHA_ERR_NOERR); /* Successfully stopped monitor */
}


/*
* get_as_extn_props()
*
* Get the extension properties set for this Node Agent resource and
* store it in the asextnprops structure. Most of the validation happens
* here itself. The validation that is not done here is done in svc_validate
*
*/

int
get_as_extn_props(scds_handle_t scds_handle, as_extn_props_t *asextnprops,
			boolean_t print_messages)
{
	int	i, err, rc = 0;
	scha_str_array_t 	*confdirs = NULL;
	scha_extprop_value_t	*extnprop = NULL;

	scds_syslog_debug(DBG_LEVEL_HIGH, "In get_as_extn_props() function");

	/*
	 * Get the path to the JSAS Home directory which is the
	 * confdir_list.
	 */

	confdirs = scds_get_ext_confdir_list(scds_handle);

	/*
	 * Return an error if config_dirs extn prop is not set
	 * or if the array_cnt is not equal to 1.
	 * The array_cnt is expected to be 1 as each resource
	 * should configure only one AS Home directory.
	 */

	if (confdirs->str_array == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * The Confdir_list property is not set. The resource creation
		 * is not possible without this property.
		 * @user_action
		 * Set the Confdir_list extension property to the complete
		 * path to the JSAS home directory. Refer to the Configuration
		 * guide for more details.
		 */
		scds_syslog(LOG_ERR,
			"Property Confdir_list is not set.");
		if (print_messages) {
			(void) fprintf(stderr, gettext("Property Confdir_list "
			    "is not set."));
		}
		return (1);
	}
	if (confdirs->array_cnt != 1) {
		/*
		 * SCMSGS
		 * @explanation
		 * Each JSAS resource can handle only a single installation of
		 * JSAS. You cannot create a resource to handle multiple
		 * installations.
		 * @user_action
		 * Create a separate resource for making each installation of
		 * JSAS Highly Available.
		 */
		scds_syslog(LOG_ERR,
			"Only a single path to the JSAS install directory "
			"has to be set in Confdir_list");
		if (print_messages) {
			(void) fprintf(stderr, gettext("Only a single path "
				"to the JSAS install directory has to be "
				"set in Confdir_list.\n"));
		}
		return (1);
	}

	/* Check that Confdir_list[0] is an absolute path. */
	if (confdirs->str_array[0][0] != '/') {
		/*
		 * SCMSGS
		 * @explanation
		 * The entries in Confdir_list must be an absolute path (start
		 * with '/').
		 * @user_action
		 * Create the resource with absolute paths in Confdir_list.
		 */
		scds_syslog(LOG_ERR,
			"Confdir_list must be an absolute path.");
		if (print_messages) {
			(void) fprintf(stderr, gettext("Confdir_list must "
					"be an absolute path."));
		}
		return (1);
	}

	/*
	 * Copy the confdirs extension property into the install_dir
	 * variable of the asextnprops structure.
	 */

	asextnprops->install_dir = confdirs->str_array[0];

	/* Get the Admin User extension property */

	err = scds_get_ext_property(scds_handle, ADMINUSER,
				SCHA_PTYPE_STRING, &extnprop);

	if (err != SCHA_ERR_NOERR || extnprop == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * The extension property could not be retrieved for the
		 * reason that is stated in the message.
		 * @user_action
		 * For details of the failure, check for other messages in
		 * /var/adm/messages.
		 */
		scds_syslog(LOG_ERR,
			"Failed to retrieve the extension property %s: %s.",
			ADMINUSER, scds_error_string(err));
		if (print_messages) {
			(void) fprintf(stderr, gettext("Failed to retrieve the "
			    "extension property %s: %s.\n"), ADMINUSER,
				scds_error_string(err));
		}
		return (1);
	}

	if (extnprop->val.val_str[0] == '\0') {
		scds_syslog(LOG_ERR,
			"Property %s is not set.", ADMINUSER);
		if (print_messages) {
			(void) fprintf(stderr, gettext("Property %s is not "
				"set.\n"), ADMINUSER);
		}
		return (1);
	}

	/* Save the extension property in the asextnprops structure */

	asextnprops->admin_user = extnprop->val.val_str;

	/* Get the extension property Admin_Password_File */

	err = scds_get_ext_property(scds_handle, PASSWORDFILE,
				SCHA_PTYPE_STRING, &extnprop);

	if (err != SCHA_ERR_NOERR || extnprop == NULL) {
		scds_syslog(LOG_ERR,
			"Failed to retrieve the extension property %s: %s.",
			PASSWORDFILE, scds_error_string(err));
		if (print_messages) {
			(void) fprintf(stderr, gettext("Failed to retrieve "
					"the extension property %s: %s.\n"),
					PASSWORDFILE,
					scds_error_string(err));
		}
		return (1);
	}

	if (extnprop->val.val_str[0] == '\0') {
		scds_syslog(LOG_ERR,
			"Property %s is not set.", PASSWORDFILE);
		if (print_messages) {
			(void) fprintf(stderr, gettext("Property %s is not "
				"set.\n"), PASSWORDFILE);
		}
		return (1);
	}

	/* Save the extension property in the asextnprops structure */

	asextnprops->password_file = extnprop->val.val_str;

	/* Get the extension property Agentdir */

	err = scds_get_ext_property(scds_handle, AGENTDIR,
				SCHA_PTYPE_STRING, &extnprop);

	if (err != SCHA_ERR_NOERR || extnprop == NULL) {
		scds_syslog(LOG_ERR,
			"Failed to retrieve the property %s: %s.",
			AGENTDIR, scds_error_string(err));
		if (print_messages) {
			(void) fprintf(stderr, gettext("Failed to retrieve "
					"the extension property %s: %s.\n"),
					AGENTDIR,
					scds_error_string(err));
		}
		return (1);
	}

	/*
	 * If the user has not set the extension property "Agentdir"
	 * then throw an error message.
	 *
	 * In DAS agent the default domain directory is retrieved
	 * from the asenv.conf file. The AS_DEF_DOMAINS_PATH contains
	 * the default path. But, for Node agents there is no
	 * variable defined in the asenv.conf file. Need to file an
	 * RFE with the App Server group to get a variable for the
	 * default Node Agent directory also. Till then force the user
	 * to set it.
	 */

	if (extnprop->val.val_str[0] == '\0') {
		scds_syslog(LOG_ERR,
			"Property %s is not set.", AGENTDIR);
		if (print_messages) {
			(void) fprintf(stderr, gettext("Property %s is not "
				"set.\n"), AGENTDIR);
		}
		return (1);
	}

	/*
	 * Set the Node Agent Directory to whatever the
	 * user has set
	 */

	asextnprops->agentdir = extnprop->val.val_str;

	/*
	 * Get the Node Agents that need to be started in this
	 * resource group. If this is a Failover resource group
	 * the Node Agents list contains all the Node Agents that
	 * are configured for the Failover IP address in this
	 * resource group. If this is a Multi Master Resource Group
	 * then the Node Agents list contains all the Node Agents that
	 * are configured for the local physical hostname.
	 */

	rc = get_node_agents_list(scds_handle, asextnprops,
			print_messages);

	if (rc != 0) {
		/* Message already logged */
		return (1);
	}

	/* list the node agents configured on this host */
	for (i = 0; i < asextnprops->number_of_na; i++) {
		scds_syslog_debug(DBG_LEVEL_HIGH, "back in "
			"get_as_extn_props() the %d node agent "
			"is %s", i, asextnprops->na_list[i]);
	}

	err = scds_get_ext_property(scds_handle, ADMINHOST,
		SCHA_PTYPE_STRING, &extnprop);

	if (err != SCHA_ERR_NOERR || extnprop == NULL) {
		scds_syslog(LOG_ERR,
			"Failed to retrieve the extension property %s: %s.",
			ADMINHOST, scds_error_string(err));
		return (1);
	}

	if (extnprop->val.val_str[0] == '\0') {
		scds_syslog(LOG_ERR,
			"Property %s is not set.", ADMINHOST);
		if (print_messages) {
			(void) fprintf(stderr, gettext("Property %s is not "
				"set.\n"), ADMINHOST);
		}
		return (1);
	}

	scds_syslog_debug(DBG_LEVEL_HIGH,
		"The Admin host set in the extn prop is %s",
		extnprop->val.val_str);

	asextnprops->admin_host = extnprop->val.val_str;

	/* Get the Admin Server Port from the extn props */

	err = scds_get_ext_property(scds_handle, ADMINPORT,
		SCHA_PTYPE_INT, &extnprop);

	if (err != SCHA_ERR_NOERR || extnprop == NULL) {
		scds_syslog(LOG_ERR,
			"Failed to retrieve the extension property %s: %s.",
			ADMINPORT, scds_error_string(err));
		return (1);
	}

	scds_syslog_debug(DBG_LEVEL_HIGH,
		"The Admin port set in the extn prop is %d",
		extnprop->val.val_int);

	asextnprops->admin_port = extnprop->val.val_int;


	/*
	 *If we are here then we have all the extension properties needed
	 */

	scds_syslog_debug(DBG_LEVEL_LOW,
		"The App Server Install directory is <%s> "
		"the Admin User name is <%s> "
		"the Admin Password File is <%s> "
		"the Agent Install Directory is <%s> "
		"The DAS host is <%s> "
		"The DAS port is <%d> ",
		asextnprops->install_dir,
		asextnprops->admin_user,
		asextnprops->password_file,
		asextnprops->agentdir,
		asextnprops->admin_host,
		asextnprops->admin_port);

	return (0);
}



int
get_node_agents_list(scds_handle_t scds_handle,
	as_extn_props_t *asextnprops, boolean_t print_messages)
{
	char		*na_ip_address;
	char		*cmd;
	FILE		*fp;
	const char	*rtbasedir;
	int		i, rc = 0;
	scds_net_resource_list_t	*snrlp = NULL;

	scds_syslog_debug(DBG_LEVEL_HIGH, "In the function "
		"get_node_agents_list()");
	/*
	 * Get the Failover IP address for the Node Agents configured
	 * in this RG.
	 * Return an error if there is an error when trying to get the
	 * available network address resources for this resource
	 * and if no Failover IP resources are created in this RG.
	 */
	if ((rc = scds_get_rs_hostnames(scds_handle, &snrlp))
		!= SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		    "Error in trying to access the configured network "
		    "resources : %s.", scds_error_string(rc));
		if (print_messages) {
			(void) fprintf(stderr,
				gettext("Error in trying to access the "
					"configured network "
					"resources : %s."),
				scds_error_string(rc));
		}
		return (1);
	}

	/*
	 * If there are no Failover IP resources in the RG
	 * or if there are more than ONE failover IP address
	 * in this RG then it is an incorrect configuration.
	 */

	/*
	 * IMPORTANT NOTE: When the Multi Master configuration
	 * is supported this functionality has to change. There
	 * will be no IP resources in the RG. The Node Agents
	 * will have to be assumed to have been configured on
	 * the physical IP address. Change the code accordingly
	 * at that time.
	 */

	if (snrlp->num_netresources != 1) {
		/*
		 * SCMSGS
		 * @explanation
		 * There must be only one Failover IP address resource in this
		 * resource group. The Failover host for which the Node agents
		 * are configured must be created in this resource group.
		 * @user_action
		 * Create the Failover host resource, on which the Node Agent
		 * and the Application Server instances are configured, in
		 * this resource group.
		 */
		scds_syslog(LOG_ERR,
			"There should be one Network Resource "
			"in this resource group.");
		if (print_messages) {
			(void) fprintf(stderr,
				gettext("There should be "
				    "one Network Resource in this Resource "
				    "Group"));
		}
		return (1);
	}

	na_ip_address = strdup(snrlp->netresources->hostnames[0]);

	if (na_ip_address == NULL) {
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
			"Failed to allocate space for"
			"Node Agent IP Address");
		if (print_messages) {
			(void) fprintf(stderr,
				gettext("Failed to allocate space for"
				"Node Agent IP Address"));
		}
		return (1);
	}

	scds_syslog_debug(DBG_LEVEL_HIGH, "The value of the IP address in this "
		"resource group is %s", na_ip_address);


	/* Get the RT base directory */
	rtbasedir = scds_get_rt_rt_basedir(scds_handle);

	cmd = ds_string_format("%s/%s %s %s 2>/dev/null",
		rtbasedir, "jsas-get-na-list", asextnprops->agentdir,
		na_ip_address);

	if (cmd == NULL) {
		if (print_messages)
			(void) fprintf(stderr, gettext("Out of memory.\n"));
		return (1);
	}
	scds_syslog_debug(DBG_LEVEL_HIGH, "The command used to get the "
		"node agents list is %s", cmd);

	fp = popen(cmd, "r");
	if (fp == NULL) {
		scds_syslog(LOG_ERR, "Unable to to run %s: %s.", cmd,
			strerror(errno));
		if (print_messages)
			(void) fprintf(stderr,
				gettext("Unable to run %s: %s.\n"), cmd,
				strerror(errno));
		free(cmd);
		return (1);
	}

	i = 0;
	while (fgets(asextnprops->na_list[i], 1024, fp) != NULL) {
		scds_syslog_debug(DBG_LEVEL_HIGH, "The %d node agent is %s",
			i, asextnprops->na_list[i]);
		i++;
	}
	asextnprops->number_of_na = i;

	scds_syslog_debug(DBG_LEVEL_HIGH, "There are totally %d Node Agents "
		"configured on the ip address %s", asextnprops->number_of_na,
		na_ip_address);

	rc = pclose(fp);

	if (WIFEXITED((uint_t)rc) && (WEXITSTATUS((uint_t)rc) != 0)) {
		/*
		 * SCMSGS
		 * @explanation
		 * An internal error has occurred.
		 * @user_action
		 * Save a copy of the /var/adm/messages files on all nodes.
		 * Contact your authorized Sun service provider for assistance
		 * in diagnosing the problem.
		 */
		scds_syslog(LOG_ERR,
			"INTERNAL ERROR: Command %s Failed.", cmd);
		free(cmd);
		return (1);
	}

	if (asextnprops->number_of_na == 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * There are no Node Agents configured on the Logical Host
		 * that is created in this Resource Group. There are no Node
		 * agents to be brought online in this resource group.
		 * @user_action
		 * Change the configuration so that the Node Agents listen on
		 * the logical host that you have in the Resource Group or
		 * change the logical host resource to the correct logical
		 * host that the Node Agents use.
		 */
		scds_syslog(LOG_ERR,
			"There are no Node Agents configured on %s "
			"in the directory %s.",
			na_ip_address, asextnprops->agentdir);
		if (print_messages)
			(void) fprintf(stderr,
				gettext("There are no Node Agents "
				"configured on %s "
				"in the directory %s"),
				na_ip_address, asextnprops->agentdir);
		free(cmd);
		return (1);
	}

	free(cmd);
	return (0);

}

/*
 * Function to get the status of the Node Agent .
 * We call jsas_utils script where the actual
 * asadmin list-node-agents command is run.
 * return 0 if Node Agent is running  or 1 if Node Agent
 * is "not running" or if the command times out.
 * We return 3 for "out of memory" errors.
 */

int
get_na_status(scds_handle_t scds_handle, int timeout,
		as_extn_props_t *asextnprops, int i)
{

	int		rc;
	char		*cmd = NULL;
	const char	*rt_base_dir;
	int		exit_code = 0;


	scds_syslog_debug(DBG_LEVEL_HIGH, "In get_na_status() "
		"function");

	rt_base_dir = scds_get_rt_rt_basedir(scds_handle);

	cmd = ds_string_format("%s/jsas-na_utils %s %s %d %s %s %s",
		rt_base_dir,
		asextnprops->install_dir,
		asextnprops->admin_host,
		asextnprops->admin_port,
		asextnprops->admin_user,
		asextnprops->password_file,
		asextnprops->na_list[i]);

	if (cmd == NULL) {
		ds_internal_error("Out of Memory");
		return (3);
	}

	scds_syslog_debug(DBG_LEVEL_HIGH, "The command used to "
		"get the status of Node Agents is %s", cmd);

	rc = scds_timerun(scds_handle, cmd, timeout,
			SIGKILL, &exit_code);

	if (rc != SCHA_ERR_NOERR) {
		if (rc == SCHA_ERR_TIMEOUT) {
			scds_syslog_debug(DBG_LEVEL_HIGH,
				"scds_timerun in get_das_status() "
				"returned SCHA_ERR_TIMEOUT");
			free(cmd);
			return (1);
		} else {
			scds_syslog_debug(DBG_LEVEL_HIGH,
				"Failed to run the asadmin command");
			free(cmd);
			return (1);
		}
	}

	free(cmd);
	return (exit_code);

}
