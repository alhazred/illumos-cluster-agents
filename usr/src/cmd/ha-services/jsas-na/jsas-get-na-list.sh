#!/bin/sh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.

#pragma ident	"@(#)jsas-get-na-list.sh	1.5	07/06/06 SMI"

#
# What:
#	This script is used to get the list of Node Agents
#	that are configured to listen on the HOSTNAME
#	passed as an argument to this list.
#
# How:
#	All the Node Agents are created under the Node Agents
#	directory nodeagents_dir. E.g the Node agents directory
#	structure is as shwon below 
#	root@pnode1% pwd
#	/global/dg1/na-dir
#	root@pnode1% ls
#	na1/  na2/  na3/
#	root@pnode1% 
#	The nodeagents.properties file under 
#	<nodeagent_name>/agent/config/nodeagent.properties
#	has a variable agent.client.host which is set to the
#	hostname on which the nodeagent is configured.
#
#	This script lists all the Nodeagents that are configured 
#	on the HOSTNAME, which is passed as a parameter to this
#	script.
#	
#	echo the Node agents that are configured on $HOSTNAME
#	on the stdout.
#	exit 1 if there is a failure or if no Node agent is 
#	configured to listen on the $HOSTNAME.
#set -x

PATH=/bin:/usr/bin:/usr/cluster/bin:/usr/sbin:/usr/cluster/lib/sc:$PATH;export PATH
syslog_tag="SC[SUNW.jsas,jsas-get_node_agents_list]"

# Checking the command syntax

if [ $# -ne 2 ]
then
	# SCMSGS
	# @explanation
	# Need explanation of this message!
	# @user_action
	# Need a user action for this message.
	scds_syslog -p error -t $syslog_tag -m \
		 "INTERNAL ERROR usage:$0 nodeagents_dir nodeagents-hostname"
	exit 1
fi


NODEAGENTS_DIR=$1
HOSTNAME=$2

count=1

for file in $NODEAGENTS_DIR/*
do
	if [ ! -f $file/agent/config/nodeagent.properties ]
	then
		exit 1
	fi

	client_host=`/usr/bin/grep agent.client.host $file/agent/config/nodeagent.properties | /usr/bin/nawk -F= '{print $2}'`

	if [ $client_host = $HOSTNAME ]
	then
		count=`expr $count + 1`
		agentname=`basename $file`
		/usr/bin/echo "$agentname"
	fi

done

if [ $count -eq 0 ]
then
	exit 1
else
	exit 0
fi
