#!/bin/sh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.

#pragma ident	"@(#)jsas-na_utils.sh	1.6	07/08/07 SMI"

#
# What:
#	This script is used to check the status of the Node Agent
#	passed as an argument to this script
#
#	The command used to get the status of the Node Agent is
#	asadmin list-node-agents --host <admin host> --port <Admin port> 
#	 --user <Admin user> --passwordfile <Admin Password File> <Node Agent Name>
#	
# Return Values:
#	If the Admin Server is not available or if there is some other problem 
#	then the above command fails with a return code 1. This could happen if
#	the Admin Server is failing over or is just starting up.
#	This script will return 1 so that the calling function knows that the 
#	asadmin command failed.	The probe can then just check for the PID. 
#	If the PID is also not available then it can be considered as a Failure.
#
#	If the above command runs successfully and the Node Agent is not in
#	the running state then this script returns 2.
#
#	If the Node Agentis running then the return code is 0
#	
#
#set -x

PATH=/bin:/usr/bin:/usr/cluster/bin:/usr/sbin:/usr/cluster/lib/sc:$PATH;export PATH
syslog_tag="SC[SUNW.jsas,jsas_utils]"

# Checking the command syntax

if [ $# -ne 6 ]
then
	# SCMSGS
	# @explanation
	# Need explanation of this message!
	# @user_action
	# Need a user action for this message.
	scds_syslog -p error -t $syslog_tag -m \
		 "INTERNAL ERROR usage:$0 install_dir adminhost adminport adminuser admin passwordfile NA-name"
	exit 1
fi


INSTALL_DIR=$1
ADMIN_HOST=$2
ADMIN_PORT=$3
USER_NAME=$4
PASSWORDFILE=$5
NODEAGENT_NAME=$6

# First make sure the asadmin command runs successfully. If the Admin Server is not available 
# or if there is some other problem the command fails and returns an exit code 1.
if [ -f "$INSTALL_DIR/bin/asadmin" ]
then
        ASADMIN=$INSTALL_DIR/bin/asadmin
else
        ASADMIN=$INSTALL_DIR/appserver/bin/asadmin
fi
output=`$ASADMIN list-node-agents --host $ADMIN_HOST --port $ADMIN_PORT --user $USER_NAME --passwordfile $PASSWORDFILE $NODEAGENT_NAME 2>&1 `

rc=$?
if [ $rc -ne 0 ]
then
#If we get here then probably the Admin Server is not running
	exit 1
fi

#If we are here then the asadmin list-node-agents command completed successfully
#The string in output will tell us if the Node Agent is running or not.

/usr/bin/echo $output | /usr/bin/grep -v "not running" | /usr/bin/grep -iv "starting" > /dev/null
rc=$?

if [ $rc -ne 0 ]
then
#If we are here then the Node Agent is not running
	exit 2
else
#The Node Agent is in the running state
	exit 0
fi
