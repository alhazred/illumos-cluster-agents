/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#pragma ident	"@(#)jsas-na_svc_start.c	1.7	07/06/06 SMI"

#include <rgm/libdsdev.h>
#include "jsas-na.h"

/*
 * The start method for s1as. Does some sanity checks on
 * the resource settings then starts the s1as under PMF with
 * an action script.
 */

int
main(int argc, char *argv[])
{
	scds_handle_t	scds_handle;
	int		rc, i;
	char		*rgname, *rsname;
	as_extn_props_t	asextnprops;

	/*
	 * Process all the arguments that have been passed to us from RGM
	 * and do some initialization for syslog
	 */

	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR) {
		return (1);
	}

	rsname = (char *)scds_get_resource_name(scds_handle);
	rgname = (char *)scds_get_resource_group_name(scds_handle);

	/* Get the App Server extension properties */

	if (get_as_extn_props(scds_handle, &asextnprops, B_FALSE) != 0) {
		/* Error messages already logged */
		rc = 1;
		goto finished;
	}

	/* Validate the configuration and if there is an error return back */
	rc = svc_validate(&asextnprops, B_FALSE);
	if (rc != 0) {
		scds_syslog(LOG_ERR,
			"Failed to validate configuration.");
		goto finished;
	}

	/*
	 * Before starting the Node Agents make sure the Admin
	 * Server is Running and that the Node Agents are not
	 * started outside sun cluster. If the Admin Server is
	 * not running then throw a error message  and return
	 * success so that the PROBE method will wait till the
	 * Admin Server is UP and then start the Node Agents.
	 * If the Node Agents are running outside Sun cluster
	 * then Bailout.
	 */

	for (i = 0; i < asextnprops.number_of_na; i++) {

		rc = get_na_status(scds_handle,
			scds_get_rs_start_timeout(scds_handle),
			&asextnprops, i);

		if (rc == NA_RUNNING) {
			scds_syslog(LOG_ERR,
				"%s is already running on this "
				"node outside of Sun Cluster. "
				"The start of %s from Sun Cluster will "
				"be aborted.",
				asextnprops.na_list[i],
				asextnprops.na_list[i]);
			rc = scha_control(SCHA_IGNORE_FAILED_START,
				rgname, rsname);
			(void) scha_resource_setstatus(rsname, rgname,
					SCHA_RSSTATUS_FAULTED,
					"Offline - Node agent running "
					"outside Sun Cluster.");
			return (2); /* Bail Out */
		}

		if (rc == DAS_NOT_RUNNING) {
			/*
			 * SCMSGS
			 * @explanation
			 * Before Starting the Node Agents, the start method
			 * checks for the status of the Domain Admin Server.
			 * The Node Agents are started only if the Domain
			 * Admin Server is UP. This error indicates that the
			 * Domain Admin Server is not running. The resource
			 * will be brought online but the start method will
			 * not start the Node Agents. The probe will monitor
			 * the status of the Domain Admin Server and will
			 * start the Node Agents once the Domain Admin Server
			 * is accessible.
			 * @user_action
			 * If the Domain Admin Server is also a suncluster
			 * resource then set the resource dependency between
			 * the Node Agents resource and the Domain Admin
			 * Server resource. If the Domain Admin Server is not
			 * a sun cluster resource then make sure it is
			 * Running.
			 */
			scds_syslog(LOG_ERR,
				"The asadmin list-node-agents command "
				"failed to run. Unable to communicate with "
				"the Domain Admin Server %s on port %d. "
				"The Probe method will wait for the Domain "
				"Admin Server to be UP before starting the "
				"Node Agents. ", asextnprops.admin_host,
				asextnprops.admin_port);
			(void) scha_resource_setstatus(rsname, rgname,
				SCHA_RSSTATUS_DEGRADED,
				"Unable to contact Admin Server. Node Agents "
				"not started");
			return (0);
		}
	}

	scds_syslog_debug(DBG_LEVEL_HIGH,
	    "The Admin Server is Running and the Node Agents are NOT "
	    "started outside cluster.We can safely Start the Node Agents");

	/* Start the data service, if it fails return with an error */
	rc = svc_start(scds_handle, &asextnprops);

	if (rc != 0) {
		goto finished;
	}

	/* Wait for the service to start up fully */
	rc = svc_wait(scds_handle, &asextnprops);
	scds_syslog_debug(DBG_LEVEL_HIGH, "Returned from svc_wait");

finished:
	if (rc != SCHA_ERR_NOERR)
		(void) scha_resource_setstatus(rsname, rgname,
			SCHA_RSSTATUS_FAULTED,
			"Failed to start the Node Agents.");
	else
		(void) scha_resource_setstatus(rsname, rgname,
			SCHA_RSSTATUS_OK,
			"Successfully started the Node Agents.");

	/* Free up the Environment resources that were allocated */
	scds_close(&scds_handle);

	return (rc);
}
