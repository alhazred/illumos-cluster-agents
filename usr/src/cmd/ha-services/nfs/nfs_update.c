/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 1998-2004 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * nfs_update.c - Update method for highly available NFS
 */

#pragma ident	"@(#)nfs_update.c	1.19	07/06/06 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <rgm/libdsdev.h>
#include "hanfs.h"

/*
 * Called when updating NFS resources.
 * Just restart the system level fault monitor and the resource fault monitor.
 */

int
main(int argc, char *argv[])
{
	scds_handle_t	scds_handle;
	char		cmd[SCDS_CMD_SIZE];
	int		rc;

	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR) {
		return (1);
	}

	/* Restart nfs_daemons_probe if it is running */
	(void) sprintf(cmd, "%s -q %s", SCDS_PMFADM, NFS_DAEMONS_PMFTAG);
	rc = run_system(HANFS_LOG_NONE, cmd);
	if (rc == 0) {
		if (stop_system_monitor() != 0) {
			/* Messages already logged */
			return (1);
		}

		if (start_system_monitor(scds_handle) != 0) {
			/* Messages already logged */
			return (1);
		}
	}

	rc = scds_pmf_restart_fm(scds_handle, 0);
	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		    "Failed to restart fault monitor.");
		return (1);
	}

	/* Successful updation */
	scds_syslog(LOG_INFO,
	    "Completed successfully.");
	return (0);
}
