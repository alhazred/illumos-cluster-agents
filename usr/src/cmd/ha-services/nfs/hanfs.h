/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_HANFS_H
#define	_HANFS_H

#pragma ident	"@(#)hanfs.h	1.38	08/10/08 SMI"

#include <rgm/libdsdev.h>

#ifdef __cplusplus
extern "C" {
#endif

#include <syslog.h>
#include <limits.h>
#include <libintl.h>
#include <stdarg.h>
#include <errno.h>
#include <string.h>
#include <sys/wait.h>
#include <arpa/inet.h>
#include <rpc/types.h>


/*
 * Operations to be performed on share paths in dfstab
 * For a detailed description of these, look at the
 * comments at the beginning of share_to_mountp.c
 */
#define	NFS_OP_SHARE		0
#define	NFS_OP_UNSHARE		1
#define	NFS_OP_CHECK		2
#define	NFS_OP_VALIDATE		3
#define	NFS_OP_CHKSHARE		4
#define	NFS_OP_RDCHKSHARE	5

#define	NFS_RPCBIND_PATH	"/usr/sbin/rpcbind"
#define	NFS_NFSD_PATH		"/usr/lib/nfs/nfsd"
#define	NFS_MOUNTD_PATH		"/usr/lib/nfs/mountd"
#define	NFS_LOCKD_PATH		"/usr/lib/nfs/lockd"
#define	NFS_STATD_PATH		"/usr/lib/nfs/statd"

/* Solaris NFS daemons configuration file location */
#define	NFSADMIN		"/etc/default/nfs"

/* Location of pmfadm binary */
#define	SCDS_PMFADM		"usr/cluster/bin/pmfadm"

/* Return code from nfs_fm_history() */
#define	NFS_HISTORY_NOACTION	0	/* No Action */
#define	NFS_HISTORY_RESTART		1	/* restart */
#define	NFS_HISTORY_FAILOVER	2	/* Failover RG */
#define	NFS_HISTORY_ERROR		3	/* No memory perhaps */
#define	NFS_HISTORY_NOACTION_DEG	4	/* Degraded, no action */

int nfs_fm_history(scds_handle_t h, int status);


/* pmf tag for the system level fault monitor: nfs_daemons_probe */
#define	NFS_DAEMONS_PMFTAG	"cluster.nfs.daemons"

/* A directory where HA NFS keeps its working files */
#define	HANFS_STATE_DIR	"/var/run/.hanfs"

/* Path to 'sharetab' file */
#define	SHARETAB	"/etc/dfs/sharetab"

/*
 * Name of the subdirectory under which
 * the "Pathprefix doesn't exist" file
 * is created. The name of the file is
 * the resource name.
 */
#define	NO_PATHPREFIX_SUBDIR	"no-pathprefix"

/*
 * HANFS_STATE_FILE: Contains the info about all the nfs
 * resource on-line on the current node.
 * The format of the state file is
 * RG-name R-name statmon-dir-path ipaddr1 ipaddr2 ipaddr3...
 */
#define	HANFS_STATE_FILE "nfs.state"

/* HANFS_LOCK_FILE: Used to serialize methods of different resources */
#define	HANFS_LOCK_FILE  "nfs.lock"

/* HANFS_PROC_STATEFILE: Use the timestamp of this file to check if */
/*	nfs daemon has been restart */
#define	HANFS_PROC_STATEFILE  "proc_statefile"

/* HANFS_DOWN_FILE: created when nfs daemon restart failed and removed */
/*	when probe all daemons succueeds */
#define	HANFS_DOWN_FILE  "nfs.down"

/* IPV4/IPV6 masks */
#define	IPV4MASK	0x1
#define	IPV6MASK	0x2

/* GIVEOVER_FILE: created when nfs failover of all NFS resources groups is */
/*	attempted */
#define	GIVEOVER_FILE  "nfs.giveover"

/* syslog priority for run_system() which means don't syslog message */
#define	HANFS_LOG_NONE	-1

/* Exit code for empty dfstab file of a HA-NFS resource */
#define	HANFS_EMPTY_DFSTAB	-1

/* op code input for getset_file_mtime() */
#define	NFS_OP_GET		0
#define	NFS_OP_SET		1

typedef struct shared_path_list {
	char		*fs_path;	/* shared file system on server */
	int		fs_status;	/* result of previous probe */
	struct shared_path_list	*fs_next;
} shared_path_list_t;

typedef struct sharelist {
	char		*shr_path;
	struct sharelist	*shr_next;
}  sharelist_t;

/* Possible values of fs_status field in struct shared_path_list */
#define	HANFS_FS_OK		0
#define	HANFS_FS_STAT_FAILED	1

typedef int nfs_fm_err_t;

#define	NFS_ERR_NONE		0	/* no error was found */
#define	NFS_ERR_PID_NOT_EXIST	1	/* process dies */
#define	NFS_ERR_NULL_RPC_FAILED	2	/* null rpc call failed */
#define	NFS_ERR_UNKNOWN		3	/* mostly internal error */

/*
 * RPC program numbers for various demons that we want to probe.
 * We list these here to avoid depending on how the name service
 * is configured, or which name service is used.  Downside is
 * that these program numbers could, in theory, change.  But
 * that is unlikely for interoperability reasons.
 */
#define	RPC_PROGNUM_RPCBIND	((rpcprog_t)100000)
#define	RPC_PROGNUM_NFSD	((rpcprog_t)100003)
#define	RPC_PROGNUM_MOUNTD	((rpcprog_t)100005)
/* Lockd is the "nlockmgr" program (n for network): */
#define	RPC_PROGNUM_LOCKD	((rpcprog_t)100021)
/* Statd is the "status" program ("statmon" is something else) */
#define	RPC_PROGNUM_STATD	((rpcprog_t)100024)

/* Use the highest numbers in Solaris 2.8 */
#define	RPC_VERSION_RPCBIND	((rpcvers_t)4)
#define	RPC_VERSION_NFSD	((rpcvers_t)3)
#define	RPC_VERSION_MOUNTD	((rpcvers_t)3)
#define	RPC_VERSION_LOCKD	((rpcvers_t)4)
#define	RPC_VERSION_STATD	((rpcvers_t)1)

#define	SCDS_CMD_SIZE		2048
#define	SCDS_ARRAY_SIZE		2048
#define	INT_ERR_MSG_SIZE	1024

extern char *svcs[];
/*
 * nfs_register_resource(): Adds the info for the given resource
 * into the nfs state file on the node. Called when a resource
 * is being brought on-line.
 */

int nfs_register_resource(scds_handle_t scds_handle);

/* Remove the info from the NFS state file */
int nfs_unregister_resource(scds_handle_t  scds_handle);

char *get_dfstab_filename(scds_handle_t scds_handle, boolean_t print_messages);
/*
 * Run the share/unshare commands specified in a given file
 * with dfstab syntax. Checks in mnttab and vfstab to make
 * sure that what is being shared makes sense.
 * op is NFS_OP_SHARE or NFS_OP_UNSHARE
 * XXX Make sure it is a PXFS file system?
 */
int nfs_share_dfstab(char *dfstabfile, int op);

/*
 * Parses dfstab of a nfs resource and returns a list of shared paths and
 * each path has one of the following status:
 *	HANFS_FS_OK - file system is ok
 *	HANFS_FS_STAT_FAILED - stat() on the path failed
 */
int nfs_get_fslist(scds_handle_t scds_handle, shared_path_list_t **fsl);

/*
 * Validate some basic properties of a nfs resource
 * The PATHPREFIX property should be set on the containing
 * RG and should point to a valid path on a PXFS file system.
 * The RG should contain at least one network resource.
 */
int svc_validate(scds_handle_t scds_handle, boolean_t print_messages);
int mon_start(scds_handle_t scds_handle);
int mon_stop(scds_handle_t scds_handle);
int svc_probe(scds_handle_t scds_handle, char *dfs_path,
	shared_path_list_t **fslp, int dtabtouched, int ismvtabtouched,
	time_t *last_cache_msts);

int nfs_serialize_lock(void);
int nfs_serialize_unlock(int lockid);

int stop_lockd_statd(time_t timeout);
int start_statd_lockd(scds_handle_t scds_handle,
	time_t timeout);
int nfs_share_resource(scds_handle_t  scds_handle);
int nfs_unshare_resource(scds_handle_t  scds_handle);
int start_nfsd_mountd_nowait(scds_handle_t scds_handle);
int start_nfsd_mountd(scds_handle_t scds_handle,
	time_t timeout);
int stop_nfsd_mountd(time_t timeout);
nfs_fm_err_t checkproc(char *procname, pid_t *pid);
int run_system(int pri, char *cmd);
int startproc(char *procname, char *args);
boolean_t is_nfs_tcp_only(void);
int start_nfs_daemon(scds_handle_t h, char *progname,
	rpcprog_t prognum, rpcvers_t progversion, time_t timeout);
int stop_nfs_daemon(char *progname, rpcprog_t prognum,
	rpcvers_t progversion, time_t timeout);
int start_system_monitor(scds_handle_t scds_handle);
int stop_system_monitor(void);

int getset_file_mtime(int op);

int create_stop_file(char *r_name);
int remove_stop_file(char *r_name);
int check_stop_file(void);
int check_pathprefix(scds_handle_t scds_handle);
int is_nfsd_active(boolean_t *is_active);
int is_process_stopped(pid_t pid);
int create_giveover_file(void);
int check_file(char *fname, off_t *size);
int remove_giveover_file(void);
int nfsd_has_warmstart(void);

boolean_t is_startd_duration_transient(char *fmri, boolean_t init_flag);
boolean_t get_application_auto_enable(char *fmri);
int set_startd_duration_transient(char *fmri);
int set_application_auto_enable_false(char *fmri);
int update_smf_services_for_ha(void);

#ifdef	__cplusplus
}
#endif

#endif /* _HANFS_H */
