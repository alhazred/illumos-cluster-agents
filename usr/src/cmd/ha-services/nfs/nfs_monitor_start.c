/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 1998-2002 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * nfs_monitor_start.c - Monitor start method for highly available NFS
 */

#pragma ident	"@(#)nfs_monitor_start.c	1.11	07/06/06 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "hanfs.h"

/*
 * There are two parts of hanfs fault monitore:
 * 	nfs_probe_daemons - monitors nfs daemons, one per node.
 * 	nfs_probe - monitors exported nfs file systems, one per resource.
 * Both are started under PMF.
 */

int
main(int argc, char *argv[])
{
	scds_handle_t	scds_handle;
	int		rc;

	/* Process arguments passed by RGM and initialize syslog */
	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR) {
		return (1);
	}

	rc = mon_start(scds_handle);

	/* Free up all the memory allocated by scds_initialize */
	scds_close(&scds_handle);

	/* Return the result of monitor_start method */
	return (rc);
}
