/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 1998-2003 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * nfs_svc_stop.c - Stop method for highly available nfs
 */

#pragma ident	"@(#)nfs_svc_stop.c	1.24	07/06/06 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include "hanfs.h"

/*
 * This method is called BEFORE the Logical ipaddresses have been
 * brought down on this node. We can't do the unshare commands
 * here (or else the clients might see a ESTALE if they happen
 * to come in BEFORE the ipaddresses are gone from this node).
 * We stop nfsd/mountd here...
 *
 */

int
main(int argc, char *argv[])
{
	int		lockid, rc;
	scds_handle_t	scds_handle;
	char		*rsname = NULL, *rgname = NULL;

	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR) {
		return (1);
	}

	/* Get resource and resource group names */
	rsname = (char *)scds_get_resource_name(scds_handle);
	rgname = (char *)scds_get_resource_group_name(scds_handle);

	/* Check to see if Pathprefix is usable */
	rc = check_pathprefix(scds_handle);
	if (rc == 0) {
		/*
		 * Patheprefix doesn't exist, but exit
		 * success anyway, as the start methods
		 * were not successful either.
		 */
		return (0);
	}
	if (rc < 0) {
		/*
		 * Patheprefix doesn't exist, no
		 * point in continuing. exit Failure.
		 */
		return (1);
	}

	/* Grab the system-wide lock. Waits... */
	lockid = nfs_serialize_lock();

	/* Stop nfsd/mountd, no timeout */
	scds_syslog(LOG_NOTICE, "Stopping %s.", "nfsd and mountd");
	rc = stop_nfsd_mountd(-1);
	if (rc != 0) {
		/* This sounds like a serious problem */
		(void) scha_resource_setstatus(rsname, rgname,
			SCHA_RSSTATUS_FAULTED,
			"Failed to stop daemon nfsd/mountd.");
		rc = 1;
		goto finished;
	}
	/* Create a stop file */
	(void) create_stop_file((char *)
		scds_get_resource_name(scds_handle));

	/* Update the timestamp of proc_statefile to let the */
	/* fault monitor know that nfs daemons have been restarted. */
	(void) getset_file_mtime(NFS_OP_SET);

finished:
	/* Release the lock. See the comment in nfs_svc_start.c */
	(void) nfs_serialize_unlock(lockid);

	return (rc);
}
