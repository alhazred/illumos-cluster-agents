/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#pragma ident	"@(#)share_to_mountp.c	1.37	08/10/08 SMI"


/*
 * For each entry in the dfstab file, if an ancestor of the entry is in
 * the vfstab file, then check if said vfstab entry is currently mounted
 * AND the dfstab entry itself exists. If so, we share the entry.
 * If not, we'll syslog the entry as not mounted, and therefore,
 * cannot be shared.
 *
 * Note that an entry in the dfstab file that doesn't have an ancestor
 * in the vfstab file but which does exist is also shared.
 * (This is a hack to benefit the ha sendmail implementation.)
 *
 * The algorithm is like this: for each path name in the dfstab,
 * find the longest matching mount point from the vfstab as the
 * mount point. Then, we'll look through the /etc/mnttab file to
 * look for the mount point. If it's found, we know the filesystem
 * is mounted.
 *
 */

#include <stdio.h>
#include <strings.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/statvfs.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mnttab.h>
#include <sys/mntent.h>
#include <sys/vfstab.h>
#include <errno.h>
#include <syslog.h>

#include "hanfs.h"

/* Bits in the mount_status member */
#define	HANFS_MOUNTED	0x0001
#define	HANFS_GLOBAL	0x0002

/* Maximum line size of the sharetab file used by nfs */
#define	MAX_SHRTAB_LINESZ	65536

struct vfslist {
	char	*vpath;
	int		mount_status;
	struct vfslist *next;
};

static const char	sepstr[] = " \t\n";
static char *vtab_path;
static char *dtab_path;

/* The list of shared paths */
sharelist_t *shrlist = NULL;

static void
printvfslist(struct vfslist *li)
{
	scds_syslog_debug(2, "printvfslist:.");
	for (; li != NULL; li = li->next) {
		scds_syslog_debug(2, "'%s' %d\n", li->vpath, li->mount_status);
	}
	scds_syslog_debug(2, "[end of printvfslist].");
}

/*
 * vlist_build()
 * Uses getvfsent() to get a list of all filesystem
 * entries in the specified file (/etc/vfstab).
 */
static struct vfslist *
vlist_build(FILE *vfp)
{
	struct vfstab   vget;
	struct vfslist  *vent, *prevp, *firstp;

	prevp = firstp = NULL;
	while (getvfsent(vfp, &vget) != -1) {
		if (vget.vfs_mountp) {
			/* root "/" should not be ignored */
			vent = (struct vfslist*) malloc(sizeof (*vent));
			if (vent == NULL) {
				scds_syslog(LOG_ERR,
				    "Out of memory.");
				exit(1);
			}
		    vent->mount_status = 0;
		    vent->next = 0;
		    vent->vpath = strdup(vget.vfs_mountp);
		    if (vent->vpath == NULL) {
				scds_syslog(LOG_ERR,
				    "Out of memory.");
				exit(1);
			}
		    if (prevp == NULL)
				firstp = prevp = vent;
		    else {
				prevp->next = vent;
				prevp = vent;
		    }
		}
	}
	return (firstp);
}

static void
vlist_free(struct vfslist *vlist)
{
	struct	vfslist *vnext;

	vnext = vlist;
	while (vlist) {
		vnext = vlist->next;
		if (vlist->vpath)
			free(vlist->vpath);
		free(vlist);
		vlist = vnext;
	}
}

/*
 * Check_mount()
 * Sets the HANFS_MOUNTED bit on the mount_status
 * field of each vfstab entry. Sets the HANFS_GLOBAL
 * flag if the file system is global.
 */
static int
check_mount(FILE *mfp, struct vfslist  *vlist)
{
	struct mnttab  mget;
	struct vfslist *vp;

	while (getmntent(mfp, &mget) != -1) {
		for (vp = vlist; vp != NULL; vp = vp->next) {
			if (strcmp(mget.mnt_mountp, vp->vpath) == 0) {
				vp->mount_status |= HANFS_MOUNTED;
				/* Check for the global flag */
				if (hasmntopt(&mget, MNTOPT_GLOBAL))
					vp->mount_status |= HANFS_GLOBAL;
				break;
			}
		}
	}
	return (0);
}


/*
 * get_share_command()
 * Function called to extract complete commands from possibly
 * multiple lines, separated by '\' in the dfstab file. Input
 * arguments are the dfstab file pointer, buffer in which the
 * complete command string would be returned, and a pointer to
 * return a flag indicating whether dfstab file is empty.
 */
static int
get_share_command(FILE *dfp, char **out_line, boolean_t *empty_dfstab)
{
	char 	line[2 * SCDS_CMD_SIZE] = "";
	char	first_token[1024] = "";
	char 	*linep = NULL, *tmp = NULL;
	size_t	new_size = 0, old_size = 0;
	boolean_t	split_command = B_FALSE;


	if (*out_line != NULL) {
		free(*out_line);
		*out_line = NULL;
	}

	while (fgets(line, sizeof (line)-1, dfp)) {

		/* Get the first token in the line */
		if (sscanf(line, "%s", first_token) != 1) {
			continue;	/* Empty line */
		}

		/*
		 * Skip comment lines starting with pound-sign or
		 * white-space and pound-sign.
		 */
		if (first_token[0] == '#') {
			continue;	/* Comment */
		}

		split_command = B_FALSE;
		*empty_dfstab = B_FALSE;

		/*
		 * Get rid of trailing comments on the line.
		 */
		tmp = strchr(line, '#');
		if (tmp) *tmp = '\0';
		tmp = NULL;

		/*
		 * Check if command spans multiple lines.
		 * If so, overwrite the '\' char with 0
		 * and set split_command to B_TRUE.
		 * Also make sure nothing lies after the
		 * backslash.
		 */
		tmp = strchr(line, '\\');
		if (tmp) {
			*tmp = '\0';
			split_command = B_TRUE;
			/*
			 * There should be no more characters
			 * after '\'. Note that comments were
			 * already removed.
			 */
			if (sscanf((tmp+1), "%s", first_token) > 0) {
				/* Syntax error on line. */
				/*
				 * SCMSGS
				 * @explanation
				 * The specified share command is incorrect.
				 * @user_action
				 * Correct the share command using the
				 * dfstab(4) man pages.
				 */
				scds_syslog(LOG_ERR,
					"Syntax error on line %s "
					"in dfstab file.", line);
				/* Clean up and exit error. */
				exit(1);
			}
		}

		/* Zero out the newline character */
		tmp = strchr(line, '\n');
		if (tmp) {
			*tmp = '\0';
		}
		/*
		 * Make a copy of the line in out_line
		 * Used to run share command.
		 */
		old_size = 0;
		new_size = strlen(line) + 1;
		if (linep) {
			old_size = strlen(linep);
			new_size += old_size;
		}
		tmp = realloc(linep, new_size);
		if (tmp == NULL) {
			scds_syslog(LOG_ERR,
			    "Out of memory.");
			exit(1);
		}
		/* Zero out newly allocated space */
		bzero(&tmp[old_size], new_size - old_size);
		linep = tmp;
		tmp = NULL;

		/* Concatanate new string to old string */
		(void) strcat(linep, line);

		/* If no backslash, we are done */
		if (split_command == B_FALSE)
			break;

	}	/* While more lines in input */

	/* return the newly allocated line */
	*out_line = linep;
	if (linep)
		scds_syslog_debug(1, "Line = <%s>", linep);
	else
		scds_syslog_debug(1, "No more share commands");
	return ((linep) ? 0 : 1);
}

static void
shrlist_free()
{
	sharelist_t *slp;

	slp = shrlist;
	while (shrlist) {
		slp = shrlist->shr_next;
		if (shrlist->shr_path) {
			free(shrlist->shr_path);
		}
		free(shrlist);
		shrlist = slp;
	}
}

/*
 * refresh_shrlist()
 * Parse the sharetab file to get the list of shared paths.
 */
static int
refresh_shrlist()
{
	FILE *fp;
	char line[MAX_SHRTAB_LINESZ+1];
	char *path, *tmp = NULL, *tmp1;
	char *delim = " \t";
	sharelist_t *slp = NULL, *prev_slp = NULL, *new_slp = NULL;

	/* Free the current list */
	if (shrlist) {
		shrlist_free();
	}

	fp = fopen(SHARETAB, "r");
	if (fp == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * Unable to open sharetab file.
		 * @user_action
		 * No user action required.
		 */
		scds_syslog(LOG_ERR,
		    "Failed to open file %s: %s.",
		    SHARETAB, strerror(errno));
		return (1);
	}

	/* Read line by line from sharetab file */
	while ((tmp1 = fgets(line, MAX_SHRTAB_LINESZ, fp)) != NULL) {
		/* The first token contains the pathname */
		path = strtok_r(tmp1, delim, &tmp);
		new_slp = (sharelist_t *)malloc(sizeof (sharelist_t));
		if (new_slp == NULL) {
			/*
			 * SCMSGS
			 * @explanation
			 * Unable to allocate memory.
			 * @user_action
			 * No user action required.
			 */
			scds_syslog(LOG_ERR,
			    "Memory allocation failure");
			(void) fclose(fp);
			return (1);
		}

		new_slp->shr_path = strdup(path);
		if (new_slp->shr_path == NULL) {
			/*
			 * SCMSGS
			 * @explanation
			 * Unable to allocate memory.
			 * @user_action
			 * No user action required.
			 */
			scds_syslog(LOG_ERR,
			    "Memory allocation failure.");
			(void) fclose(fp);
			return (1);
		}

		new_slp->shr_next = NULL;

		if (prev_slp == NULL) {
			slp = prev_slp = new_slp;
		} else {
			prev_slp->shr_next = new_slp;
			prev_slp = new_slp;
		}
	}

	shrlist = slp;
	(void) fclose(fp);
	return (0);
}

/*
 * check_share_mount()
 * Checks all the paths in the dfstab file and performs
 * share/unshare/check operations on them.
 * Return value is only useful in NFS_OP_CHECK case,
 * and is the number of share paths which make sense.
 * i.e. They are on a mounted filesystem, they exist()
 * and are absolute path names and such. Note that we
 * do not ENFORCE that the share paths be on PXFS. We
 * just issue a warning.
 *
 * NFS_OP_CHKSHARE checks if a path is shared or not.
 * And only shared if it is not already shared. It checks
 * the path against the shared paths that we cached from
 * reading the sharetab file.
 *
 * NFS_OP_RDCHKSHARE does the same as NFS_OP_CHKSHARE except
 * that it repopulates the cache as the sharetab file has
 * changed.
 *
 * Return values:
 * NFS_OP_SHARE: # of successfull shares
 * NFS_OP_CHKSHARE # of successfull shares
 * NFS_OP_RDCHKSHARE # of successfull shares
 * NFS_OP_UNSHARE: # of failed unshares
 * NFS_OP_CHECK: # of valid share paths
 * NFS_OP_VALIDATE: # of invalid share paths, or -1 for empty dfstab file
 */
static int
check_share_mount(FILE *dfp, struct vfslist  *vlist, int op)
{
	char	cmd[2 * SCDS_CMD_SIZE];
	char 	*cmd_line = NULL, *out_line = NULL,
		*lastp = NULL, *currp = NULL, *tmp = NULL;
	struct vfslist *vp, *match_vp;
	struct statvfs64 sinkstat;
	int match_length, min_length;
	int i;
	int 	rc, cont_share = 0, found = 0;
	int		retval_share = 0;
	int		retval_unshare = 0;
	int		retval_check = 0;
	int		retval_validate = 0;
	boolean_t	empty_dfstab = B_TRUE;
	sharelist_t	*slp;

	/*
	 * Refresh the cached list of shared paths.
	 * This is to avoid resharing an already shared
	 * path, when the op value is NFS_OP_CHKSHARE
	 * or NFS_OP_RDCHKSHARE.
	 *
	 * This list is refreshed only when sharetab file
	 * has changed, i.e, when the op value is NFS_OP_RDCHKSHARE.
	 * And also when the cached list is empty.
	 *
	 * If the return value is 1 ( for any errors ), then continue
	 * with sharing all the paths without checking.
	 */
	if (op == NFS_OP_RDCHKSHARE || shrlist == NULL) {
		if (cont_share = refresh_shrlist()) {
			shrlist_free();
		}
	}

	while (get_share_command(dfp, &out_line, &empty_dfstab) == 0) {

		/* Nothing to be done if the file is empty */
		if (empty_dfstab == B_TRUE) break;

		if (op == NFS_OP_SHARE || op == NFS_OP_CHKSHARE ||
		    op == NFS_OP_RDCHKSHARE) {
			cmd_line = strdup(out_line);
			if (cmd_line == NULL) {
				scds_syslog(LOG_ERR,
					"Out of memory.");
				break;
			}
		}

		if ((currp = strtok_r(out_line, sepstr, &tmp)) == NULL) {
			scds_syslog(LOG_ERR,
			    "Syntax error on line %s in dfstab file.",
			    out_line);
			retval_validate++;
			continue;
		}

		/*  Last argument is the share path */
		/*  Assumption: no trailing comments on line */
		lastp = NULL;
		while ((currp = strtok_r(NULL, sepstr, &tmp)) != NULL) {
			lastp = currp;
		}

		/*
		 * Make sure there are at least 2 fields
		 * on the line. The minimal form would be
		 * share <pathname>
		 * We ensure that by requiring that lastp
		 * be non-NULL.
		 */
		if (lastp == NULL) {
			scds_syslog(LOG_ERR,
			    "Syntax error on line %s in dfstab file.",
			    out_line);
			retval_validate++;
			continue;
		}

		/*
		 * The shared path has to start from the root "/".
		 * Try to get the length of the first path component.
		 */
		if (lastp[0] != '/') {
			/*
			 * SCMSGS
			 * @explanation
			 * A path specified in the dfstab file does not begin
			 * with "/"
			 * @user_action
			 * Only absolute path names can be shared with HA-NFS.
			 */
			scds_syslog(LOG_ERR,
			    "Share path name %s not absolute.",
			    lastp);
			retval_validate++;
			continue;
		}

		if (op == NFS_OP_UNSHARE) {
			/* Simply call unshare */
			scds_syslog_debug(3, "Calling unshare on %s.", lastp);
			(void) sprintf(cmd,
				"/usr/sbin/unshare %s > /dev/null 2>&1", lastp);
			rc = run_system(LOG_ERR, cmd);
			if (rc != 0) {
				/* perhaps the path was not shared */
				/*
				 * SCMSGS
				 * @explanation
				 * Need explanation of this message!
				 * @user_action
				 * Need a user action for this message.
				 */
				scds_syslog(LOG_WARNING,
				    "Failed to unshare %s.",
				    lastp);
				retval_unshare++;
			}
			/* No need to do matching */
			continue;
		}

		/*
		 * We need the match to match at least the first component
		 *  of the path name lastp.  For example, if lastp is "/andy"
		 *  we would not want "/an" to match.  To enforce this, we
		 *  compute min_length as the length of the first component
		 *  including the leading slash but not including the trailing
		 *  NUL, blank, or slash.  For example, min_length of "/andy"
		 *  is 5.
		 */
		for (min_length = 1; lastp[min_length] != '/' &&
			lastp[min_length] != '\0' &&
			lastp[min_length] != ' '; min_length++) {
			;
		}

		/*  Look through the vlist to find the longest */
		/*  matching mount point. */

		match_length = 0;
		match_vp = NULL;
		for (vp = vlist; vp != NULL; vp = vp->next) {
		/*
		 *  The first line in the if is testing whether
		 *  vp->vpath is an initial substring of lastp,
		 *  i.e., whether lastp starts with the entire
		 *  string vp->vpath.  The second line of the
		 *  in the if is testing whether this is the
		 *  longest match so far.
		 *
		 *  The third line is to eliminate cases of matches
		 *  like strstr("/andy", "/an"). Moreover, if there
		 *  is any match less then min_length, that should
		 *  be "/" only.
		 */
			if ((strstr(lastp, vp->vpath) == lastp) &&
			((i = (int)strlen(vp->vpath)) > match_length) &&
			((strcmp(vp->vpath, "/") == 0) || (i >= min_length))) {
					match_length = i;
					match_vp = vp;
			}
		}

		/*
		 * Shared path has a match in "/" file system. But, it could be
		 * on a mounted file system that does not have a /etc/vfstab
		 * entry or some legitimate path.
		 *
		 * Spew the message on stderr as a warning only. We dont want
		 * to take any action like validation failure because of some
		 * legitimate root file system paths. An example of that would
		 * be a symlink to any mounted file system path. An ideal
		 * solution in such cases would be to process the realpath()
		 * of the share path, but it may not exhaust all cases. Tackle
		 * it conservatively by treating this condition as a warning
		 * rather than an error. The message is to suppressed in case
		 * of zfs file system.
		 *
		 */
		if ((match_vp != NULL) && strcmp(match_vp->vpath, "/") == 0) {
			/*
			 * Check if the file system is zfs. We dont want
			 * to throw errors continously in probe method
			 * for zfs file system
			 */
			if (statvfs64(lastp, &sinkstat) == -1 ||
				(strcmp(sinkstat.f_basetype, "zfs") != 0)) {
				/*
				 * SCMSGS
				 * @explanation
				 * The path indicated is not on a non-root
				 * mounted file system. This could be damaging
				 * as upon failover NFS clients will start
				 * seeing ESTALE err's. However, there is a
				 * possibility that this share path is
				 * legitimate. The best example in this case
				 * is a share path on a root file systems but
				 * that is a symbolic link to a mounted file
				 * system path.
				 * @user_action
				 * This is a warning. However, administrator
				 * must make sure that share paths on all the
				 * primary nodes access the same file system.
				 * This validation is useful when there are
				 * no HAStorage/HAStoragePlus resources in
				 * the HA-NFS RG.
				 */
				scds_syslog(LOG_ERR,
					"WARNING: Share path %s may be on a"
					" root file system or any file system"
					" that does not have an /etc/vfstab"
					" entry.", lastp);
				if (op == NFS_OP_VALIDATE)
					(void) fprintf(stderr,
					gettext("WARNING: Share"
					" path %s may be on a root file system"
					" or any file system that does not have"
					" an /etc/vfstab entry.\n"), lastp);
			} else
				scds_syslog_debug(3,
					"The Share path %s may be on zfs",
					lastp);
		}

		/*
		 * If we wanted to enforce the fact that the dfstab entry
		 * must have an ancestor in the vfstab, we could do that
		 * here by treating the condition "match_length < min_length"
		 * as an error.
		 *
		 * The above mentioned check is clubbed with finding
		 * longest matching prefix code given above.
		 */
		if ((match_vp != NULL) &&
			(strcmp(match_vp->vpath, "/") != 0) &&
			!(match_vp ->mount_status & HANFS_MOUNTED)) {
			/*
			 * SCMSGS
			 * @explanation
			 * The specified file system, which contains the share
			 * path specified, is not currently mounted.
			 * @user_action
			 * Correct the situation with the file system so that
			 * it gets mounted.
			 */
			scds_syslog(LOG_ERR,
			    "Share path %s: file system %s is not mounted.",
			    lastp, match_vp->vpath);
			retval_validate++;
		} else if (statvfs64(lastp, &sinkstat) == -1) {
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			scds_syslog(LOG_ERR,
			    "%s does not exist or is not mounted.",
			    lastp);
			retval_validate++;
		} else {
			/* This is a valid share path */
			retval_check++;
			/* No more to do for  NFS_OP_CHECK and VALIDATE */
			if ((op == NFS_OP_CHECK) || (op == NFS_OP_VALIDATE)) {
				continue;
			}

			/* No more to do if the path is already shared */
			if (op == NFS_OP_CHKSHARE || op == NFS_OP_RDCHKSHARE) {
				/*
				 * Check if the path is already shared. If yes,
				 * then continue with next path. Else, proceed
				 * further and do the share
				 *
				 * The check is done by going through list of
				 * shared paths that we have cached. Skip this
				 * check for any errors during caching.
				 */
				if (!cont_share) {
					slp = shrlist;
					found = 0;
					while (slp) {
						if (strcmp(lastp,
						    slp->shr_path) == 0) {
							found = 1;
							break;
						}
						slp = slp->shr_next;
					}
					if (found == 1) {
						if (cmd_line) {
							free(cmd_line);
							cmd_line = NULL;
						}
						continue;
					}
				}
			}

			/* NFS_OP_SHARE: Simply call share */
			scds_syslog_debug(3, "Running %s.", cmd_line);
			rc = run_system(LOG_ERR, cmd_line);
			if (rc != 0) {
				/* Syslog, but otherwise ignore */
				/*
				 * SCMSGS
				 * @explanation
				 * Need explanation of this message!
				 * @user_action
				 * Need a user action for this message.
				 */
				scds_syslog(LOG_ERR, "Share command %s "
					"did not complete successfully.",
					cmd_line);
			} else {
				retval_share++; /* Successful share */
			}
			if (cmd_line) {
				free(cmd_line);
				cmd_line = NULL;
			}
		}
	}

	if (out_line) free(out_line);

	switch (op) {

		case NFS_OP_SHARE:
			return (retval_share);
			/* break */
		case NFS_OP_UNSHARE:
			return (retval_unshare);
			/* break */
		case NFS_OP_CHECK:
			/* Empty dfstab now ?? The dfstab is updated after */
			/* validation */
			if (empty_dfstab == B_TRUE)
				retval_check = HANFS_EMPTY_DFSTAB;
			return (retval_check);
			/* break */
		case NFS_OP_VALIDATE:
			if (empty_dfstab == B_TRUE)
				return (-1);
			else
				return (retval_validate);
			/* break */
		case NFS_OP_CHKSHARE:
			return (retval_share);
			/* break */
		case  NFS_OP_RDCHKSHARE:
			return (retval_share);
			/* break */
		default:
			break;
	}
	return (-1);
}

/*
 * nfs_share_dfstab(char *dfsfilename, int op)
 * Sanity checks entries in the given dfstab style file name
 * and calls share/unshare (depending upon op) on
 * each of the valid entries.
 */
int
nfs_share_dfstab(char *dfsfilename, int op)
{
	FILE *vfp, *dfp, *mfp;
	struct vfslist *vlist = NULL;
	struct flock mlock;
	int	rc;

	vtab_path = VFSTAB;
	dtab_path = dfsfilename;

	vfp = fopen(vtab_path, "r");
	if (vfp == NULL) {
		scds_syslog(LOG_ERR,
		    "Unable to open %s: %s.",
		    vtab_path, strerror(errno)); /*lint !e746 */
		exit(1);
	}

	dfp = fopen(dtab_path, "r");
	if (dfp == NULL) {
		scds_syslog(LOG_ERR,
		    "Unable to open %s: %s.",
		    dtab_path, strerror(errno));
		exit(1);
	}

	mfp = fopen(MNTTAB, "r");
	if (mfp == NULL) {
		scds_syslog(LOG_ERR,
		    "Unable to open %s: %s.",
		    MNTTAB, strerror(errno));
		exit(1);
	}
	(void) memset((void *)&mlock, 0, sizeof (mlock));
	mlock.l_type = F_RDLCK;
	if (fcntl(fileno(mfp), F_SETLKW, &mlock) != 0) {
		scds_syslog(LOG_ERR,
		    "Unable to lock %s: %s.",
		    MNTTAB, strerror(errno));
		exit(1);
	}

	/*
	 * Build the vfslist out of the vfstab.
	 * Initialize all mount_status to 0.
	 */
	if ((vlist = vlist_build(vfp)) == NULL) {
		char	internal_err_str[INT_ERR_MSG_SIZE];

		(void) snprintf(internal_err_str,
		    sizeof (internal_err_str),
		    "Failed to build vfstab entries");
		/* ignore err return code above */
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
		    internal_err_str);
		exit(1);
	}

	printvfslist(vlist);

	/*
	 * For each mount entry, find the matching vfslist entry.
	 * If found, sets the HANFS_MOUNTED and HANFS_GLOBAL
	 * flags on the vfstab entries.
	 */
	(void) check_mount(mfp, vlist);

	scds_syslog_debug(3, "Done check_mount.");
	printvfslist(vlist);

	/*
	 * For each dfstab entry, find the longest matching prefix
	 * from the vfslist. If the mount_status from the vfslist entry
	 * is 1, it means the filesystem is mounted; and therfore,
	 * it's ok to share it. In that case, we'll run the share
	 * command. Otherwise, we'll call syslog to issue an
	 * error for the entry to note that it's not mounted.
	 * If the operation is to unshare, call unshare on
	 * each object.
	 */
	rc = check_share_mount(dfp, vlist, op);
	vlist_free(vlist);
	(void) fclose(vfp);
	(void) fclose(dfp);
	(void) fclose(mfp);

	return (rc);
}

static void
fslist_free(shared_path_list_t *fslist)
{
	shared_path_list_t	*fsp;

	fsp = fslist;
	while (fslist) {
		fsp = fslist->fs_next;
		if (fslist->fs_path)
			free(fslist->fs_path);
		free(fslist);
		fslist = fsp;
	}
}

/*
 * nfs_get_fslist()
 * Parse the dfstab file of the given resource to get the list of shared
 * paths for resource monitor.
 */
int
nfs_get_fslist(scds_handle_t scds_handle, shared_path_list_t **fslist)
{
	char		*dfsfile = NULL;
	FILE		*dfp = NULL;
	shared_path_list_t	*fsp = NULL, *prev_fsp = NULL, *new_fsp = NULL;
	char		*out_line = NULL;
	char		*lastp = NULL, *currp = NULL, *tmp = NULL;
	boolean_t	empty_dfstab = B_TRUE;

	if (*fslist)
		fslist_free(*fslist);

	dfsfile = get_dfstab_filename(scds_handle, B_FALSE);
	if (dfsfile == NULL) {
		/* We have been checking for this all the time */
		return (1);
	}

	dfp = fopen(dfsfile, "r");
	if (dfp == NULL) {
		scds_syslog(LOG_ERR,
		    "Unable to open %s: %s.",
		    dfsfile, strerror(errno));
		return (1);
	}

	while (get_share_command(dfp, &out_line, &empty_dfstab) == 0) {
		/*
		 * Nothing to be done if empty file. This condition
		 * should never be satisfied here.
		 */
		if (empty_dfstab == B_TRUE) break;

		if ((currp = strtok_r(out_line, sepstr, &tmp)) == NULL) {
			scds_syslog(LOG_ERR,
			    "Syntax error on line %s in dfstab file.",
			    out_line);
			if (out_line) free(out_line);
			exit(1);
		}

		/*  Last argument is the share path */
		/*  Assumption: no trailing comments on line */
		lastp = NULL;
		while ((currp = strtok_r(NULL, sepstr, &tmp)) != NULL) {
			lastp = currp;
		}

		if (lastp == NULL) {
			scds_syslog(LOG_ERR,
			    "Syntax error on line %s in dfstab file.",
			    out_line);
			if (out_line) free(out_line);
			exit(1);
		}

		new_fsp = (shared_path_list_t *)
		    malloc(sizeof (shared_path_list_t));
		if (new_fsp == NULL) {
			scds_syslog(LOG_ERR,
				"Out of memory.");
			if (out_line) free(out_line);
			return (1);
		}

		new_fsp->fs_path = strdup(lastp);
		if (new_fsp->fs_path == NULL) {
			scds_syslog(LOG_ERR,
			    "Out of memory.");
			if (out_line) free(out_line);
			return (1);
		}

		new_fsp->fs_status = HANFS_FS_OK;
		new_fsp->fs_next = NULL;

		if (prev_fsp  == NULL)
			fsp = prev_fsp = new_fsp;
		else {
			prev_fsp->fs_next = new_fsp;
			prev_fsp = new_fsp;
		}
	}

	if (out_line) free(out_line);

	*fslist = fsp;
	(void) fclose(dfp);
	return (0);
}
