#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#ident	"@(#)SUNW.nfs	1.28	07/06/06 SMI"

# Registration information and Paramtable for HA-NFS
#
# NOTE: Keywords are case insensitive, i.e. users may use any
# capitalization style they wish
# 

RESOURCE_TYPE = "nfs";
VENDOR_ID = SUNW;
RT_DESCRIPTION = "HA-NFS for Sun Cluster";

RT_VERSION ="3.2"; 
API_VERSION = 2;	 

INIT_NODES = RG_PRIMARIES;

FAILOVER = TRUE;

RT_BASEDIR=/opt/SUNWscnfs/bin;

INIT				=	nfs_init;
BOOT				=	nfs_boot;
PRENET_START		=   nfs_prenet_start;
START				=	nfs_svc_start;
STOP				=	nfs_svc_stop;
POSTNET_STOP		=	nfs_postnet_stop;

VALIDATE			=	nfs_validate;
UPDATE				=	nfs_update;

MONITOR_START			=	nfs_monitor_start;
MONITOR_STOP			=	nfs_monitor_stop;
MONITOR_CHECK			=	nfs_monitor_check;

PKGLIST = SUNWscnfs;

#
# Upgrade directives
#
#$upgrade
#$upgrade_from "1.0" anytime
#$upgrade_from "3.1" anytime


# The paramtable is a list of bracketed resource property declarations 
# that come after the resource-type declarations
# The property-name declaration must be the first attribute
# after the open curly of a paramtable entry
#
{  
	PROPERTY = Init_timeout; 
	MIN=60; 
	DEFAULT=120;
}
{  
	PROPERTY = Boot_timeout; 
	MIN=60; 
	DEFAULT=120;
}
{  
	PROPERTY = Start_timeout; 
	MIN=60; 
	DEFAULT=300;
}
{
	PROPERTY = Stop_timeout; 
	MIN=60; 
	DEFAULT=300;
}
{ 
	PROPERTY = Validate_timeout; 
	MIN=60; 
	DEFAULT=300; 
}
{ 
	PROPERTY = Update_timeout; 
	MIN=60; 
	DEFAULT=300; 
}
{ 
	PROPERTY = Monitor_Start_timeout; 
	MIN=60; 
	DEFAULT=300; 
}
{ 
	PROPERTY = Monitor_Stop_timeout; 
	MIN=60; 
	DEFAULT=300; 
}
{ 
	PROPERTY = Monitor_Check_timeout; 
	MIN=60; 
	DEFAULT=300; 
}
{ 
	PROPERTY = Prenet_Start_timeout; 
	MIN=60; 
	DEFAULT=300; 
}
{ 
	PROPERTY = Postnet_Stop_timeout; 
	MIN=60; 
	DEFAULT=300; 
	TUNABLE = ANYTIME;
}

# Property Cheap_Probe_Interval is used by the fault monitor for nfs daemons.
# The value of this property amoung all nfs resources within the cluster
# should be the same.
{ 
	PROPERTY = Cheap_Probe_Interval; 
	MIN=10; 
	MAX=3600; 
	DEFAULT=20; 
	TUNABLE = ANYTIME;
}
{ 
	PROPERTY = Thorough_Probe_Interval; 
	MIN=60; 
	MAX=3600; 
	DEFAULT=120; 
	TUNABLE = ANYTIME;
}
{ 
	PROPERTY = Retry_Count; 
	MIN=0; 
	DEFAULT=2; 
	TUNABLE = ANYTIME;
}
{ 
	PROPERTY = Retry_Interval; 
	MIN=0; 
	DEFAULT=500; 
	TUNABLE = ANYTIME;
}
{ 
	PROPERTY = FailOver_Mode;
	DEFAULT = HARD; 
	TUNABLE = ANYTIME;
}
{ 
	PROPERTY = Network_resources_used; 
	TUNABLE = WHEN_DISABLED; 
	DEFAULT = "";
}

#
# Extension Properties
#

# Not to be edited by end user
{
	PROPERTY = Paramtable_version;
	EXTENSION;
	STRING;
	DEFAULT = "1.0";
	DESCRIPTION = "The Paramtable Version for this Resource";
}

# These two control the restarting of the fault monitor itself
# (not the serer daemon) by PMF.
# Need to find better names for these. Should be handled in the
# library.
{
	PROPERTY = Monitor_retry_count;
	EXTENSION;
	INT;
	MIN=-1;
	DEFAULT = 4;
	TUNABLE = ANYTIME;
	DESCRIPTION = "Number of PMF restarts allowed for the fault monitor";
}

{
	PROPERTY = Monitor_retry_interval;
	EXTENSION;
	INT;
	MIN=-1;
	DEFAULT = 2;
	TUNABLE = ANYTIME;
	DESCRIPTION = "Time window (minutes) for fault monitor restarts";
}

{
	PROPERTY = Rpcbind_nullrpc_timeout;
	EXTENSION;
	INT;
	MIN=60;
	DEFAULT = 120;
	TUNABLE = ANYTIME;
	DESCRIPTION = "Timeout(seconds) to use when probing rpcbind";
}

{
	PROPERTY = Nfsd_nullrpc_timeout;
	EXTENSION;
	INT;
	MIN=60;
	DEFAULT = 120;
	TUNABLE = ANYTIME;
	DESCRIPTION = "Timeout(seconds) to use when probing nfsd";
}

{
	PROPERTY = Mountd_nullrpc_timeout;
	EXTENSION;
	INT;
	MIN=60;
	DEFAULT = 120;
	TUNABLE = ANYTIME;
	DESCRIPTION = "Timeout(seconds) to use when probing mountd";
}

{
	PROPERTY = Statd_nullrpc_timeout;
	EXTENSION;
	INT;
	MIN=60;
	DEFAULT = 120;
	TUNABLE = ANYTIME;
	DESCRIPTION = "Timeout(seconds) to use when probing statd";
}

{
	PROPERTY = Lockd_nullrpc_timeout;
	EXTENSION;
	INT;
	MIN=60;
	DEFAULT = 120;
	TUNABLE = ANYTIME;
	DESCRIPTION = "Timeout(seconds) to use when probing lockd";
}

{
	PROPERTY = Rpcbind_nullrpc_reboot;
	EXTENSION;
	BOOLEAN;
	DEFAULT = FALSE;
	TUNABLE = ANYTIME;
	DESCRIPTION = "Boolean to indicate if we should reboot system when null rpc call on rpcbind fails";
}

{
	PROPERTY = Nfsd_nullrpc_restart;
	EXTENSION;
	BOOLEAN;
	DEFAULT = FALSE;
	TUNABLE = ANYTIME;
	DESCRIPTION = "Boolean to indicate if we should restart nfsd when null rpc call fails";
}

{
	PROPERTY = Mountd_nullrpc_restart;
	EXTENSION;
	BOOLEAN;
	DEFAULT = TRUE;
	TUNABLE = ANYTIME;
	DESCRIPTION = "Boolean to indicate if we should restart mountd when null rpc call fails";
}
