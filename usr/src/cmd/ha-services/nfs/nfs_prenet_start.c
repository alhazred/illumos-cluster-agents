/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * nfs_prenet_start.c - Prenet Start method for highly available NFS
 */

#pragma ident	"@(#)nfs_prenet_start.c	1.22	07/06/06 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "hanfs.h"

/*
 * Called BEFORE the logical ipaddresses are brought up
 * All we do here is to run the share command(s) for
 * the resource. The reason we don't want to wait till
 * the START time is that we don't want the clients to
 * start coming in with mount/io requests BEFORE the
 * share has been performed.
 *
 * For lock recovery, we obviously have to wait till START
 * time, as we need to be able to talk to the clients.
 */

int
main(int argc, char *argv[])
{
	scds_handle_t scds_handle;
	int	lockid, rc;
	char	*dfsfile;
	char	*giveover_file = HANFS_STATE_DIR"/"GIVEOVER_FILE;
	char	*state_file = HANFS_STATE_DIR"/"HANFS_STATE_FILE;
	char	*rsname = NULL, *rgname = NULL;
	off_t	size = 0;

	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR) {
		return (1);
	}

	/* Get resource and resource group names */
	rsname = (char*) scds_get_resource_name(scds_handle);
	rgname = (char*) scds_get_resource_group_name(scds_handle);

	scds_syslog_debug(4, "Calling svc_validate.");
	if (svc_validate(scds_handle, B_FALSE) != SCHA_ERR_NOERR) {
		(void) scha_resource_setstatus(rsname, rgname,
			SCHA_RSSTATUS_FAULTED,
			"Invalid resource settings.");
		return (1);
	}
	/* Grab the system-wide lock. Waits... */
	lockid = nfs_serialize_lock();

	/* Check if giveover file exists - size ignored */
	rc = check_file(giveover_file, &size);
	if (rc == 0) {
		/*
		 * Giveover file exists. Verify if it should
		 * exist by looking at the nfs.state file.
		 */
		rc = check_file(state_file, &size);
		if (rc == 0) {
			/* nfs.state file exists */
			if (size > 0) {
				/*
				 * Giveover may be in
				 * progress. Exit.
				 */
				/*
				 * SCMSGS
				 * @explanation
				 * Startup of an NFS resource was aborted
				 * because a failure was detected by another
				 * resource group, which would be in the
				 * process of failover.
				 * @user_action
				 * Attempt to start the NFS resource after the
				 * failover is completed. It may be necessary
				 * to start the resource on another node if
				 * current node is not healthy.
				 */
				scds_syslog(LOG_ERR,
					"Aborting startup: "
					"failover of NFS resource "
					"groups may be in progress.");
				rc = 1;
				goto finished;
			} else {
				/*
				 * nfs.state file is empty.
				 * Maybe nfs_postnet_stop did
				 * not cleanup giveover file.
				 */
				rc = remove_giveover_file();
			}
		} else if (rc > 0) {
			/*
			 * nfs.state file does not exist.
			 * This should not happen.
			 * (/var/run should be clean).
			 * Cleanup giveover file.
			 */
			rc = remove_giveover_file();
		}
	} else if (rc > 0) {
		/* Giveover file does not exist */
		rc = 0;
	}

	if (rc != 0) {
		/*
		 * Something wrong - could not determine if
		 * giveover file exists or not. Do not proceed.
		 */
		/*
		 * SCMSGS
		 * @explanation
		 * Startup of an NFS resource was aborted because it was not
		 * possible to determine if failover of any NFS resource
		 * groups is in progress.
		 * @user_action
		 * Save a copy of the /var/adm/messages files on all nodes.
		 * Contact your authorized Sun service provider for assistance
		 * in diagnosing the problem.
		 */
		scds_syslog(LOG_ERR,
			"Aborting startup: could not determine "
			"whether failover of NFS resource groups "
			"is in progress.");
		rc = 1;
		goto finished;
	}

	/* Giveover file does not exist. Proceed ... */

	dfsfile = get_dfstab_filename(scds_handle, B_FALSE);
	if (dfsfile == NULL) {
		return (1);	/* Messages already logged */
	}
	rc = nfs_share_dfstab(dfsfile, NFS_OP_CHECK);
	if (rc == HANFS_EMPTY_DFSTAB) {
		/* zero entires in dfstab of this resource */
		scds_syslog(LOG_WARNING,
			"dfstab file %s does not have any paths to be "
			"shared. Continuing.", dfsfile);
		(void) fprintf(stderr, gettext("Warning, dfstab file %s "
			"does not have any paths to be shared. Continuing.\n"),
			dfsfile);
		rc = 0;
		goto finished;
	}
	if (rc < 1) {
		/* There are no usable paths */
		char msg[INT_ERR_MSG_SIZE];
		(void) sprintf(msg,
			"None of the shared paths in file %s are valid.",
			dfsfile);
		(void) scha_resource_setstatus(rsname, rgname,
			SCHA_RSSTATUS_FAULTED, msg);
		rc = 1;
		goto finished;
	}
	/*
	 * Perform the share commands. Return value is the
	 * number of successfully shared paths
	 */
	rc = nfs_share_resource(scds_handle);
	if (rc < 1) {
		/* This sounds like a serious problem */
		(void) scha_resource_setstatus(rsname, rgname,
			SCHA_RSSTATUS_FAULTED,
			"Unable to complete any share commands.");
		rc = 1;
		goto finished;
	}
	/* Add this resource to the registry of NFS resources on this node */
	rc = nfs_register_resource(scds_handle);
	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_WARNING,
			"Unable to re-compute NFS resource list.");
	}

	/* stop nfsd if it is running, and if it is capable of warm start */
	if (nfsd_has_warmstart()) {
		(void) stop_nfs_daemon(NFS_NFSD_PATH, RPC_PROGNUM_NFSD,
			RPC_VERSION_NFSD, -1);
	}

	/* Start nfsd/mountd if not running already, no timeout */
	scds_syslog(LOG_NOTICE, "Starting %s.", "nfsd and mountd");

	rc = start_nfsd_mountd(scds_handle, -1);
	if (rc != SCHA_ERR_NOERR) {
		/* This sounds like a serious problem */
		(void) scha_resource_setstatus(rsname, rgname,
			SCHA_RSSTATUS_FAULTED,
			"Failed to start nfsd/mountd.");
		rc = 1;
		goto finished;  /* We still want to release the lock */
	}

	rc = 0;
finished:
	/* Release the lock. We are return ing here, but anyway... */
	(void) nfs_serialize_unlock(lockid);

	return (rc);
}
