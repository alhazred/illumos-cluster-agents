/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * hanfs_util.c - Common utilities for highly available nfs
 */

#pragma ident	"@(#)hanfs_util.c	1.87	08/10/08 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <unistd.h>
#include <signal.h>
#include <libgen.h>

#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/errno.h>
#include <fcntl.h>
#include <rpc/rpc.h>
#include <sys/stat.h>
#include <sys/statvfs.h>
#include <sys/types.h>
#include <utime.h>
#include <kstat.h>
#include <procfs.h>
#include <libintl.h>
#include <sys/utsname.h>
#include <sys/int_types.h>
#include <ds_common.h>
#include <deflt.h>

#if	(SOL_VERSION >= __s10)
#include <libscf.h>
#endif

/* Set dfstab_check flag to TRUE to perform the validation by default. */
boolean_t	dfstab_check = B_TRUE;

#include "hanfs.h"

#define	DBG_LEVEL_HIGH	9

/* The subdirectory under the RG Pathprefix */
#define	HANFS_SUFFIX	"SUNW.nfs"

#define	HANFS_R_PROBE_PROC	"nfs_probe"

#define	STARTD_DURATION_PROP	":properties/startd/duration"

#define	MAX_LINE	80

char *svcs[] = {
	"svc:/network/nfs/server:default",
	"svc:/network/nfs/nlockmgr:default",
	"svc:/network/nfs/status:default",
	NULL
};


/*
 * is_nfsd_active(): Reads the kstat nfs_server:calls
 * for module nfs. Returns TRUE if that number has
 * changed since the last call (indicating that
 * nfsd is serving requests).
 * Return values:
 * 0: Success, passed in boolean pointer is
 *		updated to TRUE/FALSE
 * -1: Some failure was encountered.
 */
int
is_nfsd_active(boolean_t *is_active)
{
	static kstat_t	*ks = NULL;
	static kstat_ctl_t *kc = NULL;
	static uint64_t old_calls = 0;

	kstat_named_t *kn = NULL;

	if (kc == NULL) {
		kc = kstat_open();
		if (kc == NULL) {
			/*
			 * SCMSGS
			 * @explanation
			 * HA-NFS fault monitor attempt to open the device but
			 * failed. The specific cause of the failure is logged
			 * with the message. The /dev/kmem interface is used
			 * to read NFS activity counters from kernel.
			 * @user_action
			 * No action. HA-NFS fault monitor would ignore this
			 * error and try to open the device again later. Since
			 * it is unable to read NFS activity counters from
			 * kernel, HA-NFS would attempt to contact nfsd by
			 * means of a NULL RPC. A likely cause of this error
			 * is lack of resources. Attempt to free memory by
			 * terminating any programs which are using large
			 * amounts of memory and swap. If this error persists,
			 * reboot the node.
			 */
			scds_syslog(LOG_ERR, "Unable to open /dev/kmem:%s",
				strerror(errno));	/*lint !e746 */
			return (-1);
		}
		ks = kstat_lookup(kc, "nfs", 0, "nfs_server");
		if (ks == NULL) {
			/*
			 * SCMSGS
			 * @explanation
			 * HA-NFS fault monitor failed to lookup the specified
			 * kstat parameter. The specific cause is logged with
			 * the message.
			 * @user_action
			 * Run the following command on the cluster node where
			 * this problem is encounterd. /usr/bin/kstat -m nfs
			 * -i 0 -n nfs_server -s calls Barring resource
			 * availability issues, this call should complete
			 * successfully. If it fails without generating any
			 * output, please contact your authorized sun service
			 * provider.
			 */
			scds_syslog(LOG_ERR, "Unable to lookup nfs:nfs_server "
				"from kstat:%s", strerror(errno));
			(void) kstat_close(kc);
			kc = NULL;
			return (-1);
		}
		scds_syslog_debug(2, "Reading from kstat: Module <%s> "
			"Name <%s> NumData <%d>", ks->ks_module, ks->ks_name,
			ks->ks_ndata);
	}
	if (kstat_read(kc, ks, NULL) == -1) {
		/*
		 * SCMSGS
		 * @explanation
		 * See 176151
		 * @user_action
		 * See 176151
		 */
		scds_syslog(LOG_ERR, "Failed to read from kstat:%s",
			strerror(errno));
		(void) kstat_close(kc);
		kc = NULL;
		return (-1);
	}
	kn = kstat_data_lookup(ks, "calls");
	if (kn == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * See 176151
		 * @user_action
		 * See 176151
		 */
		scds_syslog(LOG_ERR, "Unable to lookup nfs:nfs_server:calls "
			"from kstat.");
		(void) kstat_close(kc);
		kc = NULL;
		return (-1);
	}
	if (kn->data_type != KSTAT_DATA_UINT64) {
		/*
		 * SCMSGS
		 * @explanation
		 * See 176151
		 * @user_action
		 * See 176151
		 */
		scds_syslog(LOG_ERR, "Wrong data format from kstat: "
			"Expecting %d, Got %d.",
			KSTAT_DATA_UINT64, kn->data_type);
		(void) kstat_close(kc);
		kc = NULL;
		return (-1);
	}
	if (old_calls == kn->value.ui64) {
		*is_active = B_FALSE;
	} else {
		*is_active = B_TRUE;
		old_calls = kn->value.ui64;
	}
	scds_syslog_debug(6, "is_nfsd_active() %s. Count is %lld",
		(*is_active == B_TRUE) ? "TRUE" : "FALSE", old_calls);
	return (0);
}

/*
 * is_process_stopped(): Extremely specific code for SIGSTOPPED
 * nfsd. Go thru procfs interface to see if it is stopped
 * by SIGSTOP.
 * Assumes that LWP 1 exists for the process (always true).
 * Return values: 0 if NOT stopped
 *	1: Process is stopped
 *	-1: System Errors
 */
int
is_process_stopped(pid_t pid)
{
	lwpstatus_t pi;
	int fd;
	char lwpstatusfile[PATH_MAX + 1];

	/* Open the lwpstatus file for LWP 1 */
	(void) sprintf(lwpstatusfile, "/proc/%d/lwp/1/lwpstatus", pid);
	fd = open(lwpstatusfile, O_RDONLY);
	if (fd < 0) {
		if (errno == ENOENT) {
			/* Process died */
			return (-1);
		}
		scds_syslog(LOG_ERR, "Unable to open %s: %s.",
			lwpstatusfile, strerror(errno));
		return (-1);
	}
	if (read(fd, &pi, sizeof (lwpstatus_t)) != (int)sizeof (lwpstatus_t)) {
		(void) close(fd);
		return (-1);
	}
	if (pi.pr_flags & PR_STOPPED) {
		scds_syslog_debug(2, "Process is stopped why = %d, what = %d",
			pi.pr_why, pi.pr_what);
		if ((pi.pr_why == PR_SIGNALLED) ||
			(pi.pr_why == PR_JOBCONTROL)) {
			if ((pi.pr_what == SIGSTOP) ||
				(pi.pr_what == SIGTSTP) ||
				(pi.pr_what == SIGTTIN) ||
				(pi.pr_what == SIGTTOU)) {
				/*
				 * SCMSGS
				 * @explanation
				 * HA-NFS fault monitor has detected that the
				 * specified process has been stopped with a
				 * signal.
				 * @user_action
				 * No action. HA-NFS fault monitor would kill
				 * and restart the stopped process.
				 */
				scds_syslog(LOG_ERR, "pid %d is stopped.", pid);
				(void) close(fd);
				return (1);	/* Process IS SIGSTOPPED */
			}
		}
	}
	(void) close(fd);
	return (0);
}

/*
 * create_prenet_fail_file()
 * Creates a file (name "resource") in
 * NO_PATHPREFIX_SUBDIR subdirectory.
 * The Stop methods then check to see
 * if this file exists, and exit() success
 * early, assuming that the start methods were
 * unable to perform any useful action.
 *
 * Returns 0 for success. -1 for problems.
 */

static int
create_prenet_fail_file(scds_handle_t scds_handle)
{
	char	pp_status[MAXPATHLEN];
	int	fd;

	/* First create the subdir */
	(void) sprintf(pp_status, "%s/%s", HANFS_STATE_DIR,
		NO_PATHPREFIX_SUBDIR);
	if (mkdirp(pp_status, (mode_t)0755) != 0) {
		/*
		 * Suppress lint: call to ___errno() not made in the
		 *  presence of a prototype.
		 */
		if (errno != EEXIST) { 	/*lint !e746 */
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			scds_syslog(LOG_ERR,
			    "Unable to create directory %s: %s.",
			    pp_status, strerror(errno));
			return (-1);
		}
	}

	/* Now create the file in that subdir */
	(void) strcat(pp_status, "/");
	(void) strcat(pp_status, (char *)scds_get_resource_name(scds_handle));
	if ((fd = creat(pp_status, 0600)) < 0) {
		if (errno != EEXIST) {
			/*
			 * SCMSGS
			 * @explanation
			 * The HA-NFS stop method attempted to create the
			 * specified file but failed.
			 * @user_action
			 * Check the error message for the reason of failure
			 * and correct the situation. If unable to correct the
			 * situation, reboot the node.
			 */
			scds_syslog(LOG_ERR, "Unable to create <%s>: %s.",
				pp_status, strerror(errno));
			return (-1);
		}
	}
	(void) close(fd);
	return (0);
}

/*
 * remove_prenet_fail_file()
 * Remove the file which is used to
 * communicate the non-existence of Pathprefix
 * from prenet_start method to the stop method.
 * Return code from unlink() is ignored if the
 * file did not exist.
 * Returns -1 for failures, 0 for success.
 */

static int
remove_prenet_fail_file(scds_handle_t scds_handle)
{
	char    pp_status[MAXPATHLEN];

	(void) sprintf(pp_status, "%s/%s/%s", HANFS_STATE_DIR,
		NO_PATHPREFIX_SUBDIR,
		(char *)scds_get_resource_name(scds_handle));

	if (unlink(pp_status) != 0) {
		if (errno != ENOENT) {
			return (-1);
		}
	}
	return (0);
}

/*
 * check_prenet_fail_file()
 * Check the per resource file which
 * indicates if the prenet_start failed
 * early because Pathprefix didn't exist.
 * Returns 0 if this file does exist,
 * -1 otherwise.
 */

static int
check_prenet_fail_file(scds_handle_t scds_handle)
{
	char    pp_status[MAXPATHLEN];
	struct	statvfs64	sbuf;

	(void) sprintf(pp_status, "%s/%s/%s", HANFS_STATE_DIR,
		NO_PATHPREFIX_SUBDIR,
		(char *)scds_get_resource_name(scds_handle));

	if (statvfs64(pp_status, &sbuf) != 0) {
		return (-1);
	}
	return (0);
}

/*
 * get_statmon_dir(): Given a RG name returns
 * the pathname to be supplied to statd for this resource.
 * Returns NULL if there is any error encountered.
 * Formula to get the statmon directory name is
 * [Pathprefix of the RG]/SUNW.nfs
 * Returns static memory. caller should not free() it
 * or write into it.
 * Note that this excludes the final path component "statmon"
 * used by statd. That sub-directory is created by statd
 * itself.
 * Also note that since this function works with static
 * memory, this would work only with a given resource method,
 * specifically, one cannot make calls to this routine for
 * "other RGs" (i.e. not an RG which contains this resource).
 */
static char *
get_statmon_dir(scds_handle_t scds_handle, boolean_t print_messages)
{
	static	char	statmondir[PATH_MAX] = "";
	char		*pathprefix;
	char		*rg = NULL;
	struct statvfs64	sbuf;

	if (statmondir[0] != 0) {
		return (statmondir);	/* Already computed */
	}

	/* Get resource group name */
	rg = (char *)scds_get_resource_group_name(scds_handle);

	/* Get resource group pathprefix */
	pathprefix = (char *)scds_get_rg_pathprefix(scds_handle);
	if ((pathprefix == NULL) || (pathprefix[0] == '\0')) {
		/*
		 * SCMSGS
		 * @explanation
		 * Resource Group property Pathprefix is not set.
		 * @user_action
		 * Use scrgadm to set the Pathprefix property on the resource
		 * group.
		 */
		scds_syslog(LOG_ERR, "Pathprefix is not set "
			"for resource group %s.", rg);
		if (print_messages) {
			(void) fprintf(stderr, gettext("Pathprefix is not "
				"set for resource group %s.\n"), rg);
		}
		return (NULL);
	}

	/*
	 * Remove the file which indicates no pathprefix
	 */
	(void) remove_prenet_fail_file(scds_handle);

	/*
	 * access() may return cached values. Using this together
	 * with statvfs64() guarantees permission and status.
	 */
	if (access(pathprefix, R_OK) || statvfs64(pathprefix, &sbuf)) {
		int err = errno;
		/*
		 * SCMSGS
		 * @explanation
		 * A HA-NFS method attempted to access the specified
		 * Pathprefix but was unable to do so. The reason for the
		 * failure is also logged.
		 * @user_action
		 * This could happen if the filesystem on which the Pathprefix
		 * directory resides is not available. Use the HAStorage
		 * resource in the resource group to make sure that HA-NFS
		 * methods have access to the file system at the time when
		 * they are launched. Check to see if the pathname is correct
		 * and correct it if not so. HA-NFS would attempt to recover
		 * from this situation by failing over to some other node.
		 */
		scds_syslog(LOG_ERR,
		    "Pathprefix %s for resource group %s "
		    "is not readable: %s.",
		    pathprefix, rg, strerror(err));
		if (print_messages) {
			(void) fprintf(stderr, gettext("Pathprefix %s for "
				"resource group %s is not readable: %s.\n"),
				pathprefix, rg, gettext(strerror(err)));
		}

		/*
		 * Create the file which indicates that
		 * Pathprefix doesn't exist
		 */
		(void) create_prenet_fail_file(scds_handle);
		return (NULL);
	}
	scds_syslog_debug(2, "Resource group pathprefix is %s.", pathprefix);
	/* Make sure there is a PATHPREFIX/SUNW.nfs directory */
	(void) sprintf(statmondir, "%s/%s", pathprefix,
		HANFS_SUFFIX);
	/*
	 * access() may return cached values. Using this together
	 * with statvfs64() guarantees permission and status.
	 */
	if (access(statmondir, R_OK) || statvfs64(statmondir, &sbuf)) {
		int err = errno;
		/*
		 * SCMSGS
		 * @explanation
		 * A HA-NFS method attempted to access the specified directory
		 * but was unable to do so. The reason for the failure is also
		 * logged.
		 * @user_action
		 * If the directory is on a mounted filesystem, make sure the
		 * filesystem is currently mounted. If the pathname of the
		 * directory is not what you expected, check to see if the
		 * Pathprefix property of the resource group is set correctly.
		 * If this error occurs in any method other then VALIDATE,
		 * HA-NFS would attempt to recover the situation by either
		 * failing over to another node or (in case of Stop and
		 * Postnet_stop) by rebooting the node.
		 */
		scds_syslog(LOG_ERR,
		    "Unable to access directory %s:%s.",
		    statmondir, strerror(err));
		if (print_messages) {
			(void) fprintf(stderr, gettext("Unable to access "
				"directory %s:%s.\n"), statmondir,
				gettext(strerror(err)));
		}
		statmondir[0] = (char)0;
		/*
		 * Create the file which indicates that
		 * Pathprefix doesn't exist
		 */
		(void) create_prenet_fail_file(scds_handle);
		return (NULL);
	}
	scds_syslog_debug(2, "Statmon directory is %s.", statmondir);
	return (statmondir);
}

/*
 * get_dfstab_filename(): Given a RG name and R name returns
 * the pathname to the nfs dfstab file for this resource.
 * Returns NULL if there is any error encountered.
 * Formula to get the file name is
 * PATHPREFIX(rg)/SUNW.nfs/dfstab.{resource_name}
 * Returns static memory. caller should not free() it
 * or write into it.
 * The same restrictions as get_statmon_dir() apply here
 * as well.
 */

char *
get_dfstab_filename(scds_handle_t scds_handle, boolean_t print_messages)
{
	static	char	dfsfile[PATH_MAX] = "";
	char	*statmon_dir;
	struct statvfs64	sbuf;

	if (dfsfile[0] != 0) {
		return (dfsfile);	/* Already computed */
	}
	statmon_dir = get_statmon_dir(scds_handle, print_messages);
	if (statmon_dir == NULL) {
		return (NULL);	/* Msgs are already sysloged */
	}
	/*
	 * Make sure there is a
	 * PATHPREFIX/SUNW.nfs/dfstab.<resource_name> file
	 */
	(void) sprintf(dfsfile, "%s/dfstab.%s", statmon_dir,
		(char *)scds_get_resource_name(scds_handle));
	/*
	 * access() may return cached values. Using this together
	 * with statvfs64() guarantees permission and status.
	 */
	if (access(dfsfile, R_OK) || statvfs64(dfsfile, &sbuf)) {
		int err = errno;
		scds_syslog(LOG_ERR,
		    "File %s is not readable: %s.",
		    dfsfile, strerror(err));
		if (print_messages) {
			(void) fprintf(stderr, gettext("File %s is not "
				"readable: %s.\n"), dfsfile,
				gettext(strerror(err)));
		}
		dfsfile[0] = (char)0;
		return (NULL);
	}
	scds_syslog_debug(2, "Dfstab file is %s.", dfsfile);
	return (dfsfile);
}

/*
 * check_pathprefix()
 * If the dfstab.<rnam> does not exist,
 * check to see if the resource failed early
 * in PRENET_START phase due to unavailability
 * of Pathprefix. This is done by looking at
 * the existence of no-pathprefix/<r-name> file.
 * Used by the STOP and POSTNET_STOP methods
 * to exit successfully. This makes sure that
 * the node doesn't abort in this case.
 *
 * The intended usage is as follows
 * Return code 0: Means the caller exit()s 0
 * Return code <0: caller should exit(1)
 * Return code >0: caller should continue
 */

int
check_pathprefix(scds_handle_t scds_handle)
{
	struct statvfs64 sbuf;
	char	*rg, *pathprefix;
	char	dfsfile[MAXPATHLEN];

	/* Get resource group name */
	rg = (char *)scds_get_resource_name(scds_handle);

	/* Get resource group pathprefix */
	pathprefix = (char *)scds_get_rg_pathprefix(scds_handle);
	if ((pathprefix == NULL) || (pathprefix[0] == '\0')) {
		scds_syslog(LOG_ERR, "Pathprefix is not set "
			"for resource group %s.", rg);
		return (NULL);
	}

	/* Check to see if dfstab file is available */
	(void) sprintf(dfsfile, "%s/SUNW.nfs/dfstab.%s",
		pathprefix, (char *)scds_get_resource_name(scds_handle));
	if (statvfs64(dfsfile, &sbuf) == 0) {
		/* No need to do more */
		return (1);	/* Caller should continue */
	}

	/*
	 * dfstab.<rname> does not exist, check if
	 * PRENET_START failed due to Pathprefix
	 * not being mounted. If so,
	 * return 0, caller should exit(0).
	 */
	if (check_prenet_fail_file(scds_handle) == 0) {
		/* Resource had failed to start */
		return (0);
	}
	/*
	 * dfstab.<rname> does not exist, but it did exist
	 * at the time prenet_start method ran. We can't
	 * be sure how much progress start methods were
	 * able to make. return (-1). The caller would
	 * exit(1) aborting the node.
	 */
	return (-1);
}

/*
 * svc_validate()
 * Make sure that
 * 1. The RG has a valid PATHPREFIX
 * 2. There is a PATHPREFIX/SUNW.nfs/dfstab.<resource_name> file.
 */
int
svc_validate(scds_handle_t scds_handle, boolean_t print_messages)
{
	int	i, rc = SCHA_ERR_NOERR;
	char	*s, *dfstabfile = NULL;
	scds_net_resource_list_t *snrlp = NULL;

#if	(SOL_VERSION >= __s10)

	scha_resource_t local_handle;
	scha_extprop_value_t val;
	boolean_t create_flag = B_FALSE;
	boolean_t init_flag = B_TRUE;
	char	*rs_name = NULL;

	if (print_messages) {
		/*
		 * Means that this is called by validate method.
		 * Determine if this is creation or update of resource.
		 * If resource is being created, we do not want to log
		 * certain S10/SMF error messages before INIT has
		 * had a chance to run.
		 */
		rc = scha_resource_open(scds_get_resource_name(scds_handle),
			scds_get_resource_group_name(scds_handle),
			&local_handle);
		if (rc == SCHA_ERR_RSRC)
				create_flag = B_TRUE;
	}

	/*
	 * Skip if resource is being created
	 */
	if (!create_flag) {
		/*
		 * Make sure INIT property is set in RT.
		 * After an upgrade from sc3.0u3/s9 to sc3.1u4/s10,
		 * when the user tries to bring the RG from unmanaged
		 * state to managed state, the HA-NFS agent fails at
		 * the VALIDATE method. Since the INIT method disables
		 * SMF manifest for NFS, We need to validate SMF
		 * manifest properties only if an RT upgrade is done.
		 */
		rc = scha_resource_open(scds_get_resource_name(scds_handle),
			scds_get_resource_group_name(scds_handle),
			&local_handle);
		if (rc == SCHA_ERR_NOERR) {
			rc = scha_resource_get(local_handle, SCHA_INIT, &val);
			rs_name = (char*) scds_get_resource_name(scds_handle);
			/*
			 * close the resource handle
			 */
			(void) scha_resource_close(local_handle);
			if (rc == SCHA_ERR_PROP) {
				scds_syslog_debug(DBG_LEVEL_HIGH,
					"resource property %s of resource "
					"%s: is not set.", "INIT", rs_name);
				init_flag  = B_FALSE;
			}
		}
	}


	if (os_newer_than_s10() && !create_flag && init_flag) {
		for (i = 0; s = svcs[i]; i++) {

			scds_syslog_debug(DBG_LEVEL_HIGH,
				"Checking properties of %s.", s);

			if (!is_startd_duration_transient(s, B_FALSE)) {
				/*
				 * SCMSGS
				 * @explanation
				 * A property of the specified SMF service was
				 * not set to the expected value. This could
				 * cause unpredictable behavior of the service
				 * and failure to detect faults.
				 * @user_action
				 * If possible, update the property value
				 * manually using 'svccfg' commands. This
				 * could also be done by running the INIT
				 * method manually or re-creating the
				 * resource. If problem persists, please
				 * contact your Sun support representative for
				 * further assistance.
				 */
				scds_syslog(LOG_ERR,
					"Property %s not set to '%s' for "
					"%s. INIT method was not run or "
					"has failed on this node.",
					"startd/duration", "transient", s);
				if (print_messages)
					fprintf(stderr, gettext(
					    "Property %s not set to '%s' for "
					    "%s. INIT method was not run or "
					    "has failed on this node.\n"),
					    "startd/duration", "transient", s);
				return (1);
			}
			if (get_application_auto_enable(s)) {
				scds_syslog(LOG_ERR,
					"Property %s not set to '%s' for "
					"%s. INIT method was not run or "
					"has failed on this node.",
					"application/auto_enable",
					"false", s);
				if (print_messages)
					fprintf(stderr, gettext(
					    "Property %s not set to '%s' for "
					    "%s. INIT method was not run or "
					    "has failed on this node.\n"),
					    "application/auto_enable",
					    "false", s);
				return (1);
			}
		}
	}

#endif	/* SOL_VERSION >= __s10 */

	/*
	 * dfstab has to be validated for resource creation and
	 * updates to pathprefix or Resource_dependencies.
	 */
	if (dfstab_check == B_TRUE) {
		dfstabfile = get_dfstab_filename(scds_handle, print_messages);
		if (dfstabfile == NULL) {
			/* A message has already been logged */
			return (1);
		}
	}

	/*
	 * Make sure that the RG has some LogicalHostname resources,
	 * these could be set via the Logical_Hostnames_Used
	 * property or they could just be LogicalHostname resources
	 * in the RG.
	 */
	rc = scds_get_rs_hostnames(scds_handle, &snrlp);
	if (rc != SCHA_ERR_NOERR || snrlp == NULL ||
		snrlp->num_netresources == 0) {
		scds_syslog(LOG_ERR,
		    "No LogicalHostname resource in resource group.");
		if (print_messages) {
			(void) fprintf(stderr, gettext("No LogicalHostname "
				"resource in resource group.\n"));
		}
		rc = 1;
		goto finished_validate;
	}

	if (scds_get_rs_cheap_probe_interval(scds_handle) >=
	    scds_get_rs_thorough_probe_interval(scds_handle)) {
		/*
		 * SCMSGS
		 * @explanation
		 * The value of Thorough_Probe_Interval specified in scrgadm
		 * command or in CCR table was smaller than
		 * Cheap_Probe_Interval.
		 * @user_action
		 * Reissue the scrgadm command with appropriate values as
		 * indicated.
		 */
		scds_syslog(LOG_ERR,
		    "%s should be larger than %s.",
		    SCHA_THOROUGH_PROBE_INTERVAL,
		    SCHA_CHEAP_PROBE_INTERVAL);
		if (print_messages) {
			(void) fprintf(stderr, gettext("%s should be larger "
				"than %s.\n"), SCHA_THOROUGH_PROBE_INTERVAL,
				SCHA_CHEAP_PROBE_INTERVAL);
		}
		rc = 1;
		goto finished_validate;
	}

	/* Check to make sure other important X props are set */
	if (scds_get_ext_monitor_retry_count(scds_handle) <= 0) {
		scds_syslog(LOG_ERR,
		    "Property %s is not set.",
		    "Monitor_retry_count");
		if (print_messages) {
			(void) fprintf(stderr, gettext("Property %s is not "
				"set.\n"), "Monitor_retry_count");
		}
		rc = 1;
		goto finished_validate;
	}
	if (scds_get_ext_monitor_retry_interval(scds_handle) <= 0) {
		scds_syslog(LOG_ERR,
		    "Property %s is not set.",
		    "Monitor_retry_interval");
		if (print_messages) {
			(void) fprintf(stderr, gettext("Property %s is not "
				"set.\n"), "Monitor_retry_interval");
		}
		rc = 1;
	}

finished_validate:

	if (snrlp)
		scds_free_net_list(snrlp);

	/* We should be able to work with this setting */
	return (rc);
}

/*
 * mon_start()
 * Uses the library function to start the system fault monitor and
 * resource fault monitor under PMF.
 */
int
mon_start(scds_handle_t scds_handle)
{
	scds_syslog_debug(4,
		"Calling MONITOR_START method for resource %s.",
		(char *)scds_get_resource_name(scds_handle));

	/* start nfs_daemons_probe */
	if (start_system_monitor(scds_handle) != 0) {
		/* Messages already logged */
		return (1);
	}

	/* start the nfs resource monitor */
	if (scds_pmf_start(scds_handle, SCDS_PMF_TYPE_MON,
		SCDS_PMF_SINGLE_INSTANCE, "nfs_probe", 0)
		!= SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		    "Failed to start fault monitor.");
		return (1);
	}

	scds_syslog(LOG_INFO,
	    "Completed successfully.");

	return (SCHA_ERR_NOERR);
}

/*
 * mon_stop()
 * Uses the library function to stop nfs_probe for this
 * resource via PMF.
 */
int
mon_stop(scds_handle_t scds_handle)
{
	pid_t	pid;
	char	probe_path[PATH_MAX];

	scds_syslog_debug(4,
		"Calling MONITOR_STOP method for resource %s.",
		(char *)scds_get_resource_name(scds_handle));

	if (scds_pmf_stop(scds_handle, SCDS_PMF_TYPE_MON,
		SCDS_PMF_SINGLE_INSTANCE, SIGTERM, -1)
		!= SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		    "Failed to stop fault monitor.");
		return (1);
	}

	scds_syslog(LOG_INFO,
	    "Stopped the fault monitor.");

	/* If there is no other resource monitor running, */
	/* stop the system fault monitor */
	(void) sprintf(probe_path, "%s/%s",
		scds_get_rt_rt_basedir(scds_handle), HANFS_R_PROBE_PROC);

	if (checkproc(probe_path, &pid) != NFS_ERR_NONE) {
		if (stop_system_monitor() != 0) {
			/* Messages already logged */
			return (1);
		}
		/*
		 * SCMSGS
		 * @explanation
		 * The HA-NFS system fault monitor was stopped successfully.
		 * @user_action
		 * No action required.
		 */
		scds_syslog(LOG_INFO,
		    "Stopped the HA-NFS system fault monitor.");
	}

	return (SCHA_ERR_NOERR);
}

/*
 * check_stat_all_fs() -
 * Check statvfs64() on all file systems.
 * second arg if for special case of triggering from PROBE. We already
 * log a message from nfs_share_dfstab. We dont want to do it again here.
 * Returns 0 for ok on all paths, number of bad paths for errors.
 */
static int
check_stat_all_fs(shared_path_list_t **fslp, int justshared)
{
	shared_path_list_t	*fsp;
	struct statvfs64	st;
	int	fs_cnt = 0, bad_cnt = 0;
	int	rc;

	for (fsp = *fslp; fsp != NULL; fsp = fsp->fs_next) {
		fs_cnt = fs_cnt + 1;
		rc = statvfs64(fsp->fs_path, &st);
		if (rc == 0) {
			if (fsp->fs_status != HANFS_FS_OK) {
				scds_syslog_debug(1,
				    "file system %s is ok now, "
				    "previously in HANFS_FS_STAT_FAILED",
				    fsp->fs_path);
				fsp->fs_status = HANFS_FS_OK;
			}
			continue;
		}

		/* statvfs64() failed */
		bad_cnt++;

		if (!justshared && (fsp->fs_status == HANFS_FS_OK)) {
			/* a regression */
			/*
			 * SCMSGS
			 * @explanation
			 * HA-NFS fault monitor reports a probe failure on a
			 * specified file system.
			 * @user_action
			 * Make sure the specified path exists.
			 */
			scds_syslog(LOG_ERR,
			    "stat of share path %s failed: %s.",
			    fsp->fs_path, strerror(errno));
		}
		if (fsp->fs_status == HANFS_FS_OK)
			fsp->fs_status = HANFS_FS_STAT_FAILED;

	}

	return (bad_cnt);
}

/*
 * svc_probe()
 * Probes the shared path on the specified ipaddress.
 * Returns a float number which gets accumulated over
 * a period (RETRY_INTERVAL).
 * Currently we only do stat() on all shared path.
 * The return value from this function is a
 * floating point number between 0 and 1. 0 indicates
 * success and 1.0 mean complete failure. A value between
 * the two is considered a partial failure. These failures
 * are accumulated over a period (Retry_Interval) and
 * appropiate action (restart or failover) is taken when
 * suitable theresolds are reached (1.0 for restarts,
 * Retry_Times for giveover).
 * XXX Implement more stuff here in HA-NFS phaseII
 * AH:
 * 1. This routine needs to run under the state lock, or
 * else some other NFS probe (for some other resource)
 * could be restarting daemons which we are doing RPCs
 * to it.
 * 2. Probably a better solution is to have a per-node
 * NFS monitoring daemon which would monitor the health
 * of the daemons per say. In which case, this routine
 * should ONLY do some basic stuff on a per-resource basis
 * such as monitoring the health of individual shared paths.
 * 3. The timeouts for the RPC operations need to be tunable.
 * 4. Failure to validate statd/lockd functionality via
 * locking checks should not result in a restart of those
 * daemons. Or else we might make a bad situation worse.
 * We should just issue syslog() messages and continue.
 */

int
svc_probe(scds_handle_t scds_handle, char *dfs_path, shared_path_list_t **fslp,
	int isdtabtouched, int ismstabtouched, time_t *last_cache_msts)
{
	char *rsname = (char *)scds_get_resource_name(scds_handle);
	int rc = 0;

	scds_syslog_debug(5,
	    "Probing file systems of resource <%s>.", rsname);

	if (*fslp == NULL) {
		scds_syslog_debug(5,
		    "Resource %s has no file system to probe.", rsname);
		return (0);
	}

	/*
	 * If sharetab has been modified in the last interval,
	 * check and share the dfstab.<rsname> again. This way
	 * we avoid sharing an already shared filesystem. Note
	 * that we use NFS_OP_RDCHKSHARE here to inform that the
	 * sharetab has changed and hence to reread the sharetab
	 * file to populate the share list cache. ismstabtouched
	 * is 2 in such cases.
	 *
	 * If mnttab has been modified in the last interval, check
	 * and share the dfstab.<rsname> again. This way we avoid
	 * sharing an already shared filesystem. This is done using
	 * NFS_OP_CHKSHARE. ismstabtouched is 1 in such cases.
	 *
	 * If the dfstab has been modified in the last interval,
	 * just share the dfstab.<rsname> again. This is because
	 * the share properties can change if dfstab file has been
	 * modified. This is done using NFS_OP_SHARE.
	 *
	 * The return code is of not much value here.
	 */
	if (isdtabtouched) {
		rc = nfs_share_dfstab(dfs_path, NFS_OP_SHARE);
	} else if (ismstabtouched == 1) {
		rc = nfs_share_dfstab(dfs_path, NFS_OP_CHKSHARE);
	} else if (ismstabtouched == 2) {
		rc = nfs_share_dfstab(dfs_path, NFS_OP_RDCHKSHARE);
	}

	/*
	 * Remember, last_cache_msts is primarily reset in check_if_touched().
	 * But, we still want to Reset the cached sharetab/mnttab mtime value
	 * to the current time, because we just shared some paths. We certainly
	 * dont want to trigger the sharetab/mnttab refresh cycle again.
	 */
	if (ismstabtouched && rc > 0)
		*last_cache_msts = time(0);

	/*
	 * Now probe the share paths (stat them). This should be done
	 * in both cases (files touched or not) because, the status message
	 * of the resource depends on the stat failure of atleast one path.
	 */
	return (check_stat_all_fs(fslp, ismstabtouched | isdtabtouched));
}

/*
 * start_system_monitor()
 * Check if nfs_daemons_probe is already running under PMF.
 * If it is, return ok, else start it under PMF.
 * Returns 0 for success, others for errors.
 */
int
start_system_monitor(scds_handle_t scds_handle)
{
	char 	syscmd[SCDS_ARRAY_SIZE];
	char	internal_err_str[INT_ERR_MSG_SIZE];
	int	rc;
	int	lockid;

	/* Grab the system-wide lock. */
	lockid = nfs_serialize_lock();

	/*
	 * Query for pmf status:
	 *  pmfadm -q <tag>
	 */
	(void) sprintf(syscmd, "%s -q %s", SCDS_PMFADM, NFS_DAEMONS_PMFTAG);
	rc = run_system(HANFS_LOG_NONE, syscmd);
	if (rc == 0) {
		/* nfs_daemons_probe is registered and running under */
		scds_syslog_debug(1,
		    "System fault monitor is already running.");
		goto finished;
	}

	/*
	 *  Start nfs_daemons_probe
	 *	pmfadm -c NFS_DAEMONS_PMFTAG -n <PMF_ARG_N> -t <PMF_ARG_T> \
	 *		-C 0 <basedir>/<svc>_probe
	 */
	rc = snprintf(syscmd, sizeof (syscmd),
	    "%s -c %s -n %d -t %d -C 0 %s/nfs_daemons_probe -R %s -T %s -G %s",
	    SCDS_PMFADM, NFS_DAEMONS_PMFTAG,
	    scds_get_ext_monitor_retry_count(scds_handle),
	    scds_get_ext_monitor_retry_interval(scds_handle),
	    scds_get_rt_rt_basedir(scds_handle),
	    (char *)scds_get_resource_name(scds_handle),
	    (char *)scds_get_resource_type_name(scds_handle),
	    (char *)scds_get_resource_group_name(scds_handle));

	if (rc == -1) {
		(void) sprintf(internal_err_str,
		    "Failed to form start command: Output error encountered");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		goto finished;
	}

	scds_syslog_debug(7,
	    "Starting command under pmf: %s", syscmd);

	rc = run_system(LOG_WARNING, syscmd);
	if (rc != 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * Process monitor facility has failed to start the HA-NFS
		 * system fault monitor.
		 * @user_action
		 * Check whether the system is low in memory or the process
		 * table is full and correct these probelms. If the error
		 * persists, use scswitch to switch the resource group to
		 * another node.
		 */
		scds_syslog(LOG_ERR,
		    "Failed to start HA-NFS system fault monitor.");
		goto finished;
	}

	scds_syslog_debug(LOG_INFO,
	    "Started the HA-NFS system fault monitor.");

	rc = 0;

finished:
	/* Release the lock */
	(void) nfs_serialize_unlock(lockid);
	return (rc);
}

/*
 * stop_system_monitor()
 */
int
stop_system_monitor(void)
{
	char		syscmd[SCDS_ARRAY_SIZE];
	int		rc;
	int	lockid;

	/* Grab the system-wide lock. */
	lockid = nfs_serialize_lock();

	/*
	 * Query for pmf status:
	 *  pmfadm -q <tag>
	 */
	(void) sprintf(syscmd, "%s -q %s", SCDS_PMFADM, NFS_DAEMONS_PMFTAG);
	rc = run_system(HANFS_LOG_NONE, syscmd);
	if (rc != 0) {
		scds_syslog_debug(1,
		    "System fault monitor is not running.");
		rc = 0;
		goto finished;
	}

	scds_syslog_debug(3,
	    "Stopping monitoring command under pmf: %s", syscmd);

	/*
	 * Kill nfs_daemons_probe process via pmf.
	 *	pmfadm -s <tag>
	 */
	(void) sprintf(syscmd, "%s -s %s %d", SCDS_PMFADM,
	    NFS_DAEMONS_PMFTAG, SIGKILL);

	rc = run_system(LOG_WARNING, syscmd);
	if (rc != 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * Process monitor facility has failed to stop the HA-NFS
		 * system fault monitor.
		 * @user_action
		 * Use pmfadm(1M) with -s option to stop the HA-NFS system
		 * fault monitor with tag name "cluster.nfs.daemons". If the
		 * error still persists, then reboot the node.
		 */
		scds_syslog(LOG_ERR,
		    "Failed to stop HA-NFS system fault monitor.");
		goto finished;
	}

	scds_syslog(LOG_INFO,
	    "Stopped the HA-NFS system fault monitor.");
	rc = 0;

finished:
	/* Release the lock */
	(void) nfs_serialize_unlock(lockid);

	return (rc);
}

/*
 * nfs_set_env()
 * Need to add /usr/sbin to the PATH
 */
static int
nfs_set_env(void)
{
	return (putenv("PATH=/usr/bin:/usr/cluster/bin:/usr/sbin"));
}

#define	LOCK_MSG_HELD  "Automatically created, do not touch. pid %d\n"
#define	LOCK_MSG_UNHELD  "Automatically created, do not touch. Lock Not Held\n"

static boolean_t lock_in_use = B_FALSE;

/*
 * nfs_serialize_lock()
 * Grabs a system-wide lock (file-lock) to serialize
 * the accesses to various state files and such.
 * Returns an id (basically just the descriptor),
 * to be used in the unlock calls.
 */
int
nfs_serialize_lock(void)
{
	char	lockfilename[PATH_MAX];
	char	msg[1024];
	flock_t fl;
	int		lockfd;

	/* check the caller try to get lock twice */
	if (lock_in_use) {
		scds_syslog(LOG_WARNING, "INTERNAL ERROR: %s.",
		    "A program tried to claim the system lock twice");
		return (-1);
	}

	/*
	 * About the first thing the methods do, good place
	 * to set env stuff up.
	 */
	if (nfs_set_env() != 0)
		return (-1);

	if (mkdirp(HANFS_STATE_DIR, (mode_t)0755) != 0) {
		if (errno != EEXIST) {
			scds_syslog(LOG_ERR,
			    "Unable to create directory %s: %s.",
			    HANFS_STATE_DIR, strerror(errno));
			return (-1);
		}
	}
	(void) sprintf(lockfilename, "%s/%s", HANFS_STATE_DIR, HANFS_LOCK_FILE);
	lockfd = open(lockfilename, O_RDWR | O_CREAT, 0755);
	if (lockfd < 0) {
		scds_syslog(LOG_ERR,
		    "Unable to open %s: %s.",
		    lockfilename, strerror(errno));
		return (-1);
	}
	scds_syslog_debug(1, "About to attempt getting lock.");
	(void) memset((char *)&fl, 0, sizeof (fl));
	fl.l_type = F_WRLCK;
	if (fcntl(lockfd, F_SETLKW, &fl) != 0) {	/* Wait for lock */
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		scds_syslog(LOG_ERR,
		    "Unable to lock %s: %s.",
		    lockfilename, strerror(errno));
		(void) close(lockfd);
		return (-1);
	}
	(void) ftruncate(lockfd, (off_t)0);
	(void) sprintf(msg, LOCK_MSG_HELD, (int)getpid());
	(void) write(lockfd, msg, strlen(msg));
	scds_syslog_debug(1, "Returning from lock() routine.");

	lock_in_use = B_TRUE;
	return (lockfd);
}

/*
 * nfs_serialize_unlock(int lockid)
 * Releases the serialization lock.
 */
int
nfs_serialize_unlock(int lockid)
{
	flock_t fl;

	if (lockid < 0) {
		char	internal_err_str[INT_ERR_MSG_SIZE];

		(void) snprintf(internal_err_str,
		    sizeof (internal_err_str),
		    "Failed to release the serialization "
		    "lock: Invalid lock id %d",
		    lockid);
		/* ignore err return code above */
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
		    internal_err_str);
		return (-1);
	}
	(void) ftruncate(lockid, (off_t)0);
	(void) lseek(lockid, 0L, SEEK_SET);
	(void) write(lockid, LOCK_MSG_UNHELD, strlen(LOCK_MSG_UNHELD));
	scds_syslog_debug(1, "Releasing serialization lock.");
	(void) memset((char *)&fl, 0, sizeof (fl));
	fl.l_type = F_UNLCK;
	if (fcntl(lockid, F_SETLKW, &fl) != 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		scds_syslog(LOG_ERR,
		    "Unable to unlock file: %s.",
		    strerror(errno));
		(void) close(lockid);
		return (-1);
	}
	(void) close(lockid);

	lock_in_use = B_FALSE;
	return (0);
}

/*
 * run_cmd()
 * A wrapper around system(): The difference is
 * that it processes the results of running
 * the system() call and syslogs an appropiate
 * error message.  The return value
 * is same as returned by system() AFTER doing
 * a WEXITSTATUS() on it. If the command
 * does not exit cleanly, -1 is returned.
 *
 * All errors are syslogged at LOG_ERR, except
 * in the case of the command exiting with a
 * non-zero exit-code, in which case the
 * passed in pri argument is used to syslog.
 * The idea is that if that is an expected
 * outcome, callers would call us with LOG_DEBUG.
 */
static int
run_cmd(int pri, char *cmd)
{
	int		rc = 0;
	char	msg[1024];
	int		p = LOG_ERR;

	rc = system(cmd);
	if (rc == -1) {
		scds_syslog(LOG_ERR,
		    "Cannot execute %s: %s.",
		    cmd, strerror(errno));
		return (rc);
	}

	if (WIFSIGNALED((uint_t)rc)) {
		rc = -1;
		(void) snprintf(msg, sizeof (msg),
			"%s terminated with signal %d",
			cmd, WTERMSIG((uint_t)rc));
	} else if (WIFSTOPPED((uint_t)rc)) {
		rc = -1;
		(void) snprintf(msg, sizeof (msg),
			"%s stopped with signal %d",
			cmd, WSTOPSIG((uint_t)rc));
	} else if (WIFEXITED((uint_t)rc)) {
		rc = WEXITSTATUS((uint_t)rc);
		(void) snprintf(msg, sizeof (msg),
			"%s exited with status %d",
			cmd, rc);
		if (rc != 0)
			p = pri;
		else
			p = LOG_DEBUG;
	} else {
		rc = -1;
		(void) snprintf(msg, sizeof (msg),
			"%s returned with status %d",
			cmd, rc);
	}

	/*
	 * SCMSGS
	 * @explanation
	 * HA-NFS attempted to run the specified command to perform some
	 * action which failed. The specific reason for failure is also
	 * provided.
	 * @user_action
	 * HA-NFS will take action to recover from this failure, if possible.
	 * If the failure persists and service does not recover, contact your
	 * service provider. If an immediate repair is desired, reboot the
	 * cluster node where this failure is occuring repeatedly.
	 */
	scds_syslog(p, "Command %s failed to run: %s.", cmd, msg);

	return (rc);
}

/*
 * run_system()
 * A wrapper around run_cmd(): If the command being
 * run exits non-zero, the output from the command
 * (stderr) is captured and syslogged at the
 * supplied priority. MAXimum of 1024 chars are logged,
 * the rest of the output is silently ignored. Intended
 * to be used to run
 * share/unshare commands. The return value
 * is same as returned by system() AFTER doing
 * a WEXITSTATUS() on it.
 */
int
run_system(int pri, char *cmd)
{
	char	runcmd[2 * SCDS_CMD_SIZE];
	char	outfile[PATH_MAX];
	char	buf[1024];
	FILE	*fp = NULL;
	int		n, rc;

	(void) snprintf(outfile, sizeof (outfile),
		"%s/.run.out.%d", HANFS_STATE_DIR,
		(int)getpid());
	(void) snprintf(runcmd, sizeof (runcmd),
		"%s > %s 2>&1", cmd,
		outfile);

	rc = run_cmd(pri, runcmd);
	if (rc == -1) {
		(void) unlink(outfile);
		return (rc);
	}
	if (rc != 0) {
		buf[0] = (char)0;
		fp = fopen(outfile, "r");
		if (fp != NULL) {
			n = (int)fread(buf, sizeof (char),
				sizeof (buf) - 1, fp);
			if (n > 0)
				buf[n - 1] = (char)0;
		} else {
			scds_syslog(pri, "Unable to open %s: %s.",
				outfile, strerror(errno));
			buf[0] = (char)0;
		}
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		scds_syslog(pri, "%s failed: %s.", cmd, buf);
	}

	if (fp)
		(void) fclose(fp);
	(void) unlink(outfile);
	return (rc);
}

/*
 * checkproc -
 * Uses pgrep to see if a process is running.
 * procname is expected to contain
 * an absolute path and the process is supposed to be
 * running as well. A "full args" check is done, however
 * procname is matched only against the beginning of
 * the process name (so process arguments will not screw us up).
 */
nfs_fm_err_t
checkproc(char *procname, pid_t *pid)
{
	int		n, rc;
	char	cmd[2048];
	char	outfile[PATH_MAX];
	FILE	*fp = NULL;

	(void) sprintf(outfile, "%s/.check.out.%d", HANFS_STATE_DIR,
		(int)getpid());

	/*
	 * With Solaris zones, each local zone has an instance
	 * of statd/lockd deamons and also a "shadow" process
	 * in the global zone. So in the global zone, we have
	 * multiple copies of statd and lockd daemons. One on
	 * behalf of the global zone and one on behalf of the
	 * local zone. Hence during startup, if HA-NFS START
	 * method lands up finding more then 1 pid for, say,
	 * statd, it would kill the first one and wait for the
	 * RPC registration of statd to go away. However, it
	 * might actually have killed the local-zone-proxy
	 * instance of statd/lockd. In which case, the RPC
	 * registration in the global zone (which is really done
	 * by the process running on behalf of the global zone),
	 * will never go away and the method would timeout.
	 * To deal with this "-z global" option to pgrep will
	 * limit the HA-NFS to deal with nfs deamons in global
	 * zone only.
	 *
	 * RPCBIND executable is present in the administrative
	 * directory /usr/sbin and administrators are bound to
	 * start rpcbind with out giving the complete path name.
	 *
	 * Hence, we need to check if process is started as
	 * "rpcbind" or "/usr/sbin/rpcbind".
	 */
	if (strcmp(procname, NFS_RPCBIND_PATH) == 0) {
		if (os_newer_than_s10()) {
			(void) sprintf(cmd,
				"/usr/bin/pgrep -x -z global "
				"\\^%s > %s 2>/dev/null",
				basename(procname), outfile);
		} else {
			(void) sprintf(cmd,
				"/usr/bin/pgrep -x "
				"\\^%s > %s 2>/dev/null",
				basename(procname), outfile);
		}
		scds_syslog_debug(4, "checkproc(): Checking if %s is"
			" running with command %s OR %s.",
			procname, basename(procname), cmd);
	} else {
		if (os_newer_than_s10()) {
			(void) sprintf(cmd,
				"/usr/bin/pgrep -f -z global "
				"\\^%s > %s 2>/dev/null",
				procname, outfile);
		} else {
			(void) sprintf(cmd,
				"/usr/bin/pgrep -f \\^%s > %s "
				" 2>/dev/null", procname, outfile);
		}
		scds_syslog_debug(4, "checkproc(): Checking if %s is "
			"running with command %s.", procname, cmd);
	}

	rc = run_cmd(LOG_DEBUG, cmd);
	if (rc == -1) {
		*pid = -1;
		return (NFS_ERR_UNKNOWN);
	}

	if (rc == 0) {
		/* Already running */
		scds_syslog_debug(4, "%s is running.", procname);
		/* Get the pid */
		fp = fopen(outfile, "r");
		if (fp == NULL) {
			/* This is a problem. What pid do we return? */
			scds_syslog(LOG_ERR,
			    "Unable to open %s: %s.",
			    outfile, strerror(errno));
			*pid = -1;
			rc = NFS_ERR_UNKNOWN;
			goto finished;
		}
		n = fscanf(fp, "%d", (int *)pid);
		if (n != 1) {
			/*
			 * SCMSGS
			 * @explanation
			 * Need explanation of this message!
			 * @user_action
			 * Need a user action for this message.
			 */
			scds_syslog(LOG_ERR,
			    "Unable to read %s: %s.",
			    outfile, strerror(errno));
			*pid = -1;
			rc = NFS_ERR_UNKNOWN;
			goto finished;
		}
		rc = NFS_ERR_NONE;
	} else if (rc == 1) {
		/* No processes were matched */
		scds_syslog_debug(4, "%s is not running.", procname);
		rc = NFS_ERR_PID_NOT_EXIST;
		*pid = -1;
	} else {
		/* return NFS_ERR_UNKNOWN if exit status of pgrep */
		/* is other than 0 and 1 */
		scds_syslog(LOG_ERR,
		    "INTERNAL ERROR: %s.", "pgrep failed");
		rc = NFS_ERR_UNKNOWN;
		*pid = -1;
	}

finished:
	if (fp)
		(void) fclose(fp);
	(void) unlink(outfile);
	scds_syslog_debug(3, "checkproc(%s): Returning %d.", procname, rc);
	return (rc);
}

/*
 * stop_nfs_daemon(procname, timeout)
 * Sends SIGKILL to the named process(es).
 * A FULL pathname is expected, ALL processes
 * matching the full pathname are killed.
 *
 * Update: stop_nfs_daemon() now takes a timeout argument
 * and waits for that timeout for the process to die.
 * It returns -1 if the named process did not die
 * in timeout.
 *
 * The timeout argument can be -1, which indicates
 * infinite timeout. The idea is that the probe
 * would call this function with a timeout and
 * the methods will call the function without
 * a timeout (timeout = -1).
 */
int
stop_nfs_daemon(char *progname, rpcprog_t prognum,
	rpcvers_t progversion, time_t timeout)
{
	CLIENT  *rpcclnt = NULL;
	struct netconfig *nconf_udp = NULL;
	struct netconfig *nconf_tcp = NULL;
	char *hostname = "localhost";
	boolean_t bound_to_tcp = B_TRUE;
	boolean_t bound_to_udp = B_TRUE;
	char	internal_err_str[INT_ERR_MSG_SIZE];
	pid_t   pid;
	time_t  start_time = time(NULL);
	int procrc = 0;
	int sig_to_use = SIGKILL;

	if ((procrc = checkproc(progname, &pid)) == NFS_ERR_PID_NOT_EXIST) {
		scds_syslog_debug(2, "stop_nfs_daemon(): %s is not running.",
		progname);
		return (0);
	}
	if (procrc != NFS_ERR_NONE) {
		(void) snprintf(internal_err_str, sizeof (internal_err_str),
			"Attempt to check for process %s failed with %d.",
			progname, procrc);
		/*
		 * SCMSGS
		 * @explanation
		 * An internal error has occurred.
		 * @user_action
		 * Save a copy of the /var/adm/messages files on all nodes.
		 * Contact your authorized Sun service provider for assistance
		 * in diagnosing the problem.
		 */
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s",
			internal_err_str);
		return (-1);
	}

	/*
	 * SCMSGS
	 * @explanation
	 * The specified NFS daemon is being stopped by the HA-NFS
	 * implementation.
	 * @user_action
	 * This is an informational message. No action is needed.
	 */
	scds_syslog(LOG_NOTICE,
		"Stopping NFS daemon %s.",
		progname);

	/* Use the right signal for nfsd */
	if ((strcmp(progname, NFS_NFSD_PATH) == 0) && nfsd_has_warmstart())
		sig_to_use = SIGUSR1;

	if (kill(pid, sig_to_use) != 0) {
		if (errno == ESRCH) {
			/* Process has died */
			return (0);
		}
		/*
		 * SCMSGS
		 * @explanation
		 * HA-NFS callback method attempted to stop the specified NFS
		 * process with SIGKILL but was unable to do so because of the
		 * specified error.
		 * @user_action
		 * The failure of the method would be handled by SunCluster.
		 * If the failure happened during starting of a HA-NFS
		 * resource, the resource would be failed over to some other
		 * node. If this happened during stopping, the node would be
		 * rebooted and HA-NFS service would continue on some other
		 * node. If this error persists, please contact your local SUN
		 * service provider for assistance.
		 */
		scds_syslog(LOG_ERR,
			"Attempting to kill pid %d name %s "
			"resulted in error: %s.",
			pid, progname, strerror(errno));
			return (-1);
	}

	/*
	 * Signal has been delivered to the daemon
	 * Now we wait till it completely dies.
	 * We can't simply wait for the pid to
	 * go away, we actually have to wait untill
	 * the daemon no longer is available to
	 * its clients over RPC.
	 * For some reason, even after the process
	 * goes away (i.e. its pid is gone), the
	 * transport end points are still not free.
	 * This manifests itself as the failure
	 * to restart the daemon a little later
	 * as the daemon is not able to bind to
	 * the transport endpoint as it is still
	 * not free.
	 * So we wait for the transport end-point
	 * to go away.
	 */
	nconf_udp = getnetconfigent("udp");
	nconf_tcp = getnetconfigent("tcp");
	if (nconf_udp == NULL || nconf_tcp == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * Attempt to get network configuration failed. The precise
		 * error strings is included with the message.
		 * @user_action
		 * Check to make sure that the /etc/netconfig file on the
		 * system is not corrupted and it has entries for tcp and udp.
		 * Additionally, make sure that the system has enough
		 * resources (particularly memory). HA-NFS would attempt to
		 * recover from this failure by failing over to another node,
		 * by crashing the current node, if this error occurs during
		 * STOP or POSTNET_STOP method execution.
		 */
		scds_syslog(LOG_ERR, "Failed to retrieve network "
			"configuration: %s.",
			nc_sperror());
		return (-1);
	}

	scds_syslog_debug(1, "Waiting for daemon %s to "
		" unregister from RPC program %d version %d.",
		progname, prognum, progversion);
	/*
	 * We now keep checking periodically (every 100 ms)
	 * whether the daemon is bound to any proto/family
	 * combo for which we still need to wait.
	 */
	do {
		if ((timeout > 0) &&
			(time(NULL) - start_time > timeout)) {
			/*
			 * SCMSGS
			 * @explanation
			 * HA-NFS attempted to stop the specified process id
			 * but was unable to stop the process in a timely
			 * fashion. Since HA-NFS uses the SIGKILL signal to
			 * kill processes, this indicates a serious overload
			 * or kernelproblem with the system.
			 * @user_action
			 * HA-NFS would take appropiate action. If this error
			 * occurs in a STOP method, the node would be
			 * rebooted. Increase timeout on the appropiate
			 * method.
			 */
			scds_syslog(LOG_ERR,
				"Process %s did not die in %d seconds.",
				progname, timeout);
			freenetconfigent(nconf_udp);
			freenetconfigent(nconf_tcp);
			return (-1);
		}
		if (bound_to_tcp == B_TRUE) {
			rpcclnt = clnt_tp_create(hostname, prognum, progversion,
				nconf_tcp);
			if (rpcclnt == NULL) {
				bound_to_tcp = B_FALSE;
				scds_syslog_debug(1,
					"Daemon %s unregistered from RPC "
					"program %d version %d "
					"on TCP protocol.",
					progname, prognum, progversion);
			} else {
				clnt_destroy(rpcclnt);
				rpcclnt = NULL;
				scds_syslog_debug(1,
					"Daemon %s still registered on RPC "
					"program %d version %d "
					"on TCP protocol.",
					progname, prognum, progversion);
			}
		}
		if (bound_to_udp == B_TRUE) {
			rpcclnt = clnt_tp_create(hostname, prognum, progversion,
				nconf_udp);
			if (rpcclnt == NULL) {
				bound_to_udp = B_FALSE;
				scds_syslog_debug(1,
					"Daemon %s unregistered from RPC "
					"program %d version %d "
					"on UDP protocol.",
					progname, prognum, progversion);
			} else {
				clnt_destroy(rpcclnt);
				rpcclnt = NULL;
				scds_syslog_debug(1,
					"Daemon %s still registered on RPC "
					"program %d version %d "
					"on UDP protocol.",
					progname, prognum, progversion);
			}
		}
		if (bound_to_tcp || bound_to_udp)
			(void) usleep(100000);

	} while (bound_to_tcp || bound_to_udp);

	/*
	 * We come here if the daemon has successfully
	 * de-registered from all protocols and address
	 * families.
	 */
	scds_syslog_debug(1, "daemon %s unregistered from "
		"RPC program %d version %d.",
		progname, prognum, progversion);

	freenetconfigent(nconf_udp);
	freenetconfigent(nconf_tcp);
	return (0);
}

#define	NFS_RMTAB	"/etc/rmtab"

/*
 * empty_rmtab()
 * Empty /etc/rmtab file just before restarting mountd.
 * The reasons of doing this are:
 * - A huge rmtab file could cause the restart of mountd very slow.
 * - It could contain incorrect information after switchover/failover.
 * - It has very little value in a cluster environment.
 */
static int
empty_rmtab()
{
	int fd;

	fd = open(NFS_RMTAB, O_RDWR | O_TRUNC);
	if (fd < 0) {
		scds_syslog(LOG_WARNING,
		    "Unable to open %s: %s.",
		    NFS_RMTAB, strerror(errno));
		return (-1);
	}

	(void) close(fd);
	return (0);
}

/*
 * startproc(procname, args)
 * Start the named command.  This function used to start the process
 * in the TS scheduling class explicitly, but this was changed so that
 * Sun Cluster could co-exist with SRM.
 */
int
startproc(char *procname, char *cmd)
{
	int		rc;
	pid_t	pid;

	/*
	 * This function is supposed to be idempotent. Hence first
	 * check that the process is not running.
	 */
	if (checkproc(procname, &pid) == NFS_ERR_NONE) {
		scds_syslog_debug(2,
		    "startproc(): %s is already running.", procname);
		return (0);
	}

	/* If this is for starting mountd, first empty /etc/rmtab file. */
	/* We should continue to start mountd even if empty_rmtab() */
	/* returns error */
	if (strcmp(procname, NFS_MOUNTD_PATH) == 0)
		(void) empty_rmtab();

	rc = run_system(LOG_ERR, cmd);
	return (rc);
}

/*
 * nfs_share_resource(scds_handle)
 * shares paths contained in the dfstab.resource file.
 * Takes care not to share paths which don't exist or are not
 * mounted.
 */
int
nfs_share_resource(scds_handle_t scds_handle)
{
	char	*dfsfile;

	dfsfile = get_dfstab_filename(scds_handle, B_FALSE);
	if (dfsfile == NULL) {
		/*
		 * We have been checking for this all the time
		 */
		return (1);
	}
	return (nfs_share_dfstab(dfsfile, NFS_OP_SHARE));
}

/*
 * nfs_unshare_resource(scds_handle)
 * unshares the paths contained in the dfstab.resource file.
 */
int
nfs_unshare_resource(scds_handle_t scds_handle)
{
	char	*dfsfile;

	dfsfile = get_dfstab_filename(scds_handle, B_FALSE);
	if (dfsfile == NULL) {
		/*
		 * We have been checking for this all
		 * the time and now THIS!?!  (*&#(*(*&#
		 */
		return (1);
	}
	return (nfs_share_dfstab(dfsfile, NFS_OP_UNSHARE));
}

/*
 * remove_lock_and_symlinks()
 * Removes the file named "hostname" and the symlink
 * named ip4.<dotted-decimal-address> and also ipv6.<dotted-decimal-address>.
 * This is done for both sm and sm.bak subdirectories
 * under the specified directory.
 */

static void
remove_lock_and_symlinks(char *dir, char *hostname)
{
	char	lockfile[PATH_MAX];
	struct	hostent *hp = NULL;
	struct	in_addr in;
	char	filename[PATH_MAX];
	char	*cp;
	int	error_num;
	int	addrflag = 0;
	char	**ptr;
	char    ipaddr[INET6_ADDRSTRLEN];
	const char	*ipstr;

	/*
	 * We have to use getipnodebyname, as there could be IPv6
	 * addresses corresponding to the hostnames.
	 */
	hp = getipnodebyname(hostname, AF_INET6,
			AI_ALL | AI_V4MAPPED | AI_ADDRCONFIG, &error_num);
	if (hp == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * The data service has failed to resolve the host
		 * information.
		 * @user_action
		 * If the logical host and shared address entries are
		 * specified in the /etc/inet/hosts file, check that these
		 * entries are correct. If this is not the reason, then check
		 * the health of the name server. For more error information,
		 * check the syslog messages.
		 */
		scds_syslog(LOG_WARNING,
		    "Unable to resolve %s.",
		    hostname);
		return;
	}

	/*
	 * Remove the dotted-decimal form symlinks. Get the very first IPv4
	 * and the very first IPv6 addresses. We get IPv4 addresses from the
	 * IPv4-mapped IPv6 addresses.
	 *
	 * Assuming that the hostname would be mapped to a single
	 * IPv4 and/or single IPv6 address.
	 */
	for (ptr = hp->h_addr_list; *ptr != NULL; ptr++) {

			/* find an IPv4 address, if already not found. */
		if (((addrflag & IPV4MASK) != IPV4MASK) &&
			IN6_IS_ADDR_V4MAPPED((struct in6_addr *)*ptr)) {

				IN6_V4MAPPED_TO_INADDR((struct in6_addr *)*ptr,
								&in);
				ipstr = inet_ntop(AF_INET, (void *) &in.s_addr,
					ipaddr, INET6_ADDRSTRLEN);
				if (ipstr == NULL) /* very unlikely */
					continue;
				addrflag |= IPV4MASK;

				/* Now, get rid of the IPv4 specific links. */
				(void) sprintf(filename, "ipv4.%s", ipaddr);
				(void) sprintf(lockfile, "%s/sm/%s",
					dir, filename);
				(void) unlink(lockfile);
				(void) sprintf(lockfile, "%s/sm.bak/%s",
					dir, filename);
				(void) unlink(lockfile);

			/*
			 * find an IPv6 address, if already not found.
			 * Make sure its not IPv4-mapped IPv6 address
			 */
		} else if (((addrflag & IPV6MASK) != IPV6MASK) &&
				!IN6_IS_ADDR_V4MAPPED(
					(struct in6_addr *)*ptr)) {
				ipstr = inet_ntop(AF_INET6, *ptr, ipaddr,
					INET6_ADDRSTRLEN);
				if (ipstr == NULL) /* very unlikely */
					continue;
				addrflag |= IPV6MASK;

				/* Now, get rid of the IPv6 specific links. */
				(void) sprintf(filename, "ipv6.%s", ipaddr);
				(void) sprintf(lockfile, "%s/sm/%s",
						dir, filename);
				(void) unlink(lockfile);
				(void) sprintf(lockfile, "%s/sm.bak/%s",
						dir, filename);
				(void) unlink(lockfile);
		} else
				continue;

		/* If necessary addresses are found, then we are done */
		if ((addrflag & IPV4MASK) && (addrflag & IPV6MASK))
			break;
	}

	/* Now remove the hostname based file */
	(void) strcpy(filename, hostname);
	cp = strchr(filename, '.');
	if (cp)
		*cp = 0;	/* Terminate the string at '.' */
	(void) sprintf(lockfile, "%s/sm/%s", dir, filename);
	(void) unlink(lockfile);
	(void) sprintf(lockfile, "%s/sm.bak/%s", dir, filename);
	(void) unlink(lockfile);

}

/*
 * remove_lock()
 * Remove an ipaddress from the statmon directories.
 * We are doing this because we are about to start statd
 * and we don't want it to see "local" addresses in these
 * directories. Or else, statd would land up calling itself
 * and that gets it into trouble.
 * Here is some comment copied from statd
 *
 * If `name' is an IP address symlink to a name file,
 * remove it now.  If it is the last such symlink,
 * remove the name file as well.  Regular files with
 * no symlinks to them are assumed to be legacies and
 * are removed as well.
 */

static void
remove_lock(char *statmon_path, char *hostname)
{
	char	lockdir[PATH_MAX];

	(void) sprintf(lockdir, "%s/statmon",
			statmon_path);

	/* Don't bother with the return code */
	remove_lock_and_symlinks(lockdir, hostname);

	remove_lock_and_symlinks("/var/statmon", hostname);
}

/*
 * remove_local_lock()
 * Remove the local hostname from the statmon lock direcories.
 */
static void
remove_local_lock(char *statmon_path)
{
	char	myname[MAXHOSTNAMELEN];

	(void) gethostname(myname, sizeof (myname));
	remove_lock(statmon_path, myname);
}

/*
 * remove_all_locks()
 * Removes are appropiate locks from statmon
 * directories. Uses remove_local_lock() and
 * remove_lock() utilities.
 */
static void
remove_all_locks(scds_handle_t scds_handle)
{
	char	*statmon_dir;
	int	rs_idx, ip_idx, rc;
	scds_net_resource_list_t *snrlp = NULL;

	statmon_dir = get_statmon_dir(scds_handle, B_FALSE);
	if (statmon_dir == NULL) {
		return;
	}

	remove_local_lock(statmon_dir);

	rc = scds_get_rs_hostnames(scds_handle, &snrlp);
	if (rc != SCHA_ERR_NOERR || snrlp == NULL ||
		snrlp->num_netresources == 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * No network resources were found for the resource.
		 * @user_action
		 * Declare network resources used by the resource explicitly
		 * using the property Network_resources_used. For the resource
		 * name and resource group name, check the syslog tag.
		 */
		scds_syslog(LOG_ERR,
		    "No network resources found for resource.");
		goto finished_remove_all_locks;
	}

	for (rs_idx = 0; rs_idx < snrlp->num_netresources; rs_idx++) {
		for (ip_idx = 0;
			ip_idx < snrlp->netresources[rs_idx].num_hostnames;
			ip_idx++) {
			/* Remove this ipaddress from the statmon dirs */
			remove_lock(statmon_dir,
				snrlp->netresources[rs_idx].hostnames[ip_idx]);
		}
	}

finished_remove_all_locks:

	if (snrlp)
		scds_free_net_list(snrlp);
}

/*
 * nfs_check_resource()
 * Check to see if a given resource is already in the
 * state file. It runs the nfs_remove_resource command
 * with the opcode "check". Returns 0 if there, non-zero
 * if not.
 */
static int
nfs_check_resource(scds_handle_t scds_handle)
{
	int	rc;
	char	cmd[2048];

	(void) sprintf(cmd, "%s/nfs_remove_resource "
		"%s %s check > %s/debug.nur 2>&1",
		scds_get_rt_rt_basedir(scds_handle),
		(char *)scds_get_resource_group_name(scds_handle),
		(char *)scds_get_resource_name(scds_handle), HANFS_STATE_DIR);

	rc = run_cmd(LOG_DEBUG, cmd);
	return (rc);
}

/*
 * nfs_register_resource()
 * Adds the resource defn to the HA-NFS state file.
 * The format of the file is
 * RG-name R-name statmon-dir-path ipaddr1 ipaddr2 ipaddr3...
 *
 * The contents of this file are then used by the
 * nfs_start_daemons script to compute the list of -a and
 * -p options to be passed to statd.
 * The stuff is first created in a tmp file and then
 * concatenated into the state file.
 */
int
nfs_register_resource(scds_handle_t scds_handle)
{
	char	*statmon_dir = NULL, *rsname = NULL, *rgname = NULL;
	scds_net_resource_list_t *snrlp = NULL;
	int	rs_idx, ip_idx, rc = 0;
	struct	hostent *hp = NULL;
	struct	in_addr in;
	char	ipaddr[INET6_ADDRSTRLEN];
	const 	char *ipstr;
	char	tmp_file[PATH_MAX];
	char	tmpbuf[2048];
	int	error_num;
	uint_t	addrflag = 0;
	FILE	*fp = NULL;
	char	**ptr;

	/*
	 * First check to see that the resource is not already
	 * in the state file. If so, log an error and fail the
	 * registration.
	 */
	if (nfs_check_resource(scds_handle) == 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * While attempting to restart the resource, error has
		 * occurred. The resource is already online.
		 * @user_action
		 * This is an internal error. Save the /var/adm/messages file
		 * from all the nodes. Contact your authorized Sun service
		 * provider.
		 */
		scds_syslog(LOG_ERR,
		    "Resource is already online.");
		return (-1);
	}

	/* Get resource and resource group names */
	rsname = (char *)scds_get_resource_name(scds_handle);
	rgname = (char *)scds_get_resource_group_name(scds_handle);

	(void) sprintf(tmp_file, "%s/nfs_register_r_%s",
		HANFS_STATE_DIR, rsname);
	fp = fopen(tmp_file, "w");
	if (fp == NULL) {
		scds_syslog(LOG_ERR,
		    "Unable to open %s: %s.",
		    tmp_file, strerror(errno));
		return (-1);
	}
	statmon_dir = get_statmon_dir(scds_handle, B_FALSE);
	if (statmon_dir == NULL) {
		rc = -1;
		goto reg_fail;
	}

	(void) fprintf(fp, "%s %s %s ", rgname, rsname, statmon_dir);

	rc = scds_get_rs_hostnames(scds_handle, &snrlp);
	if (rc != SCHA_ERR_NOERR || snrlp == NULL ||
		snrlp->num_netresources == 0) {
		scds_syslog(LOG_ERR,
		    "No network resources found for resource.");
		rc = -1;
		goto reg_fail;
	}

	for (rs_idx = 0; rs_idx < snrlp->num_netresources; rs_idx++) {

		addrflag = 0;
		for (ip_idx = 0;
			ip_idx < snrlp->netresources[rs_idx].num_hostnames;
			ip_idx++) {

			/* get all the ip node addresses */
			hp = getipnodebyname(snrlp->
				netresources[rs_idx].hostnames[ip_idx],
				AF_INET6, AI_ALL | AI_V4MAPPED |
				AI_ADDRCONFIG, &error_num);
			if (hp == NULL) {
				scds_syslog(LOG_WARNING,
					"Unable to resolve %s.",
					snrlp->netresources[rs_idx].
					hostnames[ip_idx]);
				rc = -1;
				goto reg_fail;
			}

			/*
			 * Try best to find atmost one addr each of type v4/v6.
			 * Remember,we don't have to find address of both types.
			 * Just one address of either IPv4 or IPv6 is also fine.
			 *
			 * The reason being LH/SA resources apply the same
			 * logic in plumbing the appropriate addresses. We
			 * anticipate that our NFS clients will be accessed
			 * through the LH/SA only.
			 */
			for (ptr = hp->h_addr_list; *ptr != NULL; ptr++) {

				/* Find an IPv4 address, if not found */
				if (((addrflag & IPV4MASK) != IPV4MASK) &&
					IN6_IS_ADDR_V4MAPPED(
						(struct in6_addr *)*ptr)) {

					/* convert to IPv4 address */
					IN6_V4MAPPED_TO_INADDR(
						(struct in6_addr *)*ptr, &in);
					ipstr = inet_ntop(AF_INET,
						(void *) &in.s_addr,
						ipaddr, INET6_ADDRSTRLEN);
					if (ipstr == NULL) /* very unlikely */
						continue;

					/* Write this into register file. */
					(void) fprintf(fp, "%s ", ipaddr);
					addrflag |= IPV4MASK;

				/* Find an IPv6 address, if not found. */
				} else if (((addrflag & IPV6MASK)
					!= IPV6MASK) && !IN6_IS_ADDR_V4MAPPED(
						(struct in6_addr *)*ptr)) {

					ipstr = inet_ntop(AF_INET6, *ptr,
						ipaddr, INET6_ADDRSTRLEN);
					if (ipstr == NULL) /* very unlikely */
						continue;

					/* Write this into register file. */
					(void) fprintf(fp, "%s ", ipaddr);
					addrflag |= IPV6MASK;
				} else
					continue;

				/*
				 * If we find an address of each type,
				 * then exit the loop
				 */
				if ((addrflag & IPV4MASK) &&
					(addrflag & IPV6MASK))
					break;
			}
		}
	}

	/* Don't forget the newline at the end */
	(void) fwrite("\n", sizeof (char), 1, fp);
	/*
	 * Now flush the stream, fclose() does do that, but then
	 * it closes the stream as well, so we can't check for
	 * error at that time.
	 */
	(void) fflush(fp);
	if (ferror(fp) != 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		scds_syslog(LOG_ERR,
		    "Unable to write to file %s: %s.",
		    tmp_file, strerror(errno));
		rc = -1;
		goto reg_fail;
	}

	(void) fclose(fp);
	fp = NULL;
	(void) sprintf(tmpbuf, "/usr/bin/cat %s >> %s/%s", tmp_file,
		HANFS_STATE_DIR, HANFS_STATE_FILE);
	rc = run_cmd(LOG_ERR, tmpbuf);

reg_fail:
	if (fp)
		(void) fclose(fp);
	if (snrlp)
		scds_free_net_list(snrlp);

	return (rc);
}

/*
 * nfs_unregister_resource()
 * Removes the resource and associated stuff from the
 * NFS state file.
 * The state file is parsed and copied into a tmp_file,
 * omitting the resource being unregistered, then is
 * renamed in a single operation.
 */
int
nfs_unregister_resource(scds_handle_t scds_handle)
{
	int	rc;
	char	cmd[2048];
	char	internal_err_str[INT_ERR_MSG_SIZE];
	char	*rsname = (char *)scds_get_resource_name(scds_handle);

	(void) sprintf(cmd, "%s/nfs_remove_resource "
		"%s %s remove > %s/debug.nur 2>&1",
		scds_get_rt_rt_basedir(scds_handle),
		(char *)scds_get_resource_group_name(scds_handle),
		rsname, HANFS_STATE_DIR);

	rc = run_system(LOG_WARNING, cmd);
	if (rc != 0) {
		(void) snprintf(internal_err_str, sizeof (internal_err_str),
		    "failed to remove resource <%s> from state file.",
		    rsname);
		scds_syslog(LOG_ERR,
		    "INTERNAL ERROR: %s", internal_err_str);
		return (rc);
	}

	return (0);
}

/*
 * start_nfsd_mountd_nowait()
 * Start nfsd and mountd without
 * waiting for them to come up.
 * Called from BOOT/INIT methods only.
 */

int
start_nfsd_mountd_nowait(scds_handle_t scds_handle)
{
	int	rc;
	char	cmd[2048];

	/* First start mountd */
	(void) sprintf(cmd, "%s/nfs_start_daemons"
		" mountd > %s/debug.mountd 2>&1",
		scds_get_rt_rt_basedir(scds_handle),
		HANFS_STATE_DIR);

	rc = startproc(NFS_MOUNTD_PATH, cmd);
	if (rc)
		return (rc);

	/*
	 * Now start nfsd using the nfs_start_daemons
	 * script, which pulls in customizations from
	 * /etc/init.d/nfs.server
	 */
	(void) sprintf(cmd, "%s/nfs_start_daemons"
		" nfsd > %s/debug.nfsd 2>&1",
		scds_get_rt_rt_basedir(scds_handle),
		HANFS_STATE_DIR);

	rc = startproc(NFS_NFSD_PATH, cmd);
	return (rc);
}

/*
 * start_nfsd_mountd()
 * Start nfsd and mountd. For nfsd, take care to pull in
 * any customizations from /etc/init.d/nfs.server script.
 * Uses the start_nfs_daemon() utility to make sure
 * the daemon is UP before returning.
 */
int
start_nfsd_mountd(scds_handle_t scds_handle, time_t timeout)
{
	int	rc;

	/* First start mountd */
	rc = start_nfs_daemon(scds_handle, NFS_MOUNTD_PATH,
		RPC_PROGNUM_MOUNTD, RPC_VERSION_MOUNTD,
		timeout);
	if (rc)
		return (rc);

	rc = start_nfs_daemon(scds_handle, NFS_NFSD_PATH,
			RPC_PROGNUM_NFSD, RPC_VERSION_NFSD,
			timeout);
	return (rc);
}

/*
 * This routine returns non-zero if nfsd installed on the
 * system has a warm-start mode. This is true for nfsd on
 * Solaris 10 and later. A check for OS version is sufficient
 * for this purpose.
 *
 * Returns 1 if nfsd has warm-start functionality, 0 otherwise.
 * If there is a problem in determining the OS version,
 * we assume that nfsd does *not* understand USR1 and
 * this function returns 0.
 */
int
nfsd_has_warmstart()
{
	static int warmstart = -1;
	struct utsname u;

	/* return cached value, if valid */
	if (warmstart != -1)
		return (warmstart);

	/* get OS release information */
	if (uname(&u) < 0) {
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
		    "Failed to determine OS version");
		return (0);
	}

	scds_syslog_debug(2, "sysname: %s, nodename: %s, "
	    "release: %s, version: %s, machine: %s", u.sysname,
	    u.nodename, u.release, u.version, u.machine);

	/*
	 * If OS is SunOS and version is 5.10 or more, nfsd
	 * has a warm start mode
	 */
	if (strcmp(u.sysname, "SunOS") == 0) {
		long major = 0, minor = 0;
		char *e = NULL;

		major = strtol(u.release, &e, 10);

		/* e points to a dot now, move to the char after that */
		if (*e == '.')
			e++;

		if (*e)
			minor = strtol(e, NULL, 10);

		if ((major == 5 && minor >= 10) || (major > 5)) {
			warmstart = 1;
		} else {
			warmstart = 0;
		}
	} else {
		/* Not SunOS */
		warmstart = 0;
	}

	return (warmstart);
}

/*
 * stop_nfsd_mountd()
 * Stop nfsd and mountd.
 * Timeout parameter is -1 when
 * called from methods, is
 * Stop_timeout when called
 * from probe.
 */
int
stop_nfsd_mountd(time_t timeout)
{
	int		rc, rc2;

	/* First kill mountd */
	rc = stop_nfs_daemon(NFS_MOUNTD_PATH, RPC_PROGNUM_MOUNTD,
		RPC_VERSION_MOUNTD, timeout);

	/* Now kill nfsd */
	rc2 = stop_nfs_daemon(NFS_NFSD_PATH, RPC_PROGNUM_NFSD,
		RPC_VERSION_NFSD, timeout);

	/* return value is non-zero if either of them fail */
	if (rc)
		rc2 = rc;
	return (rc2);
}

/*
 * stop_lockd_statd()
 * Stop lockd and statd. Returns success even if these are
 * already stopped. Would return non-zero only if
 * something went horribly wrong.
 * timeout is -1 when called from
 * methods, Stop_timeout when called
 * from the probe.
 */
int
stop_lockd_statd(time_t timeout)
{
	int		rc1, rc2;

	/* lockd needs to be stopped first */
	rc1 = stop_nfs_daemon(NFS_LOCKD_PATH, RPC_PROGNUM_LOCKD,
		RPC_VERSION_LOCKD, timeout);

	/* stop statd too */
	rc2 = stop_nfs_daemon(NFS_STATD_PATH, RPC_PROGNUM_STATD,
		RPC_VERSION_STATD, timeout);

	return ((rc1 != 0)?rc1:rc2);
}

/*
 * start_statd_lockd()
 * Starts statd with the appropiate -a and -p options
 * The nfs_start_daemons script is used for that. lockd is
 * started with a grace period of 90 seconds.
 * If lockd fails to start, we sleep for 2 sec. then try again.
 * Returns non-zero only if something goes horribly wrong.
 * If the state files are not found, statd is started without
 * them.
 * Before starting statd we remove all lock files related to
 * ipaddresses contained in the RG.
 */
int
start_statd_lockd(scds_handle_t scds_handle, time_t timeout)
{
	int	rc;

	/* Remove all appropiate lock entries */
	remove_all_locks(scds_handle);
	/*
	 * First start statd.
	 */

	rc = start_nfs_daemon(scds_handle, NFS_STATD_PATH,
		RPC_PROGNUM_STATD, RPC_VERSION_STATD,
		timeout);

	if (rc) {
		/* If not able to start statd, don't start lockd either */
		return (rc);
	}

	/*
	 * Now start lockd. The nfs_start_daemons script is
	 * used to pull in any customization from /etc/init.d/nfs.client
	 *
	 * We have seen a case where notification threads have been
	 * lingering around, and prevented lm_status from going to LM_DOWN
	 * This prevents a new lockd from starting. Therefore we keep
	 * looping until lockd can be started.
	 *
	 * The routine start_nfs_daemon() does the looping.
	 */


	rc = start_nfs_daemon(scds_handle, NFS_LOCKD_PATH,
		RPC_PROGNUM_LOCKD, RPC_VERSION_LOCKD,
		timeout);
	return (rc);

}

/*
 * is_nfs_tcp_only()
 * Reads the NFSD config.
 * Returns 1 if NFSD is configured to use TCP only,
 * otherwise returns 0.
 *
 * For S11 onwards, we use "sharectl" to get NFS parameters.
 * For S10 and below, we read off from /etc/default/nfs file.
 */
boolean_t
is_nfs_tcp_only(void)
{
	boolean_t rv = B_FALSE;
	char *defval;

	/* For SOL_VERSION > __s10, get config data from "sharectl" */
#if (SOL_VERSION > __s10)

	char *sharectl = "/usr/sbin/sharectl get nfs";
	char aline[MAX_LINE];
	char *property, *value;
	char *separator = "=";
	FILE *pptr;

	if ((pptr = popen(sharectl, "r")) != NULL) {
		while (fgets(aline, MAX_LINE, pptr) != NULL) {
			/*
			 * Tokenize the line into property and its value.
			 *
			 * "sharectl" reports error on its stdout.
			 * Therefore, if we dont see line of the form
			 * "property=value", it means the stdout contained the
			 * error message.
			 */
			if (((property = strtok(aline, separator)) == NULL)) {
				/*
				 * SCMSGS
				 * @explanation
				 * Command "sharectl get nfs" returned error.
				 * @user_action
				 * Examine syslog messages to determine the
				 * cause of the failure. Take corrective action
				 * based on any related messages. If the problem
				 * persists, report it to your Sun support
				 * representative for further assistance.
				 */
				scds_syslog(LOG_ERR,
					"sharectl error: %s\n", aline);
				break;
			};

			value = strtok(NULL, separator);
			if ((strcmp(property, "protocol") == 0) &&
				(strncmp(value, "tcp", 3) == 0)) {
					rv = B_TRUE;
					break;
			};

			if ((strcmp(property, "server_versmin") == 0) &&
				(atoi(value) == 4)) {
					rv = B_TRUE;
					break;
			};

			/*
			 * "sharectl" doesnt report NFSD_DEVICE, even when set.
			 * Consider adding this check once "sharectl" is fixed.
			 */
		} /* while */
		pclose(pptr);
		return (rv);
	} else {
		/* Log a warning and fall through, using pre-s11 approach. */
		/*
		 * SCMSGS
		 * @explanation
		 * popen failed while getting NFS parameters using "sharectl".
		 * @user_action
		 * This is a warning message. An attempt will be made
		 * to read the properties directly from the file.
		 * Examine syslog messages to determine the cause of failure.
		 * Take corrective action based on any related messages. If
		 * the problem persists, report it to your Sun support
		 * representative for further assistance.
		 */
		scds_syslog(LOG_NOTICE, "popen failed on sharectl: %s."
			"Will attempt to read config from %s file.",
			strerror(errno), NFSADMIN);
	}; /* if popen */
#endif  /* SOL_VERSION > __s10 */


	/* For s10 and below, we read config from /etc/default/nfs file */
	if ((defopen(NFSADMIN)) == 0) {
		if ((defval = defread("NFSD_PROTOCOL=")) != NULL) {
			if (strncmp(defval, "tcp", 3) == 0) {
				/* close NFSADMIN file */
				defopen(NULL);
				return (B_TRUE);
			};
		};
		if ((defval = defread("NFSD_DEVICE=")) != NULL) {
			if (strstr(defval, "tcp")) {
				/* close NFSADMIN file */
				defopen(NULL);
				return (B_TRUE);
			};
		};

		/* NFS_SERVER_VERSMIN is available only from s10 onwards */
#if (SOL_VERSION >= __s10)
		if ((defval = defread("NFS_SERVER_VERSMIN=")) != NULL) {
			if (strtol(defval, (char **)NULL, 10) == 4) {
				/* close NFSADMIN file */
				defopen(NULL);
				return (B_TRUE);
			};
		};
#endif  /* SOL_VERSION >= __s10 */

		/* close NFSADMIN file */
		defopen(NULL);
	} else {
		scds_syslog(LOG_ERR, "Unable to open %s: %s.",
			NFSADMIN, strerror(errno));
	};

	return (B_FALSE);
}

/*
 * start_nfs_daemon()
 * Launch the specified NFS daemon
 * then make sure it starts up
 * and registers with both UDP and TCP
 * transports.
 *
 * First argument is the program name
 * i.e. /usr/lib/nfs/nfsd etc.
 * Second argument (cmd) is the whole command
 * line (including arguments).
 *
 * If the daemon dies (pid vanishes)
 * restart the daemon. If the daemon
 * registers with TCP but not with UDP
 * then restart the daemon, by first
 * killing it.
 *
 * Note that if the daemon registers
 * with UDP first, we return from this
 * function, and do not check for TCP
 * registration. It is possible that
 * the daemon would actually not be able
 * to bind to TCP (anything is possible
 * with these NFS daemons). NFS daemons
 * themselves consider success if they
 * are able to bind to at least one
 * transport.
 *
 * If the daemon doesn't die (pid remains
 * valid) but it never registers with
 * TCP. We continue waiting here, and would
 * eventually be timed out by RGM.
 *
 * The timeout parameter is passed in as -1
 * from methods, and a non-zero value from
 * the probe. If the timeout parameter is set
 * to a nonzero value, we keep checking if
 * timeout has expired, and if so we return
 * early with an error.
 */

int
start_nfs_daemon(scds_handle_t scds_handle, char *progname,
	rpcprog_t prognum, rpcvers_t progversion, time_t timeout)
{
	CLIENT  *rpcclnt = NULL;
	struct netconfig *nconf_udp = NULL;
	struct netconfig *nconf_tcp = NULL;
	char *hostname = "localhost";
	boolean_t	bound_to_udp = B_FALSE;
	boolean_t	bound_to_tcp = B_FALSE;
	int	rc = -1;
	time_t  start_time = time(NULL);
	time_t	time_remaining = -1;
	pid_t		pid;
	char	cmd[2048];

	nconf_udp = getnetconfigent("udp");
	nconf_tcp = getnetconfigent("tcp");
	if (nconf_udp == NULL || nconf_tcp == NULL) {
		scds_syslog(LOG_ERR, "Failed to retrieve network "
			"configuration: %s.",
			nc_sperror());
		return (-1);
	}

	(void) sprintf(cmd, "%s/nfs_start_daemons %s "
		" > %s/debug_%s.out 2>&1",
		scds_get_rt_rt_basedir(scds_handle),
		basename(progname),
		HANFS_STATE_DIR,
		basename(progname));

retry:
	/*
	 * if configured for TCP only, we dont need to check if
	 * its registered with UDP. So set bound_to_udp to B_TRUE.
	 */
	bound_to_udp = is_nfs_tcp_only();
	bound_to_tcp = B_FALSE;

	if (timeout > 0) {
		time_remaining = timeout - (time(NULL) - start_time);
		if (time_remaining <= 0) {
			/*
			 * SCMSGS
			 * @explanation
			 * HA-NFS implementation failed to start the specified
			 * NFS daemon.
			 * @user_action
			 * HA-NFS would attempt to remedy this situation by
			 * attempting to restart the daemon again, or by
			 * failing over if necessary. If the system is under
			 * heavy load, increase Start_timeout for the HA-NFS
			 * resource.
			 */
			scds_syslog(LOG_ERR,
				"Failed to start NFS daemon %s.",
				progname);
			rc = -1;
			goto finished;
		}
	}

	/*
	 * SCMSGS
	 * @explanation
	 * The specified NFS daemon is being started by the HA-NFS
	 * implementation.
	 * @user_action
	 * This is an informational message. No action is needed.
	 */
	scds_syslog(LOG_NOTICE,
		"Starting NFS daemon %s.",
		progname);
	/* Start the daemon */
	rc = startproc(progname, cmd);
	if (rc != 0) {
		(void) usleep(100000);
		goto retry;
	}
	rc = checkproc(progname, &pid);
	if (rc == NFS_ERR_PID_NOT_EXIST || rc == NFS_ERR_UNKNOWN) {
		/*
		 * SCMSGS
		 * @explanation
		 * While attempting to start the specified NFS daemon, the
		 * daemon did not start.
		 * @user_action
		 * This is an informational message. No action is needed.
		 * HA-NFS would attempt to correct the problem by restarting
		 * the daemon again. HA-NFS imposes a delay of 100
		 * milliseconds between restart attempts.
		 */
		scds_syslog(LOG_ERR,
			"NFS daemon %s did not start. Will retry in 100"
			" milliseconds.", progname);
		(void) usleep(100000);
		goto retry;
	}

check_rpc_reg:

	if (timeout > 0) {
		time_remaining = timeout - (time(NULL) - start_time);
		if (time_remaining <= 0) {
			scds_syslog(LOG_ERR,
				"Failed to start NFS daemon %s.",
				progname);
			rc = -1;
			goto finished;
		}
	}

	/* Start the daemon */
	/* First make sure the process is still up */
	if (kill(pid, 0) != 0) {
		if (errno == ESRCH) {
			/* Process died. Try again */
			/*
			 * SCMSGS
			 * @explanation
			 * While attempting to start the specified NFS daemon,
			 * the daemon started up, however it exited before it
			 * could complete its network configuration.
			 * @user_action
			 * This is an informational message. No action is
			 * needed. HA-NFS would attempt to correct the problem
			 * by restarting the daemon again. HA-NFS imposes a
			 * delay of milliseconds between restart attempts.
			 */
			scds_syslog(LOG_ERR,
				"NFS daemon %s died. "
				"Will restart in 100 milliseconds.",
				progname);
			(void) usleep(100000);
			goto retry;
		}
		/* Something else happend, can't deal with it */
		/*
		 * SCMSGS
		 * @explanation
		 * HA-NFS fault monitor attempted to check the status of the
		 * specified process but failed. The specific cause of the
		 * error is logged.
		 * @user_action
		 * No action. HA-NFS fault monitor would ignore this error and
		 * would attempt this operation at a later time. If this error
		 * persists, check to see if the system is lacking the
		 * required resources (memory and swap) and add or free
		 * resources if required. Reboot the node if the error
		 * persists.
		 */
		scds_syslog(LOG_ERR,
			"Attempting to check for existence "
			"of %s pid %d resulted in error: %s.",
			progname, pid, strerror(errno));

		rc = -1;
		goto finished;
	}

	/* Now check if registered with UDP */
	if (bound_to_udp == B_FALSE) {
		scds_syslog_debug(2, "Checking for service to be "
			"registered with RPC over UDP.");
		rpcclnt = clnt_tp_create(hostname, prognum, progversion,
			nconf_udp);
		if (rpcclnt != NULL) {
			bound_to_udp = B_TRUE;
			clnt_destroy(rpcclnt);
			rpcclnt = NULL;
		}
		/* OK, not registered with UDP, check for TCP */
	}

	/* Now check if registered with TCP */
	if (bound_to_tcp == B_FALSE) {
		scds_syslog_debug(2, "Checking for service to be "
			"registered with RPC over TCP.");
		rpcclnt = clnt_tp_create(hostname, prognum, progversion,
			nconf_tcp);
		if (rpcclnt != NULL) {
			bound_to_tcp = B_TRUE;
			clnt_destroy(rpcclnt);
			rpcclnt = NULL;
		}
	}

	/* If registered with TCP. Make sure also registered with UDP */
	if ((bound_to_udp == B_FALSE) && (bound_to_tcp == B_TRUE)) {
		/* Problem, we need to kill and restart the daemon */
		/*
		 * SCMSGS
		 * @explanation
		 * While attempting to start the specified NFS daemon, the
		 * daemon started up. However it registered with TCP transport
		 * before it registered with UDP transport. This indicates
		 * that the daemon was unable to register with UDP transport.
		 * @user_action
		 * This is an informational message, no user action is needed.
		 * Make sure that the order of entries in /etc/netconfig is
		 * not changed on cluster nodes where HA-NFS is running.
		 */
		scds_syslog(LOG_ERR,
			"NFS daemon %s has registered with TCP "
			"transport but not with UDP transport. "
			"Will restart the daemon.",
			progname);
		rc = stop_nfs_daemon(progname, prognum,
			progversion, time_remaining);
		if (rc != 0) {
			/* Something bad happened, can't deal with it */
			rc = -1;
			goto finished;
		}
		/* OK. We killed the daemon, start from scratch again */
		(void) usleep(100000);
		goto retry;
	}

	/* We don't wait for TCP */
	if (bound_to_udp == B_FALSE) {
		scds_syslog_debug(2, "Waiting for service to be "
			"registered with RPC over UDP.");
		(void) usleep(100000);
		goto check_rpc_reg;
	}

	/* All OK */
	/*
	 * SCMSGS
	 * @explanation
	 * The specified NFS daemon has been started by the HA-NFS
	 * implementation.
	 * @user_action
	 * This is an informational message. No action is needed.
	 */
	scds_syslog(LOG_NOTICE,
		"Started NFS daemon %s.",
		progname);
	rc = 0;
finished:

	if (nconf_udp)
		freenetconfigent(nconf_udp);
	if (nconf_tcp)
		freenetconfigent(nconf_tcp);
	if (rpcclnt)
		clnt_destroy(rpcclnt);
	return (rc);
}

/*
 * create_stop_file()
 * Creates a file named <r_name> in /var/run/.hanfs/.stop/
 * This function is called by the stop method to
 * inform the fault monitors running on the node
 * that a resource is currently in between STOP and
 * POSTNET_STOP. So that nfsd/mountd are NOT restarted
 * by the monitor.
 * Must be called under system lock.
 * Returns
 * 0 for success.
 * -1 for failures.
 */

#define	STOP_SUBDIR	".stop"

int
create_stop_file(char *r_name)
{
	char	tmp_file[PATH_MAX];
	int		fd;

	(void) sprintf(tmp_file, "%s/%s", HANFS_STATE_DIR,
		STOP_SUBDIR);
	if (mkdirp(tmp_file, 0666) != 0) {
		if (errno != EEXIST) {
			scds_syslog(LOG_ERR, "Unable to create <%s>: %s.",
				tmp_file, strerror(errno));
			return (-1);
		}
	}
	(void) sprintf(tmp_file, "%s/%s/%s", HANFS_STATE_DIR,
		STOP_SUBDIR, r_name);
	fd = creat(tmp_file, 0600);
	if (fd < 0) {
		scds_syslog(LOG_ERR, "Unable to create <%s>: %s.",
			tmp_file, strerror(errno));
		return (-1);
	}
	(void) close(fd);
	return (0);
}

/*
 * remove_stop_file()
 * Removes the stop file created by create_stop_file()
 * Called by postnet_stop method.
 * Return code same as unlink().
 */
int
remove_stop_file(char *r_name)
{
	char	tmp_file[PATH_MAX];

	(void) sprintf(tmp_file, "%s/%s/%s", HANFS_STATE_DIR,
		STOP_SUBDIR, r_name);
	return (unlink(tmp_file));
}

/*
 * check_stop_file()
 * Checks to see if there are any nfs resources
 * that are between stop and postnet_stop on this
 * node. This is indicated by the existence of
 * a file in /var/run/.hanfs dir.
 *
 * Called by the fault monitors to check if
 * the restarting of nfsd/mountd needs to be
 * suppressed. If a stop file is found, we also look
 * at the state of the resource that created the
 * file.
 *
 * Returns
 * 0: There is no nfs resource that is between STOP and
 *    POSTNET_STOP. Either there was no stop file or the
 *    status of the resource(s) that created the file was
 *    not STOPPING. The caller is free to restart nfsd/mountd.
 *    [PS: All this is assumed to be happening under the
 *    per-node system lock.]
 * 1: There is atleast one nfs resource that is between STOP
 *    and POSTNET_STOP. A stop file was found and the state
 *    of that resource was STOPPING. If the file exists and
 *    state cannot be determined, we assume it is STOPPING.
 *    Caller should not restart nfsd/mountd. They will be started
 *    by the postnet_stop method of the resource which is stopping.
 * -1: Errors.
 */

#include <dirent.h>
#include <time.h>
#include <sys/stat.h>

int
check_stop_file(void)
{
	DIR		*d;
	struct	dirent	*dir;
	char	tmp_file[PATH_MAX];
	/* Assume OK, unless we see otherwise */
	int		rc = 0, scha_rc;
	scha_resource_t rh = NULL;
	scha_rsstate_t state = NULL;

	/*
	 * The directory /var/run/.hanfs/.stop contains the
	 * list of resources which are in between STOP
	 * and POSTNET_STOP.
	 */
	(void) sprintf(tmp_file, "%s/%s",
		HANFS_STATE_DIR, STOP_SUBDIR);

	if ((d = opendir(tmp_file)) == NULL) {
		if (errno == ENOENT)
			/* .stop dir is not created yet; */
			return (0);

		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		scds_syslog(LOG_ERR, "Failed to open <%s>: %s.",
			tmp_file, strerror(errno));
		return (-1);
	}
	while ((dir = readdir(d)) != NULL) {
		/* Skip subdirs . and .. */
		if (strcmp(dir->d_name, ".") == 0 ||
		    strcmp(dir->d_name, "..") == 0)
			continue;

		(void) sprintf(tmp_file, "%s/%s/%s",
			HANFS_STATE_DIR, STOP_SUBDIR, dir->d_name);

		/* stop file exists, but is the resource really stopping? */
retry_rs:
		scha_rc = scha_resource_open(dir->d_name, NULL, &rh);
		if (scha_rc == SCHA_ERR_RSRC) {
			/*
			 * This doesn't look like a valid resource name.
			 * The file is not a stop file, ignore it.
			 */
			scds_syslog_debug(2, "File %s does not name a valid "
			    "resource. Ignoring this file.", tmp_file);
			continue;
		} else if (scha_rc != 0) {
			/*
			 * Failed to open the resource handle.
			 * Log an error but assume that the stop file is
			 * valid and that the resource is stopping.
			 */
			/*
			 * SCMSGS
			 * @explanation
			 * There was a failure while retrieving to retrieve a
			 * handle of the named resource. The reason that the
			 * error occurred is also provided in the message.
			 * @user_action
			 * The operation will be retried after some time. But
			 * there might be a delay in failing over some HA-NFS
			 * resources. If this message is logged repeatedly,
			 * save a copy of the syslog and contact your
			 * authorized Sun service provider.
			 */
			scds_syslog(LOG_ERR, "Failed to retrieve the "
			    "resource handle of resource %s: %s.",
			    dir->d_name, scds_error_string(scha_rc));
			rc = 1;
			break;
		}

		scha_rc = scha_resource_get(rh, SCHA_RESOURCE_STATE, &state);
		(void) scha_resource_close(rh);
		if (scha_rc == SCHA_ERR_SEQID) {
			rh = NULL;
			(void) sleep(1);
			goto retry_rs;
		} else if (scha_rc != 0) {
			/*
			 * Failed to determine the state of the resource.
			 * Log an error but assume that the stop file is
			 * valid and that the resource is stopping.
			 */
			/*
			 * SCMSGS
			 * @explanation
			 * There was a failure while retrieving a property of
			 * a resource. The names of the resource and the
			 * property are both provided in the error message.
			 * The reason that the error occurred is also provided
			 * in the message.
			 * @user_action
			 * The operation will be retried after some time. But
			 * there might be a delay in failing over some HA-NFS
			 * resources. If this message is logged repeatedly,
			 * save a copy of the syslog and contact your
			 * authorized Sun service provider.
			 */
			scds_syslog(LOG_ERR, "Failed to retrieve the "
			    "resource property %s of resource %s: %s.",
			    SCHA_RESOURCE_STATE, dir->d_name,
			    scds_error_string(scha_rc));
			rc = 1;
			break;
		}

		if (state == SCHA_RSSTATE_STOPPING) {
			/* OK. Found the condition we were looking for */
			scds_syslog_debug(2, "Resource %s is stopping on "
			    "this node.", dir->d_name);
			rc = 1;
			break;
		} else {
			/*
			 * state is something other than STOPPING.
			 * This can mean:
			 * a) stop file was present when we called opendir()
			 *    and readdir() but by the time we determined the
			 *    state, the resource has gone OFFLINE or is in
			 *    STOP_FAILED state.
			 * b) stop file is stale
			 *
			 * In either case it is best to leave rc as 0 and
			 * get rid of the stop file. For case (a), the file
			 * is already gone if postnet_stop ran successfully
			 */
			scds_syslog_debug(2, "State of resource %s is %d on "
			    "this node. File %s is stale and will now be "
			    "removed.", dir->d_name, state, tmp_file);
			(void) remove_stop_file(dir->d_name);
		}
	}

	(void) closedir(d);
	return (rc);
}

/*
 * getset_file_mtime() -
 *	If op is NET_OP_GET, set the mtime of the daemon_statefile file
 *	to the current time and return the result of utime().  If op is
 *	NFS_OP_SET, use stat() to query the mtime of daemon_statefile
 *	file and return mtime for success and -1 for errors.
 *	Note: Need to hold the system lock when calling this function.
 */
int
getset_file_mtime(int op)
{
	char	filename[PATH_MAX];
	int	fd;
	char	internal_err_str[INT_ERR_MSG_SIZE];
	struct stat	r_stat;

	(void) sprintf(filename, "%s/%s",
	    HANFS_STATE_DIR, HANFS_PROC_STATEFILE);

again:
	if (stat(filename, &r_stat) != 0) {
		if (errno == ENOENT) {
			/* the file not exist; may hasn't been created yet */
			if ((fd = creat(filename, 0600)) < 0) {
				/* Failed to create file */
				scds_syslog(LOG_ERR,
				    "Unable to create <%s>: %s.",
				    filename, strerror(errno));
				return (-1);
			}
			(void) close(fd);
			goto again;
		} else {
			(void) snprintf(internal_err_str,
			    sizeof (internal_err_str),
			    "stat of file %s failed.", filename);
			scds_syslog(LOG_ERR,
			    "INTERNAL ERROR: %s", internal_err_str);
			return (-1);
		}
	}

	if (op == NFS_OP_SET) {
		/* touch the file */
		return (utime(filename, (struct utimbuf *)NULL));
	}

	/* Return the mtime of the file */
	return ((int)r_stat.st_mtime);
}

/*
 * create_giveover_file()
 * Creates a file which acts as a lock when GIVEOVER of resource
 * groups is being attempted. nfs_prenet_start checks for the
 * existence of this file. nfs_postnet_stop cleans up this file.
 * Returns 0 for success, non-zero for error.
 */

int
create_giveover_file()
{
	char	fname[MAXPATHLEN];
	int	fd;

	(void) sprintf(fname, "%s", HANFS_STATE_DIR);

	if (mkdirp(fname, (mode_t)0755) != 0) {
		/*
		 * Suppress lint: call to ___errno() not made in the
		 * presence of a prototype.
		 */
		if (errno != EEXIST) { 	/*lint !e746 */
			scds_syslog(LOG_ERR,
			    "Unable to create directory %s: %s.",
			    fname, strerror(errno));
			return (-1);
		}
	}

	/* Now create the file in that subdir */
	(void) strcat(fname, "/");
	(void) strcat(fname, GIVEOVER_FILE);
	if ((fd = creat(fname, 0600)) < 0) {
		if (errno != EEXIST) {
			scds_syslog(LOG_ERR, "Unable to create <%s>: %s.",
				fname, strerror(errno));
			return (-1);
		}
	}
	(void) close(fd);
	return (0);
}

/*
 * remove_giveover_file()
 * Removes the file that is created at the beginning of an
 * attempted GIVEOVER of resource groups from a node.
 * Returns 0 for success, -1 for error, as returned by unlink().
 */

int
remove_giveover_file()
{
	char    *fname = HANFS_STATE_DIR"/"GIVEOVER_FILE;
	int	rc = 0;

	rc = unlink(fname);
	if (rc != 0) {
		if (errno == ENOENT)
			rc = 0;
	}
	return (rc);
}

/*
 * Checks for existence of file. If the file exists,
 * it returns the size of the file in the input pointer.
 * Return 0 on success, 1 if the file does not
 * exist, -1 for other errors (returned by stat()).
 */

int
check_file(char *fname, off_t *size)
{
	struct stat sbuf;
	int rc = 0;

	*size = 0;

	rc = stat(fname, &sbuf);
	if (rc == 0)
		*size = sbuf.st_size;
	else if (errno == ENOENT && fname != NULL)
		rc = 1;
	else
		/*
		 * SCMSGS
		 * @explanation
		 * Status of the named file could not be
		 * obtained.
		 * @user_action
		 * Check the permissions of the file and all
		 * components in the path prefix.
		 */
		scds_syslog(LOG_ERR,
			"stat of file %s failed.", fname);

	return (rc);
}


#if	(SOL_VERSION >= __s10)

/*
 * For the passed FMRI, set the startd/duration value to "transient"
 * -1: error
 *  0: success
 */
int
set_startd_duration_transient(char *fmri)
{
	scf_handle_t *h = NULL;
	scf_property_t *prop = NULL;
	scf_instance_t *inst = NULL;
	scf_service_t *svc = NULL;
	const char *pgname = SCF_PG_STARTD;
	const char *pgtype = SCF_GROUP_FRAMEWORK;
	const char *pname = SCF_PROPERTY_DURATION;
	scf_propertygroup_t *pg = NULL;
	scf_value_t *v = NULL, *v2 = NULL;
	scf_transaction_t *tx = NULL;
	scf_transaction_entry_t *e = NULL, *e2 = NULL;
	hrtime_t timestamp;
	uint8_t bool;
	int l1, rc, cret;
	scf_error_t error;


	scds_syslog_debug(DBG_LEVEL_HIGH,
		"Setting startd/duration to transient for %s", fmri);
	/*
	 * Generic setup
	 */
	h = scf_handle_create(SCF_VERSION);
	if (h == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * An API call failed.
		 * @user_action
		 * Examine log files and syslog messages to determine the
		 * cause of the failure. Take corrective action based on any
		 * related messages. If the problem persists, report it to
		 * your Sun support representative for further assistance.
		 */
		scds_syslog(LOG_ERR, "scf_handle_create failed: %s",
			scf_strerror(scf_error()));
		return (-1);
	}
	rc = scf_handle_bind(h);
	if (rc) {
		/*
		 * SCMSGS
		 * @explanation
		 * An API call failed.
		 * @user_action
		 * Examine log files and syslog messages to determine the
		 * cause of the failure. Take corrective action based on any
		 * related messages. If the problem persists, report it to
		 * your Sun support representative for further assistance.
		 */
		scds_syslog(LOG_ERR, "scf_handle_bind failed: %s",
			scf_strerror(scf_error()));
		scf_handle_destroy(h);
		return (-1);
	}
	svc = scf_service_create(h);
	if (!svc) {
		/*
		 * SCMSGS
		 * @explanation
		 * An API call failed.
		 * @user_action
		 * Examine log files and syslog messages to determine the
		 * cause of the failure. Take corrective action based on any
		 * related messages. If the problem persists, report it to
		 * your Sun support representative for further assistance.
		 */
		scds_syslog(LOG_ERR, "scf_service_create failed: %s",
			scf_strerror(scf_error()));
		goto scf_err;
	}
	inst = scf_instance_create(h);
	if (!inst) {
		/*
		 * SCMSGS
		 * @explanation
		 * An API call failed.
		 * @user_action
		 * Examine log files and syslog messages to determine the
		 * cause of the failure. Take corrective action based on any
		 * related messages. If the problem persists, report it to
		 * your Sun support representative for further assistance.
		 */
		scds_syslog(LOG_ERR, "scf_instance_create failed: %s",
			scf_strerror(scf_error()));
		goto scf_err;
	}
	pg = scf_pg_create(h);
	if (!pg) {
		/*
		 * SCMSGS
		 * @explanation
		 * An API call failed.
		 * @user_action
		 * Examine log files and syslog messages to determine the
		 * cause of the failure. Take corrective action based on any
		 * related messages. If the problem persists, report it to
		 * your Sun support representative for further assistance.
		 */
		scds_syslog(LOG_ERR, "scf_pg_create failed: %s",
			scf_strerror(scf_error()));
		goto scf_err;
	}
	prop = scf_property_create(h);
	if (!prop) {
		/*
		 * SCMSGS
		 * @explanation
		 * An API call failed.
		 * @user_action
		 * Examine log files and syslog messages to determine the
		 * cause of the failure. Take corrective action based on any
		 * related messages. If the problem persists, report it to
		 * your Sun support representative for further assistance.
		 */
		scds_syslog(LOG_ERR, "scf_property_create failed: %s",
			scf_strerror(scf_error()));
		goto scf_err;
	}

	/*
	 * Similar code exists in $SRC/cmd/svc/startd/libscf.c
	 * in Solaris, routines libscf_inst_set_boolean_prop(),
	 * libscf_inst_get_or_add_pg() and pg_set_prop_value().
	 */
	rc = scf_handle_decode_fmri(h, fmri, NULL, svc, inst, pg,
		prop, 0);
	if (rc != 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * An API call failed.
		 * @user_action
		 * Examine log files and syslog messages to determine the
		 * cause of the failure. Take corrective action based on any
		 * related messages. If the problem persists, report it to
		 * your Sun support representative for further assistance.
		 */
		scds_syslog(LOG_ERR, "scf_handle_decode_fmri failed: %s",
			scf_strerror(scf_error()));
		goto scf_err;
	}
	scf_instance_get_parent(inst, svc);
	rc = scf_service_get_pg(svc, pgname, pg);
	if (rc != 0) {
		rc = scf_service_add_pg(svc, pgname, pgtype, 0, pg);
		if (rc != 0) {
			/*
			 * SCMSGS
			 * @explanation
			 * An API call failed.
			 * @user_action
			 * Examine log files and syslog messages to determine
			 * the cause of the failure. Take corrective action
			 * based on any related messages. If the problem
			 * persists, report it to your Sun support
			 * representative for further assistance.
			 */
			scds_syslog(LOG_ERR, "scf_service_add_pg failed: %s",
				scf_strerror(scf_error()));
			goto scf_err;
		}
	}
	v = scf_value_create(h);
	if (!v) {
		/*
		 * SCMSGS
		 * @explanation
		 * An API call failed.
		 * @user_action
		 * Examine log files and syslog messages to determine the
		 * cause of the failure. Take corrective action based on any
		 * related messages. If the problem persists, report it to
		 * your Sun support representative for further assistance.
		 */
		scds_syslog(LOG_ERR, "scf_value_create failed: %s",
			scf_strerror(scf_error()));
		goto scf_err;
	}
	scf_value_set_astring(v, "transient");
	e = scf_entry_create(h);
	if (!e) {
		/*
		 * SCMSGS
		 * @explanation
		 * An API call failed.
		 * @user_action
		 * Examine log files and syslog messages to determine the
		 * cause of the failure. Take corrective action based on any
		 * related messages. If the problem persists, report it to
		 * your Sun support representative for further assistance.
		 */
		scds_syslog(LOG_ERR, "scf_entry_create failed: %s",
			scf_strerror(scf_error()));
		goto scf_err;
	}
	v2 = scf_value_create(h);
	if (!v2) {
		scds_syslog(LOG_ERR, "scf_value_create failed: %s",
			scf_strerror(scf_error()));
		goto scf_err;
	}
	timestamp = gethrtime();
	scf_value_set_integer(v2, timestamp);
	e2 = scf_entry_create(h);
	if (!e2) {
		scds_syslog(LOG_ERR, "scf_entry_create failed: %s",
			scf_strerror(scf_error()));
		goto scf_err;
	}

	tx = scf_transaction_create(h);
	if (!tx) {
		/*
		 * SCMSGS
		 * @explanation
		 * An API call failed.
		 * @user_action
		 * Examine log files and syslog messages to determine the
		 * cause of the failure. Take corrective action based on any
		 * related messages. If the problem persists, report it to
		 * your Sun support representative for further assistance.
		 */
		scds_syslog(LOG_ERR, "scf_transaction_create failed: %s",
			scf_strerror(scf_error()));
		goto scf_err;
	}

	do {
		rc = scf_transaction_start(tx, pg);
		if (rc != 0) {
			/*
			 * SCMSGS
			 * @explanation
			 * An API call failed.
			 * @user_action
			 * Examine log files and syslog messages to determine
			 * the cause of the failure. Take corrective action
			 * based on any related messages. If the problem
			 * persists, report it to your Sun support
			 * representative for further assistance.
			 */
			scds_syslog(LOG_ERR, "scf_transaction_start failed: %s",
				scf_strerror(scf_error()));
			goto scf_err;
		}
		rc = scf_transaction_property_new(tx, e, pname,
			SCF_TYPE_ASTRING);
		if (rc != 0) {
			error = scf_error();
			if (error == SCF_ERROR_EXISTS) {
				rc = scf_transaction_property_change(tx,
					e, pname, SCF_TYPE_ASTRING);
				if (rc != 0) {
					/*
					 * SCMSGS
					 * @explanation
					 * An API call failed.
					 * @user_action
					 * Examine log files and syslog
					 * messages to determine the cause of
					 * the failure. Take corrective action
					 * based on any related messages. If
					 * the problem persists, report it to
					 * your Sun support representative for
					 * further assistance.
					 */
					scds_syslog(LOG_ERR,
				"scf_transaction_property_change failed "
						"error %d: %s", rc,
						scf_strerror(scf_error()));
					goto scf_err;
				}
			} else {
				/*
				 * SCMSGS
				 * @explanation
				 * An API call failed.
				 * @user_action
				 * Examine log files and syslog messages to
				 * determine the cause of the failure. Take
				 * corrective action based on any related
				 * messages. If the problem persists, report
				 * it to your Sun support representative for
				 * further assistance.
				 */
				scds_syslog(LOG_ERR,
					"scf_transaction_property_new failed "
					"error %d: %s", rc,
					scf_strerror(scf_error()));
				goto scf_err;
				}
		}
		rc = scf_entry_add_value(e, v);
		if (rc != 0) {
			/*
			 * SCMSGS
			 * @explanation
			 * An API call failed.
			 * @user_action
			 * Examine log files and syslog messages to determine
			 * the cause of the failure. Take corrective action
			 * based on any related messages. If the problem
			 * persists, report it to your Sun support
			 * representative for further assistance.
			 */
			scds_syslog(LOG_ERR, "scf_entry_add_value failed: %s",
				scf_strerror(scf_error()));
			goto scf_err;
		}
		rc = scf_transaction_property_new(tx, e2,
			SCF_PROPERTY_REFRESH, SCF_TYPE_INTEGER);
		if (rc != 0) {
			error = scf_error();
			if (error == SCF_ERROR_EXISTS) {
				rc = scf_transaction_property_change(tx, e2,
					SCF_PROPERTY_REFRESH, SCF_TYPE_INTEGER);
				if (rc != 0) {
					scds_syslog(LOG_ERR,
				"scf_transaction_property_change failed "
						"error %d: %s", rc,
						scf_strerror(scf_error()));
					goto scf_err;
				}
			} else {
				scds_syslog(LOG_ERR,
					"scf_transaction_property_new failed "
					"error %d: %s", rc,
					scf_strerror(scf_error()));
				goto scf_err;
			}
		}
		rc = scf_entry_add_value(e2, v2);
		if (rc != 0) {
			scds_syslog(LOG_ERR, "scf_entry_add_value failed: %s",
				scf_strerror(scf_error()));
			goto scf_err;
		}
		cret = scf_transaction_commit(tx);
		if (cret != 1) {
			scf_transaction_reset(tx);
			scf_entry_reset(e);
			scf_entry_reset(e2);
			rc = scf_pg_update(pg);
			if (rc != 0) {
				/*
				 * SCMSGS
				 * @explanation
				 * An API call failed.
				 * @user_action
				 * Examine log files and syslog messages to
				 * determine the cause of the failure. Take
				 * corrective action based on any related
				 * messages. If the problem persists, report
				 * it to your Sun support representative for
				 * further assistance.
				 */
				scds_syslog(LOG_ERR,
					"scf_pg_update failed: %s",
					scf_strerror(scf_error()));
				goto scf_err;
			}
		}
	} while (cret != 1);

scf_err:
	if (v)
		scf_value_destroy(v);
	if (v2)
		scf_value_destroy(v2);
	if (e)
		scf_entry_destroy(e);
	if (e2)
		scf_entry_destroy(e2);
	if (tx)
		scf_transaction_destroy(tx);
	if (pg)
		scf_pg_destroy(pg);
	if (svc)
		scf_service_destroy(svc);
	if (inst)
		scf_instance_destroy(inst);
	if (prop)
		scf_property_destroy(prop);
	(void) scf_handle_unbind(h);
	scf_handle_destroy(h);
	return (0);
}


/*
 * For the passed FMRI, set the application/auto_enable value to "false"
 * -1: error
 *  0: success
 */
int
set_application_auto_enable_false(char *fmri)
{
	scf_handle_t *h = NULL;
	scf_property_t *prop = NULL;
	scf_instance_t *inst = NULL;
	scf_service_t *svc = NULL;
	const char *pgname = SCF_PG_APP_DEFAULT;
	const char *pgtype = SCF_GROUP_FRAMEWORK;
	const char *pname = "auto_enable";
	scf_propertygroup_t *pg = NULL;
	scf_value_t *v = NULL, *v2 = NULL;
	scf_transaction_t *tx = NULL;
	scf_transaction_entry_t *e = NULL, *e2 = NULL;
	hrtime_t timestamp;
	uint8_t bool;
	int l1, rc, cret;


	scds_syslog_debug(DBG_LEVEL_HIGH,
		"Setting application/auto_enable to false for %s", fmri);

	/*
	 * Generic setup
	 */

	h = scf_handle_create(SCF_VERSION);
	if (h == NULL) {
		scds_syslog(LOG_ERR, "scf_handle_create failed: %s",
			scf_strerror(scf_error()));
		return (-1);
	}
	rc = scf_handle_bind(h);
	if (rc) {
		scds_syslog(LOG_ERR, "scf_handle_bind failed: %s",
			scf_strerror(scf_error()));
		scf_handle_destroy(h);
		return (-1);
	}
	svc = scf_service_create(h);
	if (!svc) {
		scds_syslog(LOG_ERR, "scf_service_create failed: %s",
			scf_strerror(scf_error()));
		goto scf_err2;
	}
	inst = scf_instance_create(h);
	if (!inst) {
		scds_syslog(LOG_ERR, "scf_instance_create failed: %s",
			scf_strerror(scf_error()));
		goto scf_err2;
	}
	pg = scf_pg_create(h);
	if (!pg) {
		scds_syslog(LOG_ERR, "scf_pg_create failed: %s",
			scf_strerror(scf_error()));
		goto scf_err2;
	}
	prop = scf_property_create(h);
	if (!prop) {
		scds_syslog(LOG_ERR, "scf_property_create failed: %s",
			scf_strerror(scf_error()));
		goto scf_err2;
	}

	/*
	 * Similar code exists in $SRC/cmd/svc/startd/libscf.c
	 * in Solaris, routines libscf_inst_set_boolean_prop(),
	 * libscf_inst_get_or_add_pg() and pg_set_prop_value().
	 */
	rc = scf_handle_decode_fmri(h, fmri, NULL, svc, inst, pg,
		prop, 0);
	if (rc != 0) {
		scds_syslog(LOG_ERR, "scf_handle_decode_fmri failed: %s",
			scf_strerror(scf_error()));
		goto scf_err2;
	}
	scf_instance_get_parent(inst, svc);
	rc = scf_service_get_pg(svc, pgname, pg);
	if (rc != 0) {
		rc = scf_service_add_pg(svc, pgname, pgtype, 0, pg);
		if (rc != 0) {
			scds_syslog(LOG_ERR, "scf_service_add_pg failed: %s",
				scf_strerror(scf_error()));
			goto scf_err2;
		}
	}
	v = scf_value_create(h);
	if (!v) {
		scds_syslog(LOG_ERR, "scf_value_create failed: %s",
			scf_strerror(scf_error()));
		goto scf_err2;
	}
	scf_value_set_boolean(v, B_FALSE);
	e = scf_entry_create(h);
	if (!e) {
		scds_syslog(LOG_ERR, "scf_entry_create failed: %s",
			scf_strerror(scf_error()));
		goto scf_err2;
	}

	tx = scf_transaction_create(h);
	if (!tx) {
		scds_syslog(LOG_ERR, "scf_transaction_create failed: %s",
			scf_strerror(scf_error()));
		goto scf_err2;
	}

	do {
		rc = scf_transaction_start(tx, pg);
		if (rc != 0) {
			scds_syslog(LOG_ERR, "scf_transaction_start failed: %s",
				scf_strerror(scf_error()));
			goto scf_err2;
		}
		rc = scf_transaction_property_change(tx, e, pname,
			SCF_TYPE_BOOLEAN);
		if (rc != 0) {
			scds_syslog(LOG_ERR,
				"scf_transaction_property_change failed "
				"error %d: %s", rc,
				scf_strerror(scf_error()));
			goto scf_err2;
		}
		rc = scf_entry_add_value(e, v);
		if (rc != 0) {
			scds_syslog(LOG_ERR,
				"scf_entry_add_value failed: %s",
				scf_strerror(scf_error()));
			goto scf_err2;
		}
		cret = scf_transaction_commit(tx);
		if (cret != 1) {
			scf_transaction_reset(tx);
			scf_entry_reset(e);
			scf_entry_reset(e2);
			rc = scf_pg_update(pg);
			if (rc != 0) {
				scds_syslog(LOG_ERR,
					"scf_pg_update failed: %s",
					scf_strerror(scf_error()));
				goto scf_err2;
			}
		}
	} while (cret != 1);

scf_err2:
	if (v)
		scf_value_destroy(v);
	if (v2)
		scf_value_destroy(v2);
	if (e)
		scf_entry_destroy(e);
	if (e2)
		scf_entry_destroy(e2);
	if (tx)
		scf_transaction_destroy(tx);
	if (pg)
		scf_pg_destroy(pg);
	if (svc)
		scf_service_destroy(svc);
	if (inst)
		scf_instance_destroy(inst);
	if (prop)
		scf_property_destroy(prop);
	(void) scf_handle_unbind(h);
	scf_handle_destroy(h);
	return (0);
}

/*
 * Check if the startd/duration property is set to "transient".
 * This property does not exist by default and is added to the
 * property group by INIT method.
 */
boolean_t
is_startd_duration_transient(char *fmri, boolean_t init_flag)
{
	scf_simple_prop_t *prop;
	boolean_t rc = B_FALSE;

	prop = scf_simple_prop_get(NULL, fmri, "startd", "duration");
	if (prop != NULL) {
		if (strcmp(scf_simple_prop_next_astring(prop),
			"transient") == 0)
			rc = B_TRUE;
		scf_simple_prop_free(prop);
	} else if (!init_flag) {
		/*
		 * SCMSGS
		 * @explanation
		 * An API call failed.
		 * @user_action
		 * Examine log files and syslog messages to determine the
		 * cause of the failure. Take corrective action based on any
		 * related messages. If the problem persists, report it to
		 * your Sun support representative for further assistance.
		 */
		scds_syslog(LOG_ERR,
			"Could not verify value of property %s for %s.",
			"startd/duration", fmri);
	}

	scds_syslog_debug(DBG_LEVEL_HIGH,
		"is_transient for %s returned %s.", fmri, rc?"true":"false");

	return (rc);
}

/*
 * Return the application/auto_enable property value.
 * Since this property exists by default, if property cannot be
 * determined, we assume that the value is correct.
 */
boolean_t
get_application_auto_enable(char *fmri)
{
	scf_simple_prop_t *prop;
	uint8_t	*bp;
	boolean_t rc = B_TRUE; /* default property value */

	prop = scf_simple_prop_get(NULL, fmri, "application", "auto_enable");
	if (prop != NULL) {
		bp = scf_simple_prop_next_boolean(prop);
		if (*bp == 0)
			rc = B_FALSE;
		scf_simple_prop_free(prop);
	} else {
		scds_syslog(LOG_ERR,
			"Could not verify value of property %s for %s.",
			"application/auto_enable", fmri);
	}

	scds_syslog_debug(DBG_LEVEL_HIGH,
		"%s auto_enable property is %s.", fmri, rc?"true":"false");

	return (rc);
}

#endif	/* SOL_VERSION >= __s10 */


/*
 * Make the SMF services corresponding to NFS transient. This will
 * prevent SMF from restarting daemons that are running under
 * HA-NFS. This method is assumed to be called only by INIT (see
 * call to is_startd_duration_transient() below).
 *
 * Return 0 on success, non-zero on failure.
 */
int
update_smf_services_for_ha()
{
	int i, rc = 0;

#if	(SOL_VERSION >= __s10)

	char *s, *ret;
	uint8_t *bp;
	scf_simple_prop_t *prop;
	boolean_t is_transient;
	boolean_t auto_enable;
	boolean_t enabled;
	boolean_t init_flag = B_TRUE;
	char	*what_state = NULL;
	int 	already_disabled = 0;

	scds_syslog_debug(DBG_LEVEL_HIGH,
		"Executing update_smf_services_for_ha().");

	for (i = 0; s = svcs[i]; i++) {

		/* If SMF service is already transient, skip it */
		is_transient = is_startd_duration_transient(s, init_flag);
		if (is_transient)
			scds_syslog_debug(DBG_LEVEL_HIGH,
				"Skipping %s: already transient.", s);

		/* If SMF service has auto_enable set to false, skip it */
		auto_enable = get_application_auto_enable(s);
		if (!auto_enable)
			scds_syslog_debug(DBG_LEVEL_HIGH, "Skipping %s: "
				"application/auto_enable is already "
				"'false'.", s);

		if (is_transient && !auto_enable)
			continue;

		/*
		 * Make SMF service transient and set auto_enable to
		 * 'false'.
		 */
		if (!is_transient) {
			/*
			 * Save the state of the SMF service, if the
			 * service is disabled outside HA-NFS by admin,
			 * then HA-NFS should not enable it.
			 */
			what_state = smf_get_state(s);
			if (what_state == NULL) {
				/*
				 * SCMSGS
				 * @explanation
				 * The Solaris service management facility
				 * failed to get the name of the service
				 * instance's current state that the fault
				 * management resource identifier (FMRI) in
				 * the /var/adm/messages specifies.
				 * @user_action
				 * Save a copy of the /var/adm/messages files
				 * on all nodes. Contact your authorized Sun
				 * service provider for assistance
				 * in diagnosing the problem.
				 */
				scds_syslog(LOG_ERR,
				"Failed to get service state %s:%s", s,
					scf_strerror(scf_error()));
				return (-1);
			}
			already_disabled = strcmp(what_state,
				SCF_STATE_STRING_DISABLED);
			/*
			 * When the service instance is marked as transient
			 * and the daemon process is exited the
			 * ct_pr_tmpl_set_transfer (3CONTRACT) fails with an
			 * error code 3 (ESRCH) and svc.startd aborts. This
			 * means that the supplied contract ID, the service's
			 * primary contract did not correspond to a process
			 * contract.
			 * If the "startd/duration" property of service is set
			 * to "transient", and the service is refreshed "svcadm
			 * refresh <instance>". The service instance transient
			 * flag is set and the daemon is restarted.Now since the
			 * service instance is not disabled and re-enabled, it
			 * does not result in contract transfers and when the
			 * daemon is killed svc.startd attempts to transfer the
			 * contracts from the service's primary contract, to the
			 * process contract and thereby the service contract is
			 * destroyed, ct_pr_tmpl_set_transfer() fails, and
			 * svc.startd aborts. To workaround this the service
			 * instance has to be disabled, marked as transient and
			 * then enabled. Refer to CR 6216358/6454639 for more
			 * details.
			 */
			if (already_disabled != 0) {
				rc = smf_disable_instance(s, 0);
				if (rc != 0) {
					/*
					 * SCMSGS
					 * @explanation
					 * The Solaris service management
					 * facility failed to disable the
					 * service instance that the fault
					 * management resource identifier
					 * (FMRI) in the /var/adm/messages
					 * specifies.
					 * @user_action
					 * Run the command
					 * 'svcadm disable <fmri>' to disable
					 * the service instance. Then run the
					 * command 'svccfg -s <fmri>' to enable
					 * you to modify the configuration of
					 * the service instance interactively.
					 * Use the subcommand
					 * 'addpg startd framework' to add the
					 * startd and framework property group
					 * and the subcommand
					 * 'setprop startd/duration = astring:
					 * transient' to set the duration to
					 * transient. After modfiying the
					 * configuration of the service
					 * instance, run the command
					 * 'svcadm enable <fmri>' to enable
					 * the service instance. Then run the
					 * command 'svcs -x <service>' command
					 * to verify that the service is online.
					 * If the problem persists, contact your
					 * Sun Support representative for
					 * further assistance.
					 */
					scds_syslog(LOG_ERR,
						"Failed to disable %s service ",
						s);
					return (rc);
				}
			}
			/*
			 * SCMSGS
			 * @explanation
			 * The specified SMF service is being moved to
			 * disabled/maintenance state.
			 * @user_action
			 * This is an informational message. No action
			 * is needed.
			 */
			scds_syslog(LOG_NOTICE,
					"Waiting for %s service to be moved "
					"into disabled/maintenance state.", s);
			/*
			 * Wait until the SMF service is moved into disabled or
			 * to maintenance state. If the SMF service is enabled
			 * before the service state is disabled, then the nfs
			 * daemons can get restarted. See CR 6196325 for
			 * details.
			 */
			while (1) {
				what_state = smf_get_state(s);
				if (what_state == NULL) {
					scds_syslog(LOG_ERR,
					"Failed to get service state %s:%s", s,
						scf_strerror(scf_error()));
					return (-1);
				}
				scds_syslog_debug(DBG_LEVEL_HIGH,
					"%s is now in %s "
					"state.", s, what_state);
				if (strcmp(what_state,
					SCF_STATE_STRING_DISABLED) == 0 ||
					strcmp(what_state,
						SCF_STATE_STRING_MAINT) == 0) {
					break;
				}
				free(what_state);
				what_state = NULL;
				(void) usleep(100000);
			}
			rc = set_startd_duration_transient(s);
			if (rc != 0)
				return (rc);

			rc = smf_refresh_instance(s);
			if (rc != 0) {
				/*
				 * SCMSGS
				 * @explanation
				 * The Solaris service management facility
				 * failed to refresh the service instance
				 * that the fault management resource
				 * identifier (FMRI) in the /var/adm/messages
				 * specifies.
				 * @user_action
				 * Run the command 'svcadm refresh <fmri>' to
				 * refresh the service instance. Then run the
				 * command 'svcs -x <service>' to verify that
				 * the service is online. If the problem
				 * persists, contact your Sun Support
				 * representative for further assistance.
				 */
				scds_syslog(LOG_ERR,
					"Failed to refresh %s after property "
					"update.", s);
				return (rc);
			}
			/*
			 * If the SMF service is already disabled by the admin
			 * and outside the agent, then HA-NFS should not enable
			 * it. The current implementation disables and enables
			 * the SMF service as the Fix for CR 6216358/6454639.
			 * Hence if already_disabled is not zero, it means the
			 * SMF service is disabled outside HA-NFS and the HA-NFS
			 * should not enable the SMF service.
			 */
			if (already_disabled != 0) {
				rc = smf_enable_instance(s, 0);
				if (rc != 0) {
				/*
				 * SCMSGS
				 * @explanation
				 * The Solaris service management facility
				 * failed to enable the service instance
				 * that the fault management resource
				 * identifier (FMRI) in the /var/adm/messages
				 * specifies.
				 * @user_action
				 * Run the command 'svcadm enable <fmri>'to
				 * enable the service instance. Then run the
				 * command 'svcs -x <service>' to verify that
				 * the service is online. If the problem
				 * persists, contact your Sun Support
				 * representative for further assistance.
				 */
					scds_syslog(LOG_ERR,
						"Failed to enable %s service ",
						s);
					return (rc);
				}
			}
		}

		if (auto_enable) {
			rc = set_application_auto_enable_false(s);
			if (rc != 0)
				return (rc);
		}

		scds_syslog_debug(DBG_LEVEL_HIGH, "%s is now transient "
			"and has auto_enable set to 'false'.", s);

		if (what_state != NULL)
			free(what_state);
	}

#endif	/* SOL_VERSION >= __s10 */

	return (rc);
}
