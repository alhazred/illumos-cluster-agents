/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * nfs_svc_start.c - Start method for highly available NFS
 */

#pragma ident	"@(#)nfs_svc_start.c	1.25	07/06/06 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "hanfs.h"

/*
 * The start method for HA-NFS. Called when the LogicalHostnames
 * are already up. Does some sanity checks on
 * the resource settings then re-starts the appropriate
 * NFS daemons. Note that the share command(s) have already been
 * done in the nfs_prenet_start method. The steps here are
 * 1. Kill and restart statd/lockd
 * 2. Start nfsd/mountd (if not running)
 */

int
main(int argc, char *argv[])
{
	int	rc, lockid;
	scds_handle_t	scds_handle;
	char	*giveover_file = HANFS_STATE_DIR"/"GIVEOVER_FILE;
	char	*state_file = HANFS_STATE_DIR"/"HANFS_STATE_FILE;
	char	*rsname = NULL, *rgname = NULL;
	off_t	size = 0;

	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR) {
		return (1);
	}

	/* Get resource and resource group names */
	rsname = (char *)scds_get_resource_name(scds_handle);
	rgname = (char *)scds_get_resource_group_name(scds_handle);

	scds_syslog_debug(4, "Calling svc_validate.");
	if (svc_validate(scds_handle, B_FALSE) != SCHA_ERR_NOERR) {
		(void) scha_resource_setstatus(rsname, rgname,
			SCHA_RSSTATUS_FAULTED,
			"Invalid resource settings.");
		return (1);
	}
	/* Grab the system-wide lock. Waits... */
	lockid = nfs_serialize_lock();

	/* Check if giveover file exists - size ignored */
	rc = check_file(giveover_file, &size);
	if (rc == 0) {
		/*
		 * Giveover file exists. Verify if it should
		 * exist by looking at the nfs.state file.
		 */
		rc = check_file(state_file, &size);
		if (rc == 0) {
			/* nfs.state file exists */
			if (size > 0) {
				/*
				 * Giveover may be in
				 * progress. Exit.
				 */
				scds_syslog(LOG_ERR,
					"Aborting startup: "
					"failover of NFS resource "
					"groups may be in progress.");
				rc = 1;
				goto finished;
			} else {
				/*
				 * nfs.state file is empty.
				 * Maybe nfs_postnet_stop did
				 * not cleanup giveover file.
				 */
				rc = remove_giveover_file();
			}
		} else if (rc > 0) {
			/*
			 * nfs.state file does not exist.
			 * This should not happen.
			 * (/var/run should be clean).
			 * Cleanup giveover file.
			 */
			rc = remove_giveover_file();
		}
	} else if (rc > 0) {
		/* Giveover file does not exist */
		rc = 0;
	}

	if (rc != 0) {
		/*
		 * Something wrong - could not determine if
		 * giveover file exists or not. Do not proceed.
		 */
		scds_syslog(LOG_ERR,
			"Aborting startup: could not determine "
			"whether failover of NFS resource groups "
			"is in progress.");
		rc = 1;
		goto finished;
	}

	/* Stop lockd and statd, no timeout */
	rc = stop_lockd_statd(-1);
	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_WARNING,
			"Failed to stop daemon %s.",
			"lockd/statd");
		(void) scha_resource_setstatus(rsname, rgname,
			SCHA_RSSTATUS_FAULTED,
			"Failed to stop lockd/statd.");
		rc = 1;
		goto finished;
	}
	/*
	 * Start statd and lockd. We just added this resource to the
	 * list of NFS resources on the node. So the -p/-a options to
	 * statd have been already pre-computed to contain this
	 * resource.
	 */
	scds_syslog(LOG_NOTICE, "Starting %s.", "statd and lockd");
	rc = start_statd_lockd(scds_handle, -1);
	if (rc != SCHA_ERR_NOERR) {
		/* This sounds like a serious problem */
		(void) scha_resource_setstatus(rsname, rgname,
			SCHA_RSSTATUS_FAULTED,
			"Failed to start statd/lockd.");
		rc = 1;
		goto finished;	/* We still want to release the lock */
	}

	/* Update the timestamp of proc_statefile to let the */
	/* fault monitor know that nfs daemons have been restarted. */

	(void) getset_file_mtime(NFS_OP_SET);

	(void) scha_resource_setstatus(rsname, rgname,
		SCHA_RSSTATUS_OK,
		"Successfully started NFS service.");
	scds_syslog(LOG_NOTICE, "Completed successfully.");

finished:
	/*
	 * Release the lock. We are exiting here, so the locking
	 * is not really an issue. But nfs_serialize_unlock() also
	 * updates the comment in the lock file, so that while
	 * debugging things we are sure that nothing dumped core
	 * or some such...
	 */
	(void) nfs_serialize_unlock(lockid);

	/* Free up the Environment resources that were allocated */

/*	scds_close(scds_handle); */

	return (rc);
}
