#!/bin/sh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#ident	"@(#)nfs_start_daemons.sh	1.20	07/08/07 SMI"
#
# Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#
# cluster/src/cmd/ha-services/nfs/nfs_start_daemons.sh
#
# Called to start statd, lockd or nfsd.
# Computes the correct way to start them with the
# right arguments, propogates status to caller.
# Called with one argument statd, lockd, nfsd
# or mountd
#
# The output of this command is redirected into a file
# useful only for debugging. So we are free to use things
# like "echo" etc. The user never sees the output. The caller
# (presumably some HANFS RT method) uses the exit code from
# here to syslog() some stuff anyways.
#
# This script is called while holding the HA-NFS state lock
# so we need not worry about it changing from under us.
#
# We need not worry about the processes in question which we
# are attempting to start, the caller has already killed them
# (with SIGKILL). You can only do so much double checking...
#
# Some part of the code also exist in HA1.3
#

HANFS_DIR=/var/run/.hanfs

if [ $# -ne 1 ]
then
	echo invalid argument list $*
	exit 1
fi

daemon=$1
hanfs_state_file=$HANFS_DIR/nfs.state
statd_args_file=$HANFS_DIR/statd.args
nfsd_args_file=$HANFS_DIR/nfsd.args

#
# create nfsv4 state directory
# and change the owner to daemon
#
v4dir_cmd() {
	
	for dir in $*
	do
		if [ ! -d $dir ]
		then
			/bin/mkdir -m 755 -p $dir > /dev/null
			if [ $? -ne 0 ]
			then
				echo mkdir failed to create $dir
				return 1
			fi

			/usr/bin/chown -R daemon:daemon $dir \
				> /dev/null
			if [ $? -ne 0 ]
			then
				echo chown failed for $dir
				return 1
			fi
		fi
	done
        return 0

}

# To make this script to be idempotent,
# make sure hanfs_state_file exists
/usr/bin/touch $hanfs_state_file

if [ ! -r $hanfs_state_file ]
then
	echo Can not read $hanfs_state_file
	exit 1
fi

# See which daemon needs to be started up
case $daemon in
	'statd')
		# Need to assemble the -p and -a options
		rm -f $statd_args_file
		# First assemble the -p options

		awk ' {print $3} ' < $hanfs_state_file |
		sort | uniq |
		while read path
		do
        		if [ -d $path ]
        		then
                		echo "-p $path \c" >> $statd_args_file
				else
						echo invalid statmon path $path
						exit 1
        		fi
		done

		# Now the -a options. These are the ipaddresses passed to
		# statd via the -a options. The code basically parses out
		# the first 3 fields (RG, R, StatmonPath) from the state
		# File, what is left is the ipaddresses.
		#

		awk ' { for(i=4; i <= NF; i++) print $i} ' < $hanfs_state_file |
		sort | uniq |
		while read ipaddr
		do
        		echo "-a $ipaddr \c" >> $statd_args_file
		done

		# The statd.args file is created IFF there are
		# additional arguments to statd
		if [ -r $statd_args_file ]
		then
			echo statd args are `cat $statd_args_file`
        		/usr/lib/nfs/statd `cat $statd_args_file`
			rc=$?
		else
			echo No additional statd arguments
        		/usr/lib/nfs/statd
			rc=$?
		fi

		exit $rc

		;;	# End of statd case

	'nfsd')
		LC_ALL="C"
		export LC_ALL
		OS_REL=`uname -r`

		# HA-NFSv4 stable storage files will be written into
		# directory ".nfsv4" within *each* RG admin path.
		# The nfs.state file contains the Pathprefixes to be
		# passed to nfsd.
		# Need to assemble the -s options
		rm -f $nfsd_args_file

		awk ' {print $3} ' < $hanfs_state_file |
		sort | uniq |
		while read path
		do
        		if [ -d $path ]
        		then
                		echo "-s $path \c" >> $nfsd_args_file
			else
						echo invalid nfsd path $path
						exit 1
			fi
			# On Sun Cluster the HA-NFS must create and make
			# <RG-PathPrefix>/SUNW.nfs/v4_state and v4_oldstate
			# writable by "daemon". The nfsd will create the
			# state directories it needs under <RG-PathPrefix>/
			# SUNW.nfs; the ones under /var/nfs - and /var/nfs
			# itself - are created as part of the pkg install,
			# [i.e. at Solaris install]. Please refer to
			# CR 6463700
			echo $OS_REL|/usr/bin/grep -v 5\.[0-9]$ > /dev/null
			if [ $? -eq 0 ]
			then
				v4dir_cmd $path/v4_state $path/v4_oldstate
				if [ $? -ne 0 ]
				then
					exit 1
				fi
			fi
		done

		# Note that we no longer start nfsd explicitly in the TS scheduling
		# class in order to co-exist with SRM.

		#
		#
		# Search the /etc/init.d/nfs.server command file for the line
		# that starts up the nfsd, in case the system administrator
		# has customized that line, for example, by raising the
		# number of daemon threads or by using the other nfsd
		# switches, such as what network protocols to use.
		# If there are multiple nfsd lines in the nfs.server
		# command file, just use the first.
		# Thus, if the system administrator has increased the
		# number of servers in /etc/init.d/nfs.server, we'll use
		# that value.
		# According to SMCC NFS Server Performance and Tuning Guide:
		# "The default setting, 16, in Solaris 2.4 software environments
		# results in poor NFS response times.  Scale the setting with
		# the number of processors and networks.  Increase the number
		# of NFS server threads by editing the invocation of nfsd in
		# etc/init.d/nfs.server."
		# That is, the manual recommends to the administrator that the
		# way to tune the nfsd is to edit the /etc/init.d/nfs.server
		# script, thus, HA-NFS tries to honor any such edits.
		#

		DEFAULT_NFSDCMD="/usr/lib/nfs/nfsd"
		if [ -f /etc/init.d/nfs.server ]; then
			NFSDCMD="`egrep '^[^#]*/usr/lib/nfs/nfsd' \
				/etc/init.d/nfs.server \
				2>/dev/null | head -1`"
		fi
		if [ -z "$NFSDCMD" ]; then
			NFSDCMD="$DEFAULT_NFSDCMD"
		fi

		echo nfsd command is $NFSDCMD

		# The nfsd.args file is created if there are
		# additional arguments to nfsd
		if [ -r $nfsd_args_file ]
		then
			echo nfsd args are `cat $nfsd_args_file`

			# For Solaris system where nfsd warm restart option is 
			# not supported. We failback to normal startup of 
			# nfsd process

			LC_ALL="C"
			export LC_ALL

			$NFSDCMD `cat $nfsd_args_file` \
				>$HANFS_DIR/nfsd.out 2>&1
			rc=$?
			if [ $rc -ne 0 ]
			then
				grep "illegal option" $HANFS_DIR/nfsd.out
				if [ $? -eq 0 ]
				then
					cat $HANFS_DIR/nfsd.out
					echo No additional nfsd arguments
					$NFSDCMD
                        		rc=$?
				fi
			fi
		else
			echo No additional nfsd arguments
			$NFSDCMD
			rc=$?
		fi
		if [ $rc -ne 0 ]
		then
			echo nfsd start failed with $rc
			exit 1
		fi
		exit 0

		;; # End of nfsd case

	'lockd')
		# Do the same as in nfsd case by extracting the lockd startup
		# command in /etc/init.d/nfs.client
		# Strip the last '&' from the command line using sed.

		# nfs.client file removed from Solaris 10.
		if [ -f /etc/init.d/nfs.client ]; then
			LOCKDCMD="`egrep '^[^#]*/usr/lib/nfs/lockd' \
				/etc/init.d/nfs.client \
				2>/dev/null | tail -1 | sed 's/ &/ /'`"
		fi
		if [ -z "$LOCKDCMD" ]; then
			LOCKDCMD="/usr/lib/nfs/lockd"
		fi

		echo lockd command is $LOCKDCMD
		eval $LOCKDCMD
		rc=$?
		if [ $rc -ne 0 ]
		then
			echo lockd start failed with $rc
			exit 1
		fi
		exit 0

		;; # End of lockd case

	'mountd')
		# No customization is needed in this case
		# simply call mountd

		/usr/lib/nfs/mountd
		rc=$?
		if [ $rc -ne 0 ]
		then
			echo mountd start failed with $rc
			exit 1
		fi
		exit 0

		;; # End of mountd case

	*)
		echo Unknown daemon $daemon
		exit 1

		;;
esac

echo Why are we here
exit 1

