/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * nfs_postnet_stop.c - PostNetStop method for highly available nfs
 */

#pragma ident	"@(#)nfs_postnet_stop.c	1.25	07/06/06 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "hanfs.h"

/*
 * This method is called AFTER the Logical ipaddresses have been
 * brought down on this node.
 *
 * Steps:
 * 1. stop lockd/statd
 * 2. unshare filesystems
 * 3. start statd/lockd
 * 4. start nfsd/mountd (may have been killed in STOP mentod)
 *
 * Update: AH: If stopping statd/lockd fails, do not attempt
 * to restart them. Do the unshare etc. But return  non-zero.
 */

int
main(int argc, char *argv[])
{
	scds_handle_t	scds_handle;
	int		lockid, rc;
	int		statd_lockd_failed;
	char		fname[PATH_MAX]
			= HANFS_STATE_DIR"/"HANFS_STATE_FILE,
			msg[INT_ERR_MSG_SIZE];
	char		*rsname = NULL, *rgname = NULL;
	off_t		size = 0;

	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR) {
		return (1);	/* Something very basic is messed up */
	}

	/* Get resource and resource group names */
	rsname = (char *)scds_get_resource_name(scds_handle);
	rgname = (char *)scds_get_resource_group_name(scds_handle);

	/* Check to see if Pathprefix is usable */
	rc = check_pathprefix(scds_handle);
	if (rc == 0) {
		/*
		 * Patheprefix doesn't exist, but return
		 * success anyway, as the start methods
		 * were not successful either.
		 */
		return (0);
	}
	if (rc < 0) {
		/*
		 * Patheprefix doesn't exist, no
		 * point in continuing. return  Failure.
		 */
		return (1);
	}

	/* Grab the system-wide lock. Waits... */
	lockid = nfs_serialize_lock();

	/* Stop lockd and statd, no timeout */
	scds_syslog(LOG_NOTICE, "Stopping %s.", "lockd and statd");
	statd_lockd_failed = stop_lockd_statd(-1);
	if (statd_lockd_failed != 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * The HA-NFS implementation was unable to stop the specified
		 * daemon.
		 * @user_action
		 * The resource could be in a STOP_FAILED state. If the
		 * failover mode is set to HARD, the node would get
		 * automatically rebooted by the SunCluster resource
		 * management. If the Failover_mode is set to SOFT or NONE,
		 * please check that the specified daemon is indeed stopped
		 * (by killing it by hand, if necessary). Then clear the
		 * STOP_FAILED status on the resource and bring it on-line
		 * again using the scswitch command.
		 */
		scds_syslog(LOG_ERR,
		    "Failed to stop daemon %s.",
		    "lockd/statd");
		rc = statd_lockd_failed;
	}
	/*
	 * Perform the unshare commands. Return value
	 * is the number of failed unshare commands
	 * We ignore them.
	 */
	rc = nfs_unshare_resource(scds_handle);
	if (rc != 0) {
		/* Unshare return  codes are un-reliable */
		/*
		 * SCMSGS
		 * @explanation
		 * HA-NFS postnet_stop method was unable to complete the
		 * unshare(1M) command for some of the paths specified in the
		 * dfstab file.
		 * @user_action
		 * The exact pathnames which failed to be unshared would have
		 * been logged in earlier messages. Run those unshare commands
		 * by hand. If problem persists, reboot the node.
		 */
		scds_syslog(LOG_WARNING,
			"Unable to complete some unshare commands.");
	}
	/* Remove resource from the NFS state file */
	rc = nfs_unregister_resource(scds_handle);
	if (rc != 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * The list of HA-NFS resources online on the node has gotten
		 * corrupted.
		 * @user_action
		 * Make sure there is space available in /tmp. If the error is
		 * showing up despite that, reboot the node.
		 */
		scds_syslog(LOG_WARNING,
			"Unable to re-compute NFS resource list.");
	}
	/*
	 * Remove giveover file if all RGs have been given over.
	 * In case of error, message is logged. nfs_prenet_start
	 * may also attempt to clean up the giveover file if we
	 * fail to remove it, and nfs.state file size is zero.
	 */
	rc = check_file(fname, &size);
	if (rc == 0 && size == 0)
		rc = remove_giveover_file();
	rc = 0;

	/* Start statd and lockd if we were able to stop them cleanly */
	if (statd_lockd_failed == 0) {
		rc = start_statd_lockd(scds_handle, -1);
		if (rc != 0) {
			/* This sounds like a serious problem */
			(void) sprintf(msg,
				"Failed to start %s.",
				"statd/lockd");
			(void) scha_resource_setstatus(rsname, rgname,
				SCHA_RSSTATUS_FAULTED, msg);
			rc = 1;
			goto finished;
		}
	}
	/* Start nfsd/mountd if not running already */
	rc = start_nfsd_mountd(scds_handle, -1);
	if (rc != 0) {
		/* This sounds like a serious problem */
		(void) sprintf(msg,
			"Failed to start %s.",
			"nfsd/mountd");
		(void) scha_resource_setstatus(rsname, rgname,
			SCHA_RSSTATUS_FAULTED, msg);
			rc = 1;
			/* Update the timestamp of proc_statefile, since */
			/* statd and lockd have been restarted successfully */
			(void) getset_file_mtime(NFS_OP_SET);
			goto finished;
	}
	/* remove the stop file */
	(void) remove_stop_file(rsname);

	/* Update the timestamp of proc_statefile after daemon restarted */
	(void) getset_file_mtime(NFS_OP_SET);

	(void) scha_resource_setstatus(rsname, rgname,
	    SCHA_RSSTATUS_OFFLINE,
	    "Completed successfully.");

finished:
	/* Release the lock. We are return ing here, but anyway... */
	(void) nfs_serialize_unlock(lockid);

	if (statd_lockd_failed)
		rc = 1;
	return (rc);
}
