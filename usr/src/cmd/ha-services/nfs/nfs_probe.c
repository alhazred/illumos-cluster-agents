/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * nfs_probe.c - Probe for HA-NFS
 */

#pragma ident	"@(#)nfs_probe.c	1.33	08/10/08 SMI"

#include <stdio.h>
#include "hanfs.h"
#include <time.h>
#include <sys/stat.h>
#include <sys/mnttab.h>

static char	*dfs_path;

/*
 * First check for the timestamp of dfstab file to see if it has been
 * modified since last time the file paths were cached in memory.
 * If so, refresh the fs list stored in memory.  Otherwise, do nothing.
 * Note: the old fslist is free'ed in nfs_get_fslist().
 * Returns 0 for succeeds, -1 for errors.
 */
static int
refresh_fslist(scds_handle_t scds_handle, time_t *ts,
    shared_path_list_t **fslist)
{
	struct stat	st;

	/* Get the timestamp of dfstab file */
	if (stat(dfs_path, &st) != 0) {
		/* Suppress lint error about lack of _errno() prototype */
		scds_syslog(LOG_ERR,
		    "File %s is not readable: %s.",
		    dfs_path, strerror(errno));	/*lint !e746 */
		return (-1);
	}

	if (st.st_mtime > *ts) {
		scds_syslog_debug(2,
		    "dfstab file %s could have been modified lately; "
		    "do a refresh",
		    dfs_path);
		if (nfs_get_fslist(scds_handle, fslist) != 0)
			/* A message has already been logged */
			return (-1);

		*ts = time(0);
	}

	return (0);
}

/*
 * Check for the timestamp of mnttab & sharetab file to see if any one of them
 * has been modified since last time checked. If changed, cache the current
 * value. Returns 0 for succeeds, -1 for errors. ismstabtouched contains 1
 * if mnttab file changes and contains 2 if sharetab file also changes.
 */
static int
check_if_touched(time_t *ts, int *ismstabtouched)
{
	struct stat	sharest;
	struct stat	mntst;

	/* Get the timestamp of sharetab file */
	if (stat(SHARETAB, &sharest) != 0) {
		/* Suppress lint error about lack of _errno() prototype */
		scds_syslog(LOG_ERR,
			"File %s is not readable: %s.",
			SHARETAB, strerror(errno)); /*lint !e746 */
		return (-1);
	}

	/* Get the timestamp of mnttab file */
	if (stat(MNTTAB, &mntst) != 0) {
		/* Suppress lint error about lack of _errno() prototype */
		scds_syslog(LOG_ERR,
			"File %s is not readable: %s.",
			MNTTAB, strerror(errno)); /*lint !e746 */
		return (-1);
	}

	*ismstabtouched = 0;

	if (mntst.st_mtime > *ts) {
		*ismstabtouched = 1;
	}

	if (sharest.st_mtime > *ts) {
		*ismstabtouched = 2;
	}

	if (*ismstabtouched) {
		*ts = time(0);
	}

	return (0);
}

/*
 * Check the existence of /var/run/.hanfs/nfs.down file.
 * This file is created by the system fault monitor when there is a
 * serious problem on restarting nfs daemon.  If the file exist,
 * that means nfs daemons are not providing service.
 * Returns 1 if nfs daemons have problem, 0 for ok.
 */
static int
check_nfs_down()
{
	struct stat	st;
	char		filename[PATH_MAX];

	(void) sprintf(filename, "%s/%s", HANFS_STATE_DIR, HANFS_DOWN_FILE);
	if (stat(filename, &st) == 0) {
		scds_syslog_debug(1,
		    "nfs.down file exists; NFS daemons are not functioning.");
		return (1);
	}

	return (0);
}

/*
 * main():
 * An infinite loop. Sleeps for Thourough_Probe_Interval,
 * check stat() on the file system exported by NFS resource
 * in each loop.
 */
int
main(int argc, char *argv[])
{
	scds_handle_t		scds_handle;
	scha_rsstatus_t		rstatus = SCHA_RSSTATUS_UNKNOWN;
	int			rc, thorough_probe_interval, probe_result;
	shared_path_list_t	*fslist = NULL;
	time_t			last_cache_ts;
	time_t			old_last_cache_ts;
	time_t			last_cache_smts;
	int			ismstabtouched = 0;
	int			isdtabtouched = 0;
	char			*rsname = NULL, *rgname = NULL;

	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR) {
		return (1);
	}

	/* Get resource and resource group names */
	rsname = (char *)scds_get_resource_name(scds_handle);
	rgname = (char *)scds_get_resource_group_name(scds_handle);

	/* Get thorough_probe_interval */
	thorough_probe_interval =
		scds_get_rs_thorough_probe_interval(scds_handle);

	/* Get the filename of dfstab file */
	dfs_path = get_dfstab_filename(scds_handle, B_FALSE);
	if (dfs_path == NULL) {
		/* A message has already been logged */
		return (1);
	}

	/* get the entries in dfstab */
	if (nfs_get_fslist(scds_handle, &fslist) != 0) {
		/* should not happen, prenet_start should fail on this */
		/* A message has already been logged */
		return (1);
	}

	/* Initialize them to the current time. */
	last_cache_ts = time(0);
	last_cache_smts = time(0);

	/* Infinite loop */
	for (;;) {

		/*
		 * sleep for a duration of thorough_probe_interval between
		 *  successive probes.
		 */
		(void) scds_fm_sleep(scds_handle, thorough_probe_interval);

		/* store the old timestamp. useful for resharing paths. */
		old_last_cache_ts = last_cache_ts;

		/* Refresh memory if dfstab has been changed recently */
		rc = refresh_fslist(scds_handle, &last_cache_ts, &fslist);
		if (rc != 0) {
			/* Error occurs when reading dfstab file. */
			/* Set resource status to FAULTED. */
			rstatus = SCHA_RSSTATUS_FAULTED;
			(void) scha_resource_setstatus(rsname,
				rgname, SCHA_RSSTATUS_FAULTED,
				"dfstab not readable");
			continue;
		}

		/* Check if dfstab file has been refreshed. */
		isdtabtouched = last_cache_ts > old_last_cache_ts ? 1 : 0;

		/* Check if mnttab or sharetab is touched/modified. */
		if (check_if_touched(&last_cache_smts, &ismstabtouched) != 0) {
			/* Message already logged */
			/*
			 * If mnttab or sharetab is not readable, atleast
			 * continue with the usual probe (stat of dfstab paths)
			 */
			ismstabtouched = 0;
		}

		/*
		 * Probe (stat on share file path) all existing dfstab paths.
		 * If there is any change in status of paths (i.e share/unshare
		 * or mount/umount), then we share the dfstab paths again.
		 */
		probe_result = svc_probe(scds_handle, dfs_path, &fslist,
				isdtabtouched, ismstabtouched,
				&last_cache_smts);

		/*
		 * If any shared path has changed status from ok to
		 * stat_failed OR if there is any nfs daemon reporting
		 * a problem, set the resource status to FAULTED.
		 * This faulted status can be cleared when all
		 * shared paths are ok and no nfs problem.
		 */
		if (probe_result != 0 || check_nfs_down() == 1) {
			if (probe_result != 0)
				(void) scha_resource_setstatus(
					rsname, rgname,
					SCHA_RSSTATUS_FAULTED,
					"Invalid shared path(s)");
			else
				(void) scha_resource_setstatus(
					rsname, rgname,
					SCHA_RSSTATUS_FAULTED,
					"NFS daemon down");
			rstatus = SCHA_RSSTATUS_FAULTED;
			continue;
		}

		/*
		 * Probe succeeded
		 * All shared paths and nfs daemons are good
		 * Reset resource status to OK
		 */
		if (rstatus != SCHA_RSSTATUS_OK) {
			rstatus = SCHA_RSSTATUS_OK;
			(void) scha_resource_setstatus(rsname, rgname,
				SCHA_RSSTATUS_OK, "Service is online.");
		}
	}
}
