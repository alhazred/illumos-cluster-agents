/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#pragma ident	"@(#)nfs_daemons_probe.c	1.17	08/07/01 SMI"

/*
 * nfs_daemons_probe.c -
 * This is a per-node NFS monitoring daemon which
 * monitors the health of the nfs related daemons.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <libgen.h>
#include <signal.h>
#include <rpc/rpc.h>
#include <rpc/types.h>
#include <rpc/svc.h>
#include <netconfig.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/uadmin.h>
#include <netdb.h>
#include <strings.h>
#include <rpc/rpc.h>
#include <assert.h>

#include "hanfs.h"

/* tags of extension properties */
#define	RPC_TIMEOUT_RPCBIND	"Rpcbind_nullrpc_timeout"
#define	RPC_TIMEOUT_NFSD	"Nfsd_nullrpc_timeout"
#define	RPC_TIMEOUT_MOUNTD	"Mountd_nullrpc_timeout"
#define	RPC_TIMEOUT_STATD	"Statd_nullrpc_timeout"
#define	RPC_TIMEOUT_LOCKD	"Lockd_nullrpc_timeout"

#define	RPC_RESTART_RPCBIND	"Rpcbind_nullrpc_reboot"
#define	RPC_RESTART_NFSD	"Nfsd_nullrpc_restart"
#define	RPC_RESTART_MOUNTD	"Mountd_nullrpc_restart"

/* Timeout on creating client RPC handle (should be huge) */
#define	RPC_CREATE_TIMEOUT	300

/* Retry timeout for the null rpc call waits for the server to reply before */
/* retransmitting the request */
#define	RPC_RETRY_TIMEOUT	2

/*
 * daemoninfo_t
 * A data structure to keep the info of a nfs daemon.
 *
 * The first 6 fields are constants after initialization.
 * The values of restart_rpc_failed for statd and lockd are
 * meaningless, since we don't want to restart these two daemons
 * when null rpc call fails, just syslog messages.
 *
 * The last 3 fields will be reset/cached after the daemon is restarted.
 */
typedef struct daemoninfo {
	char		*proc_path;
	char		*basename_proc_path;
	rpcprog_t	prognum;
	rpcvers_t	version;
	int		rpc_timeout;
	boolean_t	restart_rpc_failed;
	/* the cache follows: */
	time_t		ts;
	pid_t		pid;
	CLIENT		*clnt;
} daemoninfo_t;


static int lockid;
static char *netid = "udp";
static struct netconfig *nconf = NULL;
static char *hostname = "localhost";	/* loopback */
static char nfs_down_file[PATH_MAX];

static void
proc_init(scds_handle_t scds_handle, char *proc, rpcprog_t prognum,
	rpcvers_t version, char *timout_p, char *restart_p,
	scha_prop_type_t prop_type, daemoninfo_t *d)
{
	scha_extprop_value_t *t, *r;

	d->proc_path = proc;
	d->basename_proc_path = basename(proc);
	d->prognum = prognum;
	d->version = version;

	if (timout_p == NULL) {
		scds_syslog(LOG_ERR,
			"INTERNAL ERROR: %s.", "Property name is null");
		exit(1);
	}

	if (scds_get_ext_property(scds_handle, timout_p, prop_type, &t)
		!= SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
			"Property %s is not set.", timout_p);
		exit(1);
	}
	d->rpc_timeout = t->val.val_int;

	if (restart_p == NULL)
		d->restart_rpc_failed = B_FALSE;
	else {
		if (scds_get_ext_property(scds_handle, restart_p,
			prop_type, &r) != SCHA_ERR_NOERR) {
			scds_syslog(LOG_ERR,
				"Property %s is not set.", restart_p);
			exit(1);
		}
		d->restart_rpc_failed = r->val.val_boolean;
	}

	d->ts = (time_t)0;
	d->pid = (pid_t)0;
	d->clnt = (CLIENT *) NULL;

	scds_syslog_debug(7,
		"proc_init: proc %s, basename_proc %s, prognum %d, "
		"version %d, timeout %d, restart %d",
		d->proc_path, d->basename_proc_path, d->prognum, d->version,
		d->rpc_timeout, d->restart_rpc_failed);
} /* proc_init */

static void
discard_cache(daemoninfo_t *d)
{
	d->ts = (time_t)0;
	d->pid = (pid_t)-1;
	if (d->clnt != NULL) {
		clnt_destroy(d->clnt);
		d->clnt = NULL;
	}
} /* discard_cache */

/*
 * checkcache -
 * If the cache is not already filled in, or if the file mtime
 * vs the cache timestamp shows that the cache is too old,
 * we fill in the cache.
 * Otherwise (the cache was already filled in and was sufficiently
 * recent) we see if the pid still exists: if so, return NFS_ERR_NONE
 * immediately; if not discard_cache and fill it in again.
 * Return code is one of:
 *	NFS_ERR_NONE
 *	NFS_ERR_UNKNOWN  --  some problem occurred such that we are unsure.
 *	NFS_ERR_PID_NOT_EXIST  --  means checkproc couldn't find the pid
 */
static int
checkcache(daemoninfo_t *d)
{
	time_t	newts;
	pid_t	newpid;
	CLIENT	*newclnt = NULL;
	int	rc;
	struct timeval timeout = { 0, 0 };

	if (d->ts != 0) {
		/*
		 * Already filled in.
		 * Check the file mtime for cache being too old.
		 *
		 * Note that >= is used here, not >, which means
		 * that equal should be treated as too old.
		 */
		if (getset_file_mtime(NFS_OP_GET) >= d->ts)
			discard_cache(d);	/* fall thru */
	}

	if (d->ts != 0) {
		/* Check if the pid still exists */
		rc = kill(d->pid, 0);
		if (rc != -1)
			return (NFS_ERR_NONE);

		/*
		 * Suppress lint: call to ___errno() not made in the
		 * presence of a prototype.
		 */
		if (errno != ESRCH) {	/*lint !e746 */
			char	internal_err_str[INT_ERR_MSG_SIZE];
			(void) sprintf(internal_err_str,
				"Invalid pid %d for daemon %s.",
				d->pid, d->basename_proc_path);
			scds_syslog(LOG_ERR,
				"INTERNAL ERROR: %s.", internal_err_str);

			discard_cache(d);
			return (NFS_ERR_UNKNOWN);
		}
		discard_cache(d);  /* and fall thru */
	}

	newts = time(0);
	rc = checkproc(d->proc_path, &newpid);
	if (rc == NFS_ERR_UNKNOWN || rc == NFS_ERR_PID_NOT_EXIST)
		return (rc);

	/* Have pid.  Try to get the rpc binding */
	timeout.tv_sec = RPC_CREATE_TIMEOUT;
	newclnt = clnt_tp_create_timed(hostname, d->prognum, d->version,
		nconf, &timeout);
	if (newclnt == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * HA-NFS fault monitor was not able to make an rpc connection
		 * to an nfs server.
		 * @user_action
		 * Save the /var/adm/messages file. Contact your authorized
		 * Sun service provider to determine whether a workaround or
		 * patch is available.
		 */
		scds_syslog(LOG_ERR,
			"clnt_tp_create_timed of program %s failed %s.",
			d->basename_proc_path, clnt_spcreateerror(""));

		/*
		 * We treat inability to create the binding handle
		 * as an unsure condition.  The rpcbind fault
		 * monitoring will itself find problems with rpcbind.
		 */
		return (NFS_ERR_UNKNOWN);
	}

	/* Have both pid and binding */
	d->ts = newts;
	d->pid = newpid;
	d->clnt = newclnt;

	/* Reset the retry timeout for retransimitting to 2 seconds */
	timeout.tv_sec = RPC_RETRY_TIMEOUT;

	/*
	* From clnt_control(3NSL), CLSET_RETRY_TIMEOUT is valid
	* for connectionless transports only
	*/
	if (is_nfs_tcp_only())
		return (NFS_ERR_NONE);

	if (clnt_control(d->clnt, CLSET_RETRY_TIMEOUT, (caddr_t)&timeout)
		== FALSE) {
		/*
		 * SCMSGS
		 * @explanation
		 * HA-NFS fault monitor failed to reset the retry timeout for
		 * retransimitting the rpc request.
		 * @user_action
		 * Save the /var/adm/messages file. Contact your authorized
		 * Sun service provider to determine whether a workaround or
		 * patch is available.
		 */
		scds_syslog(LOG_ERR,
			"clnt_control of program %s failed %s.",
			d->basename_proc_path, clnt_spcreateerror(""));
		return (NFS_ERR_UNKNOWN);
	}

	return (NFS_ERR_NONE);
} /* checkcache */

/*
 * create_down_file()
 * Creates a file named /var/run/.hanfs/nfs.down.
 * This function is called when restarting nfs daemon fails and to
 * inform the resource fault monitors running on the node
 * to mark the resource to faulted.
 * Returns 0 for success, and -1 for errors.
 */
static int
create_down_file()
{
	int	fd;

	fd = creat(nfs_down_file, 0600);

	if (fd < 0) {
		scds_syslog(LOG_ERR,
			"Unable to create <%s>: %s.",
			nfs_down_file, strerror(errno));
		return (-1);
	}

	(void) close(fd);
	return (0);
}

/*
 * probe_proc()
 * Probe the specified nfs daemon.  First check if the pid kept in memory
 * is still valid (the daemon could have been restarted by some nfs method)
 * Then do a null rpc call on the daemon.
 *
 * Update: If the daemon being probed is nfsd, then NULL RPC is
 * NOT attempted if is_nfsd_active() returns TRUE (it checks the
 * kstat counter nfs:nfs_server:calls to determine that).
 */
static int
probe_proc(daemoninfo_t *d)
{
	int		rc;
	enum clnt_stat	rpc_rc;
	struct timeval timeout;

	rc = checkcache(d);

	if (rc == NFS_ERR_UNKNOWN)
		return (NFS_ERR_UNKNOWN);

	if (rc == NFS_ERR_PID_NOT_EXIST) {
		/*
		 * If the daemon is stopped by some resource's stop method,
		 * return NFS_ERR_UNKNOWN and retry in next probe.
		 * The postnet_stop method would restart them.
		 */
		if (check_stop_file() == 1)
			/* For probing statd or lockd, this could */
			/* go into sleep, but will catch in the next cycle */
			return (NFS_ERR_UNKNOWN);
		else {
			/*
			 * SCMSGS
			 * @explanation
			 * HA-NFS fault monitor checks the health of statd,
			 * lockd, mountd and nfsd daemons on the node. It
			 * detected that one these are not currently running.
			 * @user_action
			 * No action. The monitor would restart these.
			 */
			scds_syslog(LOG_ERR,
				"Daemon %s is not running.",
				d->basename_proc_path);
			/* Mark nfs down */
			(void) create_down_file();
			return (NFS_ERR_PID_NOT_EXIST);
		}
	}

	assert(rc == NFS_ERR_NONE);

	scds_syslog_debug(3, "probe_proc(): proc = %s, pid = %d.",
		d->proc_path, d->pid);

	/*
	 * Check to see if the process is SIGSTOPPED.
	 * If so, SIGKILL it and return NFS_ERR_PID_NOT_EXIST.
	 */
	if (is_process_stopped(d->pid) > 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * HA-NFS is restarting the specified daemon.
		 * @user_action
		 * No action.
		 */
		scds_syslog(LOG_ERR, "Restarting daemon %s.",
			d->basename_proc_path);

		/*
		 * Hard-coded timeout of 30 seconds to kill the
		 * process. Since this MUST be initiated by
		 * the user (by sending SIGSTOP), one assumes
		 * that this cluster is not under production
		 * workload.
		 */
		if (stop_nfs_daemon(d->proc_path, d->prognum,
			d->version, 30) == 0) {
			/* Killed successfully */
			return (NFS_ERR_PID_NOT_EXIST);
		}
		/*
		 * Process did not die, or killproc() ran into
		 * other problems. return NFS_ERR_UNKNOWN, we
		 * will deal with it in the next iteration
		 */
		return (NFS_ERR_UNKNOWN);
	}
	/*
	 * Check to see if the daemon being probed is nfsd,
	 * if so, call is_nfsd_active() and return success
	 * if it is active.
	 */
	if (strcmp(d->basename_proc_path, "nfsd") == 0) {
		boolean_t	is_active = B_FALSE;
		if (is_nfsd_active(&is_active) == 0) {
			if (is_active == B_TRUE) {
				return (NFS_ERR_NONE);
			}
		}
	}

	/* Prepare to run NULL RPC */
	timeout.tv_sec = d->rpc_timeout;
	timeout.tv_usec = 0;

	/*
	 * Release HA-NFS system lock as we are about to
	 * start a potentially long-running operation.
	 */
	(void) nfs_serialize_unlock(lockid);

	rpc_rc = clnt_call(d->clnt, NULLPROC, (xdrproc_t)xdr_void,
		(caddr_t)NULL, (xdrproc_t)xdr_void, (caddr_t)NULL, timeout);

	lockid = nfs_serialize_lock();

	if (rpc_rc == RPC_SUCCESS)
		return (NFS_ERR_NONE);

	if ((getset_file_mtime(NFS_OP_GET) >= d->ts) ||
		check_stop_file() == 1) {
		discard_cache(d);
		return (NFS_ERR_UNKNOWN);
	}

	/*
	 * Note that we cannot discard_cache until we
	 * are done with using d->clnt to call clnt_sperror()
	 */
	/*
	 * SCMSGS
	 * @explanation
	 * HA-NFS fault monitor failed to ping an nfs daemon.
	 * @user_action
	 * No action required. The fault monitor will restart the daemon if
	 * necessary.
	 */
	scds_syslog(LOG_ERR,
		"daemon %s did not respond to null rpc call: %s.",
		d->basename_proc_path, clnt_sperror(d->clnt, ""));

	discard_cache(d);
	return (NFS_ERR_NULL_RPC_FAILED);
}  /* probe_proc */

/*
 * get_node_rg_rs()
 * This function takes pointers as arguments, and returns in them
 * the names of all local resource groups, the number of local
 * resource groups, and the name of one resource (first entry)
 * from each local resource group, as seen in the nfs.state file.
 * These return objects are intended to be used to attempt GIVEOVER
 * of the resource groups.
 * All allocated memory is expected to be freed by the calling
 * function.
 * Return values are 0 for success, and non-zero for failure.
 * This function is only called by attempt_giveover().
 */

static int
get_local_rg_rs(char ***local_rg, char ***local_rs, uint_t *count_rg)
{
	FILE	*fp = NULL;
	char	*fname = HANFS_STATE_DIR"/"HANFS_STATE_FILE;
	char	line[2048] = "";
	char	*linep = line;
	char	rgname[1024] = "";
	char	rsname[1024] = "";
	char	**tmpptr1 = NULL, **tmpptr2 = NULL;
	uint_t	i = 0, j = 0;
	int	rc = 0;
	boolean_t matched = B_FALSE;


	/* Initialize count of RGs read from nfs state file. */
	*count_rg = 0;

	/* Make sure local_rg and local_rs are NULL */
	*local_rg = NULL;
	*local_rs = NULL;

	fp = fopen(fname, "r");
	if (fp == NULL) {
		scds_syslog(LOG_ERR, "Unable to open %s: %s.",
			fname, strerror(errno));
		return (-1);
	}

	while (fgets(linep, sizeof (line), fp)) {
		/* Get the first token in the line */
		if (sscanf(linep, "%s %s", rgname, rsname) != 2) {
			scds_syslog(LOG_ERR,
				"INTERNAL ERROR: %s.",
				"Could not parse the file "
				HANFS_STATE_FILE " to attempt "
				"GIVEOVER of resource groups");
			rc = -1;
			break;
		}

		matched = B_FALSE;
		j = 0;
		while (j < i) {
			if (strcmp(*(*local_rg+j++), rgname) == 0) {
				matched = B_TRUE;
				break;
			}
		}

		if (matched == B_FALSE) {
			tmpptr1 = (char **)realloc(*local_rg,
				(i+1)*sizeof (char *));
			tmpptr2 = (char **)realloc(*local_rs,
				(i+1)*sizeof (char *));
			if (tmpptr1 == NULL || tmpptr2 == NULL) {
				scds_syslog(LOG_ERR, "Out of memory.");
				rc = -1;
				break;
			} else {
				*local_rg = tmpptr1;
				*(*local_rg+i) = strdup(rgname);
				*local_rs = tmpptr2;
				*(*local_rs+i) = strdup(rsname);

				if (*(*local_rg+i) == NULL ||
					*(*local_rs+i) == NULL) {
					scds_syslog(LOG_ERR,
						"Out of memory.");
					rc = -1;
					break;
				}

				*count_rg = ++i;
			}
		}
	}

	if (rc != 0) {
		/*
		 * Possibly out of memory.
		 * Free already allocated space.
		 */
		if (*local_rg) {
			j = 0;
			while (j < *count_rg)
				if (*(*local_rg+j)) free(*(*local_rg+j++));
			free(*local_rg);
		}
		if (*local_rs) {
			j = 0;
			while (j < *count_rg)
				if (*(*local_rs+j)) free(*(*local_rs+j++));
			free(*local_rs);
		}
	}

	(void) fclose(fp);

	return (rc);
}

/*
 * attempt_giveover()
 * Called when probe fails and history indicates that
 * a failover needs to be attempted. Parses the nfs.state
 * file and attempts failover of all resource groups found
 * in that file.
 * Returns 0 for success, non-zero for error.
 */

static int
attempt_giveover()
{
	char	**local_rg = NULL;
	char	**local_rs = NULL;
	uint_t	count_rg = 0;
	uint_t	i = 0;
	int	rc = 0;
	scha_err_t	err = SCHA_ERR_NOERR;

	/*
	 * SCMSGS
	 * @explanation
	 * HA-NFS is failing over all NFS resources.
	 * @user_action
	 * This is an informational message, no action is needed. Look for
	 * previous error messages from HA-NFS for the reason for failing
	 * over.
	 */
	scds_syslog(LOG_NOTICE,
		"Failing over all NFS resources from this node.");
	/*
	 * Create giveover file to prevent other resources from
	 * starting up while GIVEOVER is in progress.
	 */
	rc = create_giveover_file();

	/*
	 * Get list of local resource groups from the nfs.state
	 * file. We then attempt to GIVEOVER these resource groups.
	 */
	rc = get_local_rg_rs(&local_rg, &local_rs, &count_rg);
	if (rc) {
		/* Could not get list of RGs. Cleanup and exit */
		rc = remove_giveover_file();
		return (-1);
	}

	/* Release the lock */
	rc = nfs_serialize_unlock(lockid);

	if (count_rg > 0) {
		while (i < count_rg) {
			/* Attempt GIVEOVER of local RGs */
			err |= scha_control(SCHA_GIVEOVER,
				local_rg[i], local_rs[i]);

			free(local_rg[i]);
			free(local_rs[i++]);
		}
		free(local_rg);
		free(local_rs);
	}

	/* Grab the system-wide lock. */
	lockid = nfs_serialize_lock();

	/* Update modification time for the state file */
	(void) getset_file_mtime(NFS_OP_SET);

	/*
	 * If GIVEOVER of some RGs failed, remove giveover
	 * file and continue the normal probe cycle.
	 * If GIVEOVER was successful, the nfs_daemons_probe
	 * would get killed, and this code should never
	 * be executed. The giveover file should normally be
	 * removed by nfs_postnet_stop.
	 */
	rc = remove_giveover_file();

	if (err == SCHA_ERR_NOERR)
		rc = 0;
	else
		rc = -1;

	return (rc);
}

/*
 * main():
 * An infinite loop. Sleeps for Cheap_Probe_Interval,
 * probe the NFS daemons on local host in each loop.
 * We keep track of probe failures/successes and try
 * to restart the NFS daemons upon failure, call scha_control()
 * to giveup the RG if that doesn't work.
 *
 * The NFS_ERR_UNKNOWN error code is considered as "keep trying".
 *
 * The fault monitor will lock the system-wide lock at startup.
 * The normal state whilst the fault monitor is running is that it holds
 * this lock.  It will release this lock in only three places:
 * a. At the point in the main() program loop where the fault monitor goes
 * to sleep(), it will unlock the lock just before calling sleep and lock
 * the lock again just after returning from the sleep().
 * b. At the point in the probing where we make the null rpc call with the
 * longish timeout, we will unlock the lock just before doing the
 * clnt_call, and lock the lock again just after we we return from the
 * clnt_call, before we start to look at the error return code from clnt_call.
 * c. We unlock at process exit.
 */

int
main(int argc, char *argv[])
{
	scds_handle_t		scds_handle;
	nfs_fm_err_t		err, statd_err, lockd_err;
	daemoninfo_t		rpcbind_info, nfsd_info,
				mountd_info, statd_info,
				lockd_info;
	struct	stat 		statbuf;
	char			*rsname = NULL, *rgname = NULL;
	time_t			start_timeout, stop_timeout;
	int			rc, sleep_sec, e_action;

	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR) {
		return (1);
	}

	proc_init(scds_handle, NFS_RPCBIND_PATH, RPC_PROGNUM_RPCBIND,
		RPC_VERSION_RPCBIND, RPC_TIMEOUT_RPCBIND,
		RPC_RESTART_RPCBIND, SCHA_PTYPE_INT, &rpcbind_info);
	proc_init(scds_handle, NFS_NFSD_PATH, RPC_PROGNUM_NFSD,
		RPC_VERSION_NFSD, RPC_TIMEOUT_NFSD,
		RPC_RESTART_NFSD, SCHA_PTYPE_INT, &nfsd_info);
	proc_init(scds_handle, NFS_MOUNTD_PATH, RPC_PROGNUM_MOUNTD,
		RPC_VERSION_MOUNTD, RPC_TIMEOUT_MOUNTD,
		RPC_RESTART_MOUNTD, SCHA_PTYPE_INT, &mountd_info);
	proc_init(scds_handle, NFS_STATD_PATH, RPC_PROGNUM_STATD,
		RPC_VERSION_STATD, RPC_TIMEOUT_STATD,
		NULL, SCHA_PTYPE_INT, &statd_info);
	proc_init(scds_handle, NFS_LOCKD_PATH, RPC_PROGNUM_LOCKD,
		RPC_VERSION_LOCKD, RPC_TIMEOUT_LOCKD,
		NULL, SCHA_PTYPE_INT, &lockd_info);

	/* In case we are configured to use TCP only */
	if (is_nfs_tcp_only()) {
		strcpy(netid, "tcp");
	};

	nconf = getnetconfigent(netid);
	if (nconf == (struct netconfig *)NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * The daemon is unable to get needed information about
		 * transport over which it provides RPC service.
		 * @user_action
		 * Need a user action for this message.
		 */
		scds_syslog(LOG_ERR,
			"%s:  Cannot get transport information.", netid);
		return (1);
	}

	(void) sprintf(nfs_down_file,
		"%s/%s", HANFS_STATE_DIR, HANFS_DOWN_FILE);

	/* Get resource and resource group names */
	rsname = (char *)scds_get_resource_name(scds_handle);
	rgname = (char *)scds_get_resource_group_name(scds_handle);

	/* Get Cheap_probe_interval */
	sleep_sec = scds_get_rs_cheap_probe_interval(scds_handle);

	/* Get start and stop timeouts */
	start_timeout = scds_get_rs_start_timeout(scds_handle);
	stop_timeout = scds_get_rs_stop_timeout(scds_handle);

	lockid = nfs_serialize_lock();

	/* Infinite loop */
	for (;;) {

		/*
		 * Sleep for Cheap_probe_interval
		 * don't hold the system lock while sleeping....
		 */
		(void) nfs_serialize_unlock(lockid);
		/*
		 * The sleep is required in the beginning of the
		 * probe to avoid probing the service before it
		 * is up completely.
		 */
		(void) scds_fm_sleep(scds_handle, sleep_sec);

		lockid = nfs_serialize_lock();

		/*
		 * If rpcbind is dead. We can't even contact RGM
		 * to do a failover. Reboot the node unless there
		 * is a warm restart in progress. This is done
		 * only if the failover mode on the resource is
		 * set to HARD. If not, we just syslog() and continue.
		 * We don't attempt to restart rpcbind.
		 */
		err = probe_proc(&rpcbind_info);
		if (err == NFS_ERR_PID_NOT_EXIST ||
			(err == NFS_ERR_NULL_RPC_FAILED &&
			rpcbind_info.restart_rpc_failed == B_TRUE)) {
			/*
			 * If /tmp/portmap.file or /tmp/rpcbind.file
			 * exists, perhaps a warm restart of rpcbind is
			 * in progress.
			 */
			if ((stat("/tmp/portmap.file", &statbuf) == 0) ||
				(stat("/tmp/rpcbind.file", &statbuf) == 0)) {
				/*
				 * SCMSGS
				 * @explanation
				 * Need explanation of this message!
				 * @user_action
				 * Need a user action for this message.
				 */
				scds_syslog(LOG_ERR,
					"rpcbind is not responding, however "
					"/tmp/portmap.file and "
					"/tmp/rpcbind.file exist. "
					"rpcbind can be restarted with "
					"/usr/sbin/rpcbind -w."
					" Not taking any action.");
				continue;
			}
			/*
			 * If RPCBIND dies, then irrespective of Failover_mode
			 * reboot the system.
			 *
			 * If NULL RPC Fails, Reboot the system if the failover
			 * mode is set to HARD. Ignore otherwise. The reason
			 * we are rebooting is because we will not be able to
			 * get to RGM anyway without rpcbind.
			 *
			 */
			if (err == NFS_ERR_PID_NOT_EXIST ||
				scds_get_rs_failover_mode(scds_handle) ==
					SCHA_FOMODE_HARD) {
				/*
				 * SCMSGS
				 * @explanation
				 * The rpcbind daemon on this node is not
				 * running.
				 * @user_action
				 * No action. Fault monitor would reboot the
				 * node. Also see message id 804791.
				 */
				scds_syslog(LOG_ERR,
					"Rebooting this node because daemon "
					"%s is not running.",
					"rpcbind");
				/*
				 * Wait a bit for syslog to get into
				 * messages file
				 */
				(void) sleep(5);
				rc = uadmin(A_REBOOT, AD_BOOT, NULL);
				if (rc == -1) {
					/*
					 * SCMSGS
					 * @explanation
					 * HA-NFS fault monitor was attempting
					 * to reboot the node, because rpcbind
					 * daemon was unresponsive. However,
					 * the attempt to reboot the node
					 * itself did not succeed.
					 * @user_action
					 * Fault monitor would exit once it
					 * encounters this error. However,
					 * process monitoring facility would
					 * restart it (if enough resources are
					 * available on the system). If
					 * rpcbind remains unresponsive, the
					 * fault monitor (restarted by PMF)
					 * would again attempt to reboot the
					 * node. If this problem persists,
					 * reboot the node. Also see message
					 * id 804791.
					 */
					scds_syslog(LOG_ERR,
						"Failed to reboot node: %s.",
						strerror(errno));
					(void) nfs_serialize_unlock(lockid);
					return (1);
				}
				return (1);
			}
			continue;	/* Try again */
		}

		/*
		 * Probe statd and lockd
		 */
		statd_err = probe_proc(&statd_info);
		lockd_err = probe_proc(&lockd_info);

		if (statd_err == NFS_ERR_PID_NOT_EXIST ||
			lockd_err == NFS_ERR_PID_NOT_EXIST) {
			/* Must restart both, if either is dead */
			(void) stop_lockd_statd(stop_timeout);

			e_action = nfs_fm_history(scds_handle,
				SCDS_PROBE_COMPLETE_FAILURE);

			/* Failover or restart based on result */
			if (e_action == NFS_HISTORY_FAILOVER) {
				rc = attempt_giveover();
				/* If error, attempt restart */
			}

			if (start_statd_lockd(scds_handle,
				start_timeout) != 0) {
				/* Failed to restart statd/lockd */
				/* Retry in the next cycle */
				continue;
			}

			(void) getset_file_mtime(NFS_OP_SET);
			continue;
		}

		/* For NFS_ERR_NULL_RPC_FAILED and NFS_ERR_UNKNOWN, */
		/* already syslog a message, don't do any restart, */
		/* Just give it another try in next cycle */

		/*
		 * Probe mountd
		 */

		err = probe_proc(&mountd_info);

		if (err == NFS_ERR_PID_NOT_EXIST) {
			e_action = nfs_fm_history(scds_handle,
				SCDS_PROBE_COMPLETE_FAILURE);

			/* Failover or restart based on result */
			if (e_action == NFS_HISTORY_FAILOVER) {
				rc = attempt_giveover();
				/* If error, attempt restart */
			}

			if (start_nfs_daemon(scds_handle, NFS_MOUNTD_PATH,
				RPC_PROGNUM_MOUNTD, RPC_VERSION_MOUNTD,
				start_timeout) != 0) {
				/* Failed to restart mountd */
				/* Retry in next cycle */
				continue;
			}

			(void) getset_file_mtime(NFS_OP_SET);
			continue;

		} else if (err == NFS_ERR_NULL_RPC_FAILED) {
			if (mountd_info.restart_rpc_failed == B_TRUE) {
				e_action = nfs_fm_history(scds_handle,
					SCDS_PROBE_COMPLETE_FAILURE/2);

				/* Failover or restart based on result */
				if (e_action == NFS_HISTORY_FAILOVER) {
					rc = attempt_giveover();
					/* If error, attempt restart */
				}

				/* Check if the global device is available */
				/* If not, don't restart daemon, */
				/* go back to sleep and retry in next probe */
				if (scha_control(SCHA_CHECK_RESTART,
					rgname, rsname)
					== SCHA_ERR_CHECKS)
					continue;

				(void) stop_nfs_daemon(NFS_MOUNTD_PATH,
					RPC_PROGNUM_MOUNTD, RPC_VERSION_MOUNTD,
					stop_timeout);
				/* Mark nfs down */
				(void) create_down_file();
				if (start_nfs_daemon(scds_handle,
					NFS_MOUNTD_PATH, RPC_PROGNUM_MOUNTD,
					RPC_VERSION_MOUNTD,
					start_timeout) != 0) {
					/* Failed to restart mountd */
					/* Retry in next cycle */
					continue;
				}

				(void) getset_file_mtime(NFS_OP_SET);
			}

			continue;

		} else if (err == NFS_ERR_UNKNOWN)
			continue;

		/*
		 * Probe nfsd
		 */

		err = probe_proc(&nfsd_info);

		if (err == NFS_ERR_PID_NOT_EXIST) {
			e_action = nfs_fm_history(scds_handle,
				SCDS_PROBE_COMPLETE_FAILURE);

			/* Failover or restart based on result */
			if (e_action == NFS_HISTORY_FAILOVER) {
				rc = attempt_giveover();
				/* If error, attempt restart */
			}

			if (start_nfs_daemon(scds_handle, NFS_NFSD_PATH,
				RPC_PROGNUM_NFSD, RPC_VERSION_NFSD,
				start_timeout) != 0) {
				/* Failed to restart nfsd */
				/* Retry in next cycle */
				continue;
			}

			(void) getset_file_mtime(NFS_OP_SET);
			continue;

		} else if (err == NFS_ERR_NULL_RPC_FAILED) {
			if (nfsd_info.restart_rpc_failed == B_TRUE) {
				e_action = nfs_fm_history(scds_handle,
					SCDS_PROBE_COMPLETE_FAILURE/2);

				/* Failover or restart based on result */
				if (e_action == NFS_HISTORY_FAILOVER) {
					rc = attempt_giveover();
					/* If error, attempt restart */
				}
				/* Check if the pxfs device is available */
				/* If not, don't restart daemon, */
				/* go back to sleep and retry later */
				if (scha_control(SCHA_CHECK_RESTART,
					rgname, rsname) == SCHA_ERR_CHECKS)
					continue;

				(void) stop_nfs_daemon(NFS_NFSD_PATH,
					RPC_PROGNUM_NFSD, RPC_VERSION_NFSD,
					stop_timeout);
				/* Mark nfs down */
				(void) create_down_file();
				if (start_nfs_daemon(scds_handle, NFS_NFSD_PATH,
					RPC_PROGNUM_NFSD, RPC_VERSION_NFSD,
					start_timeout) != 0) {
						/* Failed to restart nfsd */
						/* Retry in next cycle */
						continue;
					}
				(void) getset_file_mtime(NFS_OP_SET);
			}

			continue;

		} else if (err == NFS_ERR_UNKNOWN)
			continue;

		if (statd_err == NFS_ERR_NONE && lockd_err == NFS_ERR_NONE) {
			/* clear the down state */
			(void) unlink(nfs_down_file);
			scds_syslog_debug(9,
				"Probe successful, "
				"all nfs daemons are healthy.");
		}

		/* Register successful probes also with history */
		e_action = nfs_fm_history(scds_handle, 0);
	}
	/* NOTREACHED */
} /* main */

/*
 * struct probe_record is the same
 * as private DSDL struct of the same name, represents
 * the result of a probe along with its timestamp.
 */

struct probe_record {
	struct probe_record	*p_next;	/* Linked list */
	hrtime_t	p_tstamp;	/* Timestamp for this probe result */
	/* Result for this probe: 0=success;100=failed;0<->100=partial */
	int	p_status;
};

/*
 * nfs_fm_history()
 * Keep track of failures in last retry_interval
 * Compares the number of complete failures in
 * last Retry_interval with Retry_count and
 * Returns:
 * NFS_HISTORY_FAILOVER: If number of complete failures
 *		is Retry_count or more.
 * NFS_HISTORY_RESTART: Local restart is needed.
 * NFS_HISTORY_NOACTION: All OK.
 * NFS_HISTORY_ERROR: Some problem (low memory and such).
 * NFS_HISTORY_NOACTION_DEG: Degraded state but not yet
 * 	a full failure to necessiate full restart.
 *
 * Keeps the history in static linked list probe_list.
 */

int
nfs_fm_history(scds_handle_t h, int status)
{
	static struct probe_record *probe_list = NULL;
	struct probe_record *r, *n, *l;
	int retry_interval, retry_count;
	int total_failures;
	hrtime_t conv = (hrtime_t)1e9;

	/* Allocate a history record */

	r = (struct probe_record *)
		calloc((size_t)1, sizeof (struct probe_record));
	if (r == NULL) {
		scds_syslog(LOG_ERR, "Out of memory.");
		return (NFS_HISTORY_ERROR);
	}
	r->p_next = NULL;
	r->p_status = status;
	r->p_tstamp = (gethrtime() / conv);	/* In seconds */

	/* Now add it at the head of the list */
	l = probe_list;
	probe_list = r;
	r->p_next = l;

	retry_interval = scds_get_rs_retry_interval(h);
	retry_count = scds_get_rs_retry_count(h);
	/*
	 * Now walk the list, computing the sum of all
	 * failures in the list. Entries older then
	 * Retry_interval are deleted from the list
	 * and are not taken into consideration.
	 */

	total_failures = 0;
	l = probe_list;
	while (l) {
		n = l->p_next;
		if (n) {
			if ((r->p_tstamp - n->p_tstamp) > retry_interval) {
				/* The next entry is too old, delete it */
				l->p_next = n->p_next;
				free(n);
				continue;
			}
		}
		total_failures += l->p_status;
		l = n;
	}
	/* Now apply some simple rules to decide if it is time to failover */
	if (status == 0) {
		return (NFS_HISTORY_NOACTION);
	}

	/*
	 * Check if we just crossed a full failure boundary
	 * i.e. the current failure (partial or otherwise)
	 * accumulate to SCDS_PROBE_COMPLETE_FAILURE or
	 * its multuple.
	 */
	if ((total_failures % SCDS_PROBE_COMPLETE_FAILURE) < status) {

		/*
		 * Total number of failures have reached retry_count
		 * Not that the DSDL internal implementation
		 * scds_fm_history() actually uses the restart_count
		 * computed via dynamic resource property
		 * SCHA_NUM_RESOURCE_RESTARTS. Since HA-NFS uses
		 * its own restarts, we cannot take the same approach
		 * here.
		 */
		if (total_failures/SCDS_PROBE_COMPLETE_FAILURE >= retry_count) {
			return (NFS_HISTORY_FAILOVER);
		}

		/* We are not yet ready to failover, restart */
		return (NFS_HISTORY_RESTART);
	}

	/*
	 * Partial failures not yet accumulated to complete failure
	 * Note that total_failures might be more then
	 * SCDS_PROBE_COMPLETE_FAILURE right now. For example, if
	 * status = 50 and total_failures = 350, meaning that
	 * old value of total_failures was 300. Still in the
	 * current probe cycle, we do not take any action. In
	 * the *previous* probe cycle, when total_failures
	 * reached 300, action would have been taken.
	 */
	return (NFS_HISTORY_NOACTION_DEG);
}
