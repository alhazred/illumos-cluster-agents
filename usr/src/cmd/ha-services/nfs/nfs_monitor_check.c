/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * nfs_monitor_check.c - Monitor check method for highly available NFS
 */

#pragma ident	"@(#)nfs_monitor_check.c	1.19	07/06/06 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "hanfs.h"

/*
 * Called BEFORE bringing a  NFS resource on-line on
 * this node. We would call svc_validate() to make
 * sure the configuration looks correct.
 */

int
main(int argc, char *argv[])
{
	scds_handle_t	scds_handle;
	char	*dfsfile;
	int		rc;
	scds_hasp_status_t	hasp_status;

	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR) {
		return (1);
	}

	/*
	 * We need to take action only if all HAStoragePlus
	 * resources that we rely on are available here
	 */
	rc = scds_hasp_check(scds_handle, &hasp_status);
	if (rc != SCHA_ERR_NOERR) {
		/* scha_hasp_check() logs everytime it fails */
		return (1);
	}

	/* was there a configuration error? */
	if (hasp_status == SCDS_HASP_ERR_CONFIG) {
		return (1); /* scds_hasp_check() syslogs the problem */
	}

	/* All HAStoragePlus resources we depend on must be online */
	if (hasp_status == SCDS_HASP_NOT_ONLINE) {
		scds_syslog(LOG_ERR, "Resource depends on a "
			"SUNW.HAStoragePlus type resource that is "
			"not online anywhere.");
		return (1);
	}

	/*
	 * Allow monitor_check to succeed if atleast one HAStoragePlus
	 * resource is not available here
	 */
	if (hasp_status == SCDS_HASP_ONLINE_NOT_LOCAL) {
		scds_syslog(LOG_INFO, "Skipping checks dependant on "
			"HAStoragePlus resources on this node.");

		return (0);
	}

	/*
	 * OK, either we dont rely on HAStoragePlus
	 * or all such resources are online here. Carry on..
	 */
	scds_syslog_debug(4, "Calling svc_validate.");
	if (svc_validate(scds_handle, B_FALSE) != SCHA_ERR_NOERR) {
		/* svc_validate() syslogs appropiately */
		return (1);
	}
	dfsfile = get_dfstab_filename(scds_handle, B_FALSE);
	if (dfsfile == NULL) {
		return (1);	/* Messages already logged */
	}
	rc = nfs_share_dfstab(dfsfile, NFS_OP_CHECK);
	if (rc == HANFS_EMPTY_DFSTAB) {
		/* zero entires in dfstab of this resource */
		/*
		 * SCMSGS
		 * @explanation
		 * The specific dfstab file does not have any entries to be
		 * shared
		 * @user_action
		 * This is a Warning. User needs to have atleast one entry in
		 * the specific dfstab file.
		 */
		scds_syslog(LOG_WARNING,
			"dfstab file %s does not have any paths to be "
			"shared. Continuing.", dfsfile);
		return (0);
	}
	if (rc < 1) {
		/* There are no usable paths */
		/*
		 * SCMSGS
		 * @explanation
		 * All the paths specified in the dfstab.<resource_name> file
		 * are invalid.
		 * @user_action
		 * Check that those paths are valid. This might be a result of
		 * the underlying disk failure in an unavailable file system.
		 * The monitor_check method would thus fail and the HA-NFS
		 * resource would not be brought online on this node. However,
		 * it is advisable that the file system be brought online
		 * soon.
		 */
		scds_syslog(LOG_ERR,
		    "None of the shared paths in file %s are valid.",
		    dfsfile);
		return (1);
	}

	/* Yes we can take this resource on this node */
	return (0);
}
