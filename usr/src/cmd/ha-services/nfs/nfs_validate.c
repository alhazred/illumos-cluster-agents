/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * nfs_validate.c - Validate method for highly available NFS
 */

#pragma ident	"@(#)nfs_validate.c	1.20	07/08/07 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <locale.h>
#include <libintl.h>
#include "hanfs.h"
#include <ds_common.h>

#if (SOL_VERSION >= __s10)
#include <zone.h>
#endif

/*
 * Called when creating/updating NFS resources.
 * For a description of things it validates,
 * look at svc_validate() in hanfs_util.c
 */

int
main(int argc, char *argv[])
{
	scds_handle_t	scds_handle;
	char	*dfsfile;
	int		rc;
	scds_hasp_status_t	hasp_status;
	extern char *optarg;
	char *value;
	int c;
	boolean_t nodelist_flag = B_FALSE;
	boolean_t update_flag = B_FALSE;
	boolean_t pathprefix_flag = B_FALSE;
	boolean_t rs_dependencies_flag = B_FALSE;
	extern boolean_t dfstab_check;
	static char *optlist[]	= {
#define	NODELIST	0
			"Nodelist",
#define	PATHPREFIX	1
			"Pathprefix",
#define	RESOURCE_DEPENDENCIES	2
			"resource_dependencies",
		(char*) NULL};

	/* I18N stuff */
	(void) setlocale(LC_ALL, "");
	(void) textdomain(TEXT_DOMAIN);
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if ((rc = scds_initialize(&scds_handle, argc, argv))
			!= SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to retrieve the resource "
			"handle: %s", scds_error_string(rc));
		(void) fprintf(stderr, gettext("Failed to retrieve the "
			"resource handle: %s\n"),
			gettext(scds_error_string(rc)));
		return (1);
	}

#if (SOL_VERSION >= __s10)
	zoneid_t	zoneid;

	if ((zoneid = getzoneid()) < 0) {
		/*
		* SCMSGS
		* @explanation
		* The validate method for the SUNW.nfs service was unable
		* to query the zoneid of the zone where it was launched into.
		* @user_action
		* Examine other syslog messages occurring at about the same
		* time to see if the problem can be identified. Save a copy
		* of the /var/adm/messages files on all nodes and contact
		* your authorized Sun service provider for assistance in
		* diagnosing and correcting the problem.
		*/
		scds_syslog(LOG_ERR, "getzoneid() failed."
			" Unable to verify privilege.");
		(void) fprintf(stderr, gettext("getzoneid() failed."
			" Unable to verify privilege.\n"));
		return (1);
	}
	if (zoneid != 0) {
		/*
		* SCMSGS
		* @explanation
		* The SUNW.nfs service can't be hosted in a non-global zone.
		* @user_action
		* Host the service only in the global zone.
		*/
		scds_syslog(LOG_ERR, "Sun Cluster HA for NFS is not supported"
				" in a non-global zone");
		(void) fprintf(stderr,
			gettext("Sun Cluster HA for NFS is not supported"
				" in a non-global zone\n"));
		return (1);
	}

#endif
	/*
	 * Determine if this is an update operation for Nodelist, Pathprefix
	 * or Resource_dependencies. Update the flags based on the arguments.
	 */
	while ((c = getopt(argc, argv, "ucr:R:T:G:x:g:")) != EOF) {
		if (c == 'u') {
			update_flag = B_TRUE;
		}
		if (c == 'g') {
			while (*optarg != '\0') {
				switch (getsubopt(&optarg, optlist, &value)) {
					case NODELIST :
						nodelist_flag = B_TRUE;
						break;
					/*
					 * Only Pathprefix update should go
					 * through HASP validation.
					 */
					case PATHPREFIX :
						pathprefix_flag = B_TRUE;
						break;
					default :
						break;
				}
			}
		}
		if (c == 'r') {
			while (*optarg != '\0') {
				if (getsubopt(&optarg, optlist, &value)
						== RESOURCE_DEPENDENCIES) {
					rs_dependencies_flag = B_TRUE;
				}
			}
		}
	}
	if (update_flag == B_TRUE &&
		pathprefix_flag == B_FALSE &&
			rs_dependencies_flag == B_FALSE) {
		dfstab_check = B_FALSE;
	}
	/*
	 * We need to take action only if all HAStoragePlus
	 * resources that we rely on are available here
	 */
	rc = scds_hasp_check(scds_handle, &hasp_status);
	if (rc != SCHA_ERR_NOERR) {
		/* scha_hasp_check() logs everytime it fails */
		(void) fprintf(stderr, gettext("INTERNAL ERROR: %s.\n"),
			gettext("scds_hasp_check failed"));
		return (1);
	}

	/* was there a configuration error? */
	if (hasp_status == SCDS_HASP_ERR_CONFIG) {
		(void) fprintf(stderr, gettext("This resource depends "
			"on a HAStoragePlus resouce that is in a "
			"different Resource Group. This configuration "
			"is not supported.\n"));
		return (1); /* scds_hasp_check() syslogs the problem */
	}
	if (dfstab_check == B_TRUE) {

		/* All HAStoragePlus resources we depend on must be online */
		if (hasp_status == SCDS_HASP_NOT_ONLINE) {
			scds_syslog(LOG_ERR, "Resource depends on a "
				"SUNW.HAStoragePlus type resource that is "
				"not online anywhere.");
			(void) fprintf(stderr, gettext("Resource depends on a "
				"SUNW.HAStoragePlus type resource that is "
				"not online anywhere.\n"));
			return (1);
		}
		/*
		 * Allow validation to succeed if atleast one HAStoragePlus
		 * resource is not available here
		 */
		if (hasp_status == SCDS_HASP_ONLINE_NOT_LOCAL) {
			scds_syslog(LOG_INFO, "Skipping checks dependant on "
				"HAStoragePlus resources on this node.");
			return (0);
		}
	}


	/*
	 * Determine if this is updation of resource group for
	 * nodelist. We do not want to call validate method
	 * before INIT has had a chance to run. In case of adding
	 * new nodes to an existing RG, the Validate method is called
	 * before INIT is run on the new nodes, Refer to CR 6326556,
	 * for more details. This fix is valid for SMF attributes on
	 * Solaris 10.
	 */
#if	(SOL_VERSION >= __s10)
	/*
	 * OK, either we dont rely on HAStoragePlus or resource group
	 * updation or all such resources are online here. Carry on..
	 */
	if (nodelist_flag != B_TRUE && update_flag != B_TRUE) {
#endif
		scds_syslog_debug(4, "Calling svc_validate.");
		if (svc_validate(scds_handle, B_TRUE) != SCHA_ERR_NOERR) {
		/* svc_validate() syslogs and prints to stderr appropriately */
			return (1);
		}
#if	(SOL_VERSION >= __s10)
	}
#endif

	if (dfstab_check == B_FALSE) {
		return (0);
	}
	/*
	 * Also verify that the paths being shared via the
	 * per resource dfstab file make sense.
	 */
	dfsfile = get_dfstab_filename(scds_handle, B_TRUE);
	if (dfsfile == NULL) {
		return (1);	/* Messages already logged and printed */
	}

	rc = nfs_share_dfstab(dfsfile, NFS_OP_VALIDATE);
	if (rc < 0) {
		/* Empty dfstab file */
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		scds_syslog(LOG_ERR, "dfstab file %s is empty.", dfsfile);
		(void) fprintf(stderr, gettext("dfstab file %s is empty.\n"),
			dfsfile);
		return (1);
	}

	if (rc > 0) {
		/* There are some paths which are not right */
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		scds_syslog(LOG_ERR,
		    "Some shared paths in file %s are invalid.", dfsfile);
		(void) fprintf(stderr, gettext("Some shared paths in file %s "
			"are invalid.\n"), dfsfile);
		return (1);
	}

	/* Successful validation */
	scds_syslog(LOG_INFO, "Completed successfully.");

	return (0);
}
