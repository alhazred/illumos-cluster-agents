/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2004,2005 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * nfs_init.c - INIT method for highly available NFS
 */

#pragma ident	"@(#)nfs_init.c	1.9	07/06/06 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ds_common.h>
#include "hanfs.h"


/*
 * The INIT method for HA-NFS. Called when the resource is
 * managed. Also called (as BOOT) when the node is starting up.
 *
 * Grab the HA-NFS nodelock and call start_nfsd_mountd() with
 * a negative timeout (meaning, don't wait up for them to
 * come fully up).
 *
 * This method implementation is purely an optimization to
 * improve HA-NFS Failover performance. At switchover, HA-NFS
 * will be able to failover faster to a node which already has
 * nfsd and mountd running.
 *
 * Thus edge case handling in this method implementation is
 * very conservative. If something fails, an error is logged
 * but no action is taken beyond that.
 */


int
main(int argc, char *argv[])
{
	int	rc, lockid;
	scds_handle_t	scds_handle;

	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR) {
		return (1);
	}

	/* If Solaris 10 or later, disable SMF service */
	if (os_newer_than_s10()) {
		rc = update_smf_services_for_ha();
		/*
		 * INIT/BOOT's prestart optimization is of no use on s10.
		 * We're going to restart nfsd later if it's already running,
		 * so why even start it in the first case?
		 *
		 * We return without doing anything else if we detect s10.
		 */

		/*
		 * Free up the environment resources
		 * that were allocated
		 */
		scds_close(&scds_handle);
		return (rc);
	}

	/* Grab the system-wide lock. Waits... */
	lockid = nfs_serialize_lock();

	/*
	 * If some resource is in between Stop and Postnet_stop,
	 * its Postnet_stop method will start nfsd/mountd.
	 * So we don't do anything and return.
	 */
	if (check_stop_file() == 1) {
		scds_syslog_debug(1, "Not starting nfsd or mountd because "
			"some NFS resource is in between Stop "
			"and Postnet_stop.");
		rc = 0;
		goto finished;
	}
	/*
	 * Start nfsd/mountd if not running already
	 * don't wait for them to come up.
	 */
	rc = start_nfsd_mountd_nowait(scds_handle);
	if (rc != SCHA_ERR_NOERR) {
		/* Do not take any action */
		scds_syslog_debug(1, "Unable to start nfsd/mountd.");
		rc = 0;
	}

finished:
	/*
	 * Release the lock. We are exiting here, so the locking
	 * is not really an issue. But nfs_serialize_unlock() also
	 * updates the comment in the lock file, so that while
	 * debugging things we are sure that nothing dumped core
	 * or some such...
	 */
	(void) nfs_serialize_unlock(lockid);

	/* Free up the Environment resources that were allocated */

	scds_close(&scds_handle);

	return (rc);
}
