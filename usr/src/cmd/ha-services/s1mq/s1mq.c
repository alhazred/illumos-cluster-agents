/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * s1mq.c - Common utilities for s1mq
 */

#pragma ident	"@(#)s1mq.c	1.12	09/01/05 SMI"

/*
 * This utility has the methods for performing the validation,
 * starting and stopping the data service and the fault monitor. It
 * also contains the method to probe the health of the data service.
 * The probe just returns either success or failure. Action is taken
 * based on this returned value in the method found in the file
 * s1mq_probe.c
 */

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <strings.h>
#include <sys/stat.h>
#include <scha.h>
#include <rgm/libdsdev.h>
#include <dlfcn.h>
#include <libintl.h>
#include <errno.h>
#include <sys/wait.h>
#include "s1mq.h"
#include "../common/ds_common.h"

/*
 * The initial timeout allowed  for the s1mq dataservice to
 * be fully up and running. We will wait for for 2 % (SVC_WAIT_PCT)
 * of the start_timeout time before probing the service.
 */
#define	SVC_WAIT_PCT		2

/*
 * We need to use 95% of probe_timeout to connect to the port and the
 * remaining time is used to disconnect from port in the svc_probe function.
 */
#define	SVC_CONNECT_TIMEOUT_PCT		95

/*
 * We need to wait for SVC_WAIT_TIME ( 2 secs) for pmf
 * to send the failure message before probing the service
 */

#define	SVC_WAIT_TIME		2

/*
 * This value will be used as disconnect timeout, if there is no
 * time left from the probe_timeout.
 */

#define	SVC_DISCONNECT_TIMEOUT_SECONDS		2

#define	SVC_SMOOTH_PCT		80

#define	IMQUSERMGR		"/usr/bin/imqusermgr"

#define	IMQ_PASSFILE_OPTION	"\\-p "

int validate_hasp(int print, scds_handle_t handle);
char *strip_path_elements(char *path, int n);
char *get_mq_password(char *confdir);
static int get_configprop_value(const char *configfile, const char *var,
	char *value);

/*
 * svc_validate():
 *
 * Do s1mq specific validation of the resource configuration.
 *
 */

int
svc_validate(scds_handle_t scds_handle, boolean_t print_messages)
{
	int		err;
	int		rc = 0, cmd_exit_code = 0;
	int		hasprc;

	scds_net_resource_list_t	*snrlp = NULL;
	scds_port_list_t		*portlist = NULL;

	scha_str_array_t	*confdirs = NULL;
	char			*broker_name = NULL;
	char			*broker_user = NULL;
	char			*password = NULL;
	char			*imq_varhome = NULL;
	char			imq_user_cmd[SCDS_CMD_SIZE];
	boolean_t		smooth;
	int			timeout = 0;
	scha_resource_t		local_handle;
	char			internal_err_str[SCDS_ARRAY_SIZE];
	char			configfile[MAXPATHLEN+1];
	char			dirpath[MAXPATHLEN+1];
	char			passfile[MAXPATHLEN+1];
	char			optionscmd[SCDS_ARRAY_SIZE];

	/*
	 * Validate HAStoragePlus resources and determine if the
	 * storage/filesystems are online on this node.
	 *
	 * If they are online validate_hasp() returns 1 in which case
	 * we proceed with the rest of validate.  If they are not
	 * online we skip the rest of the validation, leaving the
	 * validate to the other node(s) that do have access to the
	 * storage.
	 *
	 * validate_hasp() logs and prints appropriate messages for
	 * us.
	 */
	hasprc = validate_hasp(print_messages, scds_handle);

	if (hasprc == -1) {
		rc = 1;		/* error occured */
		goto finished;
	} else if (hasprc == 0) {
		rc = 0;		/* storage OK, not online on this node */
		/*
		 * If storage is not online on this node skip the rest of the
		 * property and storage validations that can safely be
		 * validated on just one node.
		 */
		goto finished;
	} else if (hasprc == 1) {
		rc = 0;		/* storage OK, online on this node */
	} else {
		ds_internal_error("unexpected return (%d) from validate_hasp",
			hasprc);
		rc = 1;
		goto finished;
	}

	/*
	 * Check that files such as /usr/bin/imbrokerd that must exist
	 * on all nodes do exist and have the correct permissions.
	 */

	err = ds_validate_file(print_messages, S_IXUSR | S_IRUSR,
		"/usr/bin/imqbrokerd");
	if (err != SCHA_ERR_NOERR) {
		rc = 1;
		goto finished;
	}

	err = ds_validate_file(print_messages, S_IXUSR | S_IRUSR,
		"/usr/bin/imqcmd");
	if (err != SCHA_ERR_NOERR) {
		rc = 1;
		goto finished;
	}

	err = ds_validate_file(print_messages, S_IXUSR | S_IRUSR,
		IMQUSERMGR);
	if (err != SCHA_ERR_NOERR) {
		rc = 1;
		goto finished;
	}

	/* Get Confdir_list extension property */
	confdirs = scds_get_ext_confdir_list(scds_handle);

	/* Return an error if there are no config_dirs x prop */
	if (confdirs == NULL || confdirs->array_cnt == 0) {
		scds_syslog(LOG_ERR, "Property %s is not set.",
			"Confdir_list");
		if (print_messages) {
			(void) fprintf(stderr, gettext("Property %s is not "
					"set.\n"), "Confdir_list");
		}
		return (1);
	}
	if (confdirs->array_cnt > 1) {
		scds_syslog(LOG_ERR,
			"Failover %s data services must have exactly "
			"one value for extension property %s.",
			APP_NAME, "Confdir_list");

		if (print_messages) {
			(void) fprintf(stderr, gettext("Failover %s data "
					"services must have exactly "
					"one value for extension "
					"property %s.\n"),
				APP_NAME, "Confdir_list");
		}
		return (1);
	}

	/* Check that Confdir_list[0] is an absolute path. */
	if (confdirs->str_array[0][0] != '/') {
		scds_syslog(LOG_ERR,
			"Confdir_list must be an absolute path.");
		if (print_messages) {
			(void) fprintf(stderr, gettext("Confdir_list must "
						"be an absolute path.\n"));
		}
		return (1);
	}

	rc = get_boolean_ext_property(scds_handle, "Smooth_shutdown",
		&smooth);
	if (rc != SCHA_ERR_NOERR) {
		/* Error already logged */
		if (print_messages)
			(void) fprintf(stderr,
				gettext("Failed to retrieve the extension "
					"property %s: %s.\n"),
				"Smooth_shutdown", scds_error_string(rc));
		goto finished;
	}

	rc = get_string_ext_property(scds_handle, "Broker_Name",
		&broker_name);
	if (rc != SCHA_ERR_NOERR) {
		/* Error already logged */
		if (print_messages)
			(void) fprintf(stderr,
				gettext("Failed to retrieve the extension "
					"property %s: %s.\n"),
				"Broker_Name", scds_error_string(rc));
		goto finished;
	}
	if (broker_name == NULL) {
		scds_syslog(LOG_ERR, "NULL value returned for the extension "
			"property %s.", "Broker_Name");
		if (print_messages)
			(void) fprintf(stderr,
				gettext("NULL value returned for the "
					"extension property %s.\n"),
				"Broker_Name");
		rc = 1;
		goto finished;
	}

	if (smooth) {
		/*
		 * Only need Broker_User if doing smooth shutdowns
		 * with imqcmd.
		 */

		rc = get_string_ext_property(scds_handle, "Broker_User",
			&broker_user);
		if (rc != SCHA_ERR_NOERR) {
			/* Error already logged */
			if (print_messages)
				(void) fprintf(stderr,
					gettext("Failed to retrieve the "
						"extension property %s: %s.\n"),
					"Broker_User", scds_error_string(rc));
			goto finished;
		}
		if (broker_user == NULL) {
			scds_syslog(LOG_ERR, "NULL value returned for the "
				"extension property %s.", "Broker_User");
			if (print_messages)
				(void) fprintf(stderr,
					gettext("NULL value returned for the "
						"extension property %s.\n"),
					"Broker_User");
			rc = 1;
			goto finished;
		}
		if (broker_user[0] == '\0') {
			/*
			 * SCMSGS
			 * @explanation
			 * A Sun ONE Message Queue resource with the
			 * Smooth_Shutdown property set to true must also set
			 * the Broker_User extension property.
			 * @user_action
			 * Set the Broker_User extension property on the
			 * resource.
			 */
			scds_syslog(LOG_ERR, "Broker_user extension property "
				"must be set");
			if (print_messages)
				(void) fprintf(stderr,
					gettext("Broker_user extension "
						"property must be set.\n"));
			rc = 1;
			goto finished;
		}
		/*
		 * cannot get the validate timeout if the rs is not
		 * created yet use 120 as timeout in that case or use
		 * 75% of VALIDATE_TIMEOUT.
		 */
		rc = scha_resource_open(scds_get_resource_name(scds_handle),
		scds_get_resource_group_name(scds_handle), &local_handle);

		if (rc != SCHA_ERR_NOERR) {
			/* resource not created yet */
			if (rc == SCHA_ERR_RSRC) {
				/*
				 * reset rc so it won't carry
				 * SCHA_ERR_RSRC forward
				 */
				rc = 0;
				timeout = 120;
				scds_syslog_debug(DBG_LEVEL_HIGH,
					"use 120 as validate timeout");
			} else {
				scds_syslog(LOG_ERR,
					"Failed to retrieve the "
					"resource handle: %s.",
					scds_error_string(rc));
				if (print_messages) {
					(void) fprintf(stderr,
					gettext("Failed to "
					"retrieve the resource handle: %s.\n"),
					scds_error_string(rc));
				}
				rc = 1;
				goto finished;
			}
		} else {
			rc = scha_resource_get(local_handle,
				SCHA_VALIDATE_TIMEOUT, &timeout);
			if (rc != SCHA_ERR_NOERR) {
				(void) snprintf(internal_err_str,
					sizeof (internal_err_str),
					"Failed to retrieve %s",
					SCHA_VALIDATE_TIMEOUT);
				scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
					internal_err_str);
				if (print_messages) {
					(void) fprintf(stderr,
					gettext("INTERNAL ERROR: %s.\n"),
					gettext(internal_err_str));
				}
				(void) scha_resource_close(local_handle);
				rc = 1;
				goto finished;
			}
			timeout = timeout * 0.75;
		}

		(void) scha_resource_close(local_handle);

		sprintf(imq_user_cmd, "%s list -i %s -u %s >/dev/null 2>&1",
					IMQUSERMGR, broker_name, broker_user);

		rc = scds_timerun(scds_handle, imq_user_cmd, timeout,
			SIGKILL, &cmd_exit_code);

		if (rc != 0 || cmd_exit_code != 0) {
			/*
			 * SCMSGS
			 * @explanation
			 * The User list operation failed for broker_user.
			 * @user_action
			 * Check the syslog and /var/adm/messages
			 * for more details.
			 */
			scds_syslog(LOG_ERR,
				"User %s does not exist", broker_user);
			if (print_messages) {
				(void) fprintf(stderr,
					gettext("User %s does not exist.\n"),
						broker_user);
			}
			rc = 1;
			goto finished;
		}
		scds_syslog_debug(DBG_LEVEL_HIGH, "broker_user validation "
						"is successful");
	}

	/*
	 * Network aware service should have at least one port specified
	 */

	rc = scds_get_port_list(scds_handle, &portlist);
	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		    "Failed to retrieve the resource property %s: %s.",
		    SCHA_PORT_LIST, scds_error_string(rc));
		if (print_messages) {
			(void) fprintf(stderr,
				gettext("Failed to retrieve the resource "
					"property %s: %s."),
					SCHA_PORT_LIST,
					scds_error_string(rc));
		}
		goto finished;
	}

	if (portlist == NULL || portlist->num_ports < 1) {
		scds_syslog(LOG_ERR,
		    "Property %s is not set.", SCHA_PORT_LIST);
		if (print_messages) {
			(void) fprintf(stderr,
				gettext("Property %s is not set."),
				SCHA_PORT_LIST);
		}
		rc = 1;
		goto finished;
	}
	if (portlist->num_ports > 1) {
		scds_syslog(LOG_ERR,
			"%s data services must have exactly "
			"one value for extension property %s.",
			APP_NAME, SCHA_PORT_LIST);
		if (print_messages) {
			(void) fprintf(stderr,
					gettext("%s data services "
					"must have exactly "
					"one value for extension property %s."),
					APP_NAME, SCHA_PORT_LIST);
		}
		rc = 1;
		goto finished;
	}

	/*
	 * Return an error if there is an error when trying to get the
	 * available network address resources for this resource
	 */
	if ((rc = scds_get_rs_hostnames(scds_handle, &snrlp))
		!= SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		    "Error in trying to access the configured network "
		    "resources : %s.", scds_error_string(rc));
		if (print_messages) {
			(void) fprintf(stderr,
				gettext("Error in trying to access the "
					"configured network "
					"resources : %s."),
				scds_error_string(rc));
		}
		goto finished;
	}

	/* Return an error if there are no network address resources */
	if (snrlp == NULL || snrlp->num_netresources == 0) {
		scds_syslog(LOG_ERR,
		    "No network address resource in resource group.");
		if (print_messages) {
			(void) fprintf(stderr,
				gettext("No network address resource in "
					"resource group."));
		}
		rc = 1;
		goto finished;
	}

	/*
	 * Make sure that config.properties exists.
	 */
	err = ds_validate_file(print_messages, S_IRUSR,
		"%s/props/config.properties", confdirs->str_array[0]);
	if (err != SCHA_ERR_NOERR) {
		rc = 1;
		goto finished;
	}

	/*
	 * Check that the broker name is correct by checking to see
	 * that we can access
	 * $IMQ_VARHOME/instances/<Broker_Name>/props/config.properties.
	 *
	 * We've checked that
	 * <Confdir_list[0]>/props/config.properties exists above.  By
	 * checking for config.properties again but by using
	 * Broker_Name we ensure that Broker_Name is valid.
	 */
	imq_varhome = strip_path_elements(confdirs->str_array[0], 2);
	if (imq_varhome == NULL) {
		ds_internal_error("Unable to construct IMQ_VARHOME.");
		if (print_messages)
			(void) fprintf(stderr,
				gettext("INTERNAL ERROR: "
					"Unable to construct IMQ_VARHOME."));
		rc = 1;
		goto finished;
	}
	err = ds_validate_file(B_FALSE, S_IRUSR,
		"%s/instances/%s/props/config.properties", imq_varhome,
		broker_name);
	if (err != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * The broker name provided in the extension property
		 * Broker_Name does not exist.
		 * @user_action
		 * Check that a broker instance exists for the supplied broker
		 * name. It should match the broker name portion of the path
		 * in Confdir_list.
		 */
		scds_syslog(LOG_ERR, "Nonexistent Broker_name (%s).",
			broker_name);
		if (print_messages)
			(void) fprintf(stderr,
				gettext("Nonexistent Broker_name (%s)."),
				broker_name);
		rc = 1;
		goto finished;
	}

	if (smooth) {
		/*
		 * Check if -p option is available for the imq server
		 * by executing the imqcmd. From IMQ 4.1 onwards, -p
		 * option for password is not supported. Instead
		 * -passfile is being used.
		 */
		rc = snprintf(optionscmd, sizeof (optionscmd),
			"/usr/bin/imqcmd | /bin/grep \"%s\" >/dev/null 2>&1",
			IMQ_PASSFILE_OPTION);
		if (rc == -1) {
			(void) fprintf(stderr,
				gettext("Failed to form command to retrieve the"
				"passfile option for mq server"));
			goto finished;
		}

		rc = system(optionscmd);
		if (rc == -1) {
			scds_syslog(LOG_ERR, "Cannot execute %s: %s.",
			    /*LINTED*/
			    optionscmd, strerror(errno));
			goto finished;
		}
		if ((!WIFEXITED(rc)) || WIFSIGNALED(rc)) {
			scds_syslog(LOG_ERR, "%s failed to complete",
			    optionscmd);
			rc = 1;
			goto finished;
		}
		rc = WEXITSTATUS((uint_t)rc);
		if (rc == 0) { 		/* Option "-p" present. */
			/*
			 * The version is 3.7 or below.
			 * Make sure that scs1mqconfig exists if smooth
			 */
			err = ds_validate_file(print_messages, 0,
				"%s/scs1mqconfig", confdirs->str_array[0]);
			if (err != SCHA_ERR_NOERR) {
				rc = 1;
				goto finished;
			}
			password = get_mq_password(confdirs->str_array[0]);
			if (password == NULL) {
				/*
				* SCMSGS
				* @explanation
				* Cannot retrieve the password for the broker.
				* @user_action
				* Check that the scs1mqconfig file is accessible
				* and correctly specifies the password.
				*/
				scds_syslog(LOG_ERR, "Unable to determine "
					"password for broker %s.",
					broker_name);
				rc = 1;
				goto finished;
			}
		} else if (rc == 1) {
			/*
			 * Option "-p" not present. Instead "-passfile"
			 * is present. Get dirpath and passfile value
			 * from MQ Configuration Properties file.
			 */
			(void) sprintf(configfile, "%s/props/config.properties",
				confdirs->str_array[0]);
			rc = get_configprop_value(configfile,
				"imq.passfile.dirpath", dirpath);
			if (rc != 0) {
				if (print_messages)
					(void) fprintf(stderr,
						gettext("Unable to determine "
						"dirpath for passfile."));
				goto finished;
			} else {
				rc = get_configprop_value(configfile,
					"imq.passfile.name", passfile);
				if (rc != 0) {
					(void) fprintf(stderr,
						gettext("Unable to determine "
						"passfile for broker %s."),
						broker_name);
					goto finished;
				}
			}
			if (dirpath != NULL && passfile != NULL) {
				err = ds_validate_file(print_messages, 0,
					"%s/%s", dirpath, passfile);
				if (err != SCHA_ERR_NOERR) {
					rc = 1;
					goto finished;
				}
			}
		}
	}

finished:

	if (broker_name != NULL) free(broker_name);
	if (broker_user != NULL) free(broker_user);
	if (password != NULL) free(password);

	/*
	 * We do not free any of the properties allocated because
	 * doing so caused a problem on a previous version of cluster.
	 *
	 * s1mq_svc_start is a short lived process and the properties
	 * are not allocated repeatedly so this does not pose a
	 * serious memory leak problem.
	 */

	return (rc); /* return result of validation */
}

/*
 * svc_start():
 *
 */

int
svc_start(scds_handle_t scds_handle)
{
	int			rc = 0;
	char			*cmd = NULL;
	scha_str_array_t	*confdirs = NULL;
	char			*broker_name = NULL;
	char			*varhome = NULL;

	scds_syslog(LOG_NOTICE, "Starting %s.", APP_NAME);

	rc = get_string_ext_property(scds_handle, "Broker_Name",
		&broker_name);
	if (broker_name == NULL) {
		rc = 1;
		/*
		 * SCMSGS
		 * @explanation
		 * The indicated property must be set by the user.
		 * @user_action
		 * Use scrgadm to set the property.
		 */
		scds_syslog(LOG_ERR, "Extension property %s must be set.",
			"Broker_Name");
		goto finished;
	}

	confdirs = scds_get_ext_confdir_list(scds_handle);
	varhome = strip_path_elements(confdirs->str_array[0], 2);
	if (varhome == NULL) {
		ds_internal_error("Unable to construct IMQ_VARHOME.");
		rc = 1;
		goto finished;
	}

	/*
	 * Build the imqbrokerd command and start under PMF.
	 *
	 * We could pass IMQ_VARHOME using scds_pmf_start_env()
	 * which is available only in the latest versions of the
	 * Sun Cluster. For backward compatibility reasons we don't
	 * use scds_pmf_start_env() and do it the following way.
	 */
	cmd = ds_string_format("/bin/sh -c 'IMQ_VARHOME=%s; "
		"export IMQ_VARHOME; "
		"/usr/bin/imqbrokerd -silent -name %s'",
		varhome, broker_name);
	if (cmd == NULL) {
		rc = 1;
		ds_internal_error("unable to create command string to "
			"start imqbrokerd");
		goto finished;
	}

	scds_syslog_debug(DBG_LEVEL_HIGH, "The command <%s> is used to "
		"start %s under pmf", cmd, APP_NAME);

	rc = scds_pmf_start(scds_handle, SCDS_PMF_TYPE_SVC, 0, cmd, -1);

	if (rc == SCHA_ERR_NOERR) {
		scds_syslog(LOG_NOTICE,
			"Start of %s completed successfully.", cmd);
	} else {
		scds_syslog(LOG_ERR, "Failed to start %s.", cmd);
		goto finished;
	}

finished:

	if (cmd != NULL) free(cmd);
	/*
	 * Do free allocated properties.  See comment at end of
	 * svc_validate.
	 */

	return (rc); /* return Success/failure status */
}

/*
 * svc_stop():
 *
 * Stop the s1mq server
 * Return 0 on success, > 0 on failures.
 *
 */
int
svc_stop(scds_handle_t scds_handle)
{
	int rc = 0, cmd_exit_code = 0;
	int stop_smooth_timeout =
		(scds_get_rs_stop_timeout(scds_handle) * SVC_SMOOTH_PCT)
		/ 100;
	char *cmd = NULL;
	char *cmdlog = NULL;

	scha_str_array_t	*confdirs = NULL;
	char			*broker_name, *broker_user;
	char			*password = NULL;
	scds_netaddr_list_t	*netaddr = NULL;
	char			*hostname = NULL;
	int			port;
	boolean_t		smooth;
	char			optionscmd[SCDS_ARRAY_SIZE];
	char			internal_err_str[SCDS_ARRAY_SIZE];
	char			configfile[MAXPATHLEN+1];
	char			dirpath[MAXPATHLEN+1];
	char			passfile[MAXPATHLEN+1];

	scds_syslog(LOG_NOTICE, "Stopping %s.", APP_NAME);

	/*
	 * First take the command out of PMF monitoring, so that it
	 * doesn't keep restarting it.
	 */
	rc = scds_pmf_stop_monitoring(scds_handle,
		SCDS_PMF_TYPE_SVC, 0);
	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to take "
			"the resource out of PMF control. "
			"Sending SIGKILL now.");
		goto send_kill;
	}

	(void) get_boolean_ext_property(scds_handle, "Smooth_shutdown",
		&smooth);

	if (smooth) {

		confdirs = scds_get_ext_confdir_list(scds_handle);

		(void) get_string_ext_property(scds_handle, "Broker_Name",
			&broker_name);
		if (broker_name == NULL)
			goto send_kill;	/* error already logged */

		(void) get_string_ext_property(scds_handle, "Broker_User",
			&broker_user);
		if (broker_user == NULL)
			goto send_kill;

		/*
		 * Get the ip addresses available for this resource.
		 * If there are problems doing that skip to send_kill:
		 */
		if (scds_get_netaddr_list(scds_handle, &netaddr)) {
			scds_syslog(LOG_ERR,
				"No network address resource in resource "
				"group.");
			goto send_kill;
		}

		if (netaddr == NULL || netaddr->num_netaddrs == 0) {
			scds_syslog(LOG_ERR,
				"No network address resource in resource "
				"group.");
			goto send_kill;
		}

		hostname = netaddr->netaddrs[0].hostname;
		port = netaddr->netaddrs[0].port_proto.port;

		/*
		 * Check if -p option is available for the imq server
		 * by executing the imqcmd. From IMQ 4.1 onwards, -p
		 * option for password is not supported. Instead
		 * -passfile is being used.
		 */
		rc = snprintf(optionscmd, sizeof (optionscmd),
			"/usr/bin/imqcmd | /bin/grep \"%s\" >/dev/null 2>&1",
			IMQ_PASSFILE_OPTION);
		if (rc == -1) {
			(void) sprintf(internal_err_str,
				"Failed to form command to retrieve the"
				"passfile option for mq server");
			scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
				internal_err_str);
			goto send_kill;
		}

		rc = system(optionscmd);
		if (rc == -1) {
			scds_syslog(LOG_ERR, "Cannot execute %s: %s.",
			    /*LINTED*/
			    optionscmd, strerror(errno));
			goto send_kill;
		}
		if ((!WIFEXITED(rc)) || WIFSIGNALED(rc)) {
			scds_syslog(LOG_ERR, "%s failed to complete",
			    optionscmd);
			goto send_kill;
		}
		rc = WEXITSTATUS((uint_t)rc);
		if (rc != 0) {
			/* grep returns 0 or 1 */
			if (rc < 0) {
				scds_syslog(LOG_ERR, "Cannot execute %s: %s.",
					optionscmd, strerror(errno));
				goto send_kill;
			}
			if (rc > 1) {
				scds_syslog_debug(DBG_LEVEL_HIGH,
					"rc = %d %s failed to complete.", rc,
					optionscmd);
				goto send_kill;
			}
		}
		/*
		 * If -p option is available, build imqcmd using -p
		 * else use -passfile option.
		 */

		if (rc == 0) {
			/*
			 * The version is 3.7 or below.
			 * Make sure that scs1mqconfig exists if smooth
			 */
			password = get_mq_password(confdirs->str_array[0]);
			if (password == NULL) {
				/*
				 * SCMSGS
				 * @explanation
				 * The STOP method was unable to determine what
				 * the password was to shutdown the broker.
				 * The STOP method will send SIGKILL to
				 * shut it down.
				 * @user_action
				 * Check that the scs1mqconfig file is
				 * accessible and correctly specifies
				 * the password.
				 */
				scds_syslog(LOG_ERR, "Unable to determine "
					"password for broker %s. Sending "
					"SIGKILL now.", broker_name);
				goto send_kill;
			}
			/*
			 * Build imqcmd and run it using hatimerun to attempt
			 * a smooth shutdown of the message broker.
			 */
			cmd = ds_string_format("/usr/bin/imqcmd shutdown bkr"
				" -s -b %s:%d -u %s -p %s -f", hostname, port,
				broker_user, password);
			cmdlog = ds_string_format("/usr/bin/imqcmd shutdown bkr"
				" -s -b %s:%d -u %s -p %s -f", hostname, port,
				broker_user, "[pass]");
		} else if (rc == 1) {
			/*
			 * Option "-p" not present. Instead "-passfile"
			 * is present.Get dirpath and passfile value
			 * from MQ Configuration Properties file.
			 */
			(void) sprintf(configfile, "%s/props/config.properties",
				confdirs->str_array[0]);
			rc = get_configprop_value(configfile,
				"imq.passfile.dirpath", dirpath);
			if (rc != 0) {
				/*
				 * SCMSGS
				 * @explanation
				 * The STOP method was unable to
				 * find the passfile to shutdown
				 * the broker. The STOP method will send
				 * SIGKILL to shutdown.
				 * @user_action
				 * Check the passfile configuration in
				 * config.properties.
				 */
				scds_syslog(LOG_ERR, "Unable to determine "
					"dirpath. Sending SIGKILL now.");
				goto send_kill;
			} else {
				rc = get_configprop_value(configfile,
					"imq.passfile.name", passfile);
				if (rc != 0) {
					/*
					 * SCMSGS
					 * @explanation
					 * The STOP method was unable to
					 * determine the passfile to shutdown
					 * the broker. The STOP method will send
					 * SIGKILL to shutdown.
					 * @user_action
					 * Check the passfile configuration in
					 * config.properties.
					 */
					scds_syslog(LOG_ERR,
						"Unable to determine passfile"
						" for broker %s. Sending "
						"SIGKILL now.", broker_name);
					goto send_kill;
				}
			}

			/*
			 * Build imqcmd and run it using hatimerun to attempt
			 * a smooth shutdown of the message broker.
			 */
			if (dirpath != NULL && passfile != NULL) {
				cmd = ds_string_format("/usr/bin/imqcmd "
					"shutdown bkr -s -b %s:%d -u %s "
					"-passfile %s/%s -f", hostname, port,
					broker_user, dirpath, passfile);
				cmdlog = ds_string_format("/usr/bin/imqcmd "
					"shutdown bkr -s -b %s:%d -u %s "
					"-passfile %s/%s -f", hostname,
					port, broker_user, dirpath,
					passfile);
			}
		}

		if (cmd == NULL) {
			scds_syslog(LOG_ERR, "Unable to compose %s path. "
				"Sending SIGKILL now.", "imqcmd");
			goto send_kill;
		}

		if (cmdlog != NULL) {
			scds_syslog_debug(DBG_LEVEL_HIGH,
				"Shutting down S1MQ with the command: %s",
				cmdlog);
		}

		/*
		 * First try to stop the application using the stop command
		 * provided.
		 */
		rc = scds_timerun(scds_handle, cmd, stop_smooth_timeout,
			SIGKILL, &cmd_exit_code);

		if (rc != 0 || cmd_exit_code != 0) {
			scds_syslog(LOG_ERR,
				"The stop command <%s> failed to stop the "
				"application. Will now use SIGKILL to stop "
				"the application.",  cmd);
		}

	} else {		/*  not smooth */
		if ((rc = scds_pmf_stop(scds_handle, SCDS_PMF_TYPE_SVC,
			0, SIGTERM, stop_smooth_timeout)) !=
			SCHA_ERR_NOERR) {
			scds_syslog(LOG_ERR, "Failed to stop %s.", APP_NAME);
		}
	}


	/*
	 * Regardless of whether the command or SIGTERM succeeded send
	 * SIGKILL to the pmf tag. This will ensure that the process
	 * tree goes away if it still exists. If it doesn't exist by
	 * then, we return NOERR.
	 */

send_kill:

	/*
	 * Notice that this call will return with success, even if the
	 * tag does not exist by now.
	 *
	 * Timeout of FINAL_STOP_TIMEOUT will wait until PMF succeeds
	 * or we are timed out by RGM.
	 */
	if ((rc = scds_pmf_stop(scds_handle, SCDS_PMF_TYPE_SVC, 0,
		SIGKILL, FINAL_STOP_TIMEOUT)) != SCHA_ERR_NOERR) {
		/*
		 * Failed to stop the application even with SIGKILL.
		 */
		scds_syslog(LOG_ERR,
			"Failed to stop the application with SIGKILL. "
			"Returning with failure from stop method.");
	}

	if (password != NULL) free(password);
	if (cmd != NULL) free(cmd);
	if (cmdlog != NULL) free(cmdlog);

	/*
	 * Do not free allocated properties.  See comment at end of
	 * svc_validate.
	 */

	if (rc == SCHA_ERR_NOERR)
		scds_syslog(LOG_NOTICE,
			"Successfully stopped the application");

	return (rc);
}

/*
 * svc_wait():
 *
 * wait for the data service to start up fully and make sure it is running
 * healthy
 */

int
svc_wait(scds_handle_t scds_handle)
{
	int			rc = 0;
	int			svc_start_timeout, probe_timeout;
	scds_netaddr_list_t	*netaddr;
	scds_pmf_status_t	status;
	scha_err_t		err;

	/* obtain the network resource to use for probing */
	if (scds_get_netaddr_list(scds_handle, &netaddr)) {
		scds_syslog(LOG_ERR,
			"Error retrieving network address resource in "
			"resource group.");
		return (1);
	}

	/* Return an error if there are no network resources */
	if (netaddr == NULL || netaddr->num_netaddrs == 0) {
		scds_syslog(LOG_ERR,
			"No network address resource in resource group.");
		return (1);
	}

	/*
	 * Get the Start method timeout, port number on which to probe,
	 * the Probe timeout value
	 */
	svc_start_timeout = scds_get_rs_start_timeout(scds_handle);
	probe_timeout = scds_get_ext_probe_timeout(scds_handle);

	scds_syslog(LOG_NOTICE, "Waiting for %s to startup", APP_NAME);

	/*
	 * sleep for SVC_WAIT_PCT percentage of start_timeout time
	 * before actually probing the dataservice. This is to allow
	 * the dataservice to be fully up in order to reply to the
	 * probe. NOTE: the value for SVC_WAIT_PCT could be different
	 * for different dataservices.
	 */
	if (scds_svc_wait(scds_handle, (svc_start_timeout * SVC_WAIT_PCT)/100)
		!= SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to start service.");
		rc = 1;
		goto finished;
	}

	do {
		/*
		 * probe the data service on the IP address of the
		 * network resource and the portname
		 */
		rc = svc_probe(scds_handle,
			netaddr->netaddrs[0].hostname,
			netaddr->netaddrs[0].port_proto.port, probe_timeout);
		if (rc == SCHA_ERR_NOERR) {
			/* Success. Free up resources and return */
			scds_free_netaddr_list(netaddr);
			rc = SCHA_ERR_NOERR;
			goto finished;
		}

		err = scds_pmf_get_status(scds_handle,
			SCDS_PMF_TYPE_SVC, SCDS_PMF_SINGLE_INSTANCE, &status);
		if (err != SCHA_ERR_NOERR) {
			scds_syslog(LOG_ERR,
				"Failed to retrieve process monitor "
				"facility tag.");
			rc = 1;
			goto finished;
		}

		/* Check if the dataservice is still up and running */
		if (status != SCDS_PMF_MONITORED) {
			scds_syslog(LOG_ERR,
			    "Application failed to stay up. "
			    "Start method Failure.");
			rc = 1;
			goto finished;
		}

		/*
		 * Dataservice is still trying to come up. Sleep for a while
		 * before probing again.
		 */
		scds_syslog(LOG_NOTICE, "Waiting for %s to startup", APP_NAME);
		if (scds_svc_wait(scds_handle, SVC_WAIT_TIME)
			!= SCHA_ERR_NOERR) {
			scds_syslog(LOG_ERR, "Failed to start service.");
			return (1);
		}

	/* We rely on RGM to timeout and terminate the program */
	} while (1);

finished:

	return (rc);
}

/*
 * This function starts the fault monitor for a s1mq resource.
 * This is done by starting the probe under PMF. The PMF tag
 * is derived as <RG-name,RS-name,instance_number.mon>. The restart option
 * of PMF is used but not the "infinite restart". Instead
 * interval/retry_time is obtained from the RTR file.
 */

int
mon_start(scds_handle_t scds_handle)
{
	scha_err_t	err;

	scds_syslog_debug(DBG_LEVEL_HIGH,
		"Calling MONITOR_START method for resource <%s>.",
		scds_get_resource_name(scds_handle));

	/*
	 * The probe s1mq_probe is assumed to be available in the same
	 * subdirectory where the other callback methods for the RT are
	 * installed. The last parameter to scds_pmf_start denotes the
	 * child monitor level. Since we are starting the probe under PMF
	 * we need to monitor the probe process only and hence we are using
	 * a value of 0.
	 */

	err = scds_pmf_start(scds_handle, SCDS_PMF_TYPE_MON,
		SCDS_PMF_SINGLE_INSTANCE, "s1mq_probe", 0);

	if (err != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to start fault monitor.");
		return (err);
	}

	scds_syslog(LOG_INFO, "Started the fault monitor.");

	return (SCHA_ERR_NOERR); /* Successfully started Monitor */
}


/*
 * This function stops the fault monitor for a s1mq resource.
 * This is done via PMF. The PMF tag for the fault monitor is
 * constructed based on <RG-name_RS-name,instance_number.mon>.
 */

int
mon_stop(scds_handle_t scds_handle)
{
	scha_err_t	err;

	scds_syslog_debug(DBG_LEVEL_HIGH, "Calling scds_pmf_stop method");

	/*
	 * Timeout of FINAL_STOP_TIMEOUT will cause scds_pmf_stop to
	 * wait until success or RGM it times out.
	 */
	err = scds_pmf_stop(scds_handle, SCDS_PMF_TYPE_MON,
		SCDS_PMF_SINGLE_INSTANCE, SIGKILL,
		FINAL_STOP_TIMEOUT);

	if (err != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to stop fault monitor.");
		return (err);
	}

	scds_syslog(LOG_INFO, "Stopped the fault monitor.");

	return (SCHA_ERR_NOERR); /* Successfully stopped monitor */
}


/*
 * svc_probe(): Do data service specific probing. Return a float value
 * between 0 (success) and 100(complete failure).
 *
 * We do a connect/read/disconnect from the S1MQ port which prints a
 * list of services and the ports they are listening on:
 *
 *	$ /pkg/local/bin/nc dixon-17-1 7676
 *	101 mq1 3.0.1
 *	admin tcp ADMIN 44284
 *	portmapper tcp PORTMAPPER 7676
 *	jms tcp NORMAL 44283
 *	.
 *
 * We do not look at the contents, a read of anything is considered
 * successful.
 */
int
svc_probe(scds_handle_t scds_handle, char *hostname, int port, int timeout)
{
	int	rc = 0;
	int	probe_status = 0;
	ulong_t	t1, t2;
	int 	sock;
	int 	time_used, time_remaining;
	long	connect_timeout;
	char	buf[DS_ARRAY_SIZE];
	size_t	size;

	/*
	 * Use the SVC_CONNECT_TIMEOUT_PCT percentage of timeout
	 * to connect to the port
	 */
	connect_timeout = (SVC_CONNECT_TIMEOUT_PCT * timeout)/100;
	t1 = (ulong_t)(gethrtime()/1E9);

	/*
	 * the probe makes a connection to the specified hostname and port.
	 * The connection is timed for 95% of the actual probe_timeout.
	 */
	rc = scds_fm_tcp_connect(scds_handle, &sock, hostname, port,
		connect_timeout);
	if (rc) {
		scds_syslog(LOG_ERR,
			"Failed to connect to the host <%s> and port <%d>.",
			hostname, port);
		/* this is a complete failure */
		probe_status = SCDS_PROBE_COMPLETE_FAILURE;
		goto finished;
	}

	t2 = (ulong_t)(gethrtime()/1E9);

	/*
	 * Compute the actual time it took to connect. This should be
	 * less than or equal to connect_timeout, the time allocated
	 * to connect.  If the connect uses all the time that is
	 * allocated for it, then the remaining value from the
	 * probe_timeout that is passed to this function will be used
	 * as disconnect timeout. Otherwise, the the remaining time
	 * from the connect call will also be added to the disconnect
	 * timeout.
	 */

	time_used = (int)(t2 - t1);

	/*
	 * Use the remaining time(timeout - time_took_to_connect) to
	 * disconnect
	 */

	time_remaining = timeout - (int)time_used;

	/*
	 * If all the time is used up, use a small hardcoded timeout
	 * to still try to disconnect. This will avoid the fd leak.
	 */
	if (time_remaining <= 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * The probe timed out connecting to the application.
		 * @user_action
		 * If the problem persists investigate why the application is
		 * responding slowly or if the Probe_timeout property needs to
		 * be increased.
		 */
		scds_syslog(LOG_ERR,
			"svc_probe used entire timeout of "
			"%d seconds during connect operation and exceeded "
			"the timeout by %d seconds. Attempting disconnect "
			"with timeout %d ", connect_timeout,
			abs(time_used), SVC_DISCONNECT_TIMEOUT_SECONDS);

		time_remaining = SVC_DISCONNECT_TIMEOUT_SECONDS;

		goto disconnect;
	}

	/* Loop until we read something */
	do {
		t2 = (ulong_t)(gethrtime()/1E9);
		time_used = (int)(t2 - t1);
		time_remaining = timeout - time_used;
		if (time_remaining < 1) {
			/*
			 * All the time is used up.  Use a small
			 * hardcoded timeout to disconnect to avoid
			 * the fd leak.
			 */
			/*
			 * SCMSGS
			 * @explanation
			 * The probe timed out while reading from the
			 * application.
			 * @user_action
			 * If the problem persists investigate why the
			 * application is responding slowly or if the
			 * Probe_timeout property needs to be increased.
			 */
			scds_syslog(LOG_ERR,
				"svc_probe used entire timeout of %d "
				"seconds during read operation and exceeded "
				"the timeout by %d seconds. Attempting "
				"disconnect with timeout %d ",
				connect_timeout, abs(time_used),
				SVC_DISCONNECT_TIMEOUT_SECONDS);
			probe_status = SCDS_PROBE_COMPLETE_FAILURE/2;
			goto disconnect;
		}

		size = sizeof (buf);
		rc = scds_fm_tcp_read(scds_handle, sock, buf, &size,
			time_remaining);
		scds_syslog_debug(DBG_LEVEL_HIGH,
			"Received %d bytes from the server at %s:%d",
			size, hostname, port);

		/*
		 * Be lenient on read errors, allow 2 failures.
		 */
		if (rc != SCHA_ERR_NOERR) {
			scds_syslog(LOG_ERR, "Failed to communicate with "
				"server %s port %d: %s.",
				hostname, port, scds_error_string(rc));

			probe_status += SCDS_PROBE_COMPLETE_FAILURE/2;

			if (probe_status >= SCDS_PROBE_COMPLETE_FAILURE) {
				t2 = (ulong_t)(gethrtime()/1E9);
				time_used = (int)(t2 - t1);
				time_remaining = timeout - time_used;
				if (time_remaining < 1)
					time_remaining =
						SVC_DISCONNECT_TIMEOUT_SECONDS;

				goto disconnect;
			}
		}
	} while (size < 1);

disconnect:

	/*
	 * Return partial failure in case of disconnection failure.
	 * Reason: The connect call is successful, which means
	 * the application is alive. A disconnection failure
	 * could happen due to a hung application or heavy load.
	 * If it is the later case, don't declare the application
	 * as dead by returning complete failure. Instead, declare
	 * it as partial failure. If this situation persists, the
	 * disconnect call will fail again and the application will be
	 * restarted.
	 */
	rc = scds_fm_tcp_disconnect(scds_handle, sock, time_remaining);
	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		    "Failed to disconnect from port %d of resource %s.",
		    port, scds_get_resource_name(scds_handle));
		/* this is a partial failure */
		probe_status += SCDS_PROBE_COMPLETE_FAILURE/2;
		goto finished;
	}

	t2 = (ulong_t)(gethrtime()/1E9);
	time_used = (int)(t2 - t1);
	time_remaining = timeout - time_used;

	/*
	 * If there is no time left return
	 * SCDS_PROBE_COMPLETE_FAILURE/2. This will make sure that if
	 * this timeout persists, server will be restarted.
	 */
	if (time_remaining <= 0) {
		scds_syslog(LOG_ERR, "Probe timed out.");
		probe_status += SCDS_PROBE_COMPLETE_FAILURE/2;
		goto finished;
	}

finished:

	return (probe_status);
}

/*
 * get_configprop_value(): Retrieve the value of var from
 * config.properties file.
 *
 * Understands just enough to get the value of a shell variable from a
 * line like: FOO=bar.  We account for leading whitespace and trailing
 * verbiage such as a comment or compound statement, but no guarantees
 * for anything tricky such as conditionally setting values.
 *
 * Assumes the string var is less then or equal to SCDS_ARRAY_SIZE and
 * that value points to a buffer at least MAXPATHLEN+1 in length.
 *
 * Returns 0 on success or 1 on failure.
 */
int
get_configprop_value(const char *configfile, const char *var, char *value)
{
	FILE    *fp = NULL;
	/*
	* line[] size is MAXPATHLEN + string terminator +
	* SCDS_ARRAY_SIZE + terminator and space for the '='.
	*/
	char    line[MAXPATHLEN+1 + SCDS_ARRAY_SIZE+1 + 1];
	char    *s;
	char    *start;
	char    *first;

	fp = fopen(configfile, "r");
	if (fp == NULL) {
		scds_syslog(LOG_ERR,
			"File %s is not readable: %s.",
			configfile, strerror(errno));
		return (1);
	}

	while (!feof(fp)) {
		/*
		* fgets() expects 2nd arg to be int, however sizeof ()
		* is long in 64-bit. Suppressing lint 747
		* "Significant prototype coercion" as a line is not
		* going to be longer then INT_MAX.
		*/
		s = fgets(line, sizeof (line), fp);	/*lint !e747 */
		if (s == NULL && !feof(fp)) {

			/*
			* SCMSGS
			* @explanation
			* The specified mq configuration file does not
			* configure the specified variable.
			* @user_action
			* Edit the configuration file and set the specified
			* variable to the correct value.
			*/
			scds_syslog(LOG_ERR,
			"MQ service with configuration file <%s> "
			"does not configure %s.\n", configfile, var);

			(void) fclose(fp);
			return (1);
		}

		/* Skip leading whitespace */
		start = line + strspn(line, " \t");
		first = strtok(start, "=");

		if (strcasecmp(var, first) == 0) {
			/*
			* Get the value.  The delimiter string is an
			* attempt to weed out any trailing comments,
			* statements, or other verbage but no
			* promises.
			*/
			if ((s = strtok(NULL, " \t\n;#")) == NULL) {
				/*
				* Nothing left on line
				* Just continue, maybe there is another
				* assignment, if not we fall out of the loop
				* at EOF and we complain about "does not
				* configure..."
				*/
				continue;
			}

			if (strlcpy(value, s, MAXPATHLEN+1) >= MAXPATHLEN+1) {
				char *fmt = "String handling error getting "
				"value from %s. The value may be "
				"too long";
				char buf[sizeof (fmt) + MAXPATHLEN+1];
				(void) snprintf(buf, sizeof (buf), fmt,
					configfile);

				scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
					buf);
				(void) fclose(fp);
				return (1);
			}

			scds_syslog_debug(DBG_LEVEL_HIGH,
				"MQ service with configuration file <%s> "
				"has %s set as %s\n", configfile, var, value);

			(void) fclose(fp);
			return (0);
		}
	}

	(void) fclose(fp);
	scds_syslog(LOG_ERR,
		"MQ service with configuration file <%s> "
		"does not configure %s.\n", configfile, var);

	return (1);
}

/*
 * Return the password for broker_name, or NULL if there is an error.
 *
 * User must free returned string.
 */
char *
get_mq_password(char *confdir)
{
	char	password[MAXPATHLEN+1];
	char	*config_filename;

	config_filename = ds_string_format("%s/scs1mqconfig", confdir);
	if (config_filename == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * Unable to construct the path to the indicated file.
		 * @user_action
		 * Check system log messages to determine the underlying
		 * problem.
		 */
		scds_syslog(LOG_ERR, "Unable to compose %s path.",
			"scs1mqconfig");
		return (NULL);
	}

	if (ds_get_magnus_value(config_filename, "Password", password) == 0)
		return (strdup(password));
	else
		return (NULL);
}

/*
 * Retrieve a string extension property.  On success return
 * SCHA_ERR_NOERR.  The returned string value may be NULL on success,
 * it will always be NULL if an error occured.
 *
 * The caller is responsible for freeing the returned string which is
 * duplicated from the underlying extension property.
 *
 * Please note the underlying extension property is not freed.  This
 * is a memory leak but freeing it caused a problem on a previous
 * version of cluster. This is normally OK since methods are short
 * lived so the leak is manageable. The exception is the probe - be
 * careful not to call this function in the probe's loop.
 */
scha_err_t
get_string_ext_property(scds_handle_t scds_handle, char *prop_name,
	char **stringval)
{
	int			rc;
	scha_extprop_value_t	*extprop = NULL;

	rc = scds_get_ext_property(scds_handle, prop_name,
		SCHA_PTYPE_STRING, &extprop);

	if (rc != SCHA_ERR_NOERR || extprop == NULL) {
		scds_syslog(LOG_ERR,
			"Failed to retrieve the property %s: %s.",
			prop_name, scds_error_string(rc));
		*stringval = NULL;
		return (rc);
	}

	if (extprop->val.val_str == NULL) {
		scds_syslog(LOG_INFO,
			"Extension property <%s> has a value of <%s>",
			prop_name, "NULL");
		*stringval = NULL;
	} else {
		*stringval = strdup(extprop->val.val_str);
		if (*stringval == NULL) {
			rc = SCHA_ERR_NOMEM;
			scds_syslog(LOG_ERR, "Out of memory.");
		} else
			scds_syslog(LOG_INFO,
				"Extension property <%s> has a value of "
				"<%s>", prop_name, *stringval);
	}

	/*
	 * Freeing extprop would cause problems on a previous version
	 * of cluster so don't.  Also don't run this function in
	 * loop.
	 */
	return (rc);
}

/*
 * Similiar to get_string_ext_property, but instead fetch a boolean.
 */
scha_err_t
get_boolean_ext_property(scds_handle_t scds_handle, char *prop_name,
	boolean_t *boolval)
{
	int			rc;
	scha_extprop_value_t	*extprop = NULL;

	rc = scds_get_ext_property(scds_handle, prop_name,
		SCHA_PTYPE_BOOLEAN, &extprop);

	if (rc != SCHA_ERR_NOERR || extprop == NULL) {
		scds_syslog(LOG_ERR,
			"Failed to retrieve the property %s: %s.",
			prop_name, scds_error_string(rc));
		/*
		 * In case the error return is ignored set the boolean
		 * to something hopefully benign.
		 */
		*boolval = B_FALSE;
		return (rc);
	}

	*boolval = extprop->val.val_boolean;

	scds_syslog(LOG_INFO,
		"Extension property <%s> has a value of <%s>",
		prop_name, ((*boolval == B_FALSE) ? "B_FALSE" : "B_TRUE"));

	return (SCHA_ERR_NOERR);
}


/*
 * Do HASP validation.
 *
 * Returns
 *	-1	error
 *	0	OK, but storage not online on this node
 *	1	OK, storage online on this node.  Do filesystem checks.
 */
int
validate_hasp(int print, scds_handle_t handle)
{
	int				err, rc = 0;
	scds_hasp_status_t		hasp_status;

	err = scds_hasp_check(handle, &hasp_status);

	if (err != SCHA_ERR_NOERR) {
		/*
		 * scha_hasp_check() logs a message to syslog when it
		 * fails
		 */
		if (print) {
			(void) fprintf(stderr,
				gettext("INTERNAL ERROR: %s.\n"),
				gettext("scds_hasp_check failed"));
		}
		/*
		 * validation has failed for this resource
		 */
		return (-1);
	}

	switch (hasp_status) {

	case SCDS_HASP_NO_RESOURCE:
		/*
		 * We do not depend on any SUNW.HAStoragePlus resources
		 */
		scds_syslog(LOG_INFO,
			"This resource does not depend on any "
			"SUNW.HAStoragePlus resources. Proceeding with "
			"normal checks.");
		rc = 1;
		break;

	case SCDS_HASP_ERR_CONFIG:
		/*
		 * Configuration error, SUNW.HAStoragePlus resource is
		 * in a different RG. Fail the validation.
		 */
		scds_syslog(LOG_ERR,
			"One or more of the SUNW.HAStoragePlus resources "
			"that this resource depends on is in a different "
			"resource group. Failing validate method "
			"configuration checks.");
		if (print) {
			(void) fprintf(stderr,
				gettext("One or more of the "
					"SUNW.HAStoragePlus resources that "
					"this resource depends on is in a "
					"different resource "
					"group. Failing validate method "
					"configuration checks."));
		}
		rc = -1;
		break;

	case SCDS_HASP_NOT_ONLINE:
		/*
		 * There is at least one SUNW.HAStoragePlus resource not
		 * online anywhere.
		 */
		scds_syslog(LOG_ERR,
			"One or more of the SUNW.HAStoragePlus resources "
			"that this resource depends on is not online "
			"anywhere. Failing validate method.");
		if (print) {
			(void) fprintf(stderr,
				gettext("One or more of the "
					"SUNW.HAStoragePlus resources that "
					"this resource depends on is not "
					"online anywhere. "
					"Failing validate method."));
		}
		rc = -1;
		break;

	case SCDS_HASP_ONLINE_NOT_LOCAL:
		/*
		 * Not all SUNW.HAStoragePlus we need, are online locally.
		 */
		scds_syslog(LOG_INFO,
			"All the SUNW.HAStoragePlus resources that this "
			"resource depends on are not online on the local "
			"node. Skipping the checks for the existence "
			"and permissions of the start/stop/probe commands.");
		rc = 0;
		break;

	case SCDS_HASP_ONLINE_LOCAL:
		/*
		 * All SUNW.HAStoragePlus resources we need are available on
		 * this node.
		 */
		scds_syslog(LOG_INFO,
			"All the SUNW.HAStoragePlus resources that this "
			"resource depends on are online on the local node. "
			"Proceeding with the checks for the existence and "
			"permissions of the start/stop/probe commands.");
		rc = 1;
		break;

	default:
		/* Unknown status code */
		scds_syslog(LOG_ERR, "Unknown status code %d.", hasp_status);
		if (print) {
			(void) fprintf(stderr,
				gettext("Unknown status code %d."),
				hasp_status);
		}
		/*
		 * Since the scds_hasp_check() returned an unknown
		 * status code we return 0 which is "OK, but not
		 * online".
		 */
		rc = 0;
		break;
	}

	return (rc);
}

/*
 * Strip n path elements of the end of an absolute path.  Return a new
 * string containing new path, or NULL if out of memory, path is not
 * absolute or if n > then the number of path elements.
 */

char *
strip_path_elements(char *path, int n)
{
	char *s, *t;
	int slen = 0;

	if (path[0] != '/') {
		scds_syslog_debug(DBG_LEVEL_MED, "strip_path_elements(): "
			"%s is not an absolute path", path);
		return (NULL);
	}

	if (path == NULL) {
		scds_syslog_debug(DBG_LEVEL_MED, "strip_path_elements(): "
			"path is NULL");
		return (NULL);
	}


	if ((s = strdup(path)) == NULL) {
		scds_syslog(LOG_ERR, "Out of memory.");
		return (NULL);
	}

	/* Remove trailing slash if any */
	slen = strlen(s);
	if (s[slen-1] == '/')
		s[slen-1] = '\0';

	for (; n > 0; n--) {
		if ((t = strrchr(s, '/')) == NULL) {
			scds_syslog_debug(DBG_LEVEL_HIGH,
				"strip_path_elements(): can not find /");
			free(s);
			return (NULL);
		}

		*t = '\0';
	}

	if (s[0] == '\0') {	/* Path contained n elements */
		s[0] = '/';
		s[1] = '\0';
	}

	return (s);
}
