/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_S1MQ_H
#define	_S1MQ_H

#pragma ident	"@(#)s1mq.h	1.6	07/06/06 SMI"

#include <limits.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Debug levels for error messages */
#define	DBG_LEVEL_HIGH		9
#define	DBG_LEVEL_MED		5
#define	DBG_LEVEL_LOW		1

#define	SCDS_CMD_SIZE		(8 * 1024)

#define	SCDS_ARRAY_SIZE		1024

/*
 * Application name defined for use in scds_syslog messages.
 * Using this allows identical messages to be defined in more
 * than one data service, hashing to one message explanation.
 */
#define	APP_NAME	"Message Queue"

/*
 * How long to wait for final stop attempt.  SC3.1 supports passing -1
 * to mean wait forever.  In SC3.0 we need to settle for INT_MAX or
 * approximately 68 years.
 *
 * "Forever" is of course until RGM times out the stop method.
 */
#define	FINAL_STOP_TIMEOUT	INT_MAX


int svc_validate(scds_handle_t scds_handle, boolean_t print_messages);

int svc_start(scds_handle_t scds_handle);

int svc_stop(scds_handle_t scds_handle);

int svc_wait(scds_handle_t scds_handle);

int mon_start(scds_handle_t scds_handle);

int mon_stop(scds_handle_t scds_handle);

int svc_probe(scds_handle_t scds_handle, char *hostname, int port,
	int timeout);

scha_err_t get_string_ext_property(scds_handle_t scds_handle,
	char *prop_name, char **stringval);

scha_err_t get_boolean_ext_property(scds_handle_t scds_handle,
	char *prop_name, boolean_t *boolval);

#ifdef __cplusplus
}
#endif

#endif /* _S1MQ_H */
