/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#pragma ident	"@(#)util.c	1.7	07/06/06 SMI"

#include <strings.h>
#include <sys/types.h>
#include <sys/statvfs.h>
#include <sys/stat.h>
#include <pwd.h>
#include <errno.h>
#include <libintl.h>
#include <rgm/libdsdev.h>

#define	SCDS_CMD_SIZE		(8*1024)


int
validate_permissions(char *rootdir, char *cmd, uid_t u_uid, gid_t u_gid,
	scds_hasp_status_t hasp_status, boolean_t print_msgs)
{
	struct stat statbuf;
	struct statvfs statvfsbuf;
	char filepath[SCDS_CMD_SIZE] = "";
	int rc = 0;

	if (rootdir == NULL || cmd == NULL)
		return (1);

	rc = snprintf(filepath, sizeof (filepath),
		"%s/bin/%s", rootdir, cmd);
	if (rc == -1) {
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
			"String handling error creating path to "
			"server root dir. The path may be too long");
		if (print_msgs)
			(void) fprintf(stderr,
				gettext("INTERNAL ERROR: %s.\n"),
				gettext("String handling error creating "
				"path to server root dir. "
				"The path may be too long"));
		return (1);
	}

	/* statvfs does not return cached status, but the actual one */
	if (statvfs(filepath, &statvfsbuf) != 0) {
		if ((hasp_status == SCDS_HASP_ONLINE_NOT_LOCAL) &&
			(errno == ENOENT)) {	/*lint !e746 */
			return (0);
		} else {
			/*
			 * SCMSGS
			 * @explanation
			 * The rgmd has failed in an attempt to stat(2) a file
			 * used for the anti-"pingpong" feature. This may
			 * prevent the anti-pingpong feature from working,
			 * which may permit a resource group to fail over
			 * repeatedly between two or more nodes. The failure
			 * to access the file might indicate a more serious
			 * problem on the node.
			 * @user_action
			 * Examine other syslog messages occurring around the
			 * same time on the same node, to see if the source of
			 * the problem can be identified.
			 */
			scds_syslog(LOG_ERR,
				"Cannot access file <%s>, err = <%s>",
				filepath, strerror(errno)); /*lint !e746 */
			if (print_msgs)
				(void) fprintf(stderr,
					gettext("Cannot access file <%s>, "
					"err = <%s>\n"),
					filepath, gettext(
					strerror(errno))); /*lint !e746 */
			return (1);
		}
	}

	rc = stat(filepath, &statbuf);

	if (rc != 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * There was a failure to stat the specified file.
		 * @user_action
		 * Make sure that the file exists.
		 */
		scds_syslog(LOG_ERR, "stat of file %s failed: <%s>.",
			filepath, strerror(errno)); /*lint !e746 */
		if (print_msgs)
			(void) fprintf(stderr,
				gettext("stat of file %s failed: "
				"<%s>.\n"), filepath,
				gettext(strerror(errno))); /*lint !e746 */
		return (1);
	}

	/* Check if owner has read/execute permissions */
	if (statbuf.st_uid == u_uid) { /* owner */
		if (!(statbuf.st_mode & S_IRUSR|S_IXUSR)) {
			rc = 1;
			scds_syslog(LOG_ERR, "No permission for owner to "
				"execute %s.", filepath);
			if (print_msgs)
				(void) fprintf(stderr,
					gettext("No permission for owner "
					"to execute %s.\n"), filepath);
		}
	} else if (statbuf.st_gid == u_gid) { /* group */
		if (!(statbuf.st_mode & S_IRGRP|S_IXGRP)) {
			rc = 1;
			scds_syslog(LOG_ERR, "No permission for group to "
				"execute %s.", filepath);
			if (print_msgs)
				(void) fprintf(stderr,
					gettext("No permission for group "
					"to execute %s.\n"), filepath);
		}
	} else if (!(statbuf.st_mode & S_IROTH|S_IXOTH)) { /* others */
		rc = 1;
		/*
		 * SCMSGS
		 * @explanation
		 * The specified path does not have the correct permissions as
		 * expected by a program.
		 * @user_action
		 * Set the permissions for the file so that it is readable and
		 * executable by others (world).
		 */
		scds_syslog(LOG_ERR, "No permission for others to "
			"execute %s.", filepath);
		if (print_msgs)
			(void) fprintf(stderr,
				gettext("No permission for others "
				"to execute %s.\n"), filepath);
	} else {
		rc = 0;
	}

	return (rc);
}
