#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#pragma ident	"@(#)probe_sblgtwy.ksh	1.5	07/06/06 SMI"
#
#
# This script is used by the PROBE method for probing Siebel gateway.
# This is also used by START method of Siebel server resource to
# determine whether gateway is up.
#
# Input:
#
# - ROOT - location of Siebel gateway or server installation and
#          siebenv.sh file
#
# Return:
#
# 0 - Success
# 1 - Failure
#

PATH=$PATH:/usr/cluster/lib/sc

echo $0 | grep gateway > /dev/null
rc=$?

if [ $rc -eq 0 ]
then
	syslog_tag="SC[SUNW.sblgtwy,probe_sblgtwy]"
else
	syslog_tag="SC[SUNW.sblsrvr,probe_sblgtwy]"
fi

if [ $# -ne 1 ]
then
	# SCMSGS
	# @explanation
	# Need explanation of this message!
	# @user_action
	# Need a user action for this message.
	scds_syslog -p error -t $syslog_tag -m \
		"INTERNAL ERROR: usage: `basename $0` <gateway_or_server_root>"
	exit 1
fi

ROOT=$1

. $ROOT/siebenv.sh
rc=$?

if [ $rc -ne 0 ]
then
	# SCMSGS
	# @explanation
	# There was an error while attempting to execute (source) the
	# specified file. This may be due to improper permissions, or improper
	# settings in this file.
	# @user_action
	# Please verify that the file has correct permissions. If permissions
	# are correct, verify all the settings in this file. Try to manually
	# source this file in korn shell (". siebenv.sh"), and correct any
	# errors.
	scds_syslog -p error -t $syslog_tag -m \
		"Error while executing siebenv.sh."
	exit 1
fi

srvredit -q -g $SIEBEL_GATEWAY -e none -z -c '$Gateway.VersionString' > /dev/null

exit $?
