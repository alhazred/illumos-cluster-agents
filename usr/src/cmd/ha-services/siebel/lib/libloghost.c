/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2002 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#pragma ident	"@(#)libloghost.c	1.5	07/06/06 SMI"

#include <stdlib.h>
#include <unistd.h>
#include <strings.h>
#include <sys/utsname.h>


/*
 * gethostname() here will return the logical hostname as set in
 * LHOSTNAME environment variable. If this variable is not set,
 * return the name returned by uname() - gethostname() will not
 * work correctly with LD_PRELOAD, when called from here.
 *
 * Upon successful completion, gethostname() returns 0. Otherwise,
 * it returns -1 and sets errno to indicate the error. This
 * behavior will be preserved.
 */
int
gethostname(char *hostname, int namelen)
{
	char *hname = NULL;
	struct utsname name;
	int rc = 0;

	hname = getenv("LHOSTNAME");
	if (hname != NULL) {
		if (strlcpy(hostname, hname, (size_t)namelen)
			>= (size_t)namelen)
			rc = -1;
	} else {
		rc = uname(&name);
		if (rc > 0) {
			/* uname is successful */
			if (strlcpy(hostname, name.nodename,
				(size_t)namelen) >= (size_t)namelen)
				rc = -1;
			else
				rc = 0;
		}
	}

	return (rc); /* always return 0 or -1 */
}
