#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)Makefile	1.13	07/08/15 SMI"
#
# cmd/ha-services/siebel/sblsrvr/Makefile
#

SRCS =	sblsrvr_svc_start.c \
	sblsrvr_svc_stop.c \
	sblsrvr_validate.c \
	sblsrvr_update.c \
	sblsrvr_monitor_start.c \
	sblsrvr_monitor_stop.c \
	sblsrvr_monitor_check.c \
	sblsrvr_probe.c 

OBJS = $(SRCS:%.c=%.o)

CPROG = $(SRCS:%.c=%)

COMMON_SRCS = sblsrvr.c
COMMON_OBJS = $(COMMON_SRCS:%.c=%.o)

UTIL_SRCS = ../common/util.c
UTIL_OBJS = ../common/util.o

KSHSRCS = start_sblsrvr.ksh \
	stop_sblsrvr.ksh \
	probe_sblsrvr.ksh \
	pingsbldb.ksh

CONF = probe_cmds

KSHPROG = $(KSHSRCS:%.ksh=%)

COMMON_PROG = ../gateway/probe_sblgtwy
COMMON_KSHPROG = $(ROOTOPTBIN)/probe_sblgtwy

PROG = $(CPROG) $(KSHPROG)

OBJECTS= $(OBJS) $(COMMON_OBJS)

include ../../../Makefile.cmd

PKGNAME = SUNWscsbl/sblsrvr
RTRFILE = SUNW.sblsrvr

TEXT_DOMAIN = SUNW_SC_SBLSRVR
MESSAGE_DIR = /opt/$(PKGNAME)/lib/locale
PIFILES = $(SRCS:%.c=%.pi) $(COMMON_SRCS:%.c=%.pi) $(UTIL_SRCS:%.c=%.pi)
POFILE = sblsrvr-ds.po

LDLIBS  += -ldsdev -lscha -ldl

LINTFILES= $(SRCS:%.c=%.ln) $(COMMON_SRCS:%.c=%.ln) $(UTIL_SRCS:%.c=%.ln)

.KEEP_STATE:

all: $(COMMON_OBJS) $(PROG) $(ROOTOPTETCRTR)

.PARALLEL: $(PROG)

install: all $(ROOTOPTBINPROG) $(COMMON_KSHPROG) $(ROOTOPTETCCONF)

include ../../../Makefile.targ

$(CPROG): $(COMMON_OBJS) $(UTIL_OBJS) $$(@:%=%.o)
	$(LINK.c) -o $@ $(@:%=%.c) $(COMMON_OBJS) $(UTIL_OBJS) $(LDFLAGS) $(LDLIBS)
	$(POST_PROCESS)

$(UTIL_OBJS): $(UTIL_SRCS)
	$(CC) -c -o $(@:%.o=%.o) $(@:%.o=%.c) $(LDFLAGS) $(CPPFLAGS)
	$(POST_PROCESS_O)

$(KSHPROG): $(KSHSRCS)
	-cp $(@:%=%.ksh) $@
	chmod +x $@

$(COMMON_KSHPROG): $(COMMON_PROG)
	$(RM) $@
	cd $(ROOTOPTBIN)
	$(SYMLINK) ../../gateway/bin/$(COMMON_PROG:../gateway/%=%) $@

clean:
	$(RM) $(PROG) $(OBJS) $(COMMON_OBJS) $(UTIL_OBJS)
