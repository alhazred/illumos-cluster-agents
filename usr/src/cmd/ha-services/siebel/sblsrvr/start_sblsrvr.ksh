#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2004 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#pragma ident	"@(#)start_sblsrvr.ksh	1.7	07/06/06 SMI"
#
#
# This script is used by the START method to start Siebel server.
#
# Input:
#
# - LHOSTNAME    - logical hostname configured for Siebel server
# - SIEBEL_ROOT  - location of Siebel server installation and siebenv.sh
#                  file (server_root)
# - SIEBEL_ENTERPRISE - Siebel enterprise name
# - SIEBSRVR_NAME - Siebel server name
#
# Return:
#
# 0 - Success
# 1 - Failure
#

PATH=$PATH:/usr/cluster/lib/sc
syslog_tag="SC[SUNW.sblsrvr,start_sblsrvr]"

if [ $# -ne 4 ]
then
	scds_syslog -p error -t $syslog_tag -m \
		"INTERNAL ERROR: usage: `basename $0` <logicalhost> <server_root> <siebel_enterprise> <siebel_servername>"
	exit 1
fi

export LHOSTNAME=$1
SERVER_ROOT=$2
SIEBEL_ENTERPRISE=$3
SIEBSRVR_NAME=$4

BASEPATH=`dirname $0`
BASEPATH=`dirname $BASEPATH`
BASEPATH=`dirname $BASEPATH`

. $SERVER_ROOT/siebenv.sh
rc=$?

if [ $rc -ne 0 ]
then
	scds_syslog -p error -t $syslog_tag -m \
		"Error while executing siebenv.sh."
	exit 1
fi

# There is no reliable method to verify if processes were started
# outside Sun Cluster control, or were leftover. Using list_server
# is not sufficient, since it uses PID to verify if the process is
# running. On another node, this PID could be an unrelated active
# process. srvrmgr requires gateway to be up. Therefore, all running
# Siebel server processes will be forced shutdown before a start is
# attempted. For manually starting Siebel server, users are required
# to disable the Sun Cluster Siebel server resource.

#
# Get Siebel server version.
#
# If possible, try to read version from an installed file rather than
# trying to connect to the gateway.
#
if [ -r $SERVER_ROOT/messages/base.txt ]
then
	VERSION=`grep LANG_INDEPENDENT $SERVER_ROOT/messages/base.txt | \
		awk '{print $1}'`
	rc=$?
else
	rc=1
fi

#
# Contact gateway only if above failed to get version.
#
if [ rc -ne 0 ]
then
	VERSION=`srvredit -q -g $SIEBEL_GATEWAY -e $SIEBEL_ENTERPRISE -s $SIEBSRVR_NAME -z -c '$Server.VersionString'` 
	rc=$?

	if [ $rc -ne 0 ]
	then
		scds_syslog -p error -t $syslog_tag -m \
			"Unable to connect to Siebel gateway."

		# Fault monitor will start Siebel server later.
		exit 0
	fi
fi

#
# LD_PRELOAD only if running Siebel 7.0 or 7.5.
#
if [[ "$VERSION" == 7\.0* || "$VERSION" == 7\.5* ]]
then
	export LD_PRELOAD=$BASEPATH/lib/libloghost.so
fi

# Using '-M' option will prevent reset_server from cleaning up shared
# memory resources created by another instance that may be up already,
# or may have just started.

reset_server -e $SIEBEL_ENTERPRISE -M $SIEBSRVR_NAME > /dev/null 2> /dev/null

start_server -e $SIEBEL_ENTERPRISE $SIEBSRVR_NAME > /dev/null 2> /dev/null
rc=$?

exit $rc
