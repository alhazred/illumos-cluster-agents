#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#pragma ident	"@(#)pingsbldb.ksh	1.5	07/10/03 SMI"
#
#
# This script is used by the START method to ping the Siebel database.
#
# Input:
#
# - SIEBEL_ROOT  - location of Siebel server installation and siebenv.sh
#                  file (server_root)
# - SIEBEL_ENTERPRISE - Siebel enterprise name
#
# Return:
#
# 0 - Success
# 1 - Failure
#

PATH=$PATH:/usr/cluster/lib/sc
syslog_tag="SC[SUNW.sblsrvr,pingsbldb]"

if [ $# -ne 2 ]
then
	# SCMSGS
	# @explanation
	# Need explanation of this message!
	# @user_action
	# Need a user action for this message.
	scds_syslog -p error -t $syslog_tag -m \
		"INTERNAL ERROR: usage: `basename $0` <server_root> <siebel_enterprise>"
	exit 1
fi

SIEBEL_ROOT=$1
SIEBEL_ENTERPRISE=$2

. $SIEBEL_ROOT/siebenv.sh
rc=$?

if [ $rc -ne 0 ]
then
	scds_syslog -p error -t $syslog_tag -m \
		"Error while executing siebenv.sh."
	exit 1
fi

. $SIEBEL_ROOT/scsblconfig
rc=$?

if [ $rc -ne 0 ]
then
	# SCMSGS
	# @explanation
	# There was an error while attempting to execute (source) the
	# specified file. This may be due to improper permissions, or improper
	# settings in this file.
	# @user_action
	# Please verify that the file has correct permissions. If permissions
	# are correct, verify all the settings in this file. Try to manually
	# source this file in korn shell (". scsblconfig"), and correct any
	# errors.
	scds_syslog -p error -t $syslog_tag -m \
		"Error while executing scsblconfig."
	exit 1
fi

# This script is also used by validate, so validate all settings
# in the scsblconfig file.

if [ x$SADMUSR == x -o x$SADMPWD == x -o x$DBUSR == x -o x$DBPWD == x ]
then
	# SCMSGS
	# @explanation
	# The specified file has not been configured correctly, or it does not
	# have all the required settings.
	# @user_action
	# Please verify that required variables (according to the installation
	# instructions for this data service) are correctly configured in this
	# file. Try to manually source this file in korn shell (".
	# scsblconfig"), and verify if the required variables are getting set
	# correctly.
	scds_syslog -p error -t $syslog_tag -m \
		"scsblconfig not configured correctly."
	exit 1
fi

#
# Get Siebel server version.
#
# If possible, try to read version from an installed file rather than
# trying to connect to the gateway.
#
rc=1

if [ -r $SIEBEL_ROOT/messages/base.txt ]
then
        VERSION=`/usr/bin/grep LANG_INDEPENDENT $SIEBEL_ROOT/messages/base.txt | \
                /usr/bin/awk '{print $1}'`
        rc=$?
else
        rc=1
fi

#
# Contact gateway only if above failed to get version.
#
if [ $rc -ne 0 ]
then
        VERSION=`srvredit -q -g $SIEBEL_GATEWAY -e $SIEBEL_ENTERPRISE -s $SIEBSRVR_NAME -z -c '$Server.VersionString'`
        rc=$?

        if [ $rc -ne 0 ]
        then
                scds_syslog -p error -t $syslog_tag -m \
                        "Unable to connect to Siebel gateway."

                # Fault monitor will start Siebel server later.
                exit 0
        fi
fi

if [[ "$VERSION" == [^0-7]\.* ]]
then
        STATUS=`odbcsql /s siebsrvr_$SIEBEL_ENTERPRISE 2>&1 <<EOF
        login $DBUSR $DBPWD
        quit
        EOF`
else
        STATUS=`odbcsql /s ${SIEBEL_ENTERPRISE}_DSN 2>&1 <<EOF
        login $DBUSR $DBPWD
        quit
        EOF`
fi

rc=`echo "$STATUS" | egrep -c "ODBC error"`

exit $rc
