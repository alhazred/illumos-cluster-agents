#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#pragma ident	"@(#)stop_sblsrvr.ksh	1.7	07/06/06 SMI"
#
#
# This script is used by the STOP method to stop Siebel server.
#
# Input:
#
# - LHOSTNAME    - logical hostname configured for Siebel server
# - SIEBEL_ROOT  - location of Siebel server installation and siebenv.sh
#                  file (server_root)
# - SIEBEL_ENTERPRISE - Siebel enterprise name
# - SIEBSRVR_NAME - Siebel server name
# - TIMEOUT      - timeout under which this method will run
#
# Return:
#
# 0 - Success
# 1 - Failure
#

PATH=$PATH:/usr/cluster/lib/sc
syslog_tag="SC[SUNW.sblsrvr,stop_sblsrvr]"

if [ $# -ne 5 ]
then
	# SCMSGS
	# @explanation
	# Need explanation of this message!
	# @user_action
	# Need a user action for this message.
	scds_syslog -p error -t $syslog_tag -m \
		"INTERNAL ERROR: usage: `basename $0` <logicalhost> <server_root> <siebel_enterprise> <siebel_servername> <timeout>"
	exit 1
fi

export LHOSTNAME=$1
SERVER_ROOT=$2
SIEBEL_ENTERPRISE=$3
SIEBSRVR_NAME=$4
typeset -i TIMEOUT=$5

HATIMER="/usr/cluster/bin/hatimerun -v -t"

BASEPATH=`dirname $0`
BASEPATH=`dirname $BASEPATH`
BASEPATH=`dirname $BASEPATH`

. $SERVER_ROOT/siebenv.sh
rc=$?

if [ $rc -ne 0 ]
then
	scds_syslog -p error -t $syslog_tag -m \
		"Error while executing siebenv.sh."
	exit 1
fi

# Try to run stop_server for max 45% of the time allocated for shutdown.
typeset -i STOP_TIMEOUT=\(TIMEOUT*45\)/100

# Use of '-M' argument prevents the stop_server and reset_server commands
# from cleaning up shared memory resources that may be in use by other
# instances of Siebel server.

$HATIMER $STOP_TIMEOUT stop_server -e $SIEBEL_ENTERPRISE -M $SIEBSRVR_NAME > /dev/null 2> /dev/null

$HATIMER $STOP_TIMEOUT reset_server -e $SIEBEL_ENTERPRISE -M $SIEBSRVR_NAME > /dev/null 2> /dev/null
rc=$?

exit $rc
