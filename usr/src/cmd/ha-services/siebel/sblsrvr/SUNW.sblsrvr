#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)SUNW.sblsrvr	1.8	07/06/06 SMI"
#
# Registration information and Paramtable for sblsrvr
#
# NOTE: Keywords are case insensitive, i.e. users may use any
# capitalization style they wish
#

RESOURCE_TYPE = "sblsrvr";
VENDOR_ID = SUNW;
RT_DESCRIPTION = "Siebel server for Sun Cluster";

RT_VERSION ="3.1"; 
API_VERSION = 2;	 
FAILOVER = FALSE;

INIT_NODES = RG_PRIMARIES;

RT_BASEDIR=/opt/SUNWscsbl/sblsrvr/bin;

START				=	sblsrvr_svc_start;
STOP				=	sblsrvr_svc_stop;

VALIDATE			=	sblsrvr_validate;
UPDATE	 			=	sblsrvr_update;

MONITOR_START			=	sblsrvr_monitor_start;
MONITOR_STOP			=	sblsrvr_monitor_stop;
MONITOR_CHECK			=	sblsrvr_monitor_check;

PKGLIST = SUNWscsbl;

#
# Upgrade directives
#
#$upgrade
#$upgrade_from "1.0" anytime

#% SERVICE_NAME = "siebel"


# The paramtable is a list of bracketed resource property declarations 
# that come after the resource-type declarations
# The property-name declaration must be the first attribute
# after the open curly of a paramtable entry
#
# The following are the system defined properties. Each of the system defined
# properties have a default value set for each of the attributes. Look at 
# man rt_reg(4) for a detailed explanation.
#
{  
	PROPERTY = Start_timeout; 
	MIN = 120;
	DEFAULT = 480;
}
{
	PROPERTY = Stop_timeout; 
	MIN = 120;
	DEFAULT = 300;
}
{ 
	PROPERTY = Validate_timeout; 
	MIN = 60;
	DEFAULT = 300; 
}
{
        PROPERTY = Update_timeout;
	MIN = 60;
        DEFAULT = 300;
}
{ 
	PROPERTY = Monitor_Start_timeout; 
	MIN = 60;
	DEFAULT = 300;
}
{ 
	PROPERTY = Monitor_Stop_timeout; 
	MIN = 60;
	DEFAULT = 120;
}
{ 
	PROPERTY = Monitor_Check_timeout; 
	MIN = 60;
	DEFAULT = 300; 
}
{
        PROPERTY = FailOver_Mode;
        DEFAULT = SOFT;
        TUNABLE = ANYTIME;
}
{
        PROPERTY = Network_resources_used;
        TUNABLE = WHEN_DISABLED;
	DEFAULT = "";
}
{ 
	PROPERTY = Thorough_Probe_Interval; 
	MAX = 3600; 
	DEFAULT = 120; 
	TUNABLE = ANYTIME;
}
{ 
	PROPERTY = Retry_Count; 
	MAX = 10; 
	DEFAULT = 2; 
	TUNABLE = ANYTIME;
}
{ 
	PROPERTY = Retry_Interval; 
	MAX = 3600; 
	DEFAULT = 1700; 
	TUNABLE = ANYTIME;
}
{
	PROPERTY = Port_list;
	DEFAULT = ;
	TUNABLE = AT_CREATION;
}
{
        PROPERTY = Scalable;
	DEFAULT=FALSE;
        TUNABLE = AT_CREATION;
}
{
        PROPERTY = Load_balancing_policy;
        DEFAULT = LB_WEIGHTED;
        TUNABLE = AT_CREATION;
}
{
        PROPERTY = Load_balancing_weights;
        DEFAULT = "";
        TUNABLE = ANYTIME;
}

#
# Extension Properties
#

# These two control the restarting of the fault monitor itself
# (not the server daemon) by PMF.
{
	PROPERTY = Monitor_retry_count;
	EXTENSION;
	INT;
	DEFAULT = 4;
	TUNABLE = ANYTIME;
	DESCRIPTION = "Number of PMF restarts allowed for the fault monitor";
}
{
	PROPERTY = Monitor_retry_interval;
	EXTENSION;
	INT;
	DEFAULT = 2;
	TUNABLE = ANYTIME;
	DESCRIPTION = "Time window (minutes) for fault monitor restarts";
}

# This is an optional property, which determines whether to failover when
# retry_count is exceeded during retry_interval.
#
{
	PROPERTY = Failover_enabled;
	EXTENSION;
	BOOLEAN;
	DEFAULT = TRUE;
	TUNABLE = WHEN_DISABLED;
	DESCRIPTION = "Determines whether to failover when retry_count is exceeded during retry_interval";
}

# Time out value for the probe
{
	PROPERTY = Probe_timeout;
	EXTENSION;
	INT;
	MIN = 2;
	DEFAULT = 300;
	TUNABLE = ANYTIME;
	DESCRIPTION = "Time out value for the probe (seconds)";
}
{
	PROPERTY = Confdir_list;
	EXTENSION;
	STRINGARRAY;
	TUNABLE = AT_CREATION;
	DESCRIPTION = "The Siebel server root directory location";
}
{
	PROPERTY = siebel_enterprise;
	EXTENSION;
	STRING;
	TUNABLE = AT_CREATION;
	DESCRIPTION = "The Siebel enterprise name";
}
{
	PROPERTY = siebel_server;
	EXTENSION;
	STRING;
	TUNABLE = AT_CREATION;
	DESCRIPTION = "The Siebel server name";
}
