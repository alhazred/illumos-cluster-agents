/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#pragma ident	"@(#)sblsrvr.c	1.15	07/06/06 SMI"

/*
 * sblsrvr.c - Common utilities for sblsrvr
 *
 * This utility has the methods for performing the validation, starting and
 * stopping the data service and the fault monitor. It also contains the method
 * to probe the health of the data service.  The probe just returns either
 * success or failure. Action is taken based on this returned value in the
 * method found in the file sblsrvr_probe.c
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <unistd.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/statvfs.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <pwd.h>
#include <scha.h>
#include <libintl.h>
#include <rgm/libdsdev.h>
#include <errno.h>
#include <dlfcn.h>
#include "sblsrvr.h"

/*
 * The initial timeout allowed  for the sblsrvr dataservice to
 * be fully up and running. We will wait for for 3 % (SVC_WAIT_PCT)
 * of the start_timeout time before probing the service.
 */
#define	SVC_WAIT_PCT		3

/*
 * We need to wait for SVC_WAIT_TIME ( 2 secs) for pmf
 * to send the failure message before probing the service
 */

#define	SVC_WAIT_TIME		5

/*
 * Percentage of start timeout that will be used to ping the
 * DB or the gateway.
 */
#define	DB_PING_PCT		20
#define	GTWY_PING_PCT		15

/* Percentage for clean shutdown attempt (soft) */
#define	SOFT_STOP_PCT		85

/* Max length of user login and enterprise names */
#define	MAX_LOGIN_LEN		256
#define	MAX_ENT_NAMELEN		256

static scds_hasp_status_t hasp_status = SCDS_HASP_NO_RESOURCE;
static const char *rtbasedir = NULL;
static char lhostname[MAXHOSTNAMELEN + 1] = "";
static char s_enterprise[MAX_ENT_NAMELEN + 1] = "";
static char s_server[MAXHOSTNAMELEN + 1] = "";
static char srootdir[MAXPATHLEN + 1] = "";
static char suser[MAX_LOGIN_LEN + 1] = "";
static uid_t u_uid;
static gid_t u_gid;

/*
 * get_props_and_suser()
 *
 * Get various extension properties and username for
 * starting the server, and store them in static variables.
 */
int
get_props_and_suser(scds_handle_t scds_handle, boolean_t print_msgs)
{
	scha_extprop_value_t *s_xprop = NULL;
	struct passwd *pwdbuf = NULL;
	scha_str_array_t *confdirs = NULL;
	struct statvfs statvfsbuf;
	struct stat statbuf;
	char cmd_path[SCDS_CMD_SIZE] = "";
	int rc = 0;

	confdirs = scds_get_ext_confdir_list(scds_handle);

	/* Return an error if there are no config_dirs x prop */
	if (confdirs == NULL || confdirs->array_cnt == 0) {
		scds_syslog(LOG_ERR,
			"Property %s is not set.",
			"Confdir_list");
		if (print_msgs)
			(void) fprintf(stderr,
				gettext("Property %s is not set.\n"),
				"Confdir_list");
		return (1);
	}

	rc = snprintf(srootdir, sizeof (srootdir), "%s",
		confdirs->str_array[0]);
	if (rc == -1) {
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
			"String handling error creating path to "
			"server root dir. The path may be too long");
		if (print_msgs)
			(void) fprintf(stderr,
				gettext("INTERNAL ERROR: %s.\n"),
				gettext("String handling error creating "
				"path to server root dir. The path may be "
				"too long"));
		return (1);
	}

	rc = scds_get_ext_property(scds_handle, "siebel_enterprise",
		SCHA_PTYPE_STRING, &s_xprop);

	if (rc != SCHA_ERR_NOERR || s_xprop == NULL) {
		scds_syslog(LOG_ERR,
			"Failed to retrieve the property %s: %s.",
			"siebel_enterprise", scds_error_string(rc));
		if (print_msgs)
			(void) fprintf(stderr,
				gettext("Failed to retrieve the "
				"property %s: %s.\n"), "siebel_enterprise",
				gettext(scds_error_string(rc)));
		return (1);
	}

	if (s_xprop->val.val_str == NULL ||
		s_xprop->val.val_str[0] == '\0') {
		scds_syslog(LOG_ERR, "Property %s is not set.",
			"siebel_enterprise");
		if (print_msgs)
			(void) fprintf(stderr,
				gettext("Property %s is not set.\n"),
				"siebel_enterprise");
		return (1);
	} else {
		/*
		 * strlcpy() returns the length of the source string
		 * that is copied - truncation can be checked as below.
		 */
		if (strlcpy(s_enterprise, s_xprop->val.val_str,
			sizeof (s_enterprise)) >= sizeof (s_enterprise)) {
			scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
				"String handling error while copying "
				"enterprise name. Enterprise name may "
				"be too long");
			if (print_msgs)
				(void) fprintf(stderr,
					gettext("INTERNAL ERROR: %s.\n"),
					gettext("String handling error "
					"while copying enterprise name. "
					"Enterprise name may be too long"));
			rc = 1;
		}
		scds_free_ext_property(s_xprop);
		if (rc != 0)
			return (rc);
	}

	rc = scds_get_ext_property(scds_handle, "siebel_server",
		SCHA_PTYPE_STRING, &s_xprop);

	if (rc != SCHA_ERR_NOERR || s_xprop == NULL) {
		scds_syslog(LOG_ERR,
			"Failed to retrieve the property %s: %s.",
			"siebel_server", scds_error_string(rc));
		if (print_msgs)
			(void) fprintf(stderr,
				gettext("Failed to retrieve the "
				"property %s: %s.\n"), "siebel_server",
				gettext(scds_error_string(rc)));
		return (1);
	}

	if (s_xprop->val.val_str == NULL ||
		s_xprop->val.val_str[0] == '\0') {
		scds_syslog(LOG_ERR, "Property %s is not set.",
			"siebel_server");
		if (print_msgs)
			(void) fprintf(stderr,
				gettext("Property %s is not set.\n"),
				"siebel_server");
		return (1);
	} else {
		if (strlcpy(s_server, s_xprop->val.val_str,
			sizeof (s_enterprise)) >= sizeof (s_enterprise)) {
			scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
				"String handling error while copying "
				"server name. Server name may "
				"be too long");
			if (print_msgs)
				(void) fprintf(stderr,
					gettext("INTERNAL ERROR: %s.\n"),
					gettext("String handling error "
					"while copying server name. "
					"Server name may be too long"));
			rc = 1;
		}
		scds_free_ext_property(s_xprop);
		if (rc != 0)
			return (rc);
	}

	/*
	 * Make sure that siebenv.sh exists and that the
	 * permissions are correct.
	 */
	rc = snprintf(cmd_path, sizeof (cmd_path), "%s/%s",
		srootdir, "siebenv.sh");
	if (rc == -1) {
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
			"String handling error while copying path "
			"for siebenv.sh. The path may be too long");
		if (print_msgs)
			(void) fprintf(stderr,
				gettext("INTERNAL ERROR: %s.\n"),
				gettext("String handling error while "
				"copying path for siebenv.sh. The path "
				"may be too long"));
		return (1);
	}

	if (statvfs(cmd_path, &statvfsbuf) != 0) {
		if ((hasp_status == SCDS_HASP_ONLINE_NOT_LOCAL) &&
			(errno == ENOENT)) {	/*lint !e746 */
			return (0);
		} else {
			scds_syslog(LOG_ERR,
				"Cannot access file <%s>, err = <%s>",
				cmd_path, strerror(errno));
			if (print_msgs)
				(void) fprintf(stderr,
					gettext("Cannot access file "
					"<%s>, err = <%s>\n"),
					cmd_path,
					gettext(strerror(errno)));
			return (1);
		}
	}

	if (access(cmd_path, (R_OK | X_OK)) != 0) {
		scds_syslog(LOG_ERR,
			"No permission for owner to execute %s.",
			"siebenv.sh");
		if (print_msgs)
			(void) fprintf(stderr,
				gettext("No permission for owner to "
				"execute %s.\n"), "siebenv.sh");
		return (1);
	}

	rc = stat(cmd_path, &statbuf);
	if (rc != 0) {
		scds_syslog(LOG_ERR,
			"stat of file %s failed: <%s>.",
			cmd_path, strerror(errno));
		if (print_msgs)
			(void) fprintf(stderr,
				gettext("stat of file %s failed: <%s>.\n"),
				cmd_path, gettext(strerror(errno)));
		return (1);
	}

	/*
	 * Get username of the owner of the siebenv.sh file.
	 * This user will launch the start command.
	 */
	u_uid = statbuf.st_uid;
	pwdbuf = getpwuid(u_uid);

	if (pwdbuf == NULL ||
		strlcpy(suser, pwdbuf->pw_name, sizeof (suser))
		>= sizeof (suser)) {
		scds_syslog(LOG_ERR,  "INTERNAL ERROR: %s.",
			"String handling error while copying "
			"username. The username may be too long");
		if (print_msgs)
			(void) fprintf(stderr,
				gettext("INTERNAL ERROR: %s.\n"),
				gettext("String handling error while "
				"copying username. The username "
				"may be too long"));
		return (1);
	}

	/* User's GID */
	u_gid = (*pwdbuf).pw_gid;

	/* Make sure that scsblconfig file is created */
	rc = snprintf(cmd_path, sizeof (cmd_path),
		"%s/scsblconfig", srootdir);
	if (rc == -1) {
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
			"String handling error while copying path "
			"for scsblconfig. The path may be too long");
		if (print_msgs)
			(void) fprintf(stderr,
				gettext("INTERNAL ERROR: %s.\n"),
				gettext("String handling error while "
				"copying path for scsblconfig. The path "
				"may be too long"));
		return (1);
	}

	if (statvfs(cmd_path, &statvfsbuf) != 0) {
		if ((hasp_status == SCDS_HASP_ONLINE_NOT_LOCAL) &&
			(errno == ENOENT)) {	/*lint !e746 */
			return (0);
		} else {
			scds_syslog(LOG_ERR,
				"Cannot access file <%s>, err = <%s>",
				cmd_path, strerror(errno));
			if (print_msgs)
				(void) fprintf(stderr,
					gettext("Cannot access file "
					"<%s>, err = <%s>\n"),
					cmd_path,
					gettext(strerror(errno)));
			return (1);
		}
	}

	rc = stat(cmd_path, &statbuf);
	if (rc != 0) {
		scds_syslog(LOG_ERR, "stat of file %s failed: <%s>.",
			cmd_path, strerror(errno));
		if (print_msgs)
			(void) fprintf(stderr,
				gettext("stat of file %s failed: <%s>.\n"),
				cmd_path, gettext(strerror(errno)));
		return (1);
	}

	/* Check if ONLY owner has read/execute permissions */
	if (statbuf.st_uid != u_uid) {
		scds_syslog(LOG_ERR, "File %s should be owned "
			"by %s.", cmd_path, suser);
		if (print_msgs)
			(void) fprintf(stderr,
				gettext("File %s should be owned by %s.\n"),
				cmd_path, suser);
		return (1);
	}
	if (!(statbuf.st_mode & S_IRUSR)) {
		/*
		 * SCMSGS
		 * @explanation
		 * A program required the specified file to be readable by the
		 * specified user.
		 * @user_action
		 * Set correct permissions for the specified file to allow the
		 * specified user to read it.
		 */
		scds_syslog(LOG_ERR, "File %s should be readable "
			"by %s.", cmd_path, suser);
		if (print_msgs)
			(void) fprintf(stderr,
				gettext("File %s should be readable by %s.\n"),
				cmd_path, suser);
		return (1);
	}
	if (statbuf.st_mode & (S_IRGRP|S_IROTH)) {
		/*
		 * SCMSGS
		 * @explanation
		 * The specified file is expected to be readable only by the
		 * specified user, who is the owner of the file.
		 * @user_action
		 * Make sure that the specified file has correct permissions
		 * permissions by running "chmod 600 <file>" for
		 * non-executable files, or "chmod 700 <file>" for executable
		 * files.
		 */
		scds_syslog(LOG_ERR, "File %s should be readable "
			"only by the owner %s.", cmd_path, suser);
		if (print_msgs)
			(void) fprintf(stderr,
				gettext("File %s should be readable "
				"only by the owner %s.\n"),
				cmd_path, suser);
		return (1);
	}

	return (0);
}

/*
 * get_and_set_lhostname()
 *
 * Get the unique network_resources_used property associated
 * with this resource, and set a static variable using the
 * value of that property.
 */
int
get_and_set_lhostname(scds_handle_t scds_handle, boolean_t print_msgs)
{
	scds_net_resource_list_t *snrlp = NULL;
	int rc = 0;


	/*
	 * Return an error if there is an error when trying to get the
	 * available network address resources for this resource
	 */
	if ((rc = scds_get_rs_hostnames(scds_handle, &snrlp))
		!= SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
			"Error in trying to access the configured network "
			"resources : %s.", scds_error_string(rc));
		if (print_msgs)
			(void) fprintf(stderr,
				gettext("Error in trying to access the "
				"configured network resources : %s.\n"),
				gettext(scds_error_string(rc)));
		return (1);
	}

	/* Return an error if there are no network address resources */
	if (snrlp == NULL || snrlp->num_netresources == 0) {
		scds_syslog(LOG_ERR,
			"No network address resource in resource group.");
		if (print_msgs)
			(void) fprintf(stderr,
				gettext("No network address resource in "
				"resource group.\n"));
		rc = 1;
		goto finished;
	} else {
		if (strlcpy(lhostname, snrlp->netresources[0].hostnames[0],
			sizeof (lhostname)) >= sizeof (lhostname)) {
			/*
			 * SCMSGS
			 * @explanation
			 * There was an error while trying to get the logical
			 * hostname. The reason for the error is specified.
			 * @user_action
			 * Save a copy of /var/adm/messages from all nodes of
			 * the cluster and contact your Sun support
			 * representative for assistance.
			 */
			scds_syslog(LOG_ERR,
				"Error trying to get logical "
				"hostname: <%s>.",
				"logical hostname may be too long");
			if (print_msgs)
				(void) fprintf(stderr,
					gettext("Error trying to get "
					"logical hostname: <%s>.\n"),
					gettext("logical hostname may "
					"be too long"));
			return (1);
		}
	}

finished:
	if (snrlp)
		scds_free_net_list(snrlp);

	return (rc);
}


/*
 * check_db_and_gtwy():
 *
 * Verify if the database and gateway are up.
 *
 * Return 0 on success. DB_OR_GTWY_DOWN when database
 * or gateway status could not be confirmed as up. Return
 * 1 for any other error.
 */
int
check_db_and_gtwy(scds_handle_t scds_handle, int timeout,
	boolean_t print_msgs)
{
	char	cmd[SCDS_CMD_SIZE] = "";
	int	rc = 0, exit_code = 0;


	/* Get RT base directory */
	if (rtbasedir == NULL)
		rtbasedir = scds_get_rt_rt_basedir(scds_handle);

	/* Get LHOSTNAME */
	rc = get_and_set_lhostname(scds_handle, print_msgs);
	if (rc != 0)
		return (rc);

	/*
	 * Check if backend Siebel DB is up.
	 */
	timeout = (DB_PING_PCT * timeout) / 100;

	rc = snprintf(cmd, sizeof (cmd), "/bin/su - %s -c "
		"\"%s/pingsbldb %s %s\" >/dev/null",
		suser, rtbasedir, srootdir, s_enterprise);
	if (rc == -1) {
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
			"String handling error creating pingsbldb "
			"command. The command may be too long");
		if (print_msgs)
			(void) fprintf(stderr,
				gettext("INTERNAL ERROR: %s.\n"),
				gettext("String handling error creating "
				"pingsbldb command. The command may be "
				"too long"));
		return (1);
	}

	/* Call pingsbldb under scds_timerun */
	rc = scds_timerun(scds_handle, cmd, timeout,
		SIGKILL, &exit_code);

	if (rc != SCHA_ERR_NOERR || exit_code != 0) {
		rc = DB_OR_GTWY_DOWN;
		/*
		 * SCMSGS
		 * @explanation
		 * A critical method was unable to determine the status of the
		 * specified service or resource.
		 * @user_action
		 * Please examine other messages in the /var/adm/messages file
		 * to determine the cause of this problem. Also verify if the
		 * specified service or resource is available or not. If not
		 * available, start the service or resource and retry the
		 * operation which failed.
		 */
		scds_syslog(LOG_ERR,
			"Could not verify status of %s.",
			"Siebel database");
		if (print_msgs)
			(void) fprintf(stderr,
				gettext("Could not verify status of %s.\n"),
				gettext("Siebel database"));
		return (rc);
	}

	/*
	 * Check if Siebel Gateway is up.
	 */
	timeout = (GTWY_PING_PCT * timeout) / 100;

	rc = snprintf(cmd, sizeof (cmd), "/bin/su - %s -c "
		"\"%s/probe_sblgtwy %s\" >/dev/null",
		suser, rtbasedir, srootdir);
	if (rc == -1) {
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
			"String handling error creating probe_sblgtwy "
			"command. The command may be too long");
		if (print_msgs)
			(void) fprintf(stderr,
				gettext("INTERNAL ERROR: %s.\n"),
				gettext("String handling error creating "
				"probe_sblgtwy command. The command may be "
				"too long"));
		return (1);
	}

	/* Ping Gateway under scds_timerun */
	rc = scds_timerun(scds_handle, cmd, timeout,
		SIGKILL, &exit_code);

	if (rc != SCHA_ERR_NOERR || exit_code != 0) {
		rc = DB_OR_GTWY_DOWN;
		scds_syslog(LOG_ERR,
			"Could not verify status of %s.",
			"Siebel gateway");
		if (print_msgs)
			(void) fprintf(stderr,
				gettext("Could not verify status of %s.\n"),
				gettext("Siebel gateway"));
		return (rc);
	}

	return (rc);
}


/*
 * svc_validate():
 *
 * - Verify extension properties.
 * - Get owner of siebenv.sh file.
 * - Verify if the above user has permissions for all
 *   executables that will be used.
 * - Verify if test user is created in Siebel and backend DB.
 * - Store some values in static variables for use in other functions.
 *
 */

int
svc_validate(scds_handle_t scds_handle, boolean_t print_msgs)
{
	scds_net_resource_list_t *snrlp = NULL;
	int	rc = 0, start_timeout;


	/* check for HAStoragePlus resources */
	rc = scds_hasp_check(scds_handle, &hasp_status);
	if (rc != SCHA_ERR_NOERR) {
		/* scha_hasp_check() logs everytime it fails */
		if (print_msgs)
			(void) fprintf(stderr,
				gettext("INTERNAL ERROR: %s.\n"),
				gettext("scds_hasp_check failed"));
		return (1);
	}

	if (hasp_status == SCDS_HASP_NOT_ONLINE) {
		scds_syslog(LOG_ERR, "Resource depends on a "
			"SUNW.HAStoragePlus type resource that is "
			"not online anywhere.");
		if (print_msgs)
			(void) fprintf(stderr, gettext("Resource depends "
				"on a SUNW.HAStoragePlus type resource "
				"that is not online anywhere.\n"));
		return (1);
	} else if (hasp_status == SCDS_HASP_ERR_CONFIG) {
		/* problem syslogged by scds_hasp_check */
		return (1);
	}

	/*
	 * Get various extension properties and username for
	 * starting the server.
	 */
	rc = get_props_and_suser(scds_handle, print_msgs);
	if (rc != 0)
		return (1);

	/*
	 * Make sure that various Siebel server command files
	 * exist and that their permissions are correct.
	 * validate_permissions() logs error message.
	 */
	if (validate_permissions(srootdir, "start_server",
		u_uid, u_gid, hasp_status, print_msgs) != 0)
		return (1);

	if (validate_permissions(srootdir, "stop_server",
		u_uid, u_gid, hasp_status, print_msgs) != 0)
		return (1);

	if (validate_permissions(srootdir, "reset_server",
		u_uid, u_gid, hasp_status, print_msgs) != 0)
		return (1);

	if (validate_permissions(srootdir, "odbcsql",
		u_uid, u_gid, hasp_status, print_msgs) != 0)
		return (1);

	if (validate_permissions(srootdir, "srvredit",
		u_uid, u_gid, hasp_status, print_msgs) != 0)
		return (1);

	if (validate_permissions(srootdir, "srvrmgr",
		u_uid, u_gid, hasp_status, print_msgs) != 0)
		return (1);

	/*
	 * Executables for checking DB and gateway are available
	 * on this node if HASP is not configured, or if it *is*
	 * online locally.
	 */
	if (hasp_status != SCDS_HASP_ONLINE_NOT_LOCAL) {

		/* Get Start_timeout for this resource */
		start_timeout = scds_get_rs_start_timeout(scds_handle);

		/*
		 * Verify if DB and gateway status can be checked.
		 * Passing start timeout since a percentage of start
		 * timeout is to be used to probe the service. If this
		 * fails here due to validate timeout being small
		 * compared to start timeout or for any other reason,
		 * only a warning is logged.
		 */
		rc = check_db_and_gtwy(scds_handle, start_timeout,
			print_msgs);
		if (rc != 0) {
			if (rc == DB_OR_GTWY_DOWN) {
				/*
				 * Only log a warning in validate. This
				 * is to warn the user that the
				 * DB and gateway must be up before
				 * Siebel server can to be started.
				 */
				/*
				 * SCMSGS
				 * @explanation
				 * This is a warning message indicating a
				 * problem in determining the status of Siebel
				 * database and/or the Siebel gateway.
				 * @user_action
				 * Please verify that the scsblconfig file is
				 * correctly configured, and that the Siebel
				 * database and Siebel gateway are up before
				 * attempting to start the Siebel server.
				 */
				scds_syslog(LOG_WARNING,
					"Siebel server can be started "
					"only after Siebel database and "
					"Siebel gateway are running, and "
					"the scsblconfig file is "
					"correctly configured.");
				if (print_msgs)
					(void) fprintf(stderr,
						gettext("Siebel server "
						"can be started only "
						"after Siebel database "
						"and Siebel gateway are "
						"running, and the "
						"scsblconfig file is "
						"correctly configured.\n"));
				rc = 0;
			} else {
				goto finished_validate;
			}
		}
	}

	/*
	 * Return an error if there is an error when trying to get the
	 * available network address resources for this resource
	 */
	if ((rc = scds_get_rs_hostnames(scds_handle, &snrlp))
		!= SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
			"Error in trying to access the configured network "
			"resources : %s.", scds_error_string(rc));
		if (print_msgs)
			(void) fprintf(stderr,
				gettext("Error in trying to access the "
				"configured network resources : %s.\n"),
				gettext(scds_error_string(rc)));
		rc = 1;
		goto finished_validate;
	}

	/* Return an error if there are no network address resources */
	if (snrlp == NULL || snrlp->num_netresources == 0) {
		scds_syslog(LOG_ERR,
			"No network address resource in resource group.");
		if (print_msgs)
			(void) fprintf(stderr,
				gettext("No network address resource in "
				"resource group.\n"));
		rc = 1;
		goto finished_validate;
	}

	/* Check to make sure other important extension props are set */
	if (scds_get_ext_monitor_retry_count(scds_handle) <= 0) {
		scds_syslog(LOG_ERR,
			"Property %s is not set.",
			"Monitor_retry_count");
		if (print_msgs)
			(void) fprintf(stderr,
				gettext("Property %s is not set.\n"),
				"Monitor_retry_count");
		rc = 1;
		goto finished_validate;
	}
	if (scds_get_ext_monitor_retry_interval(scds_handle) <= 0) {
		scds_syslog(LOG_ERR,
			"Property %s is not set.",
			"Monitor_retry_interval");
		if (print_msgs)
			(void) fprintf(stderr,
				gettext("Property %s is not set.\n"),
				"Monitor_retry_interval");
		rc = 1;
		goto finished_validate;
	}

	/* All validation checks were successful */
	scds_syslog(LOG_INFO, "Successful validation.");

finished_validate:
	if (snrlp)
		scds_free_net_list(snrlp);

	return (rc); /* return result of validation */
}

/*
 * svc_start():
 *
 */

int
svc_start(scds_handle_t scds_handle)
{
	char	cmd[SCDS_CMD_SIZE] = "";
	int	rc = 0, start_timeout;
	boolean_t print_msgs = B_FALSE;


	/* Get Start_timeout for this resource */
	start_timeout = scds_get_rs_start_timeout(scds_handle);

	/* Get RT base directory */
	rtbasedir = scds_get_rt_rt_basedir(scds_handle);

	/* Get LHOSTNAME */
	rc = get_and_set_lhostname(scds_handle, print_msgs);
	if (rc != 0)
		goto finished_start;

	/*
	 * Check if backend Siebel DB and gateway are up.
	 */
	rc = check_db_and_gtwy(scds_handle, start_timeout, print_msgs);
	if (rc != SCHA_ERR_NOERR) {
		if (rc == DB_OR_GTWY_DOWN)
			/*
			 * SCMSGS
			 * @explanation
			 * Siebel server could not start because a service it
			 * depends on is not running.
			 * @user_action
			 * Make sure that the Siebel database and the Siebel
			 * gateway are running before attempting to restart
			 * the Siebel server resource.
			 */
			scds_syslog(LOG_ERR,
				"Could not start Siebel server: %s.",
				"Siebel database or gateway may not be "
				"running");
		goto finished_start;
	}

	/* Construct Start command */
	rc = snprintf(cmd, sizeof (cmd), "/bin/su - %s -c "
		"\"%s/start_sblsrvr %s %s %s %s\" >/dev/null",
		suser, rtbasedir, lhostname,
		srootdir, s_enterprise, s_server);
	if (rc == -1) {
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
			"String handling error creating start "
			"command. The command may be too long");
		return (1);
	}

	scds_syslog_debug(DBG_LEVEL_LOW,
		"Starting Siebel server under PMF using the "
		"command \"%s\".", cmd);

	/* Start Siebel server */
	rc = scds_pmf_start(scds_handle, SCDS_PMF_TYPE_SVC, 0, cmd, -1);

	if (rc == SCHA_ERR_NOERR) {
		scds_syslog_debug(DBG_LEVEL_LOW,
			"Siebel server started under PMF.");
	} else {
		scds_syslog_debug(DBG_LEVEL_LOW,
			"Siebel server did not start under PMF.");
	}

finished_start:
	return (rc);
}

/*
 * svc_stop():
 *
 * Stop the Siebel server
 * Return 0 on success, > 0 on failures.
 *
 */
int
svc_stop(scds_handle_t scds_handle)
{
	char	cmd[SCDS_CMD_SIZE] = "";
	char	*rsname = NULL, *rgname = NULL;
	int	rc = 0, exit_code = 0,
		stop_timeout, timeout;
	time_t	start_time = time(NULL);
	scds_pmf_status_t status;
	boolean_t print_msgs = B_FALSE;


	/* Get resource and resource group names */
	rsname = (char *)scds_get_resource_name(scds_handle);
	rgname = (char *)scds_get_resource_group_name(scds_handle);

	/* Get Stop_timeout for this resource */
	stop_timeout = scds_get_rs_stop_timeout(scds_handle);

	/* Get RT base directory */
	rtbasedir = scds_get_rt_rt_basedir(scds_handle);

	/*
	 * First take the command out of PMF monitoring,
	 * so that it doesn't keep restarting it.
	 */
	rc = scds_pmf_stop_monitoring(scds_handle, SCDS_PMF_TYPE_SVC, 0);
	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to take "
			"the resource out of PMF control.");
		return (rc);
	} else {
		scds_syslog_debug(DBG_LEVEL_HIGH,
			"Resource taken out of PMF control.");
	}

	/*
	 * Get various extension properties and username for
	 * stopping the server.
	 */
	rc = get_props_and_suser(scds_handle, print_msgs);
	if (rc != 0)
		goto finished_stop;

	/* Get LHOSTNAME */
	rc = get_and_set_lhostname(scds_handle, print_msgs);
	if (rc != 0)
		goto finished_stop;

	/* Construct Stop command */
	timeout = stop_timeout - (time(NULL) - start_time);
	timeout = (SOFT_STOP_PCT * timeout) / 100;
	rc = snprintf(cmd, sizeof (cmd), "/bin/su - %s -c "
		"\"%s/stop_sblsrvr %s %s %s %s %d\" >/dev/null",
		suser, rtbasedir, lhostname, srootdir,
		s_enterprise, s_server, timeout);
	if (rc == -1) {
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
			"String handling error creating stop "
			"command. The command may be too long");
		return (1);
	}

	/* Stop Siebel server */
	rc = scds_timerun(scds_handle, cmd, timeout,
		SIGKILL, &exit_code);
	if (rc != SCHA_ERR_NOERR || exit_code != 0) {
		scds_syslog(LOG_ERR,
			"The stop command \"%s\" failed to stop %s. "
			"Using SIGKILL.", cmd, "Siebel server");
	}

finished_stop:
	rc = scds_pmf_get_status(scds_handle, SCDS_PMF_TYPE_SVC,
		SCDS_PMF_SINGLE_INSTANCE, &status);
	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
			"Failed to get the pmf_status. "
			"Error: %s.", scds_error_string(rc));
		return (1);
	} else if (status == SCDS_PMF_NOT_MONITORED) {
		/* Clean shutdown had worked */
		return (0);
	}

	rc = scds_pmf_signal(scds_handle,
		SCDS_PMF_TYPE_SVC, 0, SIGKILL, -1);
	if (rc == 1) rc = SCHA_ERR_NOERR; /* tag does not exist */

	if (rc == SCHA_ERR_NOERR) {
		(void) scha_resource_setstatus(rsname, rgname,
			SCHA_RSSTATUS_OFFLINE,
			"Successfully stopped Siebel server.");
	} else {
		(void) scha_resource_setstatus(rsname, rgname,
			SCHA_RSSTATUS_FAULTED,
			"Failed to stop Siebel server.");
		scds_syslog_debug(DBG_LEVEL_LOW,
			"scds_timerun returned %d.", rc);
	}

	return (rc);
}

/*
 * svc_wait():
 *
 * wait for the data service to start up fully and make sure it is running
 * healthy
 */

int
svc_wait(scds_handle_t scds_handle)
{
	int rc, svc_start_timeout, probe_timeout;
	scds_pmf_status_t status;


	/*
	 * Get the Start method timeout, and the Probe timeout value.
	 */
	svc_start_timeout = scds_get_rs_start_timeout(scds_handle);
	probe_timeout = scds_get_ext_probe_timeout(scds_handle);

	/*
	 * sleep for SVC_WAIT_PCT percentage of start_timeout time
	 * before actually probing the dataservice. This is to allow
	 * the dataservice to be fully up inorder to reply to the
	 * probe. NOTE: the value for SVC_WAIT_PCT could be different
	 * for different dataservices.
	 */
	if (scds_svc_wait(scds_handle, (svc_start_timeout * SVC_WAIT_PCT)/100)
		!= SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
			"Failed to start the service %s.",
			"Siebel server");
		return (1);
	}

	for (;;) {
		/*
		 * probe the data service on the IP address of the
		 * network resource
		 */
		rc = svc_probe(scds_handle, probe_timeout, B_FALSE);

		/* If only Siebel component is down, ignore error */
		if (rc == SCDS_PROBE_COMPLETE_FAILURE/10)
			return (0);
		/*
		 * If there is no error, or if the DB or gateway is
		 * down, we rely on the probe to do the restart the
		 * service after the DB and gateway are up.
		 */
		if (rc == SCHA_ERR_NOERR || rc == DB_OR_GTWY_DOWN) {
			return (rc);
		}

		/* Log notice indicating that service in not fully up */
		scds_syslog(LOG_NOTICE,
			"Waiting for %s to come up.",
			"Siebel server");

		rc = scds_pmf_get_status(scds_handle, SCDS_PMF_TYPE_SVC,
			SCDS_PMF_SINGLE_INSTANCE, &status);
		if (rc != SCHA_ERR_NOERR) {
			scds_syslog(LOG_ERR,
				"Failed to get the pmf_status. "
				"Error: %s.", scds_error_string(rc));
			return (1);
		}

		/* Check if the dataservice is still up and running */
		if (status != SCDS_PMF_MONITORED) {
			scds_syslog(LOG_ERR,
				"Data service failed to stay up. "
				"Start method failed.");
			return (1);
		}

		/*
		 * Dataservice is still trying to come up. Sleep for a while
		 * before probing again.
		 */
		(void) scds_svc_wait(scds_handle, SVC_WAIT_TIME);

	/* We rely on RGM to timeout and terminate the program */
	}
}

/*
 * This function starts the fault monitor for a sblsrvr resource.
 * This is done by starting the probe under PMF. The PMF tag
 * is derived as <RG-name,RS-name,instance_number.mon>. The restart option
 * of PMF is used but not the "infinite restart". Instead
 * interval/retry_time is obtained from the RTR file.
 */

int
mon_start(scds_handle_t scds_handle)
{
	scha_err_t	err;

	scds_syslog_debug(DBG_LEVEL_HIGH,
		"Calling MONITOR_START method for resource <%s>.",
		scds_get_resource_name(scds_handle));

	/*
	 * The probe sblsrvr_probe is assumed to be available in the same
	 * subdirectory where the other callback methods for the RT are
	 * installed. The last parameter to scds_pmf_start denotes the
	 * child monitor level. Since we are starting the probe under PMF
	 * we need to monitor the probe process only and hence we are using
	 * a value of 0.
	 */
	err = scds_pmf_start(scds_handle, SCDS_PMF_TYPE_MON,
		SCDS_PMF_SINGLE_INSTANCE, "sblsrvr_probe", 0);

	if (err != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to start fault monitor.");
		return (err);
	}

	scds_syslog(LOG_INFO, "Started the fault monitor.");

	return (SCHA_ERR_NOERR);
}


/*
 * This function stops the fault monitor for a sblsrvr resource.
 * This is done via PMF. The PMF tag for the fault monitor is
 * constructed based on <RG-name_RS-name,instance_number.mon>.
 */

int
mon_stop(scds_handle_t scds_handle)
{
	scha_err_t	err;

	scds_syslog_debug(DBG_LEVEL_HIGH, "Calling scds_pmf_stop method");

	err = scds_pmf_stop(scds_handle, SCDS_PMF_TYPE_MON,
		SCDS_PMF_SINGLE_INSTANCE, SIGKILL,
		scds_get_rs_monitor_stop_timeout(scds_handle));

	if (err != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to stop fault monitor.");
		return (err);
	}

	scds_syslog(LOG_INFO, "Stopped the fault monitor.");

	return (SCHA_ERR_NOERR);
}


/*
 * This function is called by svc_probe to get the resource
 * status and message. Returns 1 on failure, and 0 and the
 * status and message on success.
 */
int
get_status_local(scds_handle_t scds_handle, scha_rsstatus_t *status)
{
	scha_resource_t	rs_handle;
	char *rsname = NULL, *rgname = NULL;
	scha_status_value_t *rsstatus = NULL;
	scha_err_t e = SCHA_ERR_NOERR;
	int rc = 0;


	scds_syslog_debug(DBG_LEVEL_HIGH, "Getting local status.");

	/* Get resource and resource group names */
	rsname = (char *)scds_get_resource_name(scds_handle);
	rgname = (char *)scds_get_resource_group_name(scds_handle);

retry_status:
	e = scha_resource_open(rsname, rgname, &rs_handle);
	if (e != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * An API operation has failed while retrieving the resource
		 * property. Low memory or API call failure might be the
		 * reasons.
		 * @user_action
		 * In case of low memory, the problem will probably cured by
		 * rebooting. If the problem reoccurs, you might need to
		 * increase swap space by configuring additional swap devices.
		 * Otherwise, if it is API call failure, check the syslog
		 * messages from other components. For resource name and the
		 * property name, check the current syslog message.
		 */
		scds_syslog(LOG_ERR,
			"Failed to open the resource handle"
			": %s.", scds_error_string(e));
		return (1);
	}

	e = scha_resource_get(rs_handle, SCHA_STATUS, &rsstatus);
	if (e == SCHA_ERR_SEQID) {
		scds_syslog(LOG_INFO,
			"Retrying to retrieve the resource "
			"information: %s.", scds_error_string(e));
			/* close the resource handle */
		(void) scha_resource_close(rs_handle);
		rs_handle = NULL;
		(void) sleep(1);
		goto retry_status;
	} else if (e != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
			"Failed to retrieve the resource "
			"property %s: %s.",
			SCHA_STATUS, scds_error_string(e));
		rc = 1;
	} else {
		*status = (*rsstatus).status;
		rc = 0;
	}

	(void) scha_resource_close(rs_handle);

	scds_syslog_debug(DBG_LEVEL_HIGH, "Completed getting status.");

	return (rc);
}


/*
 * svc_probe(): Do data service specific probing. Return an int value
 * between 0 (success) and 100 (complete failure).
 */
int
svc_probe(scds_handle_t scds_handle, int timeout, boolean_t print_msgs)
{
	static char probe_cmd[SCDS_CMD_SIZE] = "";
	char	*rsname = NULL, *rgname = NULL;
	int	rc = 0, exit_code = 0, cmd_timeout;
	time_t	start_time = time(NULL);
	static int is_db_gtwy_down = 0;


	/* Get resource and resource group names */
	rsname = (char *)scds_get_resource_name(scds_handle);
	rgname = (char *)scds_get_resource_group_name(scds_handle);

	/* Get RT base directory */
	if (rtbasedir == NULL)
		rtbasedir = scds_get_rt_rt_basedir(scds_handle);

	/*
	 * Get various extension properties and username for
	 * stopping the server.
	 */
	if (srootdir[0] == '\0' || suser[0] == '\0' ||
		s_enterprise[0] == '\0' || s_server[0] == '\0') {
		rc = get_props_and_suser(scds_handle, print_msgs);
		if (rc != 0)
			return (SCDS_PROBE_COMPLETE_FAILURE);
	}

	/* Get LHOSTNAME */
	if (lhostname[0] == '\0') {
		rc = get_and_set_lhostname(scds_handle, print_msgs);
		if (rc != 0)
			return (SCDS_PROBE_COMPLETE_FAILURE);
	}

	/* Construct probe command */
	if (probe_cmd[0] == '\0') {
		rc = snprintf(probe_cmd, sizeof (probe_cmd),
			"/bin/su - %s -c "
			"\"%s/probe_sblsrvr %s %s %s %s\" >/dev/null",
			suser, rtbasedir, lhostname, srootdir,
			s_enterprise, s_server);
		if (rc == -1) {
			scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
				"String handling error creating probe "
				"command. The command may be too long");
			return (SCDS_PROBE_COMPLETE_FAILURE);
		}
	}

	/* Probe Siebel server */
	cmd_timeout = timeout - (time(NULL) - start_time);
	rc = scds_timerun(scds_handle, probe_cmd, cmd_timeout,
		SIGKILL, &exit_code);

	if (rc == SCHA_ERR_TIMEOUT) {
		if (print_msgs)
			scds_syslog(LOG_ERR, "Probe timed out.");
		rc = SCDS_PROBE_COMPLETE_FAILURE/2;
	} else if (rc != SCHA_ERR_NOERR) {
		/* scds_timerun error should be a partial failure */
		rc = SCDS_PROBE_COMPLETE_FAILURE/2;
	} else if (exit_code == DB_OR_GTWY_DOWN) {
		/*
		 * Need to wait till the DB or gateway is up.
		 * Return special value - do not treat this as
		 * failure (yet).
		 */
		if (print_msgs)
			/*
			 * SCMSGS
			 * @explanation
			 * This indicates that the Siebel database or Siebel
			 * gateway is unavailable for the Siebel server.
			 * @user_action
			 * Please determine the reason for Siebel database or
			 * Siebel gateway failure, and ensure that they are
			 * both running. If the Siebel server resource is not
			 * offline, it should get started by the fault
			 * monitor.
			 */
			scds_syslog(LOG_ERR,
				"Database or gateway down.");

		/*
		 * If we find the DB or gateway down, and
		 * is_db_gtwy_down variable is not already set to 1,
		 * then update the status to degraded.
		 */
		if (is_db_gtwy_down == 0) {
			is_db_gtwy_down = 1;
			(void) scha_resource_setstatus(rsname, rgname,
				SCHA_RSSTATUS_DEGRADED,
				"Database or gateway down.");
		}

		rc = DB_OR_GTWY_DOWN;
	} else if (exit_code == (SCDS_PROBE_COMPLETE_FAILURE/10)) {
		/*
		 * Partial failure. Default value of Retry_interval
		 * and Thorough_probe_interval will allow these
		 * partial failures to accumulate only upto 50%.
		 * This should not cause any restart or failover.
		 * rc = SCDS_PROBE_COMPLETE_FAILURE/10;
		 */
		rc = SCDS_PROBE_COMPLETE_FAILURE/10;
	} else if (exit_code != 0) {
		if (print_msgs)
			scds_syslog(LOG_ERR,
				"Probe for %s returned error.",
				"Siebel server");
		scds_syslog_debug(DBG_LEVEL_LOW,
			"The probe command \"%s\" returned "
			"with non-zero status", probe_cmd);
		rc = SCDS_PROBE_COMPLETE_FAILURE;
	}

	/*
	 * If the probe is successful and is_db_gtwy_down variable set to 1,
	 * then update the status to online.
	 */
	if ((rc == 0) && (is_db_gtwy_down == 1)) {
		is_db_gtwy_down = 0;
		(void) scha_resource_setstatus(rsname, rgname,
			SCHA_RSSTATUS_OK, "Service is online.");
	}

	return (rc);
}
