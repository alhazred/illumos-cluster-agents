/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2002 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#pragma ident	"@(#)sblsrvr_svc_start.c	1.5	07/06/06 SMI"

/*
 * sblsrvr_svc_start.c - Start method for sblsrvr
 */

#include <rgm/libdsdev.h>
#include "sblsrvr.h"

/*
 * The start method for sblsrvr. Does some sanity checks on
 * the resource settings then starts the sblsrvr under PMF with
 * an action script.
 */

int
main(int argc, char *argv[])
{
	scds_handle_t	scds_handle;
	char *rsname = NULL, *rgname = NULL;
	int rc;

	/*
	 * Process all the arguments that have been passed to us from RGM
	 * and do some initialization for syslog
	 */

	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR) {
		return (1);
	}

	/* Get resource and resource group names */
	rsname = (char *)scds_get_resource_name(scds_handle);
	rgname = (char *)scds_get_resource_group_name(scds_handle);

	/* Validate the configuration and if there is an error return back */
	rc = svc_validate(scds_handle, B_FALSE);
	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		    "Failed to validate configuration.");
	} else {
		/* Start the data service, if it fails return with an error */
		rc = svc_start(scds_handle);
	}

	if (rc == SCHA_ERR_NOERR) {
		/* Wait for the service to start up fully */
		rc = svc_wait(scds_handle);
		scds_syslog_debug(DBG_LEVEL_HIGH, "Returned from svc_wait");
	}

	if (rc == SCHA_ERR_NOERR) {
		(void) scha_resource_setstatus(rsname, rgname,
			SCHA_RSSTATUS_OK,
			"Completed successfully.");
	} else if (rc == DB_OR_GTWY_DOWN) {
		(void) scha_resource_setstatus(rsname, rgname,
			SCHA_RSSTATUS_DEGRADED,
			"Database or gateway down.");
		rc = SCHA_ERR_NOERR;
	} else {
		(void) scha_resource_setstatus(rsname, rgname,
			SCHA_RSSTATUS_FAULTED,
			"Failed to start Siebel server.");
		scds_syslog_debug(DBG_LEVEL_HIGH,
			"scds_timerun returned SCHA_ERR_TIMEOUT");
	}

	/* Free up the Environment resources that were allocated */
	scds_close(&scds_handle);

	return (rc);
}
