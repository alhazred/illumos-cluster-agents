#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#pragma ident	"@(#)probe_sblsrvr.ksh	1.7	07/10/03 SMI"
#
#
# This script is used by the PROBE method to probe Siebel server.
#
# Input:
#
# - LHOSTNAME    - logical hostname configured for Siebel server
# - SIEBEL_ROOT  - location of Siebel server installation and siebenv.sh
#                  file (server_root)
# - SIEBEL_ENTERPRISE - Siebel enterprise name
# - SIEBSRVR_NAME - Siebel server name
#
# Return:
#
# 0 - Success
# 1 - Failure
#

PATH=$PATH:/usr/cluster/lib/sc
syslog_tag="SC[SUNW.sblsrvr,probe_sblsrvr]"

if [ $# -ne 4 ]
then
	# SCMSGS
	# @explanation
	# Need explanation of this message!
	# @user_action
	# Need a user action for this message.
	scds_syslog -p error -t $syslog_tag -m \
		"INTERNAL ERROR: usage: `basename $0` <logicalhost> <server_root> <siebel_enterprise> <siebel_servername>"
	exit 1
fi

export LHOSTNAME=$1
SERVER_ROOT=$2
SIEBEL_ENTERPRISE=$3
SIEBSRVR_NAME=$4

BASEPATH=`dirname $0`
BASEPATH=`dirname $BASEPATH`
BASEPATH=`dirname $BASEPATH`

typeset -i DB_OR_GTWY_DOWN=200
typeset -i COMPLETE_FAILURE=100
typeset -i PARTIAL_FAILURE=$COMPLETE_FAILURE/10

. $SERVER_ROOT/siebenv.sh
rc=$?

if [ $rc -ne 0 ]
then
	scds_syslog -p error -t $syslog_tag -m \
		"Error while executing siebenv.sh."
	exit 1
fi

. $SERVER_ROOT/scsblconfig
rc=$?

if [ $rc -ne 0 ]
then
	scds_syslog -p error -t $syslog_tag -m \
		"Error while executing scsblconfig."
	exit 1
fi

if [ x$SADMUSR == x -o x$SADMPWD == x -o x$DBUSR == x -o x$DBPWD == x ]
then
	scds_syslog -p error -t $syslog_tag -m \
		"scsblconfig not configured correctly."
	exit 1
fi


#
# Get Siebel server version (also checks gateway status).
#
VERSION=`srvredit -q -g $SIEBEL_GATEWAY -e $SIEBEL_ENTERPRISE -s $SIEBSRVR_NAME -z -c '$Server.VersionString'`
rc=$?

if [ $rc -ne 0 ]
then
	# SCMSGS
	# @explanation
	# Siebel gateway may be unreachable.
	# @user_action
	# Please verify that the Siebel gateway resource is up.
	scds_syslog -p error -t $syslog_tag -m \
		"Unable to connect to Siebel gateway."
	exit $DB_OR_GTWY_DOWN
fi

if [[ "$VERSION" == [^0-7]\.* ]]
then
	STATUS=`odbcsql /s siebsrvr_$SIEBEL_ENTERPRISE 2>&1 <<EOF
	login $DBUSR $DBPWD
	quit
	EOF`
else
	STATUS=`odbcsql /s ${SIEBEL_ENTERPRISE}_DSN 2>&1 <<EOF
	login $DBUSR $DBPWD
	quit
	EOF`
fi

rc=`echo "$STATUS" | egrep -c "ODBC error"`

if [ $rc -ne 0 ]
then
	# SCMSGS
	# @explanation
	# Siebel database may be unreachable.
	# @user_action
	# Please verify that the Siebel database resource is up.
	scds_syslog -p error -t $syslog_tag -m \
		"Unable to connect to Siebel database."
	exit $DB_OR_GTWY_DOWN
fi



#
# LD_PRELOAD only if running Siebel 7.0 or 7.5. If Siebel 7.7 or later,
# srvrmgr needs to run multiple commands to generate status. Using
# file to input commands, we have to connect to srvrmgr only once.
#
if [[ "$VERSION" == 7\.0* || "$VERSION" == 7\.5* ]]
then
	export LD_PRELOAD=$BASEPATH/lib/libloghost.so

	STATUS=`srvrmgr /g $SIEBEL_GATEWAY /e $SIEBEL_ENTERPRISE /s $SIEBSRVR_NAME /u $SADMUSR /c "list compgrps" 2>&1 <<-EOF
	$SADMPWD
	EOF`

	rc=`echo "$STATUS" | egrep -c "ADM-"`

	unset LD_PRELOAD
else
	STATUS=`srvrmgr /g $SIEBEL_GATEWAY /e $SIEBEL_ENTERPRISE /s $SIEBSRVR_NAME /u $SADMUSR /i $BASEPATH/sblsrvr/etc/probe_cmds 2>&1 <<-EOF
	$SADMPWD
	EOF`

	rc2=`echo "$STATUS" | grep -iv "not running" | egrep -c "Running"`

	# rc is opposite of rc2
	rc=`expr $rc2 == 0`
fi

if [ $rc -ne 0 ]
then
	# SCMSGS
	# @explanation
	# Siebel server may not be running.
	# @user_action
	# This is an informative message. Fault Monitor should either restart
	# or failover the Siebel server resource. This message may also be
	# generated during the start method while waiting for the service to
	# come up.
	scds_syslog -p error -t $syslog_tag -m \
		"Siebel server not running."
	rc=1
else
	rc=`echo "$STATUS" | grep Enabled | egrep -c \(Unavailable\|offline\)`
	if [ $rc -ne 0 ]
	then
		# SCMSGS
		# @explanation
		# Not all of the enabled Siebel server components are running.
		# @user_action
		# This is an informative message. Fault Monitor will not take
		# any action. Please manually start the Siebel component(s)
		# that may have gone down to ensure complete service.
		scds_syslog -p error -t $syslog_tag -m \
			"Siebel server components maybe unavailable or offline. No action will be taken."
		rc=$PARTIAL_FAILURE
	fi
fi

exit $rc
