/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2002 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_SBSRVR_COMMON_H
#define	_SBSRVR_COMMON_H

#pragma ident	"@(#)sblsrvr.h	1.7	07/06/06 SMI"

#include <rgm/libdsdev.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Debug levels for error messages */
#define	DBG_LEVEL_HIGH		9
#define	DBG_LEVEL_MED		5
#define	DBG_LEVEL_LOW		1

#define	SCDS_CMD_SIZE		(8*1024)

#define	SCDS_ARRAY_SIZE		1024

/*
 * Used by start and probe to indicate that database or gateway
 * may be down. This value should be greater than that for
 * complete failure (100). Otherwise, change may be required
 * in the probe's main() method.
 */
#define	DB_OR_GTWY_DOWN		200


int svc_validate(scds_handle_t scds_handle, boolean_t print_msgs);

int svc_start(scds_handle_t scds_handle);

int svc_stop(scds_handle_t scds_handle);

int svc_wait(scds_handle_t scds_handle);

int mon_start(scds_handle_t scds_handle);

int mon_stop(scds_handle_t scds_handle);

int svc_probe(scds_handle_t scds_handle, int timeout, boolean_t print_msgs);

int validate_permissions(char *rootdir, char *cmd, uid_t u_uid, gid_t u_gid,
	scds_hasp_status_t hasp_status, boolean_t print_msgs);

#ifdef __cplusplus
}
#endif

#endif /* _SBSRVR_COMMON_H */
