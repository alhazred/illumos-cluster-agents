/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#pragma ident	"@(#)sblgtwy.c	1.11	07/06/06 SMI"

/*
 * sblgtwy.c - Common utilities for sblgtwy
 *
 * This utility has the methods for performing the validation, starting and
 * stopping the data service and the fault monitor. It also contains the method
 * to probe the health of the data service.  The probe just returns either
 * success or failure. Action is taken based on this returned value in the
 * method found in the file sblgtwy_probe.c
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <unistd.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/statvfs.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <pwd.h>
#include <scha.h>
#include <libintl.h>
#include <rgm/libdsdev.h>
#include <errno.h>
#include <dlfcn.h>
#include "sblgtwy.h"

/*
 * The initial timeout allowed  for the sblgtwy dataservice to
 * be fully up and running. We will wait for for 3 % (SVC_WAIT_PCT)
 * of the start_timeout time before probing the service.
 */
#define	SVC_WAIT_PCT		3

/*
 * We need to wait for SVC_WAIT_TIME ( 2 secs) for pmf
 * to send the failure message before probing the service
 */

#define	SVC_WAIT_TIME		5

/*
 * Percentage for clean shutdown attempt (soft) *
 */
#define	SOFT_STOP_PCT		85

/* Max length of user login name */
#define	MAX_LOGIN_LEN		256

static scds_hasp_status_t hasp_status = SCDS_HASP_NO_RESOURCE;
static const char *rtbasedir = NULL;
static char grootdir[MAXPATHLEN + 1] = "";
static char suser[MAX_LOGIN_LEN + 1] = "";
static uid_t u_uid;
static gid_t u_gid;

/*
 * get_props_and_suser()
 *
 * Get various extension properties and username for
 * starting the gateway, and store them in static variables.
 */
int
get_props_and_suser(scds_handle_t scds_handle, boolean_t print_msgs)
{
	struct passwd *pwdbuf = NULL;
	scha_str_array_t *confdirs = NULL;
	struct statvfs statvfsbuf;
	struct stat statbuf;
	char cmd_path[SCDS_CMD_SIZE] = "";
	int rc = 0;


	confdirs = scds_get_ext_confdir_list(scds_handle);

	/* Return an error if there are no config_dirs x prop */
	if (confdirs == NULL || confdirs->array_cnt == 0) {
		scds_syslog(LOG_ERR,
			"Property %s is not set.",
			"Confdir_list");
		if (print_msgs)
			(void) fprintf(stderr,
				gettext("Property %s is not set.\n"),
				"Confdir_list");
		return (1);
	}

	rc = snprintf(grootdir, sizeof (grootdir), "%s",
		confdirs->str_array[0]);
	if (rc == -1) {
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
			"String handling error creating path to "
			"gateway root dir. The path may be too long");
		if (print_msgs)
			(void) fprintf(stderr,
				gettext("INTERNAL ERROR: %s.\n"),
				gettext("String handling error creating "
				"path to gateway root dir. The path may be "
				"too long"));
		return (1);
	}

	/*
	 * Make sure that siebenv.sh exists and that the
	 * permissions are correct.
	 */
	rc = snprintf(cmd_path, sizeof (cmd_path), "%s/%s",
		grootdir, "siebenv.sh");
	if (rc == -1) {
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
			"String handling error while copying path "
			"for siebenv.sh. The path may be too long");
		if (print_msgs)
			(void) fprintf(stderr,
				gettext("INTERNAL ERROR: %s.\n"),
				gettext("String handling error while "
				"copying path for siebenv.sh. The path "
				"may be too long"));
		return (1);
	}

	if (statvfs(cmd_path, &statvfsbuf) != 0) {
		if ((hasp_status == SCDS_HASP_ONLINE_NOT_LOCAL) &&
			(errno == ENOENT)) {	/*lint !e746 */
			return (0);
		} else {
			scds_syslog(LOG_ERR,
				"Cannot access file <%s>, err = <%s>",
				cmd_path, strerror(errno));
			if (print_msgs)
				(void) fprintf(stderr,
					gettext("Cannot access file "
					"<%s>, err = <%s>\n"),
					cmd_path,
					gettext(strerror(errno)));
			return (1);
		}
	}

	if (access(cmd_path, (R_OK | X_OK)) != 0) {
		scds_syslog(LOG_ERR,
			"No permission for owner to execute %s.",
			"siebenv.sh");
		if (print_msgs)
			(void) fprintf(stderr,
				gettext("No permission for owner to "
				"execute %s.\n"), "siebenv.sh");
		return (1);
	}

	rc = stat(cmd_path, &statbuf);
	if (rc != 0) {
		scds_syslog(LOG_ERR,
			"stat of file %s failed: <%s>.",
			cmd_path, strerror(errno));
		if (print_msgs)
			(void) fprintf(stderr,
				gettext("stat of file %s failed: <%s>.\n"),
				cmd_path, gettext(strerror(errno)));
		return (1);
	}

	/*
	 * Get username of the owner of the siebenv.sh file.
	 * This user will launch the start command.
	 */
	u_uid = statbuf.st_uid;
	pwdbuf = getpwuid(u_uid);

	/*
	 * getpwuid() uses static memory. Therefore, copy the
	 * returned value in new variable.
	 */
	if (pwdbuf == NULL ||
		strlcpy(suser, pwdbuf->pw_name, sizeof (suser))
		>= sizeof (suser)) {
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
			"String handling error while copying "
			"username. The username may be too long");
		if (print_msgs)
			(void) fprintf(stderr,
				gettext("INTERNAL ERROR: %s.\n"),
				gettext("String handling error while "
				"copying username. The username "
				"may be too long"));
		return (1);
	}

	/* User's GID */
	u_gid = (*pwdbuf).pw_gid;

	/* Make sure that siebns.dat file is readable/writable */
	rc = snprintf(cmd_path, sizeof (cmd_path),
		"%s/sys/siebns.dat", grootdir);
	if (rc == -1) {
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
			"String handling error while copying path "
			"for siebns.dat. The path may be too long");
		if (print_msgs)
			(void) fprintf(stderr,
				gettext("INTERNAL ERROR: %s.\n"),
				gettext("String handling error while "
				"copying path for siebns.dat. The path "
				"may be too long"));
		return (1);
	}

	if (statvfs(cmd_path, &statvfsbuf) != 0) {
		if ((hasp_status == SCDS_HASP_ONLINE_NOT_LOCAL) &&
			(errno == ENOENT)) {    /*lint !e746 */
			return (0);
		} else {
			scds_syslog(LOG_ERR,
				"Cannot access file <%s>, err = <%s>",
				cmd_path, strerror(errno));
			if (print_msgs)
				(void) fprintf(stderr,
					gettext("Cannot access file "
					"<%s>, err = <%s>\n"),
					cmd_path,
					gettext(strerror(errno)));
			return (1);
		}
	}


	rc = stat(cmd_path, &statbuf);
	if (rc != 0) {
		scds_syslog(LOG_ERR,
			"stat of file %s failed: <%s>.",
			cmd_path, strerror(errno));
		if (print_msgs)
			(void) fprintf(stderr,
				gettext("stat of file %s failed: <%s>.\n"),
				cmd_path, gettext(strerror(errno)));
		return (1);
	}

	/* Check if owner has read/write permissions */
	if (statbuf.st_uid != u_uid) {
		/*
		 * SCMSGS
		 * @explanation
		 * A program required the specified file to be owned by the
		 * specified user.
		 * @user_action
		 * Use chown command to change to owner as suggested.
		 */
		scds_syslog(LOG_ERR, "File %s should be owned "
			"by %s.", cmd_path, suser);
		if (print_msgs)
			(void) fprintf(stderr,
				gettext("File %s should be owned "
				"by %s.\n"), cmd_path, suser);
		return (1);
	}
	if (!(statbuf.st_mode & S_IRUSR|S_IWUSR)) {
		/*
		 * SCMSGS
		 * @explanation
		 * A program required the specified file to be readable and
		 * writable by the specified user.
		 * @user_action
		 * Set correct permissions for the specified file to allow the
		 * specified user to read it and write to it.
		 */
		scds_syslog(LOG_ERR, "File %s should be readable "
			"and writable by %s.", cmd_path, suser);
		if (print_msgs)
			(void) fprintf(stderr,
				gettext("File %s should be readable "
				"and writable by %s.\n"),
				cmd_path, suser);
		return (1);
	}

	return (0);
}


/*
 * svc_validate():
 *
 * - Verify extension properties.
 * - Get owner of siebenv.sh file.
 * - Verify if the above user has permissions for all
 *   executables that will be used.
 * - Store some values in static variables for use in other functions.
 *
 */

int
svc_validate(scds_handle_t scds_handle, boolean_t print_msgs)
{
	scds_net_resource_list_t *snrlp = NULL;
	int rc = 0;


	/* check for HAStoragePlus resources */
	rc = scds_hasp_check(scds_handle, &hasp_status);
	if (rc != SCHA_ERR_NOERR) {
		/* scha_hasp_check() logs everytime it fails */
		if (print_msgs)
			(void) fprintf(stderr,
				gettext("INTERNAL ERROR: %s.\n"),
				gettext("scds_hasp_check failed"));
		return (1);
	}

	if (hasp_status == SCDS_HASP_NOT_ONLINE) {
		scds_syslog(LOG_ERR, "Resource depends on a "
			"SUNW.HAStoragePlus type resource that is "
			"not online anywhere.");
		if (print_msgs)
			(void) fprintf(stderr, gettext("Resource depends "
				"on a SUNW.HAStoragePlus type resource "
				"that is not online anywhere.\n"));
		return (1);
	} else if (hasp_status == SCDS_HASP_ERR_CONFIG) {
		/* problem syslogged by scds_hasp_check */
		return (1);
	}

	/*
	 * Get various extension properties and username for
	 * starting the gateway.
	 */
	rc = get_props_and_suser(scds_handle, print_msgs);
	if (rc != 0)
		return (1);

	/*
	 * Make sure that various Siebel gateway command files
	 * exist and that their permissions are correct.
	 * validate_permissions() logs error message.
	 */

	if (validate_permissions(grootdir, "start_ns",
		u_uid, u_gid, hasp_status, print_msgs) != 0)
		return (1);
	if (validate_permissions(grootdir, "stop_ns",
		u_uid, u_gid, hasp_status, print_msgs) != 0)
		return (1);
	if (validate_permissions(grootdir, "srvredit",
		u_uid, u_gid, hasp_status, print_msgs) != 0)
		return (1);

	/*
	 * Return an error if there is an error when trying to get the
	 * available network address resources for this resource
	 */
	if ((rc = scds_get_rs_hostnames(scds_handle, &snrlp))
		!= SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
			"Error in trying to access the configured network "
			"resources : %s.", scds_error_string(rc));
		if (print_msgs)
			(void) fprintf(stderr,
				gettext("Error in trying to access the "
				"configured network resources : %s.\n"),
				gettext(scds_error_string(rc)));
		rc = 1;
		goto finished_validate;
	}

	/* Return an error if there are no network address resources */
	if (snrlp == NULL || snrlp->num_netresources == 0) {
		scds_syslog(LOG_ERR,
			"No network address resource in resource group.");
		if (print_msgs)
			(void) fprintf(stderr,
				gettext("No network address resource in "
				"resource group.\n"));
		rc = 1;
		goto finished_validate;
	}

	/* Check to make sure other important extension props are set */
	if (scds_get_ext_monitor_retry_count(scds_handle) <= 0) {
		scds_syslog(LOG_ERR,
			"Property %s is not set.",
			"Monitor_retry_count");
		if (print_msgs)
			(void) fprintf(stderr,
				gettext("Property %s is not set.\n"),
				"Monitor_retry_count");
		rc = 1;
		goto finished_validate;
	}
	if (scds_get_ext_monitor_retry_interval(scds_handle) <= 0) {
		scds_syslog(LOG_ERR,
			"Property %s is not set.",
			"Monitor_retry_interval");
		if (print_msgs)
			(void) fprintf(stderr,
				gettext("Property %s is not set.\n"),
				"Monitor_retry_interval");
		rc = 1;
		goto finished_validate;
	}

	/* All validation checks were successful */
	scds_syslog(LOG_INFO, "Successful validation.");

finished_validate:
	if (snrlp)
		scds_free_net_list(snrlp);

	return (rc);
}

/*
 * svc_start():
 *
 */

int
svc_start(scds_handle_t scds_handle)
{
	char	cmd[SCDS_CMD_SIZE] = "";
	int	rc = 0;


	/* Get RT base directory */
	rtbasedir = scds_get_rt_rt_basedir(scds_handle);

	/* Construct Start command */
	rc = snprintf(cmd, sizeof (cmd),
		"/bin/su - %s -c \"%s/start_sblgtwy %s\" "
		">/dev/null", suser, rtbasedir, grootdir);
	if (rc == -1) {
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
			"String handling error creating start "
			"command. The command may be too long");
		return (1);
	}

	scds_syslog_debug(DBG_LEVEL_LOW,
		"Starting Siebel gateway under PMF using the "
		"command \"%s\".", cmd);

	/* Start Siebel gateway under PMF */
	rc = scds_pmf_start(scds_handle, SCDS_PMF_TYPE_SVC, 0, cmd, -1);

	if (rc == SCHA_ERR_NOERR) {
		scds_syslog_debug(DBG_LEVEL_LOW,
			"Siebel gateway started under PMF.");
	} else {
		scds_syslog_debug(DBG_LEVEL_LOW,
			"Siebel gateway did not start under PMF.");
	}

	return (rc);
}

/*
 * svc_stop():
 *
 * Stop the Siebel gateway
 * Return 0 on success, > 0 on failures.
 *
 */
int
svc_stop(scds_handle_t scds_handle)
{
	char	cmd[SCDS_CMD_SIZE] = "";
	char	*rsname = NULL, *rgname = NULL;
	int	rc = 0, exit_code = 0,
		stop_timeout, timeout;
	time_t	start_time = time(NULL);
	scds_pmf_status_t status;
	boolean_t print_msgs = B_FALSE;


	/* Get resource and resource group names */
	rsname = (char *)scds_get_resource_name(scds_handle);
	rgname = (char *)scds_get_resource_group_name(scds_handle);

	/* Get Stop_timeout for this resource */
	stop_timeout = scds_get_rs_stop_timeout(scds_handle);

	/* Get RT base directory */
	rtbasedir = scds_get_rt_rt_basedir(scds_handle);

	/*
	 * First take the command out of PMF monitoring,
	 * so that it doesn't keep restarting it.
	 */
	rc = scds_pmf_stop_monitoring(scds_handle, SCDS_PMF_TYPE_SVC, 0);
	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to take "
			"the resource out of PMF control.");
		return (rc);
	} else {
		scds_syslog_debug(DBG_LEVEL_HIGH,
			"Resource taken out of PMF control.");
	}

	/*
	 * Get various extension properties and username for
	 * stopping the server.
	 */
	rc = get_props_and_suser(scds_handle, print_msgs);
	if (rc != 0)
		goto finished_stop;

	/* Construct Stop command */
	rc = snprintf(cmd, sizeof (cmd),
		"/bin/su - %s -c \"%s/stop_sblgtwy %s\" "
		">/dev/null", suser, rtbasedir, grootdir);
	if (rc == -1) {
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
			"String handling error creating stop "
			"command. The command may be too long");
		return (1);
	}

	/* Stop Siebel server */
	timeout = stop_timeout - (time(NULL) - start_time);
	timeout = (SOFT_STOP_PCT * timeout) / 100;
	rc = scds_timerun(scds_handle, cmd, timeout, SIGKILL, &exit_code);
	if (rc != SCHA_ERR_NOERR || exit_code != 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * The specified stop command was unable to stop the specified
		 * resource. A SIGKILL signal will be sent to all the
		 * processes associated with the resource.
		 * @user_action
		 * No action required by the user. This is an informational
		 * message.
		 */
		scds_syslog(LOG_ERR,
			"The stop command \"%s\" failed to stop %s. "
			"Using SIGKILL.", cmd, "Siebel gateway");
	}

finished_stop:
	rc = scds_pmf_get_status(scds_handle, SCDS_PMF_TYPE_SVC,
		SCDS_PMF_SINGLE_INSTANCE, &status);
	if (rc != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * A method could not obtain the status of the service from
		 * PMF. The specific cause for the failure may be logged with
		 * the message.
		 * @user_action
		 * Look in /var/adm/messages for the cause of failure. Save a
		 * copy of the /var/adm/messages files on all nodes. Contact
		 * your authorized Sun service provider for assistance in
		 * diagnosing the problem.
		 */
		scds_syslog(LOG_ERR,
			"Failed to get the pmf_status. "
			"Error: %s.", scds_error_string(rc));
		return (1);
	} else if (status == SCDS_PMF_NOT_MONITORED) {
		/* Clean shutdown had worked */
		return (0);
	}

	rc = scds_pmf_signal(scds_handle,
		SCDS_PMF_TYPE_SVC, 0, SIGKILL, -1);
	if (rc == 1) rc = SCHA_ERR_NOERR; /* tag does not exist */

	if (rc == SCHA_ERR_NOERR) {
		(void) scha_resource_setstatus(rsname, rgname,
			SCHA_RSSTATUS_OFFLINE,
			"Successfully stopped Siebel gateway.");
	} else {
		(void) scha_resource_setstatus(rsname, rgname,
			SCHA_RSSTATUS_FAULTED,
			"Failed to stop Siebel gateway.");
		scds_syslog_debug(DBG_LEVEL_LOW,
			"scds_timerun returned %d.", rc);
	}

	return (rc);
}

/*
 * svc_wait():
 *
 * wait for the data service to start up fully and make sure it is running
 * healthy
 */

int
svc_wait(scds_handle_t scds_handle)
{
	int rc, svc_start_timeout, probe_timeout;
	scds_pmf_status_t status;


	/*
	 * Get the Start method timeout, and the Probe timeout value.
	 */
	svc_start_timeout = scds_get_rs_start_timeout(scds_handle);
	probe_timeout = scds_get_ext_probe_timeout(scds_handle);

	/*
	 * sleep for SVC_WAIT_PCT percentage of start_timeout time
	 * before actually probing the dataservice. This is to allow
	 * the dataservice to be fully up inorder to reply to the
	 * probe. NOTE: the value for SVC_WAIT_PCT could be different
	 * for different dataservices.
	 */
	if (scds_svc_wait(scds_handle, (svc_start_timeout * SVC_WAIT_PCT)/100)
		!= SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
			"Failed to start the service %s.",
			"Siebel gateway server");
		return (1);
	}

	for (;;) {
		/*
		 * probe the data service on the IP address of the
		 * network resource
		 */
		rc = svc_probe(scds_handle, probe_timeout, B_FALSE);
		if (rc == SCHA_ERR_NOERR) {
			return (SCHA_ERR_NOERR);
		}

		/* Log notice indicating that service in not fully up */
		scds_syslog(LOG_NOTICE,
			"Waiting for %s to come up.",
			"Siebel gateway");

		rc = scds_pmf_get_status(scds_handle, SCDS_PMF_TYPE_SVC,
			SCDS_PMF_SINGLE_INSTANCE, &status);
		if (rc != SCHA_ERR_NOERR) {
			scds_syslog(LOG_ERR,
				"Failed to get the pmf_status. "
				"Error: %s.", scds_error_string(rc));
			return (1);
		}

		/* Check if the dataservice is still up and running */
		if (status != SCDS_PMF_MONITORED) {
			/*
			 * SCMSGS
			 * @explanation
			 * The data service may have failed to startup
			 * completely.
			 * @user_action
			 * Look in /var/adm/messages for the cause of failure.
			 * Save a copy of the /var/adm/messages files on all
			 * nodes. Contact your authorized Sun service provider
			 * for assistance in diagnosing the problem.
			 */
			scds_syslog(LOG_ERR,
				"Data service failed to stay up. "
				"Start method failed.");
			return (1);
		}

		/*
		 * Dataservice is still trying to come up. Sleep for a while
		 * before probing again.
		 */
		(void) scds_svc_wait(scds_handle, SVC_WAIT_TIME);

	/* We rely on RGM to timeout and terminate the program */
	}

}

/*
 * This function starts the fault monitor for a sblgtwy resource.
 * This is done by starting the probe under PMF. The PMF tag
 * is derived as <RG-name,RS-name,instance_number.mon>. The restart option
 * of PMF is used but not the "infinite restart". Instead
 * interval/retry_time is obtained from the RTR file.
 */

int
mon_start(scds_handle_t scds_handle)
{
	scha_err_t	err;

	scds_syslog_debug(DBG_LEVEL_HIGH,
		"Calling MONITOR_START method for resource <%s>.",
		scds_get_resource_name(scds_handle));

	/*
	 * The probe sblgtwy_probe is assumed to be available in the same
	 * subdirectory where the other callback methods for the RT are
	 * installed. The last parameter to scds_pmf_start denotes the
	 * child monitor level. Since we are starting the probe under PMF
	 * we need to monitor the probe process only and hence we are using
	 * a value of 0.
	 */
	err = scds_pmf_start(scds_handle, SCDS_PMF_TYPE_MON,
		SCDS_PMF_SINGLE_INSTANCE, "sblgtwy_probe", 0);

	if (err != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to start fault monitor.");
		return (err);
	}

	scds_syslog(LOG_INFO, "Started the fault monitor.");

	return (SCHA_ERR_NOERR);
}


/*
 * This function stops the fault monitor for a sblgtwy resource.
 * This is done via PMF. The PMF tag for the fault monitor is
 * constructed based on <RG-name_RS-name,instance_number.mon>.
 */

int
mon_stop(scds_handle_t scds_handle)
{
	scha_err_t	err;

	scds_syslog_debug(DBG_LEVEL_HIGH, "Calling scds_pmf_stop method");

	err = scds_pmf_stop(scds_handle, SCDS_PMF_TYPE_MON,
		SCDS_PMF_SINGLE_INSTANCE, SIGKILL,
		scds_get_rs_monitor_stop_timeout(scds_handle));

	if (err != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to stop fault monitor.");
		return (err);
	}

	scds_syslog(LOG_INFO, "Stopped the fault monitor.");

	return (SCHA_ERR_NOERR);
}


/*
 * svc_probe(): Do data service specific probing. Return an int value
 * between 0 (success) and 100 (complete failure).
 */
int
svc_probe(scds_handle_t scds_handle, int timeout, boolean_t print_msgs)
{
	static char cmd[SCDS_CMD_SIZE] = "";
	int	rc = 0, exit_code = 0, cmd_timeout;
	time_t	start_time = time(NULL);


	/* Get RT base directory */
	if (rtbasedir == NULL) {
		rtbasedir = scds_get_rt_rt_basedir(scds_handle);
	}

	/*
	 * Get various extension properties and username for
	 * stopping the server.
	 */
	if (grootdir[0] == '\0' || suser[0] == '\0') {
		rc = get_props_and_suser(scds_handle, print_msgs);
		if (rc != 0)
			return (SCDS_PROBE_COMPLETE_FAILURE);
	}

	/* Construct probe command */
	if (cmd[0] == '\0') {
		rc = snprintf(cmd, sizeof (cmd), "/bin/su - %s -c "
			"\"%s/probe_sblgtwy %s\" >/dev/null",
			suser, rtbasedir, grootdir);
		if (rc == -1) {
			scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
				"String handling error creating probe "
				"command. The command may be too long");
			return (SCDS_PROBE_COMPLETE_FAILURE);
		}
	}

	/* Probe Siebel server */
	cmd_timeout = timeout - (time(NULL) - start_time);
	rc = scds_timerun(scds_handle, cmd, cmd_timeout,
		SIGKILL, &exit_code);

	if (rc != SCHA_ERR_NOERR || exit_code != 0) {
		if (rc == SCHA_ERR_TIMEOUT) {
			if (print_msgs)
				scds_syslog(LOG_ERR, "Probe timed out.");
			rc = SCDS_PROBE_COMPLETE_FAILURE/2;
		} else {
			if (print_msgs)
				/*
				 * SCMSGS
				 * @explanation
				 * Probe for the specified service returned
				 * error.
				 * @user_action
				 * Need a user action for this message.
				 */
				scds_syslog(LOG_ERR,
					"Probe for %s returned error.",
					"Siebel gateway");
			scds_syslog_debug(DBG_LEVEL_LOW,
				"The probe command \"%s\" returned "
				"with non-zero status", cmd);
			rc = SCDS_PROBE_COMPLETE_FAILURE;
		}

		return (rc);
	}

	return (SCHA_ERR_NOERR);
}
