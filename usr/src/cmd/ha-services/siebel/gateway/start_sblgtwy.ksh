#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#pragma ident	"@(#)start_sblgtwy.ksh	1.6	07/06/06 SMI"
#
#
# This script is used by the START method for starting Siebel gateway.
#
# Input:
#
# - GATEWAY_ROOT - location of Siebel gateway installation and siebenv.sh
#                  file
#
# Return:
#
# 0 - Success
# 1 - Failure
#

PATH=$PATH:/usr/cluster/lib/sc
syslog_tag="SC[SUNW.sblgtwy,start_sblgtwy]"

if [ $# -ne 1 ]
then
	# SCMSGS
	# @explanation
	# Need explanation of this message!
	# @user_action
	# Need a user action for this message.
	scds_syslog -p error -t $syslog_tag -m \
		"INTERNAL ERROR: usage: `basename $0` <gateway_root>"
	exit 1
fi

GATEWAY_ROOT=$1

. $GATEWAY_ROOT/siebenv.sh
rc=$?

if [ $rc -ne 0 ]
then
	scds_syslog -p error -t $syslog_tag -m \
		"Error while executing siebenv.sh."
	exit 1
fi

srvredit -q -g $SIEBEL_GATEWAY -e none -z -c '$Gateway.VersionString' > /dev/null
rc=$?

if [ $rc -eq 0 ]
then
	# SCMSGS
	# @explanation
	# Siebel gateway was not expected to be running. This may be due to
	# the gateway having started outside Sun Cluster control.
	# @user_action
	# Please shutdown the gateway instance manually, and retry the
	# previous operation.
	scds_syslog -p error -t $syslog_tag -m \
		"Siebel gateway already running."
	exit 1
fi

STATUS=`start_ns 2> /dev/null`
rc=$?

exit $rc
