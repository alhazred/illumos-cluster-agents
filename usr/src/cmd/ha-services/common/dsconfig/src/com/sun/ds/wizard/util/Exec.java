/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * ident	"@(#)Exec.java	1.5	07/06/06 SMI"
 *
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

package com.sun.ds.wizard.util;

import java.util.*;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.io.InputStream;
import java.io.OutputStream;


/**
 * Execute a command
 */
public class Exec {
    private Process underlyingProcess = null;
    final HashMap results = new HashMap();

    private ThreadGroup threadGroup = null;

    /**
     * Default Constructor.
     * Creates a new <code>Process</code> object
     * @param argv array of <code>String</code> containing the command to call
     * 		and its arguments
     * @param envp array of <code>String</code>, each element of which has
     * 		environment variables settings in format
     * 		<code>name=value</code>. If null, an empty environment
     * 		will be set.
     * @throws IOException If an I/O error occurred.
     */
    public Exec(String[] argv,
            String[] envp)
            throws IOException {

        String[] internalArgv = argv;
        threadGroup = new ThreadGroup("HANFS");
	/**
	 * do not allow the daemon environment to be inherited -
	 * if no environment is specified, set the environment to
	 * be empty
	 */
        if (envp == null) {
            envp = new String[] { };
        }

        underlyingProcess = Runtime.getRuntime().exec(internalArgv, envp);

    }
    /**
     * Kills the subprocess. The subprocess represented by this
     * <code>Process</code> object is forcibly terminated.
     */
    public void destroy() {
        underlyingProcess.destroy();
    }

    /**
     * Returns the exit value for the subprocess.
     * @return The exit value of the subprocess represented by this
     * 		<code>Process</code> object. By convention, the value
     * 		<code>0</code> indicates normal termination.
     */
    public int exitValue() {
        return underlyingProcess.exitValue();
    }

    /**
     * Gets the error stream of the subprocess.
     * The stream obtains data piped from the error output stream of the
     * process represented by this <code>Process</code> object.
     * <p>
     * Implementation note: It is a good idea for the input stream to be
     * buffered.
     * @return the input stream connected to the error stream of the subprocess
     */
    public InputStream getErrorStream() {
        return underlyingProcess.getErrorStream();
    }

    /**
     * Gets the input stream of the subprocess.
     * The stream obtains data piped from the standard output stream
     * of the process represented by this <code>Process</code> object.
     * <p>
     * Implementation note: It is a good idea for the input stream to be
     * buffered.
     * @return the input stream connected to the normal output of the subprocess
     */
    public InputStream getInputStream() {
        return underlyingProcess.getInputStream();
    }
    /**
     * Gets the output stream of the subprocess.
     * Output to the stream is piped into the standard input stream of
     * the process represented by this <code>Process</code> object.
     * <p>
     * Implementation note: It is a good idea for the output stream to be
     * buffered.
     * @return the output stream connected to the normal input of the
     * 		subprocess.
     */
    public OutputStream getOutputStream() {
        return underlyingProcess.getOutputStream();
    }

    /**
     * Causes the current thread to wait, if necessary, until the
     * process represented by this <code>Process</code> object has
     * terminated. This method returns immediately if the subprocess has already
     * terminated. If the subprocess has not yet terminated, the calling thread
     * will be blocked until the subprocess exits.
     * @return the exit value of the process. By convention, <code>0</code>
     * 		indicates normal termination.
     * @throws InterruptedException If the current thread is
     * 		{@link Thread#interrupt() interrupted} by another thread
     * 		while it is waiting, then the wait is ended and an
     * 		{@link InterruptedException} is thrown.
     */
    public int waitFor() throws InterruptedException {
        return underlyingProcess.waitFor();
    }

    /**
     * Returns the buffered stream as a string.
     * @return the buffered sream as a <code>String</code>.
     * @param key Determines if output or error data
     */
    public String getResult(String key) {
        return (String)results.get(key);
    }
    /**
     * A Set of threads that are currently running.
     * This set is also used as a lock to synchronize
     * anything that touches running threads.
     */
    private HashSet runningThreads = new HashSet();

    /**
     * A queue of jobs that have not yet been started.
     */
    private LinkedList toRunQueue = new LinkedList();

    /**
     * Run the stream gobbler thread. If this method throws an error, that
     * error may be handled and this method may be called again as it will not
     * rethrow the same instance of the error.
     * @param stream Input stream determines output and error stream
     * @param threadName name for the thread that will be created to run
     * the thread
     */
    public void streamGlobbler(final InputStream stream,
				final String threadName) {
        throwFirstError();

        Runnable gobble = new Runnable() {
            public void run() {
                try {
                    StringWriter out = new StringWriter();
                    try {
                        BufferedReader reader =
                                new BufferedReader(new
					InputStreamReader(stream));

                        char[] buf = new char[1024];

                        while (true) {
                            int count = reader.read(buf);
                            if (count == -1)
                                break;
                            out.write(buf, 0, count);
                        }
                    } catch (Exception e) {

                    } finally {
                        results.put(threadName, out.toString());
                    }

                } catch (RuntimeException runtimeException) {
                    // Put exceptions in the exception queue
                    synchronized (runningThreads) {
                        exceptionList.add(runtimeException);
                    }
                } catch (Error error) {
                    // Put errors in the error queue
                    synchronized (runningThreads) {
                        errorList.add(error);
                    }
                } finally {
                    synchronized (runningThreads) {
                        // when done remove ourselves from the list
                        // of running threads.
                        runningThreads.remove(Thread.currentThread());
                        // Notify the block method.
                        runningThreads.notifyAll();
                    }
                }
            }
        };

        // ensure the thread name is not null, and auto generate a name if it is
        // threadName = getNextThreadName(threadName);

        // If we are already running the max number of jobs, queue this job up
        synchronized (runningThreads) {
            toRunQueue.add(
                    new Thread(
                    threadGroup,
                    gobble,
		    threadName));
        }

        // check the queue and see if the job should be started
        startStreamGlobber();
    }

    /**
     * An number to assign to the next auto generated thread name
     */
    private static int threadNameCount = 0;

    /**
     * Ensure the given thread name is not null.  If not null, return it,
     * if it is null, then then generate a name.
     *
     * @param threadName existing thread name to check
     * @return the given thread name or a generated thread name
     * if the specified name was null.
     */
    private static String getNextThreadName(String threadName) {
        if (threadName != null)
		return threadName;
        return "executor-"+(threadNameCount++);
    }

    /**
     * A queue of exceptions that running threads have thrown.
     */
    private LinkedList exceptionList = new LinkedList();

    /**
     * Remove the first exception from the exception list and throw it.
     *
     * @throws RuntimeException if a running thread has thrown an
     * exception not yet thrown by this method.
     */
    private void throwFirstException() {
        synchronized (runningThreads) {
            if (exceptionList.size() > 0) {
                throw (RuntimeException)exceptionList.removeFirst();
            }
        }
    }

    /**
     * A queue of exceptions that running threads have thrown.
     */
    private LinkedList errorList = new LinkedList();

    /**
     * Remove the first error from the error list and throw it.
     *
     * @throws Error if a running thread has thrown an error
     * not yet thrown by this method.
     */
    private void throwFirstError() throws Error {
        synchronized (runningThreads) {
            if (errorList.size() > 0) {
                throw (Error)errorList.removeFirst();
            }
        }
    }

    /**
     * Remove a job from the toRunQueue, create a thread for it,
     * start the thread, and put the job in the set of running jobs.
     * But do all this only if there are jobs queued up to be run
     * and we are not already running the max number of concurrent
     * jobs at once.
     */
    private void startStreamGlobber() {
        synchronized (runningThreads) {
            // If there are no more job to run, return
            if (toRunQueue.size() == 0)
		return;

            // Get a job out of the queue
            Thread thread = (Thread)toRunQueue.removeFirst();

            // Put the thread in the list of running threads
            runningThreads.add(thread);
            thread.start();
        }
    }

    /**
     * Return true if all streamer that have been requested to run
     * in this executor have completed.
     * <p>
     * If this method throws an error, that
     * error may be handled and this method
     * may be called again as it will not rethrow the same
     * instance of the error.
     * @return Whether all streams are done or not.
     */
    public boolean done() {
        throwFirstError();
        synchronized (runningThreads) {
            return (toRunQueue.size() + runningThreads.size()) == 0;
        }
    }

    /**
     * All currently running threads will be interrupted.
     * The threads interrupted threads may die, causing
     * jobs that were queued but not yet started, to start.
     * <p>
     * If this method throws an error, that
     * error may be handled and this method
     * may be called again as it will not rethrow the same
     * instance of the error.
     */
    public void interrupt() {
        throwFirstError();
        synchronized (runningThreads) {
            for (Iterator i = runningThreads.iterator(); i.hasNext(); ) {
                ((Thread)i.next()).interrupt();
                throwFirstError();
            }
        }
    }

    /**
     * Dump the stack of each running thread.
     * <p>
     * If this method throws an error, that
     * error may be handled and this method
     * may be called again as it will not rethrow the same
     * instance of the error.
     */
    public void dumpStack() {
        throwFirstError();
        synchronized (runningThreads) {
            for (Iterator i = runningThreads.iterator(); i.hasNext(); ) {
                ((Thread)i.next()).dumpStack();
                throwFirstError();
            }
        }
    }

    /**
     * Gets a list of all running threads.
     * <p>
     * If this method throws an error, that
     * error may be handled and this method
     * may be called again as it will not rethrow the same
     * instance of the error.
     * @throws Error if any of the running threads has thrown an Error.
     * @return an array of all currently running threads.
     */
    public Thread[] getRunningThreads() {
        throwFirstError();
        synchronized (runningThreads) {
            return (Thread[])runningThreads.toArray(new Thread[0]);
        }
    }

    /**
     * Block until all the threads in this executor have run
     * and then return.
     * <p>
     * If this method throws an exception or an error, that
     * exception or error may be handled and this method
     * may be called again as it will not rethrow the same
     * instance of the exception or error.
     * @throws InterruptedException if interrupted while waiting.
     */
    public void join() throws InterruptedException {
        while (!done()) {
            synchronized (runningThreads) {
                throwFirstException();
                runningThreads.wait();
                throwFirstError();
                throwFirstException();
            }
        }
    }

    /**
     * Test Program
     * @param argv arguements to test program
     * @throws java.lang.Exception Exception
     */
    public static void main(String[] argv) throws Exception {
        Exec ps = null;
        String[] cmd = {"/bin/ksh", " date"};
        ps = new Exec(cmd, null);
        ps.streamGlobbler(ps.getInputStream(), "Output");
        ps.streamGlobbler(ps.getErrorStream(), "Error");
        try {
            ps.waitFor();
            ps.join();
            System.out.println(ps.getResult("Output"));
            System.out.println(ps.getResult("Error"));

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(ps.exitValue());
    }
}
