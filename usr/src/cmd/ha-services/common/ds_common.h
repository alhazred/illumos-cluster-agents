/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2004 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef _DS_COMMON_H
#define	_DS_COMMON_H

#pragma ident	"@(#)ds_common.h	1.11	07/06/06 SMI"

/*
 * Common routines shared by data services.
 */

#ifdef __cplusplus
extern "C" {
#endif

#include <stdarg.h>
#include <scha.h>
#include <libdsdev.h>

#define	DS_STRING_FORMAT_SIZE	128
#define	DS_ARRAY_SIZE		1024

/*
 * String and log message handling.
 */
char *ds_string_format(const char *format, ...);
char *ds_string_format_v(const char *format, va_list ap);
void ds_internal_error(const char *format, ...);

scha_err_t ds_http_get_status(scds_handle_t handle, const char *uri, int
	*status, time_t timeout, boolean_t log_messages);
scha_err_t ds_parse_simple_uri(const char *uri, char **scheme,
	char **hostname, char **port, char **path);

/*
 * File and executable validation routines.
 */
int ds_validate_file(int print, mode_t mode, const char *file_format, ...);

scha_err_t
ds_validate_file_owner(int print, uid_t uid, gid_t gid,
	const char *file_format, ...);

/*
 * Get a simple value out of a magnus.conf style file (as used by iWS
 * and S1AS).
 */
scha_err_t ds_get_magnus_value(const char *filename, const char *key,
	char *value);


/* For SMF support */
int os_newer_than_s10(void);
int disable_smf_services(char **svcs);
int check_disabled_smf_services(char **svcs, boolean_t print_msgs);

/* Following required until sol_version.h available - bugid 6201432 */
#ifndef	__s10
#define	__s10	200411
#endif

#ifdef __cplusplus
}
#endif

#endif /* _DS_COMMON_H */
