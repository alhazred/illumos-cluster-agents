/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ds_http.c - Send an HTTP GET request and retrieve response.
 */

#pragma ident	"@(#)ds_http.c	1.17	07/06/06 SMI"

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <rgm/libdsdev.h>
#include "ds_common.h"

#define	DBG_LEVEL_HIGH		9
#define	DBG_LEVEL_LOW		1

/*
 * We need to use 95% of probe_timeout to connect to the port and the
 * remaining time is used to disconnect from port in the svc_probe function.
 */
#define	SVC_CONNECT_TIMEOUT_PCT		95

/*
 * This value will be used as disconnect timeout, if there is no
 * time left from the probe_timeout.
 */
#define	SVC_DISCONNECT_TIMEOUT_SECONDS		2

int build_and_get_status_code(char *buf, char **line,
	char *hostname, int port);

/*
 * Get the HTTP Status-Code out of buf.
 *
 * Status lines are the first line in a response and look like:
 *     Status-Line = HTTP-Version SP Status-Code SP Reason-Phrase CRLF
 * An example of a Status Line would be:
 *	HTTP/1.1 400 Bad Request
 *
 * We just want the status code so we ignore the rest of the line.
 *
 * Return the status code or -1 if the line is not well formed.
 *
 * Note: this mangles the contents of buf.
 */
static int
get_status_code(char *buf)
{
	char	*s;
	int	code;

	s = strchr(buf, ' ');	/* Skip over "HTTP/1.1" */
	if (s == NULL)
		return (-1);
	s++;

	/*
	 * atoi() will stop at the next space so just run it starting
	 * at s and let atoi() figure out where to stop.
	 */
	code = atoi(s);

	/*
	 * If atoi() has an error it returns 0.  In addition anything
	 * below 100 is not a valid response code, and anything over
	 * 999 is also invalid.  Actually HTTP RFC doesn't define any
	 * error codes above 5xx, but why get picky?
	 */
	if (100 <= code && code <= 999)
		return (code);
	else
		return (-1);
}

/*
 * Return true (1) if c is a valid character in a hostname, return
 * false (0) if it is not or is the null character.
 *
 * This function is very permissive.  It allows any character that
 * would not start the port or abs_path portions of a http URL.
 */
static int
hostchar(char c)
{
	if (c == ':' || c == '/' || c == '.' || c == '\0')
		return (0);
	else
		return (1);
}

/*
 * Parse a URI in the format of:
 *	<scheme>://<host>[:<port>][/<path>]
 *
 * Examples:
 *	http://www
 *	http://www.sun.com
 *	http://sun.com/
 *	http://sun.com:80
 *
 * Returns one of SCHA_ERR_NOERR (successful), SCHA_ERR_NOMEM (out of
 * memory), or SCHA_ERR_INVAL (URI syntax error).
 *
 * Scheme and hostname are required.  If port and path are not
 * specified they are set to NULL.
 *
 * On success scheme, hostname, port, and path are allocated if they
 * are present in the URI.  The caller is responsible for freeing
 * them.
 */
scha_err_t
ds_parse_simple_uri(const char *uri, char **scheme, char **hostname,
	char **port, char **path)
{
	char	*p, *s;
	char	*dupuri;
	char	save;
	int	err;

	*scheme = NULL;
	*hostname = NULL;
	*port = NULL;
	*path = NULL;

	if ((dupuri = strdup(uri)) == NULL) {
		err = SCHA_ERR_NOMEM;
		goto error;
	}

	/* Scheme */
	if ((p = strchr(dupuri, ':')) == NULL) { /* find end of "http:" */
		err = SCHA_ERR_INVAL;
		goto error;
	}
	if (!(p[1] == '/' && p[2] == '/')) { /* check we have "http://" */
		err = SCHA_ERR_INVAL;
		goto error;
	}
	/* null terminate scheme portion of string and copy it. */
	*p = '\0';
	if ((*scheme = strdup(dupuri)) == NULL) {
		err = SCHA_ERR_NOMEM;
		goto error;
	}

	/*
	 * Host
	 *
	 * Skip past :// to beginning of host then walk string until
	 * we get to the end of the string or a non host character
	 */
	for (p += 3, s = p; hostchar(*p); p++)
		;
	/*
	 * If p is still equal to s we didn't find any legal host
	 * characters.
	 */
	if (s == p) {		/* no host specified */
		err = SCHA_ERR_INVAL;
		goto error;
	}

	save = *p;		/* remember what char stopped us. */

	/* null terminate host portion of string and copy it. */
	*p = '\0';
	if ((*hostname = strdup(s)) == NULL) {
		err = SCHA_ERR_NOMEM;
		goto error;
	}

	/*
	 * Skip to the end of the FQDN (if used).
	 */
	while (save == '.') {
		*p = save;
		for (p++; hostchar(*p); p++)
			;
		save = *p;
	}

	/* Optional Port */
	if (save == ':') {
		/*
		 * Walk string until we run out of digits, which
		 * includes hitting the end of the string.
		 */
		for (p++, s = p; isdigit((int)*p); p++)
			;
		if (s == p) {	/* :, but no port */
			err = SCHA_ERR_INVAL;
			goto error;
		}
		save = *p;
		*p = '\0';
		if ((*port = strdup(s)) == NULL) {
			err = SCHA_ERR_NOMEM;
			goto error;
		}
	}

	/* Optional Path - if it's there assume rest of string is path */
	if (save == '/') {
		*p = save;
		if ((*path = strdup(p)) == NULL) {
			err = SCHA_ERR_NOMEM;
			goto error;
		}
	}

	/*
	 * In the case of no error return now before hitting error:
	 * below
	 */
	free(dupuri);
	return (SCHA_ERR_NOERR);

	/* We only goto here in the case of an error */
error:
	/* We can free all of these even if they are NULL */
	free(dupuri);
	free(*scheme);
	free(*hostname);
	free(*port);
	free(*path);
	return (err);
}

/*
 * Do a HTTP/1.1 GET request for uri.
 *
 * If everything "works": connect/write/read/disconnect is successful
 * the return value will be SCHA_ERR_NOERR.
 *
 * Other return values and suggested failure levels:
 *	SCHA_ERR_NOMEM		out of memory		0
 *	SCHA_ERR_INVAL		uri syntax was bad	0
 *	SCHA_ERR_INTERNAL	*status is NULL		0
 *
 * Also this function calls the
 * scds_fm_tcp_[connect|write|read|disconnect] functions.  If they
 * fail we return the error code they returned to us.  Most notably
 * they return SCHA_ERR_TIMEOUT.
 *
 * Errors are syslogged by this function.  If log is 0 error log
 * messages that deal with network errors are suppressed, but errors
 * about bad arguments, internal errors, or no memory are logged.
 * This will silence the messages you would expect to get using this
 * function while waiting for a web server to start.
 *
 * *status, is set to the HTTP status code if known, otherwise it will
 * be set to a value < 0.
 *
 * Usage:
 *
 * rc = ds_http_get_status(scds_handle, "http://www.sun.com/", &status,
 *	60, B_TRUE);
 * if (rc == SCHA_ERR_NOERR) {
 *	Look at status, whatever...
 * } else {
 *	Error
 * }
 *
 */
scha_err_t
ds_http_get_status(scds_handle_t handle, const char *uri, int *status,
	time_t timeout, boolean_t log_messages)
{
	char	*scheme;
	char	*hostname;
	char	*portstr;
	char	*path;
	int	port;

	int	sock;
	char	buf[DS_ARRAY_SIZE+1]; /* need space for end of string */
	char	*line = NULL;
	char	*request = NULL;
	char	*request_log;
	size_t	size;

	int	rc = SCHA_ERR_NOERR;

	time_t	connect_timeout;
	ulong_t	t1, t2;
	int	time_used;
	time_t	time_remaining;

	if (status == NULL) {
		ds_internal_error("ds_http_get_status() called with status "
			"set to NULL");
		return (SCHA_ERR_INTERNAL);
	}
	if (uri == NULL) {
		ds_internal_error("ds_http_get_status() called with uri "
			"set to NULL");
		return (SCHA_ERR_INTERNAL);
	}


	*status = -1;

	/*
	 * Use the SVC_CONNECT_TIMEOUT_PCT percentage of timeout
	 * to connect to the port
	 */
	connect_timeout = ((SVC_CONNECT_TIMEOUT_PCT * timeout)/100);
	t1 = (ulong_t)(gethrtime()/1E9);

	rc = ds_parse_simple_uri(uri, &scheme, &hostname, &portstr, &path);
	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Error parsing URI: %s", uri);
		goto finished_no_connection;
	}
	/*
	 * The next two checks should already be taken care of by the
	 * parse returning an error, but being paranoid is safe.
	 */
	if (scheme == NULL) {
		ds_internal_error("ds_http_get_status(): scheme is NULL");
		rc = SCHA_ERR_INVAL;
		goto finished_no_connection;
	}
	if (hostname == NULL) {
		ds_internal_error("ds_http_get_status(): hostname is NULL");
		rc = SCHA_ERR_INVAL;
		goto finished_no_connection;
	}

	if ((strcasecmp(scheme, "http")) != 0) {
		scds_syslog(LOG_ERR,
			"URI (%s) must be an absolute http URI.");
		rc = SCHA_ERR_INVAL;

		goto finished_no_connection;
	}

	if (path == NULL) {	/* empty path */
		if ((path = strdup("/")) == NULL) {
			rc = SCHA_ERR_NOMEM;
			goto finished_no_connection;
		}
	}

	if (portstr != NULL)
		port = atoi(portstr);
	else
		port = 80;
	/*
	 * atoi() returns zero if it has problems converting, also
	 * ports under 1 are invalid.
	 */
	if (port < 1) {
		rc = SCHA_ERR_INVAL;
		goto finished_no_connection;
	}

	scds_syslog_debug(DBG_LEVEL_HIGH, "URI parsed to: %s %s %d %s",
		scheme, hostname, port, path);

	/*
	 * the probe makes a connection to the specified hostname and port.
	 * The connection is timed for 95% of the actual probe_timeout.
	 */
	rc = scds_fm_tcp_connect(handle, &sock, hostname, port,
		connect_timeout);
	if (rc != SCHA_ERR_NOERR) {
		if (log_messages)
			scds_syslog(LOG_ERR,
				"Failed to connect to host %s and "
				"port %d: %s.",
				hostname, port,
				scds_error_string(rc));
		goto finished_no_connection;
	} else {
		scds_syslog_debug(DBG_LEVEL_LOW,
			"Successful connection to server %s "
			"port %d.",
			hostname, port);
	}

	t2 = (ulong_t)(gethrtime()/1E9);

	/*
	 * Compute the actual time it took to connect. This should be
	 * less than or equal to connect_timeout, the time allocated
	 * to connect.  If the connect uses all the time that is
	 * allocated for it, then the remaining value from the
	 * probe_timeout that is passed to this function will be used
	 * as disconnect timeout. Otherwise, the the remaining time
	 * from the connect call will also be added to the disconnect
	 * timeout.
	 */
	time_used = (int)(t2 - t1);

	/*
	 * Use the remaining time(timeout - time_took_to_connect) to
	 * do a HTTP GET and then disconnect.
	 */
	time_remaining = timeout - time_used;

	/*
	 * If all the time is used up, use a small hardcoded timeout
	 * to still try to disconnect. This will avoid the fd leak.
	 */
	if (time_remaining <= 0) {
		if (log_messages)
			/*
			 * SCMSGS
			 * @explanation
			 * The probe used it's entire timeout time to connect
			 * to the HTTP port.
			 * @user_action
			 * Check that the web server is functioning correctly
			 * and if the resources probe timeout is set too low.
			 */
			scds_syslog(LOG_ERR,
				"HTTP GET probe used entire timeout of "
				"%d seconds during connect operation "
				"and exceeded the timeout by %d seconds. "
				"Attempting disconnect with timeout %d",
				connect_timeout,
				abs((int)time_remaining),
				SVC_DISCONNECT_TIMEOUT_SECONDS);

		time_remaining = SVC_DISCONNECT_TIMEOUT_SECONDS;
		goto finished;
	}

	/* Do HTTP GET request. */
	request = ds_string_format(
		"GET %s HTTP/1.1\r\nHost: %s\r\n\r\n", path, hostname);
	if (request == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * A process has failed to allocate new memory, most likely
		 * because the system has run out of swap space.
		 * @user_action
		 * The problem will probably be cured by rebooting. If the
		 * problem reoccurs, you might need to increase swap space by
		 * configuring additional swap devices. See swap(1M) for more
		 * information.
		 */
		scds_syslog(LOG_ERR, "Out of memory.");
		rc = SCHA_ERR_NOMEM;
		goto finished;
	}
	request_log = ds_string_format(
		"GET %s HTTP/1.1 CRLF Host: %s CRLF CRLF", path, hostname);
	if (request_log != NULL) {
		scds_syslog_debug(DBG_LEVEL_HIGH,
			"Sending HTTP GET request: [%s]", request_log);
		free(request_log);
	} else
		scds_syslog_debug(DBG_LEVEL_HIGH, "Sending HTTP GET request");

	size = strlen(request);
	if ((rc = scds_fm_tcp_write(handle, sock, request, &size,
		time_remaining)) != SCHA_ERR_NOERR) {
		/*
		 * write()s should never fail unless the server has
		 * closed its end of the socket.
		 */
		if (log_messages)
			/*
			 * SCMSGS
			 * @explanation
			 * The agent could not send data to the server at the
			 * specified server and port.
			 * @user_action
			 * This is an informational message, no user action is
			 * needed. If the problem persists the fault monitor
			 * will restart or failover the resource group the
			 * server is part of.
			 */
			scds_syslog(LOG_ERR,
				"Write to server failed: "
				"server %s port %d: %s.",
				hostname, port,
				scds_error_string(rc));
		goto finished;
	}

	/*
	 * Data sent to us by server may span several packets, hence
	 * must do things in a loop.
	 */
	do {
		size = sizeof (buf) - 1; /* need space for EOS */

		t2 = (ulong_t)(gethrtime()/1E9);
		time_used = (int)(t2 - t1);
		time_remaining = timeout - time_used;
		if (time_remaining < 1) {
			if (log_messages)
				/*
				 * SCMSGS
				 * @explanation
				 * While reading the response from a request
				 * sent to the server the probe timeout ran
				 * out.
				 * @user_action
				 * Check that the server is functioning
				 * correctly and if the resources probe
				 * timeout is set too low.
				 */
				scds_syslog(LOG_ERR,
					"Reading from server timed out: "
					"server %s port %d", hostname, port);
			rc = SCHA_ERR_TIMEOUT;
			goto finished;
		}

		scds_syslog_debug(DBG_LEVEL_HIGH,
			"Waiting to receive data from %s:%d.",
			hostname, port);

		if ((rc = scds_fm_tcp_read(handle, sock,
			buf, &size, time_remaining)) != SCHA_ERR_NOERR) {
			if (log_messages)
				scds_syslog(LOG_ERR,
					"Failed to communicate with "
					"server %s port %d: %s.",
					hostname, port,
					scds_error_string(rc));
			goto finished;
		}
		buf[size] = '\0'; /* make sure it's a C string */

		/*
		 * If status has not been set this is either the first
		 * time through the loop or the first read did not
		 * have a complete Status Line.
		 */
		if (*status == -1) {
			*status = build_and_get_status_code(buf, &line,
				hostname, port);
		}

		scds_syslog_debug(DBG_LEVEL_HIGH,
			"Received %d bytes from the server %s:%d",
			size, hostname, port);

	} while (size == (int)sizeof (buf));

finished:

	/*
	 * Return partial failure in case of disconnection failure.
	 * Reason: The connect call is successful, which means
	 * the application is alive. A disconnection failure
	 * could happen due to a hung application or heavy load.
	 * If it is the later case, don't declare the application
	 * as dead by returning complete failure. Instead, declare
	 * it as partial failure. If this situation persists, the
	 * disconnect call will fail again and the application will be
	 * restarted.
	 */
	rc = scds_fm_tcp_disconnect(handle, sock, time_remaining);
	if (rc != SCHA_ERR_NOERR) {
		if (log_messages)
			scds_syslog(LOG_ERR,
				"Failed to disconnect from host %s "
				"and port %d.", hostname, port);
		goto finished_no_connection;
	}

	t2 = (ulong_t)(gethrtime()/1E9);
	time_used = (int)(t2 - t1);
	time_remaining = timeout - time_used;

	/*
	 * If there is no time left, return
	 * SCDS_PROBE_COMPLETE_FAILURE/2 instead. This will make sure
	 * that if this timeout persists, server will be restarted.
	 */
	if (time_remaining <= 0) {
		if (log_messages)
			scds_syslog(LOG_ERR, "Probe timed out.");
		rc = SCHA_ERR_TIMEOUT;
	}

finished_no_connection:

	free(line);
	free(request);
	free(scheme);
	free(hostname);
	free(portstr);
	free(path);

	return (rc);
}

/*
 * Build a complete line to check for status code.
 *
 * buf needs to be null terminated.
 *
 * For the first call *line needs to be NULL.  It is used to build up
 * the status line.
 *
 * Returns
 *
 *	-1 if buf does not contain a complete line, function will
 *	continue to build up line during repeated calls.
 *
 *	-2 bad status line or other error.  Do not call again.  Caller
 *	free *line.
 *
 *	1 out of memory.  Do not call again.  Caller free *line.
 *
 *	>= 100 status code of response.  Do not call again.  Caller
 *	frees *line.
 */
int
build_and_get_status_code(char *buf, char **line, char *hostname, int port)
{
	int	code = -20;
	char	*end, *temp;

	/*
	 * Sanity check to make sure *line does not grow without bound
	 * due to server error, or error in building up the line.
	 *
	 * 200 is not magic, it simply significantly bigger then any
	 * reasonable status line would be.
	 */
	if (*line != NULL && (strlen(*line) > 200)) {
		/*
		 * SCMSGS
		 * @explanation
		 * The response to the GET request did not start or had a
		 * malformed status line.
		 * @user_action
		 * Check that the URI being probed is correct and functioning
		 * correctly.
		 */
		scds_syslog(LOG_ERR, "HTTP GET response from %s:%d has "
			"no status line", hostname, port);
		return (-2);
	}

	if (*line == NULL) {
		end = strstr(buf, "\r\n");
		if (end != NULL) /* trunc it to just log the first line */
			*end = '\0';
		scds_syslog_debug(DBG_LEVEL_HIGH, "status line so far: [%s]",
			buf);
		if (end != NULL) /* fix it back up  */
			*end = '\r';

		if (end != NULL) { /* have complete first line */
			code = get_status_code(buf);
			if (code < 100) {
				/*
				 * No well formed Status Line.  Return
				 * -2 so we don't try to get the
				 * status code from other reads.
				 */
				code = -2;
				scds_syslog(LOG_ERR,
					"HTTP GET response from %s:%d has "
					"no status line",
					hostname, port);
			} else {
				scds_syslog_debug(DBG_LEVEL_HIGH,
					"HTTP GET response code from "
					"%s:%d: %d",
					hostname, port, code);
			}
		} else {	/* only have partial line */
			*line = strdup(buf);
			code = -1;
			if (*line == NULL) {
				/* no memory, punt */
				scds_syslog(LOG_ERR, "Out of memory.");
				code = 1;
			}
		}
	} else {
		/* Have a partial line already */
		temp = *line;
		*line = ds_string_format("%s%s", *line, buf);
		free(temp);
		if (*line == NULL) {
			/* no memory, punt */
			scds_syslog(LOG_ERR, "Out of memory.");
			return (1);
		}

		end = strstr(*line, "\r\n");
		if (end != NULL)
			*end = '\0';
		scds_syslog_debug(DBG_LEVEL_HIGH, "status line so far: [%s]",
			*line);
		if (end != NULL)
			*end = '\r';

		if (end != NULL) { /* have complete first line */
			code = get_status_code(*line);
			if (code < 100) {
				/*
				 * No well formed Status Line.  Return
				 * -2 so we don't try to get the
				 * status code from other reads.
				 */
				code = -2;
				scds_syslog(LOG_ERR,
					"HTTP GET response from %s:%d has "
					"no status line",
					hostname, port);
			} else {
				scds_syslog_debug(DBG_LEVEL_HIGH,
					"HTTP GET response code from "
					"%s:%d: %d",
					hostname, port, code);
			}
		} else {
			code = -1;
		}
	}

	return (code);
}
