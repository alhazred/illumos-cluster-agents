/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ds_utils.c - Common data service routines.
 */

#pragma ident	"@(#)ds_utils.c	1.12	07/06/06 SMI"

#include <errno.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <libintl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/statvfs.h>
#include <rgm/libdsdev.h>

/* Following required until sol_version.h available - bugid 6201432 */
#ifndef	__s10
#define	__s10	200411
#endif

#if	(SOL_VERSION >= __s10)
#include <libscf.h>
#endif

#include "ds_common.h"

#define	DBG_LEVEL_HIGH	9

static void log_mode_problem(int print, char *file, mode_t actual,
	mode_t requested);

/*
 * ds_validate_file()
 *
 * Check if a file exists and has at least mode bits set for the
 * file's mode, logging and optionally printing to stderr, appropriate
 * messages.
 *
 * Returns:
 *	SCHA_ERR_NOERR		file exists and has correct mode
 *	SCHA_ERR_NOMEM		no memory to do checks
 *	SCHA_ERR_ACCESS		file doesn't exist or has wrong mode
 *
 * errno is left intact so it can be checked for more info if the file
 * is unable to be accessed.
 *
 * mode could be any valid mode_t (see stat(2)), but we only log
 * specific messages for some possible missing mode bits.  We log
 * specific messages for these frequently checked mode bits:
 *
 *	S_IXUSR			owner execute
 *	S_IRUSR			owner read
 *	S_IWUSR			owner write
 *	S_IXGRP			group execute
 *	S_IRGRP			group read
 *	S_IFREG			ordinary file
 *	S_IFDIR			directory
 *
 * For others (say, S_ISUID - set uid) we would print out an
 * error message.  But the above listed modes are the
 * only one currently checked for by existing Sun Cluster supplied
 * agents.
 *
 * file_format is treated as a printf format string so file and any
 * additional arguments are used to build up the file name.  For
 * example to check to see if Confdir_list[0]/bin/startserv exists and
 * is executable:
 *
 * rc = ds_validate_file(1, S_IXUSR, "%s/bin/startserv",
 *	confdirs->str_array[0]);
 *
 * If we don't care about permissions and just want to check to see if
 * a file exists:
 *
 * rc = ds_validate_file(1, 0, "/etc/system");
 */
scha_err_t
ds_validate_file(int print, mode_t mode, const char *file_format, ...)
{
	char		*file;
	va_list		ap;
	struct stat	statbuf;
	struct statvfs	vfsbuf;

	/* Suppress Undeclared identifier (__builtin_va_alist) */
	va_start(ap, file_format);   /*lint !e40 */
	file = ds_string_format_v(file_format, ap);
	va_end(ap);

	if (file == NULL) {
		/* message already logged */
		if (print)
			(void) fprintf(stderr, gettext("Out of memory.\n"));
		return (SCHA_ERR_NOMEM);
	}

	scds_syslog_debug(DBG_LEVEL_HIGH, "Validating file %s with mode %03o",
		file, mode);

	if (statvfs(file, &vfsbuf) != 0) {
		/* Lint info: ___errno(): no prototype */
		/*
		 * SCMSGS
		 * @explanation
		 * Unable to access the file because of the indicated reason.
		 * @user_action
		 * Check that the file exists and has the correct permissions.
		 */
		scds_syslog(LOG_ERR,
			"Cannot access file: %s (%s)",
			file, strerror(errno)); /*lint !e746 */
		if (print) {
			(void) fprintf(stderr,
				gettext("Cannot access file: %s (%s)\n"),
				file, strerror(errno));
		}
		free(file);
		return (SCHA_ERR_ACCESS);
	}

	if (mode == 0)		/* Not checking mode */
		return (SCHA_ERR_NOERR);

	if (stat(file, &statbuf) != 0) {
		/* Lint info: ___errno(): no prototype */
		scds_syslog(LOG_ERR,
			"Cannot access file: %s (%s)",
			file, strerror(errno)); /*lint !e746 */
		if (print) {
			(void) fprintf(stderr,
				gettext("Cannot access file: %s (%s)\n"),
				file, strerror(errno));
		}
		free(file);
		return (SCHA_ERR_ACCESS);
	}

	if ((statbuf.st_mode & mode) != mode) {
		/*
		 * Some bit in mode is not set in statbuf.st_mode.
		 */
		log_mode_problem(print, file, statbuf.st_mode, mode);

		free(file);
		return (SCHA_ERR_ACCESS);
	}

	free(file);
	return (SCHA_ERR_NOERR);
}

/*
 * Similar function as ds_validate_file() execpt this one checks the uid
 * and gid for the file. If uid or gid is -1, this function will not
 * check it.
 */
scha_err_t
ds_validate_file_owner(int print, uid_t uid, gid_t gid,
	const char *file_format, ...)
{
	char		*file;
	va_list		ap;
	struct stat	statbuf;
	struct statvfs	vfsbuf;

	/* Suppress Undeclared identifier (__builtin_va_alist) */
	va_start(ap, file_format);   /*lint !e40 */
	file = ds_string_format_v(file_format, ap);
	va_end(ap);

	if (file == NULL) {
		/* message already logged */
		if (print)
			(void) fprintf(stderr, gettext("Out of memory.\n"));
		return (SCHA_ERR_NOMEM);
	}

	scds_syslog_debug(DBG_LEVEL_HIGH, "Validating file %s", file);

	if (statvfs(file, &vfsbuf) != 0) {
		/* Lint info: ___errno(): no prototype */
		scds_syslog(LOG_ERR,
			"Cannot access file: %s (%s)",
			file, strerror(errno)); /*lint !e746 */
		if (print) {
			(void) fprintf(stderr,
				gettext("Cannot access file: %s (%s)\n"),
				file, strerror(errno));
		}
		free(file);
		return (SCHA_ERR_ACCESS);
	}

	if (stat(file, &statbuf) != 0) {
		/* Lint info: ___errno(): no prototype */
		scds_syslog(LOG_ERR,
			"Cannot access file: %s (%s)",
			file, strerror(errno)); /*lint !e746 */
		if (print) {
			(void) fprintf(stderr,
				gettext("Cannot access file: %s (%s)\n"),
				file, strerror(errno));
		}
		free(file);
		return (SCHA_ERR_ACCESS);
	}

	if (uid != -1) {	/* will not check if uid = -1 */
		if (statbuf.st_uid != uid) {
		/*
		 * uid is not the right one.
		 */
			/*
			 * SCMSGS
			 * @explanation
			 * The file is not owned by the uid which is listed in
			 * the message.
			 * @user_action
			 * Set the permissions on the file so that it is owned
			 * by the uid which is listed in the message.
			 */
			scds_syslog(LOG_ERR,
				"File %s is not owned by user (UID) %d",
				file, uid);
			if (print) {
				(void) fprintf(stderr,
					gettext("File %s is not owned by user "
						"(UID) %d\n"), file, uid);
			}

			free(file);
			return (SCHA_ERR_ACCESS);
		}
	}

	if (gid != -1) {	/* will not check if gid = -1 */
		if (statbuf.st_gid != gid) {
		/*
		 * gid is not the right one.
		 */
			/*
			 * SCMSGS
			 * @explanation
			 * The file is not owned by the gid which is listed in
			 * the message.
			 * @user_action
			 * Set the permissions on the file so that it is owned
			 * by the gid which is listed in the message.
			 */
			scds_syslog(LOG_ERR,
				"File %s is not owned by group (GID) %d",
				file, gid);
			if (print) {
				(void) fprintf(stderr,
				gettext("File %s is not owned by group "
					"(GID) %d\n"), file, gid);
			}

			free(file);
			return (SCHA_ERR_ACCESS);
		}
	}

	free(file);
	return (SCHA_ERR_NOERR);
}

/*
 * Log a mode (permission) problem.  See ds_validate_file() for
 * comments.
 */
static void
log_mode_problem(int print, char *file, mode_t actual, mode_t requested)
{
	int	diagnosed = 0;

	/*
	 * The following code works along these lines:
	 *
	 * - First test if the bit under consideration is one of the
	 * requested bits - a mode bit that needs to be set on the
	 * file.
	 *
	 * - If so we check to see if that bit is set in the files
	 * actual mode.
	 *
	 * - If the mode bit is not set log and print the appropriate
	 * diagnostic.  We also set the diagnostic flag to note that
	 * we have printed a diagnostic.
	 *
	 * - Continue with check the rest of the mode bits.
	 *
	 * - Finally, before returning we check to see if a diagnostic
	 * has been printed.  If one has not we log/print a somewhat
	 * generic diagnostic.
	 *
	 * This has an issue in that if the file needs to have a
	 * checked bit and an unchecked bit set, but neither is set we
	 * will print a diagnostic for the checked bit but not the
	 * generic diag for the unchecked bit.  However this should be
	 * relatively rare.  We currently diagnose all the bits
	 * currently checked by Sun Cluster agents.
	 */

	/*
	 * Diagnose owner (USR) read/write/execute permission problems
	 */
	if ((requested & S_IRUSR) && ((actual & S_IRUSR) == 0)) {
		diagnosed++;
		/*
		 * SCMSGS
		 * @explanation
		 * The owner of the file does not have read permission on it.
		 * @user_action
		 * Set the permissions on the file so the owner can read it.
		 */
		scds_syslog(LOG_ERR, "No permission for owner to read %s.",
			file);
		if (print) {
			(void) fprintf(stderr, gettext
				("No permission for owner to read %s.\n"),
				file);
		}
	}
	if ((requested & S_IWUSR) && ((actual & S_IWUSR) == 0)) {
		diagnosed++;
		/*
		 * SCMSGS
		 * @explanation
		 * The owner of the file does not have write permission on it.
		 * @user_action
		 * Set the permissions on the file so the owner can write it.
		 */
		scds_syslog(LOG_ERR, "No permission for owner to write %s.",
			file);
		if (print) {
			(void) fprintf(stderr, gettext
				("No permission for owner to write %s.\n"),
				file);
		}
	}
	if ((requested & S_IXUSR) && ((actual & S_IXUSR) == 0)) {
		diagnosed++;
		/*
		 * SCMSGS
		 * @explanation
		 * The specified path does not have the correct permissions as
		 * expected by a program.
		 * @user_action
		 * Set the permissions for the file so that it is readable and
		 * executable by the owner.
		 */
		scds_syslog(LOG_ERR, "No permission for owner to execute %s.",
			file);
		if (print) {
			(void) fprintf(stderr, gettext
				("No permission for owner to execute %s.\n"),
				file);
		}
	}

	/*
	 * Diagnose group (GRP) read/write/execute permission problems.
	 */
	if ((requested & S_IRGRP) && ((actual & S_IRGRP) == 0)) {
		diagnosed++;
		/*
		 * SCMSGS
		 * @explanation
		 * The group of the file does not have read permission on it.
		 * @user_action
		 * Set the permissions on the file so the group can read it.
		 */
		scds_syslog(LOG_ERR, "No permission for group to read %s.",
			file);
		if (print) {
			(void) fprintf(stderr, gettext
				("No permission for group to read %s.\n"),
				file);
		}
	}
	if ((requested & S_IWGRP) && ((actual & S_IWGRP) == 0)) {
		diagnosed++;
		/*
		 * SCMSGS
		 * @explanation
		 * The group of the file does not have write permission on it.
		 * @user_action
		 * Set the permissions on the file so the group can write it.
		 */
		scds_syslog(LOG_ERR, "No permission for group to write %s.",
			file);
		if (print) {
			(void) fprintf(stderr, gettext
				("No permission for group to write %s.\n"),
				file);
		}
	}
	if ((requested & S_IXGRP) && ((actual & S_IXGRP) == 0)) {
		diagnosed++;
		/*
		 * SCMSGS
		 * @explanation
		 * The specified path does not have the correct permissions as
		 * expected by a program.
		 * @user_action
		 * Set the permissions for the file so that it is readable and
		 * executable by the group.
		 */
		scds_syslog(LOG_ERR, "No permission for group to execute %s.",
			file);
		if (print) {
			(void) fprintf(stderr, gettext
				("No permission for owner to execute %s.\n"),
				file);
		}
	}
	if ((requested & S_IFDIR) && ((actual & S_IFDIR) == 0)) {
		diagnosed++;
		/*
		 * SCMSGS
		 * @explanation
		 * The path listed in the message is not a directory.
		 * @user_action
		 * Specify a directory path for the extension property.
		 */
		scds_syslog(LOG_ERR, "%s is not a directory.",
			file);
		if (print) {
			(void) fprintf(stderr, gettext
				("%s is not a directory.\n"),
				file);
		}
	}
	if ((requested & S_IFREG) && ((actual & S_IFREG) == 0)) {
		diagnosed++;
		/*
		 * SCMSGS
		 * @explanation
		 * The path listed in the message is not an ordinary file.
		 * @user_action
		 * Specify an ordinary file path for the extension property.
		 */
		scds_syslog(LOG_ERR, "%s is not an ordinary file.",
			file);
		if (print) {
			(void) fprintf(stderr, gettext
				("%s is not an ordinary file.\n"),
				file);
		}
	}

	if (!diagnosed) {
		/*
		 * SCMSGS
		 * @explanation
		 * The file needs to have the indicated mode.
		 * @user_action
		 * Set the mode of the file correctly.
		 */
		scds_syslog(LOG_ERR, "Mode for file %s needs to be %03o",
			file, requested);
		if (print) {
			(void) fprintf(stderr, gettext
				("Mode for file %s needs to be %03o\n"),
				file, requested);
		}
	}
}

/*
 * ds_internal_error()
 *
 * Log an INTERNAL ERROR message using Scds_syslog()[1] at LOG_ERR
 * priority.
 *
 * Formats it's arguments and appends it to them to INTERNAL ERROR.
 * If ds_internal_error() runs into an error formating it's arguments
 * it logs it's own error message.
 *
 * [1] Don't uncap that function name.  You'll confuse the machinery
 * that makes sure everything is explained in sc_msgid_expl.
 */
void
ds_internal_error(const char *format, ...)
{
	char	*msg;
	va_list	ap;
	char	errbuf[DS_STRING_FORMAT_SIZE];

	/* Suppress Undeclared identifier (__builtin_va_alist) */
	va_start(ap, format);	/*lint !e40 */
	msg = ds_string_format_v(format, ap);
	va_end(ap);

	if (msg != NULL)
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", msg);
	else {
		/*
		 * Suppress lint message re: "call to ___errno() not made
		 * in the presence of a prototype"
		 */
		(void) snprintf(errbuf, (size_t)DS_STRING_FORMAT_SIZE,
			"ds_internal_error (%s)",
			strerror(errno)); /*lint !e746 */
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", errbuf);
	}

	free(msg);
}

/*
 * ds_string_format()
 *
 * Formats it's arguments into a string using vsnprintf() and returns
 * the resulting string.  The returned string needs to be freed (using
 * free(3C)) when it's no longer needed.
 *
 * Returns NULL on error after logging an error message.
 *
 * The advantages of using this over snprintf() are:
 *
 * - The caller no longer needs to worry about dealing with a string
 * that formats to a longer length then the buffer allocated for it.
 *
 * - We also simplify error checking.  The caller only needs to check
 * the returned pointer to NULL rather then checking snprintf's return
 * for less then 0 (error occured) *and* if the returned length is
 * larger then the string buffer.
 *
 * - Finally, we log appropriate messages so the caller doesn't need
 * to.  If the caller is a validate method that needs to print
 * messages also, the caller still needs to do that.
 *
 * Calls ds_string_format_v() to do the actual heavy lifting.
 */
char *
ds_string_format(const char *format, ...)
{
	va_list ap;
	char *buf;

	/* Suppress "Undeclared identifier (__builtin_va_alist)" */
	va_start(ap, format);	/*lint !e40 */
	buf = ds_string_format_v(format, ap);
	va_end(ap);

	return (buf);
}

/*
 * ds_string_format_v()
 *
 * The same as ds_string_format(), except that instead of being called
 * with a variable number of arguments, this is called with an
 * argument list as defined in the <stdarg.h> header.
 */
char *
ds_string_format_v(const char *format, va_list ap)
{
	char	*buf = NULL;
	char	*nbuf = NULL;
	int	len;
	size_t	size;
	va_list cap;

	va_copy(cap, ap);

	/*
	 * Allocate a buffer that should be big enough for most
	 * strings.
	 *
	 * We could call vsnprintf() with a buffer size of 0 just to
	 * calculate the size of the formatted string and always call
	 * vsnprintf() a second time to actually format into the
	 * buffer.  By first allocating a buffer large enough for most
	 * strings we avoid always calling the expensive vsnprintf()
	 * twice every time.
	 */
	if ((buf = malloc((size_t)DS_STRING_FORMAT_SIZE)) == NULL) {
		scds_syslog(LOG_ERR, "Out of memory.");
		return (NULL);
	}

	len = vsnprintf(buf, (size_t)DS_STRING_FORMAT_SIZE, format, ap);

	/*
	 * If len indicates that vsnprintf() formatted more characters
	 * then fit into the initial buffer we allocated we reallocate
	 * the buffer so it will be big enough for the entire string
	 * and call vsnprintf() again to get the entire formatted
	 * string.
	 */
	if (len > DS_STRING_FORMAT_SIZE - 1) {
		scds_syslog_debug(DBG_LEVEL_HIGH, "ds_string_format_v(): "
			"growing string");
		/* When arch is v9 below is a "suspicious cast" */
		size = (size_t)(len + 1); /*lint !e571 */
		if ((nbuf = realloc(buf, size)) == NULL) {
			/*
			 * Suppress spurious warning about buf not
			 * being initialized.
			 */
			free(buf); /*lint !e644 */
			scds_syslog(LOG_ERR, "Out of memory.");
			return (NULL);
		} else {
			buf = nbuf;
		}

		len = vsnprintf(buf, size, format, cap);
	}

	/*
	 * If an error occured with either snprintf() (len < 0)
	 * construct an internal error in the existing allocated buf
	 * and syslog it.
	 */
	if (len < 0) {
		/* Suppress ___errno() has no prototype message */
		(void) snprintf(buf, (size_t)DS_STRING_FORMAT_SIZE,
			"ds_string_format_v (%s)",
			strerror(errno)); /*lint !e746 */
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", buf);
		free(buf);
		return (NULL);
	}

	return (buf);
}

/*
 * split_magnus_line()
 *
 * Private function to split a parameter line from a iWS magnus.conf
 * style file.  This is not meant for the geting information out of
 * Init SAFs which have a more complex syntax then <variable> <value>
 * that the rest of magnus.conf has.
 *
 * key is expected to be at least DS_ARRAY_SIZE long, val to be at
 * least MAXPATHLEN+1.
 *
 * line is altered by the calls to strtok().
 *
 * Returns 1 if there is not enough room in either key or val to store
 * the entire value that they should.
 *
 * Returns 0 otherwise.
 */

int
split_magnus_line(char *line, char *key, char *val)
{
	char *kp, *vp;
	int toobig;

	key[0] = '\0';		/* Make sure we don't return garbage */
	val[0] = '\0';		/* in key or val */
	toobig = 0;

	kp = strtok(line, " \t\n");
	vp = strtok(NULL, "\n");

	if (kp != NULL) {
		if (strlcpy(key, kp, (size_t)DS_ARRAY_SIZE) >= DS_ARRAY_SIZE)
			toobig++;
	}

	if (vp != NULL) {
		if (strlcpy(val, vp, (size_t)MAXPATHLEN+1) >= MAXPATHLEN+1)
			toobig++;
	}

	if (toobig)
		return (1);
	else
		return (0);
}

/*
 * get_magnus_value()
 *
 * Return the value of the given key from the specified magnus.conf.
 *
 * value must at least be MAXPATHLEN+1.
 * key must not be longer then DS_ARRAY_SIZE.
 *
 * Returns SCHA_ERR_NOERR on success, on error return some other
 * SCHA_ERR code.
 */

int
ds_get_magnus_value(const char *filename, const char *key, char *value)
{
	FILE	*fp = NULL;
	/* 3 = 2 str term + 1 delim */
	char	line[MAXPATHLEN + DS_ARRAY_SIZE + 3];
	char	lkey[DS_ARRAY_SIZE+1];
	char	*s;
	int	toobig;

	fp = fopen(filename, "r");
	if (fp == NULL) {
		scds_syslog(LOG_ERR,
			"File %s is not readable: %s.",
			filename, strerror(errno));
		return (SCHA_ERR_ACCESS);
	}

	while (!feof(fp)) {
		/*
		 * fgets() expects 2nd arg to be int, however sizeof()
		 * is long in 64-bit. Suppressing lint 747
		 * "Significant prototype coercion" as a line is not
		 * going to be longer then INT_MAX.
		 */
		s = fgets(line, sizeof (line), fp);	 /*lint !e747 */
		if (s == NULL && !feof(fp)) {
			(void) fclose(fp);
			return (1);
		}

		toobig = split_magnus_line(line, lkey, value);

		if (strcasecmp(lkey, key) == 0) {
			/*
			 * Put check for too big here so that we only
			 * complain if the line that is too big is the
			 * one with our value on it.
			 */
			if (toobig) {
				(void) fclose(fp);
				ds_internal_error("String handling error "
					"getting value of %s from %s. "
					"The value may be too long.",
					key, filename);
				return (SCHA_ERR_INTERNAL);
			}

			scds_syslog_debug(DBG_LEVEL_HIGH,
				"Configuration file <%s> "
				"has %s set as %s", filename, key,
				value);

			(void) fclose(fp);
			return (SCHA_ERR_NOERR);
		}
	}

	/*
	 * We return as soon as the key is found.  If we get here key
	 * was not found.
	 */

	(void) fclose(fp);
	/*
	 * SCMSGS
	 * @explanation
	 * The configuration file does not have a valid entry for the
	 * indicated configuration item.
	 * @user_action
	 * Check that the file has a correct entry for the configuration item.
	 */
	scds_syslog(LOG_ERR, "Configuration file <%s> does not "
		"configure %s.", filename, key);

	return (SCHA_ERR_INVAL);
}


/*
 * This function determines if OS is S10 or later.
 * This is determined at compile time since we have separate
 * executables for each version of the OS.
 * Returns 1 if OS is newer than S10, 0 if it is not.
 */
int
os_newer_than_s10()
{
#if	(SOL_VERSION >= __s10)
		return (1);
#else
		return (0);
#endif
}


/*
 * Check an array of SMF services and disable them.
 *
 * Return: 0 if services are disabled, non-zero otherwise.
 *
 */
int
disable_smf_services(char **svcs)
{
	char *s = NULL, *state = NULL;
	int i, rc = 0;

#if	(SOL_VERSION >= __s10)

	scds_syslog_debug(DBG_LEVEL_HIGH, "Starting to disable SMF services.");

	for (i = 0; s = svcs[i]; i++) {
		if ((state = smf_get_state(s)) != NULL) {
			if (strcmp(SCF_STATE_STRING_DISABLED, state) == 0)
				scds_syslog_debug(DBG_LEVEL_HIGH,
					"Skipping %s: already disabled.", s);
			free(state);
			break;
		}
		rc = smf_disable_instance(s, 0);
		if (rc != 0) {
			/*
			 * SCMSGS
			 * @explanation
			 * The Solaris SMF service indicated above could not
			 * be disabled as required by Sun Cluster to make the
			 * service capable of restart/failover.
			 * @user_action
			 * Try to manually disable the SMF service specified
			 * by the error message using the 'svcadm disable'
			 * command and retry the operation.
			 */
			scds_syslog(LOG_ERR,
				"Failed to disable %s.", s);
			break;
		}
	}

	if (rc == 0)
		scds_syslog_debug(DBG_LEVEL_HIGH,
			"Successfully disabled SMF services.");

#endif	/* SOL_VERSION >= __s10 */

	return (rc);
}

/*
 * Check an array of services and verify that they are disabled.
 *
 * Return: 0 if all the services are disabled, non-zero otherwise.
 */
int
check_disabled_smf_services(char **svcs, boolean_t print_msgs)
{
	char *s = NULL, *state = NULL;
	int i, rc = 0;

#if	(SOL_VERSION >= __s10)

	scds_syslog_debug(DBG_LEVEL_HIGH,
		"Starting to check states of SMF services");

	for (i = 0; s = svcs[i]; i++) {
		if ((state = smf_get_state(s)) != NULL) {
			scds_syslog_debug(DBG_LEVEL_HIGH,
				"DNS state is %s", state);
			rc = strcmp(SCF_STATE_STRING_DISABLED, state);
			free(state);
			if (rc != 0) {
				/*
				 * SCMSGS
				 * @explanation
				 * The Solaris SMF service indicated above
				 * could not be disabled as required by Sun
				 * Cluster to make the service capable of
				 * restart/failover.
				 * @user_action
				 * Try to manually disable the SMF service
				 * specified by the error message using the
				 * 'svcadm disable' command and retry the
				 * operation.
				 */
				scds_syslog(LOG_ERR,
					"Warning! Sun Cluster resource "
					"cannot be brought online on this "
					"node because the corresponding "
					"Solaris SMF services may be online. "
					"Please disable the Solaris SMF "
					"services using the following "
					"command: "
					"'svcadm disable %s'", s);

				if (print_msgs)
					(void) fprintf(stderr, gettext(
					"Warning! Sun Cluster resource "
					"cannot be brought online on this "
					"node because the corresponding "
					"Solaris SMF services may be online. "
					"Please disable the Solaris SMF "
					"services using the following "
					"command: "
					"\n'svcadm disable %s'\n"), s);
				break;
			}
		} else {
			rc = 1;
			/*
			 * SCMSGS
			 * @explanation
			 * There was a failure in attempting to determine the
			 * state of the SMF service.
			 * @user_action
			 * Examine other syslog messages occurring around the
			 * same time on the same node, to see if the source of
			 * the problem can be identified.
			 */
			scds_syslog(LOG_ERR,
				"Failed to get state of SMF service %s", s);
			if (print_msgs)
				(void) fprintf(stderr, gettext(
					"Failed to get state of SMF "
					"service %s\n"), s);
			break;
		}
	}

	scds_syslog_debug(DBG_LEVEL_HIGH,
		"Completed getting the states of SMF services.");

#endif	/* SOL_VERSION >= __s10 */

	return (rc);
}
