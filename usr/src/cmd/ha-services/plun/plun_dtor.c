#include <stdio.h>
#include <syslog.h>
#include <libstmf.h>
#include <string.h>
#include <strings.h>
#include <limits.h>

#include "plun.h"

#define MAX_LUS 32
#define STR_GUID_LENGTH 32

static char *target_lus[MAX_LUS];
static char *resource_name;

static const char *
dump_guid(stmfGuid *in_guid) {
	static char string_guid[64];
	uchar_t *guid = in_guid->guid;

	memset(string_guid, 0, sizeof(string_guid));

	(void) sprintf(string_guid,
	    "%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
	    guid[0], guid[1], guid[2], guid[3], guid[4], guid[5],
	    guid[6], guid[7], guid[8], guid[9], guid[10], guid[11],
	    guid[12], guid[13], guid[14], guid[15]);

	return &string_guid[0];
}

static int
check_lu(const char *guid) {
	int idx, found = 0;

	for (idx = 0; idx < MAX_LUS && target_lus[idx] != NULL; idx++) {
		if (strcasecmp(guid, target_lus[idx]) == 0) {
			found = 1;
			break;
		}
	}

	return (found);
}

#define CLEVENT_PROG "/usr/cluster/lib/sc/sc_publish_event"

static void
count_guid_for_event(const char *guid) {
	static const char *handled_lus[MAX_LUS];
	static int lu_idx = 0;

	if (guid != NULL) {
		handled_lus[lu_idx++] = strdup(guid);
	}

	if (lu_idx == MAX_LUS || (guid == NULL && lu_idx > 0)) {
		/*             executable   array of GUIDs    */
		char exec_buf[2*PATH_MAX + MAX_LUS*STR_GUID_LENGTH +
			/* Num commas and extra symbols: 1 space and '\0'*/
			(MAX_LUS-1) + 2];
		int i;

		strncpy(exec_buf, CLEVENT_PROG, PATH_MAX);
		strcat(exec_buf, " -c ESC_cluster_nef_event -p NEF -i 0 -s 0 "
			"-x nef_subclass -y SE_DATA_TYPE_STRING -z NEF_plun_invalidate_lus ");

		/* We build the following JSON object:
		      {resource: <resource>, args: guid1,guid2..}
		   Since we're going to use system(), all " will be escaped.
		First, add {resource: "XXX */
		strcat(exec_buf, "-x nef_payload -y SE_DATA_TYPE_STRING -z \"{\\\"resource\\\":\\\"");
		strcat(exec_buf, resource_name);

		/* Add ", "args": " */
		strcat(exec_buf, "\\\",\\\"args\\\":\\\"");

		/* Add comma-separated GUIDs. */
		for (i = 0; i < lu_idx; i++) {
			strcat(exec_buf, handled_lus[i]);

			if (i != (lu_idx - 1)) {
				strcat(exec_buf, ",");
			}
		}

		/* Add } */
		strcat(exec_buf, "\\\"}\"");
		system(exec_buf);

		lu_idx = 0;
	}
}

static int
handle_pool_standby_lus(const char *pool, int primary)
{
	int stmfRet, j;
	stmfGuidList *luList;
	stmfLogicalUnitProperties luProps;
	int poolname_len;

	if ((stmfRet = stmfGetLogicalUnitList(&luList))
	    != STMF_STATUS_SUCCESS) {
		syslog(LOG_ERR, "Failed to get the list of active LUs "
		    "(err=%d)\n", stmfRet);
		return (1);
	}

	poolname_len = strlen(pool);
	for (j = 0; j < luList->cnt; j++) {
		stmfGuid *guid = &luList->guid[j];
		luResource resHdl;
		char *volume;
		int poolname_idx;
		char propVal[MAXNAMELEN];
		size_t propValSize = sizeof (propVal);
		const char *s_guid = dump_guid(guid);

		stmfRet = stmfGetLogicalUnitProperties(guid, &luProps);
		if (stmfRet != STMF_STATUS_SUCCESS) {
			syslog(LOG_ERR, "Failed to get the properties for LU %s "
			   "(err=%d)\n", s_guid, stmfRet);
			continue;
		}

		/* Check LU name against target pool. */
		volume = luProps.alias;
		if (strncmp(ZVOL_PREFIX, volume, ZVOL_PREFIX_LEN) != 0) {
			syslog(LOG_ERR, "Ignoring non-ZVOL LU: %s\n", s_guid);
			continue;
		}

		/* Make sure this LU is in standby mode. */
		if ((stmfRet = stmfGetLuResource(guid,
		    &resHdl)) != STMF_STATUS_SUCCESS) {
			if (stmfRet == STMF_ERROR_NOT_FOUND) {
				/* LU has been concurrently removed. */
				continue;
			}
			syslog(LOG_ERR, "Failed to get resource "
			    "descriptor for LU %s (err=%d)\n", s_guid, stmfRet);
			continue;
		}

		/* Obtain data file. We only have to check error code. */
		stmfRet = stmfGetLuProp(resHdl, STMF_LU_PROP_FILENAME, propVal,
		    &propValSize);
		if (stmfRet != STMF_STATUS_SUCCESS) {
			if (stmfRet == STMF_ERROR_NOT_FOUND) {
				/* LU has been concurrently removed. */
				continue;
			}
			if (stmfRet != STMF_ERROR_NO_PROP_STANDBY) {
				syslog(LOG_ERR,
				    "LU %s is not in standby mode.\n", s_guid);
				continue;
			}
		}

		/* Found a valid zvol device. */
		poolname_idx = ZVOL_PREFIX_LEN;

		/* Handle [r]dsk */
		if (volume[poolname_idx] == 'r')
			poolname_idx++;

		poolname_idx += 4; /* dsk/ */

		/* Now we're pointint at poolname. */
		if (strncmp(&volume[poolname_idx],
		    pool, poolname_len) != 0) {
			/* Ignore ZVOL that doesn't belong to target pool. */
			continue;    
		}

		if (primary) {
			/* Count this LU for generating events. */
			count_guid_for_event(s_guid);
		} else {
			/* Make sure this LU is allowed. */
			if (check_lu(dump_guid(guid)) == 0) {
				continue;
			}
		}

		/* Delete LU. */
		stmfRet = stmfDeleteLu(guid);
		switch (stmfRet) {
			case STMF_STATUS_SUCCESS:
				syslog(LOG_NOTICE, "LU %s: successfully removed.\n",
				    dump_guid(guid));
				break;
			default:
				syslog(LOG_ERR, "LU %s: removal failed "
				    "(err=%d)\n", dump_guid(guid), stmfRet);
				break;
		}
	}

	/* Propage LU cleanup across cluster. */
	if (primary) {
		count_guid_for_event(NULL);
	}

	return (0);
}

int
main(int argc, char *argv[])
{
	int	rc;
	char *rsname = NULL, *poolname = NULL;
	off_t size = 0;
	int primary = 0;

	openlog("plun_dtor", LOG_CONS | LOG_PID | LOG_NDELAY, LOG_DAEMON);
	if (argc < 2 || argc > 3) {
		syslog(LOG_ERR, "Insufficient number of command line arguments.\n");
		return (1);
	}

	/* Get resource, resource group and pool names */
	rsname = argv[1];
	poolname = RSNAME_TO_POOLNAME(rsname);

	if (argc == 3) {
		/* Cleanup cpecific LUs. */
		char *lu;
		int id = 0;

		memset(target_lus, 0, sizeof(target_lus));
		lu = strtok(argv[2], ",");

		while (lu != NULL && (id < MAX_LUS)) {
			target_lus[id++] = lu;
			lu = strtok(NULL, ",");
		}
	} else {
		primary = 1;
	}

	resource_name = argv[1];

	syslog(LOG_NOTICE, "Deleting all standby LUs for pool %s\n", poolname);
	return (handle_pool_standby_lus(poolname, primary));
}
