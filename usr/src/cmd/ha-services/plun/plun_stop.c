#include <stdio.h>
#include <rgm/libdsdev.h>
#include <syslog.h>
#include <libzfs.h>
#include <libstmf.h>

#include "plun.h"


libzfs_handle_t *g_zfs;


static const char *
dump_guid(stmfGuid *in_guid) {
	static char string_guid[64];
	uchar_t *guid = in_guid->guid;

	memset(string_guid, 0, sizeof(string_guid));

	(void) sprintf(string_guid,
	    "%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
	    guid[0], guid[1], guid[2], guid[3], guid[4], guid[5],
	    guid[6], guid[7], guid[8], guid[9], guid[10], guid[11],
	    guid[12], guid[13], guid[14], guid[15]);

	return &string_guid[0];
}


static int
standby_pool_lus(const char *pool)
{
	int stmfRet, j;
	stmfGuidList *luList;
	stmfLogicalUnitProperties luProps;
	int poolname_len;

	if ((stmfRet = stmfGetLogicalUnitList(&luList))
	    != STMF_STATUS_SUCCESS) {
		scds_syslog(LOG_ERR, "Failed to get the list of active LUs "
		    "(err=%d)\n", stmfRet);
		return (1);
	}

	poolname_len = strlen(pool);
	for (j = 0; j < luList->cnt; j++) {
		stmfGuid *guid = &luList->guid[j];
		luResource resHdl;
		char *volume;
		int poolname_idx;
		char propVal[MAXNAMELEN];
		size_t propValSize = sizeof (propVal);

		stmfRet = stmfGetLogicalUnitProperties(guid, &luProps);
		if (stmfRet != STMF_STATUS_SUCCESS) {
			scds_syslog(LOG_ERR, "Failed to get the properties for LU %s "
			   "(err=%d)\n", dump_guid(guid), stmfRet);
			continue;
		}

		if ((stmfRet = stmfGetLuResource(guid,
		    &resHdl)) != STMF_STATUS_SUCCESS) {
			if (stmfRet == STMF_ERROR_NOT_FOUND) {
				/* LU has been concurrently removed. */
				continue;
			}
			scds_syslog(LOG_ERR, "Failed to get resource "
			    "descriptor for LU %s (err=%d)\n",
			    dump_guid(guid), stmfRet);
			continue;
		}

		/* Obtain data file. */
		stmfRet = stmfGetLuProp(resHdl, STMF_LU_PROP_FILENAME, propVal,
		    &propValSize);
		if (stmfRet != STMF_STATUS_SUCCESS) {
			if (stmfRet != STMF_ERROR_NO_PROP_STANDBY) {
				scds_syslog(LOG_ERR,
				    "Failed to get ZVOL name for for LU %s "
			    	    "(err=%d)\n", dump_guid(guid), stmfRet);
			}
			continue;
		}

		/* Check LU name against target pool. */
		volume = propVal;
		if (strncmp(ZVOL_PREFIX, volume, ZVOL_PREFIX_LEN) != 0) {
			scds_syslog(LOG_INFO, "Ignoring non-ZVOL LU: %s\n",
			    dump_guid(guid));
			continue;
		}

		/* Found a valid zvol device. */
		poolname_idx = ZVOL_PREFIX_LEN;

		/* Handle [r]dsk */
		if (volume[poolname_idx] == 'r')
			poolname_idx++;

		poolname_idx += 4; /* dsk/ */

		/* Now we're pointint at poolname. */
		if (strncmp(&volume[poolname_idx],
		    pool, poolname_len) != 0) {
			/* Ignore ZVOL that doesn't belong to target pool. */
			continue;    
		}

		/* Do LU standby. */
		stmfRet = stmfLuStandby(guid);
		switch (stmfRet) {
			case STMF_STATUS_SUCCESS:
				scds_syslog(LOG_INFO, "LU %s: standby succeeded.\n",
				    dump_guid(guid));
				break;
			default:
				scds_syslog(LOG_ERR, "LU %s: standby failed "
				    "(err=%d)\n", dump_guid(guid), stmfRet);
				break;
		}
	}

	return (0);
}


static zfs_handle_t *
get_pool_handle(const char *pool)
{
	char poolname[MAXNAMELEN];
	zfs_handle_t *zhp;

	/* Add leading slash to the pool name. */
	snprintf(poolname, sizeof(poolname), "/%s", pool);

	return (zfs_path_to_zhandle(g_zfs, poolname, ZFS_TYPE_POOL));
}


int
main(int argc, char *argv[])
{
	int	rc;
	scds_handle_t	scds_handle;
	char	*rsname = NULL, *poolname = NULL;
	off_t	size = 0;
	static zfs_handle_t *zhp;

	if ((rc = scds_initialize(&scds_handle, argc, argv)) != SCHA_ERR_NOERR) {
		return (1);
	}

	/* Get resource, resource group and pool names */
	rsname = (char *)scds_get_resource_name(scds_handle);
	poolname = RSNAME_TO_POOLNAME(rsname);

	/* Command validated, proceed. */
	if ((g_zfs = libzfs_init()) == NULL) {
		scds_syslog(LOG_ERR, "internal error: failed to initialize "
		    "ZFS library\n");
		return (0);
	}

	/* Make sure target pool exists. */
	if ((zhp = get_pool_handle(poolname)) == NULL) {
		scds_syslog(LOG_ERR,"Pool '%s' doesn't exist.\n", poolname);
		return (0);
	}

	scds_syslog(LOG_INFO, "Standing by LUs for pool %s\n", poolname);
	rc = standby_pool_lus(poolname);

	return (rc);
}
