#ifndef __PLUN_H__
#define __PLUN_H__

#include <string.h>

#define ZVOL_PREFIX "/dev/zvol/"
#define ZVOL_PREFIX_LEN (10)

#define PLUN_RS_PREFIX "rs-plun-"

#define RSNAME_TO_POOLNAME(rsname) (strlen((rsname)) > strlen(PLUN_RS_PREFIX) ? \
 	&(rsname)[strlen(PLUN_RS_PREFIX)] : NULL)

#endif
