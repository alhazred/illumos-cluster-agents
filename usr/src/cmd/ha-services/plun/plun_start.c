#include <stdio.h>
#include <rgm/libdsdev.h>
#include <syslog.h>
#include <libzfs.h>
#include <libstmf.h>

#include "plun.h"


libzfs_handle_t *g_zfs;


static int
zfs_callback(zfs_handle_t *zhp, void *data)
{
	if (zfs_get_type(zhp) == ZFS_TYPE_VOLUME) {
		const char *volume = zfs_get_name(zhp);
		int stmfRet;
		char abspath[MAXNAMELEN];

		snprintf(abspath, sizeof(abspath), "/dev/zvol/rdsk/%s",
		    volume);

		stmfRet = stmfImportLu(STMF_DISK, abspath, NULL);
		switch (stmfRet) {
			case STMF_STATUS_SUCCESS:
				scds_syslog(LOG_INFO, "LU %s successfully imported.\n",
					abspath);
				break;
			case STMF_ERROR_BUSY:
			case STMF_ERROR_FILE_IN_USE:
				/* Nothing critical, just ignore ZVOL. */
				break;
			default:
				scds_syslog(LOG_WARNING, "Failed to import LU: %s"
				    " (err=%d)\n", abspath, stmfRet);
			break;
		}
	}

	zfs_close(zhp);
	return (0);
}


static int
import_pool_lus(zfs_handle_t *zhp)
{
	zfs_iter_children(zhp, zfs_callback, NULL);

	return (0);
}


static zfs_handle_t *
get_pool_handle(const char *pool)
{
	char poolname[MAXNAMELEN];
	zfs_handle_t *zhp;

	/* Add leading slash to the pool name. */
	snprintf(poolname, sizeof(poolname), "/%s", pool);

	return (zfs_path_to_zhandle(g_zfs, poolname, ZFS_TYPE_POOL));
}


int
main(int argc, char *argv[])
{
	int	rc;
	scds_handle_t	scds_handle;
	char	*rsname = NULL, *poolname = NULL;
	off_t	size = 0;
	static zfs_handle_t *zhp;

	if ((rc = scds_initialize(&scds_handle, argc, argv)) != SCHA_ERR_NOERR) {
		return (1);
	}

	/* Get resource, resource group and pool names */
	rsname = (char *)scds_get_resource_name(scds_handle);
	poolname = RSNAME_TO_POOLNAME(rsname);

	/* Command validated, proceed. */
	if ((g_zfs = libzfs_init()) == NULL) {
		scds_syslog(LOG_ERR, "internal error: failed to initialize "
		    "ZFS library\n");
		return (0);
	}

	/* Make sure target pool exists. */
	if ((zhp = get_pool_handle(poolname)) == NULL) {
		scds_syslog(LOG_ERR,"Pool '%s' doesn't exist.\n", poolname);
		return (0);
	}

	scds_syslog(LOG_INFO, "Importing LUs for pool %s\n", poolname);
	rc = import_pool_lus(zhp);

	return (rc);
}
