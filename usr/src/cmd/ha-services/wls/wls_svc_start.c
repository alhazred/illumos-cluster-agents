/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#pragma ident	"@(#)wls_svc_start.c	1.7	07/06/06 SMI"

/*
 * wls_svc_start.c - Start method for BEA WebLogic Server
 */

#include <rgm/libdsdev.h>
#include "wls.h"

/* Percentage of start/probe timeout for the DB probe */
#define	DB_PROBE_TIMEOUT_PCT	25

/*
 * The start method for BEA WebLogic Server. Does some sanity checks on
 * the resource settings and then starts the WLS under PMF with
 * an action script.
 */

int
main(int argc, char *argv[])
{
	bea_extn_props_t	beaextnprops;
	scds_handle_t		scds_handle;
	int 			rc;
	int			svc_start_timeout, db_probe_timeout;
	char			*rsname = NULL, *rgname = NULL;

	/*
	 * Process all the arguments that have been passed to us from RGM
	 * and do some initialization for syslog
	 */

	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR) {
		return (1);
	}

	/* Get resource and resource group names */
	rsname = (char *)scds_get_resource_name(scds_handle);
	rgname = (char *)scds_get_resource_group_name(scds_handle);

	/* Get the extension properties */

	if (get_wls_extn_props(scds_handle, &beaextnprops, B_FALSE) != 0) {
		scds_syslog(LOG_ERR,
			"Failed to retrieve WLS extension properties.");
		rc = 1;
		goto finished;
	}


	/* Validate the configuration and if there is an error return back */
	rc = svc_validate(scds_handle, &beaextnprops, B_FALSE);

	if (rc != 0) {
		scds_syslog(LOG_ERR,
		    "Failed to validate configuration.");
		goto finished;
	}

	/*
	 * Before starting the WLS check if the DB is UP. If the DB is
	 * not UP return 0 (so that the start method does not fail).
	 * The probe method will wait till the DB is up and start the
	 * WebLogic Server.
	 */
	svc_start_timeout = scds_get_rs_start_timeout(scds_handle);
	db_probe_timeout = (svc_start_timeout * DB_PROBE_TIMEOUT_PCT) /100;

	rc = probe_db(scds_handle, beaextnprops.db_probe_script,
			db_probe_timeout);
	if (rc != 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * The Data base probe (set in the extension property
		 * db_probe_script) failed. The start method will not start
		 * the WLS. The probe method will wait till the DB probe
		 * succeeds before starting the WLS.
		 * @user_action
		 * Make sure the DB probe (set in db_probe_script) succeeds.
		 * Once the DB is started the WLS probe will start the WLS
		 * instance.
		 */
		scds_syslog(LOG_ERR, "The Data base probe %s failed."
			"The WLS probe will wait for the DB to be UP before "
			"starting the WLS", beaextnprops.db_probe_script);
		rc = DB_NOT_AVAILABLE;
		goto finished;
	}

	/* Start the data service, if it fails return with an error */
	rc = svc_start(scds_handle, &beaextnprops);
	if (rc != 0) {
		goto finished;
	}

	/* Wait for the service to start up fully */
	rc = svc_wait(scds_handle, &beaextnprops);
	scds_syslog_debug(DBG_LEVEL_HIGH, "Returned from svc_wait");


finished:

	if (rc == SCHA_ERR_NOERR) {
		(void) scha_resource_setstatus(rsname, rgname,
			SCHA_RSSTATUS_OK, "Completed successfully.");
	} else if (rc == DB_NOT_AVAILABLE) {
		(void) scha_resource_setstatus(rsname, rgname,
			SCHA_RSSTATUS_DEGRADED,
			"Database not available.");
		/*
		 * we dont want to fail the start method if the DB is
		 * not available. We let the probe handle the starting
		 * of the WLS. Set rc to 0;
		 */
		rc = 0;
	} else {
		(void) scha_resource_setstatus(rsname, rgname,
			SCHA_RSSTATUS_FAULTED,
			"Failed to start the WLS.");
	}

	/* Free up the Environment resources that were allocated */
	scds_close(&scds_handle);

	return (rc);
}
