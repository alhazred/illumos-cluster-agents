#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#pragma ident	"@(#)wls_utils.ksh	1.7	07/06/06 SMI"

#
#This file contains the commands to stop the WebLogic Servers, either
#Admin server or Managed server
#
# Input:
# WEBLOGIC_HOME - Complete path to the Weblogic home directory.
# SERVER_URL    - The WebLogic Server URL.
# START_SCRIPT  - The complete path to the start script used
#                 to start this server.
#
# Return:
#
# 0 - success
# 1 - failure


PATH=/bin:/usr/bin:/usr/cluster/bin:/usr/sbin:/usr/cluster/lib/sc:$PATH;export PATH
syslog_tag="SC[SUNW.wls,stop_wls.ksh]"

if [ $# -ne 4 ]
then
	# SCMSGS
	# @explanation
	# An internal error has occured.
	# @user_action
	# Save a copy of the /var/adm/messages files on all nodes. Contact
	# your authorized Sun service provider for assistance in diagnosing
	# the problem.
	scds_syslog -p error -t $syslog_tag -m \
		"INTERNAL ERROR usage: $0 <validate/stop> <weblogic_home> <server_url> <server_start_script>"
	exit 1

fi

action=$1
WEBLOGIC_HOME=$2
SERVER_URL=$3
START_SCRIPT=$4

#echo "The WebLogic Home is $WEBLOGIC_HOME, The Server URL is $SERVER_URL and the Start Script is $START_SCRIPT, The action to be performed is $action"

#
#Function to do a smooth shutdown of the WebLogic Server. The WLS is shutdown
#smoothly only if the extension property Smooth_shutdown is set to TRUE. If this
#is set then the users are expected to set the WLS_USER and WLS_PW in the 
#start script. If these are not found in the start_script then kill -9 is
#used to force shutdownthe WLS.
#
stop_wls()
{

	#source the environment variables file setWLSEnv.sh file before 
	#calling any java utilities. The Administrators are expected to set all
	#the necessary environment variables in this file.

	. $WEBLOGIC_HOME/server/bin/setWLSEnv.sh > /dev/null 2>&1

	#Shutdown the WebLogic Server
	#In WLS 8 the username and password can be entered in a  boot identity 
	#file which is encrypted after the first startup. If this file exists 
	#then the weblogic server can be shutdown without providing the username
	# and password. For versions earlier than 8.0, username and password 
	#need to be provided for smooth_shutdown.

	VERSION=`java weblogic.version | head -2 | /usr/bin/awk '{print \$3}'`

	if [ $VERSION -lt 8 ]; then

		java weblogic.Admin -url $SERVER_URL SHUTDOWN -username $USERNAME -password $PASSWORD > /dev/null 2>&1

	else 

		java weblogic.Admin -url $SERVER_URL SHUTDOWN > /dev/null 2>&1

	fi

	rc=$?
	if [ $rc -ne 0 ]
	then
		return 1
	fi

	return 0

}

#Function to validate the WLS_USER and WLS_PW. This validation is done only
#if the Smooth_shutdown extension property is set to TRUE.
#

validate_wls()
{

	#source the environment variables file setWLSEnv.sh file before
	#calling any java utilities. The Administrators are expected to set all
	#the necessary environment variables in this file.

	. $WEBLOGIC_HOME/server/bin/setWLSEnv.sh > /dev/null 2>&1

	#For WLS versions earlier than 8.0,get the values of WLS_USER 
	#and WLS_PW from the start script. For WLS versions 8.0 and above,
	#check if boot.properties file is present in $START_SCRIPT_DIR
	#so that WLS_USER and WLS_PW need not be made public. For WLS versions
	#9.0 and above, new directory structure has been introduced with
	#start_script being present in $DOMAIN_NAME/bin directory. Hence
	#for 9.0 onwards, a check has to be made if boot.properties exist in
	#$DOMAIN_NAME directory.
	
	VERSION=`java weblogic.version | head -2 | /usr/bin/awk '{print \$3}'`
	START_SCRIPT_DIR=`/usr/bin/dirname $START_SCRIPT` 

	if [ $VERSION -lt 8 ]; then

		USERNAME=`cat $START_SCRIPT | grep "^WLS_USER=" | /usr/bin/awk -F= '{print \$2}'`
		if [ -z "$USERNAME" ]; then
			# SCMSGS
			# @explanation
			# Need explanation of this message!
			# @user_action
			# Need a user action for this message.
			scds_syslog -p error -t $syslog_tag -m \
				"WLS_USER not specified in the start script $START_SCRIPT.This is
				 required as the SMOOTH_SHUTDOWN extension property is set to TRUE."
			return 1
		fi

		PASSWORD=`cat $START_SCRIPT | grep "^WLS_PW=" | /usr/bin/awk -F= '{print \$2}'`
		if [ -z "$PASSWORD" ]; then
			# SCMSGS
			# @explanation
			# Need explanation of this message!
			# @user_action
			# Need a user action for this message.
			scds_syslog -p error -t $syslog_tag -m \
				"WLS_PW not specified in the start script $START_SCRIPT.This is
				 required as the SMOOTH_SHUTDOWN extension property is set to TRUE."
			return 1
		fi
	
	else

		if [ $VERSION -eq 9 ] || [ $VERSION -gt 9 ]; then

			START_SCRIPT_DIR=`/usr/bin/dirname $START_SCRIPT_DIR`
		fi
		if [ ! -f "$START_SCRIPT_DIR/boot.properties" ]; then
			# SCMSGS
			# @explanation
			# Need explanation of this message!
			# @user_action
			# Need a user action for this message.
			scds_syslog -p error -t $syslog_tag -m \
				"boot.properties does not exist in the directory $START_SCRIPT_DIR.
			 	This is required as the SMOOTH_SHUTDOWN extension property is set
			 	to TRUE."
			return 1
		fi
	fi
	return 0

}

#
# main() function
#
main()
{
	case "$action" in
		validate)
			validate_wls
			rc=$?
			;;
		stop)
			#First validate 

			validate_wls
			if [ $? -ne 0 ]
			then
				return 1
			fi

			#We have WLS_USER and WLS_PW
			#Now call the stop method

			stop_wls
			rc=$?
			;;
		*)
			rc=1
	esac

	exit $rc
}

main
