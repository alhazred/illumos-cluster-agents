/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2002 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#pragma ident	"@(#)wls_probe.c	1.5	07/06/06 SMI"

/*
 * wls_probe.c - Probe for WebLogic Server
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <rgm/libdsdev.h>
#include "wls.h"

int
main(int argc, char *argv[])
{
	scds_handle_t		scds_handle;
	int			rc, timeout;
	int			probe_result;
	hrtime_t		ht1, ht2;
	long			dt;
	bea_extn_props_t	beaextnprops;
	scha_str_array_t	*uris = NULL;
	scha_extprop_value_t	*prop = NULL;

	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR)
		return (1);

	/*
	 * Get the extension properties that should be passed to the
	 * probe method.
	 */

	if (get_wls_extn_props(scds_handle, &beaextnprops, B_FALSE) != 0) {
		scds_syslog(LOG_ERR,
			"Failed to retrieve WLS extension properties.");
		scds_close(&scds_handle);
		return (1);
	}

	/* Get the monitor uri list */
	rc = scds_get_ext_property(scds_handle, "Monitor_Uri_List",
		SCHA_PTYPE_STRINGARRAY, &prop);
	if (rc != SCHA_ERR_NOERR) {
		/*
		 * Logging as NOTICE because this failure will not be considered
		 * as an error and will not fail the probe.
		 */
		scds_syslog(LOG_NOTICE,
			"Failed to retrieve the property %s: %s.",
			"Monitor_Uri_List.", scds_error_string(rc));

		/*
		 * If we cannot retrieve the URI's We dont have to fail
		 * the probe. we will not probe the uris. We still can
		 * probe the server_url.
		 */
		uris = NULL;
	} else {
		uris = prop->val.val_strarray;
	}

	/* Get the probe timeout from the extension property */
	timeout = scds_get_ext_probe_timeout(scds_handle);

	for (;;) {

		/*
		 * sleep for a duration of thorough_probe_interval between
		 *  successive probes.
		 */
		(void) scds_fm_sleep(scds_handle,
		    scds_get_rs_thorough_probe_interval(scds_handle));

		ht1 = gethrtime(); /* Latch probe start time */


		/* Probe the WebLogic Server */
		probe_result = svc_probe(scds_handle, &beaextnprops,
					timeout, uris);

		scds_syslog_debug(DBG_LEVEL_HIGH,
			"Probe result = %d. ", probe_result);

		ht2 = gethrtime();

		/* Convert to milliseconds */
		dt = (long)((ht2 - ht1) / 1e6);

		/*
		 * Compute failure history and take action if needed
		 */
		(void) scds_fm_action(scds_handle, probe_result, dt);

	} 	/* Keep probing forever */
}
