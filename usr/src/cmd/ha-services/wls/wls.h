/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_WLS_H
#define	_WLS_H

#pragma ident	"@(#)wls.h	1.5	07/06/06 SMI"

#ifdef __cplusplus
extern "C" {
#endif

/* Debug levels for error messages */
#define	DBG_LEVEL_HIGH		9
#define	DBG_LEVEL_MED		5
#define	DBG_LEVEL_LOW		1

#define	SCDS_CMD_SIZE		(8 * 1024)

#define	SCDS_ARRAY_SIZE		1024

#define	DB_NOT_AVAILABLE	200
/*
* BEA extension property variables
*/
#define	EXTN_SERVER_URL		"Server_url"
#define	EXTN_START_SCRIPT	"Start_script"
#define	EXTN_MONITOR_URI_LIST	"Monitor_uri_list"
#define	EXTN_SERVER_NAME	"Server_name"
#define	EXTN_DB_PROBE_SCRIPT	"DB_Probe_Script"

/* structure to hold the BEA WLS extension properties */

typedef struct bea_extension_props
{
	char		*home_dir;
	char		*server_url;
	char		*start_script;
	char		*server_name;
	char		*db_probe_script;
}bea_extn_props_t;

int svc_validate(scds_handle_t scds_handle, bea_extn_props_t *beaextnprops,
		boolean_t print_messages);

int svc_start(scds_handle_t scds_handle, bea_extn_props_t *beaextnprops);

int svc_stop(scds_handle_t scds_handle);

int svc_wait(scds_handle_t scds_handle, bea_extn_props_t *beaextnprops);

int mon_start(scds_handle_t scds_handle);

int mon_stop(scds_handle_t scds_handle);

int svc_probe(scds_handle_t scds_handle, bea_extn_props_t *beaextnprops,
	int timeout, scha_str_array_t *uris);

int get_wls_extn_props(scds_handle_t scds_handle,
	bea_extn_props_t *beaextnprops, boolean_t print_messages);

int probe_server(scds_handle_t scds_handle, char *url, int timeout,
	boolean_t print_messages);

int probe_db(scds_handle_t scds_handle, char *db_probe_script, int timeout);

int validate_monitor_uri_list(scds_handle_t scds_handle,
	scds_net_resource_list_t *snrlp, boolean_t print_messages);

int validate_server_url(char *uri, scds_net_resource_list_t *snrlp,
		boolean_t print_messages);

int validate_smooth_shutdown(scds_handle_t scds_handle,
	boolean_t print_messages, bea_extn_props_t *beaextnprops);

int smooth_shutdown_wls(scds_handle_t scds_handle);

#ifdef __cplusplus
}
#endif

#endif	/* _WLS_H */
