/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)wls.c	1.8	07/06/06 SMI"

/*
 * wls.c - Common utilities for WLS
 *
 * This file has the methods for performing the validation, starting and
 * stopping the data service and the fault monitor. It also contains the method
 * to probe the health of the data service.  The probe just returns either
 * success or failure. Action is taken based on this returned value in the
 * method found in the file wls_probe.c
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <scha.h>
#include <rgm/libdsdev.h>
#include <errno.h>
#include <libintl.h>
#include <libgen.h>
#include "wls.h"
#include "../common/ds_common.h"

/*
 * The initial timeout allowed  for the WLS dataservice to
 * be fully up and running. We will wait for for 5 % (SVC_WAIT_PCT)
 * of the start_timeout time before probing the service.
 */
#define	SVC_WAIT_PCT		5

/*
 * We need to wait for SVC_WAIT_TIME ( 8 secs) for pmf
 * to send the failure message before probing the service
 */

#define	SVC_WAIT_TIME		8

#define	SOFT_STOP_PCT		50

/* Percentage of start/probe timeout for the DB probe */
#define	DB_PROBE_TIMEOUT_PCT	25

#define	INT_MAX	2147483647

/* WLS start up messages redirecting to /var/cluster/ */
#define	CLUSTER_LOG_DIR		"/var/cluster/logs"

#define	WLS_LOG_DIR		"hawls"


/*
 * svc_validate():
 *
 * Do WLS specific validation of the resource configration.
 *
 */

int
svc_validate(scds_handle_t scds_handle, bea_extn_props_t *beaextnprops,
		boolean_t print_messages)
{
	int 			err = 0, rc = 0;
	char			*config_dir_name;
	char			*start_script_dir = NULL;
	char			wls_config_file[MAXPATHLEN];
	struct stat		statbuf;
	scds_hasp_status_t	hasp_status;
	scds_net_resource_list_t	*snrlp = NULL;

	scds_syslog_debug(DBG_LEVEL_HIGH,
		"In svc_validate() method of WLS resource");

	/* check for HAStoragePlus resources */
	rc = scds_hasp_check(scds_handle, &hasp_status);

	if (rc != SCHA_ERR_NOERR) {
		/* scha_hasp_check() logs msg when it fails */
		if (print_messages) {
			(void) fprintf(stderr,
				gettext("HASP check failed.\n"));
		}
		return (1);
	}

	if (hasp_status == SCDS_HASP_NOT_ONLINE) {
		scds_syslog(LOG_ERR, "Resource depends on a "
			"SUNW.HAStoragePlus type resource that is "
			"not online anywhere.");
		if (print_messages) {
			(void) fprintf(stderr, gettext("Resource depends on "
				"a SUNW.HAStoragePlus type resource that is "
				"not online anywhere.\n"));
		}
		return (1);
	} else if (hasp_status == SCDS_HASP_ERR_CONFIG) {
		/* problem syslogged by scds_hasp_check */
		if (print_messages) {
			(void) fprintf(stderr, gettext("This resource depends "
					"on a HAStoragePlus resouce that is in "
					"a different Resource Group. This "
					"configuration is not supported.\n"));
		}
		return (1);
	}

	/* don't check on the global stuff if HASP is not local. */
	if (hasp_status == SCDS_HASP_ONLINE_NOT_LOCAL) {
		scds_syslog_debug(DBG_LEVEL_HIGH, "scha_hasp_check() returned"
			"SCDS_HASP_ONLINE_NOT_LOCAL, rc = %d", rc);
		/* The filesystem is not local. Just return 0 */
		rc = 0;
		goto finished;
	}

	/*
	 * If we are here then we are either on a global file system
	 * or a locally mounted HASP filesystem.
	 */

	/*
	 * Make sure setWLSEnv.sh and startWLS.sh scripts exists in
	 * $WL_HOME/server/bin directory and have execute permissions.
	 */

	/* validate setWLSEnv.sh */

	err = ds_validate_file(print_messages, S_IXUSR,
			"%s/server/bin/setWLSEnv.sh", beaextnprops->home_dir);
	if (err != SCHA_ERR_NOERR) {
		/*
		 * The above function would log the appropriate error
		 * message
		 */
		rc = 1;
		goto finished;
	}

	/* validate startWLS.sh */
	err = ds_validate_file(print_messages, S_IXUSR,
			"%s/server/bin/startWLS.sh", beaextnprops->home_dir);
	if (err != SCHA_ERR_NOERR) {
		rc = 1;
		goto finished;
	}

	/* validate start script set in the extension property Start_script */
	err = ds_validate_file(print_messages, S_IXUSR,
			beaextnprops->start_script);
	if (err != SCHA_ERR_NOERR) {
		rc = 1;
		goto finished;
	}

	/*
	 * For versions earlier than 9.0, validate config.xml to make sure
	 * it is in the same directory as the start script.
	 */
	start_script_dir = strdup(beaextnprops->start_script);
	if (start_script_dir == NULL) {
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
			"Failed to allocate space for "
			"start_script_dir");
		if (print_messages) {
			(void) fprintf(stderr, gettext("INTERNAL ERROR: %s.\n"),
				"Failed to allocate space for "
				"start_script_dir");
		}
		rc = 1;
		goto finished;
	}

	/*
	 * Get the directory in which the start script exists
	 * The following code change is to accomodate the new
	 * directory structure that has been introduced in
	 * the WebLogic Server 9.0 version. From 9.0 onwards,start scripts
	 * and config.xml exist in domain_name/bin and domain_name/config
	 * directory respectively. This code checks
	 * to see if config.xml exists in the start script directory
	 * else get the domain_name directory and check if config.xml
	 * is present in domain_name/config directory.
	 */
	config_dir_name = dirname(start_script_dir);

	/*
	 * Check if config.xml exists in config_dir_name directory.If stat
	 * fails, it could be Weblogic 9.0 version. Check if config.xml is
	 * present in domain_name/config directory.If stat fails, then throw
	 * a message that config.xml is not present either in config_dir_name
	 * directory or config_dir_name/config directory.
	 */
	(void) sprintf(wls_config_file, "%s/config.xml", config_dir_name);
		if (stat(wls_config_file, &statbuf) != 0) {
			/* if we are here, WLS version is either 9.0 or above */
			config_dir_name = dirname(config_dir_name);
			(void) sprintf(wls_config_file, "%s/config/config.xml",
				config_dir_name);
			if (stat(wls_config_file, &statbuf) != 0) {
				rc = errno;
				/*
				 * SCMSGS
				 * @explanation
				 * The config.xml file does not exist either
				 * in the <domain> or <domain>/config
				 * directory.
				 * @user_action
				 * Make sure if config.xml file is present in
				 * the <domain> or <domain>/config directory
				 * and also make sure if proper permissions
				 * are set for the file.
				 */
				scds_syslog(LOG_ERR,
					"Cannot access "
					"file %s/config.xml "
					"or %s/config/config.xml: %s",
					config_dir_name, config_dir_name,
					strerror(rc));
				if (print_messages) {
					(void) fprintf(stderr,
					gettext("Cannot access file %s"
					"/config.xml or %s/config/config.xml:"
					" %s\n"), config_dir_name,
					config_dir_name, gettext(strerror(rc)));
				}
				rc = 1;
				goto finished;
			}
		}

	scds_syslog_debug(DBG_LEVEL_HIGH, "The config.xml used for starting"
			" Weblogic Server is %s", wls_config_file);
	if ((statbuf.st_mode & S_IRUSR) != S_IRUSR) {
			scds_syslog(LOG_ERR,
				"Incorrect permissions set for %s.",
				wls_config_file);
			if (print_messages) {
				(void) fprintf(stderr,
				gettext("Incorrect permissions "
				"set for %s.\n"), wls_config_file);
			}
			rc = 1;
			goto finished;
	}


	/*
	 * If the Database probe script is set, make sure the script
	 * is executable.
	 */
	if (strcmp(beaextnprops->db_probe_script, "") != 0) {
		scds_syslog_debug(DBG_LEVEL_HIGH, "DB probe script set in "
							"extn props");
		/* validate the db_probe_script */
		err = ds_validate_file(print_messages, S_IXUSR,
				beaextnprops->db_probe_script);
		if (err != SCHA_ERR_NOERR) {
			/*
			 * The above function would log the appropriate error
			 * message
			 */
			rc = 1;
			goto finished;
		}
	}

	/*
	 * validate the server_url and also make sure it uses the http protocol
	 * and not https
	 */

	/*
	 * Return an error if there is an error when trying to get the
	 * available network address resources for this resource
	 * and if no Failover IP resources are created in this RG.
	 */
	if ((rc = scds_get_rs_hostnames(scds_handle, &snrlp))
		!= SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		"Error in trying to access the configured network "
		"resources : %s.", scds_error_string(rc));
		if (print_messages) {
			(void) fprintf(stderr,
			gettext("Error in trying to access the "
				"configured network "
				"resources : %s."),
		scds_error_string(rc));
		}
		rc = 1;
		goto finished;
	}

	rc = validate_server_url(beaextnprops->server_url, snrlp,
					print_messages);

	if (rc != 0) {
		/* Appropriate error message is logged by the above function */
		rc = 1;
		goto finished;
	}

	/* validate all the uris and make sure they use http in the url */
	rc = validate_monitor_uri_list(scds_handle, snrlp, print_messages);

	if (rc != 0) {
		/* Appropriate error message is logged by the above function */
		rc = 1;
		goto finished;
	}

	/*
	 * validate the SMOOTH_SHUTDOWN extension property. If this flag is TRUE
	 * then the WLS_USER and WLS_PW have to be set in start_script itself
	 * so that the STOP method can use the username and password to shutdown
	 * the WebLogic Server smoothly. If this extension is FALSE(which is the
	 * default value) then no validation is done. The STOP method will use
	 * kill -9 to force shutdown the WLS. For weblogic versions earlier than
	 * 8.0, setting this flag to TRUE will expose the username and password
	 * to everyone as the they have to be passed to the shutdown command.
	 * For WLS versions 8.0 and above, these values will be encrypted in
	 * boot.properties file present in start script directory or domain_name
	 * directory as is the case.
	 */

	rc = validate_smooth_shutdown(scds_handle, print_messages,
				beaextnprops);

	if (rc != 0) {
		/* Appropriate error message is logged by the above function */
		rc = 1;
		goto finished;
	}

	/* All validation checks were successful */
	rc = 0;

finished:
	if (start_script_dir)
		free(start_script_dir);
	return (rc); /* return result of validation */
}


/*
 * svc_start():
 *
 */

int
svc_start(scds_handle_t scds_handle, bea_extn_props_t *beaextnprops)
{
	int 			rc;
	char			*rsname = NULL;
	char			*config_xml_dir;
	char			*start_script;
	char			*start_script_copy;
	char			*start_cmd;
	char			wls_log_file[MAXPATHLEN];

	rsname = (char *)scds_get_resource_name(scds_handle);

	scds_syslog_debug(DBG_LEVEL_HIGH,
		"In svc_start() method of resource %s",
		rsname);

	/*
	 * Get the directory of the  start command . This is the directory
	 * where the config.xml file also resides. If the config.xml file
	 * is not present in the same directory as the start script the
	 * WL Server startup will fail.
	 */
	start_script_copy = strdup(beaextnprops->start_script);
	if (start_script_copy == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * An internal error has occurred.
		 * @user_action
		 * Save a copy of the /var/adm/messages files on all nodes.
		 * Contact your authorized Sun service provider for assistance
		 * in diagnosing the problem.
		 */
		scds_syslog(LOG_ERR,
			"Failed to allocate space for %s.",
			beaextnprops->start_script);
	}

	config_xml_dir = dirname(start_script_copy);

	/* Get the startup script from the complete path */

	start_script = basename(beaextnprops->start_script);

	scds_syslog_debug(DBG_LEVEL_HIGH,
		"The directory in which the start script is present is %s "
		"and the start script is %s", config_xml_dir, start_script);

	/*
	 * Fix for 4838969: add option for user to send wls log and error
	 * msgs to a file.
	 */

	(void) sprintf(wls_log_file, "%s/%s", CLUSTER_LOG_DIR, WLS_LOG_DIR);

		if (mkdirp(wls_log_file, (mode_t)0755) != 0) {
			if (errno != EEXIST) {  /*lint !e746 */
			scds_syslog(LOG_ERR,
				"Unable to create directory %s: %s.",
				wls_log_file, strerror(errno));
			return (1);
			}
		}
	(void) strcat(wls_log_file, "/");
	(void) strcat(wls_log_file,
		(char *)scds_get_resource_name(scds_handle));

	scds_syslog_debug(DBG_LEVEL_HIGH,
		"The weblogic startup console messages are logged in "
		"the file %s \n", wls_log_file);
	/*
	 * Construct the start command . The start command has to be run
	 * by cd'ing to its directory and then start it from there
	 */
	start_cmd = ds_string_format("/usr/bin/sh -c \
		\"cd %s; ./%s %s > %s 2>&1\"",
		config_xml_dir, start_script, beaextnprops->server_name,
		wls_log_file);

	if (start_cmd == NULL) {
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
			"String handling error creating start"
			"command.");
		return (1);
	}

	scds_syslog_debug(DBG_LEVEL_HIGH,
		"The command <%s> is used to start the WLS under pmf",
		start_cmd);

	/*
	 * start the WebLogic Server under pmf.
	 * The server start script is passed to pmf.
	 */
	rc = scds_pmf_start(scds_handle, SCDS_PMF_TYPE_SVC, 0,
			start_cmd, -1);

	if (rc == SCHA_ERR_NOERR) {
		scds_syslog_debug(DBG_LEVEL_LOW,
			"WebLogic server configured in resource %s "
			"started under pmf", rsname);
		free(start_cmd);
		return (rc);
	} else {
		/*
		 * SCMSGS
		 * @explanation
		 * The WebLogic Server configured in the resource could not be
		 * started by the agent.
		 * @user_action
		 * Try to start the Weblogic Server manually and check if it
		 * can be started manually. Make sure the logical host is UP
		 * before starting the WLS manually. If it fails to start
		 * manually then check your configuration and make sure it can
		 * be started manually before it can be started by the agent.
		 * Make sure the extension properties are correct. Make sure
		 * the port is not already in use.
		 */
		scds_syslog(LOG_ERR,
			"Failed to start the WebLogic server configured"
			"in resource %s", rsname);
		free(start_cmd);
		return (rc);
	}
}

/*
 * svc_stop():
 *
 * Stop the WebLogic server
 * Return 0 on success, > 0 on failures.
 * We are not using the smooth shutdown using the weblogic.admin command
 * because it needs a username and password which is a security issue.
 * We decided to just kill the process using SIGKILL.
 *
 */
int
svc_stop(scds_handle_t scds_handle)
{
	char			*rsname = NULL, *rgname = NULL;
	int			rc;

	rsname = (char *)scds_get_resource_name(scds_handle);
	rgname = (char *)scds_get_resource_group_name(scds_handle);

	scds_syslog_debug(DBG_LEVEL_HIGH,
		"In svc_stop() method of BEA resource %s.", rsname);

	/*
	 * Try smooth_shutdown first before doing a kill -9.
	 * The smooth_shutdown will be tried only if the
	 * extension property Smooth_shutdown is set to true.
	 * We ignore the return value of the smooth shutdown
	 * function as we will go ahead and do a kill -9 anyway.
	 */

	(void) smooth_shutdown_wls(scds_handle);

	/*
	 * In sc 3.1 scds_pmf_stop takes -1 as the timeout which
	 * gives infinite timeout to the pmfd for stopping the
	 * WLS. This is needed in situations when RGM has suspended
	 * timeouts. This bug fix went into only 3.1 and not in
	 * sc 3.0. For backward compatibility (since this agent
	 * has to work on sc 3.0 also) we are setting the timeout to
	 * INT_MAX which is a very BIG value and would not timeout the
	 * stop method.
	 */
	if (scds_pmf_stop(scds_handle, SCDS_PMF_TYPE_SVC,
			SCDS_PMF_SINGLE_INSTANCE, SIGKILL,
				INT_MAX) != SCHA_ERR_NOERR) {
		(void) scha_resource_setstatus(rsname, rgname,
			SCHA_RSSTATUS_FAULTED,
			"Failed to stop WLS.");
		rc = 1;
	} else {
		(void) scha_resource_setstatus(rsname, rgname,
			SCHA_RSSTATUS_OFFLINE,
			"Successfully stopped WLS.");
		rc = 0;
	}

	return (rc);
}

/*
 * svc_wait():
 *
 * wait for the data service to start up fully and make sure it is running
 * healthy
 */

int
svc_wait(scds_handle_t scds_handle, bea_extn_props_t *beaextnprops)
{
	int rc, svc_start_timeout, probe_timeout;
	scds_pmf_status_t	status;
	scha_err_t err;
	char *rsname = NULL;

	rsname = (char *)scds_get_resource_name(scds_handle);

	scds_syslog_debug(DBG_LEVEL_HIGH,
		"In svc_wait() method of BEA resource %s.", rsname);

	/*
	 * Get the Start method timeout and the Probe timeout value
	 */
	svc_start_timeout = scds_get_rs_start_timeout(scds_handle);
	probe_timeout = scds_get_ext_probe_timeout(scds_handle);

	/*
	 * sleep for SVC_WAIT_PCT percentage of start_timeout time
	 * before actually probing the dataservice. This is to allow
	 * the dataservice to be fully up inorder to reply to the
	 * probe. NOTE: the value for SVC_WAIT_PCT could be different
	 * for different dataservices.
	 */
	if (scds_svc_wait(scds_handle, (svc_start_timeout * SVC_WAIT_PCT)/100)
	    != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to start service.");
		return (1);
	}

	do {
		/*
		 * probe the WLS and check if it is UP.
		 */

		/*
		 * probe the server_url We dont call svc_probe() because it
		 * probes the server_url and also the URI(s). During startup
		 * we just need to make sure the server is UP and let the
		 * probe handle the complete probing. So we just call
		 * probe_server()
		 */
		rc = probe_server(scds_handle, beaextnprops->server_url,
				probe_timeout, B_FALSE);

		if (rc == SCHA_ERR_NOERR) {
			/* Return success */
			scds_syslog_debug(DBG_LEVEL_HIGH,
				"In svc_wait() probe returned success");
			return (SCHA_ERR_NOERR);
		}

		err = scds_pmf_get_status(scds_handle,
		    SCDS_PMF_TYPE_SVC, SCDS_PMF_SINGLE_INSTANCE, &status);
		if (err != SCHA_ERR_NOERR) {
			scds_syslog(LOG_ERR,
			    "Failed to retrieve process monitor facility tag.");
			return (1);
		}

		/* Check if the dataservice is still up and running */
		if (status != SCDS_PMF_MONITORED) {
			scds_syslog(LOG_ERR,
			    "Application failed to stay up. "
			    "Start method Failure.");
			return (1);
		}

		/*
		 * Dataservice is still trying to come up. Sleep for a while
		 * before probing again.
		 */
		/*
		 * SCMSGS
		 * @explanation
		 * This is just a informational message that the WLS is still
		 * starting up.
		 * @user_action
		 * None
		 */
		scds_syslog(LOG_NOTICE, "Waiting for WLS to startup");

		if (scds_svc_wait(scds_handle, SVC_WAIT_TIME) !=
			SCHA_ERR_NOERR) {
			scds_syslog(LOG_ERR, "Failed to start service.");
			return (1);
		}

	/* We rely on RGM to timeout and terminate the program */
	} while (1);

}

/*
 * This function starts the fault monitor for the WLS resource.
 * This is done by starting the probe under PMF. The PMF tag
 * is derived as <RG-name,RS-name,instance_number.mon>. The restart option
 * of PMF is used but not the "infinite restart". Instead
 * interval/retry_time is obtained from the RTR file.
 */

int
mon_start(scds_handle_t scds_handle)
{
	scha_err_t	err;

	scds_syslog_debug(DBG_LEVEL_HIGH,
	    "Calling MONITOR_START method for resource <%s>.",
	    scds_get_resource_name(scds_handle));

	/*
	 * The probe wls_probe is assumed to be available in the same
	 * subdirectory where the other callback methods for the RT are
	 * installed. The last parameter to scds_pmf_start denotes the
	 * child monitor level. Since we are starting the probe under PMF
	 * we need to monitor the probe process only and hence we are using
	 * a value of 0.
	 */

	err = scds_pmf_start(scds_handle, SCDS_PMF_TYPE_MON,
	    SCDS_PMF_SINGLE_INSTANCE, "wls_probe", 0);

	if (err != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to start fault monitor.");
		return (err);
	}

	scds_syslog(LOG_INFO, "Started the fault monitor.");

	return (SCHA_ERR_NOERR); /* Successfully started Monitor */
}


/*
 * This function stops the fault monitor for the WLS resource.
 * This is done via PMF. The PMF tag for the fault monitor is
 * constructed based on <RG-name_RS-name,instance_number.mon>.
 */

int
mon_stop(scds_handle_t scds_handle)
{
	scha_err_t	err;

	scds_syslog_debug(DBG_LEVEL_HIGH, "Calling scds_pmf_stop method");

	err = scds_pmf_stop(scds_handle, SCDS_PMF_TYPE_MON,
	    SCDS_PMF_SINGLE_INSTANCE, SIGKILL,
	    scds_get_rs_monitor_stop_timeout(scds_handle));

	if (err != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to stop fault monitor.");
		return (err);
	}

	scds_syslog(LOG_INFO, "Stopped the fault monitor.");

	return (SCHA_ERR_NOERR); /* Successfully stopped monitor */
}


/*
 * svc_probe(): Do data service specific probing. Return a value
 * between 0 (success) and 100(complete failure).
 *
 * This function probes the server_url and then probes the uri(s)(if
 * set).
 *
 */
int
svc_probe(scds_handle_t scds_handle, bea_extn_props_t *beaextnprops,
		int timeout, scha_str_array_t *uris)
{
	int  				i, rc = 0;
	int 				probe_status = 0;
	int				db_probe_timeout;
	char				*rsname = NULL;
	char				*rgname = NULL;

	scds_syslog_debug(DBG_LEVEL_HIGH,
		"In svc_probe() method of resource %s",
		scds_get_resource_name(scds_handle));

	/*
	 * Probe the WLS using the  server url specified in the
	 * Server_url extension property.
	 * If the Monitor_uri_list extension property is non NULL
	 * probe the URIs.
	 * If any of the above probes fail, check if the DB is running
	 * by calling the db_probe(). If the DB is down then throw a
	 * message and return (0) so that the Admin can start the DB.
	 * If server url probe fails it is considered 100% failure. If
	 * the uri probing fails with http error 500 it is considered 100%
	 * failure. Other http error codes are not considered as failures.
	 * A failure status is returned for WLS probe failures only if
	 * the DB probe succeeds since it doesnt make sense to take action
	 * on the WLS failures if the DB is not UP.
	 *
	 * We give the full probe timeout of server_url_timeout for all the
	 * url/uri probes and DB_PROBE_TIMEOUT_PCT of probe_timeout for DB
	 * probes.
	 */

	/* probe the server_url */
	rc = probe_server(scds_handle, beaextnprops->server_url, timeout,
			B_TRUE);

	if (rc != 0) {

		/*
		 * The return value rc from probe_server will have the proper
		 * value set (COMPLETE_FAILURE or COMPLETE_FAILURE/2 ...)
		 */
		probe_status = rc;

		/*
		 * SCMSGS
		 * @explanation
		 * The probing of the url set in the Server_url extension
		 * property failed. The agent probe will take action.
		 * @user_action
		 * None. The agent probe will take action. However, the cause
		 * of the failure should be investigated further. Examine the
		 * log file and syslog messages for additional information.
		 */
		scds_syslog(LOG_ERR, "Server_url %s probe failed",
			beaextnprops->server_url);

		scds_syslog_debug(DBG_LEVEL_HIGH, "Probing to make sure the DB "
				"is UP Before taking action on the WLS");

		db_probe_timeout = (timeout * DB_PROBE_TIMEOUT_PCT) / 100;

		/*
		 * probe the DB and make sure it is running before declaring
		 * the WLS probe as complete failure.
		 */
		rc = probe_db(scds_handle, beaextnprops->db_probe_script,
				db_probe_timeout);

		if (rc != 0) {

			rsname = (char *)scds_get_resource_name(scds_handle);
			rgname = (char *)scds_get_resource_group_name(
				scds_handle);

			/*
			 * SCMSGS
			 * @explanation
			 * probing of the URL's set in the Server_url or the
			 * Monitor_uri_list failed. Before taking any action
			 * the WLS probe would make sure the DB is up (if a
			 * db_probe_script extension property is set). But,
			 * the DB probe also failed. The probe will not take
			 * any action till the DB is UP and the DB probe
			 * succeeds.
			 * @user_action
			 * Start the Data Base and make sure the DB probe (the
			 * script set in the db_probe_script) returns 0 for
			 * success. Once the DB is started the WLS probe will
			 * take action on the failed WLS instance.
			 */
			scds_syslog(LOG_ERR, "The Data base probe %s also "
				"failed. Will restart or failover the WLS "
				"only if the DB is UP",
				beaextnprops->db_probe_script);
			(void) scha_resource_setstatus(rsname, rgname,
				SCHA_RSSTATUS_DEGRADED,
				"Database not available.");
			probe_status = 0;
			goto finished;
		}

		scds_syslog_debug(DBG_LEVEL_HIGH, "DB probe successful. "
				"WLS probe failed. Setting the WLS probe "
				"as complete failure");

		goto finished;

	}
	scds_syslog_debug(DBG_LEVEL_HIGH, "Server_Url probe successful");

	/* probe the monitor uris */

	/* If the Monitor_uri_list is not null probe the list of uris */

	if (uris == NULL) {
		scds_syslog_debug(DBG_LEVEL_HIGH, "uri(s) structure is null"
			"No uri probing done");
		goto finished;
	}

	/* probe the uri(s) */
	for (i = 0; i < (int)uris->array_cnt; i++) {
		char *uri = uris->str_array[i];

		scds_syslog_debug(DBG_LEVEL_HIGH, "Probing uri %s", uri);

		rc = probe_server(scds_handle, uri, timeout, B_TRUE);

		if (rc != 0) {
			/*
			 * The return value rc from probe_server will have the
			 * proper value set (COMPLETE_FAILURE or
			 * COMPLETE_FAILURE/2 ...).
			 * Set the probe_status to the return value of
			 * probe_server.
			 */
			probe_status = rc;

			/*
			 * SCMSGS
			 * @explanation
			 * The probing of the url set in the monitor_uri_list
			 * extension property failed. The agent probe will
			 * take action.
			 * @user_action
			 * None. The agent probe will take action. However,
			 * the cause of the failure should be investigated
			 * further. Examine the log file and syslog messages
			 * for additional information.
			 */
			scds_syslog(LOG_ERR, "uri <%s> probe failed", uri);
			scds_syslog_debug(DBG_LEVEL_HIGH, "Probe to make sure "
					"the DB is UP Before taking action on "
					"the WLS");

			db_probe_timeout =
				(timeout * DB_PROBE_TIMEOUT_PCT) / 100;

			/*
			 * probe the DB and make sure it is running before
			 * declaring the WLS probe as complete failure.
			 */
			rc = probe_db(scds_handle,
					beaextnprops->db_probe_script,
					db_probe_timeout);

			if (rc != 0) {
				rsname =
				    (char *)scds_get_resource_name(
				    scds_handle);
				rgname =
				    (char *)scds_get_resource_group_name(
				    scds_handle);
				scds_syslog(LOG_ERR, "The Data base probe %s "
					"also failed. Will restart or failover "
					"the WLS only if the DB is UP",
					beaextnprops->db_probe_script);
				(void) scha_resource_setstatus(rsname, rgname,
					SCHA_RSSTATUS_DEGRADED,
					"Database not available.");
				probe_status = 0;
				goto finished;
			}
			scds_syslog_debug(DBG_LEVEL_HIGH, "DB probe successful."
					" Setting the WLS probe as failed. WLS "
					"probe returned probe_status %d",
					probe_status);
			goto finished;
		}
		scds_syslog_debug(DBG_LEVEL_HIGH, "Monitor_uri_list %s "
				"probe also successful", uri);
	}

finished:

	return (probe_status);
}

/* probe the server using URL/URI */

int
probe_server(scds_handle_t scds_handle, char *uri, int timeout,
			boolean_t log_messages)
{

	int		rc;
	int		status;
	int		failure_code = 0;

	scds_syslog_debug(DBG_LEVEL_HIGH, "In probe_server() function");

	rc = ds_http_get_status(scds_handle, uri, &status, timeout,
			log_messages);
	scds_syslog_debug(DBG_LEVEL_HIGH, "ds_http_get_status returned "
				"rc = %d, status = %d for uri %s",
				rc, status, uri);

	switch (rc) {
	case SCHA_ERR_NOERR :
		if (status == 500) {
			failure_code = SCDS_PROBE_COMPLETE_FAILURE;
		}
		break;
	case SCHA_ERR_NOMEM:
	case SCHA_ERR_INVAL:
	case SCHA_ERR_INTERNAL:
		failure_code = 0;
		break;
	case SCHA_ERR_TIMEOUT:
		scds_syslog_debug(DBG_LEVEL_HIGH, "ds_http_get_status returned "
				"SCHA_ERR_TIMEOUT");
		failure_code = SCDS_PROBE_COMPLETE_FAILURE / 2;
		break;
	case SCHA_ERR_STATE:
		/* Connection refused */
		failure_code = SCDS_PROBE_COMPLETE_FAILURE;
		break;
	default:
		failure_code = 0;
		break;
	}

	return (failure_code);
}


/*
 * Function to probe the DB. If the user hasnt specified any script that will
 * probe the database then this function returns 0 assuming the DB is healthy
 * and the user has to make sure the DB is running before starting the WLS.
 * If a script is specified in the extension property "DB_Probe_Script" then
 * it is run under scds_timerun and the return value is passed to the calling
 * funtion.
 */
int
probe_db(scds_handle_t scds_handle, char *db_probe_script, int timeout)
{
	int	rc;
	int	probe_exit_status;

	scds_syslog_debug(DBG_LEVEL_HIGH, "In probe_db script");

	/* If the db_probe_script is empty string then return 0 */

	if (strcmp(db_probe_script, "") == 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * This is a informational messages. probing of the URL's set
		 * in the Server_url or the Monitor_uri_list failed. Before
		 * taking any action the WLS probe would make sure the DB is
		 * up (if a db_probe_script extension property is set). But,
		 * the Database probe script is not specified. The probe will
		 * assume that the DB is UP and will go ahead and take action
		 * on the WLS.
		 * @user_action
		 * Make sure the DB is UP.
		 */
		scds_syslog(LOG_NOTICE,
			"No Database probe script specified. Will "
			"Assume the Database is Running");
		return (0);
	}

	/*
	 * Validate method would have made sure the DB probe method
	 * is executable.
	 */

	/* run the probe method under scds_timerun */

	rc = scds_timerun(scds_handle, db_probe_script, timeout, SIGKILL,
		&probe_exit_status);

	if (rc != SCHA_ERR_NOERR) {
		if (rc == SCHA_ERR_TIMEOUT) {
			/*
			 * SCMSGS
			 * @explanation
			 * probing of the URL's set in the Server_url or the
			 * Monitor_uri_list failed. Before taking any action
			 * the WLS probe would make sure the DB is up. The
			 * Database probe script set in the extension property
			 * db_probe_script timed out while executing. The
			 * probe will not take any action till the DB is UP
			 * and the DB probe succeeds.
			 * @user_action
			 * Make sure the DB probe (set in db_probe_script)
			 * succeeds. Once the DB is started the WLS probe will
			 * take action on the failed WLS instance.
			 */
			scds_syslog(LOG_ERR, "The DB probe script %s "
				"Timed out while executing", db_probe_script);
		} else {
			/*
			 * SCMSGS
			 * @explanation
			 * This is an internal error.
			 * @user_action
			 * Save a copy of the /var/adm/messages files on all
			 * nodes. Contact your authorized Sun service provider
			 * for assistance in diagnosing the problem.
			 */
			scds_syslog(LOG_ERR, "Failed to run the DB "
				"probe script %s", db_probe_script);
		}
	}

	scds_syslog_debug(DBG_LEVEL_HIGH, "The DB probe utility returned %d",
			probe_exit_status);

	return (probe_exit_status);
}

/*
 * Function to validate the uri/url. This function makes sure that
 * the url/uri uses only http protocol and not any other protocol.
 * The WLS agent probe doesnt support https. Also, If a hostname
 * is not entered this validation will fail.
 */

int
validate_server_url(char *uri, scds_net_resource_list_t *snrlp,
			boolean_t print_messages)
{
	char    *scheme = NULL, *host = NULL, *port = NULL, *path = NULL;
	int		rc;
	int	i, j;

	scds_syslog_debug(DBG_LEVEL_HIGH, "In validate_server_url()");

	rc = ds_parse_simple_uri(uri, &scheme, &host, &port,
			&path);

	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Error parsing URI: %s", uri);
		if (print_messages) {
			(void) fprintf(stderr,
					gettext("Error parsing URI: %s\n"),
					uri);
		}
		return (1);
	}

	if ((strcasecmp(scheme, "http")) != 0) {
		scds_syslog(LOG_ERR,
			"URI (%s) must be an absolute http URI.", uri);
		if (print_messages) {
			(void) fprintf(stderr, gettext("URI <%s> must "
					"be an absolute "
					"http URI.\n"), uri);
		}
		rc = 1;
		goto finished;
	}

	/*
	* Return an error if there are no network address resources.
	* WLS needs atleast one Network resource in this RG.
	*/
	if (snrlp == NULL || snrlp->num_netresources == 0) {
		scds_syslog(LOG_ERR,
			"No network address resource in resource group.");
		if (print_messages) {
			(void) fprintf(stderr,
				gettext("No network address resource in "
					"resource group."));
		}
		rc = SCHA_ERR_INVAL;
		goto finished;
	}

	for (i = 0; i < snrlp->num_netresources; i++) {
		for (j = 0; j < snrlp->netresources[i].num_hostnames; j++) {
			if (strcasecmp(host,
				snrlp->netresources[i].hostnames[j]) == 0) {
				scds_syslog_debug(DBG_LEVEL_HIGH,
					"URI validation: match "
					"on %s [%d][%d]", host, i, j);
				rc = 0;
				goto finished;
			}
		}
	}


	/*
	 * If we get here we did not find a match for the hostname in
	 * any of the network resource hostnames.  Log and if
	 * requested print a message and set rc to SCHA_ERR_INVAL for
	 * the return.
	 */

	scds_syslog(LOG_ERR, "The hostname in %s is not a network address "
		"resource in this resource group.", uri);
	if (print_messages)
		(void) fprintf(stderr, gettext("The hostname in %s is not "
			"a network address resource in this "
				"resource group."), uri);
	rc = SCHA_ERR_INVAL;


finished:
	free(scheme);
	free(host);
	free(port);
	free(path);

	/* successful uri validation */

	return (rc);
}

/*
 * This function validates the monitor_uri_list extension property.
 * This function extracts the extension property and calls the function
 * validate_server_url for each uri.
 */

int
validate_monitor_uri_list(scds_handle_t scds_handle,
	scds_net_resource_list_t *snrlp, boolean_t print_messages)
{
	int		i, rc;
	scha_extprop_value_t	*prop = NULL;
	scha_str_array_t		*uris = NULL;

	scds_syslog_debug(DBG_LEVEL_HIGH, "In validate_monitor_uri_list()");

	/* Get the monitor uri list */
	rc = scds_get_ext_property(scds_handle, "Monitor_Uri_List",
			SCHA_PTYPE_STRINGARRAY, &prop);
	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
				"Failed to retrieve the "
				"property %s: %s.",
				"Monitor_Uri_List", scds_error_string(rc));
		if (print_messages) {
			(void) fprintf(stderr, gettext("Failed to retrieve "
					"the extension "
					"property %s: %s.\n"),
					"Monitor_Uri_List",
					scds_error_string(rc));
		}
		return (1);
	} else {
		uris = prop->val.val_strarray;
	}

	for (i = 0; i < (int)uris->array_cnt; i++) {
		char *uri = uris->str_array[i];

		scds_syslog_debug(DBG_LEVEL_HIGH, "Validating uri %s", uri);

		if (validate_server_url(uri, snrlp, print_messages) != 0) {
			scds_syslog(LOG_ERR,
				"Validation of URI %s failed", uri);
			if (print_messages) {
				(void) fprintf(stderr, gettext("Validation of "
						"URI %s failed"), uri);
			}
			return (1);
		}
	}

	scds_syslog_debug(DBG_LEVEL_HIGH, "Successfully validated the "
				"monitor_uri_list");
	return (0);
}

/*
 * Call the wls_utils script and validate the SMOOTH_SHUTDOWN extension
 * property.
 */
int
validate_smooth_shutdown(scds_handle_t scds_handle, boolean_t print_messages,
		bea_extn_props_t *beaextnprops)
{
	int			err, rc;
	const char		*rtbasedir;
	scha_extprop_value_t	*extnprop = NULL;
	char			*cmd;

	scds_syslog_debug(DBG_LEVEL_HIGH, "In validate_smooth_shutdown()");

	/* Get the extension property Smooth_shutdown */

	err = scds_get_ext_property(scds_handle, "Smooth_shutdown",
				SCHA_PTYPE_BOOLEAN, &extnprop);

	if (err != SCHA_ERR_NOERR || extnprop == NULL) {
		scds_syslog(LOG_ERR,
			"Failed to retrieve the property %s: %s.",
			"Smooth_shutdown", scds_error_string(err));
		if (print_messages) {
			(void) fprintf(stderr, gettext("Failed to retrieve "
				"the property %s: %s."), "Smooth_shutdown",
				scds_error_string(err));
		}
		return (1);
	}

	if (extnprop->val.val_boolean == B_FALSE) {
		scds_syslog_debug(DBG_LEVEL_HIGH, "The Smooth_shutdown"
			" is not set to TRUE. No need to validate");
		return (0);
	}

	scds_syslog_debug(DBG_LEVEL_HIGH, "The Smooth_shutdown"
		"flag is set to TRUE. Calling the wls_utils to validate"
		"the WLS_USER and WLS_PW");

	/* Get the RT base directory */
	rtbasedir = scds_get_rt_rt_basedir(scds_handle);

	scds_syslog_debug(DBG_LEVEL_HIGH, "Before ds_string_format  "
			"the start_script is %s", beaextnprops->start_script);
	cmd = ds_string_format("%s/wls_utils validate %s %s %s", rtbasedir,
			beaextnprops->home_dir, beaextnprops->server_url,
			beaextnprops->start_script);
	if (cmd == NULL) {
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
			"String handling error creating weblogic server"
			"validate command");
		if (print_messages) {
			(void) fprintf(stderr, gettext("INTERNAL ERROR: %s."),
				"String handling error creating weblogic "
				"server validate command");
		}
		return (1);
	}

	scds_syslog_debug(DBG_LEVEL_HIGH, "The command run to validate "
			"Smooth_shutdown is %s", cmd);

	rc = system(cmd);
	if (rc == -1) {
		scds_syslog(LOG_ERR,
			"Cannot execute %s: %s.",
			cmd, strerror(errno));	/*lint !e746 */
		if (print_messages) {
			(void) fprintf(stderr, gettext("Cannot execute "
					"%s: %s.\n"), cmd, strerror(errno));
		}
		free(cmd);
		return (1);
	}

	if (WIFSIGNALED((uint_t)rc) ||
		(WIFEXITED((uint_t)rc) && WEXITSTATUS((uint_t)rc) != 0)) {
		/*
		 * SCMSGS
		 * @explanation
		 * The command failed.
		 * @user_action
		 * Check the syslog and /var/adm/messages for more details.
		 */
		scds_syslog(LOG_ERR,
			"%s failed to complete", cmd);
		if (print_messages) {
			(void) fprintf(stderr, gettext("%s failed to complete"),
					cmd);
		}
		free(cmd);
		return (1);
	}

	scds_syslog_debug(DBG_LEVEL_HIGH, "WLS_USER and WLS_PW validation "
		"successful");
	free(cmd);
	return (0);
}

/*
 * Function to do a smooth shutdown of the WLS. This is done
 * only if the extension property Smooth_shutdown is set to TRUE.
 * This function will call the shell script wls_utils where the
 * actual work is done. This function wil fail if the WLS_USER
 * and the WLS_PW are not set in the start_script of the WLS.
 */
int
smooth_shutdown_wls(scds_handle_t scds_handle)
{
	int			err, rc;
	int			exit_code;
	time_t			start_time = time(NULL);
	int			stop_timeout, timeout;
	const char		*rtbasedir;
	scha_extprop_value_t	*extnprop = NULL;
	char			*stop_server;
	bea_extn_props_t	beaextnprops;
	char			*scheme, *host, *port, *path;


	scds_syslog_debug(DBG_LEVEL_HIGH,
		"In smooth_shutdown_wls()");

	/* Get the extension property Smooth_shutdown */

	err = scds_get_ext_property(scds_handle, "Smooth_shutdown",
				SCHA_PTYPE_BOOLEAN, &extnprop);

	if (err != SCHA_ERR_NOERR || extnprop == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * This is an internal error. The property could not be
		 * retrieved. The WLS stop method however will go ahead and
		 * kill the WLS process.
		 * @user_action
		 * Save a copy of the /var/adm/messages files on all nodes.
		 * Contact your authorized Sun service provider for assistance
		 * in diagnosing the problem.
		 */
		scds_syslog(LOG_ERR,
			"Failed to retrieve the property %s: %s. "
			"Will shutdown the WLS using sigkill",
			"Smooth_shutdown", scds_error_string(err));
		return (1);
	}

	if (extnprop->val.val_boolean == B_FALSE) {
		scds_syslog_debug(DBG_LEVEL_HIGH, "The Smooth_shutdown"
			" is not enabled to TRUE.");
		/*
		 * SCMSGS
		 * @explanation
		 * This is a information message. The Smooth_shutdown flag is
		 * not set to true and hence the WLS will be stopped using
		 * SIGKILL.
		 * @user_action
		 * None. If the smooth shutdown has to be enabled then set the
		 * Smooth_shutdown extension property to TRUE. To enable
		 * smooth shutdown the username and pasword that have to be
		 * passed to the "java weblogic.Admin .." has to be set in the
		 * start script. Refer to your Admin guide for details.
		 */
		scds_syslog(LOG_INFO, "Smooth_shutdown flag is not set "
			"to TRUE. The WebLogic Server will be shutdown "
			"using sigkill.");
		return (0);
	}

	/* Get Stop timeout for this resource */
	scds_syslog_debug(DBG_LEVEL_HIGH,
		"Smooth_shutdown flag set to TRUE. Will call wls_utils to"
			"shutdown");

	stop_timeout = scds_get_rs_stop_timeout(scds_handle);

	/* Get the RT base directory */
	rtbasedir = scds_get_rt_rt_basedir(scds_handle);

	/* First take the command out of PMF monitoring */

	rc = scds_pmf_stop_monitoring(scds_handle, SCDS_PMF_TYPE_SVC, 0);
	if (rc != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * The resource could not be taken out of pmf control. The WLS
		 * will however be shutdown by killing the process using
		 * sigkill.
		 * @user_action
		 * Save a copy of the /var/adm/messages files on all nodes.
		 * Contact your authorized Sun service provider for assistance
		 * in diagnosing the problem.
		 */
		scds_syslog(LOG_INFO, "Failed to take "
			"the resource out of PMF control. Will shutdown WLS "
			"using sigkill");
		return (rc);
	} else {
		scds_syslog_debug(DBG_LEVEL_HIGH,
			"WLS Resource taken out of PMF control");
	}

	/* Get the extension properties */
	if (get_wls_extn_props(scds_handle, &beaextnprops, B_FALSE) != 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * Failed to retrieve the WLS exension properties that are
		 * needed to do a smooth shutdown. The WLS stop method however
		 * will go ahead and kill the WLS process.
		 * @user_action
		 * Save a copy of the /var/adm/messages files on all nodes.
		 * Contact your authorized Sun service provider for assistance
		 * in diagnosing the problem.
		 */
		scds_syslog(LOG_ERR,
			"Failed to retrieve WLS extension properties."
			"Will shutdown the WLS using sigkill");
		return (1);
	}

	/*
	 * While trying to shutdown using the Weblogic.Admin the url has
	 * to be hostname:port and not http://hostname:port which the users
	 * would have set in the Server_url and which is needed by the
	 * http GET probe. So we strip the http:// from the url and pass it
	 * to the smooth shutdown script. This could be because Weblogic.Admin
	 * could be using t3 protocol (the default protocol) to connect
	 * to the WLS and not http.
	 *
	 *
	 * Strip the http:// from the Server_url. Get the hostname and
	 * port by parsing the server_url
	 */

	rc = ds_parse_simple_uri(beaextnprops.server_url, &scheme, &host,
			&port, &path);

	if (rc != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * There is an error in parsing the URL needed to do a smooth
		 * shutdown. The WLS stop method however will go ahead and
		 * kill the WLS process.
		 * @user_action
		 * Save a copy of the /var/adm/messages files on all nodes.
		 * Contact your authorized Sun service provider for assistance
		 * in diagnosing the problem.
		 */
		scds_syslog(LOG_ERR, "Error parsing URL: %s. Will shutdown "
			"the WLS using sigkill", beaextnprops.server_url);
		return (1);
	}


	stop_server =  ds_string_format("%s/wls_utils stop %s %s:%s %s",
			rtbasedir,
			beaextnprops.home_dir, host, port,
			beaextnprops.start_script);

	if (stop_server == NULL) {
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
			"String handling error creating weblogic server"
			"stop command. Will shutdown the WLS using sigkill");
		return (1);
	}

	scds_syslog_debug(DBG_LEVEL_HIGH, "The command used to do a smooth "
		"shutdown of the WLS is <%s>", stop_server);

	timeout = stop_timeout - (time(NULL) - start_time);
	timeout = (SOFT_STOP_PCT * timeout) / 100;

	/* Stop WL server */
	rc = scds_timerun(scds_handle, stop_server, timeout,
		SIGKILL, &exit_code);

	/*
	 * The exit_code is always "1" whether it is a
	 * succesful shutdown or a failure to shutdown.(This could
	 * be a bug in the Weblogic.Admin class run under java)
	 * Since we send sigkill anyway to pmf we are not
	 * going to check for the exit_code.
	 */

	if (rc != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * The Smooth shutdown of the WLS failed. The WLS stop method
		 * however will go ahead and kill the WLS process.
		 * @user_action
		 * Check the WLS logs for more details. Check the
		 * /var/adm/messages and the syslogs for more details to fix
		 * the problem.
		 */
		scds_syslog(LOG_ERR,
			"Failed to stop the WebLogic server smoothly."
			"Will try killing the process using sigkill");
	}

	if (stop_server)
		free(stop_server);

	return (rc);
}

/*
* get_wls_extn_props()
*
* Get the needed extension properties and store it in the
* beaextnprops structure. The Monitor_uri_list extension property
* is not stored in this structure as this is used only by the
* probe and hence will be retrieved in the probe function.
*
*/

int
get_wls_extn_props(scds_handle_t scds_handle, bea_extn_props_t *beaextnprops,
			boolean_t print_messages)
{
	scha_str_array_t 	*confdirs = NULL;
	scha_extprop_value_t	*extnprop = NULL;
	int	err;

	scds_syslog_debug(DBG_LEVEL_HIGH, "In get_wls_extn_props() function");

	if (beaextnprops == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * This is an internal Error.
		 * @user_action
		 * Save a copy of the /var/adm/messages files on all nodes.
		 * Contact your authorized Sun service provider for assistance
		 * in diagnosing the problem.
		 */
		scds_syslog(LOG_ERR,
		"INTERNAL ERROR: WLS extension properties structure is NULL.");
		if (print_messages) {
			(void) fprintf(stderr, gettext("INTERNAL ERROR: WLS "
			    "extension properties structure is NULL."));
		}
		return (1);
	}

	/*
	 * Get the path to the WebLogic Home directory which is the
	 * confdir_list.
	 */

	confdirs = scds_get_ext_confdir_list(scds_handle);

	/*
	 * Return an error if config_dirs extn prop is not set
	 * or if the array_cnt is not equal to 1.
	 * The array_cnt is expected to be 1 as each resource
	 * should configure only one WL Home directory.
	 */

	if (confdirs == NULL) {
		scds_syslog(LOG_ERR,
			"Property Confdir_list is not set.");
		if (print_messages) {
			(void) fprintf(stderr, gettext("Property Confdir_list "
			    "is not set."));
		}
		return (1);
	}
	if (confdirs->array_cnt != 1) {
		/*
		 * SCMSGS
		 * @explanation
		 * Each WLS resource can handle only a single installation of
		 * WLS. You cannot create a resource to handle multiple
		 * installations.
		 * @user_action
		 * Create a separate resource for making each installation of
		 * WLS Highly Available.
		 */
		scds_syslog(LOG_ERR,
			"Only a single path to the WLS Home directory "
			"has to be set in Confdir_list");
		if (print_messages) {
			(void) fprintf(stderr, gettext("Only a single path "
				"to the WLS Home directory has to be set in "
				"Confdir_list.\n"));
		}
		return (1);
	}

	beaextnprops->home_dir = confdirs->str_array[0];

	/* Get the extension property Server url */

	err = scds_get_ext_property(scds_handle, EXTN_SERVER_URL,
				SCHA_PTYPE_STRING, &extnprop);

	if (err != SCHA_ERR_NOERR || extnprop == NULL) {
		scds_syslog(LOG_ERR,
			"Failed to retrieve the property %s: %s.",
			EXTN_SERVER_URL, scds_error_string(err));
		if (print_messages) {
			(void) fprintf(stderr, gettext("Failed to retrieve the "
			    "extension property %s: %s.\n"), EXTN_SERVER_URL,
				scds_error_string(err));
		}
		return (1);
	}

	/* Save the extension property in the beaextnprops structure */

	beaextnprops->server_url = extnprop->val.val_str;

	/* Get the extension property Start script */

	err = scds_get_ext_property(scds_handle, EXTN_START_SCRIPT,
				SCHA_PTYPE_STRING, &extnprop);

	if (err != SCHA_ERR_NOERR || extnprop == NULL) {
		scds_syslog(LOG_ERR,
			"Failed to retrieve the property %s: %s.",
			EXTN_START_SCRIPT, scds_error_string(err));
		if (print_messages) {
			(void) fprintf(stderr, gettext("Failed to retrieve "
					"the extension property %s: %s.\n"),
					EXTN_START_SCRIPT,
					scds_error_string(err));
		}
		return (1);
	}

	/* Save the extension property in the beaextnprops structure */

	beaextnprops->start_script = extnprop->val.val_str;

	/* Get the extension property Server_name */

	err = scds_get_ext_property(scds_handle, EXTN_SERVER_NAME,
				SCHA_PTYPE_STRING, &extnprop);

	if (err != SCHA_ERR_NOERR || extnprop == NULL) {
		scds_syslog(LOG_ERR,
			"Failed to retrieve the property %s: %s.",
			EXTN_SERVER_NAME, scds_error_string(err));
		if (print_messages) {
			(void) fprintf(stderr, gettext("Failed to retrieve "
					"the extension property %s: %s.\n"),
					EXTN_SERVER_NAME,
					scds_error_string(err));
		}
		return (1);
	}

	/* Save the extension property in the beaextnprops structure */

	beaextnprops->server_name = extnprop->val.val_str;

	/* Get the extension property DB probe script */

	err = scds_get_ext_property(scds_handle, EXTN_DB_PROBE_SCRIPT,
				SCHA_PTYPE_STRING, &extnprop);

	if (err != SCHA_ERR_NOERR || extnprop == NULL) {
		scds_syslog(LOG_ERR,
			"Failed to retrieve the property %s: %s.",
					EXTN_DB_PROBE_SCRIPT,
					scds_error_string(err));
		if (print_messages) {
			(void) fprintf(stderr, gettext("Failed to retrieve "
			    "extension property %s.\n"), EXTN_DB_PROBE_SCRIPT);
		}
		return (1);
	}

	/* Save the extension property in the beaextnprops structure */

	beaextnprops->db_probe_script = extnprop->val.val_str;

	/*
	 *If we are here then we have all the extension properties needed
	 */

	scds_syslog_debug(DBG_LEVEL_LOW,
		"the BEA Home directory is %s "
		"the server url is %s "
		"the server start script is %s "
		"the server name is %s "
		"the db_probe_script is %s",
		beaextnprops->home_dir,
		beaextnprops->server_url,
		beaextnprops->start_script,
		beaextnprops->server_name,
		beaextnprops->db_probe_script);

	return (0);
}
