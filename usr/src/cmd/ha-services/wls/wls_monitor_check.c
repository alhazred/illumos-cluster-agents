/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#pragma ident	"@(#)wls_monitor_check.c	1.6	07/06/06 SMI"

/*
 * wls_monitor_check.c - Monitor Check method for wls
 */

#include <rgm/libdsdev.h>
#include "wls.h"

/*
 * Just make a simple validate check on the service
 */

int
main(int argc, char *argv[])
{
	bea_extn_props_t	beaextnprops;
	scds_handle_t   	scds_handle;
	int	rc;

	/* Process the arguments passed by RGM and initialize syslog */
	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR) {
		return (1);
	}

	/* Get the extension properties */

	if (get_wls_extn_props(scds_handle, &beaextnprops, B_FALSE) != 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * The WLS Extension properties could not be retrieved.
		 * @user_action
		 * Check for other messages in syslog and /var/adm/messages
		 * for details of failure.
		 */
		scds_syslog(LOG_ERR,
			"Failed to retrieve WLS extension properties.");
		return (1);
	}

	rc =  svc_validate(scds_handle, &beaextnprops, B_FALSE);
	scds_syslog_debug(DBG_LEVEL_HIGH, "monitor_check method "
	    "was called and returned <%d>.", rc);

	/* Free up all the memory allocated by scds_initialize */
	scds_close(&scds_handle);

	/* Return the result of validate method run as part of monitor check */
	return (rc);
}
