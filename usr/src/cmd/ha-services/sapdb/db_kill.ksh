#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# Terminate the kernel processes by signal (SIGKILL).
# Input: SAPDB name
#
#pragma ident	"@(#)db_kill.ksh	1.5	07/06/06 SMI"

PGREP=/usr/bin/pgrep
WC=/usr/bin/wc
XARGS=/usr/bin/xargs
PATH=$PATH:/usr/cluster/lib/sc
syslog_tag="SC[SUNW.sapdb,db_kill]"

if [ $# = 1 ]; then
	DB_NAME=$1
else
	# SCMSGS
	# @explanation
	# An internal error has occurred.
	# @user_action
	# Save a copy of the /var/adm/messages files on all nodes. Contact
	# your authorized Sun service provider for assistance in diagnosing
	# the problem.
	scds_syslog -p error -t syslog_tag -m \
		"INTERNAL ERROR: usage: `basename $0` <DB_Name>"
	exit 1
fi

# send SIGKILL to kernel processes in a loop
#
while [[ `$PGREP -f "pgm/kernel $DB_NAME"|$WC -l` -gt 0 ]]; do
	$PGREP -f "pgm/kernel $DB_NAME"|$XARGS kill -9
	sleep 1
done

exit 0
