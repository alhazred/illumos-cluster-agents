/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2003 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * sapdb_validate.c - validate method for SAPDB.
 */

#pragma ident	"@(#)sapdb_validate.c	1.8	07/06/06 SMI"

#include <ds_common.h>
#include "sapdb.h"

/* Use 50% of the validate timeout to check db w/ logical host */
#define	TIMEOUT_PCT	50

/*
 * Check to make sure that the properties have been set properly.
 */

int
main(int argc, char *argv[])
{
	scds_handle_t   scds_handle;
	int		rc;
	int		timeout = 0;
	sapdb_extprops_t	sapdbxpropsp;
	char			*db_state_cmd = NULL;
	boolean_t		hsp_online_not_local;

	/* I18N stuff */
	(void) setlocale(LC_ALL, "");
	(void) textdomain(TEXT_DOMAIN);
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	/* Process arguments passed by RGM and initialize syslog */
	if ((rc = scds_initialize(&scds_handle, argc, argv))
		!= SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to retrieve the resource "
			"handle: %s", scds_error_string(rc));
		(void) fprintf(stderr, gettext("Failed to retrieve the "
			"resource handle: %s\n"), scds_error_string(rc));
		return (1);
	}

	if (get_sapdb_extension(scds_handle, &sapdbxpropsp, B_TRUE) != 0) {
		scds_close(&scds_handle);
		return (1);
	}

	rc = svc_validate(scds_handle, &sapdbxpropsp, B_TRUE,
		&hsp_online_not_local);

	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to validate configuration.");
		(void) fprintf(stderr, gettext("Failed to validate "
			"configuration.\n"));
		goto finished;
	}

	if (hsp_online_not_local == B_TRUE) {
		/* hsp is online not local, skip the rest xserver checks */
		scds_syslog_debug(SCDS_DBG_HIGH, "Won't check xserver "
			"because hsp online not local.");
		rc = 0;
		goto finished;
	}

	/*
	 * check xserver running or not locally. Timeout is retrieved
	 * in db_state_local() and pass back since db_state() needs the
	 * timeout too.
	 */
	rc = db_state_local(scds_handle, sapdbxpropsp.ext_indep_prog_path,
		SCHA_VALIDATE_TIMEOUT, &timeout, B_TRUE);

	if (rc != 0) { /* xserver is not running, exit 0 */
		(void) fprintf(stderr, gettext("\nSAP xserver is not running, "
			"User_Key and related information were not verified."));
		rc = 0;
		goto finished;
	}

	/* xserver is running, do further checks w/ logical host */
	db_state_cmd = ds_string_format(
		"%s/%s %s/bin/dbmcli %s %s %s>/dev/null",
		scds_get_rt_rt_basedir(scds_handle),
		CHECK_DB_STATE, sapdbxpropsp.ext_indep_prog_path,
		sapdbxpropsp.ext_user_key,
		sapdbxpropsp.ext_pid_dir,
		sapdbxpropsp.ext_db_name);
	if (db_state_cmd == NULL) {
		ds_internal_error("Failed to create command string to "
			"check SAP DB state");
		rc = 1;
		goto finished;
	}

	/*
	 * Need to run the check under 50% of validate timeout.
	 * otherwise, RGM will time it out and we won't know the reason.
	 * 1 minute (50% of 120 seconds) should be sufficient to get the
	 * status of sapdb using logical host. If the logical host is
	 * running, but this check times out, that means the system is
	 * really slow, then increasing the validate timeout will help to
	 * solve the timeout problem.
	 */
	timeout = timeout * TIMEOUT_PCT / 100;
	scds_syslog_debug(SCDS_DBG_HIGH, "run-db-cmd timeout =%d", timeout);

	rc = run_db_command(scds_handle, &sapdbxpropsp, timeout,
		db_state_cmd, B_TRUE);

	switch (rc) {
		case DB_ONLINE:
		case XSVR_DOWN:
		case XSVR_UP:
		case PARENT_DIE:
					/*
					 * this can happen if the DB was up
					 * then crashed before HA is brought up
					 * Failing it in Validate will not
					 * help. So let Start handle it since
					 * Start do db_clear before starting
					 * DB. That should clear the problem.
					 */
		case PPID_FILE_GONE:
					/*
					 * validate is being run on all
					 * potential master nodes. On the node
					 * DB is not online, the ppid file
					 * won't exist. So shouldn't check it
					 * however, probe will do that.
					 */
		case DB_TIMEOUT:
					/*
					 * this can happen if the logical host
					 * is not up at Validate time.
					 */
			rc = 0;
			break;

	default: /* Other errors, need to fail validate */
		rc = 1;
	}

	free(db_state_cmd);
	scds_syslog_debug(SCDS_DBG_HIGH, "in svc_validate rc = %d", rc);

finished:
	/* free memory allocated by get_sapdb_extension */
	free_sapdb_property(&sapdbxpropsp);

	/* Free up all the memory allocated by scds_initialize */
	scds_close(&scds_handle);

	/* Return the result of validate method */
	return (rc);
}
