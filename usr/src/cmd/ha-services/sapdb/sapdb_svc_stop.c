/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2003 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * sapdb_svc_stop.c - Stop method for SAPDB.
 */

#pragma ident	"@(#)sapdb_svc_stop.c	1.5	07/06/06 SMI"

#include "sapdb.h"

/*
 * Stops the SAPDB processes
 */

int
main(int argc, char *argv[])
{

	scds_handle_t	scds_handle;
	int		rc;
	sapdb_extprops_t	sapdbxpropsp;
	char			*rgname, *rsname;

	/* Process the arguments passed by RGM and initalize syslog */
	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR) {
		return (1);
	}

	rsname = (char *)scds_get_resource_name(scds_handle);
	rgname = (char *)scds_get_resource_group_name(scds_handle);

	if (get_sapdb_extension(scds_handle, &sapdbxpropsp, B_FALSE) != 0) {
		scds_close(&scds_handle);
		return (1);
	}

	/* retrieve uid and gid for db-user */
	if (retrieve_db_user_id(&sapdbxpropsp, B_FALSE) != 0) {
		scds_close(&scds_handle);
		return (1);
	}

	rc = svc_stop(scds_handle, &sapdbxpropsp);

	if (rc == SCHA_ERR_NOERR)
		(void) scha_resource_setstatus(rsname, rgname,
			SCHA_RSSTATUS_OFFLINE,
			"Successfully stopped SAPDB.");
	else
		(void) scha_resource_setstatus(rsname, rgname,
			SCHA_RSSTATUS_FAULTED,
			"Failed to stop SAPDB.");

	/* free memory allocated by get_sapdb_extension */
	free_sapdb_property(&sapdbxpropsp);

	/* Free up all the memory allocated by scds_initialize */
	scds_close(&scds_handle);

	/* Return the result of svc_stop method */
	return (rc);
}
