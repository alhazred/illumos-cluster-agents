#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# Check the status of SAP xserver and SAPDB instance.
#
#Input: dbmcli-cmd
#Input: user-key
#Input: pid-dir-path
#Input: db-name
#
#Output
#		0: xserver is up, SAPDB is running (ONLINE-7.4 WARM-7.3)
#		1: xserver is up
#		2: xserver is down
#		3: xserver is up, SAPDB not found (wrong NAME)
#		4: XUser not found for the specified user_key
#		5: parent kernel process terminated
#		6: parent kernel process file doesn't exist
#		7: no server rights for this command
#		20: dbmcli command failed (other err)
#		30: usage error
#
#pragma ident	"@(#)check_db_state.ksh	1.6	07/06/06 SMI"

GREP="/usr/bin/grep"
EGREP="/usr/bin/egrep"
PATH=$PATH:/usr/cluster/lib/sc
syslog_tag="SC[SUNW.sapdb,check_db_state]"

if [ $# = 4 ]; then
	CMD=$1
	USER_KEY=$2
	PID_DIR=$3
	DB_NAME=$4
else
	# SCMSGS
	# @explanation
	# An internal error has occurred.
	# @user_action
	# Save a copy of the /var/adm/messages files on all nodes. Contact
	# your authorized Sun service provider for assistance in diagnosing
	# the problem.
	scds_syslog -p error -t $syslog_tag -m \
		"INTERNAL ERROR: usage: `basename $0` <dbmcli-command> <User_Key> <Pid_Dir_Path> <DB_Name>"
	exit 30
fi

OUT=`$CMD -U $USER_KEY db_state` 
rc=$?

# Can't check for return code, since it can return non-zeron either the command
# itself failed or for some other reasons (eg, the xserver is not running).
# Will check whether the output of the command is blank. If the output is
# blank (command itself failed), then exit.
if [[ -z "$OUT" ]]; then
	scds_syslog -p error -t $syslog_tag -m \
		"Command %s failed to complete. Return code is %d." $CMD "$rc"
	exit 20
fi

# Command succeed, need to check the result based on what is returned
# from the output of the command.	
if [[ `echo $OUT|$GREP -i -c 'x_server not running'` -gt 0 ]]; then
	# SCMSGS
	# @explanation
	# An SAP command failed for the reason that is stated in the message.
	# @user_action
	# No action is required.
	scds_syslog -p error -t $syslog_tag -m \
		"Command failed: %s -U %s db_state: %s." $CMD $USER_KEY "$OUT"
	exit 2
elif [[ `echo $OUT|$GREP -i -c 'database or server not found'` -gt 0 ]]; then
	scds_syslog -p error -t $syslog_tag -m \
		"Command failed: %s -U %s db_state: %s." $CMD $USER_KEY "$OUT"
	exit 3
elif [[ `echo $OUT|$GREP -i -c 'XUser not found'` -gt 0 ]]; then
	scds_syslog -p error -t $syslog_tag -m \
		"Command failed: %s -U %s db_state: %s." $CMD $USER_KEY "$OUT"
	exit 4
elif [[ `echo $OUT|$EGREP -i -c '24937|ERR_MISSRIGHT'` -gt 0 ]]; then
	scds_syslog -p error -t $syslog_tag -m \
		"Command failed: %s -U %s db_state: %s." $CMD $USER_KEY "$OUT"
	exit 7
elif [[ `echo $OUT|$EGREP -i -c 'ONLINE|WARM'` -gt 0 ]]; then

# if DB is online, then check whether the ppid files exist, if it is,
# check ppid is running on the system or not.
# If the ppid is running, return 0. Otherwise, return error.

	if [ ! -f $PID_DIR/ppid/$DB_NAME ]; then
		exit 6
	else
		parent_id=`cat $PID_DIR/ppid/$DB_NAME`
		ps -ef | grep  $parent_id | grep -v grep > /dev/null
		rc=$?
		if [[ $rc -ne 0 ]]; then
			exit 5
		else
			exit 0
		fi
	fi
elif [[ `echo $OUT|$GREP -i -c 'OK'` -gt 0 ]]; then
	exit 1
fi

# dbmcli command failed
scds_syslog -p error -t $syslog_tag -m \
	"Command failed: %s -U %s db_state: %s." $CMD $USER_KEY "$OUT"
exit 20
