/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2003 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * sapdb_svc_start.c - Start method for SAPDB
 */

#pragma ident	"@(#)sapdb_svc_start.c	1.6	07/06/06 SMI"

#include "sapdb.h"

/*
 * The start method for SAPDB. Does some sanity checks on
 * the resource settings then starts SAPDB using sap utility dmbcli.
 * Check on SAPDB right after svc_start().
 */

int
main(int argc, char *argv[])
{
	scds_handle_t	scds_handle;
	int rc;
	sapdb_extprops_t	sapdbxpropsp;
	char			*rgname, *rsname;
	boolean_t		hsp_online_not_local;

	/*
	 * Process all the arguments that have been passed to us from RGM
	 * and do some initialization for syslog
	 */

	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR) {
		return (1);
	}

	rsname = (char *)scds_get_resource_name(scds_handle);
	rgname = (char *)scds_get_resource_group_name(scds_handle);

	if (get_sapdb_extension(scds_handle, &sapdbxpropsp, B_FALSE) != 0) {
		scds_close(&scds_handle);
		return (1);
	}

	/* retrieve uid and gid for db-user */
	if (retrieve_db_user_id(&sapdbxpropsp, B_FALSE) != 0) {
		scds_close(&scds_handle);
		return (1);
	}

	/* Validate the configuration and if there is an error return back */
	/*
	 * Since the start method only runs on the master node, so it'll
	 * always be online local. No need to check.
	 */
	rc = svc_validate(scds_handle, &sapdbxpropsp, B_FALSE,
		&hsp_online_not_local);
	if (rc != 0) {
		scds_syslog(LOG_ERR,
		    "Failed to validate configuration.");
		goto finished;
	}

	/* Start the data service, if it fails return with an error */
	rc = svc_start(scds_handle, &sapdbxpropsp);

	if (rc != 0) {
		goto finished;
	}

	/* Wait for the service to come to a final state */
	rc = svc_wait(scds_handle, &sapdbxpropsp);
	scds_syslog_debug(SCDS_DBG_HIGH,
		"Returned from svc_wait rc=%d.", rc);

finished:

	if (rc == SCHA_ERR_NOERR)
		(void) scha_resource_setstatus(rsname, rgname,
			SCHA_RSSTATUS_OK,
			"Successfully started SAPDB.");
	else
		(void) scha_resource_setstatus(rsname, rgname,
			SCHA_RSSTATUS_FAULTED,
			"Failed to start SAPDB.");

	/* free memory allocated by get_sapdb_extension */
	free_sapdb_property(&sapdbxpropsp);

	/* Free up the Environment resources that were allocated */
	scds_syslog_debug(SCDS_DBG_HIGH, "In START, done now.");
	scds_close(&scds_handle);
	return (rc);
}
