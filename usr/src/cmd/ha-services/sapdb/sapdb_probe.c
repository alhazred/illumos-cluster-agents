/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2003 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * sapdb_probe.c - Probe for SAPDB.
 */

#pragma ident	"@(#)sapdb_probe.c	1.4	07/06/06 SMI"

#include "sapdb.h"

int
main(int argc, char *argv[])
{
	scds_handle_t	scds_handle;
	int		probe_result, timeout, thorough_interval;
	hrtime_t	ht1, ht2;
	long		dt;
	sapdb_extprops_t	sapdbxpropsp;

	scds_syslog_debug(SCDS_DBG_HIGH, "In PROBE.");

	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR)
		return (1);

	if (get_sapdb_extension(scds_handle, &sapdbxpropsp, B_FALSE) != 0) {
		return (1);
	}

	/* retrieve uid and gid for db-user */
	if (retrieve_db_user_id(&sapdbxpropsp, B_FALSE) != 0) {
		return (1);
	}

	timeout = scds_get_ext_probe_timeout(scds_handle);
	thorough_interval = scds_get_rs_thorough_probe_interval(scds_handle);

	for (;;) {
		/*
		 * sleep for a duration of thorough_probe_interval between
		 *  successive probes.
		 */
		(void) scds_fm_sleep(scds_handle, thorough_interval);

		ht1 = gethrtime();
		probe_result = svc_probe(scds_handle, &sapdbxpropsp, timeout,
				B_TRUE);

		scds_syslog_debug(SCDS_DBG_HIGH, "In PROBE, probe-result=%d",
			probe_result);

		ht2 = gethrtime();
		dt = (long)((ht2 - ht1) / 1e6);

		/*
		 * Compute failure history and take action if needed
		 */
		(void) scds_fm_action(scds_handle, probe_result, dt);
	} 	/* Keep probing forever */
}
