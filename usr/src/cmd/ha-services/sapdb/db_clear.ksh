#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# Clean up the SAPDB database (in case it crashed), and remove pid files.
# This is called in Start method before starting up the DB. 
#
# Input: Independent_program_path
# 	 User_Name
#	 User_key
#	 Pid_Dir_Path
# Output: 0 if succeed
# 	  30 if usage
#	  1 if failed to remove ppid dir and files or
#	    if failed to remove pid dir and files
#	  $rc if db_clear command failed
#
#pragma ident	"@(#)db_clear.ksh	1.5	07/06/06 SMI"

PGREP=/usr/bin/pgrep
WC=/usr/bin/wc
XARGS=/usr/bin/xargs
PATH=$PATH:/usr/cluster/lib/sc
syslog_tag="SC[SUNW.sapdb,db_clear]"

if [ $# = 4 ]; then
	INDEP_PROG_PATH=$1
	DB_NAME=$2
	USER_KEY=$3
	PID_DIR=$4
else
	# SCMSGS
	# @explanation
	# An internal error has occurred.
	# @user_action
	# Save a copy of the /var/adm/messages files on all nodes. Contact
	# your authorized Sun service provider for assistance in diagnosing
	# the problem.
	scds_syslog -p error -t $syslog_tag -m \
		"INTERNAL ERROR: usage: `basename $0` <Independent_Program_Path> <DB_Name> <User_Key> <Pid_Dir_Path>"

	exit 30
fi

OUT=`$INDEP_PROG_PATH/bin/dbmcli -U $USER_KEY db_clear`
rc=$?

if [[ $rc -ne 0 ]]; then
	# SCMSGS
	# @explanation
	# The cleanup SAP command failed for the reason stated in the message.
	# The Sun Cluster software continues to try to start the SAPDB
	# database instance.
	# @user_action
	# Check the SAPDB log file for the possible cause of this fauilure.
	scds_syslog -p warning -t $syslog_tag -m \
		"Command failed: %s/bin/dbmcli -U %s db_clear: %s. Will continue to start up SAPDB database." $INDEP_PROG_PATH $USER_KEY "$OUT"
	exit $rc
fi

# remove all pid files
/bin/rm -f $PID_DIR/ppid/$DB_NAME
rc=$?

if [[ $rc -ne 0 ]]; then
	# SCMSGS
	# @explanation
	# An attempt to remove the directory and all the files in the
	# directory failed. This command is being executed as the user that is
	# specified by the extension property DB_User.
	# @user_action
	# No action is required.
	scds_syslog -p warning -t $syslog_tag -m \
		"Command failed: /bin/rm -f %s/ppid/%s" $PID_DIR $DB_NAME
	exit 1
fi

/bin/rm -f $PID_DIR/pid/$DB_NAME
rc=$?

if [[ $rc -ne 0 ]]; then
	# SCMSGS
	# @explanation
	# An attempt to remove the directory and all the files in the
	# directory failed. This command is being executed as the user that is
	# specified by the extension property DB_User.
	# @user_action
	# No action is required.
	scds_syslog -p warning -t $syslog_tag -m \
		"Command failed: /bin/rm -f %s/pid/%s" $PID_DIR $DB_NAME
	exit 1
fi

exit $rc
