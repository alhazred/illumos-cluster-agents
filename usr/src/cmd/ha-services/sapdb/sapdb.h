/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_SAPDB_H
#define	_SAPDB_H

#pragma ident	"@(#)sapdb.h	1.5	07/08/07 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <scha.h>
#include <sys/time.h>
#include <signal.h>
#include <rgm/libdsdev.h>
#include <errno.h>
#include <pwd.h>
#include <sys/wait.h>
#include <unistd.h>
#include <locale.h>
#include <libintl.h>
#include <sys/statvfs.h>

/* extension property */
typedef struct sapdb_extprops
{
	char		*ext_db_name;
	char		*ext_db_user;
	char		*ext_user_key;
	char		*ext_indep_prog_path;
	char		*ext_pid_dir;
	boolean_t	ext_restartdb;
	char		*ext_dbmcli_opt;
	char		*db_home_dir;	/* not a real ext property */
	uid_t		db_uid;		/* not a real ext property */
	gid_t		db_gid;		/* not a real ext property */
} sapdb_extprops_t;

/* executable scripts */
#define	CHECK_DB_STATE		"check_db_state"
#define	STOP_DB_SIGNAL		"db_kill"
#define	DBCLEAR			"db_clear"

/* Extension property */
#define	INDEPPROGPATH		"Independent_Program_Path"
#define	DBNAME			"DB_Name"
#define	DBUSER			"DB_User"
#define	DBUSERKEY		"User_Key"
#define	PIDDIR			"Pid_Dir_Path"
#define	RESTARTDB		"Restart_if_Parent_Terminated"
#define	DBMCLIOPTION		"dbmcli_Start_Option"

/* return code from check_db_state */
#define	DB_ONLINE		0
#define	XSVR_UP			1
#define	XSVR_DOWN		2
#define	DB_NOT_FOUND		3
#define	XUSER_NOT_FOUND		4
#define	PARENT_DIE		5
#define	PPID_FILE_GONE		6
#define	NO_RIGHT		7
#define	DBMCLI_FAILED		20
#define	USAGE_ERR		30

#define	SYS_ERR			31
#define	DB_TIMEOUT		32

#define	SCDS_DBG_HIGH		9

#define	XSVR_RT_TYPE		"SUNW.sap_xserver"
#define	XUSERFILE		".XUSER.62"

#define	APP_NAME		"SAPDB"

int svc_validate(scds_handle_t scds_handle, sapdb_extprops_t *sapdbxpropsp,
	boolean_t print_messages, boolean_t *hsp_online_not_local);

int svc_start(scds_handle_t scds_handle, sapdb_extprops_t *sapdbxpropsp);

int svc_stop(scds_handle_t scds_handle, sapdb_extprops_t *sapdbxpropsp);

int mon_start(scds_handle_t scds_handle);

int mon_stop(scds_handle_t scds_handle);

int svc_probe(scds_handle_t scds_handle, sapdb_extprops_t *sapdbxpropsp,
	int timeout, boolean_t print_msgs);

int svc_mon_check(scds_handle_t scds_handle, char *indep_prog_path);

/* call a sap command after setting uid/gid as sap user */
int
run_db_command(scds_handle_t scds_handle, sapdb_extprops_t *sapdbxpropsp,
	int timeout, char *db_cmd, boolean_t print_message);

/* retrieve sapdb status (w/ logical host) */
int
db_state(scds_handle_t scds_handle, sapdb_extprops_t *sapdbxpropsp,
	int timeout, boolean_t print_message);

/* retrieve sapdb status (w/o logical host) on local node */
int
db_state_local(scds_handle_t scds_handle, char *indep_prog_path,
	const char *rs_tag, int *timeout, boolean_t print_msgs);

int
get_sapdb_extension(scds_handle_t handle, sapdb_extprops_t *sapdbxpropsp,
	boolean_t print_messages);

int
get_one_sapdb_extension(scds_handle_t scds_handle, char *prop_name, char **prop,
	boolean_t print_messages);

int
svc_wait(scds_handle_t scds_handle, sapdb_extprops_t *sapdbxpropsp);

void free_sapdb_property(sapdb_extprops_t *sapdbxpropsp);

int
retrieve_db_user_id(sapdb_extprops_t *sapdbxpropsp, boolean_t print_message);

int validate_hasp(int print, scds_handle_t handle);

#ifdef __cplusplus
}
#endif

#endif	/* _SAPDB_H */
