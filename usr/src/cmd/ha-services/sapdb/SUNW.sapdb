#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#pragma	ident	"@(#)SUNW.sapdb	1.8	07/06/06 SMI"
#

RESOURCE_TYPE = "sapdb";
VENDOR_ID = SUNW;
RT_DESCRIPTION = "Sun Cluster HA for SAPDB";

RT_VERSION = "3.1";

API_VERSION = 2;
FAILOVER = TRUE;

RT_BASEDIR = /opt/SUNWscsapdb/bin;

START				=	sapdb_svc_start;
STOP				=	sapdb_svc_stop;

VALIDATE			=	sapdb_validate;
UPDATE				=	sapdb_update;

MONITOR_START			=	sapdb_monitor_start;
MONITOR_STOP			=	sapdb_monitor_stop;
MONITOR_CHECK			=	sapdb_monitor_check;

#for pkg
PKGLIST = SUNWscsapdb;

#for pkg upgrade
#% SERVICE_NAME = "sapdb"

#
#Upgrade directives
#
#$upgrade
#$upgrade_from "1" anytime


#The paramtable is a list of bracketed resource property declarations
#that come after the resource-type declarations
#The property-name declaration must be the first attribute
#after the open curly of a paramtable entry
#
#The following are the system defined properties. Each of the system defined
#properties have a default value set for each of the attributes. Look at
#man rt_reg(4) for a detailed explanation.
#
{
	PROPERTY = Start_timeout;
	MIN = 60;
	DEFAULT = 300;
}
{
	PROPERTY = Stop_timeout;
	MIN = 60;
	DEFAULT = 300;
}
{
	PROPERTY = Validate_timeout;
	MIN = 60;
	DEFAULT = 120;
}
{
	PROPERTY = Update_timeout;
	MIN = 60;
	DEFAULT = 120;
}
{
	PROPERTY = Monitor_Start_timeout;
	MIN = 60;
	DEFAULT = 120;
}
{
	PROPERTY = Monitor_Stop_timeout;
	MIN = 60;
	DEFAULT = 120;
}
{
	PROPERTY = Monitor_Check_timeout;
	MIN = 60;
	DEFAULT = 120;
}
{
	PROPERTY = FailOver_Mode;
	DEFAULT = SOFT;
	TUNABLE = ANYTIME;
}
{
	PROPERTY = Network_resources_used;
	TUNABLE = WHEN_DISABLED;
	DEFAULT = "";
}
{
	PROPERTY = Thorough_Probe_Interval;
	MAX = 3600;
	DEFAULT = 120;
	TUNABLE = ANYTIME;
}
{
	PROPERTY = Retry_Count;
	MAX = 10;
	DEFAULT = 2;
	TUNABLE = ANYTIME;
}
{
	PROPERTY = Retry_Interval;
	MAX = 3600;
	DEFAULT = 850;
	TUNABLE = ANYTIME;
}

#
#Extension Properties
#
#The following two extension properties control the restarting of the
#fault monitor itself (not the server daemon) by PMF.
{
	PROPERTY = Monitor_retry_count;
	EXTENSION;
	INT;
	DEFAULT = 4;
	TUNABLE = ANYTIME;
	DESCRIPTION = "Number of PMF restarts allowed for the fault monitor";
}

{
	PROPERTY = Monitor_retry_interval;
	EXTENSION;
	INT;
	DEFAULT = 2;
	TUNABLE = ANYTIME;
	DESCRIPTION = "Fault monitor restart interval (minutes)";
}

#This is an optional property, which determines whether to failover when
#Retry_count is exceeded during Retry_interval.
#
{
	PROPERTY = Failover_enabled;
	EXTENSION;
	BOOLEAN;
	DEFAULT = TRUE;
	TUNABLE = ANYTIME;
	DESCRIPTION = "Determines whether to failover when Retry_count is exceeded during Retry_interval";
}

#Time-out value for the probe
{
	PROPERTY = Probe_timeout;
	EXTENSION;
	INT;
	MIN = 30;
	DEFAULT = 90;
	TUNABLE = ANYTIME;
	DESCRIPTION = "Time out value for the probe (seconds)";
}

{
	PROPERTY = DB_Name;
	EXTENSION;
	STRING;
	TUNABLE = WHEN_DISABLED;
	DESCRIPTION = "Name of the SAPDB instance in upper case";
}

{
	PROPERTY = DB_User;
	EXTENSION;
	STRING;
	TUNABLE = WHEN_DISABLED;
	DESCRIPTION = "The user ID who administers the SAPDB instance";
}

{
	PROPERTY = User_Key;
	EXTENSION;
	STRING;
	TUNABLE = WHEN_DISABLED;
	DESCRIPTION = "The user_key defined for DB_Name for the DB_User that's using XUSER";
}

{
	PROPERTY = Independent_Program_Path;
	EXTENSION;
	STRING;
	DEFAULT = "/sapdb/programs";
	TUNABLE = WHEN_DISABLED;
	DESCRIPTION = "The full path to the directory that contains the programs and libraries for the client run-time environment that are independent of the database software version";
}

{
	PROPERTY = Pid_Dir_Path;
	EXTENSION;
	STRING;
	DEFAULT = "/var/spool/sql";
	TUNABLE = WHEN_DISABLED;
	DESCRIPTION = "The full path to the directory where the directories for different set of pid files will be created for the database instance";
}

{
	PROPERTY = Restart_if_Parent_Terminated;
	EXTENSION;
	BOOLEAN;
	DEFAULT = FALSE;
	TUNABLE = ANYTIME;
	DESCRIPTION = "Determines whether to restart the database if the parent kernel process is terminated";
}

{
	PROPERTY = dbmcli_Start_Option;
	EXTENSION;
	STRING;
	DEFAULT = "db_online";
	TUNABLE = WHEN_DISABLED;
	DESCRIPTION = "The option that is passed to the 'dbmcli' command to start up the SAPDB database instance";
}
