/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */


#pragma ident	"@(#)sapdb.c	1.9	08/07/28 SMI"

/*
 * sapdb.c - Common utilities for SAPDB.
 *
 * This utility has the methods for performing the validation, starting and
 * stopping the data service and the fault monitor. It also contains the
 * method to probe the health of the data service.  The probe just returns
 * either success or failure. Action is taken based on this returned value
 * in the method found in the file sapdb_probe.c
 *
 */

#include <limits.h>
#include <ds_common.h>
#include <grp.h>
#include "sapdb.h"

/*
 * Use a percentage of the stop_timeout to stop the application.
 */
#define	SVC_SMOOTH_PCT		80
#define	SVC_HARD_PCT		15

/*
 * The initial timeout allowed  for the SAPDB dataservice to
 * be fully up and running. We will wait for 1% (SVC_WAIT_PCT)
 * of the start_timeout time before probing the service.
 */
#define	SVC_WAIT_PCT		1

/*
 * Time interval (in second) to wait b/t probing the service
 * when service is first being started.
 */
#define	SVC_WAIT_TIME		5

/*
 * This function sets the gid/uid to <db-user>, set sap binary path, set
 * home path for <db-user>, then execute the command passed in by the caller
 * under scds_timerun().
 *
 * Return
 *	   exit_code from the sap command or
 *	   DB_TIMEOUT if command timed out or
 *	   SYS_ERR if some system error (system command failed).
 */
int
run_db_command(scds_handle_t scds_handle, sapdb_extprops_t *sapdbxpropsp,
	int timeout, char *db_cmd, boolean_t print_messages)
{
	int 	rc = 0, exit_code = 0;
	int	pid;
	int	wstat;
	int	cmdrtncode;
	char	*path_env, *sap_path;
	char	*home_path = NULL;


	scds_syslog_debug(SCDS_DBG_HIGH, "run-db-cmd timeout =%d", timeout);

	/* home string for user <db-user> */
	home_path = ds_string_format("HOME=%s", sapdbxpropsp->db_home_dir);

	if (home_path == NULL) {
		ds_internal_error("Failed to compose path string for "
			"HOME=<DB_User-home-dir>");
		return (SYS_ERR);
	}
	scds_syslog_debug(SCDS_DBG_HIGH, "home path = %s", home_path);

	if ((path_env = getenv("PATH")) != NULL) {
		/* path string including sapdb-independent-program-path */
		sap_path = ds_string_format("PATH=%s/bin:%s",
			sapdbxpropsp->ext_indep_prog_path, path_env);
	} else { /* ignore path_env if null */
		sap_path = ds_string_format("PATH=%s/bin",
			sapdbxpropsp->ext_indep_prog_path);
	}

	if (sap_path == NULL) {
		ds_internal_error("Failed to compose path string for "
			"PATH=<Independent_Program_Path>/bin:$PATH");
		return (SYS_ERR);
	}

	scds_syslog_debug(SCDS_DBG_HIGH, "sap-path = %s", sap_path);

	/* fork off a child process to do the work for setuid/setgid */
	if ((pid = fork1()) == -1) {
		/*
		 * SCMSGS
		 * @explanation
		 * The fork(2) system call failed because a system resource is
		 * exhausted.
		 * @user_action
		 * Install more memory, increase swap space, or reduce peak
		 * memory consumption.
		 */
		scds_syslog(LOG_ERR, "Couldn't fork1.");
		return (SYS_ERR);
	}

	if (pid == 0) { /* child */
		scds_syslog_debug(SCDS_DBG_HIGH, "in child...");

		if ((rc = putenv(sap_path)) != 0) {
			ds_internal_error("putenv: %s",
				strerror(errno)); /*lint !e746 */
			if (print_messages)
				(void) fprintf(stderr, gettext(
					"INTERNAL ERROR: putenv: %s.\n"),
						strerror(errno));
			exit(SYS_ERR);
		}

		if ((rc = putenv(home_path)) != 0) {
			ds_internal_error("putenv: %s", strerror(errno));
			if (print_messages)
				(void) fprintf(stderr, gettext(
					"INTERNAL ERROR: putenv: %s.\n"),
						strerror(errno));
			exit(SYS_ERR);
		}

		if ((rc = setgid(sapdbxpropsp->db_gid)) != 0) {
			ds_internal_error("setgid: %s", strerror(errno));
			if (print_messages)
				(void) fprintf(stderr, gettext(
					"INTERNAL ERROR: setgid: %s.\n"),
						strerror(errno));
			exit(SYS_ERR);
		}
		/*
		 * initialize the supplementary group access list
		 * this is required to allow the file/resource access
		 * of other groups by the service
		 */

		if (initgroups(sapdbxpropsp->ext_db_user,
				sapdbxpropsp->db_gid) == -1) {
			ds_internal_error("initgroups: %s", strerror(errno));
			if (print_messages)
				(void) fprintf(stderr, gettext(
					"INTERNAL ERROR: initgroups: %s.\n"),
					strerror(errno));
			exit(SYS_ERR);
		}

		if ((rc = setuid(sapdbxpropsp->db_uid)) != 0) {
			ds_internal_error("setuid: %s", strerror(errno));
			if (print_messages)
				(void) fprintf(stderr, gettext(
					"INTERNAL ERROR: setuid: %s.\n"),
						strerror(errno));
			exit(SYS_ERR);
		}

		/* execute the cmd under scds_timeout */
		rc = scds_timerun(scds_handle, db_cmd, timeout,
			SIGKILL, &exit_code);
		if (rc != 0) {
			if (rc == SCHA_ERR_TIMEOUT) {
				/*
				 * SCMSGS
				 * @explanation
				 * The command that is listed failed for the
				 * reason that is shown in the message. The
				 * failure might be caused by the logical host
				 * not being available.
				 * @user_action
				 * Ensure the logical host that is used for
				 * the SAPDB database instance is online.
				 */
				scds_syslog(LOG_WARNING, "Command '%s' failed: "
					"%s. The logical host of the database"
					" server might not be available. "
					"User_Key and related information "
					"cannot be verified.",
					db_cmd, scds_error_string(rc));
				if (print_messages)
					(void) fprintf(stderr, gettext(
					    "Command '%s' failed: %s. The "
					    "logical host of the database "
					    "server might not be "
					    "available. User_Key and related "
					    "information cannot be "
					    "verified.\n"),
					    db_cmd, scds_error_string(rc));
				rc = DB_TIMEOUT;
			} else {
				scds_syslog(LOG_ERR, "Command %s failed to "
					"complete. Return code is %d.",
					db_cmd, rc);
				if (print_messages)
					(void) fprintf(stderr, gettext(
						"Command %s failed to complete"
						". Return code is %d.\n"),
						db_cmd, rc);
			}
			exit(rc);
		} else {
			if (print_messages) { /* validate method */
				if ((exit_code == DB_NOT_FOUND) ||
					(exit_code == XUSER_NOT_FOUND) ||
					(exit_code == NO_RIGHT) ||
					(exit_code == DBMCLI_FAILED)) {
					/*
					 * SCMSGS
					 * @explanation
					 * The command provided in the message
					 * finished with some error. The
					 * reason for the error is listed in a
					 * seperate message which includes the
					 * same command.
					 * @user_action
					 * Refer to a seperate message which
					 * listed the same command for the
					 * reason the command failed.
					 */
					scds_syslog(LOG_ERR, "Command %s "
						"finished with error. Refer to"
						" an earlier message with the"
						" same command for details.",
						db_cmd);
				}
			}
			scds_syslog_debug(SCDS_DBG_HIGH,
				"db_state(child) %s finished w/ exit-code %d",
				db_cmd, exit_code);
			exit(exit_code);
		}
	}  /* end child */

	/* wait for scds_timerun to complete */
	do  {
		rc = waitpid(pid, &wstat, 0);
	} while (rc == -1 && errno == EINTR);

	cmdrtncode = WEXITSTATUS((uint_t)wstat);
	scds_syslog_debug(SCDS_DBG_HIGH,
		"In run_db_command, cmd return rc=%d", cmdrtncode);

	free(home_path);
	free(sap_path);
	return (cmdrtncode);
}


/* Retrieve a string extension property. */
int
get_string_ext_extension(scds_handle_t scds_handle, char *prop_name,
	char **prop,
	boolean_t print_messages)
{
	scha_extprop_value_t	*extprop = NULL;
	int			rc = 0;

	rc = scds_get_ext_property(scds_handle, prop_name,
		SCHA_PTYPE_STRING, &extprop);
	if (rc != SCHA_ERR_NOERR || extprop == NULL) {
		scds_syslog(LOG_ERR,
			"Failed to retrieve the property %s: %s.",
			prop_name, scds_error_string(rc));
		if (print_messages) {
			(void) fprintf(stderr, gettext("Failed to retrieve "
				"the property %s: %s.\n"), prop_name,
				scds_error_string(rc));
		}
		rc = 1;
	}

	if (extprop->val.val_str == NULL) {
		scds_syslog(LOG_INFO,
			"Extension property <%s> has a value of <%s>",
			prop_name, "NULL");
		*prop = NULL;
	}

	if ((*prop = strdup(extprop->val.val_str)) == NULL) {
		scds_syslog(LOG_ERR, "Out of memory.");
		if (print_messages) {
			(void) fprintf(stderr, gettext("Out of memory.\n"));
		}
		rc = 1;
	} else
		scds_syslog(LOG_INFO,
			"Extension property <%s> has a value of "
			"<%s>", prop_name, *prop);

	return (rc);
}

/*
 * This function validates the DB_User and also retrieve the uid and gid
 * of the user, save it in the sap structure.
 *
 * Return 0 if succeed, 1 otherwise.
 */
int
retrieve_db_user_id(sapdb_extprops_t *sapdbxpropsp, boolean_t print_messages)
{
	struct passwd 	*sapdb_usr_pwd = NULL;
	struct passwd	pwd;
	char		buf[MAXPATHLEN];

	/* set db-user home_dir, uid and gid */
	sapdb_usr_pwd = getpwnam_r(sapdbxpropsp->ext_db_user, &pwd, buf,
		MAXPATHLEN);
	if (sapdb_usr_pwd == NULL) {
		if (errno == ERANGE) {
			ds_internal_error("Data buffer for user %s "
				"is not big enough", sapdbxpropsp->ext_db_user);
			if (print_messages) {
				(void) fprintf(stderr,
					gettext("Data buffer for user %s "
						"is not big enough.\n"),
						sapdbxpropsp->ext_db_user);
			}
		} else {
			scds_syslog(LOG_ERR,
				"Failed to retrieve information for "
				"user %s.", sapdbxpropsp->ext_db_user);
			if (print_messages) {
				(void) fprintf(stderr,
					gettext("Failed to retrieve information"
					" for user %s.\n"),
					sapdbxpropsp->ext_db_user);
			}
		}
		return (1);
	} else if ((pwd.pw_dir == NULL) || (strlen(pwd.pw_dir) == 0)) {
		scds_syslog(LOG_ERR, "Home dir is not set for user %s.",
			sapdbxpropsp->ext_db_user);
		if (print_messages) {
			(void) fprintf(stderr, gettext("Home dir is not set"
				" for user %s.\n"),
				sapdbxpropsp->ext_db_user);
		}
		return (1);
	} else {
		if ((sapdbxpropsp->db_home_dir = strdup(pwd.pw_dir)) == NULL) {
			scds_syslog(LOG_ERR, "Out of memory.");
			if (print_messages) {
				(void) fprintf(stderr,
					gettext("Out of memory.\n"));
			}
			return (1);
		}
		sapdbxpropsp->db_uid = pwd.pw_uid;
		sapdbxpropsp->db_gid = pwd.pw_gid;
	}
	return (0);
}


/* Retrieve the SAP DB extension properties */
int
get_sapdb_extension(scds_handle_t scds_handle, sapdb_extprops_t *sapdbxpropsp,
	boolean_t print_messages)
{

	int				rc;
	scds_net_resource_list_t	*snrlp = NULL;
	scha_extprop_value_t		*extprop = NULL;

	if (sapdbxpropsp == NULL) {
		ds_internal_error("Extension property structure was NULL");

		if (print_messages)
			(void) fprintf(stderr, gettext("INTERNAL ERROR: "
				"Extension property structure was NULL.\n"));
		return (1);
	}

	/* retrieve  Independent_program_path */
	if ((rc = get_string_ext_extension(scds_handle, INDEPPROGPATH,
		&(sapdbxpropsp->ext_indep_prog_path), print_messages)) != 0) {
		return (1);
	}

	/* retrieve  user_key */
	if ((rc = get_string_ext_extension(scds_handle, DBUSERKEY,
		&(sapdbxpropsp->ext_user_key), print_messages)) != 0) {
		return (1);
	}

	/* retrive DBNAME */
	if ((rc = get_string_ext_extension(scds_handle, DBNAME,
		&(sapdbxpropsp->ext_db_name), print_messages)) != 0) {
		return (1);
	}

	/* retrieve DB_User */
	if ((rc = get_string_ext_extension(scds_handle, DBUSER,
		&(sapdbxpropsp->ext_db_user), print_messages)) != 0) {
		return (1);
	}

	/* retrieve Pid_Dir_Path */
	if ((rc = get_string_ext_extension(scds_handle, PIDDIR,
		&(sapdbxpropsp->ext_pid_dir), print_messages)) != 0) {
		return (1);
	}

	/* retrieve Restart_If_Parent_Terminated */
	if ((rc = scds_get_ext_property(scds_handle, RESTARTDB,
		SCHA_PTYPE_BOOLEAN, &extprop)) != 0) {
		return (1);
	}
	sapdbxpropsp->ext_restartdb = extprop->val.val_boolean;

	/* retrieve dbmcli_option_for_start */
	if ((rc = get_string_ext_extension(scds_handle, DBMCLIOPTION,
		&(sapdbxpropsp->ext_dbmcli_opt), print_messages)) != 0) {
		return (1);
	}

	scds_syslog_debug(SCDS_DBG_HIGH, "before scds_get_rs_hostnames()");

	/*
	 * Return an error if there is an error when trying to get the
	 * available network address resources for this resource
	 */
	if ((rc = scds_get_rs_hostnames(scds_handle, &snrlp))
		!= SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		    "Error in trying to access the configured network "
		    "resources : %s.", scds_error_string(rc));
		if (print_messages) {
			(void) fprintf(stderr, gettext("Error in trying to "
				"access the configured network resources : "
				"%s.\n"), scds_error_string(rc));
		}
		return (1);
	}

	scds_syslog_debug(SCDS_DBG_HIGH, "after scds_get_rs_hostnames");

	/* Return an error if there are no network address resources */
	if (snrlp == NULL || snrlp->num_netresources == 0) {
		scds_syslog(LOG_ERR,
		    "No network address resource in resource group.");
		if (print_messages) {
			(void) fprintf(stderr, gettext("No network address "
				" resource in resource group.\n"));
		}
		return (1);
	}

	scds_syslog_debug(SCDS_DBG_HIGH, "snrlp != null");
	scds_syslog_debug(SCDS_DBG_HIGH, "lh-host=%s",
		snrlp->netresources->hostnames[0]);

	return (0);
}



/*
 * This function checks whether sap xserver is running on the local node
 * by calling a sap command (doesn't use the logical host name):
 *	dbmcli db_enum
 * Returns 0 if xserver is running, 1 otherwise.
 *	It also returns the timeout (validate timeout) for convinent purpose
 *		for the Validate method.
 */
int
db_state_local(scds_handle_t scds_handle, char *indep_prog_path,
	const char *rs_tag, int *timeout, boolean_t print_msgs)
{
	int			rc = 0, local_timeout, tag = 0;
	scha_resource_t		local_handle;
	char			*probe_cmd = NULL;
	int			exit_code = 0;

	if (strcasecmp(rs_tag, SCHA_MONITOR_CHECK_TIMEOUT) == 0) {
		tag = 1;
	} else if (strcasecmp(rs_tag, SCHA_VALIDATE_TIMEOUT) == 0) {
		tag = 2;
	} else {
		ds_internal_error("Tag is not SCHA_MONITOR_CHECK_TIMEOUT or "
			"SCHA_VALIDATE_TIMEOUT");
		return (1);
	}

	scds_syslog_debug(SCDS_DBG_HIGH, "tag = %d", tag);

retry_open:
	rc = scha_resource_open(scds_get_resource_name(scds_handle),
		scds_get_resource_group_name(scds_handle), &local_handle);

	if (rc != SCHA_ERR_NOERR) {
		if (rc == SCHA_ERR_RSRC) { /* resource not created yet */
			scds_syslog_debug(SCDS_DBG_HIGH, "rs not created yet");
			local_timeout = 120;
		} else {
			scds_syslog(LOG_ERR,
				"Failed to retrieve the resource handle: %s.",
				scds_error_string(rc));
			return (1);
		}
	} else {
		switch (tag) {
		    case 1:
			rc = scha_resource_get(local_handle,
				SCHA_MONITOR_CHECK_TIMEOUT, &local_timeout);
			break;
		    case 2:
			rc = scha_resource_get(local_handle,
				SCHA_VALIDATE_TIMEOUT, &local_timeout);
			break;
		    default:
			ds_internal_error("Tag is not SCHA_MONITOR_CHECK_"
				"TIMEOUT or SCHA_VALIDATE_TIMEOUT");
			return (1);
		}

		if (rc == SCHA_ERR_SEQID) {
			scds_syslog(LOG_INFO, "Retrying to retrieve the "
				"resource information: %s.",
				scds_error_string(rc));
			(void) scha_resource_close(local_handle);
			local_handle = NULL;
			(void) sleep(1);
			goto retry_open;
		} else if (rc != SCHA_ERR_NOERR) {
			scds_syslog(LOG_ERR, "Failed to retrieve the resource "
				"property %s: %s.", rs_tag,
				scds_error_string(rc));
			(void) scha_resource_close(local_handle);
			return (1);
		}
	}

	scds_syslog_debug(SCDS_DBG_HIGH, "local-timeout=%d", local_timeout);

	/* check whether xserver is running or not */
	probe_cmd = ds_string_format(
		"%s/bin/dbmcli db_enum >/dev/null 2>&1", indep_prog_path);

	if (probe_cmd == NULL) {
		ds_internal_error(
			"Failed to form command /usr/bin/sh -c "
			"%s/bin/dbmcli db_enum", indep_prog_path);
		return (1);
	} else
		scds_syslog_debug(SCDS_DBG_HIGH, "probe-cmd=%s", probe_cmd);

	rc = scds_timerun(scds_handle, probe_cmd, local_timeout, SIGKILL,
		&exit_code);

	if (rc != 0) {
		if (rc == SCHA_ERR_TIMEOUT) {
			scds_syslog(LOG_ERR, "Probing SAP xserver timed out "
				"with command %s.", probe_cmd);
			if (print_msgs)
				(void) fprintf(stderr, gettext("Probing SAP "
					"xserver timed out with command %s."),
					probe_cmd);
		}
		if (rc == SCHA_ERR_NOMEM) {
			scds_syslog(LOG_ERR, "Out of memory.");
			if (print_msgs)
				(void) fprintf(stderr, gettext("Out of "
					"memory."));
		}
		/* system error */
		return (1);
	}

	scds_syslog_debug(SCDS_DBG_HIGH, "dbmcli db_enum rc=%d, exit-code=%d",
		rc, exit_code);

	if (exit_code != 0) {
		/* xserver is not running or other error while checking */
		scds_syslog(LOG_WARNING, "SAP xserver is not available.");
		if (print_msgs)
			(void) fprintf(stderr, gettext("SAP xserver is not"
				"available."));
		(void) scha_resource_close(local_handle);
		return (1);
	}

	/* SAP x-server is up and running */
	*timeout = local_timeout;
	(void) scha_resource_close(local_handle);
	free(probe_cmd);
	return (0);
}


/*
 * svc_validate():
 *
 * Verify the followings:
 * 1) HAStoragePlus check
 * 2) SAPDB executable dbmcli is available
 * 3) validate DB_User
 * 4) check XUSER file is accessible by DB_User
 * 5) network address is already checked in get_sapdb_extension()
 * 6) dbmcli_startup_option
 * If hsp is online not local, shouldn't check on xserver later on.
 */
int
svc_validate(scds_handle_t scds_handle, sapdb_extprops_t *sapdbxpropsp,
	boolean_t print_messages, boolean_t *hsp_online_not_local)
{
	int 		rc = 0;
	int		hasprc;
	char		*xuserfile = NULL;
	struct 		statvfs	sbuf;

	*hsp_online_not_local = B_FALSE;

	/* check for HAStoragePlus resources */
	hasprc = validate_hasp(print_messages, scds_handle);

	if (hasprc == -1) {
		/* error occured in scds_hasp_check() */
		rc = 1;
		goto finished;
	} else if (hasprc == 0) {
		/* online but not on local node, exit */
		*hsp_online_not_local = B_TRUE;
		rc = 0;
		goto finished;
	}

	/* if hasprc=1 that means online local, check all */

	/* verify DB_User and retrieve uid and gid for DB_User */
	rc = retrieve_db_user_id(sapdbxpropsp, B_TRUE);

	if (rc != SCHA_ERR_NOERR) {
		rc = 1;
		goto finished;
	}

	/* independent_prog_path/bin/dbmcli is owner read and executable */
	rc = ds_validate_file(print_messages, S_IXUSR | S_IRUSR,
		"%s/bin/dbmcli", sapdbxpropsp->ext_indep_prog_path);

	if (rc != SCHA_ERR_NOERR) {
		rc = 1;
		goto finished;
	}

	/* xuser file is owner read and write */
	if ((xuserfile = ds_string_format("%s/%s", sapdbxpropsp->db_home_dir,
				XUSERFILE)) == NULL) {
		ds_internal_error("Failed to create XUSER string");
		rc = 1;
		goto finished;
	}

	rc = ds_validate_file(print_messages, S_IRUSR | S_IWUSR,
		"%s", xuserfile);

	if (rc != SCHA_ERR_NOERR) {
		rc = 1;
		goto finished;
	}

	/* xuser file is owned by the right user and group */
	rc = ds_validate_file_owner(print_messages, sapdbxpropsp->db_uid,
		sapdbxpropsp->db_gid, "%s", xuserfile);

	if (rc != SCHA_ERR_NOERR) {
		rc = 1;
		goto finished;
	}

	/* check Pid_Dir_Path is absolute path and exists */
	if (sapdbxpropsp->ext_pid_dir[0] != '/') {
		scds_syslog(LOG_ERR, "%s is not an absolute path.",
			sapdbxpropsp->ext_pid_dir);
		if (print_messages) {
			(void) fprintf(stderr, gettext("%s is not an absolute "
				"path.\n"), sapdbxpropsp->ext_pid_dir);
		}
		return (1);
	}

	/*
	 * Make sure Pid_Dir_Path exist. access() may return cached values.
	 * Using w/ statvfs() guarantees permission and status.
	 */
	if (access(sapdbxpropsp->ext_pid_dir, F_OK) ||
		statvfs(sapdbxpropsp->ext_pid_dir, &sbuf)) {
		int	err = errno;
		/*
		 * SCMSGS
		 * @explanation
		 * The path which is listed in the message is not readable.
		 * The reason for the failure is also listed in the message.
		 * @user_action
		 * Make sure the path which is listed exists. Use the
		 * HAStoragePlus resource in the same resource group of the
		 * HA-SAPDB resource. So that the HA-SAPDB method have access
		 * to the file system at the time when they are launched.
		 */
		scds_syslog(LOG_ERR, "Pid_Dir_Path %s is not readable: %s.",
			sapdbxpropsp->ext_pid_dir, strerror(err));
		if (print_messages) {
			(void) fprintf(stderr, gettext("Pid_Dir_Path %s is "
				"not readable: %s.\n"),
				sapdbxpropsp->ext_pid_dir, strerror(err));
		}
		return (1);
	}

	/* startup_option is db_warm (7.3) or db_online (7.4) */
	if ((strcmp("db_warm", sapdbxpropsp->ext_dbmcli_opt) == 0) ||
		(strcmp("db_online", sapdbxpropsp->ext_dbmcli_opt) == 0)) {
		scds_syslog_debug(SCDS_DBG_HIGH, "dbmcli start option ok.");
	} else {
		/*
		 * SCMSGS
		 * @explanation
		 * The extension property in the message is set to an invalid
		 * value.
		 * @user_action
		 * Change the value of the extension property to a valid
		 * value.
		 */
		scds_syslog(LOG_WARNING, "dbmcli option (%s) is not "
			"'db_warm' nor 'db_online'.", DBMCLIOPTION);
		if (print_messages)
			(void) fprintf(stderr, gettext("dbmcli option (%s) is"
				" not 'db_warm' nor 'db_online'.\n"),
				DBMCLIOPTION);
	}


finished:
	free(xuserfile);
	return (rc); /* return result of validation */
}



/*
 * Check if the host where this program is executing is healthy.
 * Make sure sap xserver is running.
 */
int
svc_mon_check(scds_handle_t scds_handle, char *indep_prog_path)
{
	int	rc = 0, timeout = 0;

	rc = db_state_local(scds_handle, indep_prog_path,
		SCHA_MONITOR_CHECK_TIMEOUT, &timeout, B_FALSE);
	return (rc);
}



/*
 * svc_start():
 * Return 0 if SAPDB is online already or started up.
 *	  1 if otherwise.
 */
int
svc_start(scds_handle_t scds_handle, sapdb_extprops_t *sapdbxpropsp)
{
	char	*db_cleanup_cmd = NULL,
		*db_start_cmd = NULL,
		*db_state_cmd = NULL,
		*rm_ipcrefs_cmd = NULL;
	int	rc = 0, exit_code = 0, start_timeout;

	/* Get Start timeout value */
	start_timeout = scds_get_rs_start_timeout(scds_handle);

	/* check the DB state first */
	db_state_cmd = ds_string_format(
		"%s/%s %s/bin/dbmcli %s %s %s>/dev/null",
		scds_get_rt_rt_basedir(scds_handle),
		CHECK_DB_STATE, sapdbxpropsp->ext_indep_prog_path,
		sapdbxpropsp->ext_user_key,
		sapdbxpropsp->ext_pid_dir,
		sapdbxpropsp->ext_db_name);

	if (db_state_cmd == NULL) {
		ds_internal_error("Failed to create command string to "
			"check SAP DB state");
		rc = 1;
		goto finished;
	}

	/*
	 * SCMSGS
	 * @explanation
	 * Checking the state of the SAPDB database instance with the command
	 * which is listed.
	 * @user_action
	 * Informational message. No action is needed.
	 */
	scds_syslog(LOG_INFO, "Check SAPDB state with command %s.",
		db_state_cmd);

	/* Adjust timeout to be 15% of start timeout */
	rc = run_db_command(scds_handle, sapdbxpropsp,
		(15 * start_timeout/100),
		db_state_cmd, B_FALSE);

	if (rc == DB_ONLINE) {
		/* SAPDB is online already */
		/*
		 * SCMSGS
		 * @explanation
		 * Informational message. The application in the message is
		 * already running on the cluster.
		 * @user_action
		 * No action is required.
		 */
		scds_syslog(LOG_INFO, "%s is already online.", APP_NAME);
		rc = 0;
		goto finished;
	} else if (rc == XSVR_UP) {
		/*
		 * SCMSGS
		 * @explanation
		 * Informational message. The SAP xserver is running.
		 * Therefore the SAPDB database instance will be started by
		 * the Sun Cluster software.
		 * @user_action
		 * No action is required.
		 */
		scds_syslog(LOG_INFO, "SAP xserver is running, will "
			"start SAPDB database now.");
	} else { /* error */
		scds_syslog_debug(SCDS_DBG_HIGH, "db_state rc = %d", rc);
		rc = 1;
		goto finished;
	}

	/*
	 * If it gets here, SAP xserver is up, SAPDB is not online but
	 * has no other db releated error from db_state.
	 *
	 * Before starting up SAPDB, need to run dbmcli db_clear,
	 * and remove all pid files.
	 */
	db_cleanup_cmd = ds_string_format("%s/%s %s %s %s %s",
	    scds_get_rt_rt_basedir(scds_handle), DBCLEAR,
	    sapdbxpropsp->ext_indep_prog_path,
	    sapdbxpropsp->ext_db_name, sapdbxpropsp->ext_user_key,
	    sapdbxpropsp->ext_pid_dir);

	if (db_cleanup_cmd == NULL) {
		ds_internal_error("Failed to form the command to clean up");
		rc = 1;
		goto finished;
	}

	/*
	 * if db_clear failed, not sure what to do, will still go ahead for
	 * best effort.
	 * Appropriate msg has been logged in db_clear script.
	 */
	/*
	 * SCMSGS
	 * @explanation
	 * Cleanup the SAPDB database instance before starting up the instance
	 * in case the database crashed and was in a bad state with the
	 * command which is listed.
	 * @user_action
	 * Informational message. No action is needed.
	 */
	scds_syslog(LOG_INFO, "Cleanup with command %s.", db_cleanup_cmd);

	/* Adjust timeout to be 15% of start timeout */
	(void) run_db_command(scds_handle, sapdbxpropsp,
		(15 * start_timeout/100),
		db_cleanup_cmd, B_FALSE);

	/* cleanup directories which holds files named with Semaphores */
	rm_ipcrefs_cmd = ds_string_format(
		"/bin/rm -rf /usr/spool/sql/ipc/*:%s > /dev/null 2>&1",
		sapdbxpropsp->ext_db_name);

	if (rm_ipcrefs_cmd == NULL) {
		ds_internal_error("Failed to form command to remove "
			"the IPC directory.");
		rc = 1;
		goto finished;
	}

	scds_syslog_debug(SCDS_DBG_HIGH, "remove semaphore files with %s",
				rm_ipcrefs_cmd);
	/* Adjust timeout to be 5% of start timeout */
	rc = scds_timerun(scds_handle, rm_ipcrefs_cmd,
		(5 * start_timeout/100),
		SIGKILL, &exit_code);

	if (rc != SCHA_ERR_NOERR) {
		if (rc == SCHA_ERR_TIMEOUT) {
			/*
			 * SCMSGS
			 * @explanation
			 * The listed command failed to complete. HA-SAPDB
			 * will continue to start up SAPDB.
			 * @user_action
			 * Informational message. No action is needed.
			 */
			scds_syslog(LOG_INFO, "Command %s timed out. Will "
				"continue to start up %s.", rm_ipcrefs_cmd,
				APP_NAME);
		} else {
			/*
			 * SCMSGS
			 * @explanation
			 * The listed command failed to complete. HA-SAPDB
			 * will continue to start up SAPDB.
			 * @user_action
			 * Informational message. No action is needed.
			 */
			scds_syslog(LOG_INFO, "Failed to complete command %s. "
				"Will continue to start up %s.", rm_ipcrefs_cmd,
				APP_NAME);
		}
	} else if (exit_code != 0) {
		scds_syslog(LOG_INFO, "Command %s failed to complete. Return "
			"code is %d.", rm_ipcrefs_cmd, exit_code);
	}

	/* start SAPDB now */
	db_start_cmd = ds_string_format(
		"%s/bin/dbmcli -U %s %s>/dev/null 2>&1",
		sapdbxpropsp->ext_indep_prog_path,
		sapdbxpropsp->ext_user_key,
		sapdbxpropsp->ext_dbmcli_opt);

	if (db_start_cmd == NULL) {
		ds_internal_error("Failed to form command to start "
			"the database with dbmcli %s",
			sapdbxpropsp->ext_dbmcli_opt);
		rc = 1;
		goto finished;
	}

	scds_syslog(LOG_NOTICE, "Starting %s with command %s.", APP_NAME,
	    db_start_cmd);

	rc = run_db_command(scds_handle, sapdbxpropsp, start_timeout,
		db_start_cmd, B_FALSE);

	scds_syslog_debug(SCDS_DBG_HIGH, "Started with rc=%d.", rc);
finished:
	if (db_cleanup_cmd)
		free(db_cleanup_cmd);
	if (db_start_cmd)
		free(db_start_cmd);
	if (db_state_cmd)
		free(db_state_cmd);
	if (rm_ipcrefs_cmd)
		free(rm_ipcrefs_cmd);
	return (rc);
}


/*
 * svc_wait():
 *
 * Waits for the data service is at a final state (either it's started up
 * fully or user stopped it outside of Sun Cluster).
 */
int
svc_wait(scds_handle_t scds_handle, sapdb_extprops_t *sapdbxpropsp)
{
	int	start_timeout, rc;

	start_timeout = scds_get_rs_start_timeout(scds_handle);

	/*
	 * PMF is not used to start the service, scds_svc_wait is the same
	 * as sleep.
	 */
	if (scds_svc_wait(scds_handle, (start_timeout * SVC_WAIT_PCT)/100) !=
		SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to start the service %s.",
			APP_NAME);
		return (1);
	}

	for (;;) {
		rc = svc_probe(scds_handle, sapdbxpropsp, start_timeout,
			B_FALSE);
		if (rc == 0) {
			scds_syslog(LOG_NOTICE, "Service is online.");
			return (0);
		}
		scds_syslog(LOG_NOTICE, "Waiting for %s to come up.",
		    APP_NAME);
		(void) scds_svc_wait(scds_handle, SVC_WAIT_TIME);
	/* We rely on RGM to timeout and terminate the program */
	}
}


/*
 * Check on:
 * 	SAPDB database instance status.
 *
 * Return:
 *	0	(SAPDB is running)
 *	SCDS_PROBE_COMPLETE_FAILURE (if xserver is up but SAPDB not up)
 *	SCDS_PROBE_COMPLETE_FAILURE/2  (if xserver not running)
 *	SCDS_PROBE_COMPLETE_FAILURE/4 (system error)
 *
 * Will not call scha_resource_setstatus except for PARENT_DIE. Because
 * DSDL will reset the status/message if return code from probe is non-zero.
 */
int
svc_probe(scds_handle_t handle, sapdb_extprops_t *sapdbxpropsp, int timeout,
boolean_t print_msgs)
{
	int		db_rc = 0;
	static int	old_db_state = -1;
	char		*db_state_cmd = NULL;

	scds_syslog_debug(SCDS_DBG_HIGH, "In svc_probe");

	/* db status check */
	db_state_cmd = ds_string_format(
		"%s/%s %s/bin/dbmcli %s %s %s>/dev/null",
		scds_get_rt_rt_basedir(handle),
		CHECK_DB_STATE, sapdbxpropsp->ext_indep_prog_path,
		sapdbxpropsp->ext_user_key,
		sapdbxpropsp->ext_pid_dir,
		sapdbxpropsp->ext_db_name);

	if (db_state_cmd == NULL) {
		ds_internal_error("Failed to create command string to "
			"check SAP DB state");
		return (1);
	}

	db_rc = run_db_command(handle, sapdbxpropsp, timeout,
		db_state_cmd, B_FALSE);

	scds_syslog_debug(SCDS_DBG_HIGH,
		"In probe,db_state returns %d", db_rc);

	switch (db_rc) {
	    case DB_ONLINE:
		free(db_state_cmd);
		scds_syslog_debug(SCDS_DBG_HIGH,
			"DB is online. old_db_state=%d, db_rc=%d",
			old_db_state, db_rc);
		if (db_rc != old_db_state) {
			/*
			 * Need to setstatus otherwise if PARENT_DIE or
			 * PPID_FILE_GONE before, then DB comes back
			 * online later, the status msg won't be reset
			 * by DSDL (due to no status change - 0 for both
			 * cases).
			 * PARENT_DIE then DB becomes online is
			 * not really likely to happen, so this is for
			 * completeness, in case someone manually mv the
			 * ppid file to something else, then discover that
			 * caused problem, then mv it back.
			 */
			(void) scha_resource_setstatus(
				scds_get_resource_name(handle),
				scds_get_resource_group_name(handle),
				SCHA_RSSTATUS_OK, "Completed successfully.");
			old_db_state = db_rc;
		}
		return (0);

	    case PARENT_DIE:
		free(db_state_cmd);
		/* SAPDB parent died */
		scds_syslog_debug(SCDS_DBG_HIGH,
			"kernel ppid died. old_db_state=%d, db_rc=%d",
			old_db_state, db_rc);
		if (db_rc != old_db_state) {
			/*
			 * SCMSGS
			 * @explanation
			 * The SAPDB parent kernel process was not running on
			 * the system.
			 * @user_action
			 * During normal operation, this error should not
			 * occurred, unless the process was terminated
			 * manually, or the SAPDB kernel process was
			 * terminated due to SAPDB kernel problem. Consult the
			 * SAPDB log file for additional information to
			 * determine whether the process was terminated
			 * abnormally.
			 */
			scds_syslog(LOG_WARNING,
				"SAPDB parent kernel process "
				"was terminated.");
			(void) scha_resource_setstatus(
				scds_get_resource_name(handle),
				scds_get_resource_group_name(handle),
				SCHA_RSSTATUS_DEGRADED,
				"SAPDB parent kernel process "
				"was terminated.");
			old_db_state = db_rc;
		}

		if (sapdbxpropsp->ext_restartdb == B_TRUE) {
			return (SCDS_PROBE_COMPLETE_FAILURE);
		} else
			return (0);

	    case PPID_FILE_GONE:
		free(db_state_cmd);
		/* SAPDB parent pid file doesn't exist */
		scds_syslog_debug(SCDS_DBG_HIGH,
			"kernel ppid file gone. old_db_state=%d"
			"db_rc=%d", old_db_state, db_rc);
		if (db_rc != old_db_state) {
			/*
			 * SCMSGS
			 * @explanation
			 * The SAPDB parent kernel process file cannot be
			 * found on the system.
			 * @user_action
			 * During normal operation, this error should not
			 * occurred, unless the file was deleted manually. No
			 * action required.
			 */
			scds_syslog(LOG_WARNING,
				"SAPDB parent kernel process "
				"file does not exist.");
			(void) scha_resource_setstatus(
				scds_get_resource_name(handle),
				scds_get_resource_group_name(handle),
				SCHA_RSSTATUS_DEGRADED,
				"SAPDB parent kernel process "
				"file does not exist.");
			old_db_state = db_rc;
		}

		if (sapdbxpropsp->ext_restartdb == B_TRUE) {
			return (SCDS_PROBE_COMPLETE_FAILURE);
		} else
			return (0);

	    case XSVR_DOWN:
		free(db_state_cmd);
		if (db_rc != old_db_state)
			old_db_state = db_rc;
		return (SCDS_PROBE_COMPLETE_FAILURE/2);

	    case XSVR_UP:
		free(db_state_cmd);
		if (db_rc != old_db_state)
			old_db_state = db_rc;
		if (print_msgs)
			/*
			 * SCMSGS
			 * @explanation
			 * SAPDB database instance is not available. The
			 * HA-SAPDB will restart it locally or fail over it to
			 * another available cluster node. Messages in the
			 * SAPDB log might provide more information regarding
			 * the failure.
			 * @user_action
			 * Informational message. No action is needed.
			 */
			scds_syslog(LOG_ERR, "SAPDB is down.");
		return (SCDS_PROBE_COMPLETE_FAILURE);

	    case SYS_ERR:
		free(db_state_cmd);
		return (SCDS_PROBE_COMPLETE_FAILURE/4);

	    default: /* other sapdb errors */
		scds_syslog(LOG_ERR, "Command %s finished with error. Refer to "
			"an earlier message with the same command for details.",
			db_state_cmd);
		free(db_state_cmd);
		return (SCDS_PROBE_COMPLETE_FAILURE/4);
	}
}



/*
 * svc_stop():
 *
 * Stop the SAPDB database.
 * Return 0 on success, > 0 on failures.
 *
 * First stop with dbmcli db_offline using smooth-stop-timeout.
 * If failed, stop with dbmcli db_stop using hard-stop-timeout.
 * If failed, send signal to all sapdb kernel processes.
 */
int
svc_stop(scds_handle_t scds_handle, sapdb_extprops_t *sapdbxpropsp)
{
	int 	rc = 0;
	char 	*soft_stop_cmd = NULL,
		*hard_stop_cmd = NULL,
		*kill_cmd = NULL;
	int 	stop_smooth_timeout = 0,
		stop_hard_timeout = 0,
		stop_timeout = 0;


	stop_timeout = scds_get_rs_stop_timeout(scds_handle);
	stop_smooth_timeout = (stop_timeout * SVC_SMOOTH_PCT) / 100;
	stop_hard_timeout = (stop_timeout * SVC_HARD_PCT) / 100;

	soft_stop_cmd = ds_string_format(
		"%s/bin/dbmcli -U %s db_offline>/dev/null 2>&1",
		sapdbxpropsp->ext_indep_prog_path,
		sapdbxpropsp->ext_user_key);
	if (soft_stop_cmd == NULL) {
		ds_internal_error(
			"Failed to form the command dbmcli -U<user-key> "
			"db_offline");
		return (1);
	}

	scds_syslog(LOG_NOTICE, "Stopping %s with command %s.", APP_NAME,
		soft_stop_cmd);
	rc = run_db_command(scds_handle, sapdbxpropsp, stop_smooth_timeout,
		soft_stop_cmd, B_FALSE);

	scds_syslog_debug(SCDS_DBG_HIGH, "soft stop = %s, rc=%d",
		soft_stop_cmd, rc);

	if (rc != SCHA_ERR_NOERR) {
		if (rc == SCHA_ERR_TIMEOUT) {
			/*
			 * SCMSGS
			 * @explanation
			 * An attempt to stop the application by the command
			 * that is listed was timed out.
			 * @user_action
			 * Other syslog messages that occur shortly before
			 * this message might indicate the reason for the
			 * failure. A more severe command will be used to stop
			 * the application after the command in the message
			 * was timed out. No user action is needed.
			 */
			scds_syslog(LOG_ERR, "Stopping %s timed out with "
					"command %s.", APP_NAME, soft_stop_cmd);
		} else
			/*
			 * SCMSGS
			 * @explanation
			 * An attempt to stop the application by the command
			 * that is listed failed. Informational message.
			 * @user_action
			 * Other syslog messages that occur shortly before
			 * this message might indicate the reason for the
			 * failure. Check the application installation and
			 * make sure the command listed in the message can be
			 * executed successfully outside Sun Cluster. A more
			 * severe command will be used to stop the application
			 * after this. No user action is needed.
			 */
			scds_syslog(LOG_ERR, "Stopping %s "
				"with command %s failed.",
				APP_NAME, soft_stop_cmd);

		hard_stop_cmd = ds_string_format(
			"%s/bin/dbmcli -U %s db_stop>/dev/null 2>&1",
			sapdbxpropsp->ext_indep_prog_path,
			sapdbxpropsp->ext_user_key);
		if (hard_stop_cmd == NULL) {
			ds_internal_error(
				"Failed to form the command dbmcli "
				"-U<user-key> db_stop");
			free(soft_stop_cmd);
			return (1);
		}

		scds_syslog(LOG_NOTICE, "Stopping %s with command %s.",
			APP_NAME, hard_stop_cmd);
		rc = run_db_command(scds_handle, sapdbxpropsp,
			stop_hard_timeout, hard_stop_cmd, B_FALSE);

		scds_syslog_debug(SCDS_DBG_HIGH, "hard stop = %s, rc=%d",
			hard_stop_cmd, rc);

		if (rc != SCHA_ERR_NOERR) {
			if (rc == SCHA_ERR_TIMEOUT) {
				scds_syslog(LOG_ERR, "Stopping %s timed out "
					"with command %s.", APP_NAME,
					hard_stop_cmd);
			} else
				scds_syslog(LOG_ERR, "Stopping %s "
					"with command %s failed.", APP_NAME,
					hard_stop_cmd);

			/* send signal to kernel processes */
			kill_cmd = ds_string_format("%s/%s %s",
				scds_get_rt_rt_basedir(scds_handle),
				STOP_DB_SIGNAL,
				sapdbxpropsp->ext_db_name);
			if (kill_cmd == NULL) {
				ds_internal_error(
					"Failed to form the command to"
					"terminate kernel processes with "
					"signal SIGKILL.");
				free(soft_stop_cmd);
				free(hard_stop_cmd);
				return (1);
			}

			/* stopping it w/ signal w/ no timeout */
			scds_syslog(LOG_NOTICE, "Stopping %s with command %s.",
				APP_NAME, kill_cmd);
			rc = run_db_command(scds_handle, sapdbxpropsp,
				stop_timeout, kill_cmd, B_FALSE);

			scds_syslog_debug(SCDS_DBG_HIGH, "signal stop %s rc=%d",
				kill_cmd, rc);

			/*
			 * rc  should be 0. If it doesn't come back,
			 * that means the kill -9 command didn't terminate
			 * the processes, eventually stop will timeout by
			 * RGM.
			 */
			free(soft_stop_cmd);
			free(hard_stop_cmd);
			free(kill_cmd);
		} else {
			free(soft_stop_cmd);
			free(hard_stop_cmd);
		}
	} else
		free(soft_stop_cmd);

	scds_syslog(LOG_NOTICE, "Successfully stopped %s.", APP_NAME);

	return (rc);
}


/*
 * This function starts the fault monitor for a SAPDB resource.
 * This is done by starting the probe under PMF. The PMF tag
 * is derived as <RG-name,RS-name,instance_number.mon>. The restart option
 * of PMF is used but not the "infinite restart". Instead
 * interval/retry_time is obtained from the RTR file.
 */

int
mon_start(scds_handle_t scds_handle)
{
	scha_err_t	err;

	scds_syslog_debug(SCDS_DBG_HIGH,
	    "Calling MONITOR_START method for resource <%s>.",
	    scds_get_resource_name(scds_handle));

	/*
	 * The probe sapdb_probe is assumed to be available in the same
	 * subdirectory where the other callback methods for the RT are
	 * installed. The last parameter to scds_pmf_start denotes the
	 * child monitor level. Since we are starting the probe under PMF
	 * we need to monitor the probe process only and hence we are using
	 * a value of 0.
	 */

	err = scds_pmf_start(scds_handle, SCDS_PMF_TYPE_MON,
	    SCDS_PMF_SINGLE_INSTANCE, "sapdb_probe", 0);

	if (err != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to start fault monitor.");
		return (err);
	}

	scds_syslog(LOG_INFO, "Started the fault monitor.");
	return (SCHA_ERR_NOERR); /* Successfully started Monitor */
}


/*
 * This function stops the fault monitor for a SAPDB resource.
 * This is done via PMF. The PMF tag for the fault monitor is
 * constructed based on <RG-name_RS-name,instance_number.mon>.
 */

int
mon_stop(scds_handle_t scds_handle)
{
	scds_syslog_debug(SCDS_DBG_HIGH, "Calling scds_pmf_stop method");
	if (scds_pmf_stop(scds_handle, SCDS_PMF_TYPE_MON, 0, SIGKILL, -1) !=
	    SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to stop fault monitor.");
		return (1);
	}

	scds_syslog(LOG_INFO, "Stopped the fault monitor.");

	return (SCHA_ERR_NOERR);
}


/*
 * Do HASP validation.
 *
 * Returns
 *	-1	error
 *	0	OK, but storage not online on this node
 *	1	OK, storage online on this node.  Do filesystem checks.
 */
int
validate_hasp(int print, scds_handle_t handle)
{
	int				err, rc = 0;
	scds_hasp_status_t		hasp_status;

	err = scds_hasp_check(handle, &hasp_status);

	if (err != SCHA_ERR_NOERR) {
		/*
		 * scha_hasp_check() logs a message to syslog when it
		 * fails
		 */
		if (print) {
			(void) fprintf(stderr,
				gettext("INTERNAL ERROR: "
				"scds_hasp_check failed.\n"));
		}
		/*
		 * validation has failed for this resource
		 */
		return (-1);
	}

	switch (hasp_status) {

	case SCDS_HASP_NO_RESOURCE:
		/*
		 * We do not depend on any SUNW.HAStoragePlus resources
		 */
		scds_syslog(LOG_INFO,
			"This resource does not depend on any "
			"SUNW.HAStoragePlus resources. Proceeding with "
			"normal checks.");
		if (print) {
			(void) fprintf(stderr,
				gettext("This resource does not depend "
					"on any SUNW.HAStoragePlus "
					"resources. Proceeding with "
					"normal checks.\n"));
		}
		rc = 1;
		break;

	case SCDS_HASP_ERR_CONFIG:
		/*
		 * Configuration error, SUNW.HAStoragePlus resource is
		 * in a different RG. Fail the validation.
		 */
		scds_syslog(LOG_ERR,
			"One or more of the SUNW.HAStoragePlus resources "
			"that this resource depends on is in a different "
			"resource group. Failing validate method "
			"configuration checks.");
		if (print) {
			(void) fprintf(stderr,
				gettext("One or more of the "
					"SUNW.HAStoragePlus resources that "
					"this resource depends on is in a "
					"different resource "
					"group. Failing validate method "
					"configuration checks.\n"));
		}
		rc = -1;
		break;

	case SCDS_HASP_NOT_ONLINE:
		/*
		 * There is at least one SUNW.HAStoragePlus resource not
		 * online anywhere.
		 */
		scds_syslog(LOG_ERR,
			"One or more of the SUNW.HAStoragePlus resources "
			"that this resource depends on is not online "
			"anywhere. Failing validate method.");
		if (print) {
			(void) fprintf(stderr,
				gettext("One or more of the "
					"SUNW.HAStoragePlus resources that "
					"this resource depends on is not "
					"online anywhere. "
					"Failing validate method.\n"));
		}
		rc = -1;
		break;

	case SCDS_HASP_ONLINE_NOT_LOCAL:
		/*
		 * Not all SUNW.HAStoragePlus we need, are online locally.
		 */
		scds_syslog_debug(SCDS_DBG_HIGH, "HASP online not local.");
		rc = 0;
		break;

	case SCDS_HASP_ONLINE_LOCAL:
		/*
		 * All SUNW.HAStoragePlus resources we need are available on
		 * this node.
		 */
		scds_syslog_debug(SCDS_DBG_HIGH, "HSP is online local.");
		rc = 1;
		break;

	default:
		/* Unknown status code */
		scds_syslog(LOG_ERR, "Unknown status code %d.", hasp_status);
		if (print) {
			(void) fprintf(stderr,
				gettext("Unknown status code %d.\n"),
				hasp_status);
		}
		/*
		 * Since the scds_hasp_check() returned an unknown
		 * status code we return 0 which is "OK, but not
		 * online".
		 */
		rc = 0;
		break;
	}

	return (rc);
}

void
free_sapdb_property(sapdb_extprops_t *sapdbpropsp)
{

	if (sapdbpropsp == NULL)
		return;
	free(sapdbpropsp->ext_indep_prog_path);
	free(sapdbpropsp->ext_db_name);
	free(sapdbpropsp->ext_db_user);
	free(sapdbpropsp->ext_user_key);
	free(sapdbpropsp->ext_pid_dir);
}	
