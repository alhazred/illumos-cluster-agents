#!/usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#ident	"@(#)start_mysql.ksh	1.8	07/06/06 SMI"


typeset opt

while getopts 'R:G:B:D:U:H:F:L:C' opt
do
        case "$opt" in
                R)      RESOURCE=$OPTARG;;
                G)      RESOURCEGROUP=$OPTARG;;
                B)      MYSQL_BASEDIR=$OPTARG;;
                D)      MYSQL_DATADIR=$OPTARG;;
                U)      MYSQL_USER=$OPTARG;;
                H)      MYSQL_HOST=$OPTARG;;
                F)      MYSQL_FMUSER=$OPTARG;;
                L)      MYSQL_LOGDIR=$OPTARG;;
                C)      MYSQL_CHECK=TRUE;;
                *)      exit 1;;
        esac
done

. `dirname $0`/../etc/config
. `dirname $0`/functions
. `dirname $0`/../lib/functions_static

validate_options
rc_val=${?}
if [ ${rc_val} -ne 0 ]
then
	exit ${rc_val}
fi


debug_message "Method: `basename $0` - Begin"
$SET_DEBUG

variables_init

if ! validate
then
        debug_message "Method: `basename $0` - End (Exit 1)"
        exit 1
fi

if ! start_mysql
then
        # SCMSGS
        # @explanation
        # GDS couldn't start this instance of MySQL.
        # @user_action
        # Look at previous error messages.
        scds_syslog -p daemon.error -t $(syslog_tag) -m \
                "start_mysql - Could not start mysql server for %s" \
        "${MYSQL_DATADIR}"

        debug_message "Method: `basename $0` - End (Exit 1)"
        exit 1
else
        debug_message "start_mysql - Mysqld  started"
fi


debug_message "Method: `basename $0` - End (Exit 0)"
exit 0
