#!/usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#ident	"@(#)ha_mysql_smf_register.ksh	1.9	08/04/10 SMI"
#

MYDIR=/opt/SUNWscmys
MYFILE=
MYCONFIG=
MANIFEST_DIR=/var/svc/manifest/application/sczone-agents

#############################################################
# create_xml()
#
# This function creates the xml manifest that will get called 
# by SMF, i.e. svcadm enable <service> . This allows the SMF
# service to be called in a zone by the 
#
# 	Sun Cluster Data Service for Solaris Container
#	- sczsmf component
#
#############################################################

create_xml()
{
	MYDIR=$1
	MYFILE=$2

	if [ ! -d "${MANIFEST_DIR}" ]
	then
		mkdir -p ${MANIFEST_DIR}
	fi
		
	cat > ${MANIFEST_DIR}/${MYFILE}.xml <<-EOF
	<?xml version="1.0"?>
	<!DOCTYPE service_bundle SYSTEM "/usr/share/lib/xml/dtd/service_bundle.dtd.1">

	<!--
	    Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
	    Use is subject to license terms.

	-->

	<service_bundle type='manifest' name='${MYFILE}'>

	<service
	name='application/sczone-agents'
	type='service'
	version='1'>

	<!--
	    Common dependencies for the service
	-->

	<dependency name='mysql_services'
	grouping='require_all'
	restart_on='none'
	type='service'>
		<service_fmri value='svc:/network/loopback'/>
	</dependency>

	<instance name='${MYFILE}' enabled='false'>

	<!--
	    Instance specific dependencies allows for multiple instances
	    add application specific path dependencies here
	-->

	<dependency name='mysql_paths'
	grouping='require_all'
	restart_on='none'
	type='path'>
		<service_fmri value='file://${BASEDIR}'/>
		<service_fmri value='file://${DATADIR}'/>
		<service_fmri value='file://${LOGDIR}'/>
	</dependency>

	<exec_method
	type='method'
	name='start'
	exec='${MYDIR}/bin/control_mysql start'
	timeout_seconds='300' >
	<method_context project='${PROJECT}' >
		<method_credential user='root' />
	</method_context>
	</exec_method>

	<exec_method
	type='method'
	name='stop'
	exec='${MYDIR}/bin/control_mysql stop'
	timeout_seconds='300' >
	<method_context project='${PROJECT}' >
		<method_credential user='root' />
	</method_context>
	</exec_method>


	<!--
	    Instance specific parameters

	    add as many as you need, one per option

	-->

        <property_group name='parameters' type='general'>
        <propval name='Resource' type='astring' value='${RS}'/>
        <propval name='Resource_group' type='astring' value='${RG}'/>
        <propval name='Mysql_basedir' type='astring' value='${BASEDIR}'/>
        <propval name='Mysql_datadir' type='astring' value='${DATADIR}'/>
        <propval name='Mysql_user' type='astring' value='${MYSQLUSER}'/>
        <propval name='Mysql_host' type='astring' value='${MYSQLHOST}'/>
        <propval name='Mysql_fmuser' type='astring' value='${FMUSER}%${FMPASS}'/>
        <propval name='Mysql_logdir' type='astring' value='${LOGDIR}'/>
        <propval name='Mysql_check' type='astring' value='${CHECK}'/>
	</property_group>

	</instance>

	<stability value='Evolving' />

	<template>
	<common_name>
	<loctext xml:lang='C'>
	Application mysql
	</loctext>
	</common_name>
	</template>
	</service>

	</service_bundle>

	EOF
}

typeset opt

while getopts 'f:' opt
do
        case "${opt}" in
                f)      MYCONFIG=${OPTARG};;
                *)      exit 1;;
        esac
done

# Sourcing the specified config file, either the default one,
# or the one supplied with -f

if [ -n "${MYCONFIG}" ] && [ -f "${MYCONFIG}" ]
then
        echo "sourcing ${MYCONFIG}"
        . ${MYCONFIG}
else
        PKGCONF=`dirname $0`/ha_mysql_config
        echo "sourcing ${PKGCONF}"
        . ${PKGCONF}
fi

# checking and predefining user and project, if they are not set

if [ -z "${PROJECT}" ]
then
	PROJECT=:default
fi

MYFILE=${RS}

if create_xml $MYDIR $MYFILE
then
	printf "${MANIFEST_DIR}/${MYFILE}.xml successfully created\n"
else
	printf "${MANIFEST_DIR}/${MYFILE}.xml failed\n"
	exit 1
fi

if /usr/sbin/svccfg validate ${MANIFEST_DIR}/${MYFILE}.xml
then
	printf "${MANIFEST_DIR}/${MYFILE}.xml successfully validated\n"
else
	exit 1
fi
	
if /usr/sbin/svccfg import ${MANIFEST_DIR}/${MYFILE}.xml
then
	printf "${MANIFEST_DIR}/${MYFILE}.xml successfully imported\n"
else
	exit 1
fi

exit 0
