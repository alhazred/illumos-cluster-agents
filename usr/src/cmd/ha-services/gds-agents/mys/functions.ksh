#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#ident	"@(#)functions.ksh	1.14	08/10/07 SMI"


SCLOGGER=/usr/cluster/lib/sc/scds_syslog
PKG=SUNWscmys
METHOD=`basename $0`
TASK_COMMAND=""

ZONENAME=/usr/bin/zonename
TEST=/usr/bin/test

PMFADM=/usr/cluster/bin/pmfadm
variables_init()
{
	debug_message "Function: variables_init - Begin"
	${SET_DEBUG}

        #
        # Initialize some variable out of the extracted or given paramters
        #

	MYSQL_MYSQLD=./bin/mysqld
	MYSQL_PIDFILE=${MYSQL_DATADIR}/mysqld.pid
	MYSQL_DEFAULT_FILE=${MYSQL_DATADIR}/my.cnf
	MYSQL_MYSQLADMIN=${MYSQL_BASEDIR}/bin/mysqladmin
	MYSQL_MYSQL=${MYSQL_BASEDIR}/bin/mysql
	MYSQL_MYISAMCHK=${MYSQL_BASEDIR}/bin/myisamchk
	MYSQL_MYSQLD_LOGFILE=${MYSQL_LOGDIR}/${MYSQL_HOST}.log

	# The % character in the date format string needs to be hidden in a variable.
	# Otherwise a sccs delget will mess up the date format string by 
	# expanding the format string with its keywords.

	PERCENT=%
	MYSQL_MYSQLD_LOGFILE_OLD=${MYSQL_MYSQLD_LOGFILE}.`date +${PERCENT}y${PERCENT}m${PERCENT}d${PERCENT}H${PERCENT}M${PERCENT}S`
	MYSQL_FMUSER_USER=`echo ${MYSQL_FMUSER} | cut -d'%' -f1`
	MYSQL_FMUSER_PW=`echo ${MYSQL_FMUSER} | cut -d'%' -f2`
	
	MYSQL_TEST_DB="sc3_test_database"
	MYSQL_TEST_TABLE="sc3_test_table"
	MYSQL_THOROUGH_SEC=300

	MYSQL_ERRORF=/tmp/${RESOURCE}.mysql.error
	MYSQL_RESULTF=/tmp/${RESOURCE}.mysql.result
	MYSQL_LAST_RUN_THOROUGH=/tmp/${RESOURCE}.mysql.last


	MYSQL_HOUR=`date '+%H'`
	MYSQL_MIN=`date '+%M'`
	MYSQL_SEC=`date '+%S'`
	MYSQL_THOROUGH_TIME=`expr ${MYSQL_HOUR} \* 3600 + ${MYSQL_MIN} \* 60 + ${MYSQL_SEC}`

	# Retrieve MySQL Version
	VERSION=$(${ZLOGIN} ${MYSQL_MYSQLADMIN} -V |awk '{print $5}' | cut -d',' -f1 )
	MYSQL_VERSION=`echo ${VERSION} | cut -d'.' -f1`
	MYSQL_RELEASE=`echo ${VERSION} | cut -d'.' -f2`
	MYSQL_UPDATE=` echo ${VERSION} | cut -d'.' -f3`

	# The MySQL agent supports the MySQL server when it is configured as a slave or a master.
	# For slave configurations it will probe the slave status and complain on erros in the 
	# slave configuration. If the MySQL server will be stopped, the slave processes will be stopped 
	# first, and then the mysqladmin shutdown will be executed.

	if [ ${MYSQL_VERSION} -lt 5 ]
	then
		MYSQL_SLAVE=$(${ZLOGIN} grep master-host= ${MYSQL_DEFAULT_FILE} |grep -v '#' )
		MASTER_INFO=$(${ZLOGIN} grep master-info-file= ${MYSQL_DEFAULT_FILE} |grep -v '#'|awk -F= '{print $2}' )
		if [ -z "${MASTER_INFO}" ]
		then
			MASTER_INFO=${MYSQL_DATADIR}/master.info
		fi

	else
		
		MASTER_INFO=$(${ZLOGIN} grep master-info-file= ${MYSQL_DEFAULT_FILE} |grep -v '#'|awk -F= '{print $2}' )
		if [ -z "${MASTER_INFO}" ]
		then
			MASTER_INFO=${MYSQL_DATADIR}/master.info
		fi

		# The slave processes are running only if a master.info file exists, so we set the 
		# MYSQL_SLAVE flag only, if the master.info file exists.

		MYSQL_SLAVE=
		if  ${ZLOGIN} ${TEST} -f ${MASTER_INFO} 
		then
			MYSQL_SLAVE=true
		fi
		
	fi

	debug_message "Function: variables_init - End"
}

validate_options()
{
	debug_message "Function: validate_options - Begin"
	${SET_DEBUG}

        #
        # Ensure all options are set
        #

	rc_validate_options=0

        for i in RESOURCE RESOURCEGROUP MYSQL_BASEDIR MYSQL_DATADIR MYSQL_USER MYSQL_HOST MYSQL_FMUSER MYSQL_LOGDIR

        do
                case $i in
                        RESOURCE)
                        if [ -z $RESOURCE ]; then
                                scds_syslog -p daemon.err -t $(syslog_tag) -m \
                                "ERROR: Option -%s not set" \
                                "R" 
                                rc_validate_options=1
                        fi;;

                        RESOURCEGROUP)
                        if [ -z $RESOURCEGROUP ]; then
                                scds_syslog -p daemon.err -t $(syslog_tag) -m \
                                "ERROR: Option -%s not set" \
                                "G" 
                                rc_validate_options=1
                        fi;;

                        MYSQL_BASEDIR)
                        if [ -z $MYSQL_BASEDIR ]; then
                                scds_syslog -p daemon.err -t $(syslog_tag) -m \
                                "ERROR: Option -%s not set" \
                                "B" 
                                rc_validate_options=1
                        fi;;

                        MYSQL_DATADIR)
                        if [ -z $MYSQL_DATADIR ]; then
                                scds_syslog -p daemon.err -t $(syslog_tag) -m \
                                "ERROR: Option -%s not set" \
                                "D" 
                                rc_validate_options=1
                        fi;;

                        MYSQL_USER)
                        if [ -z $MYSQL_USER ]; then
                                scds_syslog -p daemon.err -t $(syslog_tag) -m \
                                "ERROR: Option -%s not set" \
                                "U" 
                                rc_validate_options=1
                        fi;;

                        MYSQL_HOST)
                        if [ -z $MYSQL_HOST ]; then
                                scds_syslog -p daemon.err -t $(syslog_tag) -m \
                                "ERROR: Option -%s not set" \
                                "H" 
                                rc_validate_options=1
                        fi;;

                        MYSQL_FMUSER)
                        if [ -z $MYSQL_FMUSER ]; then
                                scds_syslog -p daemon.err -t $(syslog_tag) -m \
                                "ERROR: Option -%s not set" \
                                "F" 
                                rc_validate_options=1
                        fi;;

                        MYSQL_LOGDIR)
                        if [ -z $MYSQL_LOGDIR ]; then
                                scds_syslog -p daemon.err -t $(syslog_tag) -m \
                                "ERROR: Option -%s not set" \
                                "L" 
                                rc_validate_options=1
                        fi;;

                esac
        done
	debug_message "Function: validate_options - Begin"
	return ${rc_validate_options}
}

validate() 
{
	#
	# Validate MySQL 
	#

        debug_message "Function: validate - Begin"
	$SET_DEBUG

	rc_validate=0
	#
	# Validate the base directory
	#

	if [ ! -d "${MYSQL_BASEDIR}" ]
	then
   		# SCMSGS
   		# @explanation
   		# The defined basedirectory (-B option) don't exist.
   		# @user_action
   		# Make sure that defined basedirectory exists.
   		scds_syslog -p daemon.error -t $(syslog_tag) -m \
			"Validate - MySQL basedirectory  %s does not exist" \
			"${MYSQL_BASEDIR}"
		rc_validate=1
	else
		debug_message "Validate - MySQL base directory ${MYSQL_BASEDIR} exists" 
	fi


	#
	# Validate the database directory
	#

	if [ ! -d "${MYSQL_DATADIR}" ]
	then
   		# SCMSGS
   		# @explanation
   		# The defined database directory (-D option) does not exist.
   		# @user_action
   		# Make sure that defined database directory exists.
   		scds_syslog -p daemon.error -t $(syslog_tag) -m \
			"Validate - MySQL database directory %s does not exist" \
			"${MYSQL_DATADIR}"
		rc_validate=1
	else
		debug_message "Validate - MySQL database directory ${MYSQL_DATADIR} exists"
	fi

	#
	# Validate that the mysqld is executable
	#

        cd ${MYSQL_BASEDIR}

	if [ ! -x "${MYSQL_MYSQLD}" ]
	then
   		# SCMSGS
   		# @explanation
   		# The mysqld command don't exist or is not executable.
   		# @user_action
   		# Make sure that MySQL is installed correctly or right
   		# base directory is defined.
   		scds_syslog -p daemon.error -t $(syslog_tag) -m \
			"Validate - mysqld %s non-existent executable" \
			"${MYSQL_MYSQLD}"
		rc_validate=1
	else
		debug_message "Validate - MySQL ${MYSQL_MYSQLD} exists and is executable"
	fi

	#
	# Validate that the mysqladmin is executable
	#

	if [ ! -x "${MYSQL_MYSQLADMIN}" ]
	then
   		# SCMSGS
   		# @explanation
   		# The mysqladmin command does not exist or is not executable.
   		# @user_action
   		# Make sure that MySQL is installed correctly or right
   		# base directory is defined.
   		scds_syslog -p daemon.error -t $(syslog_tag) -m \
			"Validate - mysqladmin %s non-existent or non-executable" \
			"${MYSQL_MYSQLADMIN}"
		rc_validate=1
	else
		debug_message "Validate - mysqladmin ${MYSQL_MYSQLADMIN} exists and is executable"
	fi

	#
	# Validate that the myisamchk is executable
	#

	if [ ! -x "${MYSQL_MYISAMCHK}" ]
	then
   		# SCMSGS
   		# @explanation
   		# The mysqladmin command does not exist or is not executable.
   		# @user_action
   		# Make sure that MySQL is installed correctly or right
   		# base directory is defined.
   		scds_syslog -p daemon.error -t $(syslog_tag) -m \
			"Validate - myisamchk %s non-existent or non-executable" \
			"${MYSQL_MYISAMCHK}"
		rc_validate=1
	else
		debug_message "Validate - myisamchk ${MYSQL_MYISAMCHK} exists and is executable"
	fi

	#
	# Validate that my.cnf file exists
	#

	if [ ! -f "${MYSQL_DEFAULT_FILE}" ]
	then
   		# SCMSGS
   		# @explanation
   		# The my.cnf configuration does not exist in the defined database
   		# directory.
   		# @user_action
   		# Make sure that my.cnf is placed in the defined database
   		# directory.
   		scds_syslog -p daemon.error -t $(syslog_tag) -m \
			"Validate - my.cnf %s does not exist" \
			"${MYSQL_DEFAULT_FILE}"
		rc_validate=1	
	else
		debug_message "Validate - my.cnf ${MYSQL_DEFAULT_FILE} exists"
	fi


	#
	# Validate that MySQL user exists
	#

	if [ -z "`getent passwd ${MYSQL_USER}`" ]
	then
 		# SCMSGS
 		# @explanation
 		# Couldn't retrieve the defined user from nameservice.
 		# @user_action
 		# Make sure that the right user is defined or the user exists.
 		# Use getent passwd 'username' to verify that defined user
 		# exists.
 		scds_syslog -p daemon.error -t $(syslog_tag) -m \
			"Validate - Couldn't retrieve MySQL-user <%s> from the nameservice" \
			"${MYSQL_USER}"
		rc_validate=1
	else
		debug_message "Validate - Retrieved MySQL-user ${MYSQL_USER} from the nameservice"
	fi

	if [ ! -d "${MYSQL_LOGDIR}" ]
	then
   		# SCMSGS
   		# @explanation
   		# The defined (-L option) log directory does not exist.
   		# @user_action
   		# Make sure that the defined log directory exists.
   		scds_syslog -p daemon.error -t $(syslog_tag) -m \
			"Validate - MySQL logdirectory for mysqld does not exist" \
			"${MYSQL_LOGDIR}"
		rc_validate=1
	else
		debug_message "Validate - MySQL logdirectory ${MYSQL_LOGDIR} exists"
	fi

	#
	# Check the MySQL version
	#

	debug_message "Validate - MySQL version <${MYSQL_VERSION}.${MYSQL_RELEASE}.${MYSQL_UPDATE}> is being used"

	if [ -z "${VERSION}" ]
	then
   		# SCMSGS
   		# @explanation
   		# Internal error when retrieving MySQL version.
   		# @user_action
   		# Make sure that supported MySQL version is being used.
   		scds_syslog -p daemon.error -t $(syslog_tag) -m \
			"Validate - Couldn't retrieve MySQL version number"
		rc_validate=1

	# Evaluate the MySQL release and update only if the MySQL version is 3.
	# This avoids naming conflicts with higher versions of MySQL.

	elif [ "${MYSQL_VERSION}" -eq 3 ]
	then 
		if [ "${MYSQL_RELEASE}" -lt 23 -a "${MYSQL_UPDATE}" -lt 54 ]
		then
	   		# SCMSGS
	   		# @explanation
	   		# An unsupported MySQL version is being used.
	   		# @user_action
	   		# Make sure that supported MySQL version is being used.
	   		scds_syslog -p daemon.error -t $(syslog_tag) -m \
				"Validate - This version of MySQL <%s> is not supported with this dataservice" \
				`printf ${MYSQL_VERSION}.${MYSQL_RELEASE}.${MYSQL_UPDATE}`
			rc_validate=1
		fi
	fi

        debug_message "Function: validate - End"
	return ${rc_validate}
}

start_mysql()
{
	#
	# Start MySQL
	#

        start_status=0

        debug_message "Function: start_mysql - Begin"
	$SET_DEBUG

	# construct the ${PORT} variable

	get_mysql_port

        # If MYSQL_CHECK is set run myisamchk on index

	if [ ! -z "${MYSQL_CHECK}" ]; then

           debug_message "start_mysql - Run myisamchk on index in ${MYSQL_DATADIR}"

	   ${MYSQL_MYISAMCHK} -c -s ${MYSQL_DATADIR}/*/*.MYI

           start_status=$?

           if [ ${start_status} -ne 0 ]; then
                # SCMSGS
                # @explanation
                # mysiamchk found errors in MyISAM based tables.
                # @user_action
                # Consult MySQL documentation when repairing MyISAM tables.
                scds_syslog -p daemon.error -t $(syslog_tag) -m \
                        "start_mysql - myisamchk found errors in some index in %s, perform manual repairs" \
                        "${MYSQL_DATADIR}"

             return ${start_status}
           fi
        fi

	#
	# IF ${MYSQL_MYSQLD_LOGFILE) EXIST MOVE IT TO ${MYSQL_MYSQLD_LOGFILE}.date
	#

	if [ -f "${MYSQL_MYSQLD_LOGFILE}" ]; then
		debug_message "start_mysql - Move ${MYSQL_MYSQLD_LOGFILE} to ${MYSQL_MYSQLD_LOGFILE_OLD}"
		mv ${MYSQL_MYSQLD_LOGFILE} ${MYSQL_MYSQLD_LOGFILE_OLD}
        fi

	#
	# Start mysqld
	#

	cd ${MYSQL_BASEDIR}
	${MYSQL_MYSQLD} --defaults-file=${MYSQL_DEFAULT_FILE} --basedir=${MYSQL_BASEDIR} --datadir=${MYSQL_DATADIR} --user=${MYSQL_USER} --pid-file=${MYSQL_PIDFILE} ${PORT} > ${MYSQL_MYSQLD_LOGFILE} 2>&1 &

        debug_message "Function: start_mysql - End"
	return 0
}

stop_mysql()
{
	#
	# Stop MySQL
	#

        debug_message "Function: stop_mysql - Begin"
	$SET_DEBUG

	# construct the ${PORT} variable

	get_mysql_port

	#
	# Initialze SECONDS to have a valid timer
	#

	SECONDS=0

	#
	# get the stop timeout if not done already in get_fmri_parameters for smf services
	#

	if [ -z "${SMF_FMRI}" ]; then
		STOP_TIMEOUT=`${SCHA_RESOURCE_GET} -R ${RESOURCE} -G ${RESOURCEGROUP} -O STOP_TIMEOUT`
	fi

	#
	# calculate the MAX_STOP_TIME as 70 percent of STOP_TIMEOUT
	#

	MAX_STOP_TIME=`expr ${STOP_TIMEOUT} \* 70 \/ 100`

	#
	# If Slave stop slave first
	# 

	if [ ! -z "${MYSQL_SLAVE}" ]; then
          debug_message "Function: stop_mysql - Issue SLAVE STOP to MySQL instance"

          ${MYSQL_MYSQL} -h ${MYSQL_HOST} -u ${MYSQL_FMUSER_USER} -p${MYSQL_FMUSER_PW} ${PORT} -e "SLAVE STOP" > ${MYSQL_RESULTF} 2> ${MYSQL_ERRORF}

          if [ $? -ne 0 ]; then
            Msg="`cat ${MYSQL_ERRORF}`"
            # SCMSGS
            # @explanation
            # Couldn't stop slave instance.
            # @user_action
            # Examine the returned Sql-status message and consult MySQL
            # documentation.
            scds_syslog -p daemon.error -t $(syslog_tag) -m \
            "Function: stop_mysql - Sql-command SLAVE STOP returned error (%s)" \
            "$Msg"
          else
           debug_message "Function: stop_mysql - STOP SLAVE issued"
          fi
        fi

	#
	# Flush MySql tables
	#

        debug_message "Function: stop_mysql - Flush MySql tables"

	${MYSQL_MYSQLADMIN} -h ${MYSQL_HOST} -u${MYSQL_FMUSER_USER} -p${MYSQL_FMUSER_PW} ${PORT} flush-tables

        if [ $? -ne 0 ]; then
           # SCMSGS
           # @explanation
           # mysqladmin command failed to flush MySQL tables.
           # @user_action
           # Either was MySQL already down or the faultmonitor user does not have
           # the right permission to flush tables. The defined faultmonitor
           # should have Process-,Select-, Reload- and Shutdown-privileges and
           # for MySQL 4.0.x also Super-privileges.
           scds_syslog -p daemon.error -t $(syslog_tag) -m \
                        "stop_mysql - Failed to flush MySQL tables for %s" \
                        "${MYSQL_DATADIR}"

        else
           debug_message "Function: stop_mysql - MySQL tables flushed"
        fi

	#
	# Flush MySql logfiles
	#

        debug_message "Function: stop_mysql - Flush MySql logfiles"

	${MYSQL_MYSQLADMIN} -h ${MYSQL_HOST} -u${MYSQL_FMUSER_USER} -p${MYSQL_FMUSER_PW} ${PORT} flush-logs

        if [ $? -ne 0 ]; then
           # SCMSGS
           # @explanation
           # mysqladmin command failed to flush MySQL logfiles.
           # @user_action
           # Either was MySQL already down or the faultmonitor user does not have
           # the right permission to flush logfiles. The defined faultmonitor
           # should have Process-,Select-, Reload- and Shutdown-privileges and
           # for MySQL 4.0.x also Super-privileges.
           scds_syslog -p daemon.error -t $(syslog_tag) -m \
                        "stop_mysql - Failed to flush MySql logfiles for %s" \
                        "${MYSQL_DATADIR}"

        else
           debug_message "Function: stop_mysql - MySql logfiles flushed"
        fi

	# 
	# Stop MySQL
	#


        debug_message "Function: stop_mysql - Stop through mysqladmin"

	MYSQL_STOP=0

	# check if mysql is running with a pid file as it always should

	PID_FILE_EXISTS=0

	if [ -f "${MYSQL_PIDFILE}" ]; then
	   PID_FILE_EXISTS=1
	fi
	
	${MYSQL_MYSQLADMIN} -h ${MYSQL_HOST} -u${MYSQL_FMUSER_USER} -p${MYSQL_FMUSER_PW} ${PORT} shutdown

        if [ $? -ne 0 ]; then
           # SCMSGS
           # @explanation
           # mysqladmin command failed to stop MySQL instance.
           # @user_action
           # Either was MySQL already down or the faultmonitor user does not have
           # the right permission to stop MySQL. The defined faultmonitor
           # should have Process-,Select-, Reload- and Shutdown-privileges and
           # for MySQL 4.0.x also Super-privileges.
           scds_syslog -p daemon.error -t $(syslog_tag) -m \
                        "stop_mysql - Failed to stop MySQL through mysqladmin for %s, send TERM signal to process" \
                        "${MYSQL_DATADIR}"

	   MYSQL_STOP=1
	else

           if [ ${PID_FILE_EXISTS} -ne 0 ]; then

              # Wait until pid is gone or MAX_STOP_TIME is exceeded

              while test ${SECONDS} -lt ${MAX_STOP_TIME} -a -f ${MYSQL_PIDFILE}
              do
                    sleep 5
              done

	      if [ -f ${MYSQL_PIDFILE} ]; then

                 # In the SMF case we can not rely on PMF, therefore the kill is introduced.
                 # Because in the SMF case PMF has only the corresponding mysql pids,
                 # if there was no restart of the resource after it got enabled.

                 debug_message "Function: stop_mysql - Stop through mysqladmin unsuccessful, mysql will be killed with signal KILL"
   		 pid=`cat ${MYSQL_PIDFILE}`
   		 /bin/kill -KILL ${pid}

              else
                 debug_message "Function: stop_mysql - Stopped through mysqladmin"
              fi

           else

              # sleep 70 percent of STOP_TIMEOUT - seconds already spent in the stop function, because 
	      # mysql was running without a pid file.
              # This should happen only if the pid file was deleted manually, which is illegal anyway.

	      let sleep_time=${MAX_STOP_TIME}-${SECONDS}
              debug_message "Function: stop_mysql - Wait ${sleep_time} seconds to let the shutdown complete"
              sleep ${sleep_time}

           fi

        fi

	if [ "${MYSQL_STOP}" -ne 0 ]; then

           debug_message "Function: stop_mysql - Try stop MySQL by sending signal TERM for ${MYSQL_DATADIR}"

           if [ -f "${MYSQL_PIDFILE}" ]
	   then
   		pid=`cat ${MYSQL_PIDFILE}`

		debug_message "stop_mysql - MySQL lockfile found"

		# check if pid is still there

		/bin/kill -0 ${pid}

		if [ $? -eq 0 ]; then

		  debug_message "stop_mysql - Send TERM to ${pid} and wait until pid-file is removed"

   		  /bin/kill -TERM ${pid}

   		  # wait until pid is gone or MAX_STOP_TIME is exceeded

		  while test ${SECONDS} -lt ${MAX_STOP_TIME} -a -f ${MYSQL_PIDFILE}
                  do
                     sleep 5
                  done

                  # The timout is exceeded, and mysql is still running if the pid file still exists

                  if [ -f ${MYSQL_PIDFILE} ]
                  then

                     debug_message "Function: stop_mysql - Stop through kill -TERM unsuccesful, mysql will be killed with signal KILL"
   		     /bin/kill -KILL ${pid}
                     
                  fi
		else
                 # SCMSGS
                 # @explanation
                 # The saved Pid didn't exist in process list.
                 # @user_action
                 # None
                 scds_syslog -p daemon.error -t $(syslog_tag) -m \
                 "stop_mysql - Pid is not running, let GDS stop MySQL for %s" \
                 "${MYSQL_DATADIR}"

		 debug_message "stop_mysql - Pid is not running, let GDS stop MySQL"
                fi
	     else
		debug_message "stop_mysql - mysqld lockfile not found - let GDS stop MySQL"
             fi

	fi


        debug_message "Function: stop_mysql - End"
	return 0
}

check_mysql()
{

	#
	# Check MySQL 
	#

        debug_message "Function: check_mysql - Begin"
	$SET_DEBUG

	rc_check_mysql=0

	# construct the ${PORT} variable

	get_mysql_port

	#
	# MYSQL AVAILBLE TEST
	#

	debug_message "check_mysql - Available test started"

        ${MYSQL_MYSQLADMIN} -h ${MYSQL_HOST} -u ${MYSQL_FMUSER_USER} -p${MYSQL_FMUSER_PW} ${PORT} ping > ${MYSQL_RESULTF} 2> ${MYSQL_ERRORF}

	if [ -s "${MYSQL_ERRORF}" ]
	then
  		# SCMSGS
  		# @explanation
  		# The faultmonitor can't connect to the specified MySQL
  		# instance.
  		# @user_action
  		# None
  		scds_syslog -p daemon.error -t $(syslog_tag) -m \
			"check_mysql - mysqld server <%s> not working, failed to connect to MySQL" \
			"${MYSQL_DATADIR}"

                rc_check_mysql=100
                return ${rc_check_mysql}
	else
		debug_message "check_mysql - MySQL is working"
	fi


	debug_message "check_mysql - Available test ended"


	# Check is we should do thorough test

	if [ ! -f "${MYSQL_LAST_RUN_THOROUGH}" ]; then
          MYSQL_THOROUGH_TIME_LAST=0
        else
	  MYSQL_THOROUGH_TIME_LAST=`cat ${MYSQL_LAST_RUN_THOROUGH}`
        fi

	# Check if we past 12/24 hour

	if [ "${MYSQL_THOROUGH_TIME_LAST}" -gt "${MYSQL_THOROUGH_TIME}" ]; then
           MYSQL_THOROUGH_TIME_LAST=${MYSQL_THOROUGH_TIME}
	   echo ${MYSQL_THOROUGH_TIME} > ${MYSQL_LAST_RUN_THOROUGH}
	fi

	# If it has past > ${MYSQL_THOROUGH_SEC} sec then run thorough test

	s1=`expr ${MYSQL_THOROUGH_TIME} - ${MYSQL_THOROUGH_TIME_LAST}`

	if [ "${s1}" -gt ${MYSQL_THOROUGH_SEC} ]; then
          check_mysql_thorough
	  rc_check_mysql=${?}

	  # UPDATE FILE
	  echo ${MYSQL_THOROUGH_TIME} > ${MYSQL_LAST_RUN_THOROUGH}
        fi

        debug_message "Function: check_mysql - End"
        return ${rc_check_mysql}

}

check_mysql_thorough()
{
	#
	# MYSQL CONSISTENCY TEST
	#

	$SET_DEBUG

	debug_message "check_mysql - Consistency test started"

	# check the slave status if the mysql daatabse is configured as a slave

	check_mysql_slave

	debug_message "check_mysql - Get defined databases"

	Dbs=`${MYSQL_MYSQL} -h ${MYSQL_HOST} -u ${MYSQL_FMUSER_USER} -p${MYSQL_FMUSER_PW} -B ${PORT} -e 'show databases' | tail +2`

	if [ $? -eq 0 ]; then

	   debug_message "check_mysql - Got defined databases (${Db})"

	   for db in ${Dbs}
           do
             debug_message "check_mysql - Use database $db and do show tables for that database"

	     ${MYSQL_MYSQL} -h ${MYSQL_HOST} -u ${MYSQL_FMUSER_USER} -p${MYSQL_FMUSER_PW} -B ${PORT} -e "use  \`${db}\`; show tables" > ${MYSQL_RESULTF} 2> ${MYSQL_ERRORF}

             if [ $? -ne 0 ]; then
	       Msg="`cat ${MYSQL_ERRORF}`"
  	       # SCMSGS
  	       # @explanation
  	       # The faultmonitor can't issue show tables for the specified
  	       # database.
  	       # @user_action
  	       # Either was MySQL already down or the faultmonitor user does not
  	       # have the right permission. The defined faultmonitor should
  	       # have Process-,Select-, Reload- and Shutdown-privileges and
  	       # for MySQL 4.0.x also Super-privileges. Check also the MySQL
  	       # logfiles for any other errors.
  	       scds_syslog -p daemon.error -t $(syslog_tag) -m \
		  "check_mysql - Couldn't do show tables for defined database %s (%s)" \
		  "${db}" "$Msg"
             fi
           done

	else
  	  # SCMSGS
  	  # @explanation
  	  # The faultmonitor can't retrieve all defined databases for the
  	  # specified instance.
  	  # @explanation-2
  	  # The database in the start, stop and probe command does not exist.
  	  # @user_action
  	  # Either was MySQL already down or the faultmonitor user does not have
  	  # the right permission. The defined faultmonitor should have
  	  # Process-,Select-, Reload- and Shutdown-privileges and for MySQL
  	  # 4.0.x also Super-privileges. Check also the MySQL logfiles for any
  	  # other errors.
  	  # @user_action-2
  	  # Fix the start, stop and probe command.
  	  scds_syslog -p daemon.error -t $(syslog_tag) -m \
		"check_mysql - Couldn't retrieve defined databases for %s " \
		"${MYSQL_DATADIR}"
	fi


	debug_message "check_mysql - Consistency test ended"

	#
	# MYSQL FUNCTION TEST
	#


	debug_message "check_mysql - Function test started"


	debug_message "check_mysql - Retrive tables from ${MYSQL_TEST_DB}"

	Tab=`${MYSQL_MYSQL} -h ${MYSQL_HOST} -u ${MYSQL_FMUSER_USER} -p${MYSQL_FMUSER_PW} -B ${PORT} -e "use ${MYSQL_TEST_DB}; show tables" | tail +2 2> ${MYSQL_ERRORF}`

	if [ ! -z "${Tab}" ]; then

	   debug_message "check_mysql - ${MYSQL_TEST_DB} contained ${Tab}"

	   debug_message "check_mysql - Drop those tables"

	   for s1 in ${Tab}
           do
              ${MYSQL_MYSQL} -h ${MYSQL_HOST} -u ${MYSQL_FMUSER_USER} -p${MYSQL_FMUSER_PW} ${PORT} -e "use ${MYSQL_TEST_DB}; drop table ${s1}; COMMIT" > ${MYSQL_RESULTF} 2> ${MYSQL_ERRORF}

	      if [ $? -ne 0 ]; then
                Msg="`cat ${MYSQL_ERRORF}`"
                # SCMSGS
                # @explanation
                # The faultmonitor can't drop specified table from the test
                # database.
                # @user_action
                # Either was MySQL already down or the faultmonitor user does not
                # have the right permission. The defined faultmonitor should
                # have Process-,Select-, Reload- and Shutdown-privileges and
                # for MySQL 4.0.x also Super-privileges. Check also the MySQL
                # logfiles for any other errors.
                scds_syslog -p daemon.error -t $(syslog_tag) -m \
                    "check_mysql - Couldn't drop table %s from database %s (%s)" \
                    "${MYSQL_TEST_DB}" "$s1" "$Msg"
              fi

	   done

	   debug_message "check_mysql - Tables droped"
	fi

	for cmd in 'USE sc3_test_database; CREATE TABLE sc3_test_table ( Number int(5) NOT NULL, PRIMARY KEY (Number)) TYPE=MyISAM;COMMIT' \
                   'USE sc3_test_database; INSERT INTO sc3_test_table VALUES (1000); COMMIT' \
		  'USE sc3_test_database; UPDATE sc3_test_table Set Number=2000 WHERE Number=1000; COMMIT' \
		  'USE sc3_test_database; DELETE FROM sc3_test_table WHERE Number=2000; COMMIT' \
		  'USE sc3_test_database; DROP TABLE sc3_test_table; COMMIT'
        do
	  debug_message "check_mysql - Run Sql-command ${cmd}"

          ${MYSQL_MYSQL} -h ${MYSQL_HOST} -u ${MYSQL_FMUSER_USER} -p${MYSQL_FMUSER_PW} ${PORT} -e "${cmd}" > ${MYSQL_RESULTF} 2> ${MYSQL_ERRORF}

          if [ $? -ne 0 ]; then
            rc_check_mysql=100
            Msg="`cat ${MYSQL_ERRORF}`"
            # SCMSGS
            # @explanation
            # The faultmonitor can't execute the specified SQL command.
            # @user_action
            # Either was MySQL already down or the faultmonitor user does not
            # have the right permission. The defined faultmonitor should have
            # Process-,Select-, Reload- and Shutdown-privileges and for MySQL
            # 4.0.x also Super-privileges. Check also the MySQL logfiles for
            # any other errors.
            scds_syslog -p daemon.error -t $(syslog_tag) -m \
            "check_mysql - Sql-command %s returned error (%s)" \
            "${cmd}" "$Msg"
            return
          fi
 
        done

	debug_message "check_mysql - Function test ended"
}

get_fmri_parameters ()
{

# extract the smf properties you need to call your agent commands

        debug_message "Function: get_fmri_parameters - Begin "
        ${SET_DEBUG}

# Resource name

	RESOURCE=`/usr/bin/svcprop -p parameters/Resource ${SMF_FMRI}`

# Resource Group

	RESOURCEGROUP=`/usr/bin/svcprop -p parameters/Resource_group ${SMF_FMRI}`

# Start_timeout

	START_TIMEOUT=`/usr/bin/svcprop -p start/timeout_seconds ${SMF_FMRI}`

# Stop_timeout

	STOP_TIMEOUT=`/usr/bin/svcprop -p stop/timeout_seconds ${SMF_FMRI}`

# Start Project

	Project=:default
	if /usr/bin/svcprop ${SMF_FMRI}|grep start/project >/dev/null
	then
		Project=`/usr/bin/svcprop -p start/project ${SMF_FMRI}`
		if [ "${Project}" != ":default" ]
		then
			PROJ_OPT=" -P ${Project}"
		fi
	fi
#
#  Mysql specific parameters
#


	MYSQL_BASEDIR=`/usr/bin/svcprop -p parameters/Mysql_basedir ${SMF_FMRI}`
	MYSQL_DATADIR=`/usr/bin/svcprop -p parameters/Mysql_datadir ${SMF_FMRI}`
	MYSQL_USER=`/usr/bin/svcprop -p parameters/Mysql_user ${SMF_FMRI}`
	MYSQL_HOST=`/usr/bin/svcprop -p parameters/Mysql_host ${SMF_FMRI}`
	MYSQL_FMUSER=`/usr/bin/svcprop -p parameters/Mysql_fmuser ${SMF_FMRI}`
	MYSQL_LOGDIR=`/usr/bin/svcprop -p parameters/Mysql_logdir ${SMF_FMRI}`
	MYSQL_CHECK=`/usr/bin/svcprop -p parameters/Mysql_check ${SMF_FMRI}`

        debug_message "Function: get_fmri_parameters - End "
}

get_mysql_port ()
{

	# Construct the port option to a value --port=3306 or --port=number.
	# This is done by parsing the output of mysqld --defaults-file=${MYSQL_DEFAULT_FILE} --print-defaults.
	# This command shows the start options of mysqld after parsing the configuration file.
	# If no --port is in the command string, the value --port=3306 is placed in the ${PORT} variable.

        debug_message "Function: get_mysql_port - Begin "

        ${SET_DEBUG}

	# get the start options

	TPORT=$(eval ${ZLOGIN} ${QUOTE}  ${MYSQL_BASEDIR}/${MYSQL_MYSQLD} --defaults-file=${MYSQL_DEFAULT_FILE} --print-defaults ${QUOTE}| sed s/.*--port=/--port=/|awk '{print $1}' )

	# construct the PORT value.

	PREFIX=`echo ${TPORT}|nawk -F" |=" '{print$2}'`
	if [ "${PREFIX}" = "--port" ]
	then
	   PORT=`echo ${TPORT}|nawk  '{print$2}'`
	else
	   PORT="--port=3306"
	fi
        debug_message "Function: get_mysql_port - End "
}
check_mysql_slave()
{
	#
	# MYSQL SLAVE CONSISTENCY TEST
	#
	# This function returns the following variables:
	# State which is either UNKNOWN, OK, FAULTED or DEGRADED
	# Message which contains the status message for the replication resource probe
	# The variables state and message are evaluated only from the geocontrol module
	# the exit code is always 0


	$SET_DEBUG

	debug_message "check_mysql_slave - Consistency test Begin"
	State="UNKNOWN"
	Message=""

	# If slave instance, check if slave connection to master is active

	if [ ! -z "${MYSQL_SLAVE}" ]; then

		# Use right columns for version 3 and 4

		if [ "${MYSQL_VERSION}" -eq 3 ]; then
		   MYSQL_STATUS_COM="/bin/cut -f1,7,10,11"
		elif [ "${MYSQL_VERSION}" -eq 4 ]; then

		   # The format of slave status changed at MySQL release 4.1
		   # so we need different MYSQL_STATUS_COM commands

		   if [ ${MYSQL_RELEASE} -eq 0 ]
		   then
		   	MYSQL_STATUS_COM="/bin/cut -f1,10,14,15"
		   else
		   	MYSQL_STATUS_COM="/usr/bin/sed s/\ /-/g |/bin/cut -f2,11,20,21"
		   fi
		elif [ "${MYSQL_VERSION}" -gt 4 ]; then
		   MYSQL_STATUS_COM="/usr/bin/sed s/\ /-/g |/bin/cut -f1,2,11,12,20,21"
                fi

		debug_message "check_mysql_slave - Check slave connection to master"

		mask_string "show slave status"
		command="${ZLOGIN} ${MYSQL_MYSQL} -h ${MYSQL_HOST} -u ${MYSQL_FMUSER_USER} -p${MYSQL_FMUSER_PW} -B ${PORT} -e ${masked_string} >${MYSQL_RESULTF} 2> ${MYSQL_ERRORF} "
		eval ${command}

		if [ $? -ne 0 ]; then
                   Msg="`cat ${MYSQL_ERRORF}`"
                   # SCMSGS
                   # @explanation
                   # The faultmonitor can't retrieve the MySQL slave status
                   # for the specified instance.
                   # @user_action
                   # Either was MySQL already down or the faultmonitor user
                   # does not have the right permission. The defined faultmonitor
                   # should have Process-,Select-, Reload- and
                   # Shutdown-privileges and for MySQL 4.0.x also
                   # Super-privileges. Check also the MySQL logfiles for any
                   # other errors.
                   scds_syslog -p daemon.error -t $(syslog_tag) -m \
                  "check_mysql_slave - Couldn't get SHOW SLAVE STATUS for instance %s (%s)"  \
		  "${MYSQL_DATADIR}" "${Msg}"

		  # Set State to FAULTED because the connection was impossible

		  State="FAULTED"
		  Message="${Msg}"
		
                else

		   # RESULT CHECK

		   Res=`cat ${MYSQL_RESULTF} | tail +2 | eval ${MYSQL_STATUS_COM}`

		   # Starting with version 5, a node which was acting as a slave, does always run
		   # some slave processes unless they are excluded at the start with the skip-slave-start
                   # command in the my.cnf. If the set of processes is incomplete, it is a master which
		   # was acting as a slave sometime ago.
		   # Entries in the field SLAVE_IO_STATE mark a node as an acting slave.
		   # In version 4 and 3 we place true into the field SLAVE_IO_STATE to trigger the 
		   # slave check. Starting with Version 5 we check the SLAVE_IO_STATE field.

		    if [ ${MYSQL_VERSION} -lt 5 ]
		    then
		       SLAVE_IO_STATE="true"
		       SLAVE_HOST=`echo $Res | awk '{print $1}'`
		       SLAVE_RUNNING=`echo $Res | awk '{print $2}'`
		       SLAVE_ERROR=`echo $Res | awk '{print $3}'`
		       SLAVE_ERROR_MESSAGE=`echo $Res | awk '{print $4}'`
		    else

		       # By the word count we can determine if an SLAVE_IO_STATE is defined,
		       # and define the necessary variables according to the word count.

		       WORDS=`echo $Res |wc -w`
		       if [ $WORDS -gt 4 ]
		       then
		          SLAVE_IO_STATE=`echo $Res | awk '{print $1}'`
		          SLAVE_HOST=`echo $Res | awk '{print $2}'`
		          SLAVE_RUNNING=`echo $Res | awk '{print $3}'`
		          SLAVE_SQL_RUNNING=`echo $Res | awk '{print $4}'`
		          SLAVE_ERROR=`echo $Res | awk '{print $5}'`
		          SLAVE_ERROR_MESSAGE=`echo $Res | awk '{print $6}'`

                       # The slave may have been stopped manually

		       elif  [ $WORDS -eq 4 ]
                       then
		          SLAVE_IO_STATE=
		          SLAVE_HOST=`echo $Res | awk '{print $1}'`
		          SLAVE_RUNNING=`echo $Res | awk '{print $2}'`
		          SLAVE_SQL_RUNNING=`echo $Res | awk '{print $3}'`
		          SLAVE_ERROR=`echo $Res | awk '{print $4}'`
		          SLAVE_ERROR_MESSAGE=""

                       # The slave did not start but, does not state an error in the SLAVE_IO_STATE

		       elif  [ $WORDS -eq 3 ]
                       then
		          SLAVE_IO_STATE=
		          SLAVE_HOST=`echo $Res | awk '{print $1}'`
		          SLAVE_RUNNING=`echo $Res | awk '{print $2}'`
		          SLAVE_SQL_RUNNING=`echo $Res | awk '{print $3}'`
		          SLAVE_ERROR=
		          SLAVE_ERROR_MESSAGE=""
		       fi
		 fi

	         debug_message "check_mysql_slave - Slave connection to ${SLAVE_HOST}, Connect status is ${SLAVE_RUNNING} with error ${SLAVE_ERROR}"


		 if [ "${SLAVE_RUNNING}" == "No" ] && [ -n "${SLAVE_IO_STATE}" ]; then
                    # SCMSGS
                    # @explanation
                    # The faultmonitor has detected that the MySQL slave
                    # instance is not connected to the specified master.
                    # @user_action
                    # Check MySQL logfiles to determine why the slave has been
                    # disconnected to the master.
                    scds_syslog -p daemon.error -t $(syslog_tag) -m \
                       "check_mysql_slave - MySQL slave instance %s is not connected to master %s with MySql error (%s)"  \
	               "${MYSQL_DATADIR}" "${SLAVE_HOST}" "${SLAVE_ERROR}"
                     State="DEGRADED"
                     if [ -n "${SLAVE_ERROR_MESSAGE}" ]
                     then
                         Message="${SLAVE_ERROR_MESSAGE}"
	             else
                         # If the slaves error message is not empty, there is a problem description 
                         # in the SLAVE_IO_STATE.
	                 Message="${SLAVE_IO_STATE}"
                     fi
                 else 
                     if [ -n "${SLAVE_SQL_RUNNING}" ]
                     then
                        if [ "${SLAVE_RUNNING}" == "${SLAVE_SQL_RUNNING}" ]
                        then
                           State="OK"
                           Message=""
                        else
                           # SCMSGS
                           # @explanation
                           # The faultmonitor has detected that the MySQL slave
                           # instance has a different status for the slaves io_thread and the sql_thread
                           # @user_action
                           # Check MySQL logfiles to determine why the slaves status for the 
                           # io_thread and the sql thread do not match
                           scds_syslog -p daemon.error -t $(syslog_tag) -m \
                              "check_mysql_slave - MySQL slave instance %s has the io_thread status %s and the sql_thread status %s"  \
      	                "${MYSQL_DATADIR}" "${SLAVE_RUNNING}" "${SLAVE_SQL_RUNNING}"
                           State="DEGRADED"
                           if [ -n "${SLAVE_ERROR_MESSAGE}" ]
                           then
                               Message="${SLAVE_ERROR_MESSAGE}"
                           else
                               # If the slaves error message is not empty, there is a problem description 
                               # in the SLAVE_IO_STATE.
                               Message="The slave io_thread has status ${SLAVE_RUNNING} and the sql_thread has status ${SLAVE_SQL_RUNNING}"
                           fi
                        fi
                     else

                        # There is no SLAVE_SQL_RUNNING defined, so we set the status to OK

                        State="OK"
                        Message=""

                     fi
                 fi
             fi

        fi
	debug_message "check_mysql_slave  Consistency test End"
	return 0
}

mask_string()
{

	# Assembles the variable masked_string according to the zone node variable.
	# The result is in the variable masked_string and is ready to be used in
	# an eval command. The string in $ 1 is stored in the variable mask_string

	debug_message "Function: mask_string - Begin"
	${SET_DEBUG}
	
	if [ -n "${zonenode}" ]
	then
		masked_string="\\\"${1}\\\""
	else
		masked_string="\"${1}\""
	fi

	# store the parameter in the variable maks_string

	mask_string="${1}"
	debug_message "Function: mask_string - END"
}
