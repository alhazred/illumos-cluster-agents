#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#ident	"@(#)my.cnf_sample_slave	1.7	07/06/06 SMI"

# Note: Please be aware that this sample my.conf file is 
# 	provided to get you started. You should read
#	the MySQL documentation for a complete description 
#	of the options listed here.
#	
#	Furthermore, /global/mysql-data-1 is used here to match up
#	with the Sun Cluster HA for MySQL documentation. You
#	will need to change this if your path is different.

[mysqld]
#Server Id
server-id=2

#Port Number 
port=3306

# Bind Address
bind-address=<Logical Host>

# Socket
socket=/tmp/<Logical Host>.sock

# General Logfile
log=/global/mysql-data/logs/log1

# Binary Logfile
log-bin=/global/mysql-data/logs/bin-log

# Ignore logg updates for sc3_test_database
binlog-ignore-db=sc3_test_database

# Log Slow Queries
log-slow-queries=/global/mysql-data/logs/log-slow-queries

#
# Innodb Table Type Section
#

# Should InnoDB Table be used.
#skip-innodb

# Home Directory for InnoDB
innodb_data_home_dir = /global/mysql-data/innodb

# File-name and size for InnoDB
innodb_data_file_path = ibdata1:10M:autoextend

# Log directory for InnoDB
innodb_log_group_home_dir = /global/mysql-data/innodb

# Archive directory for InnoDB
innodb_log_arch_dir = /global/mysql-data/innodb

set-variable = innodb_buffer_pool_size=50M
# You can set .._buffer_pool_size up to 50 - 80 %
# of RAM but beware of setting memory usage too high

set-variable = innodb_additional_mem_pool_size=20M
# Set .._log_file_size to 25 % of buffer pool size

# Log file size
set-variable = innodb_log_file_size=12M

# Log buffer size
set-variable = innodb_log_buffer_size=4M

# Should InnoDB flush logs to disk on commit.
innodb_flush_log_at_trx_commit=1

#
# BDB Table Types section
#

# Should BDB Table be used.
#skip-bdb

#Base directory for `BDB' tables
bdb-home=/global/mysql-data

#Don't start Berkeley DB in recover mode.
bdb-no-recover

# Berkeley lock detect.
bdb-lock-detect=DEFAULT

# BDB Log Directory
bdb-logdir=/global/mysql-data/BDB

# BDB Temp Directory
bdb-tmpdir=/global/mysql-data/BDB

#
# Slave Configuration Section
#

# Master hostname or IP address for replication.
master-host=<hostname>

# The username the slave thread will use for
# authentication when connecting to the
# master.
master-user=repl


# The password the slave thread will use for
# authentication when connecting to the
# master.
master-password=repl

# The location of the file that remembers
# where we left off on the master during the
# replication process.
master-info-file=/global/mysql-data/logs/master.info

# Specific for MySQL 4.x

# Where to put the slave's relay bin log.
#relay-log=/global/mysql-data/logs/slave-bin.log

# Where to put the slave's info file
#relay-log-info-file=/global/mysql-data/logs/slave-info

