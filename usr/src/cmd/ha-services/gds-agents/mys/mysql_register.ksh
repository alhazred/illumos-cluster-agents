#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#ident	"@(#)mysql_register.ksh	1.8	09/03/02 SMI"


BINDIR=/opt/SUNWscmys/bin
UTILDIR=/opt/SUNWscmys/util
MYCONFIG=

typeset opt

while getopts 'f:' opt
do
        case "${opt}" in
                f)      MYCONFIG=${OPTARG};;
                *)      exit 1;;
        esac
done

# source the necessary functions for defining the PORT variable

. `dirname $0`/../etc/config
. `dirname $0`/../bin/functions
. `dirname $0`/../lib/functions_static

# Sourcing the specified config file, either the default one,
# or the one supplied with -f

if [ -n "${MYCONFIG}" ] && [ -f "${MYCONFIG}" ]
then
	echo "sourcing ${MYCONFIG} and create a working copy under ${UTILDIR}/mysql_config.work"
	cp ${MYCONFIG} ${UTILDIR}/mysql_config.work 2>/dev/null
	. ${MYCONFIG}
else
	PKGCONF=`dirname $0`/mysql_config
	echo "sourcing ${PKGCONF}"
	. ${PKGCONF}
fi

# initialize the variables common with start, stop and check

MYSQL_BASEDIR=${MYSQL_BASE}
variables_init

# initialize the rest 

TEST_DB="sc3_test_database"
SOLARIS_RELEASE=`uname -r`


if [ ! -f "${MYSQL_DEFAULT_FILE}" ]; then
  echo "${MYSQL_DEFAULT_FILE} don't exist"
  exit 1
fi

if [ ! -x "${MYSQL_MYSQL}" ]; then
  echo "${MYSQL_MYSQL} don't exist"
  exit 1
fi

# get the PORT variable  out of the mysql configuration, which then will contain --port=3306 or whatever port is specified

get_mysql_port

echo
echo "MySQL version ${MYSQL_VERSION} detected on ${SOLARIS_RELEASE}"
echo

# CHECK THAT MYSQL_NIC_HOSTNAME IS SET

if [ -z "${MYSQL_NIC_HOSTNAME}" ]; then
   echo "Variable MYSQL_NIC_HOSTNAME is not set in mysql_config"
   exit 1
fi

node_array=${MYSQL_NIC_HOSTNAME}

cd ${MYSQL_BASEDIR}

#
# Check that MySQL is running and accessible.
# Whenever the MySQL daemon is running, mysql_admin returns with 0.
# Only the absence of messages on STDERR confirms a real success.
#

echo 
echo Check if the MySQL server is running and accepting connections
echo 

${MYSQL_MYSQLADMIN} -h ${MYSQL_HOST} -u ${MYSQL_USER} -p${MYSQL_PASSWD} ${PORT} ping > ${MYSQL_RESULTF} 2> ${MYSQL_ERRORF}
if [ -s "${MYSQL_ERRORF}" ]
then
	echo
	echo "The MySQL server is not running or not accepting connections"
	echo
	echo "The Mysql configuration for HA failed"
	echo
	exit 1
fi

config_errors=0

for node in ${node_array}
do

  echo
  echo "Add faulmonitor user (${FMUSER}) with password (${FMPASS}) with Process-,Select-, Reload- and Shutdown-privileges to user table for mysql database for host ${node}"
  echo

  ${MYSQL_MYSQL} -S ${MYSQL_SOCK} -u${MYSQL_USER} -p${MYSQL_PASSWD} ${PORT} -e "GRANT PROCESS,SELECT,RELOAD,SHUTDOWN ON *.* TO ${FMUSER}@'${node}' IDENTIFIED BY '${FMPASS}'"

  if [ $? -ne 0 ]; then
   echo "Add faulmonitor user ${FMUSER} with Process-, Select-, Reload- and Shutdown-privileges to user table for mysql database for ${node} failed"
  fi

  if [ "${MYSQL_VERSION}" -gt 3 ]; then
     #
     # ADD SUPER PRIVILEGE FOR ${FMUSER}
     #

     echo
     echo "Add SUPER privilege for ${FMUSER}@${node}"
     echo

     ${MYSQL_MYSQL} -S ${MYSQL_SOCK} -u${MYSQL_USER} -p${MYSQL_PASSWD} ${PORT} -e "use mysql; UPDATE user SET Super_priv='Y' WHERE user='${FMUSER}' AND Host='${node}';"
     if [ $? -ne 0 ]; then
        echo "Add GRANT privilege for ${MYSQL_USER}@${node} failed"
        let config_errors=${config_errors}+1
     fi
  fi
done


echo
echo "Create test-database ${TEST_DB}"
echo
${MYSQL_MYSQL} -S ${MYSQL_SOCK} -u${MYSQL_USER} -p${MYSQL_PASSWD} ${PORT} -e "CREATE DATABASE ${TEST_DB};"

if [ $? -ne 0 ]; then
   echo "Create test-database ${TEST_DB} failed"
   let config_errors=${config_errors}+1
fi

for node in ${node_array}
do
   echo
   echo "Grant all privileges to ${TEST_DB} for faultmonitor-user ${FMUSER} for host ${node}"
   echo

   ${MYSQL_MYSQL} -S ${MYSQL_SOCK} -u${MYSQL_USER} -p${MYSQL_PASSWD} ${PORT} -e "GRANT ALL ON ${TEST_DB}.* TO ${FMUSER}@'${node}'"

   if [ $? -ne 0 ]; then
      echo "Adding all privileges to ${TEST_DB} for faultmonitor-user ${FMUSER} for host ${node} failed"
      let config_errors=${config_errors}+1
   fi
done

echo
echo "Flush all privileges"
echo
${MYSQL_MYSQLADMIN} -S ${MYSQL_SOCK} -u${MYSQL_USER} -p${MYSQL_PASSWD} ${PORT} flush-privileges

if [ $? -ne 0 ]; then
   echo "Flush all privileges failed"
   let config_errors=${config_errors}+1
fi


if [ ${config_errors} -eq 0 ]
then
   echo
   echo "Mysql configuration for HA is done"
   echo
   exit 0
else
   echo
   echo "Mysql configuration for HA reported errors"
   echo "Some errors like database exists are acceptable"
   echo "Check the previous output"
   echo
   exit 1
fi

