#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#ident	"@(#)ha_mysql_config.ksh	1.7	07/06/06 SMI"

# This file will be sourced in by ha_mysql_register and the parameters
# listed below will be used.
#
# These parameters can be customized in (key=value) form
#
#        RS - name of the resource for the application
#        RG - name of the resource group containing RS
#
#        To have the mysql agent local zone aware, 4 Variables are needed:
#      ZONE - the zone name where the Mysql Database should run in
#             Optional
#    ZONEBT - The resource name which controls the zone. 
#             Optional
#   PROJECT - A project in the zone, that will be used for this service
#             specify it if you have an su - in the start stop or probe,
#             or to define the smf credentials. If the variable is not set,
#             it will be translated as :default for the sm and default 
#             for the zsh component
#             Optional
#     ZUSER - A user in the the zone which is used for the smf method 
#             credentials. Your smf service will run under this user
#             Optional
#
# Mysql specific Variables
#
#   BASEDIR - name of the Mysql bin directory
#   DATADIR - name of the Mysql Data directory
# MYSQLUSER - name of the user Mysql should be started of
#        LH - name of the LogicalHostname SC resource
# MYSQLHOST - name of the host in /etc/hosts
#    FMUSER - name of the Mysql fault monitor user
#    FMPASS - name of the Mysql fault monitor user password
#    LOGDIR - name of the directory mysqld should store it's logfile.
#    CHECK  - should HA-MySQL check MyISAM index files before start YES/NO.
#    HAS_RS - name of the MySQL HAStoragePlus SC resource
#
#	The following examples illustrate sample parameters
#	for Mysql
#
#	BASEDIR=/usr/local/mysql
#	DATADIR=/global/mysqldata
#	MYSQLUSER=mysql
#	LH=mysqllh
#       MYSQLHOST=mysqllh
#	FMUSER=fmuser
#	FMPASS=fmuser
#	LOGDIR=/global/mysqldata/logs
#	CHECK=YES
#

RS=
RG=
PORT=
LH=
HAS_RS=

# local zone specific options

ZONE=
ZONE_BT=
PROJECT=

# mysql specifications

BASEDIR=
DATADIR=
MYSQLUSER=
MYSQLHOST=
FMUSER=
FMPASS=
LOGDIR=
CHECK=
