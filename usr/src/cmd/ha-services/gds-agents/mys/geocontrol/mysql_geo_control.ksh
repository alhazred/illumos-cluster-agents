#!/usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#ident	"@(#)mysql_geo_control.ksh	1.2	08/10/07 SMI"

# Central script to control the actions for the MySQL script based plugin
# for Sun Cluster Gegraphic Edition.
# 
# The parameters fro the MySQL specific actions are passed as options.
# In addition to that the Sun Cluster Geographic Edition script based plugin
# framework passes several additional parameters as key=value pairs, which
# must be evaluated.
# These prameters are passed like: "funktion=check_failover".
# The following list is passed.

# function=
# isModify= true|false
# validate_parameters= true|false
# currentRole= primary/secondary
# pg=	Protection group name
# rgList=    A "," separated list called in the functions add_application_rgs and remove_application_rgs
# newRole= The values are primary|secondary, it is  called in the switchover and takeover functions.

# The following functions are defined:
# check_switchover
# perform_switchover
# check_takeover
# perform_takeover
# start_replication
# stop_replication
# add_application_rgs
# create_configuration
# remove_configuration
# 
# The following functions will not be implemeted by the MySQL SBP:
# remove_application_rgs

# If the function is create_c onfiguration, ist has to write three variables back
# to standard out.
# reprs=<name of the replication resource>
# reprg=<name of the replication resource group>
# rgList=<, separated list of resource groups considered as internal>
# In case of MySQL rgList is empty and echoed as rgList=

MYNAME=`basename ${0}`
MYDIR=`dirname ${0}`

# Source functions to nail down parameters which are set by options and parameters 

. ${MYDIR}/../../bin/functions
. ${MYDIR}/functions
. ${MYDIR}/../etc/config
. ${MYDIR}/../../lib/functions_static
 
debug_message "Method: ${MYNAME} - Begin"
${SET_DEBUG}
set -u

# set the variables transfered as key=value pairs in the parameters or with optional 
# commandline options to satisfy set -u

function=
isModify=
validate_parameters= 
currentRole=
pg=
rgList=
newRole=
READONLY=

# get the options and evaluate the parameters

while getopts 'O:r:g:G:R:ot:T:' opt
do
        case "${opt}" in
                O)      REPLICATED_COMPONENT=${OPTARG};;
		r)	REPL_MYSQL_RESOURCE=${OPTARG};;
		g)	REPL_MYSQL_RESOURCEGROUP=${OPTARG};;
                R)      REAL_MYSQL_RESOURCE=${OPTARG};;
                G)      REAL_MYSQL_RESOURCEGROUP=${OPTARG};;
                t)      SHORTPING=${OPTARG};;
                T)      LONGPING=${OPTARG};;
                o)      READONLY="true";;
        esac
done

shift $((${OPTIND} - 1))

numparameters=${#}

i=1
while [ ${i} -le ${numparameters} ]
do
	eval call="\${${i}}"
	eval ${call}
	let i=$i+1
done

# Copy ${REPLICATED_COMPONENT} to RESOURCE and ${pg} to RESOURCEGROUP to reuse the MySQL agents
# functions in function_static.

RESOURCE=${REPLICATED_COMPONENT}
RESOURCEGROUP=${pg}

# Set some generic variables
LOGFILE=/var/tmp/${REPLICATED_COMPONENT}_logfile
${RM} ${LOGFILE} >/dev/null 2>&1


# Normalise currentRole and newRole to upper characters

currentRole=$(${ECHO} ${currentRole} | ${TR} [:lower:] [:upper:])
newRole=$(${ECHO} ${newRole} | ${TR} [:lower:] [:upper:])

# Start with the operation

validation_err=0
if [ "${validate_parameters}" == "true" ]
then
	validate_sbp
	exit_value=${?}
else
	case ${function} in
	
	check_switchover)
		check_switchover >${LOGFILE}
		exit_value=${?};; 
	
	perform_switchover)
		perform_switchover >${LOGFILE}
		exit_value=${?};; 
	
	check_takeover)
		check_takeover >${LOGFILE}
		exit_value=${?};; 
	
	perform_takeover)
		perform_takeover >${LOGFILE}
		exit_value=${?};; 
	
	start_replication)
		start_replication >${LOGFILE}
		exit_value=${?};; 
	
	stop_replication)
		stop_replication >${LOGFILE}
		exit_value=${?};; 
	
	add_application_rgs)
		add_application_rgs >${LOGFILE}
		exit_value=${?};; 
	create_configuration)
		create_configuration >${LOGFILE}
		exit_value=${?}
		if [ ${exit_value} -eq 0 ]
		then

			# There is no rg internal to geo for the MySQL SBP, so 
			# echo an empty rgList

			${ECHO} "rgList="

			# Write the replication resource and the replication resource group to
			# standard output.

			${ECHO} "reprs=${REPL_MYSQL_RESOURCE}"
			${ECHO} "reprg=${REPL_MYSQL_RESOURCEGROUP}"

		fi;; 
	
	remove_configuration)
		remove_configuration >${LOGFILE}
		exit_value=${?};; 
	
	esac
fi

	if [ "${exit_value}" -eq 0 ]
	then
		log_message notice "mysql_geo_control_command function = ${function} rc<${exit_value}>"
	else
		log_message err "mysql_geo_control_command function = ${function} rc<${exit_value}>"
		# Putting a message to STDERR, if there was an error on a command
		${ECHO} "Consult The /var/adm/messages logs on all cluster nodes involved for further details about ${REPLICATED_COMPONENT}" 1>&2
	fi
 
exit ${exit_value}
