#!/usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#ident	"@(#)mysql_geo_register.ksh	1.3	09/03/02 SMI"

#  The mysql_geo_register script creates the protection group, and
#  adds the replicated component to the protection group.
#
#  This script takes one mandatory option: 
#  -f filename states a configuration file
#     This file will be sourced to provide the values for the registration

MYNAME=`basename ${0}`
MYDIR=`dirname ${0}`

# Set generic variables:

BINDIR=/opt/SUNWscmys/geocontrol/bin
UTILDIR=/opt/SUNWscmys/geocontrol/util
ECHO=/usr/bin/echo

. ${MYDIR}/../etc/config
. ${MYDIR}/../../lib/functions_static
. ${MYDIR}/../bin/functions
. ${MYDIR}/../../bin/functions

${SET_DEBUG}

MYCONFIG=
create_pg()
{
	${SET_DEBUG}
	
	# Create the protection group

	${GEOPG} create --role PRIMARY \
	--datarep-type sbp \
	--partnership ${PS} ${PG} \
	--property Timeout=3600 \
	--property takeover_script=${control_command} \
	--property stop_replication_script=${control_command} \
	--property RoleChange_ActionArgs= \
	--property RoleChange_ActionCmd= \
	--property create_config_script=${control_command} \
	--property start_replication_script=${control_command} \
	--property add_app_rg_script=${control_command} \
	--property remove_app_rg_script=/bin/true \
	--property remove_app_rg_args= \
	--property description="${DESC}" \
	--property configuration_file=${CONFIGFILE} \
	--property External_Dependency_Allowed=true \
	--property add_app_rg_args="${cmd_args}" \
	--property switchover_script=${control_command} \
	--property remove_config_script=${control_command} 
}

create_rc()
{
	# Add the replicated component to the protection group

	${SET_DEBUG}
	geopg add-replication-component ${REPCOMP} ${PG} \
	--property switchover_args="${cmd_args}" \
	--property takeover_args="${cmd_args}" \
	--property start_replication_args="${cmd_args}" \
	--property remove_config_args="${cmd_args}" \
	--property create_config_args="${cmd_args}" \
	--property stop_replication_args="${cmd_args}" 
}

typeset opt

while getopts 'f:' opt
do
        case "${opt}" in
                f)      MYCONFIG=${OPTARG};;
                *)      ${ECHO} "ERROR: ${MYNAME} Option ${OPTARG} unknown - early End. Only -f is valid"
                        exit 1;;
        esac
done

# Sourcing the specified config file

if [ -n "${MYCONFIG}" ] && [ -f "${MYCONFIG}" ]
then
	${ECHO} 
	${ECHO} "sourcing ${MYCONFIG} "
	${ECHO} 
	. ${MYCONFIG}
else
	${ECHO} 
	${ECHO} "ERROR: Specifiy an existing configuration file at ${MYNAME} -f <filename>"
	${ECHO} 
	exit 1
fi

# Check variables which will not be checked by the SBP validate algorithem

rc_val=0

# check the partnership

if [ -z "${PS}" ]
then
	rc_val=1
	${ECHO} "ERROR: The value for the partnership is undefined"
fi

# check the protection group

if [ -z "${PG}" ]
then
	rc_val=1
	${ECHO} "ERROR: The value for the protection group is undefined"
fi

# check the appication resource group

if [ -z "${APPRG}" ]
then
	rc_val=1
	${ECHO} "ERROR: The value for the appication resource group is undefined"
fi

if [ ${rc_val} -ne 0 ]
then
	exit 1
fi

# set the default values for LONGPING and SHORTPING

if [ -z "${LONGPING}" ]
then
	LONGPING=60
fi
if [ -z "${SHORTPING}" ]
then
	SHORTPING=10
fi

# finalize the optional commandline arguments

if [ -n "${READONLY}" ]; then
   readonly=" -o"
else
   readonly=""
fi

control_command=${BINDIR}/mysql_geo_control
cmd_args="-O ${REPCOMP} -r ${REPRS} -g ${REPRG} -R ${REALMYSRS} -G ${REALMYSRG} -t ${SHORTPING} -T ${LONGPING} ${readonly}"

if ! ${GEOPG} list ${PG} >/dev/null 2>&1
then

	# Registering the protection group
	
	if create_pg
	then
		${ECHO} 
		${ECHO} "Add the replicated component \"${REPCOMP}\" to the protection group \"${PG}\""
		${ECHO} 
		if  create_rc
		then
			if [ -n "${APPRG}" ]
			then
				${ECHO} 
				${ECHO} "Add the application resource group ${APPRG} to ${PG}"
				${ECHO} 
				if ${GEOPG} add-resource-group ${APPRG} ${PG}
				then
					${ECHO} 
					${ECHO} " Do not forget to call \"geopg get --partnership ${PS} ${PG}\" on the remote cluster"
					${ECHO} 
				else
					${ECHO} "ERROR: Application resource group could not be added, remove the protection group \"${PG}\""
					${GEOPG} delete ${PG}
				fi
			fi
		else
	
			${ECHO} 
			${ECHO} "ERROR: Replicated component \"${REPCOMP}\" could not be added, remove the protection group \"${PG}\" "
			${ECHO} 
			${GEOPG} delete ${PG}
		fi
	fi
else
	${ECHO} 
	${ECHO} "Add the replicated component \"${REPCOMP}\" to the protection group \"${PG}\""
	${ECHO} 
	if create_rc
	then
		if [ -n "${APPRG}" ]
		then
			${ECHO} 
			${ECHO} "Add the application resource group ${APPRG} to ${PG}"
			${ECHO} 
			if ${GEOPG} add-resource-group ${APPRG} ${PG}
			then
				${ECHO} 
				${ECHO} " Do not forget to call \"geopg update ${PG}\" on the remote cluster"
				${ECHO} 
			else
				${ECHO} "ERROR: Application resource group could not be added, remove the replicated component \"${REPCOMP}\" "
				${GEOPG} remove-replication-component ${REPCOMP} ${PG}
			fi
		fi
	else

		${ECHO} 
		${ECHO} "ERROR: Replicated component \"${REPCOMP}\" could not be added"
		${ECHO} 
	fi
fi
exit 0
