#!/usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#ident	"@(#)mysql_geo_replication.ksh	1.3	09/01/15 SMI"


#
# $1 validate, start, stop or probe
# $2 is the smf service tag name. It is used only if the parameter $1 is probe
#

MYNAME=`basename ${0}`
MYDIR=`dirname ${0}`

. ${MYDIR}/../../bin/functions
. ${MYDIR}/functions
. ${MYDIR}/../etc/config
. ${MYDIR}/../../lib/functions_static

debug_message "Method: ${MYNAME} ${1} - Begin"
${SET_DEBUG}
set -u

# Set variables which are expected to be undefined for the replication resource
# because they will be defiend only in functions used by the mysq_geo_control

ZLOGIN=
QUOTE=
MASK=
zonenode=

# get the options and evaluate the parameters

while getopts 'R:G:r:g:' opt
do
        case "${opt}" in
                R)      RESOURCE=${OPTARG};;
                G)      RESOURCEGROUP=${OPTARG};;
                r)      REAL_MYSQL_RESOURCE=${OPTARG};;
                g)      REAL_MYSQL_RESOURCEGROUP=${OPTARG};;
        esac
done

# Get the parameters behind the options

shift $((${OPTIND} - 1))


# Source functions to nail down parameters which are set by options 
. ${MYDIR}/functions
 
# set some generic variables
LOGFILE=/var/tmp/${RESOURCE}_logfile

case ${1} in
start)

	# Start of the mysql replication resource
	# This start does not do anything than satisfy and disable pmf

	start_mysql_replication_rs
	rc_val=${?};;
stop)

	# Stop of the mysql replication resource
	# This stop does not do anything than satisfy and disable pmf
	
	stop_mysql_replication_rs
	rc_val=${?};;

probe)

	# probe application mysql

	# exit from probe, if the options are wrong

	validate_options_replication_rs
	rc_val=${?}
	if [ ${rc_val} -ne 0 ]
	then
		terminate ${1} ${rc_val}
	fi

	get_mysql_rs_parameters ${REAL_MYSQL_RESOURCEGROUP} ${REAL_MYSQL_RESOURCE} 

	variables_init
	
	if ! validate_replication_rs
	then
	        rc_val=100
	else
		
		check_mysql_replication_rs
		rc_val=${?}
	fi;;

validate)

	# validate the parameters for application mysql

	validate_options_replication_rs
	rc_val=${?}
	if [ ${rc_val} -ne 0 ]
	then
		terminate ${1} ${rc_val}
	fi
	
	rm ${LOGFILE} 2>/dev/null

	if validate_replication_rs
	then
	        rc_val=0
	else
	        rc_val=1
	fi;;
	
esac

# terminate with the right return code, either with an smf specific or the gds/zsh based
# return code

terminate ${1} ${rc_val}
