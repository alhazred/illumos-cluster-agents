#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#ident	"@(#)mysql_geo_config.ksh	1.2	08/10/07 SMI"

# This file will be sourced in by mysql_geo_register and uses the parameters
# parameters listed below.
#
# These parameters can be customized in (key=value) form
#
#         PS - Name of the partnership
#         PG - Name of the protection group
#    REPCOMP - Name of the replicated component
#      REPRS - Name of the replication resource 
#      REPRG - Name of the replication resource group
#       DESC - Descriotion for the protection group
# CONFIGFILE - Configuration file for the script based plugin evaluation
#              rules.
#  REALMYSRG - List of resource group names containing the MySQL
#              database resource on the clusters. If the names differ between the 
#              clusters, provide a "," separated list. 
#  REALMYSRS - List of resource names configured as the master and slave MySQL
#              database resources. If the names differ between the clusters, 
#              provide a "," separated list. 
#   READONLY - Switch for setting the readonly variable at the MySQL slave.
#              If the readonly variable should not be set, leave this value undefined.
#              Every entry here will trigger the readonly variable to be set.
#      APPRG - Application resource group, which is unmanaged and contains the logical host
#              at least.
#  SHORTPING - Timeout for short ping test, The default is 10 seconds if unset.
#              The Short ping timeout is used whenever a connection should succeed, 
#              but does not have to.
#   LONGPING - Timeout for extensve ping test, The default is 60 seconds if unset.
#              This timeout is used at the check_takeover where we have to guarantee,
#              that the remote site is unavailable.
#
#	The following examples illustrate sample parameters
#	for the Mysql geographic control
#       
#       PS=mysql-parship
#       PG=mysql-pg
#       REPCOMP=mysql.sbp
#       REPRS=mysql-rep-rs
#       REPRG=mysql-rep-rg
#       DESC="mysql sbp realisation"
#       CONFIGFILE=/temp/sbpconfig
#       REALMYSRG=paris-rg,london-rg
#       REALMYSRS=paris-mys-rs,london-mys-rs
#       READONLY=
#       APPRG=europe-rg
#       LONGPING=
#       SHORTPING=
   
PS=
PG=
REPCOMP=
REPRS=
REPRG=
DESC=""
CONFIGFILE=
REALMYSRG=
REALMYSRS=
READONLY=
APPRG=
LONGPING=
SHORTPING=
