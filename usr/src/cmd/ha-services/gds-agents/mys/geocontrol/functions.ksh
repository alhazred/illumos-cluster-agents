#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#ident	"@(#)functions.ksh	1.4	09/03/02 SMI"

# initialze some generic variables

SCLOGGER=/usr/cluster/lib/sc/scds_syslog
PKG=SUNWscmys
METHOD=`basename $0`
TASK_COMMAND=""

ZONENAME=/usr/bin/zonename
CLRS=/usr/cluster/bin/clrs
CLRG=/usr/cluster/bin/clrg
CLUSTER=/usr/cluster/bin/cluster
SCHA_RESOURCE_GET=/usr/cluster/bin/scha_resource_get
SCHA_RESOURCEGROUP_GET=/usr/cluster/bin/scha_resourcegroup_get
SCHA_RESOURCE_SETSTATUS=/usr/cluster/bin/scha_resource_setstatus
GREP=/usr/bin/grep
XGREP=/usr/xpg4/bin/grep
EGREP=/usr/bin/egrep
TAIL=/usr/bin/tail
HEAD=/usr/bin/head
CUT=/usr/bin/cut
CAT=/usr/bin/cat
SED=/usr/bin/sed
ECHO=/usr/bin/echo
TR=/usr/xpg4/bin/tr
CP=/usr/bin/cp
SLEEP=/usr/bin/sleep
WC=/usr/bin/wc
GEOPG=/usr/cluster/bin/geopg
GEOPGGET=/usr/cluster/lib/geo/lib/scgeo_protectiongroup_get
GEODGGET=/usr/cluster/lib/geo/lib/scgeo_devicegroup_get
PING=/usr/sbin/ping
RM=/usr/bin/rm

PMFADM=/usr/cluster/bin/pmfadm

define_grep_string()
{

	# Assembles the variable grep_string according to the zone node variable

	debug_message "Function: define_grep_string - Begin"
	${SET_DEBUG}
	
	if [ -n "${zonenode}" ]
	then
		grep_string="\"${1}\""
	else
		grep_string="${1}"
	fi

	debug_message "Function: define_grep_string - END"
}
	

combine_db_reprs_info()
{
	# Bring the information from the MySQL db resource and the 
	# gecontrol command together

	debug_message "Function: combine_db_reprs_info - Begin"
	${SET_DEBUG}

	# Determine the local MySQL db resource

	get_real_mys_name

	# Extract the parameters of the MySQL db resource from the ccr

	get_mysql_rs_parameters ${real_rg} ${real_rs}	

	# Initialize the variables needed for MySQL operations
	
	variables_init

	debug_message "Function: combine_db_reprs_info - End"
}
get_real_mys_name()
{
	# Gets the real MySQL databse resource name and resource group name
	# out of an unordered pair of resource and group names. 
	# This function evaluates the variables REAL_MYSQL_RESOURCE and REAL_MYSQL_RESOURCEGROUP
	# and constructs the mysql resourc adn resource group in the variables real_rs and real_rg.
	# It defines the variables zonenode, ZLOGIN, MASK and QUOTE if the MySQL database 
	# runs in a zone node, the variables above are undefined if the MySQL database runs in
	# the global zone.

	debug_message "Function: get_real_mys_name - Begin"
	${SET_DEBUG}

	rc_get_real_mys_name=1

	resource_list=$(${ECHO} ${REAL_MYSQL_RESOURCE} | ${TR} "," " ")	
	resource_group_list=$(${ECHO} ${REAL_MYSQL_RESOURCEGROUP} | ${TR} "," " ")	
	
	for resource in ${resource_list}
	do 
		for resourcegroup in ${resource_group_list}
		do
			if ${SCHA_RESOURCE_GET} -O STATUS -R ${resource} -G ${resourcegroup} >/dev/null 2>&1
			then
				rc_get_real_mys_name=0
				# remember the resource name and the rg name for usage in
				# other functions
				real_rs=${resource}
				real_rg=${resourcegroup}
				nodelist=$(${ECHO} $(${SCHA_RESOURCEGROUP_GET} -O nodelist -G ${real_rg})|${TR} " " ",")
			fi
		done
	done

	# Determine on which node the resource is on line to remember the zone name where it runs.
	# If the resource group runs or is about to run in the global zone the $zonenode, $MASK, $ZLOGIN 
	# and $QUOTE variable will be undefined.

	ZLOGIN=""
	QUOTE=""
	MASK=""
	zonenode=""
	for node in $(${ECHO} ${nodelist} |${TR} "," " ")
	do 
		state=$(${SCHA_RESOURCE_GET} -O status_node -G ${real_rg} -R ${real_rs} ${node}|${HEAD} -1)
		if [ "${state}" != "OFFLINE" ] 
		then
			zonenode=$(${ECHO} ${node}|${AWK} -F: '{print $2}')
			if [ "${MYNAME}" == "mysql_geo_control" ] && [ -n "${zonenode}" ]
			then
				ZLOGIN="/usr/sbin/zlogin ${zonenode} "
				QUOTE="\""
				MASK="\\"
			fi
		fi
	done

	debug_message "Function: get_real_mys_name - End"
	return ${rc_get_real_mys_name}
}

validate_sbp()
{
	debug_message "Function: validate_sbp - Begin"
	${SET_DEBUG}
	rc_validate_sbp=0
	
	# Check if the mandatory parameters are defined

	# Validate if the real mysql resource group is defined

	if [ -z "${REAL_MYSQL_RESOURCEGROUP}" ]
	then
   		# SCMSGS
   		# @explanation
   		# The required real mysql resource group is not specified.
   		# @user_action
   		# Make sure that the required resource group is specified.
   		scds_syslog -p daemon.error -t $(syslog_tag) -m \
			"Validate_sbp - MySQL resource group is not specified in option %s" 
			"-G"
		rc_validate_sbp=1
	else
		debug_message "Function: validate_sbp - The real mysql resource group ${REAL_MYSQL_RESOURCEGROUP} is specified"
	fi

	# Validate if the real mysql resource exists

	if [ -z "${REAL_MYSQL_RESOURCE}" ]
	then
   		# SCMSGS
   		# @explanation
   		# The required real mysql resource is not specified.
   		# @user_action
   		# Make sure that the required resource group is specified.
   		scds_syslog -p daemon.error -t $(syslog_tag) -m \
			"Validate_sbp - MySQL resource %s is not specified in option %s" \
			"-R"
		rc_validate_sbp=1
	else
		debug_message "Function: validate_sbp - The real mysql resource group ${REAL_MYSQL_RESOURCE} is specified"
	fi

	# Validate if the mysql replication resource group is defined

	if [ -z "${REPL_MYSQL_RESOURCEGROUP}" ]
	then
   		# SCMSGS
   		# @explanation
   		# The required mysql replication resource group is not specified.
   		# @user_action
   		# Make sure that the required resource group is specified.
   		scds_syslog -p daemon.error -t $(syslog_tag) -m \
			"Validate_sbp - MySQL replication resource group is not specified in option %s" \
			"-g"
		rc_validate_sbp=1
	else
		debug_message "Function: validate_sbp - The mysql replication resource group ${REPL_MYSQL_RESOURCEGROUP} is specified"
	fi

	# Validate if the mysql replication resource is defined

	if [ -z "${REPL_MYSQL_RESOURCE}" ]
	then
   		# SCMSGS
   		# @explanation
   		# The required mysql replication resource is not specified.
   		# @user_action
   		# Make sure that the required resource is specified.
   		scds_syslog -p daemon.error -t $(syslog_tag) -m \
			"Validate_sbp - MySQL replication resource is not specified in option %s" \
			"-r"
		rc_validate_sbp=1
	else
		debug_message "Function: validate_sbp - The mysql replication resource group ${REPL_MYSQL_RESOURCE} is specified"
	fi

	# Validate if the replicated component is defined

	if [ -z "${REPLICATED_COMPONENT}" ]
	then
   		# SCMSGS
   		# @explanation
   		# The required replicated component is not specified.
   		# @user_action
   		# Make sure that the replicated component is specified.
   		scds_syslog -p daemon.error -t $(syslog_tag) -m \
			"Validate_sbp - MySQL replication object is not specified in option %s" \
			"-O"
		rc_validate_sbp=1
	else
		debug_message "Function: validate_sbp - The replication object ${REPLICATED_COMPONENT} is specified"
	fi

	# Check if the real mysql resource is configured.

	if ! get_real_mys_name
	then
   		# SCMSGS
   		# @explanation
   		# The defined real MySQL resource is not configured in the specified resource group.
   		# @user_action
   		# Make sure that defined resource is configured in the real MySQL resource group.
   		scds_syslog -p daemon.error -t $(syslog_tag) -m \
			"Validate_sbp - MySQL resource %s is not configured in one of the specified resource groups %s" \
			"${REAL_MYSQL_RESOURCE}" "${REAL_MYSQL_RESOURCEGROUP}"
		rc_validate_sbp=1
	else
		debug_message "Function: validate_sbp - The real MySQL resource group ${REAL_MYSQL_RESOURCE} is configured"
	fi

	# Validate if the replication resource exists, and contains the right start command

	replrsexist=0
	if ${SCHA_RESOURCE_GET} -O STATUS -R ${REPL_MYSQL_RESOURCE} -G ${REPL_MYSQL_RESOURCEGROUP} >/dev/null 2>&1
	then
		if start_command=$(${SCHA_RESOURCE_GET} -O EXTENSION -R ${REPL_MYSQL_RESOURCE} -G ${REPL_MYSQL_RESOURCEGROUP} start_command)
		then
			if ! ${ECHO} ${start_command}|${GREP} mysql_geo_replication >/dev/null 2>&1
			then
		   		# SCMSGS
		   		# @explanation
		   		# The specified mysql replication resource is configured for something else.
		   		# @user_action
		   		# Make sure that the defined resource is either unconfigured, or is the desired MySQL replication resource.
		   		scds_syslog -p daemon.error -t $(syslog_tag) -m \
					"Validate_sbp - MySQL resource %s is configured for something else" \
					"${REPL_MYSQL_RESOURCE}" 
				rc_validate_sbp=1
			fi
		else
		   	scds_syslog -p daemon.error -t $(syslog_tag) -m \
				"Validate_sbp - MySQL resource %s is configured for something else" \
				"${REPL_MYSQL_RESOURCE}" 
			rc_validate_sbp=1
		fi
		
		replrsexist=1
	fi

	# Validate if the application resource group exists, if rgList was set in the
	# function add_application_rgs 
	
	if [ -n "${rgList}" ]
	then
		app_rgs=$(${ECHO} ${rgList} |${SED} 's/,/ /g')
		for rg in ${app_rgs}
		do
			if ! $(${SCHA_RESOURCEGROUP_GET} -G ${rg} -O AUTO_START_ON_NEW_CLUSTER>/dev/null)
			then
			   	# SCMSGS
			   	# @explanation
				# The specified application resource group is not configured.
		   		# @user_action
		   		# Make sure that the application resource group is created.
		  		scds_syslog -p daemon.error -t $(syslog_tag) -m \
					"Validate_sbp - The application resource group %s is not created" \
					"${rg}" 
				rc_validate_sbp=1
				
			else
				debug_message "Function: validate_sbp - The application resource group ${rg} is configured"
			fi
		done
	fi

	# Check if the short ping timeout is numeric.

	if ! let x=${SHORTPING}+1 >/dev/null 2>&1
	then
   		# SCMSGS
   		# @explanation
   		# The short ping timeout is not numeric
   		# @user_action
   		# Make sure that the specified short ping timeout is numeric
   		scds_syslog -p daemon.error -t $(syslog_tag) -m \
			"Validate_sbp - The short ping timeout %s is not numeric" \
			"${SHORTPING}" 
		rc_validate_sbp=1
	else
		debug_message "Function: validate_sbp - The short ping timeout is ${SHORTPING}"
	fi

	# Check if the long ping timeout is numeric.

	if ! let x=${LONGPING}+1 >/dev/null 2>&1
	then
   		# SCMSGS
   		# @explanation
   		# The long ping timeout is not numeric
   		# @user_action
   		# Make sure that the specified long ping timeout is numeric
   		scds_syslog -p daemon.error -t $(syslog_tag) -m \
			"Validate_sbp - The long ping timeout %s is not numeric" \
			"${LONGPING}" 
		rc_validate_sbp=1
	else
		debug_message "Function: validate_sbp - The long ping timeout is ${LONGPING}"
	fi

	debug_message "Function: validate_sbp - End"
	return ${rc_validate_sbp}
}

create_configuration()
{
	
	debug_message "Function: create_configuration - Begin"
	${SET_DEBUG}
	
	rc_create_configuration=0

	# Check if the real MySQL resource on this cluster is configured for replication

	combine_db_reprs_info

	if ! check_replication ${real_rg} ${real_rs}
	then
		rc_create_configuration=1
	fi
	# Create replication rg if it does not exist
	
	if ! ${CLRG} list ${REPL_MYSQL_RESOURCEGROUP}  >/dev/null 2>&1
	then

		${CLRG} create -n ${nodelist} -p RG_affinities=+++${real_rg} ${REPL_MYSQL_RESOURCEGROUP}

		if [ ${?} -ne 0 ] 
		then
   			# SCMSGS
   			# @explanation
   			# The creation of the MySQL replication resource group was unsuccessful.
   			# @user_action
   			# Consult /var/adm/messages and fix the errors outlined there.
   			scds_syslog -p daemon.error -t $(syslog_tag) -m \
				"Create_configuration - MySQL replication resource group %s creation unsuccessful" \
				"${REPL_MYSQL_RESOURCEGROUP}"
			rc_create_configuration=1
		fi

	else
		affinity_list=$(construct_rg_affinities ${REPL_MYSQL_RESOURCEGROUP} +++${real_rg})
		${CLRG} set -n ${nodelist} -p RG_affinities=${affinity_list} ${REPL_MYSQL_RESOURCEGROUP}
		if [ ${?} -ne 0 ] 
		then
	   		# SCMSGS
			# @explanation
	 		# The MySQL setting of the affinity failed for the MySQL replication rg
	 		# @user_action
	   		# Pick a different replication resource group
	   		scds_syslog -p daemon.error -t $(syslog_tag) -m \
				"Create_configuration - the affinity specification of the MySQL replication resource group %s failed" \
				"${REPL_MYSQL_RESOURCEGROUP}"
			rc_create_configuration=1
		fi
	fi

	if [ ${rc_create_configuration} -eq 0 ]
	then

		# Check if the replication rg does not contain a MySQL database resource

		reprsexist=0
		repl_rg_rslist=$(${CLRS} list -g ${REPL_MYSQL_RESOURCEGROUP} -t gds)
		if [ -n "${repl_rg_rslist}" ]
		then
			for rs in ${repl_rg_rslist}
			do
				if ${SCHA_RESOURCE_GET} -R ${rs} -G ${RESOURCEGROUP} -O extension start_command|${EGREP} "control_mysql |start_mysql" >/dev/null 2>&1
				then
					# SCMSGS
   					# @explanation
		   			# The MySQL replication resource group contains a MySQL database resource already
   					# @user_action
   					# Specify a replication resource group without MySQL database resources
   					scds_syslog -p daemon.error -t $(syslog_tag) -m \
						"Create_configuration - MySQL replication resource group %s contains a MySQL database resource" \
						"${REPL_MYSQL_RESOURCEGROUP}"
					rc_create_configuration=1
				fi
			done
		fi
		
		# Create the MySQL replication resource, modify it if it exists

		if ${SCHA_RESOURCE_GET} -R ${REPL_MYSQL_RESOURCE} -G ${REPL_MYSQL_RESOURCEGROUP} -O status >/dev/null 2>&1
		then
			action="set"
		else
			action="create"
		fi
	
		Start_command="${MYDIR}/mysql_geo_replication -R ${REPL_MYSQL_RESOURCE} -G  ${REPL_MYSQL_RESOURCEGROUP} -r ${real_rs} -g ${real_rg} start"
		Stop_command="${MYDIR}/mysql_geo_replication -R ${REPL_MYSQL_RESOURCE} -G  ${REPL_MYSQL_RESOURCEGROUP} -r ${real_rs} -g ${real_rg} stop"
		Probe_command="${MYDIR}/mysql_geo_replication -R ${REPL_MYSQL_RESOURCE} -G  ${REPL_MYSQL_RESOURCEGROUP} -r ${real_rs} -g ${real_rg} probe"
		Validate_command="${MYDIR}/mysql_geo_replication -R ${REPL_MYSQL_RESOURCE} -G  ${REPL_MYSQL_RESOURCEGROUP} -r ${real_rs} -g ${real_rg} validate"

		# There is no scds-syslog necessry for each staement, because  every error is on STDERR and 
		# will be passed to the calling geopg command.

		if [ "${action}" == "create" ]
		then
			${CLRS} create -d -t gds -g ${REPL_MYSQL_RESOURCEGROUP} -p Network_aware=false \
				-p Resource_dependencies=${real_rs} -p Stop_signal=9 \
				-p Start_command="${Start_command}" \
				-p Stop_command="${Stop_command}" \
				-p Probe_command="${Probe_command}" \
				-p Validate_command="${Validate_command}"  \
				${REPL_MYSQL_RESOURCE}
			rc_reprs=${?}
		else
			
			# Determine if the replication resource status is OK anywhere in the cluster.
			# If the status is OK we enable it again. If we find any other status, we leave it disabled.

			reprs_status=""
			reenable=0
			for node in $(${ECHO} ${nodelist}|${TR} "," " ")
			do
				reprs_status=$(${SCHA_RESOURCE_GET} -R ${REPL_MYSQL_RESOURCE} -O Status_node $node|${HEAD} -1)
				if [ "${reprs_status}" == "OK" ]
				then
					reenable=1
					break
				fi
			done

			${CLRS} disable ${REPL_MYSQL_RESOURCE}

			if [ ${?} -eq 0 ]
			then
				${CLRS} set -t gds -g ${REPL_MYSQL_RESOURCEGROUP} \
					-p Resource_dependencies=${real_rs} -p Stop_signal=9 \
					-p Start_command="${Start_command}" \
					-p Stop_command="${Stop_command}" \
					-p Probe_command="${Probe_command}" \
					-p Validate_command="${Validate_command}"  \
					${REPL_MYSQL_RESOURCE}
				rc_reprs=${?}
				if [ ${reenable} -eq 1 ] 
				then
					if ! ${CLRS} enable ${REPL_MYSQL_RESOURCE}
					then
						rc_reprs=1
					fi
				fi
			else
				rc_reprs=1
			fi
		fi

		if [ ${rc_reprs} -ne 0 ]
		then
   			# SCMSGS
			# @explanation
   			# The MySQL creation/modification of the MySQL replication resource failed
   			# @user_action
   			# Fix the errors outlined in /var/adm/messages
   			scds_syslog -p daemon.error -t $(syslog_tag) -m \
				"Create_configuration - the creation/modification of the MySQL replication resource %s failed" \
				"${REPL_MYSQL_RESOURCE}"
			rc_create_configuration=1
		else
			debug_message "Function: create_configuration replication resource created"
			
		fi

		# Determine the node where the repllication rg should go online,
		# leave it empty if the MySQL rg is offline or unmanaged.

		onlnode=

		for node in $(${ECHO} ${nodelist}|${TR} "," " ")
		do
			state=$(${SCHA_RESOURCEGROUP_GET} -G ${real_rg} -O RG_STATE_NODE ${node})
			if [ "${state}" != "OFFLINE" ] -a [ "${state}" != "UNMANAGED" ]
			then
				onlnode=${node}
			fi
		done
		
		nodeopt=
		if [ -n "${onlnode}" ]
		then
			nodeopt="-n ${onlnode}"
		fi
		
		# Manage the replication resource group

		${CLRG} online -M ${nodeopt} ${REPL_MYSQL_RESOURCEGROUP}

	fi
	
	debug_message "Function: create_configuration - End"
	return ${rc_create_configuration}
}

construct_rg_affinities()
{
	debug_message "Function: construct_rg_affinities - Begin"
	${SET_DEBUG}

	# This function accepts two parameters and prints a rg_affinitiey list
	# $1 is the resource group
	# $2 is the affinity

	rc_construct_rg_affinities=0

	# Transform the affinity string in a grepable regular expression
	
	affinity=$(${ECHO} ${2}|${TR} "-" "#"|${TR} "+" ":")

	# Get the affinity list and transform it into a grepable list

	current_aff_list=$(${ECHO} $(${SCHA_RESOURCEGROUP_GET} -G ${1} -O RG_affinities)|${TR} " " ",")
	grep_aff_list=$(${ECHO} ${current_aff_list}|${TR} "+" ":"|${TR} "-" "#")
	
	# Add the affinity to the output string, if it is not there already

	if ${ECHO} ${grep_aff_list}|${EGREP} "^${affinity},|^${affinity}$|,${affinity}$|,${affinity}," >/dev/null 2>&1
	then
		print ${current_aff_list}
	else
		print "${current_aff_list},${2}"
	fi	

	debug_message "Function: construct_rg_affinities - End"
	return ${rc_construct_rg_affinities}
}

check_replication()
{
	# Check for the geo control script if the replication is working
	# This function requires 2 parameters. 
	# $1  resource group name of the MySQL database resource group
	# $2  resource name of the MySQL database resource

	debug_message "Function: check_replication - Begin"
	${SET_DEBUG}
	rc_check_replication=0

	# Evaluate the parameters of the MySQL database resource

	get_mysql_rs_parameters ${1} ${2}

	# Initialize the variables needed for MySQL operations
	
	variables_init

	define_grep_string "^skip-slave-start$"

	# Determine if MySQL is configured to start the slave threads.
	# If it is, check if the slave is working.

	if ! ${ZLOGIN} ${XGREP} -E ${grep_string} ${MYSQL_DATADIR}/my.cnf >/dev/null 2>&1 
	then
	
		get_mysql_port
		check_mysql_slave
		if [ "${State}" != "OK" ]
		then
			# SCMSGS
			# @explanation
			# The check for the replication status failed
   			# @user_action
			# Fix the replication issues
			scds_syslog -p daemon.error -t $(syslog_tag) -m \
				"Check_replication - the check for replication failed for replication resource %s" \
			"${REPL_MYSQL_RESOURCE}"
			rc_check_replication=1
		else
			debug_message "Function: check_replication replication OK"
		fi
	fi
	debug_message "Function: check_replication - End"
	return ${rc_check_replication}
}

remove_configuration()
{ 
	debug_message "Function: remove_configuration - Begin"
	${SET_DEBUG}
	rc_remove_configuration=0

	# Delete the replication resource only if it is there.
	# If it is gone, do nothing.

	if ${CLRS} list -g ${REPL_MYSQL_RESOURCEGROUP} | ${GREP} "^${REPL_MYSQL_RESOURCE}$" >/dev/null 2>&1
	then
		debug_message "remove_configuration resource ${REPL_MYSQL_RESOURCE} was found"
		if ${CLRS} delete -F ${REPL_MYSQL_RESOURCE} >/dev/null 2>&1
		then
			debug_message "remove_configuration resource ${REPL_MYSQL_RESOURCE} deleted"
		else
			# SCMSGS
			# @explanation
			# The MySQL replication resource was not deleted
	   		# @user_action
			# Evaluate the root cause for the failed deletion and retry the operation
			scds_syslog -p daemon.error -t $(syslog_tag) -m \
				"Remove_configuration - The deletion of the MySQL replication resource %s failed" \
				"${REPL_MYSQL_RESOURCE}"
			rc_remove_configuration=1
		fi
	else
		debug_message "remove_configuration resource ${REPL_MYSQL_RESOURCE} was not found"
	fi

	# Delete the replication resource group if it is defined and empty.
	# If it is gone, do nothing

	if ${CLRS} list -g ${REPL_MYSQL_RESOURCEGROUP} >/dev/null 2>&1
	then

		debug_message "remove_configuration resource ${REPL_MYSQL_RESOURCEGROUP} was found"

		num_res_left=$(${CLRS} list -g ${REPL_MYSQL_RESOURCEGROUP}|${WC} -w)
		if [ ${num_res_left} -eq 0 ]
		then
			if ! ${CLRG} delete  ${REPL_MYSQL_RESOURCEGROUP}
			then
				# SCMSGS
   				# @explanation
		   		# The MySQL replication resource group was not deleted
   				# @user_action
   				# Evaluate the root cause for the failed deletion and retry the operation
   				scds_syslog -p daemon.error -t $(syslog_tag) -m \
					"Remove_configuration - The deletion of the MySQL replication resource group %s failed" \
					"${REPL_MYSQL_RESOURCEGROUP}"
			else
				debug_message "remove_configuration resource group ${REPL_MYSQL_RESOURCEGROUP} deleted"
			fi
		fi
	else
		debug_message "remove_configuration resource ${REPL_MYSQL_RESOURCEGROUP} was not found"
	fi

	debug_message "Function: remove_configuration - End"
	return ${rc_remove_configuration}
}

manipulate_my_cnf_param()
{
	# This function adds or remove a given parameter from a my.cnf file
	# The my.cnf file is specified by the datadir
	# $1 is the action create/remove
	# $2 is the parameter
	debug_message "Function: manipulate_my_cnf_param - Begin"
	${SET_DEBUG}
	rc_manipulate_my_cnf_param=0

	define_grep_string "^${2}"

	case $1 in
	create)
		if ! ${ZLOGIN} ${XGREP} -E ${grep_string} ${MYSQL_DATADIR}/my.cnf >/dev/null 2>&1 
		then
			command="${ZLOGIN} ${QUOTE} ${ECHO} $2 >> ${MYSQL_DATADIR}/my.cnf ${QUOTE} "
			eval ${command}
		fi;;
	remove)
		if  ${ZLOGIN} ${XGREP} -E ${grep_string} ${MYSQL_DATADIR}/my.cnf >/dev/null 2>&1 
		then
			
			${ZLOGIN} ${CP} ${MYSQL_DATADIR}/my.cnf /var/tmp/my.cnf.${REPLICATED_COMPONENT}
			command="${ZLOGIN} ${QUOTE} ${CAT} /var/tmp/my.cnf.${REPLICATED_COMPONENT}|${SED} /${2}/d > ${MYSQL_DATADIR}/my.cnf ${QUOTE}"
			eval ${command}
			${ZLOGIN} ${RM} /var/tmp/my.cnf.${REPLICATED_COMPONENT}
		fi;;
		
		
	esac
	debug_message "Function: manipulate_my_cnf_param - End"
	return ${rc_manipulate_my_cnf_param}
}
check_switchover()
{
	debug_message "Function: check_switchover - Begin"
	${SET_DEBUG}
	rc_check_switchover=0

	combine_db_reprs_info

	get_mysql_port

	# extract the slaves configuration

	get_slave_configuration
			
	# check for the connectivity of the remote database

	mask_string "show databases;"
	command="${ZLOGIN} ${QUOTE} ${MYSQL_MYSQL} -h ${Master_Host} -u ${MYSQL_FMUSER_USER} -p${MYSQL_FMUSER_PW} -B ${PORT} -e ${masked_string} ${QUOTE} > ${MYSQL_RESULTF} 2> ${MYSQL_ERRORF}"
	eval ${command}

	if [ ${?} -ne 0 ]
	then
		# SCMSGS
		# @explanation
		# The check for the remote mysql connectivity failed
   		# @user_action
		# Ensure that the remote MySQL databse is working
		scds_syslog -p daemon.error -t $(syslog_tag) -m \
			"Check_switchover - the check for remote MySQL connectivity failed for the replicated component %s" \
			"${REPLICATED_COMPONENT}"
		rc_check_switchover=1
	else
		debug_message "Function: check_switchover - check for remote connectivity successful"
	
		# Now check for the replication status

		if ! check_replication ${real_rg} ${real_rs}
		then
			# SCMSGS
			# @explanation
			# The check for the replication status failed
	   		# @user_action
			# Fix the replication issues
			scds_syslog -p daemon.error -t $(syslog_tag) -m \
				"Check_switchover - the check for switchover failed because of replication issues on the MySQL resource %s" \
				"${REAL_MYSQL_RESOURCE}"
			rc_check_switchover=1
		else
			debug_message "Function: check_switchover - successful"
		fi
	fi

	debug_message "Function: check_switchover - End"
	return ${rc_check_switchover}
}
perform_switchover()
{
	debug_message "Function: perform_switchover - Begin"
	${SET_DEBUG}
	rc_perform_switchover=0
	
	combine_db_reprs_info

	# the variable currentRole and newRole are passed and evaluated from the switchover call 

	if [ "${currentRole}" == "SECONDARY" ] &&  [ "${newRole}" == "PRIMARY" ]
	then
		if ! make_primary
		then
			rc_perform_switchover=1
			# SCMSGS
   			# @explanation
		  	# The promotion to primary for the replicated object failed on the secondary side
   			# @user_action
   			# Evaluate the root cause for the failed promotion and retry the operation
   			scds_syslog -p daemon.error -t $(syslog_tag) -m \
				"perform_switchover - the promotion to primary failed for %s" \
				"${REPLICATED_COMPONENT}"
		else

			# Serialize the slave configuration to be performed after the reset of the master.
			# It is necessary to call it from the slave, because the geo framework does not 
			# guarantee the order of perform_switchover.

			# extract the slaves configuration

			get_slave_configuration
			
			# Reset the slave to wipe out the master log file and the master log position.

			mask_string "reset slave;"
			command="${ZLOGIN} ${QUOTE} ${MYSQL_MYSQL} -h ${Master_Host} -u ${MYSQL_FMUSER_USER} -p${MYSQL_FMUSER_PW} -B ${PORT} -e ${masked_string} > ${MYSQL_RESULTF} 2> ${MYSQL_ERRORF} ${QUOTE} "
			eval ${command}
			if [ ${?} -ne 0 ]
			then
				# SCMSGS
   				# @explanation
	  			# The mysql command failed.
   				# @user_action
   				# Consult the mysql error logs to determine the root cause
   				scds_syslog -p daemon.error -t $(syslog_tag) -m \
					"%s - the sql command %s failed" \
					"perform_switchover" "${mask_string}"
				rc_perform_switchover=1
			else
				debug_message "perform_switchover the sql command ${mask_string} was successful"
			fi

			# Restore the slaves connect configuration
			
			mask_string "change master to master_user='${Master_User}', master_Password='${Master_Password}', master_port=${Master_Port}"
			command="${ZLOGIN} ${QUOTE} ${MYSQL_MYSQL} -h ${Master_Host} -u ${MYSQL_FMUSER_USER} -p${MYSQL_FMUSER_PW} -B ${PORT} -e ${masked_string} > ${MYSQL_RESULTF} 2> ${MYSQL_ERRORF}  ${QUOTE} "
			eval ${command}
			if [ ${?} -ne 0 ]
			then
   				scds_syslog -p daemon.error -t $(syslog_tag) -m \
					"%s - the sql command %s failed" \
					"perform_switchover" "${mask_string}"
				rc_perform_switchover=1
			else
				debug_message "perform_switchover the sql command ${mask_string} was successful"
			fi

			# Start the slave

			mask_string "start slave;"
			command="${ZLOGIN} ${QUOTE} ${MYSQL_MYSQL} -h ${Master_Host} -u ${MYSQL_FMUSER_USER} -p${MYSQL_FMUSER_PW} -B ${PORT} -e ${masked_string} > ${MYSQL_RESULTF} 2> ${MYSQL_ERRORF} ${QUOTE} "
			eval ${command}
			if [ ${?} -ne 0 ]
			then
   				scds_syslog -p daemon.error -t $(syslog_tag) -m \
					"%s - the sql command %s failed" \
					"perform_switchover" "${mask_string}"
				rc_perform_switchover=1
			else
				debug_message "perform_switchover the sql command ${mask_string} was successful"
			fi
		
			debug_message "perform_switchover the promotion to a primary of ${REPLICATED_COMPONENT} was successful"
		fi
	else

		# Prepare the my.cnf file to come up at the next restart as a slave and flip the current 
		# readonly setting for all sessions if required

		manipulate_my_cnf_param remove skip-slave-start
		if [ "${READONLY}" == "true" ]
		then

			# Make the readonly setting persistent

			manipulate_my_cnf_param create read-only=true

			get_mysql_port

			# Change the readonly setting for the already running database

			mask_string "set global read_only=on"
			command="${ZLOGIN} ${QUOTE} ${MYSQL_MYSQL} -h ${MYSQL_HOST} -u ${MYSQL_FMUSER_USER} -p${MYSQL_FMUSER_PW} -B ${PORT} -e ${masked_string} > ${MYSQL_RESULTF} 2> ${MYSQL_ERRORF} ${QUOTE} "
			eval ${command}
			if [ ${?} -ne 0 ]
			then
   				scds_syslog -p daemon.error -t $(syslog_tag) -m \
					"%s - the sql command %s failed" \
					"perform_switchover" "${mask_string}"
				rc_perform_switchover=1
			else
				debug_message "perform_switchover the sql command ${mask_string} was successful"
			fi
		fi
	fi

	debug_message "Function: perform_switchover - End"
	return ${rc_perform_switchover}
}
make_primary()
{
	debug_message "Function: make_primary - Begin"
	${SET_DEBUG}
	rc_make_primary=0

	get_mysql_port
	
	# stop the slaves io_thread

	mask_string "stop slave io_thread"
	command="${ZLOGIN} ${QUOTE} ${MYSQL_MYSQL} -h ${MYSQL_HOST} -u ${MYSQL_FMUSER_USER} -p${MYSQL_FMUSER_PW} -B ${PORT} -e  ${masked_string} > ${MYSQL_RESULTF} 2> ${MYSQL_ERRORF}  ${QUOTE} "
	eval ${command}
	if [ ${?} -ne 0 ]
	then 
		rc_make_primary=1
   		scds_syslog -p daemon.error -t $(syslog_tag) -m \
			"%s - the sql command %s failed" \
			"make_primary" "${mask_string}"
	else

		# wait until the remaining relay logs are processed

		mask_string "show processlist"
		command="${ZLOGIN} ${QUOTE} ${MYSQL_MYSQL} -h ${MYSQL_HOST} -u ${MYSQL_FMUSER_USER} -p${MYSQL_FMUSER_PW} -B ${PORT} -e ${masked_string} ${QUOTE} > ${MYSQL_RESULTF} 2> ${MYSQL_ERRORF}"
		eval ${command}
		if [ ${?} -eq 0 ]
		then
			${GREP} "Has read all relay log"  ${MYSQL_RESULTF} >/dev/null 2>&1 
			relay_logs_read=${?}		
		else
   			scds_syslog -p daemon.error -t $(syslog_tag) -m \
				"%s - the sql command %s failed" \
				"make_primary" "${mask_string}"
			rc_make_primary=1
		fi

		while [ ${relay_logs_read} -ne 0 ] && [ ${rc_make_primary} -eq 0 ]
		do
			eval ${command}
			if [ ${?} -eq 0 ]
			then
				${GREP} "Has read all relay log"  ${MYSQL_RESULTF} >/dev/null 2>&1 
				relay_logs_read=${?}		
			else
	   			scds_syslog -p daemon.error -t $(syslog_tag) -m \
					"%s - the sql command %s failed" \
					"make_primary" "${mask_string}"
				rc_make_primary=1
			fi
			relay_logs_read=${?}		
			${SLEEP} 1
		done

		# wait until Read_Master_Log_Pos and Exec_Master_Log_Pos are equal
		
		get_slave_configuration
		if [ ${?} -eq 0 ]
		then
			while [ "${Read_Master_Log_Pos}" != "${Exec_Master_Log_Pos}" ]
			do 
				get_slave_configuration
				if [ ${?} -ne 0 ] 
				then
					Read_Master_Log_Pos=""
					Exec_Master_Log_Pos=""
				fi
				${SLEEP} 1
			done

			# Check if the master and slave are at the same log position

			if [ -n "${Master_Log_Pos}" ]
			then
				
				while [ "${Master_Log_Pos}" != "${Exec_Master_Log_Pos}" ] && [ -n "${Master_Log_Pos}" ]
				do 
					get_slave_configuration
					if [ ${?} -ne 0 ] 
					then
						Read_Master_Log_Pos=""
						Exec_Master_Log_Pos=""
					fi
					${SLEEP} 1
				done
				
			fi
			
		fi
		
		# Finally stop the slave 

		mask_string "stop slave"
		command="${ZLOGIN} ${QUOTE} ${MYSQL_MYSQL} -h ${MYSQL_HOST} -u ${MYSQL_FMUSER_USER} -p${MYSQL_FMUSER_PW} -B ${PORT} -e ${masked_string} > ${MYSQL_RESULTF} 2> ${MYSQL_ERRORF} ${QUOTE} "
		eval ${command}
		if [ ${?} -ne 0 ]
		then
	   		scds_syslog -p daemon.error -t $(syslog_tag) -m \
				"%s - the sql command %s failed" \
				"make_primary" "${mask_string}"
			rc_make_primary=1
		fi

		# Reset the master 
	
		mask_string "reset master"
		command="${ZLOGIN} ${QUOTE} ${MYSQL_MYSQL} -h ${MYSQL_HOST} -u ${MYSQL_FMUSER_USER} -p${MYSQL_FMUSER_PW} -B ${PORT} -e ${masked_string} > ${MYSQL_RESULTF} 2> ${MYSQL_ERRORF} ${QUOTE} "
		eval ${command}
		if [ ${?} -ne 0 ]
		then
	   		scds_syslog -p daemon.error -t $(syslog_tag) -m \
				"%s - the sql command %s failed" \
				"make_primary" "${mask_string}"
			rc_make_primary=1
		fi

		# Prepare the my.cnf file to come up as a master at the next restart and flip 
		# the current readonly setting for all sessions if required

		manipulate_my_cnf_param create skip-slave-start
		if [ "${READONLY}" == "true" ]
		then
			manipulate_my_cnf_param remove read-only=true
			mask_string "set global read_only=off"
			command="${ZLOGIN} ${QUOTE} ${MYSQL_MYSQL} -h ${MYSQL_HOST} -u ${MYSQL_FMUSER_USER} -p${MYSQL_FMUSER_PW} -B ${PORT} -e ${masked_string} > ${MYSQL_RESULTF} 2> ${MYSQL_ERRORF} ${QUOTE} "
			eval ${command}
			if [ ${?} -ne 0 ]
			then
	   			scds_syslog -p daemon.error -t $(syslog_tag) -m \
					"%s - the sql command %s failed" \
					"make_primary" "${mask_string}"
				rc_make_primary=1
			fi
		fi
	fi

	debug_message "Function: make_primary - End"
	return ${rc_make_primary}
	
}

get_slave_configuration()
{
	debug_message "Function: get_slave_configuration - Begin"
	${SET_DEBUG}

	rc_get_slave_configuration=0

	# Evaluate various values from the slave status
	# The values are stored in the variables: 
	# Master_Host
	# Master_User
	# Master_Port
	# Read_Master_Log_Pos
	# Exec_Master_Log_Pos
	# Master_Log_Pos

	mask_string "show slave status\G"
	command="${ZLOGIN} ${QUOTE} ${MYSQL_MYSQL} -h ${MYSQL_HOST} -u ${MYSQL_FMUSER_USER} -p${MYSQL_FMUSER_PW} -B ${PORT} -e ${masked_string} ${QUOTE} > ${MYSQL_RESULTF} 2> ${MYSQL_ERRORF}"
	eval ${command}
	
	if [ ${?} -ne 0 ]
	then
		rc_get_slave_configuration=1
   		scds_syslog -p daemon.error -t $(syslog_tag) -m \
			"%s - the sql command %s failed" \
			"get_slave_configuration" "${mask_string}"
	else
		Master_Host=$(${GREP} Master_Host ${MYSQL_RESULTF} 2>/dev/null|${AWK} -F: '{print $2}'|${TR} -d " ")
		Master_User=$(${GREP} Master_User ${MYSQL_RESULTF} 2>/dev/null|${AWK} -F: '{print $2}'|${TR} -d " ")
		Master_Port=$(${GREP} Master_Port ${MYSQL_RESULTF} 2>/dev/null|${AWK} -F: '{print $2}'|${TR} -d " ")
		Read_Master_Log_Pos=$(${GREP} Read_Master_Log_Pos ${MYSQL_RESULTF} 2>/dev/null|${AWK} -F: '{print $2}'|${TR} -d " ")
		Exec_Master_Log_Pos=$(${GREP} Exec_Master_Log_Pos ${MYSQL_RESULTF} 2>/dev/null|${AWK} -F: '{print $2}'|${TR} -d " ")

		# If the master info file contains a binlog information like bin-log.000001, 
		# the replication_user password is at position 6 otherwise at position 5

		${ECHO} $(${ZLOGIN} ${CAT} ${MASTER_INFO}) |${AWK} '{print $2}'| ${GREP} ${MASK}"\.${MASK}" >/dev/null 2>&1 
		if [ ${?} -eq 0 ]
		then
			Master_Password=$(${ECHO} $(${ZLOGIN} ${CAT} ${MASTER_INFO})|${AWK} '{print $6}')
		else
			Master_Password=$(${ECHO} $(${ZLOGIN} ${CAT} ${MASTER_INFO})|${AWK} '{print $5}')
		fi

		Master_Log_Pos=

		# Evaluate the master log position only if a ping to the master host is successful

		if ${PING} ${Master_Host} ${SHORTPING} >/dev/null 2>&1
		then
			mask_string "show master status\G"
			command="${ZLOGIN} ${QUOTE} ${MYSQL_MYSQL} -h ${Master_Host} -u ${MYSQL_FMUSER_USER} -p${MYSQL_FMUSER_PW} -B ${PORT} -e ${masked_string} ${QUOTE} > ${MYSQL_RESULTF} 2> ${MYSQL_ERRORF} "
			eval ${command}
			if [  ${?} -eq 0 ]
			then
				Master_Log_Pos=$(${GREP} Position ${MYSQL_RESULTF} 2>/dev/null|${AWK} -F: '{print $2}'|${TR} -d " ")
			else
				# We do not throw an error message here, becoause it is a valid condition 
				# that the master host is down

				debug_message "Function: get_log_positions - Master_host is unavailable"
			fi
		fi
		
	fi
	
	debug_message "Function: get_slave_configuration - End"
	return ${rc_get_slave_configuration}
}
check_takeover()
{
	debug_message "Function: check_takeover - Begin"
	${SET_DEBUG}
	rc_check_takeover=0

	combine_db_reprs_info

	get_mysql_port

	# Extract the slaves configuration

	get_slave_configuration
			
	if ${PING} ${Master_Host} ${LONGPING} >/dev/null 2>&1
	then

		# Check for the connectivity of the remote database

		mask_string "show databases;"
		command="${ZLOGIN} ${QUOTE} ${MYSQL_MYSQL} -h ${Master_Host} -u ${MYSQL_FMUSER_USER} -p${MYSQL_FMUSER_PW} -B ${PORT} -e ${masked_string} > ${MYSQL_RESULTF} 2> ${MYSQL_ERRORF} ${QUOTE} "
		eval ${command}
		
		if [ ${?} -ne 0 ]
		then

			# Allow the further testing for the takeover only if the remote database is not available

			if check_replication  ${real_rg} ${real_rs}
			then
				# SCMSGS
				# @explanation
				# The check for the replication status is successful on a takeover
		   		# @user_action
				# Try a switchover instead
				scds_syslog -p daemon.error -t $(syslog_tag) -m \
					"Check_takeover - the check for takeover failed because the replication is still working for resource %s" \
					"${REPL_MYSQL_RESOURCE}"
				rc_check_takeover=1
			else
				debug_message "Function: check_takeover - successful the master database is unavailable"
			fi
		else
			# SCMSGS
			# @explanation
			# The check for the remote mysql connectivity succesful, takever not advisable
   			# @user_action
			# Ensure that the remote MySQL databse is down for a takeover
			scds_syslog -p daemon.error -t $(syslog_tag) -m \
				"Check_takeover - the check for remote MySQL connectivity is successful, it should be unsuccessful for the replicated object %s" \
				"${REPLICATED_COMPONENT}"
			rc_check_takeover=1
		fi
	else
		debug_message "Function: check_takeover - successful, the master host is down"
	fi
	
	debug_message "Function: check_takeover - End"
	return ${rc_check_takeover}
}

perform_takeover()
{
	debug_message "Function: perform_takeover - Begin"
	${SET_DEBUG}
	rc_perform_takeover=0

	# We take action only if the role needs to be changed.
	# if the old primary is reachable, the perform_takeover
	# will result in a noop

	if [ "${currentRole}" == "SECONDARY" ] && [ "${newRole}" == "PRIMARY" ]
	then
		combine_db_reprs_info
		
		if ! make_primary
		then
			rc_perform_takeover=1
			# SCMSGS
	   		# @explanation
		  	# The promotion to primary for the replicated object failed on the seconndary side
	   		# @user_action
	   		# Evaluate the root cause for the failed promotion and retry the operation
	   		scds_syslog -p daemon.error -t $(syslog_tag) -m \
				"perform_takeover - the promotion to primary failed for %s" \
				"${REPLICATED_COMPONENT}"
		else
			debug_message "perform_takeover the promotion to a primary of ${REPLICATED_COMPONENT} was successful"
		fi
	else
		debug_message "perform_takeover there is nothing to do for ${REPLICATED_COMPONENT} we are still primary"
	fi
	
	debug_message "Function: perform_takeover - End"
	return ${rc_perform_takeover}
}
start_replication()
{
        debug_message "Function: start_replication - Begin"
	$SET_DEBUG

	# Start the replication, but only if MySQL is configured as a slave
	# and the replication is to be started at database start
	
	rc_start_replication=0

	combine_db_reprs_info

	# Start the replication

	if [ -n "${MYSQL_SLAVE}" ]
	then
		define_grep_string "^skip-slave-start$"
		if ! ${ZLOGIN} ${XGREP} -E ${grep_string} ${MYSQL_DATADIR}/my.cnf >/dev/null 2>&1 
		then
	
			# Start the slave threads
	
			get_mysql_port
			mask_string "start slave ;"
			command="${ZLOGIN} ${QUOTE} ${MYSQL_MYSQL} -h ${MYSQL_HOST} -u ${MYSQL_FMUSER_USER} -p${MYSQL_FMUSER_PW} ${PORT} -e ${masked_string} > ${MYSQL_RESULTF} 2> ${MYSQL_ERRORF} ${QUOTE} "
			eval ${command}
	
			if [ $? -ne 0 ]; then
   				scds_syslog -p daemon.error -t $(syslog_tag) -m \
					"%s - the sql command %s failed" \
					"start_replication" "${mask_string}"
				rc_start_replication=1
			else
				debug_message "Function: start_replication - START SLAVE called"

				# Set the session to readonly if required

				if [ "${READONLY}" == "true" ]
				then
					mask_string "set global read_only=on"
					command="${ZLOGIN} ${QUOTE} ${MYSQL_MYSQL} -h ${MYSQL_HOST} -u ${MYSQL_FMUSER_USER} -p${MYSQL_FMUSER_PW} ${PORT} -e ${masked_string} > ${MYSQL_RESULTF} 2> ${MYSQL_ERRORF} ${QUOTE}"
					eval ${command}
					if [ $? -ne 0 ]; then
   						scds_syslog -p daemon.error -t $(syslog_tag) -m \
							"%s - the sql command %s failed" \
							"start_replication" "${mask_string}"
						rc_start_replication=1
					else
						debug_message "Function: start_replication - readonly session variable is set"
					fi
				fi
			fi
		else
			debug_message "Function: start_replication - do not start the slave"
		fi
	
	fi

	# Enable the MySQL replication resource

	if ${CLRS} enable ${REPL_MYSQL_RESOURCE}
	then
		debug_message "Function: start_replication - enable of ${REPL_MYSQL_RESOURCE} was successful"
	else
		# SCMSGS
		# @explanation
		# The clrs enable of the MySQL replication resource
		# was unsuccessfull
		# @user_action
		# Examine the /var/adm/messages for the additional error messages
		# and fix the root cause
		scds_syslog -p daemon.error -t $(syslog_tag) -m \
			"Function: start_mysql - clrs enable for (%s) was unsuccessful" \
			"${REPL_MYSQL_RESOURCE}"
		rc_start_replication=1
	fi
        debug_message "Function: start_replication - End"
	return ${rc_start_replication}

}
stop_replication()
{
        debug_message "Function: stop_replication - Begin"
	$SET_DEBUG

	# Stop the replication, but only if MySQL is configured as a slave
	# and the replication is to be started at database start
	
	rc_stop_replication=0

	combine_db_reprs_info

	if [ -n "${MYSQL_SLAVE}" ]
	then
		define_grep_string "^skip-slave-start$"
		if ! ${ZLOGIN} ${XGREP} -E ${grep_string} ${MYSQL_DATADIR}/my.cnf >/dev/null 2>&1 
		then
	
			# Stop the slave threads
	
			get_mysql_port
			mask_string "stop slave;"
			command="${ZLOGIN} ${QUOTE} ${MYSQL_MYSQL} -h ${MYSQL_HOST} -u ${MYSQL_FMUSER_USER} -p${MYSQL_FMUSER_PW} ${PORT} -e ${masked_string} ${QUOTE} > ${MYSQL_RESULTF} 2> ${MYSQL_ERRORF} "
			eval ${command}
	
			if [ $? -ne 0 ]; then
   				scds_syslog -p daemon.error -t $(syslog_tag) -m \
					"%s - the sql command %s failed" \
					"stop_replication" "${mask_string}"
				rc_stop_replication=1
			else
				debug_message "Function: stop_replication - STOP SLAVE called"
			fi
		else
			debug_message "Function: stop_replication - do not stop the slave"
		fi
	
	fi

	# Disable the MySQL replication resource

	if ${CLRS} disable ${REPL_MYSQL_RESOURCE}
	then
		debug_message "Function: stop_replication - disable of ${REPL_MYSQL_RESOURCE} was successful"
	else
		# SCMSGS
		# @explanation
		# The clrs disable of the MySQL replication resource
		# was unsuccessfull
		# @user_action
		# Examine the /var/adm/messages for the additional error messages
		# and fix the root cause
		scds_syslog -p daemon.error -t $(syslog_tag) -m \
			"Function: stop_mysql - clrs disable for (%s) was unsuccessful" \
			"${REPL_MYSQL_RESOURCE}"
		rc_stop_replication=1
	fi
        debug_message "Function: stop_replication - End"
	return ${rc_stop_replication}

}
add_application_rgs()
{
	debug_message "Function: add_application_rgs - Begin"
	${SET_DEBUG}
	rc_add_application_rgs=0

	# Perform the necessary checks to add the application resource groups

	if get_real_mys_name
	then

		# Make sure that every resource groups has the attribute auto_start_on_new_cluster=true

		app_rgs=$(${ECHO} ${rgList} |${SED} 's/,/ /g')
		for rg in ${app_rgs}
		do
			auto_start=$(${SCHA_RESOURCEGROUP_GET} -G ${rg} -O AUTO_START_ON_NEW_CLUSTER)
			if [ "${auto_start}" != "FALSE" ]
			then
				
				${CLRG} set -p AUTO_START_ON_NEW_CLUSTER=False ${rg}
				debug_message "Function: add_application_rgs - For application resource group ${rg} AUTO_START_ON_NEW_CLUSTER is set to False"
			else
				debug_message "Function: add_application_rgs - The application resource group ${rg} has AUTO_START_ON_NEW_CLUSTER=False"
			fi

			# Make sure, that the real mysql resource is not configured in any  of the 
			# application resource groups

			if ${CLRS} list -g ${rg}|${GREP} "^${real_rs}$" >/dev/null 2>&1
			then
				# SCMSGS
				# @explanation
				# The application resource group contains the real MySQL resource group
				# @user_action
				# Specify an application resource group that does not contain the real
				# MySQL resource
				scds_syslog -p daemon.error -t $(syslog_tag) -m \
					"Function: add_application_rgs - The application resource group %s contains the real MySQL resource %s" \
					"${rg}", "${real_rs}"
				rc_add_application_rgs=1
			else
				debug_message "Function: add_application_rgs - The application resource group ${rg} does not contain the real MySQL resource ${real_rs}"
			fi
		done
	else
		# SCMSGS
		# @explanation
		# The combination, real MySQL resources, real MySQL resource groups is not valid any more 
		# @user_action
		# Recreate the real MySQL resources in the real MySQL resource group
		scds_syslog -p daemon.error -t $(syslog_tag) -m \
			"Function: add_application_rgs - The resources %s do not exist any more in %s" \
			"${REAL_MYSQL_RESOURCE}", "${REAL_MYSQL_RESOURCEGROUP}"
		rc_add_application_rgs=1
	fi
	
	debug_message "Function: add_application_rgs - End"
	return ${rc_add_application_rgs}
}

validate_options_replication_rs()
{
	debug_message "Function: validate_options_replication - Begin"
	${SET_DEBUG}

        #
        # Ensure all options are set
        #

	rc_validate_options_replication=0

        for i in RESOURCE RESOURCEGROUP REAL_MYSQL_RESOURCE REAL_MYSQL_RESOURCEGROUP 

        do
                case $i in
                        RESOURCE)
                        if [ -z ${RESOURCE} ]; then
                                scds_syslog -p daemon.err -t $(syslog_tag) -m \
                                	"ERROR: Option -%s not set" \
                                	"R" 
                               	rc_validate_options_replication=1
                        fi;;

                        RESOURCEGROUP)
                        if [ -z ${RESOURCEGROUP} ]; then
                                scds_syslog -p daemon.err -t $(syslog_tag) -m \
                              		"ERROR: Option -%s not set" \
                                	"G" 
                                rc_validate_options_replication=1
                        fi;;

                        REAL_MYSQL_RESOURCE)
                        if [ -z ${REAL_MYSQL_RESOURCE} ]; then
                                scds_syslog -p daemon.err -t $(syslog_tag) -m \
	                                "ERROR: Option -%s not set" \
                                	"r" 
                                rc_validate_options_replication=1
                        fi;;

                        REAL_MYSQL_RESOURCEGROUP)
                        if [ -z ${REAL_MYSQL_RESOURCEGROUP} ]; then
                                scds_syslog -p daemon.err -t $(syslog_tag) -m \
	                                "ERROR: Option -%s not set" \
                                	"g" 
                                rc_validate_options_replication=1
                        fi;;

                esac
        done
	debug_message "Function: validate_options_replication - Begin"
	return ${rc_validate_options_replication}
}

validate_replication_rs() 
{
	#
	# Validate MySQL 
	#

        debug_message "Function: validate_replication_rs - Begin"
	$SET_DEBUG

	rc_validate_replication=0

	#
	# Validate the mysql resource group 
	#

	if  ! ${CLRG} list |${GREP} "^${REAL_MYSQL_RESOURCEGROUP}$" >/dev/null 2>&1
	then
   		# SCMSGS
   		# @explanation
   		# The defined real MySQL resource group (-g option) does not exist.
   		# @user_action
   		# Make sure that defined resource group exists.
   		scds_syslog -p daemon.error -t $(syslog_tag) -m \
			"Validate_replication_rs - MySQL resource group %s does not exist" \
			"${REAL_MYSQL_RESOURCEGROUP}"
		rc_validate_replication=1
	else
		debug_message "Validate_replication_rs - MySQL resource group ${REAL_MYSQL_RESOURCEGROUP} exists" 
	fi

	# Validate that the MySQL resource group is not in the mysql replication resource group

	if [ "${REAL_MYSQL_RESOURCEGROUP}" == "${RESOURCEGROUP}" ]
	then

   		# SCMSGS
   		# @explanation
   		# The defined real MySQL resource group (-G option) must not be the
		# same resource as the mysql replication resource group 
   		# @user_action
   		# Make sure that defined resource group names differ.
   		scds_syslog -p daemon.error -t $(syslog_tag) -m \
			"Validate_replication_rs - MySQL resource group %s is the same as the MySQL replication resource group %s" \
			"${REAL_MYSQL_RESOURCEGROUP}" "${RESOURCEGROUP}"
		rc_validate_replication=1
		
	else
		debug_message "Validate_replication_rs - MySQL resource group ${REAL_MYSQL_RESOURCEGROUP} differs from MySQL replication resource group ${REAL_MYSQL_RESOURCEGROUP}" 
	fi

	#
	# Validate the MySQL resource 
	#

	if  ! ${CLRS} list |${GREP} "^${REAL_MYSQL_RESOURCE}$" >/dev/null 2>&1
	then
   		# SCMSGS
   		# @explanation
   		# The defined real MySQL resource (-r option) does not exist.
   		# @user_action
   		# Make sure that defined resource exists.
   		scds_syslog -p daemon.error -t $(syslog_tag) -m \
			"Validate_replication_rs - MySQL resource %s does not exist" \
			"${REAL_MYSQL_RESOURCE}"
		rc_validate_replication=1
	else
		debug_message "Validate_replication - MySQL resource ${REAL_MYSQL_RESOURCE} exists" 
	fi

	# Validate that the MySQL resource group is not the mysql replication resource 

	if [ "${REAL_MYSQL_RESOURCE}" == "${RESOURCE}" ]
	then

   		# SCMSGS
   		# @explanation
   		# The defined real MySQL resource (-r option) must not be the
		# same resource as the MySQL replication resource -R
   		# @user_action
   		# Make sure that defined resource names differ.
   		scds_syslog -p daemon.error -t $(syslog_tag) -m \
			"Validate_replication_rs - MySQL resource %s is the same as the MySQL replication resource %s" \
			"${REAL_MYSQL_RESOURCE}" "${RESOURCE}"
		rc_validate_replication=1
		
	else
		debug_message "Validate_replication_rs - MySQL resource name ${REAL_MYSQL_RESOURCE} differs from mysql replication resource ${REAL_MYSQL_RESOURCE}" 
	fi

        debug_message "Function: validate_replication_rs - End"
	return ${rc_validate_replication}
}

get_mysql_rs_parameters()
{
        debug_message "Function: get_mysql_rs_parameters - Begin"
	$SET_DEBUG

	# usage get_mysql_rs_parameter $1 $2 $3
	# $1 should be the name of a resource group containing the MySQL database resource
	# $2 should be the name of a MySQL database resource

	# Parses the mysql resource start command to extract the command options

	mysql_options=$(${SCHA_RESOURCE_GET} -R ${2} -G ${1} \
	-O EXTENSION Start_command| ${TAIL} +2 | ${CUT} -d ' ' -f 2-$$ )

	# Translate the options into variables 

	translate_options ${mysql_options}
	
        debug_message "Function: get_mysql_rs_parameters - End"
}

translate_options()
{
        debug_message "Function: translate_options - Begin"
	$SET_DEBUG

	# Translate the relevant options of ha-mysql start command into variables
	
	while getopts 'R:G:B:D:U:H:F:L:C' opt
	do
		case "$opt" in
			B)      MYSQL_BASEDIR=${OPTARG};;
			D)      MYSQL_DATADIR=${OPTARG};;
			U)      MYSQL_USER=${OPTARG};;
			H)      MYSQL_HOST=${OPTARG};;
			F)      MYSQL_FMUSER=${OPTARG};;
			L)      MYSQL_LOGDIR=${OPTARG};;
			C)      MYSQL_CHECK="TRUE";;
		esac
	done

        debug_message "Function: translate_options - End"
}

start_mysql_replication_rs()
{
        debug_message "Function: start_mysql_replication_rs - Begin"
	$SET_DEBUG

	# Ramp up a sleep for start_timout seconds and disable the pmf monitoring

	debug_message "Function: start_mysql_replication_rs - disable pmf"
	start_timeout=$(${SCHA_RESOURCE_GET} -R ${RESOURCE} -G ${RESOURCEGROUP} -O START_TIMEOUT)
	${SLEEP} ${start_timeout}&
	${PMFADM} -s ${RESOURCEGROUP},${RESOURCE},0.svc
        debug_message "Function: start_mysql_replication_rs - End"

	return 0
}
stop_mysql_replication_rs()
{
        debug_message "Function: stop_mysql_replication_rs - Begin"
	$SET_DEBUG

	# Kill all the remaining pids under the pmf tags

	${PMFADM} -s ${RESOURCEGROUP},${RESOURCE},0.svc KILL 2>/dev/null

        debug_message "Function: stop_mysql_replication_rs - End"

	return 0
}
check_mysql_replication_rs()
{
        debug_message "Function: check_mysql_replication_rs - Begin"
	$SET_DEBUG

	# Check the MySQL slave status if the database is configured to start the slave threads

	rc_check_mysql_replication_rs=0

	if ! ${GREP} "^skip-slave-start$" ${MYSQL_DATADIR}/my.cnf >/dev/null 2>&1
	then
		get_mysql_port
		check_mysql_slave

		# Evaluate the status, and change it only if it should transfer from to something else

		curr_status=$(${SCHA_RESOURCE_GET} -R ${RESOURCE} -G ${RESOURCEGROUP} -O STATUS| ${HEAD} -1)
		curr_message=$(${SCHA_RESOURCE_GET} -R ${RESOURCE} -G ${RESOURCEGROUP} -O STATUS| ${TAIL} +2)
		
		if [ "${curr_status}" != "${State}" ]  || [ "${curr_message}" != "${Message}" ] 
		then
			if [ "${State}" == "OK" ]
			then
				# Ignore the last error message of show slave status
				Message=""
			else
				# SCMSGS
				# @explanation
				# The check for the replication status failed
   				# @user_action
				# Fix the replication issues
				scds_syslog -p daemon.error -t $(syslog_tag) -m \
					"Check__mysql_replication_rs - the check for the MySQL replication resource failed because of replication issues on replication resource %s" \
					"${RESOURCE}"
			fi
			${SCHA_RESOURCE_SETSTATUS}  -R ${RESOURCE} -G ${RESOURCEGROUP} -s ${State} -m "${Message}"
			if [ ${?} -ne 0 ]
			then
				# SCMSGS
				# @explanation
				# Couldn't set the resource status
				# @user_action
				# None
				scds_syslog -p daemon.warning -t $(syslog_tag) -m \
					"Function: check_mysql_replication - Setting the resource status returned an error" 
			fi
		else
			if [ "${State}" != "OK" ]
			then
				scds_syslog -p daemon.error -t $(syslog_tag) -m \
					"Check__mysql_replication_rs - the check for the MySQL replication resource failed because of replication issues on replication resource %s" \
					"${RESOURCE}"
			fi
			
		fi

		# Determine the return code by the value of State
		# DEGRADED=1
		# FAULTED=100

		if [ "${State}" == "DEGRADED" ]
		then
			rc_check_mysql_replication_rs=1
		elif [ "${State}" == "FAULTED" ]
		then
			rc_check_mysql_replication_rs=100
		fi
	fi
        debug_message "Function: check_mysql_replication_rs - End"
	return ${rc_check_mysql_replication_rs}
}
