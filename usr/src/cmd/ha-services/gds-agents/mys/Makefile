#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#ident	"@(#)Makefile	1.9	08/10/07 SMI"

# cmd/ha-services/gds-data/mys/Makefile
#

SUBDIRS = geocontrol

include $(SRC)/Makefile.subdirs

# list the content of the components bin directory $PKGNAME/bin

KSH_BIN_SRCS = functions.ksh \
		control_mysql.ksh \
		probe_mysql.ksh \
		probe_smf_mysql.ksh \
		start_mysql.ksh \
		stop_mysql.ksh

# list the configuration script of the components etc directory $PKGNAME/etc

CONF = config \
	my.cnf_sample_master \
	my.cnf_sample_slave

# list the ksh content of the common lib directory $PKGNAME/lib

KSH_LIB_SRCS = functions_static.ksh

# list the content of the components util directory $PKGNAME/util

KSH_UTIL_SRCS = ha_mysql_config.ksh \
		ha_mysql_register.ksh \
		ha_mysql_smf_register.ksh \
		ha_mysql_smf_remove.ksh \
		mysql_config.ksh \
		mysql_register.ksh

# definition for the ksh substitution

LIBPROG = $(KSH_LIB_SRCS:%.ksh=%)
	
UTILPROG = $(KSH_UTIL_SRCS:%.ksh=%)

# assembly of the PROG variable

PROG = $(KSH_BIN_SRCS:%.ksh=%)

include $(SRC)/cmd/Makefile.cmd

# Packaging
PKGNAME = SUNWscmys
RTRFILE = SUNW.mys

# Disable I18N stuff, the po filename needs to be unique to support parallel builds of the agents below gds-agents
POFILE= $(PKGNAME).po
PIFILES=

# Disable lint
LINTFILES=

.KEEP_STATE:

all: $(PROG) $(UTILPROG) $(LIBPROG)

.PARALLEL: $(PROG) $(UTILPROG) $(LIBPROG)

install: all $(ROOTOPTBINPROG) $(ROOTOPTETCRTR) $(ROOTOPTETCCONF) $(ROOTOPTUTILPROG) $(ROOTOPTLIBKSHPROG)

include $(SRC)/cmd/Makefile.targ

clean:
	$(RM) $(PROG) $(UTILPROG) $(LIBPROG) 
