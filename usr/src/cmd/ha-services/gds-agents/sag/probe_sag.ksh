#!/usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#ident	"@(#)probe_sag.ksh	1.5	07/08/07 SMI"

# this is needed for backwards compatibility
USE_SETTINGS_FILE=TRUE

typeset opt

while getopts 'R:G:U:D:Z' opt
do
	case "${opt}" in
		R)      RESOURCE=${OPTARG};;
		G)      RESOURCEGROUP=${OPTARG};;
		U)	USE_SETTINGS_FILE=FALSE
			User=${OPTARG};;
		D)	USE_SETTINGS_FILE=FALSE
			GATEWAY_DIR=${OPTARG};;
		Z)	;;		# kept for backwards compatibility
		*)      exit 1;;
	esac
done

. `dirname $0`/../etc/config
. `dirname $0`/functions

validate_options

debug_message "Method: `basename $0` - Begin"
$SET_DEBUG

for file in ${GATEWAY_DIR}/bin/sag_env.ksh ${GATEWAY_DIR}/bin/sag_system
do
  validate ${file}

  if [ "${rc_validate}" -ne 0 ]
  then
        debug_message "Method: `basename $0` - End (Exit 100)"
        exit 100
  fi
done

check_sag

if [ "${rc_check_command}" -ne 0 ]
then
	debug_message "Method: `basename $0` - End (Exit 100)"
	exit 100
fi

debug_message "Method: `basename $0` - End (Exit 0)"
exit 0
