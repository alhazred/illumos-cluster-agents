#!/usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#ident	"@(#)sag_register.ksh	1.6	07/08/07 SMI"

SCRGADM=/usr/cluster/bin/scrgadm

. `dirname $0`/sag_config

${SCRGADM} -a -j ${RS} -g ${RG} -t SUNW.gds \
-x Start_command="/opt/SUNWscsag/bin/start_sag \
-R ${RS} -G ${RG} -U ${SAGUSER} -D ${SAGDIR}" \
-x Stop_command="/opt/SUNWscsag/bin/stop_sag \
-R ${RS} -G ${RG} -U ${SAGUSER} -D ${SAGDIR}" \
-x Probe_command="/opt/SUNWscsag/bin/probe_sag \
-R ${RS} -G ${RG} -U ${SAGUSER} -D ${SAGDIR}" \
-x Stop_signal=9 \
-y Port_list=${PORT}/tcp \
-y Network_resources_used=${LH} \
-y Resource_dependencies=${HAS_RS}


St=$?
if [ "${St}" -ne 0 ]; then
        echo "Registration of resource ${RS} failed, please correct the wrong parameters."
        exit 1
else
        echo "Registration of resource ${RS} succeeded."
fi

# VALIDATE RESOURCE

echo "Validate resource ${RS} in resourcegroup ${RG}:"

`dirname $0`/../bin/validate_sag -R ${RS} -G ${RG} -U ${SAGUSER} -D ${SAGDIR}

St=$?
if [ "${St}" -ne 0 ]; then

        echo "Validation of resource ${RS} failed, correct the wrong parameters."
        echo "Removing resource ${RS} from the cluster configuration."

        ${SCRGADM} -r -j ${RS}

        exit 1
else
        echo "Validation of resource ${RS} succeeded."
fi

exit 0
