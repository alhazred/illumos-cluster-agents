#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)functions.ksh	1.8	09/01/26 SMI"
#

PKG=SUNWscsag
METHOD=`basename $0`
LOGFILE=/var/tmp/${RESOURCE}_logfile

SCLOGGER=/usr/cluster/lib/sc/scds_syslog
SCHA_RESOURCE_GET=/usr/cluster/bin/scha_resource_get
SCHA_RESOURCEGROUP_GET=/usr/cluster/bin/scha_resourcegroup_get
PMFADM=/usr/cluster/bin/pmfadm
TASK_COMMAND=""

debug_message()
{
        #
        # Output a debug message to syslog if required
        #

        if [ "$DEBUG" = "$RESOURCE" -o "$DEBUG" = "ALL" ]
        then
                SET_DEBUG="set -x"

                DEBUG_TEXT=$1

                scds_syslog -p daemon.debug -t $(syslog_tag) -m \
                        "%s" "$DEBUG_TEXT"
        else
                SET_DEBUG=
        fi
}

if [ \( -f /opt/${PKG}/etc/settings \) -a \( "${USE_SETTINGS_FILE}" = "TRUE" \) ]
then
	. /opt/${PKG}/etc/settings
	if [ -z "${GATEWAY_DIR}" ]; then
		debug_message "File /opt/${PKG}/etc/settings has not been ammended!"
	fi
else
	debug_message "File /opt/${PKG}/etc/settings is not getting used."
fi		

if [ \( -n "${SagUser}" \) -a \( -n "${SagPwd}" \) ]
then
	SAGCREDS="-SagUser ${SagUser} -SagPwd ${SagPwd}"
else
	SAGCREDS=""
fi

# load SAG variables
. ${GATEWAY_DIR}/bin/sag_env.ksh

preamble="su - ${User} -c "

syslog_tag()
{
        #
        # Data Service message format
        #

        $SET_DEBUG

        print "SC[${PKG:-??}.${METHOD:-??}]:${RESOURCEGROUP:-??}:${RESOURCE:-??}"
}

scds_syslog()
{
        #
        # Log a message
        #

        $SET_DEBUG

        $SCLOGGER "$@" &
}

log_message()
{
        #
        # Output a message to syslog as required
        #

        debug_message "Function: log_message - Begin"
        $SET_DEBUG

        if [ -s "${LOGFILE}" ]
        then
                PRIORITY=$1
                HEADER=$2

		# 
		# Ensure the while loop only reads a closed file
		#

		strings ${LOGFILE} > ${LOGFILE}.copy
                while read MSG_TXT
                do
                        scds_syslog -p daemon.${PRIORITY} -t $(syslog_tag) -m \
                                "%s - %s" \
                                "${HEADER}" "${MSG_TXT}"
                done < ${LOGFILE}.copy

		cat /dev/null > ${LOGFILE} > /dev/null
		cat /dev/null > ${LOGFILE}.copy
        fi

        debug_message "Function: log_message - End"
}

standard_resource_get()
{
        debug_message "Function: standard_resource_get - Begin"
        $SET_DEBUG

        PROPERTY=$1

        ${SCHA_RESOURCE_GET} -R ${RESOURCE} -G ${RESOURCEGROUP} -O ${PROPERTY}

        s1=$?

        if [ "${s1}" -ne 0 ]; then
                debug_message "standard_resource_get - Retrievment of property ${PROPERTY} returned ${s1}"
        fi

        debug_message "Function: standard_resource_get - End"
        return ${s1}
}

standard_resourcegroup_get()
{
        debug_message "Function: standard_resourcegroup_get - Begin"
        $SET_DEBUG

        PROPERTY=$1

        ${SCHA_RESOURCEGROUP_GET} -R ${RESOURCE} -G ${RESOURCEGROUP} -O ${PROPERTY}

        s1=$?

        if [ "${s1}" -ne 0 ]; then
                debug_message "standard_resource_get - Retrievment of property ${PROPERTY} returned ${s1}"
        fi

        debug_message "Function: standard_resourcegroup_get - End"

        return ${s1}
}

srm_function()
{
        USER=$1

        debug_message "Function: srm_function - Begin"
        $SET_DEBUG


        #
        # If Solaris 8 just return
        #

        if [ `/usr/bin/uname -r` = "5.8" ];
        then
                return 0
        fi

        #
        # Retrieve RESOURCE_PROJECT_NAME
        #

        RESOURCE_PROJECT_NAME=`standard_resource_get RESOURCE_PROJECT_NAME`

        #
        # Retrieve RG_PROJECT_NAME if RESOURCE_PROJECT_NAME is not set
        #

        if [ -z "${RESOURCE_PROJECT_NAME}" ]; then

                RESOURCE_PROJECT_NAME=`standard_resourcegroup_get RG_PROJECT_NAME`
        fi

        #
        # Return if no projects are defined
        #

        if [ -z "${RESOURCE_PROJECT_NAME}" ]; then
                return 0
        fi

        #
        # Validate that $USER belongs to the project defined by
        # ${RESOURCE_PROJECT_NAME}
        #

        PROJ_MEMBER=`/usr/bin/projects ${USER} | /usr/bin/grep -w ${RESOURCE_PROJECT_NAME}`

        if [ -z "${PROJ_MEMBER}" ];
        then
             scds_syslog -p daemon.error -t $(syslog_tag) -m \
                        "srm_function - The user %s does not belongs to project %s" \
                        "${USER}" "${RESOURCE_PROJECT_NAME}" 
                return 1
        else
                debug_message "srm_function - User ${USER} belongs to project ${RESOURCE_PROJECT_NAME}"
        fi

        #
        # Set TASK_COMMAND
        #

        TASK_COMMAND="/usr/bin/newtask -p ${RESOURCE_PROJECT_NAME}"

        debug_message "Function: srm_function - End"

        return 0
}

validate_options()
{
        #
        # Ensure all options are set
        #

	${SET_DEBUG}

	debug_message "Function: validate_options - Begin"

        for i in RESOURCE RESOURCEGROUP User GATEWAY_DIR
        do
                case $i in
                        RESOURCE)
                        if [ -z ${RESOURCE} ]; then
                                scds_syslog -p daemon.err -t $(syslog_tag) -m \
                                "ERROR: Option -%s not set" "R"
                                exit 1
                        fi;;

                        RESOURCEGROUP)
                        if [ -z ${RESOURCEGROUP} ]; then
                                scds_syslog -p daemon.err -t $(syslog_tag) -m \
                                "ERROR: Option -%s not set" "G"
                                exit 1
                        fi;;

			User)
			if [ -z ${User} ]; then
                                scds_syslog -p daemon.err -t $(syslog_tag) -m \
                                "ERROR: Option -%s not set" "U"
                                exit 1
                        fi;;

			GATEWAY_DIR)
			if [ -z ${GATEWAY_DIR} ]; then
                                scds_syslog -p daemon.err -t $(syslog_tag) -m \
                                "ERROR: Option -%s not set" "D"
                                exit 1
                        fi;;
                esac
        done

	debug_message "Function: validate_options - End"
}

validate()
{
	#
	# Validate sag
	#
	filename=$1
        debug_message "Function: validate - Begin"
	$SET_DEBUG

	rc_validate=0

	#
	# Validate that ${filename} exists
	#

	if [ ! -f "${filename}" ]
	then
   		scds_syslog -p daemon.err -t $(syslog_tag) -m \
			"Validate - file %s does not exist" \
			"${filename}"
		rc_validate=1
	else
		debug_message "Validate - ${filename} exists"
	fi

	#
	# More validate checks here
	#

	debug_message "Function: validate - End"
}

start_sag()
{
	#
	# Start sag
	#

        debug_message "Function: start_sag - Begin"
	$SET_DEBUG

	rc_start_command=0

	# make sure there is a pid left for pmf until start_sag() does finish
	START_TIMEOUT=`${SCHA_RESOURCE_GET} -O START_TIMEOUT -R ${RESOURCE} -G ${RESOURCEGROUP}`
	/usr/bin/sleep ${START_TIMEOUT} &

	# disable pmf tag
	if ${PMFADM} -q ${RESOURCEGROUP},${RESOURCE},0.svc
	then
		${PMFADM} -s ${RESOURCEGROUP},${RESOURCE},0.svc
	fi

	# give 95% of the START_TIMEOUT to sag_bootstrap
	MAX_SAG_START_TIMEOUT=`expr ${START_TIMEOUT} \* 95 \/ 100`

	# Starting SAG with sag_bootstrap. According to Swift this is
	# working for version 5.0 and 6.0.
	${preamble} "${GATEWAY_DIR}/bin/sag_bootstrap -sagstart -timeout ${MAX_SAG_START_TIMEOUT} start" > $LOGFILE 2>&1
	rc_start_command=$?
	if [ ${rc_start_command} != 0 ]; then
		# execution of bootstrap failed... bailing out
		debug_message "SWIFTAlliance Gateway failed to start with returncode ${rc_start_command}"
		return 100
	fi

	# depend on the probe to actually test the app running

	debug_message "Function: start_sag - End"
}

stop_sag()
{
	#
	# Stop sag
	#

        debug_message "Function: stop_sag - Begin"
	$SET_DEBUG

	rc_stop_command=0

	${preamble} "${SAG_HOME}/bin/sag_bootstrap stop" > $LOGFILE 2>&1
	rc_stop_command=$?
	if [ ${rc_stop_command} != 0 ]; then
		debug_message "SWIFTAlliance Gateway failed to stop with returncode ${rc_start_command}"
		# bootstrap returned fault : rc -> 102
		return 102
	fi

	# make sure the dummy sleep started in start_sag() did already
	# finish, otherwise kill it.
	if ${PMFADM} -q ${RESOURCEGROUP},${RESOURCE},0.svc
	then
		${PMFADM} -s ${RESOURCEGROUP},${RESOURCE},0.svc KILL 2> /dev/null
	fi

	debug_message "Function: stop_sag - End"
}

check_sag()
{
	# 
	# Probe sag
	#

        debug_message "Function: check_sag - Begin"
	$SET_DEBUG

	if pgrep -u root -f "start_sag .*-R ${RESOURCE} " >/dev/null
	then
		rc_check_command=100
		return 100
	else
		rc_check_command=0
	fi

        ################
	##
	## probing consists of a few checks
	## provided by swift to perform the checks
	##
	################
	${preamble} "${SAG_HOME}/bin/sag_system ${SAGCREDS} -- status system | grep started" >/dev/null 2>&1
	retcode=$?
	if [ ${retcode} != 0 ]; then
		debug_message "sag_system did not return status started on first check!"
		${preamble} "${SAG_HOME}/bin/sag_system ${SAGCREDS} -- status system | grep partial" >/dev/null 2>&1
		if [ $? != 0 ]; then
			debug_message "sag_system did not return status partial on second check!"
			rc_check_command=1
			return 100
		else
			# clear the state ... running ...
			debug_message "sag_system did return status partial."
		fi
	fi

	debug_message "Function: check_sag - End"
}
