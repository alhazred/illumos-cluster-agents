#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#
#ident	"@(#)sag_config.ksh	1.5	07/08/07 SMI"
#
# 
# This file will be sourced in by sag_register and the parameters
# listed below will be used.
#
# These parameters can be customized in (key=value) form
#
#        RS - name of the resource for the application
#        RG - name of the resource group containing RS
#      PORT - name of the port number
#        LH - name of the LogicalHostname SC resource
#    HAS_RS - name of the HAStoragePlus SC resource
#   SAGUSER - name of the SWIFTAlliance Gateway User Account
#    SAGDIR - path for the SWIFTAlliance Gateway installation location
#

RS=
RG=
PORT=
LH=
HAS_RS=
SAGUSER=
SAGDIR=
