#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident  "@(#)functions.ksh 1.4     09/03/26 SMI"
#

PKG=SUNWscxvm
TASK_COMMAND=""
RESOURCE_PROJECT_NAME=""
SCLOGGER=/usr/cluster/lib/sc/scds_syslog
LOGGER=/usr/bin/logger

syslog_tag()
{
	${SET_DEBUG}
	print "SC[${PKG:-??}.${METHOD:-??}]:${RESOURCEGROUP:-??}:${RESOURCE:-??}"
}

scds_syslog()
{
	if [ -f "${SCLOGGER}" ]
	then
	   ${SCLOGGER} "$@" &
	else
	   while getopts 'p:t:m' opt
	   do
		case "${opt}" in
		   t) TAG=${OPTARG};;
		   p) PRI=${OPTARG};;
		esac
	   done
	
	   shift $((${OPTIND} - 1))
	   LOG_STRING=$(/usr/bin/printf "$@")
	   ${LOGGER} -p ${PRI} -t ${TAG} ${LOG_STRING}
	fi
}

debug_message()
{
	if [ "${DEBUG}" = "${RESOURCE}" -o "${DEBUG}" = "ALL" ]
	then
	   SET_DEBUG="set -x"
	   DEBUG_TEXT=${1}

	   scds_syslog -p daemon.debug -t $(syslog_tag) -m \
	      "%s" "${DEBUG_TEXT}"
	else
	   SET_DEBUG=
	fi
}

validate()
{
	debug_message "Function: validate - Begin"
	${SET_DEBUG}

	if [ "$(/usr/bin/uname -i)" != "i86xpv" ] 
	then
	   # SCMSGS
	   # @explanation
	   # Solaris is not booted with xVM.
	   # @user_action
	   # Ensure that the default boot grub menu is set to boot
	   # Solaris xVM.
	   scds_syslog -p daemon.error -t $(syslog_tag) -m \
		"Node is not booted with xVM"

	   return 1
	fi

	if [[ "${FAILOVER_TYPE}" != "normal" && "${FAILOVER_TYPE}" != "migrate" && "${FAILOVER_TYPE}" != "migrate --live" ]]
	then
	   # SCMSGS
	   # @explanation
	   # Incorrect Failover_type specified.
	   # @user_action
	   # Ensure that Failover_type="normal"|"migrate"|"migrate --live" 
	   # is specified within /opt/SUNWscxvm/util/xvm_config.
	   scds_syslog -p daemon.error -t $(syslog_tag) -m \
		"Failover_type %s is not valid" \
		"${FAILOVER_TYPE}"

	   return 1
	fi

	if [ ! -d "${ADMIN}" ]
	then
	   # SCMSGS
	   # @explanation
	   # The agent administrative pathname does not exist.
	   # @user_action
	   # Ensure that the agent administrative pathname is specified
	   # within /opt/SUNWscxvm/util/xvm_config.
	   scds_syslog -p daemon.error -t $(syslog_tag) -m \
		"Administrative pathname not found"

	   return 1
	fi

	debug_message "Function: validate - End"
	return 0
}

start_domain()
{
	debug_message "Function: start_domain - Begin"
	${SET_DEBUG}

	typeset rc=0

	# Turn off PMF restart. Starting a domain does not leave
	# a running pid as in a classic Solaris Cluster agent.

	START_TIMEOUT=$(/usr/cluster/bin/scha_resource_get -O START_TIMEOUT \
	   -R ${RESOURCE} -G ${RESOURCEGROUP} )

	/usr/bin/sleep ${START_TIMEOUT} &
	/usr/cluster/bin/pmfadm -s ${RESOURCEGROUP},${RESOURCE},0.svc

	# Check if the domain exists.
	#
	# If the domain does not exist, we maybe starting the domain
	# on a new cluster node following a failover. As such we will
	# define the domain using the previously dumped XML file
	# located within the agent's administraative file system.
	#
	# If the domain does exist either the domain was manually
	# started or the domain was migrated or live migrated from 
	# another cluster node. As such we will use the already 
	# defined domain.
	#
	# Note that when the domain is successfully stopped the domain
	# is deleted. We do this simply to avoid the domain from 
	# being manually started on multiple cluster nodes. See
	# domain_delete() for more information.

	if /usr/bin/virsh dominfo ${DOMAIN} > /dev/null 2>&1
	then
	   debug_message "Validate - domain ${DOMAIN} exists"
	else
	   if [ -f ${ADMIN}/${RESOURCE}.xml ]
	   then
		if /usr/bin/virsh define ${ADMIN}/${RESOURCE}.xml > /dev/null 2>&1
		then
		   # SCMSGS
		   # @explanation
		   # The domain is being defined using a XML file.
		   # @user_action
		   # None, the domain is being defined using a previously defined
		   # XML file when the domain was last successfully started.
		   scds_syslog -p daemon.notice -t $(syslog_tag) -m \
			"Domain %s defined using %s/%s.xml" \
			"${DOMAIN}" "${ADMIN}" "${RESOURCE}"
		else
		   # SCMSGS
		   # @explanation
		   # The domain failed to be defined using a XML file.
		   # @user_action
		   # The comamnd /usr/bin/virsh define failed to define the domain. 
		   # Determine if you have specified the correct domain name when
		   # registering the resource.
		   scds_syslog -p daemon.error -t $(syslog_tag) -m \
			"Failed to define %s using %s/%s.xml" \
			"${DOMAIN}" "${ADMIN}" "${RESOURCE}"

		   return 1
		fi
	   else
		# SCMSGS
		# @explanation
		# The domain does not exist.
		# @user_action
		# You must ensure that the domain exists. The preferred
		# method for creating a domain is to use virt-install. 
		# Refer to the virt-install(1M) man page for further
		# information.
		scds_syslog -p daemon.error -t $(syslog_tag) -m \
		   "Domain %s does not exist" \
		   "${DOMAIN}"

		return 1
	   fi
	fi

	# Tolerate a manually started domain and a NO-OP start
	# otherwise start the domain.

	if [ -f ${ADMIN}/.noop_${RESOURCE} ]
	then
	   # SCMSGS
	   # @explanation
	   # The domain was migrated or live migrated.
	   # @user_action
	   # None required. Informational message.
	   scds_syslog -p daemon.notice -t $(syslog_tag) -m \
	      "NO-OP START being performed"

	   /usr/bin/rm ${ADMIN}/.noop_${RESOURCE}
	   debug_message "start_domain - ${ADMIN}/.noop_${RESOURCE} deleted"
	   
	elif echo $(/usr/bin/virsh domstate ${DOMAIN}) | /usr/xpg4/bin/grep -q -E "running|blocked|paused|in shutdown" > /dev/null 2>&1
	then
	   # SCMSGS
	   # @explanation
	   # The domain was manually started.
	   # @user_action
	   # None required. Informational message.
	   scds_syslog -p daemon.notice -t $(syslog_tag) -m \
	      "Domain %s was manually started" \
	      "${DOMAIN}"
	else
	   if /usr/bin/virsh start ${DOMAIN} > /dev/null 2>&1
	   then
		# SCMSGS
		# @explanation
		# The domain was started successfully.
		# @user_action
		# None required. Informational message.
		scds_syslog -p daemon.notice -t $(syslog_tag) -m \
		   "Domain %s started" \
		   "${DOMAIN}"
	   else
		# SCMSGS
		# @explanation
		# The domain failed to start.
		# @user_action
		# Check the syslog for further messages. If possible 
		# the cluster will attempt to restart the domain.
		scds_syslog -p daemon.error -t $(syslog_tag) -m \
		   "Domain %s failed to start" \
		   "${DOMAIN}"

		rc=1
	   fi
	fi

	if [ ${rc} -eq 0 ]
	then
	   # Dump the domain configuration into an XML file. This file is then 
	   # used on another cluster node to define the domain but only if the
	   # domain does not exist.

	   if /usr/bin/virsh dumpxml ${DOMAIN} > ${ADMIN}/${RESOURCE}.xml
	   then
		debug_message "start_domain - ${ADMIN}/${RESOURCE}.xml created"
	   else
		# SCMSGS
		# @explanation
		# /usr/bin/virsh dumpxml for domain failed.
		# @user_action
		# Determine why the /usr/bin/virsh dumpxml command failed.
		scds_syslog -p daemon.error -t $(syslog_tag) -m \
		   "/usr/bin/virsh dumpxml %s > %s/%s.xml failed" \
		   "${DOMAIN}" "${ADMIN}" "${RESOURCE}"
	   fi
	fi

	debug_message "Function: start_domain - End"
	return ${rc}
}

check_domain()
{
	debug_message "Function: check_domain - Begin"
	${SET_DEBUG}

	typeset rc

	if /usr/bin/pgrep -f "control_xvm start -R ${RESOURCE} " >/dev/null 2>&1
	then
	   debug_message "Function: check_domain - start program is still running "
	   rc=100
	else
	   domstate=$(/usr/bin/virsh domstate ${DOMAIN})

	   case "${domstate}" in

		# Acceptable run states

		"running"|"blocked"|"paused"|"in shutdown")

		   if [ "${#PLUGIN_PROBE}" -ne 0 ]
		   then
			if [ -x "$(echo ${PLUGIN_PROBE} | /usr/bin/awk '{print $1}')" ]
			then
			   PROBE_TIMEOUT=$(/usr/cluster/bin/scha_resource_get -O Extension \
				-R ${RESOURCE} -G ${RESOURCEGROUP} Probe_timeout)

			   PROBE_TIMEOUT=$(/usr/bin/echo ${PROBE_TIMEOUT} | /usr/bin/awk '{print $2}')

			   # Run the supplied probe with only 90% of PROBE_TIMEOUT. Also note that this
			   # is supplied as a parameter to the PLUGIN_PROBE.

			   HATIMERUN_TIMEOUT=$(/usr/bin/expr ${PROBE_TIMEOUT} \* 90 \/ 100)

			   output=$(/usr/cluster/bin/hatimerun -t ${HATIMERUN_TIMEOUT} -k 9 ${PLUGIN_PROBE} ${HATIMERUN_TIMEOUT})
			   rc=$?

			   case ${rc} in
				0)	debug_message "check_domain - ${DOMAIN} ${output}"
					rc=0
					;;
				99)
					# SCMSGS
					# @explanation
					# The domain probe timed out.
					# @user_action
					# Ensure that ${PLUGIN_PROBE} can complete within
					# 90% of PROBE_TIMEOUT. 
					scds_syslog -p daemon.error -t $(syslog_tag) -m \
					   "%s did not complete within %s seconds" \
					   "${PLUGIN_PROBE}" "${HATIMERUN_TIMEOUT}"
				
					rc=100
					;;
				100)	if /usr/bin/pgrep -f "gds_svc_start .*-R ${RESOURCE} " >/dev/null 2>&1
					then
					   debug_message "check_domain - ${DOMAIN} is still starting"
					   rc=100
					elif /usr/bin/pgrep -f "gds_svc_stop .*-R ${RESOURCE} " >/dev/null 2>&1
					then
					   debug_message "check_domain - ${DOMAIN} is stopping"
					   rc=100
					else
					   # SCMSGS
					   # @explanation
					   # The domain probe has requested a domain restart.
					   # @user_action
					   # None. A domain restart will be attempted.
					   scds_syslog -p daemon.error -t $(syslog_tag) -m \
						"% has requested a domain restart %s" \
						"${PLUGIN_PROBE}" "${output}"

					   rc=100
					fi
					;;
				201)	if /usr/bin/pgrep -f "gds_svc_start .*-R ${RESOURCE} " >/dev/null 2>&1
					then
					   debug_message "check_domain - ${DOMAIN} is still starting"
					   rc=100
					elif /usr/bin/pgrep -f "gds_svc_stop .*-R ${RESOURCE} " >/dev/null 2>&1
					then
					   debug_message "check_domain - ${DOMAIN} is stopping"
					   rc=100
					else
					   # SCMSGS
					   # @explanation
					   # The domain has requested an immediate failover.
					   # @user_action
					   # None. The domain will be immediately failed over.
					   scds_syslog -p daemon.error -t $(syslog_tag) -m \
						"%s has requested an immediate failover" \
						"${PLUGIN_PROBE}"

					   rc=201
					fi
					;;
				*)	
					# SCMSGS
					# @explanation
					# ${PLUGIN_PROBE} did not return 0, 100 or 201.
					# @user_action
					# None. A domain restart will be attempted.
					scds_syslog -p daemon.error -t $(syslog_tag) -m \
					   "%s did not return 0, 100 or 201, a domain restart will be attempted" \
					   "${PLUGIN_PROBE}"
					rc=100
					;;
			   esac
			else
			   # SCMSGS
			   # @explanation
			   # ${PLUGIN_PROBE} does not exist or is not executable.
			   # @user_action
			   # Check the pathname exists and that ${PLUGIN_PROBE} is executable.
			   scds_syslog -p daemon.error -t $(syslog_tag) -m \
				"%s non-existent executable" \
				"${PLUGIN_PROBE}"					
			
			   rc=0
			fi
		   else
	 		rc=0
		   fi

			;;
			   
		# Restartable run states

		"shut off"|"crashed")

			rc=100
			;;

		# Unknown run states
	
		*)		
			rc=100
			;;
	   esac

	   debug_message "check_domain - ${DOMAIN} ${domstate}"

	fi

	debug_message "Function: check_domain - End"
	return ${rc}
}

stop_domain()
{
	debug_message "Function: stop_domain - Begin"
	${SET_DEBUG}

	STOP_TIMEOUT=$(/usr/cluster/bin/scha_resource_get -O STOP_TIMEOUT \
	   -R ${RESOURCE} -G ${RESOURCEGROUP} )

	# Note that GDS will attempt to cleanup after 80% of STOP_TIMEOUT
	# has been consumed.  In this regard, we only allocate a combined 
	# 75% of STOP_TIMEOUT to MAX_MIGRATE_TIMEOUT and MAX_STOP_TIMEOUT.
	# 
	# This leaves 5% for domain_destroy() which maybe called if 
	# domain_shutdown() exeecds it's timeout and finally domain_delete().

	MAX_MIGRATE_TIMEOUT=$(/usr/bin/expr ${STOP_TIMEOUT} \* 25 \/ 100)
	MAX_STOP_TIMEOUT=$(/usr/bin/expr ${STOP_TIMEOUT} \* 50 \/ 100)
	SECONDS=0

	# At resource creation, the administrator can determine the Failover_type.
	# Valid values for Failover_type are 
	#
	# Failover_type="normal"
	#   o Stop the resource (shutdown the domain)
	#   o Failover the resource group from the source node to the target node
	#   o Start the resource (start the domain)
	#
	# Failover_type="migrate"
	#   o Suspend the domain on the source node
	#   o Copy the domain's memory pages from the source node to the target node
	#   o Resume the domain on the target node
	#    
	# Failover_type="migrate --live"
	#   o Iteratively copy the domain's memory pages from the source node to the taregt node
	#   o When pre-copy is no longer benefical, suspend the domain on the source node
	#   o Copy the domain's remaning "dirty" pages from the source node to the taregt node
	#   o Resume the domain on the target node
	#
	# Note that migraation or live migration is performed over the cluster interconnect.
	#
	# For migration or live migration to be attempted across Solaris Cluster xVM nodes 
	# the following conditions must be met.
	#
	# - The target Solaris Cluster xVM node must be running the same xVM version.
	#
	# - The migration TCP port must be open and accepting connections from the source
	#    Solaris Cluster xVM node.
	#
	# - There must be sufficient resources for the domain to run in.
	#
	# - If the conditions are met and migration or live migration is successful a NO-OP 
	# STOP and START is performed. This will ensure a successful STOP and START to the 
	# appropriate RGM callback methods. Furthermore, doing a NO-OP RGM failover will
	# ensure that RGM subsequently actions any dependencies and that Solaris Cluster
	# reflects the correct state and status of resource groups and resources.
	#
	# - If the conditions are met but migration or live migration is not successful a
	# normal failover will be performed.
	#
	# - If the conditions are not met, migration or live migration will fail and a normal 
	# failover will be performed. 
	#
	# However, before attempting a migration or live migration we need to determine if the
	# resource is being disabled. To distinguish if the resource is being disabled we
	# test the ON_OFF_SWITCH property of the resource.
	#
	# If the resource is being disabled the ON_OFF_SWITCH will be DISABLED before the STOP
	# method is called. So, conversely if the ON_OFF_SWITCH is ENABLED the resource is not
	# being disabled and instead the resource group is undergoing either a switch to
	# another node or is being evacuated from the node. 
	#
	# - If the resource is being disabled we perform a normal shutdown, regardless of the
	# Failover_type setting. 

	ON_OFF_SWITCH=$(/usr/cluster/bin/scha_resource_get -O ON_OFF_SWITCH -R ${RESOURCE} -G ${RESOURCEGROUP})

	debug_message "stop_domain - ON_OFF_SWITCH=${ON_OFF_SWITCH}"
	debug_message "stop_domain - FAILOVER_TYPE=${FAILOVER_TYPE}"

	if [ "${ON_OFF_SWITCH}" = "DISABLED" ]
	then
	   domain_shutdown
	else
	   case "${FAILOVER_TYPE}" in
		normal)		domain_shutdown
				rc=$?
				;;
		migrate*)	if ! domain_migrate
				then
				   domain_shutdown
				fi
				rc=$?
				;;
		*)		
				# SCMSGS
				# @explanation
				# Invalid Failover_type specified.
				# @user_action
				# Delete and reregister the resource with 
				# a valid Failover_type entry.
				scds_syslog -p daemon.error -t $(syslog_tag) -m \
				   "Invalid Failover_type=%s" \
				   "${FAILOVER_TYPE}"
				rc=1
				;;
	   esac
	fi
	
	debug_message "Function: stop_domain - End"
	return ${rc}
}

get_target_host()
{
	debug_message "Function: get_target_host - Begin"
	${SET_DEBUG}

	typeset rc=1

	# Here, we need to determine the target host as the resource group is either being
	# switched or the node, where the resoure grouop is online, is being evacuated.
	#
	# To determine the target host for a resource group switch we rely on the cluster
	# command log file /var/cluster/logs/commandlog to supply the target host. We need to
	# obtain the correct entry from the command log file and match against the following
	#
	# 	<date> + ${RESOURCEGROUP} + "START" + "switch"
	#
	# after which we only save the nodename from a clrg or scswitch command. 
	#
	# Sample /var/cluster/log/commandlog output is as follows,
	#
	# 02/07/2008 08:45:13 pelko1 10548 root START - scswitch -z -g "xvm2-rg" -h "pelko2"
	# 02/07/2008 08:45:38 pelko1 10548 root END 0
	# 02/07/2008 09:01:35 pelko1 10874 root START - clrg "switch" -n "pelko1" "xvm2-rg"
	# 02/07/2008 09:01:36 pelko1 10874 root END -20827641
	#
	# If we are unable to match an entry, as perhaps the entry was logged at <date>
	# and we are checking at <date> + 1 second, i.e. we are checking just as the second
	# entry is incrementing to the next second, we perform another check. In fact the
	# last 10 seconds are checked from the commandlog. 
	#
	# Once we have matched an entry from /var/cluster/logs/commandlog, we verify that 
	# the target host is a valid nodelist entry for the resource group. 
	#
	# - If we have a valid nodelist entry we then determine that target host's cluster
	# interconnect hostname to perform the migration or live migration.
	#
	# - If we are unable to find a match for a switch, we need to consider that an evacuate
	# node is being performed. However, if the node is being evacuated we will rely on 
	# RGM to dertermine the nodename regardless if a mirgation or live migration was 
	# requested. Subsequently, we perform a normal failover. This ensures that we do not
	# migrate or live migrate the domain to a node that maybe different to the node
	# selected by RGM. 
	#
	# So, suffice to say that if a "switch" match is not found, following the discovery
	# that the resource is not just being disabled, and that a migrate or live migrate 
	# was defined, we will always perform a normal failover.
	#
	# Note that the target host match is performed within check_commandlog().

	check_commandlog

	debug_message "get_target_host - ${TARGET_HOST} size=${#TARGET_HOST}"

	if [ "${#TARGET_HOST}" -eq 0 ]
	then
	   # SCMSGS
	   # @explanation
	   # A target host was not found
	   # @user_action
	   # None required. The domain will not be migrated or live
	   # migrated instead a normal failover will be performed.
	   scds_syslog -p daemon.notice -t $(syslog_tag) -m \
		"Target host not found, normal failover will be performed"

	elif [ ${TARGET_HOST} = "$(/usr/bin/uname -n)" ] || [ $(echo ${TARGET_HOST} | /usr/bin/grep [0-9]:global) ] 
	then
	   # SCMSGS
	   # @explanation
	   # The node is being evecuated.
	   # @user_action
	   # None required. The domain will not be migrated or live
	   # migrated instead a normal failover will be performed.
	   scds_syslog -p daemon.notice -t $(syslog_tag) -m \
		"Node is being evacuated, normal failover will be performed"

	else
	   for i in $(/usr/cluster/bin/scha_resourcegroup_get -O NODELIST -G ${RESOURCEGROUP})
	   do
		[[ "${i}" != "$(uname -n)" || "${i}" = "${TARGET_HOST}" ]] && rc=0 && break
	   done

	   if [ "${rc}" -eq 0 ]
	   then
		PRIVATELINK_TARGET_HOST=$(/usr/cluster/bin/scha_cluster_get -O PRIVATELINK_HOSTNAME_NODE ${TARGET_HOST})
		debug_message "get_target_host - PRIVATELINK_TARGET_HOST=${PRIVATELINK_TARGET_HOST}"
	   else
		# SCMSGS
		# @explanation
		# The target host found in the command log file is not
		# a valid entry within the resource groups nodelist.
		# @user_action
		# None required. The domain will not be migrated or live
		# migrated instead a normal failover will be performed.
		scds_syslog -p daemon.notice -t $(syslog_tag) -m \
		   "Target host %s not matched with the resource group nodelist, normal failover will be performed" \
		   "${TARGET_HOST}"
	   fi
	fi

	debug_message "Function: get_target_host - End"
	return ${rc}
}

check_commandlog()
{
	debug_message "Function: check_commandlog - Begin"
	
	# Get the current epoch time
	typeset ETIME=$(/usr/bin/perl -e 'print time;')
	typeset DATE=$(/usr/bin/date '+%m/%d/%Y')
	i=10

	while (( $i > 0 ))
	do
	   # Iteratively search the commandlog for a switch or evacuate, going back in time
	   # by one second each time. If a match is found we break out of the loop.
	   #
	   # The following may help to understand the iterative loop. 
	   #
	   # bash-3.2# ETIME=$(perl -e 'print time;')
	   # bash-3.2# echo $ETIME
	   # 1202814041
	   # bash-3.2# HHMMSS=$(echo "0t${ETIME}=Y" | /usr/bin/mdb | awk '{print $4}')
	   # bash-3.2# echo $HHMMSS
	   # 03:00:41
	   # bash-3.2# ETIME=$(expr ${ETIME} - 1)
	   # bash-3.2# echo $ETIME
	   # 1202814040
	   # bash-3.2# HHMMSS=$(echo "0t${ETIME}=Y" | /usr/bin/mdb | awk '{print $4}')
	   # bash-3.2# echo $HHMMSS
	   # 03:00:40
	   # bash-3.2#

	   # Convert the epoch time into a readable format
	   HHMMSS=$(echo "0t${ETIME}=Y" | /usr/bin/mdb | /usr/bin/awk '{print $4}')

	   debug_message "check_commadlog - performed for ${DATE} ${HHMMSS}"

	   # Check for a clrg switch or scswitch 
	   TARGET_HOST=$(/usr/bin/grep "${DATE} ${HHMMSS}" /var/cluster/logs/commandlog |\
		/usr/bin/grep -w START | /usr/bin/grep switch | /usr/bin/grep \"${RESOURCEGROUP}\" |\
		/usr/bin/sed -e 's/^.*-h //' -e 's/^.*-n //' | /usr/bin/awk '{print $1}' | /usr/xpg4/bin/tr -d '" ')

	   [ "${#TARGET_HOST}" -ne 0 ] && break

	   # Check for a clrg evacuate 
	   TARGET_HOST=$(/usr/bin/grep "${DATE} ${HHMMSS}" /var/cluster/logs/commandlog |\
		/usr/bin/grep -w START | /usr/bin/grep evacuate |\
		 /usr/bin/sed -e 's/^.*-n //' | /usr/bin/awk '{print $1}' | /usr/xpg4/bin/tr -d '+" ' )

	   [ "${#TARGET_HOST}" -ne 0 ] && break

	   # Check for a scswitch -S
	   TARGET_HOST=$(/usr/bin/grep "${DATE} ${HHMMSS}" /var/cluster/logs/commandlog |\
		/usr/bin/grep -w START | /usr/bin/grep scswitch | /usr/bin/grep "\-S" |\
		/usr/bin/sed -e 's/^.*-h //' | /usr/bin/awk '{print $1}' | /usr/xpg4/bin/tr -d '\-SK" ' )

	   [ "${#TARGET_HOST}" -ne 0 ] && break

	   i=$(expr $i - 1)
	   ETIME=$(expr ${ETIME} - 1)
	done

	debug_message "check_commandlog - TARGET_HOST=${TARGET_HOST}"

	debug_message "Function: check_commandlog - End"
}

domain_migrate()
{
	debug_message "Function: domain_migrate - Begin"
	${SET_DEBUG}

	typeset rc

	[ "${FAILOVER_TYPE}" = "migrate" ] && MSG="migrated"
	[ "${FAILOVER_TYPE}" = "migrate --live" ] && MSG="live migrated"

	if get_target_host 
	then
	   # SCMSGS
	   # @explanation
	   # The domain is being migrated or live migrated to the target host.
	   # @user_action
	   # None required. 
	   scds_syslog -p daemon.notice -t $(syslog_tag) -m \
		"Domain %s is being %s to %s" \
		"${DOMAIN}" "${MSG}" "${TARGET_HOST}"

	   debug_message "domain_migrate - Running /usr/sbin/xm ${FAILOVER_TYPE} ${DOMAIN} ${PRIVATELINK_TARGET_HOST}"

	   /usr/cluster/bin/hatimerun -t ${MAX_MIGRATE_TIMEOUT} -k KILL \
		/usr/sbin/xm ${FAILOVER_TYPE} "${DOMAIN}" ${PRIVATELINK_TARGET_HOST} > /dev/null 2>&1
	   rc=$?

	   if [ "${rc}" -eq 0 ]
	   then
		# SCMSGS
		# @explanation
		# The domain was migrated or live migrated to the target host.
		# @user_action
		# None required. The domain successfully migrated or live migrated 
		# from the source node to the target node.
		scds_syslog -p daemon.notice -t $(syslog_tag) -m \
		   "Domain %s successfully %s to %s" \
		   "${DOMAIN}" "${MSG}" "${TARGET_HOST}"

		# As the domain has been successfully migrated or live migrated
		# we need to indicate a successful stop by performing a NO-OP stop
		# and subsequently a successful start by performing a NO-OP start.
		
		touch ${ADMIN}/.noop_${RESOURCE}
		debug_message "domain_migrate - ${ADMIN}/.noop_${RESOURCE} created"

		# SCMSGS
		# @explanation
		# The domain was migrated or live migrated.
		# @user_action
		# None required. Informational message.
		scds_syslog -p daemon.notice -t $(syslog_tag) -m \
		   "NO-OP STOP being performed" 

	   elif [ "${rc}" -eq 99 ]
	   then
		# SCMSGS
		# @explanation
		# The domain migration or live migration timed out.
		# @user_action
		# None required. Informational message.
		scds_syslog -p daemon.notice -t $(syslog_tag) -m \
		   "Migration of domain %s timed out, the domain state is now shut off" \
		   "${DOMAIN}" 

		rc=1
	   else
		# SCMSGS
		# @explanation
		# The domain failed to migrate or live migrate to the target host.
		# @user_action
		# None required. The domain failed to migrate or live migrate 
		# from the source node to the target node. A normal failover
		# will be performed.
		scds_syslog -p daemon.notice -t $(syslog_tag) -m \
		   "Domain %s failed to %s to %s, normal failover will be performed" \
		   "${DOMAIN}" "${MSG}" "${TARGET_HOST}" 

		rc=1
	   fi
	else
	   rc=1
	fi

	# If the domain has successfully migrated, we will now delete the domain. 
	#
	# Doing this ensures that the domain is only defined and able to be started
	# on one cluster node at a time. Domains can use shared storage between cluster
	# nodes so it is very important that we prevent any data corruption if a domain
	# gets manually started on multiple cluster nodes where shared storage is used. 
	#
	# Of course using SUNW.HAStoragePlus somewhat protects against this, however we 
	# simply want to avoid any manual administrative errors performed by mistake.
	#
	# Note, unless the domain was migrated or live migrated, the domain is defined
	# before startup using a previously dumped XML file for the administrative file
	# system.

	[ "${rc}" -eq 0 ] && domain_delete

	debug_message "Function: domain_migrate - End"
	return ${rc}
}
	
domain_shutdown()
{
	debug_message "Function: domain_shutdown - Begin"
	${SET_DEBUG}

	typeset rc

	# Corordinate with the domain OS to perform a graceful shutdown.
	# Note that the virsh shutdown command returns before the domain
	# has shutdown, as such we do not use hatimerun.

	if /usr/bin/virsh shutdown ${DOMAIN} > /dev/null 2>&1
	then

	   # Loop to test if the domain shuts down gracefully
	   # or if the shutdown time is exceeded.

	   while [ "${SECONDS}" -lt "${MAX_STOP_TIMEOUT}" ]
	   do
		if echo $(/usr/bin/virsh domstate ${DOMAIN}) | /usr/xpg4/bin/grep -q -E "running|blocked|paused|in shutdown"
		then
		   /usr/bin/sleep 5
		else
		   SECONDS=${MAX_STOP_TIMEOUT}
		fi
	   done

	   if echo $(/usr/bin/virsh domstate ${DOMAIN}) | /usr/xpg4/bin/grep -q -E "running|blocked|paused|in shutdown"
	   then
		# SCMSGS
		# @explanation
		# The domain failed to shutdown gracefully.
		# @user_action
		# None required. The domain failed to shutdown 
		# gracefully and will now be immediately terminated.
		scds_syslog -p daemon.notice -t $(syslog_tag) -m \
		   "Domain %s failed to shutdown gracefully, immediate shutdown will now be performed" \
		   "${DOMAIN}"

		domain_destroy
		rc=$?
	   else
		# SCMSGS
		# @explanation
		# The domain was shutdown gracefully.
		# @user_action
		# None required. The domain has shutdown gracefully.
		scds_syslog -p daemon.info -t $(syslog_tag) -m \
		   "Domain %s has been gracefully shutdown" \
		   "${DOMAIN}"
		rc=0
	   fi

	else
	   # SCMSGS
	   # @explanation
	   # The /usr/bin/virsh shutdown command failed.
	   # @user_action
	   # None required. The domain will now be terminated immediately.
	   scds_syslog -p daemon.notice -t $(syslog_tag) -m \
		"/usr/bin/virsh shutdown %s failed, immediate shutdown will now be performed" \
		"${DOMAIN}"

	   domain_destroy
	   rc=$?
	fi

	# If the domain has successfully shutdown, we will now delete the domain.
	#
	# Doing this ensures that the domain is only defined and able to be started
	# on one cluster node at a time. Domains can use shared storage between cluster
	# nodes so it is very important that we prevent any data corruption if a domain
	# gets manually started on multiple cluster nodes where shared storage is used. 
	#
	# Of course using SUNW.HAStoragePlus somewhat protects against this, however we 
	# simply want to avoid any manual administrative errors performed by mistake.
	#
	# Note, unless the domain was migrated or live migrated, the domain is defined
	# before startup using a previously dumped XML file for the administrative file
	# system.

	[ "${rc}" -eq 0 ] && domain_delete

	debug_message "Function: domain_shutdown - End"
	return ${rc}
}

domain_destroy()
{
	debug_message "Function: domain_destroy - Begin"
	${SET_DEBUG}

	typeset rc

	if /usr/bin/virsh destroy ${DOMAIN} > /dev/null 2>&1
	then
	   # SCMSGS
	   # @explanation
	   # The domain was immediately terminated.
	   # @user_action
	   # None required. The domain had previously failed to shutdown
	   # gracefully but has now been immediately terminated.
	   scds_syslog -p daemon.notice -t $(syslog_tag) -m \
		"Domain %s has been immediately terminated" \
		"${DOMAIN}"
	   rc=0
	else
	   # SCMSGS
	   # @explanation
	   # The /usr/bin/virsh destroy command failed.
	   # @user_action
	   # Determine why it was not possible to immediately terminate
	   # the domain. 
	   scds_syslog -p daemon.error -t $(syslog_tag) -m \
		"Domain %s failed to shutdown immediately" \
		"${DOMAIN}"
	   rc=1
	fi

	debug_message "Function: domain_destroy - End"
 	return ${rc}
}

domain_delete()
{
	debug_message "Function: domain_delete - Begin"
	${SET_DEBUG}

	# The purpose of deleting the domain after shutdown is to avoid the possibility of 
	# someone manually starting the domain on a different node. Doing so would compromise 
	# the domain if shared storage was used for the domain. The domain's configuration
	# is always dumped to the agent's administrative file system so that the domain can 
	# be defined before startup.

	typeset rc

	if /usr/sbin/xm delete ${DOMAIN} > /dev/null 2>&1
	then
	   # SCMSGS
	   # @explanation
	   # The domain was deleted.
	   # @user_action
	   # None required. The domain has been deleted as it 
	   # will be defined on another node. Deleting the domain
	   # on this node ensures that it can't be started on
	   # more than one cluster node at a time. 
	   scds_syslog -p daemon.notice -t $(syslog_tag) -m \
		"Domain %s has been deleted on this node" \
		"${DOMAIN}"
	   rc=0
	else
	   # SCMSGS
	   # @explanation
	   # The /usr/sbin/xm delete command failed.
	   # @user_action
	   # Determine why it was not possible to delete the domain.
	   scds_syslog -p daemon.error -t $(syslog_tag) -m \
		"Failed to delete domain %s on this node" \
		"${DOMAIN}"
	   rc=1
	fi

	debug_message "Function: domain_delete - End"
}
