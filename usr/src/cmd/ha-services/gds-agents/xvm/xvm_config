#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident  "@(#)xvm_config 1.3     09/02/19 SMI"
#
#
# This file will be sourced in by xvm_register and the parameters
# listed below will be used.
#
# These parameters can be customized in (key=value) form
#
#               RS - name of the resource for the xVM guest domain (required)
#
#               RG - name of the resource group to contain RS (required)
#
#           HAS_RS - name of the HAStoragePlus resource (optional)
#
#            LH_RS - name of the Logical Hostname resource (optional)
#
#		If you want to protect against all physical interfaces failing within
#		an IPMP group then you must define a Logical Hostname resource and 
#		specify that resource here.
#
#           DOMAIN - name of the xVM guest domain (required)
#
#     PLUGIN_PROBE - Script or command to check the guest domain (optional)
#
#		The optional parameter, PLUGIN_PROBE, provides the ability to check that the
#		  domain has reached a working state or milestone. This script or command is
#		  run under hatimerun conditions, meaning that if the script or command exceeds
#		  the hatimerun timeout then hatimerun will kill the script or command. The 
#		  hatimerun timeout is derived from PROBE_TIMEOUT and represents 90% of
#		  PROBE_TIMEOUT.
#
#		The script or command must return one of the values below, else 100 will be assumed.
#
#		  0 - The domain is working
#		100 - The domain should be restarted 
#		201 - The domain has requested an immediate failover
#
#		Note: That return code 201, requires that this resource 
#		  has an appropriate extension property value for
#		  FAILOVER_MODE and FAILOVER_ENABLED=TRUE
#
#		For FAILOVER_MODE refer to the r_properties(5) man page.
#
#		While you are free to code your own probe, the following probe is provided
#		   within /opt/SUNWscxvm/bin. 
#
#		ppkssh - This probe uses a one-way private/public key ssh authorization 
#		   between Domain-0 and the guest domain. 
#		   
#		   Specifically, ppkssh requires the following input parameters in the form of
#
#		   -P USER:IDFILE:HOST:SERVICE:STATE
#
#			- Guest domain's login name
#			- SSH identity file to use
#			- "runlevel" or SMF service name 
#			- Guest domain's hostname
#			- runlevel number or "online"
#
#		   for example, 
#
#	PLUGIN_PROBE="/opt/SUNWscxvm/bin/ppkssh -P fmuser:/export/fmuser/.ssh/id_dsa:vzodio2a:runlevel:3"
#	PLUGIN_PROBE="/opt/SUNWscxvm/bin/ppkssh -P fmuser:/export/fmuser/.ssh/id_dsa:vzodio1a:multi-user-server:online"
#
#		Note: As mentioned above, the PLUGIN_PROBE is run under hatimerun conditions.  In this regard
#		   you should be aware that a timeout value is passed to the PLUGIN_PROBE command or script, for example
#		   
#		   /usr/cluster/bin/hatimerun -t ${HATIMERUN_TIMEOUT} -k 9 ${PLUGIN_PROBE} ${HATIMERUN_TIMEOUT}
#
#		${HATIMERUN_TIMEOUT} represents 90% of PROBE_TIMEOUT, and within your PLUGIN_PROBE command or script you 
#		   can either use ${HATIMERUN_TIMEOUT} or ignore it, i.e. you just need to be aware that it is being passed
#		   to your PLUGIN_PROBE command or script.
#
#         FAILOVER - type of failover to perform (required)
#
#		valid entry is "normal"|"migrate"|"migrate --live"
#		e.g.
#	
# 		Failover="normal"
#		  o Stop the resource (shutdown the domain)
#		  o Failover the resource group from the source node to the target node
#		  o Start the resource (start the domain)
#
#		Failover="migrate"
# 		  o Suspend the domain on the source node
# 		  o Copy the domain's memory pages from the source node to the target node
# 		  o Resume the domain on the target node
#
# 		Failover="migrate --live"
#		  o Iteratively copy the domain's memory pages from the source node to the taregt node
# 		  o When pre-copy is no longer benefical, suspend the domain on the source node
# 		  o Copy the domain's remaning "dirty" pages from the source node to the taregt node
# 		  o Resume the domain on the target node
#
#            ADMIN - path of the administrative directory (required)
#		
#		The administrative directory is used to store administrative files that are 
#		  used when defining the domain and to manage the correct state of the resource 
#		  when a migrate or live mirate is used. 
#
#
RS=
RG=
HAS_RS=
LH_RS=
DOMAIN=
PLUGIN_PROBE=""
FAILOVER=""
ADMIN=
