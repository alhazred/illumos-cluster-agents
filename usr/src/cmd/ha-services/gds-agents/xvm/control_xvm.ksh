#!/usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident   "@(#)control_xvm.ksh 1.2     09/02/19 SMI"
#
# Usage GDS: <parameter> <options>
#
#	parameter: start | stop | probe | validate
#	<options>: -R <resource> -G <resourcegroup> etc.

MYNAME=`/usr/bin/basename $0`
MYDIR=`/usr/bin/dirname $0`

. ${MYDIR}/../etc/config

METHOD=${1}

shift ${OPTIND}

typeset opt

while getopts 'R:G:D:M:P:A:' opt
do
        case "${opt}" in
		R)      RESOURCE=${OPTARG};;
		G)      RESOURCEGROUP=${OPTARG};;
		D)	DOMAIN=${OPTARG};;
		M)	FAILOVER_TYPE=${OPTARG};;
		P)	PLUGIN_PROBE=${OPTARG};;
		A)	ADMIN=${OPTARG};;
		*)      exit 1;;
	esac
done


. ${MYDIR}/functions
	
debug_message "Method: ${MYNAME} - Begin"
${SET_DEBUG}

case "${METHOD}" in
	start)
		start_domain
		rc=$?
		;;
		
	stop)
		stop_domain
		rc=$?
		;;

	probe)
		check_domain
		rc=$?
		;;
	validate)
		validate
		rc=$?
		;;
esac

debug_message "Method: ${MYNAME} - End"
exit ${rc}
