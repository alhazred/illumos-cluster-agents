#!/usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident  "@(#)ppkssh.ksh 1.3     09/02/19 SMI"
#

typeset opt

while getopts 'P:' opt
do
  case "${opt}" in
	P)	PARMS=${OPTARG};;
	*)	exit 1;;
   esac
done

shift $((${OPTIND} -1))

TIMEOUT=$1

echo ${PARMS} | /usr/bin/awk -F: '{print $1" "$2" "$3" "$4" "$5}' | read USER IDFILE HOST SERVICE STATE

SERVICE=$(echo ${SERVICE} | /usr/xpg4/bin/tr -s '[:upper:]' '[:lower:]')

# This probe can be viewed as a plugin probe to the agent probe. As such the agent probe performs
# very basic domain checks whereas this plugin probe provides more user specific checks. Currently,
# we only provide private/public key ssh authentication to the guest domain to check either a specific 
# runlevel or SMF service. 
# 
# This plugin probe is passed the following colon separated parameter. 
#
# -P USER:IDFILE:HOST:SERVICE:STATE
#
# USER - User name of user for the prublic key ssh authentication.
# IDFILE - The ssh identity file to read the private key.
# HOST - The guest domain's hostname.
# SERVICE - Either "runlvel" or SMF service.
# STATE - The desired runlevel or online for the SMF service.
#
# For example, 
#
# /opt/SUNWscxvm/bin/ppkssh -P fmuser:/export/fmuser/.ssh/id_dsa:vzodio1a:multi-user-server:online
# /opt/SUNWscxvm/bin/ppkssh -P fmuser:/export/fmuser/.ssh/id_dsa:vzodio2a:runlvel:3
#
# Note that the agent probe passes us a parameter which represents 90% of PROBE_TIMEOUT. As such 
# this plugin probe must complete within that value.
#
# The agent probe will output whatever is output from this plugin probe, e.g.
#
# vzodio1a multi-user-server is online
# vaodio2a runlevel is 3
# 
# The agent probe expects this plugin probe to exit with one of the following return codes.
#
#   0 - Indicates a successful probe.
# 100 - Indicates a complete failure, the agent probe will then attempt to restart the domain.
# 201 - Indicates a complete failure but requests an immeidate failover.
#
# Note that return code 201 requires that the resource has an appropriate value for extension
# properties FAILOVER_MODE and FAILOVER_ENABLED=TRUE. For FAILOVER_MODE refer to the
# r_properties(5) man page.

if [ "${SERVICE}" = "runlevel" ]
then
   output=$(/usr/cluster/bin/hatimerun -t ${TIMEOUT} /usr/bin/ssh -l ${USER} -i ${IDFILE} ${HOST} \
	"/sbin/runlevel | /usr/bin/awk '{print \$2}'" 2> /dev/null)
else
   output=$(/usr/cluster/bin/hatimerun -t ${TIMEOUT} /usr/bin/ssh -l ${USER} -i ${IDFILE} ${HOST} \
	"/usr/bin/svcs -H -o STATE ${SERVICE}" 2> /dev/null)
fi

rc=$?

[ "${rc}" -eq 99 ] && exit 99

if [ "${rc}" -eq 0 ] && [ "${output}" = "${STATE}" ]
then
	rc=0
else
	rc=100
fi

printf "${HOST} ${SERVICE} is ${output}\n"

exit ${rc}
