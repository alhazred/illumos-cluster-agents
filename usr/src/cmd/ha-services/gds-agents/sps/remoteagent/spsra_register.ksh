#!/usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#ident	"@(#)spsra_register.ksh	1.6	07/06/06 SMI"

#
#  This script takes 1 options.
#  -f filename states a config file different from spsra_config.
#     This file will be sourced instead of spsra_config if -f filename is specified
#

# Set generic variables:

BINDIR=/opt/SUNWscsps/remoteagent/bin
UTILDIR=/opt/SUNWscsps/remoteagent/util
SMFUTILDIR=/opt/SUNWsczone/sczsmf/util
DIRNAME=/usr/bin/dirname
ECHO=/usr/bin/${ECHO}

MYNAME=`basename ${0}`
MYDIR=`dirname ${0}`

. ${MYDIR}/../etc/config
. ${MYDIR}/../../lib/functions_static
. ${MYDIR}/../bin/functions

global_zone()
{
 
	# define your start, stop, probe an validate command

	start_command="${BINDIR}/start_spsra -R ${RS} -G ${RG} -B ${BASE} -U ${USER} "
	stop_command="${BINDIR}/stop_spsra -R ${RS} -G ${RG} -B ${BASE} -U ${USER} "
	validate_command="${BINDIR}/validate_spsra -R ${RS} -G ${RG} -B ${BASE} -U ${USER} "

	# register your resource

	if /usr/cluster/bin/scrgadm -pvv -t SUNW.gds | /usr/bin/grep Validate_command>/dev/null 2>&1
	then

		# Register in 3.2 style
		
		scrgadm -a -j $RS -g $RG -t SUNW.gds \
		-x Start_command="${start_command}" \
		-x Stop_command="${stop_command}" \
		-x Validate_command="${validate_command}" \
		-y Port_list=$PORT/tcp -y Network_resources_used=$LH \
		-x Stop_signal=9 \
		-y Resource_dependencies=$HAS_RS
		
		St=$?
	else

		# Register in 3.1 style
		
		scrgadm -a -j $RS -g $RG -t SUNW.gds \
		-x Start_command="${start_command}" \
		-x Stop_command="${stop_command}" \
		-y Port_list=$PORT/tcp -y Network_resources_used=$LH \
		-x Stop_signal=9 \
		-y Resource_dependencies=$HAS_RS
		
		St=$?
	fi
	
	if [ "${St}" -ne 0 ]; then
	        echo "Registration of resource ${RS} failed, please correct the wrong parameters"
	
	        exit 1
	else
	        echo "Registration of resource ${RS} succeeded"
	fi
	
	if ! /usr/cluster/bin/scrgadm -pvv -t SUNW.gds | /usr/bin/grep Validate_command>/dev/null 2>&1
	then

		# VALIDATE RESOURCE
		
		echo "Validate resource ${RS} in resource group ${RG}"

		# determine whether the resource runs in the global zone or in a non-global zone
		# and set the zcommand to the appropriate zlogin command

		zcommand=
		rgs_zone=$(rgs_zonename)
		if [ -n "${rgs_zone}" ]
		then
			zcommand="/usr/sbin/zlogin ${rgs_zone} "
		fi

		# validate the contents of the parameter file

		${zcommand} ${BINDIR}/control_spsra -R ${RS} -G ${RG} -P ${PFILE} validate

		St=$?
		
		if [ "${St}" -ne 0 ]; then
		
		        echo "Validation of resource ${RS} failed, please correct the wrong parameters"
		        echo "Removing resource ${RS} from the cluster configuration"
		
		        scrgadm -r -j ${RS}
		
		        exit 1
		else
		        echo "Validation of resource ${RS} succeeded"
		fi
	fi
	return 0
}

local_zone_smf()
{

# function to register a smf resource

	SERVICE_TAG=svc:/application/sczone-agents:${RS}

	if [ ! -f ${SMFUTILDIR}/sczsmf_config ]
	then
		${ECHO}  ${SMFUTILDIR}/sczsmf_config does not exist. Make sure, that \
		      Sun Cluster HA for Solaris container is intalled in the global zone
		return 1
	fi

# prepare the config file

	cp ${SMFUTILDIR}/sczsmf_config ${SMFUTILDIR}/sczsmf_config.work
	cat << EOF > ${SMFUTILDIR}/sczsmf_config 
#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# This file will be sourced in by sczsmf_register and the parameters
# listed below will be used.
#
# These parameters can be customized in (key=value) form
#
#               RS - Name of the resource
#               RG - Name of the resource group containing RS
#         SCZBT_RS - Name of the SC Zone boot resource
#             ZONE - Name of the Zone
#
#       For SERVICE, RECURSIVE and STATE, refer to the svcadm(1M)
#                 man page
#
#          SERVICE - {FMRI | pattern}
#               FMRI - Fault management resource identifier
#               pattern - Pattern matching a service
#
#        RECURSIVE - {false | true}     Default: true
#               False - Just enable the service and no dependents
#               True - Enable the service and recursively enable
#                 its dependents
#
#               RECURSIVE=true equates to svcadm enable "-r"
#
#            STATE - {false | true}     Default: true
#               False - Do not wait until service state is reached
#               True - Wait until service state is reached
#
#               STATE=true equates to svcadm enable/disable "-s"
#
#    SERVICE_PROBE - Script to check the SMF service
#
#               The optional parameter, SERVICE_PROBE, provides the
#                 ability to check that the SMF service is working.
#                 This must be a script within the zone and must
#                 adhere to these return codes,
#
#                 0 - The SMF service is working
#               100 - The SMF service should be restarted
#               201 - The SMF service should initiate a failover of
#                       the Resource Group
#
#               Note: That return code 201, requires that this resource
#                 has an appropriate extension property value for
#                 FAILOVER_MODE and FAILOVER_ENABLED=TRUE
#
#               For FAILOVER_MODE refer to the r_properties(5) man page.
#

RS=${RS}
RG=${RG}
SCZBT_RS=${ZONE_BT}
ZONE=${ZONE}
SERVICE=${SERVICE_TAG}
RECURSIVE=false
STATE=true
SERVICE_PROBE=""
  
EOF

	# determine if a working copy of the config file was created if yes use the -f option

	REGOPT=""
	if [ -f ${UTILDIR}/spsra_config.work ]
	then
	        /usr/bin/cat ${UTILDIR}/spsra_config.work | /usr/sbin/zlogin ${ZONE} /usr/bin/cat - \>/tmp/spsra_config.work 
		REGOPT="-f /tmp/spsra_config.work"
	else
	        /usr/bin/cat ${UTILDIR}/spsra_config | /usr/sbin/zlogin ${ZONE} /usr/bin/cat - \>/tmp/spsra_config.work 
		REGOPT="-f /tmp/spsra_config.work"
	fi

        ${ECHO} clean up the manifest / smf resource
        ${UTILDIR}/spsra_smf_remove ${REGOPT} 2>/dev/null

	# register the manifest

	if /usr/sbin/zlogin ${ZONE} ${UTILDIR}/spsra_smf_register ${REGOPT}
	then
		${ECHO} "Manifest svc:/application/sczone-agents:${RS} was created in zone ${ZONE}"
		${ECHO} "Registering the zone smf resource"
		ksh ${SMFUTILDIR}/sczsmf_register
		ret_code=${?}
                mv ${SMFUTILDIR}/sczsmf_config.work ${SMFUTILDIR}/sczsmf_config
	else
		${ECHO} "The registration of the manifest did not complete, fix the errors and retry"
		ret_code=0
	fi

	/usr/sbin/zlogin ${ZONE} /usr/bin/rm /tmp/spsra_config.work
	
	return ${ret_code}

}


MYCONFIG=
ZONETYPE=global

typeset opt

while getopts 'f:' opt
do
        case "${opt}" in
                f)      MYCONFIG=${OPTARG};;
                *)      ${ECHO} "ERROR: ${MYNAME} Option ${OPTARG} unknown - early End. Only -f is valid"
                        exit 1;;
        esac
done

# Sourcing the specified config file, either the default one,
# or the one supplied with -f

if [ -n "${MYCONFIG}" ] && [ -f "${MYCONFIG}" ]
then
	${ECHO} "sourcing ${MYCONFIG} and create a working copy under ${UTILDIR}/spsra_config.work"
	cp ${MYCONFIG} ${UTILDIR}/spsra_config.work
	. ${MYCONFIG}
else
	PKGCONF=`dirname $0`/spsra_config
	${ECHO} "sourcing ${PKGCONF}"
	. ${PKGCONF}
fi

# Registering the resource

if [ -n "${ZONE}" ]
then
	local_zone_smf
else
	global_zone
fi

if [ -f ${UTILDIR}/spsra_config.work ]
then
        ${ECHO} " remove the working copy ${UTILDIR}/spsra_config.work"
	rm ${UTILDIR}/spsra_config.work
fi

exit 0


