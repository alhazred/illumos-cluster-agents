#!/usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#ident	"@(#)pfile.ksh	1.5	07/06/06 SMI"

# Set the Centerrun specific environment variables which the start, stop
# and check fuctions will use
#
# User          Centerrun User
# Basepath      Absolute path to N1 Grid Service Prosioning Systems Apache Basedir directory 
# Host          Hostname to test Apache Tomcat
# Tport         Port where the N1 Grid Service Prosioning Systems Apache Tomcat instance 
#		is configured to listen
# TestCmd       Apache Tomcat test command, this variable needs different contents, depending on
#		your master server configuration.
#		Your master server answers http request, configure:
#		TestCmd="get /index.jsp"
#		Your master server answers https request, configure:
#		TestCmd="/index.jsp"
# ReturnString	Use one of the strings below according to your N1 Grid Service Prosioning Systems 
#		Server Version.
#		Version 4.1 and 5.x = SSL|Service
# Startwait     Sleeping $Startwait seconds after completion of the
#               start command
# WgetPath	If the Master server is configured to answer https requests only, the absolute path
#		to the wget command is needed here. Omit this variable if your master server answers
#		on http requests.
#		example: WgetPath=/usr/sfw/bin/wget
#		Optional
User=
Basepath=
Host=
Tport=
TestCmd="get /index.jsp"
ReturnString="SSL|Service"
Startwait=
WgetPath=
