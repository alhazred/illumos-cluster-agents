#!/usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#ident	"@(#)spsma_smf_register.ksh	1.6	07/06/06 SMI"

MYDIR=/opt/SUNWscsps/master
MYFILE=
MYCONFIG=
MANIFEST_DIR=/var/svc/manifest/application/sczone-agents


MYNAME=`basename ${0}`

#############################################################
# create_xml()
#
# This function creates the xml manifest that will get called 
# by SMF, i.e. svcadm enable <service> . This allows the SMF
# service to be called in a zone by the 
#
# 	Sun Cluster Data Service for Solaris Container
#	- sczsmf component
#
#############################################################

create_xml()
{
	MYDIR=$1
	MYFILE=$2

	if [ ! -d "${MANIFEST_DIR}" ]
	then
		mkdir -p ${MANIFEST_DIR}
	fi
		
	cat > ${MANIFEST_DIR}/${MYFILE}.xml <<-EOF
	<?xml version="1.0"?>
	<!DOCTYPE service_bundle SYSTEM "/usr/share/lib/xml/dtd/service_bundle.dtd.1">

	<!--
	    Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
	    Use is subject to license terms.

	-->

	<service_bundle type='manifest' name='${MYFILE}'>

	<service
	name='application/sczone-agents'
	type='service'
	version='1'>

	<!--
	    Common dependencies for the service
	-->

	<dependency name='spsma_services'
	grouping='require_all'
	restart_on='none'
	type='service'>
		<service_fmri value='svc:/milestone/multi-user-server'/>
		<service_fmri value='svc:/network/loopback'/>
		<service_fmri value='svc:/network/physical'/>
	</dependency>

	<instance name='${MYFILE}' enabled='false'>


	<exec_method
	type='method'
	name='start'
	exec='${MYDIR}/bin/control_spsma start'
	timeout_seconds='300' >
	<method_context project='${PROJECT}' >
		<method_credential user='${User}' />
		<method_environment>
			<envvar  name='LOGNAME' value='${User}' />
		</method_environment>
	</method_context>
	</exec_method>

	<exec_method
	type='method'
	name='stop'
	exec='${MYDIR}/bin/control_spsma stop'
	timeout_seconds='300' >
	<method_context project='${PROJECT}' >
		<method_credential user='${User}' />
		<method_environment>
			<envvar  name='LOGNAME' value='${User}' />
		</method_environment>
	</method_context>
	</exec_method>

	<!--
	    Instance specific parameters
	-->

	<!--

	This  manifest does not restart on coredumps or abnormal process termination.
	The included PostgreSQL database has its own restart capabilities, to restart 
	child postmaster processes. If the parent postmaster dies, the whole postgres 
	process tree will disappear, and the probe will kick in. If the tomcat dies, 
	the probe will kick in. Due to this fact it is sufficient, to restart on an 
	empty process tree and on HW error.

	For additional information start with man svc.startd

	-->

	<property_group name='startd' type='framework'>
       		 <propval name='ignore_error' type='astring' value='core,signal' />
	</property_group>

        <property_group name='parameters' type='general'>
        <propval name='Resource' type='astring' value='${RS}'/>
        <propval name='Resource_group' type='astring' value='${RG}'/>
        <propval name='Parameter_File' type='astring' value='${PFILE}'/>
	</property_group>

	</instance>

	<stability value='Evolving' />

	<template>
	<common_name>
	<loctext xml:lang='C'>
	Application Service Provisioning System Master Server
	</loctext>
	</common_name>
	<description>
	<loctext xml:lang='C'>
	Application Service Provisioning System Master Server
	</loctext>
	</description>
	</template>
	</service>

	</service_bundle>

	EOF
}

typeset opt

while getopts 'f:' opt
do
        case "${opt}" in
                f)      MYCONFIG=${OPTARG};;
                *)      exit 1;;
        esac
done

# Sourcing the specified config file, either the default one,
# or the one supplied with -f

if [ -n "${MYCONFIG}" ] && [ -f "${MYCONFIG}" ]
then
        echo "sourcing ${MYCONFIG}"
        . ${MYCONFIG}
else
        PKGCONF=`dirname $0`/spsma_config
        echo "sourcing ${PKGCONF}"
        . ${PKGCONF}
fi

# source the parameter file

if [ -n "${PFILE}" ] && [ -f ${PFILE} ]
then
	echo sourcing parameter file ${PFILE}
	. ${PFILE}
else
	echo the parameter file ${PFILE} does not exist or is not set
	exit 1	
fi

# checking and initializing user and project, if they are not set

if [ -z "${PROJECT}" ]
then
	PROJECT=:default
fi

if [ -z "${User}" ]
then
	User=root
fi

# create, validate and import the manifest

MYFILE=${RS}

if create_xml $MYDIR $MYFILE
then
	printf "${MANIFEST_DIR}/${MYFILE}.xml successfully created\n"
else
	printf "${MANIFEST_DIR}/${MYFILE}.xml failed\n"
	exit 1
fi

if /usr/sbin/svccfg validate ${MANIFEST_DIR}/${MYFILE}.xml
then
	printf "${MANIFEST_DIR}/${MYFILE}.xml successfully validated\n"
else
	exit 1
fi
	
if /usr/sbin/svccfg import ${MANIFEST_DIR}/${MYFILE}.xml
then
	printf "${MANIFEST_DIR}/${MYFILE}.xml successfully imported\n"
else
	exit 1
fi

# Validate the parameters of the Master Server Resource

${MYDIR}/bin/control_spsma validate svc:/application/sczone-agents:${RS}
if [ ${?} -eq 0 ]
then
	echo " validation of the SPS Master Server smf service svc:/application/sczone-agents:${RS} successful"
else
	echo " validation of the SPS Master Server smf service svc:/application/sczone-agents:${RS} failed, fix the errors and retry"
	exit 1
fi

exit 0
