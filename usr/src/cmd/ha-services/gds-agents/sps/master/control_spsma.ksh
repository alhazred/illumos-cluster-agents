#!/usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#ident	"@(#)control_spsma.ksh	1.7	07/06/06 SMI"

#
# Method for the Master Server agents and the  smf manifest
#
# This method is called by the GDS manifest by the optional probe script of the smf method.
#
# it is started with options and up to 2 parameters:
#
#
# $1 start stop or probe
# $2 is the smf service tag name. It is used only if the parameter $1 is probe
#

MYNAME=`basename ${0}`
MYDIR=`dirname ${0}`

. ${MYDIR}/../etc/config
. ${MYDIR}/../../lib/functions_static
. ${MYDIR}/functions

if [ -f /lib/svc/share/smf_include.sh ]
then
	. /lib/svc/share/smf_include.sh
fi

debug_message "Method: ${MYNAME} ${*} - Begin"
${SET_DEBUG}

# get the options for gds and the zsh command, amend as appropriate

while getopts 'R:G:P:' opt
do
        case "${opt}" in
                R)      RESOURCE=${OPTARG};;
                G)      RESOURCEGROUP=${OPTARG};;
                P)      PARFILE=${OPTARG};;
                *)      exit 1;;
        esac
done

# if no option is set ($OPTIND is 1 in this case), use the smf properties

if [ ${OPTIND} -gt 1 ]
then
	shift $((${OPTIND} - 1))
else

	. /lib/svc/share/smf_include.sh
	
	# Setting SMF_FMRI in case of validate and probe
        
	if [ -z "${SMF_FMRI}" ]
	then
		SMF_FMRI=${2}	
	fi

	# getting the necessary parameters and filling the variables usually filled
	# in from options

	get_fmri_parameters
fi

# set some generic variables

LOGFILE=/var/tmp/${RESOURCE}_logfile

case ${1} in
start)

	# start application Master Server

	# exit from start, if the options are wrong

	validate_options
	rc_val=${?}
	if [ ${rc_val} -ne 0 ]
	then
		terminate ${1} ${rc_val}
	fi
	
	rm ${LOGFILE} 2>/dev/null
	
	# check the content of the options 

	if validate
	then

		# source the paramter file before the actual start

		. ${PARFILE}

		start_spsma
		rc_val=${?}
		
		if [ ${rc_val} -eq 0 ]
		then
		        log_message notice "start_command rc<${rc_val}>"
		        debug_message "Method: ${MYNAME} - End (Exit 0)"
		else
		        log_message err "start_command rc<${rc_val}>"
		fi
	else
	        debug_message "Method: ${MYNAME} - End (Exit 1)"
		rc_val=1
	fi;;
stop)

	# stop application Master Server

	# exit from stop, if the options are wrong

	validate_options
	rc_val=${?}
	if [ ${rc_val} -ne 0 ]
	then
		terminate ${1} ${rc_val}
	fi

	# source the paramter file before the actual stop

	. ${PARFILE}
	
	stop_spsma
	rc_val=${?}
	
	if [ "${rc_val}" -eq 0 ]
	then
	        log_message notice "stop_command rc<${rc_val}>"
	else
	        log_message err "stop_command rc<${rc_val}>"
	fi
	rc_val=0;;

probe)

	# probe application Master Server

	# exit from probe, if the options are wrong

	validate_options
	rc_val=${?}
	if [ ${rc_val} -ne 0 ]
	then
		terminate ${1} ${rc_val}
	fi

	
	# spin off a project based smf probe if necessary, do the probe with the check function otherwise

	if [ "${Project}" != ":default" ] && [ -n "${SMF_FMRI}" ]
	then
		/usr/bin/newtask -p ${Project} ${MYDIR}/probe_smf_spsma ${SMF_FMRI}
		rc_val=$?
	else


		# source the paramter file before the actual probe

		. ${PARFILE}

		check_spsma
		rc_val=$?
	fi

	if [ ${rc_val} -ne 0 ]
	then
		rc_val=100
	fi;;

validate)

	# validate the parameters for application Master Server

	validate_options
	rc_val=${?}
	if [ ${rc_val} -ne 0 ]
	then
		terminate ${1} ${rc_val}
	fi
	
	rm ${LOGFILE} 2>/dev/null
	
	if validate
	then
	        rc_val=0
	else
	        rc_val=1
	fi;;
	
esac

# terminate with the right return code, either with an smf specific or the gds/zsh based
# return code

debug_message "Method: ${MYNAME} ${*} - End terminating"
terminate ${1} ${rc_val}

