#!/usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#ident	"@(#)db_prep_postgres.ksh	1.6	07/06/06 SMI"

#
# This utility prepares the Postres sql command for the use of the HA N1 Grid Service Provisioning Server
# agent.
#
Basepath=$1
#
# Validating the Baspath parameter
#
if [ -z $Basepath ]; then
	echo "Error: The Basepath is not set but, it is required"
fi
if [ ! -d $Basepath ]; then
	echo "Error: The directory $Basepath does not exist" 
fi
if [ ! -f ${Basepath}/server/bin/roxdbcmd ]; then
	echo "Error: The N1 Grid Service Provisioning Server database utility does not exist, its not a valid masterserver installation"
fi

# Drop the user scuser and the table sc_test, because there may be
# a mismatch between the effective postgres userid (usesysid), the
# user name and the effective owner of the table sc_test due to the
# backup and restore procedure of the master server.
# This mismatch happens only if you restored the master server from a
# different cluster. We try to drop the user and the table in advance
# regardless if they are there, to circumvent this issue.
# If the database is newly installed or restored from a non
# clustered system, the drop statements will throw an error message
# which is ignored anyway.

${Basepath}/server/bin/roxdbcmd psql rox >/dev/null 2>&1 << EOF
drop table sc_test;
drop user sc_test;
EOF

# Create the user and the table needed to monitor the postgres 
# database.

${Basepath}/server/bin/roxdbcmd psql rox << EOF
create user sc_test;
EOF
${Basepath}/server/bin/roxdbcmd psql rox sc_test <<EOF2
create table sc_test (sccol varchar(30));
EOF2

