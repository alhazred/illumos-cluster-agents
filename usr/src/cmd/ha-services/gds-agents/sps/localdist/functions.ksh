#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#ident	"@(#)functions.ksh	1.7	07/06/06 SMI"

SCLOGGER=/usr/cluster/lib/sc/scds_syslog
PKG=SUNWscsps
METHOD=`basename $0`
LOGFILE=/var/tmp/${RESOURCE}_logfile
SCHA_RESOURCE_GET=/usr/cluster/bin/scha_resource_get
SCHA_RESOURCEGROUP_GET=/usr/cluster/bin/scha_resourcegroup_get
TASK_COMMAND=""
RESOURCE_PROJECT_NAME=""

get_fmri_parameters ()
{

# extract the smf properties, you need to call your agent commands

        debug_message "Function: get_fmri_parameters - Begin "
        ${SET_DEBUG}

# Resource name

	RESOURCE=`/usr/bin/svcprop -p parameters/Resource ${SMF_FMRI}`

# Resource Group

	RESOURCEGROUP=`/usr/bin/svcprop -p parameters/Resource_group ${SMF_FMRI}`

# Start Project

	Project=:default
	if /usr/bin/svcprop ${SMF_FMRI}|grep start/project >/dev/null
	then
		Project=`/usr/bin/svcprop -p start/project ${SMF_FMRI}`
		if [ "${Project}" != ":default" ]
		then
			PROJ_OPT=" -P ${Project}"
			ZONE_PROJECT=${Project}
		fi
	fi

# Base Path 

	Basepath=`/usr/bin/svcprop -p parameters/Base_Path ${SMF_FMRI}`

# User

	User=`/usr/bin/svcprop -p parameters/User ${SMF_FMRI}`

        debug_message "Function: get_fmri_parameters - End "
}

Check_InterRG_dependency()
{
        if [ -z "`/usr/cluster/bin/scrgadm -pvv -j ${RESOURCE} | /bin/grep Resource_dependencies_restart`" ]; then

                scds_syslog -p daemon.error -t $(syslog_tag) -m \
               "No Inter RG-dependency found, using internal dependency"

                USE_INTERNAL_DEP=TRUE
        fi
}

validate_options()
{
        #
        # Ensure all options are set
        #

	rc_validate_options=0
	SCRIPTNAME=`basename $0`
        for i in RESOURCE RESOURCEGROUP USE_INTERNAL_DEP
        do
                case $i in
                        RESOURCE)
                        if [ -z "$RESOURCE" ]; then
                                # SCMSGS
                                # @explanation
                                # A requred optin is not set in the start,
                                # stop, validate or probe command.
                                # @user_action
                                # Fix the appropriate command in example by
                                # regegistering.
                                scds_syslog -p daemon.err -t $(syslog_tag) -m \
                                "validate_options: HA N1 Grid Service Provisioning System Local Distributor %s Option %s is not set" \
                                "${SCRIPTNAME}" "-R" 
				rc_validate_options=1
                        fi;;

                        RESOURCEGROUP)
                        if [ -z "$RESOURCEGROUP" ]; then
                                scds_syslog -p daemon.err -t $(syslog_tag) -m \
                                "validate_options: HA N1 Grid Service Provisioning System Local Distributor %s Option %s is not set" \
                                "${SCRIPTNAME}" "-G" 
				rc_validate_options=1
                        fi;;

			Basepath)
			if [ -z "$Basepath" ]; then
				scds_syslog -p daemon.err -t $(syslog_tag) -m \
                                "validate_options: HA N1 Grid Service Provisioning System Local Distributor %s Option %s is not set" \
                                "${SCRIPTNAME}" "-B" 
				rc_validate_options=1
			fi;;

			User)
			if [ -z "$User" ]; then
				scds_syslog -p daemon.err -t $(syslog_tag) -m \
                                "validate_options: HA N1 Grid Service Provisioning System Local Distributor %s Option %s is not set" \
                                "${SCRIPTNAME}" "-U" 
				rc_validate_options=1
			fi;;

                        USE_INTERNAL_DEP)
                        if [ "${USE_INTERNAL_DEP}" = "FALSE" ]; then
				Check_InterRG_dependency
                        fi;;

                esac
        done
	return ${rc_validate_options}
}

validate()
{
	#
	# Validate N1 Grid Service Provisioning System
	#
	
        debug_message "Function: validate - Begin"
	$SET_DEBUG

	rc_validate=0

	# Test if all the mandatory variables are included and set correctly in the parameterfile 

	for i in User Basepath 
	do
		case $i in

                        User)

			# Test N1 Grid Service Provisioning System user
                      
                        if [ -z "$User" ]; then
   				# SCMSGS
   				# @explanation
   				# The parameter User is not set in the
   				# parameter file
   				# @explanation-2
   				# The user is not set in the parameter file,
   				# or in the start stop or probe command.
   				# @user_action
   				# set the variable User in the paramter file
   				# mentioned in option -N to a of the start,
   				# stop and probe command to valid contents.
   				# @user_action-2
   				# Set he user in the appropriate place. If it
   				# is from the master server use the parameter
   				# file, otherwise use the start, stop or probe
   				# command.
   				scds_syslog -p daemon.err -t $(syslog_tag) -m \
                                "validate: User is not set but it is required"
				rc_validate=1
                        else
				id ${User} >/dev/null 2>&1
				if [ $? -ne 0 ]; then
	   				# SCMSGS
	   				# @explanation
	   				# The user with the name $Username
	   				# does not exist or was not returned
	   				# by the nameservice.
	   				# @user_action
	   				# set the variable User in the
	   				# paramter file mentioned in option -N
	   				# to a of the start, stop and probe
	   				# command to valid contents.
	   				scds_syslog -p daemon.err -t $(syslog_tag) -m \
	                                "validate: User %s does not exist but it is required" \
					"${User}"
					rc_validate=1
	                        fi
                        fi;;

                        Basepath)

			# Test N1 Grid Service Provisioning System basepath 
                      
                        if [ -z "$Basepath" ]; then
   				# SCMSGS
   				# @explanation
   				# The parameter Basepath is not set in the
   				# parameter file
   				# @user_action
   				# set the variable Basepath in the paramter
   				# file mentioned in option -N to a of the
   				# start, stop and probe command to valid
   				# contents.
   				scds_syslog -p daemon.err -t $(syslog_tag) -m \
                                "validate: Basepath is not set but it is required"
				rc_validate=1
                        else
	                        if [ ! -f ${Basepath}/ld/bin/cr_ld ]; then
	   				# SCMSGS
	   				# @explanation
	   				# The Basepath is set to a false
	   				# value.
	   				# @user_action
	   				# Set the Basepath to the right
	   				# directory
	   				scds_syslog -p daemon.err -t $(syslog_tag) -m \
	                                "validate: The N1 Grid Service Provisioning System local distributor agent start command does not exist, its not a valid local distributor installation"
					rc_validate=1
	                        fi
	                        if [ ! -d $Basepath ]; then
	   				# SCMSGS
	   				# @explanation
	   				# The specified directory does not
	   				# exist
	   				# @user_action
	   				# Set the directory name to the right
	   				# directory.
	   				scds_syslog -p daemon.err -t $(syslog_tag) -m \
	                                "validate: Directory %s does not exist" \
					"${Basepath}"
					rc_validate=1
	                        fi
                        fi;;

		esac
	done

	debug_message "Function: validate - End"
	return ${rc_validate}

}


start_spsld()
{
	#
	# Start N1 Grid Service Provisioning System
	#

        debug_message "Function: start_spsld - Begin"
	$SET_DEBUG


	# Get the project name for the resource

	if ! srm_function $User ; then
		# The error message is out of by the function
		return 1
	fi

	#
	# This should take care of sh, ksh, csh, tcsh and bash
	#

	if [ -z "${SMF_FMRI}" ]
	then
		if getent passwd ${User} | awk -F: '{print $7}' | grep csh > /dev/null
		then
			# the noprompt suppresses the password challenge for ssl configurations
			# if no ssl is configured noprompt is ignored
			su - ${User} -c "${TASK_COMMAND} ${Basepath}/ld/bin/cr_ld start noprompt >& ${LOGFILE}" > /dev/null
			rc_start_command=$?
		else
			# the noprompt suppresses the password challenge for ssl configurations
			# if no ssl is configured noprompt is ignored
			su - ${User} -c "${TASK_COMMAND} ${Basepath}/ld/bin/cr_ld start noprompt >${LOGFILE} 2>&1" > /dev/null
			rc_start_command=$?
		fi
	else
		# the noprompt suppresses the password challenge for ssl configurations
		# if no ssl is configured noprompt is ignored
		${Basepath}/ld/bin/cr_ld start noprompt >${LOGFILE} 2>&1
		rc_start_command=$?
	fi

	debug_message "Function: start_spsld - End"
	return ${rc_start_command}
}

stop_spsld()
{
	#
	# Stop N1 Grid Service Provisioning System
	#

        debug_message "Function: stop_spsld - Begin"
	$SET_DEBUG


	# Get the project name for the resource

	if  ! srm_function $User ; then
		# The error message is out of by the function
		return 1
	fi

	#
	# This should take care of sh, ksh, csh, tcsh and bash
	#

	if [ -z "${SMF_FMRI}" ]
	then
		if getent passwd ${User} | awk -F: '{print $7}' | grep csh > /dev/null
		then
			su - ${User} -c "$TASK_COMMAND ${Basepath}/ld/bin/cr_ld stop >& ${LOGFILE}" > /dev/null
			rc_stop_command=$?
		else
			su - ${User} -c "$TASK_COMMAND ${Basepath}/ld/bin/cr_ld stop >${LOGFILE} 2>&1" > /dev/null
			rc_stop_command=$?
		fi
	else
		${Basepath}/ld/bin/cr_ld stop >${LOGFILE} 2>&1
		rc_stop_command=$?
	fi

	debug_message "Function: stop_spsld - End"
	return ${rc_stop_command}
}
