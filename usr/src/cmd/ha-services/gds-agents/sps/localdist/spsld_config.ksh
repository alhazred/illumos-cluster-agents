#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#ident	"@(#)spsld_config.ksh	1.5	07/06/06 SMI"

# This file will be sourced in by spsld_register and the parameters
# listed below will be used.
#
# These parameters can be customized in (key=value) form
#
#        RS - name of the resource for the application
#        RG - name of the resource group containing RS
#      PORT - name of the port number to satisfy GDS registration
#        LH - name of the LogicalHostname SC resource
#      USER - name of the owner of the local distributor
#      BASE - name of the directry where the N1 Service Provisioning Server 
#             is installed
#    HAS_RS - name of the HAStoragePlus SC resource
#
#
# The following variables need to be set only if the agent runs in a
# failover zone
#
#              ZONE - Zonename where the zsmf component should be registered
#           ZONE_BT - Resource name of the zone boot component
#           PROJECT - A project in the zone, that will be used for the PostgreSQL
#                     smf service.
#                     If the variable is not set it will be translated as :default for
#                     the smf credentialss.
#                     Optional
#

RS=
RG=
PORT=22
LH=
USER=
BASE=
HAS_RS=

# failover zone specific options

ZONE=
ZONE_BT=
PROJECT=
