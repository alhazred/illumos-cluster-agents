#!/usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)samba_config.ksh	1.6	07/06/06 SMI"
#
# These parameters can be customized in (key=value) form
#
#	+++ Resource Specific Parameters +++
#
#                 RS - name for the Sun Cluster Samba resource
#                 RG - name of the Sun Cluster Resource Group to contain RS
#	       RS_LH - name of the Sun Cluster Logical Hostname resource
#	      RS_HAS - name of the Sun Cluster HAStoragePlus resource
#           SERVICES - name of the Samba service(s) this resource should run
#  			Valid entries are
# 		   	SERVICES="smbd"
#		   	SERVICES="smbd,nmbd"
#		   	SERVICES="winbindd"
#
#	+++ Common Parameters +++
#
#             BINDIR - name of the Samba bin directory
#            SBINDIR - name of the Samba sbin directory
#             CFGDIR - name of the Samba instance configuration directory
#             LDPATH - name of the Samba LD_LIBRARY paths
#             FMUSER - name of the Samba fault monitor user
#
#	+++ SMBD & NMBD Specific Parameters +++
#
#       SAMBA_LOGDIR - name of the Samba log directory
#       SAMBA_FMPASS - name of the Samba fault monitor user password
#     SAMBA_FMDOMAIN - name of the Samba NT-domain where the fault monitor user is located
#
#	+++ WINBINDD Specific Parameters +++
#
#   WINBIND_DISCACHE - disable winbind cache (TRUE or FALSE)
# WINBIND_SINGLEMODE - run winbind in single daemon mode (TRUE or FALSE)
#
#	+++ Zone Specific Parameters +++
#
#    	     RS_ZONE - name of the non-global (failover) zone managed by the
#                      Sun Cluster Data Service for Solaris Containers
#              LHOST - name of the Zone IP Address to be used for Samba
#            PROJECT - name for the Solaris Project to be used for this resource
#                      (Default PROJECT=default)
#            TIMEOUT - probe timeout value
#                      (Default TIMEOUT=300)
#
# Note 1: +++ SMBD & NMBD Specific parametes +++
#	These parameter entries are only required when
# 		   SERVICES="smbd"
#		   SERVICES="smbd,nmbd"
#	
# Note 2: +++ WINBINDD Specific parametes +++
#	These parameter entries are only required when
#		   SERVICES="winbindd"
#
# Note 3: +++ Non-global (failover) zone parameters +++
#       These parameters are only required when Samba should run
#       within a failover zone managed by the Sun Cluster Data Service
#       for Solaris Containers.
#
#       To use the Sun Cluster Data Service for Samba together with
#       the Sun Cluster Data Service for Solaris Containers, you must do
#       the following,
#
#       1. Install the Sun Cluster Data Service for Solaris Containers.
#
#       2. Follow the Sun Cluster Data Service for Solaris Containers Guide
#          for instructions on how to create and install a non-global zone
#          in a failover configuration.
#
#          The following text represents condensed documentation and shows the
#          required steps to install Samba into a failover zone on a two node
#          Sun Cluster.
#
#          On all nodes within the cluster, ensure that "exclude lofs" is commented
#          out within /etc/system and that patch 120590-03 or later is installed.
#
#          In the examples below, Samba is installed in the failover zone's zonepath.
#          /usr/local/samba are simply directories within the zone.
#
#          If a global or highly available local file system (instead of using the
#	   zone's zonepath) is required for Samba in the failover zone, you must 
#          configure the "Mounts" parameter within the sczcbt_config file in Step 3.
#
#          You can use a whole" or sparse root failover zone for the installation of
#          Samba on both nodes of the cluster. Performing these tasks on both nodes
#          of the cluster is required to ensure that the zone is "known" to
#          both nodes of the cluster, i.e. configured and installed.
#	
#	   The following example creates a whole root zone. To create a sparse root 
#	   zone, omit the "-b"  flag with the create sub-command, i.e. 
#	
#	   zonecfg:zone1> create 	For a sparse root zone
#	   zonecfg:zone1> create -b	For a whole root zone
#
#          node1# scrgadm -a -g zone1-rg
#          node1# scrgadm -a -g zone1-rg -L -l zone1 -j zone1-lh
#          node1# scrgadm -a -g zone1-rg -t SUNW.HAStoragePlus -j zone1-has \
#          node1# -x FileSystemMountPoints=<failover file system>
#          node1#
#          node1# scswitch -Z -g zone1-rg
#          node1# scswitch -z -g zone1-rg -h node1
#          node1#
#          node1# zonecfg -z zone1
#          zone1: No such zone configured
#          Use 'create' to begin configuring a new zone.
#          zonecfg:zone1> create -b
#          zonecfg:zone1> set zonepath=<failover file system>/zone1
#          zonecfg:zone1> set autoboot=false
#          zonecfg:zone1> add inherit-pkg-dir
#          zonecfg:zone1:inherit-pkg-dir> set dir=/opt/SUNWscsmb
#          zonecfg:zone1:inherit-pkg-dir> end
#          zonecfg:zone1> exit
#          node1#
#          node1# zoneadm -z zone1 install
#          node1# scswitch -z -g zone1-rg -h node2
#
#          node2# cd <failover file system>
#          node2# rm -r zone1
#          node2#
#          node2# zonecfg -z zone1
#          zone1: No such zone configured
#          Use 'create' to begin configuring a new zone.
#          zonecfg:zone1> create -b
#          zonecfg:zone1> set zonepath=<failover file system>/zone1
#          zonecfg:zone1> set autoboot=false
#          zonecfg:zone1> add inherit-pkg-dir
#          zonecfg:zone1:inherit-pkg-dir> set dir=/opt/SUNWscsmb
#          zonecfg:zone1:inherit-pkg-dir> end
#          zonecfg:zone1> exit
#          node2#
#          node2# zoneadm -z zone1 install
#          node2# zoneadm -z zone1 boot
#
#          At this stage zone1 is configured and installed, but Solaris within
#          zone1 is not yet configured. This needs to be done as follows,
#
#          node2# zlogin -C zone1
#          zone1# ~.
#          node2# zoneadm -z zone1 halt
#
#       3. Bring online the whole root non-global zone, so that it is now
#          managed by the Sun Cluster Data Service for Solaris Containers.
#
#          Edit sczbt_config and execute sczbt_register from /opt/SUNWsczone/sczbt/util
#
#          node2# cd /opt/SUNWsczone/sczbt/util
#          node2# vi sczbt_config
#          node2# ./sczbt_register
#          node2#
#          node2# scswitch -e -j <res>
#
#       4. Install Samba in the whole root failover zone.
#
#       5. Provide values for the parameters below and include values for
#          RS_ZONE, LHOST, PROJECT and TIMEOUT.
#
#          The value for the RG= parameter should be the same SC resource group
#          name that was used by the Sun Cluster Data Service for Solaris Containers
#          agent for the failover zone.
#
#          RS_zone should also be the same SC resource name that was used by the
#          Sun Cluster Data Service for Solaris Containers agent for the failover zone.
#
#          Refer to Example 2 - Configuration parameters for deployment of Samba
#                                within a Solaris 10 non-global (failover) zone.
#
#          node2# vi samba_config
#
#       6. Register the SC resource using the samba_register script.
#
#          node2# ./samba_register
#
#       7. Bring online the SC resource that was registered.
#
#          node2# scswitch -e -j <res>
#
# Example 1 - Configuration parameters for deployment of Samba
#               within Solaris 10 global zone nodes or pre-Solaris 10 nodes.
#
#	The following example uses Samba packaged with Solaris, where the Samba
#	instance configuration directory is on a failover file system named
#	/local/samba/PDC
#
#	+++ Resource Specific Parameters +++
#	RS=pdc
#	RG=samba-rg
#	RS_LH=pdc-lh
#	RS_HAS=pdc-has
#	SERVICES="smbd,nmbd"
#
#	+++ Common Parameters +++
#	BINDIR=/usr/sfw/bin
#	SBINDIR=/usr/sfw/sbin
#	CFGDIR=/local/samba/PDC
#	LDPATH=/usr/sfw/lib
#	FMUSER=samba
#
#	+++ SMBD & NMBD Specific Parameters (See Note 1) +++
#	SAMBA_LOGDIR=/local/samba/PDC/logs
#	SAMBA_FMPASS=samba
#	SAMBA_FMDOMAIN=
#
#	+++ WINBIND Specific Parameters (See Note 2) +++
#	WINBIND_DISCACHE=FALSE
#	WINBIND_SINGLEMODE=FALSE
#
#	+++ Zone Specific Parameters (See Note 3) +++
#	RS_ZONE=
#	LHOST=
#	PROJECT=default
#	TIMEOUT=30
#
#
# Example 2 - Configuration parameters for deployment of Samba
#               within a Solaris 10 non-global (failover) zone.
#
#	The following example uses Samba downloaded and compiled into 
#	/opt/samba, where the samba instance configuration directory is 
#	on a failover file system named /local/samba/DMS1
#
#	Note that if you require the latest version of Samba or a newer
#	Samba version to that packaged with Solaris, you will need to 
#	download and compile Samba. The following example uses /opt/samba
#	for the location of Samba after it has been downloaded and compiled.
#
#	You may of course have chosen a different location. Refer to the 
#	Sun Cluster Dataserivce for Samba Guide for an example of downloading 
#	and compiling Samba. 
#
#	+++ Resource Specific Parameters +++
#	RS=zone1-dms1
#	RG=zone1-rg
#	RS_LH=zone1-lh
#	RS_HAS=zone1-has
#	SERVICES="smbd"
#
#	+++ Common Parameters +++
#	BINDIR=/opt/samba/bin
#	SBINDIR=/opt/samba/sbin
#	CFGDIR=/local/samba/DMS1
#	LDPATH=/opt/samba/lib
#	FMUSER=samba
#
#	+++ SMBD & NMBD Specific Parameters (See Note 1) +++
#	SAMBA_LOGDIR=/local/samba/DMS1/logs
#	SAMBA_FMPASS=smb4#ads
#	SAMBA_FMDOMAIN=
#
#	+++ WINBIND Specific Parameters (See Note 2) +++
#	WINBIND_DISCACHE=FALSE
#	WINBIND_SINGLEMODE=FALSE
#
#	+++ Zone Specific Parameters (See Note 3) +++
#	RS_ZONE=zone1
#	LHOST=192.168.100.198
#	PROJECT=default
#	TIMEOUT=30
#
#+++ Resource Specific Parameters +++
RS=
RG=
RS_LH=
RS_HAS=
SERVICES="smbd,nmbd"

#+++ Common Parameters +++
BINDIR=
SBINDIR=
CFGDIR=
LDPATH=
FMUSER=

#+++ SMBD & NMBD Specific Parameters (See Note 1) +++
SAMBA_LOGDIR=
SAMBA_FMPASS=
SAMBA_FMDOMAIN=

#+++ WINBIND Specific Parameters (See Note 2) +++
WINBIND_DISCACHE=FALSE
WINBIND_SINGLEMODE=FALSE

#+++ Zone Specific Parameters (See Note 3) +++
RS_ZONE=
LHOST=
PROJECT=default
TIMEOUT=30
