#!/usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)samba_register.ksh	1.9	07/06/06 SMI"
#

SMBDIR=/opt/SUNWscsmb
MYNAME=`basename $0`
rc=0

TMP_WORK_CONFIG=/opt/SUNWscsmb/util/.tmp_work_config
typeset opt

while getopts 'f:' opt
do
        case "$opt" in
           f)   MYCONFIG=${OPTARG};;
           *)   /usr/bin/echo "ERROR: ${MYNAME} Option ${OPTARG} unknown"
                /usr/bin/echo "Usage: ${MYNAME} -f <config file>"
                exit 1;;
        esac
done

[ -z "${MYCONFIG}" ] && MYCONFIG=/opt/SUNWscsmb/util/samba_config

/usr/bin/cp ${MYCONFIG} ${TMP_WORK_CONFIG}
. ${TMP_WORK_CONFIG}

if [ ${SERVICES} ]
then
	SERVICES=`/usr/bin/echo ${SERVICES} | /usr/bin/tr -d " " | /usr/bin/tr "," " "`

	case ${SERVICES} in
		smbd|"smbd nmbd")	COMPONENT=samba;;
		winbindd)		COMPONENT=winbind;;
		*)			/usr/bin/printf "${MYCONFIG} SERVICES parameter incorrect\n"
					exit 1;;
	esac
else
	/usr/bin/printf "${MYCONFIG} requires SERVICES to be set\n"
	exit 1
fi
	
case ${COMPONENT} in
	samba)
		# Ensure all the required parameters are set

		for i in RS RG SERVICES BINDIR SBINDIR CFGDIR LDPATH FMUSER \
			SAMBA_LOGDIR SAMBA_FMPASS 
		do
			if eval [ -z \$$i ]; then
	   		   /usr/bin/printf "${SMBDIR}/util/samba_register script requires the following parameter, but it is not set $i=\n"
	   		   rc=1
			fi
		done

		[ "${rc}" -ne 0 ] && exit 1

		if [ "${SAMBA_FMDOMAIN}" ]; then
			FMUSER=${SAMBA_FMDOMAIN}'\\\\'${FMUSER}%${SAMBA_FMPASS}
		else
			FMUSER=${FMUSER}%${SAMBA_FMPASS}
		fi
		;;
	winbind)
		# Ensure all the required parameters are set

		for i in RS RG SERVICES BINDIR SBINDIR CFGDIR LDPATH FMUSER \
        		WINBIND_DISCACHE WINBIND_SINGLEMODE
		do
        		if eval [ -z \$$i ]; then
           		   /usr/bin/printf "${SMBDIR}/util/samba_register requires the following parameter, but it is not set $i=\n"
           		   rc=1
        		fi
		done

		# If amending WINBIND_DISCACHE or WINBIND_SINGLEMODE, check samba_smf_register
		# Note, samba_register has WINBIND_DISCACHE="-N"
		# and samba_smf_register has WINBIND_DISCACHE="-n"

		WINBIND_DISCACHE=`/usr/bin/echo ${WINBIND_DISCACHE} | /usr/bin/tr -s  '[:lower:]' '[:upper:]'`
		WINBIND_SINGLEMODE=`/usr/bin/echo ${WINBIND_SINGLEMODE} | /usr/bin/tr -s  '[:lower:]' '[:upper:]'`

		if [ "${WINBIND_DISCACHE}" = "TRUE" ]
		then
			WINBIND_DISCACHE="-N"
		else
			WINBIND_DISCACHE=""
		fi

		if [ "${WINBIND_SINGLEMODE}" = "TRUE" ]
		then
			WINBIND_SINGLEMODE="-Y"
		else
			WINBIND_SINGLEMODE=""
		fi

		[ "${rc}" -ne 0 ] && exit 1
		;;
	*)	/usr/bin/printf "Unexpected error SERVICES=${SERVICES} COMPONENT=${COMPONENT}\n"
		exit 1
		;;
esac

if [ -z ${RS_ZONE} ]
then
	### Process pre-S10, S10 global zone and S10 integrated zone registration ###
	###           All parameter values get stored within the CCR              ###

	if [ "${COMPONENT}" = "samba" ] 
	then

/usr/cluster/bin/scrgadm -a -j ${RS} -g ${RG} -t SUNW.gds \
-x Start_command="/opt/SUNWscsmb/samba/bin/start_samba \
-R '${RS}' -G '${RG}' -X '${SERVICES}' -B '${BINDIR}' -S '${SBINDIR}' -C '${CFGDIR}' \
-L '${SAMBA_LOGDIR}' -U ${FMUSER} -M 'scmondir' -P '${LDPATH}' -H %HOSTNAMES " \
-x Stop_command="/opt/SUNWscsmb/samba/bin/stop_samba \
-R '${RS}' -G '${RG}' -X '${SERVICES}' -B '${BINDIR}' -S '${SBINDIR}' -C '${CFGDIR}' \
-L '${SAMBA_LOGDIR}' -U ${FMUSER} -M 'scmondir' -P '${LDPATH}' -H %HOSTNAMES " \
-x Probe_command="/opt/SUNWscsmb/samba/bin/probe_samba \
-R '${RS}' -G '${RG}' -X '${SERVICES}' -B '${BINDIR}' -S '${SBINDIR}' -C '${CFGDIR}' \
-L '${SAMBA_LOGDIR}' -U ${FMUSER} -M 'scmondir' -P '${LDPATH}' -H %HOSTNAMES " \
-y Port_list=23/tcp -y Network_resources_used=${RS_LH} \
-x Stop_signal=9 \
-y Resource_dependencies_restart=${RS_LH},${RS_HAS}

	else

/usr/cluster/bin/scrgadm -a -j ${RS} -g ${RG} -t SUNW.gds \
-x Start_command="/opt/SUNWscsmb/winbind/bin/start_winbind \
-R '${RS}' -G '${RG}' -X '${SERVICES}' -B '${BINDIR}' -S '${SBINDIR}' -C '${CFGDIR}' \
-U ${FMUSER} -P '${LDPATH}' ${WINBIND_DISCACHE} ${WINBIND_SINGLEMODE} " \
-x Stop_command="/opt/SUNWscsmb/winbind/bin/stop_winbind \
-R '${RS}' -G '${RG}' -X '${SERVICES}' -B '${BINDIR}' -S '${SBINDIR}' -C '${CFGDIR}' \
-U ${FMUSER} -P '${LDPATH}' ${WINBIND_DISCACHE} ${WINBIND_SINGLEMODE} " \
-x Probe_command="/opt/SUNWscsmb/winbind/bin/probe_winbind \
-R '${RS}' -G '${RG}' -X '${SERVICES}' -B '${BINDIR}' -S '${SBINDIR}' -C '${CFGDIR}' \
-U ${FMUSER} -P '${LDPATH}' ${WINBIND_DISCACHE} ${WINBIND_SINGLEMODE} " \
-y Port_list=23/tcp -y Network_resources_used=${RS_LH} \
-x Stop_signal=9 \
-y Resource_dependencies_restart=${RS_LH},${RS_HAS}

	fi
else
	###         Process S10 non-global (failover) zone registration           ###
	###       All parameter values get stored within the SMF repository       ###

	### 1. Validate ${TIMEOUT} is an integer, greater than or equal to 0      ###
	### 2. Get the failover zone's pfile and determine failover zone name     ###
	### 3. Build a temporary sczsmf_config file (i.e. /tmp/${RS}_smf_config)  ###
	### 4. zlogin to the failover zone execute mqr_smf_register               ###
	###    - Creates the SMF manifest using parameter values                  ###
	###    - Validate and import the SMF manifest into the SMF repository     ###
	### 5. Register a SC sczsmf resource to enable/disable the SMF instance   ###

        RS_STATE=`/usr/cluster/bin/scha_resource_get -O status -R ${RS}`
        rc=$?

        if [ "${rc}" -eq 0 ]
        then
           /usr/bin/printf "Sun Cluster resource ${RS} already exists\n"
           exit 1
        fi

        if ! [ "${TIMEOUT}" -ge 0 ] 2> /dev/null
        then
           /usr/bin/printf "TIMEOUT value is not an integer\n"
           exit 1
        fi

        ZONEPFILE=`/usr/cluster/bin/scrgadm -pvv -j ${RS_ZONE} | /usr/bin/grep -w Start_command | \
           /usr/bin/grep -w value | /usr/bin/awk '{ if ($10 == "-P") print $11 }'`

        if [ ! -d "${ZONEPFILE}" ]
        then
           /usr/bin/printf "Unable to retrieve zone parameter file directory\n"
           exit 1
        else
           ZONE=`/usr/bin/grep Zonename= ${ZONEPFILE}/sczbt_${RS_ZONE} | /usr/bin/awk -F= '{print $2}'`
        fi

        SERVICE_TAG=svc:/application/sczone-agents:${RS}

	/usr/bin/cat > /tmp/${RS}_smf_config <<-EOF
	RS=${RS}
	RG=${RG}
	SCZBT_RS=${RS_ZONE}
	ZONE=${ZONE}
	SERVICE=${SERVICE_TAG}
	RECURSIVE=true
	STATE=true
	SERVICE_PROBE="/opt/SUNWscsmb/bin/control_samba probe ${COMPONENT} ${SERVICE_TAG}"
	EOF

        # Remove an existing FMRI in the zone if it already exists
        ${SMBDIR}/util/samba_smf_remove -z ${ZONE} -f ${SERVICE_TAG} 2>/dev/null

        if /usr/sbin/zlogin ${ZONE} ${SMBDIR}/util/samba_smf_register ${TMP_WORK_CONFIG} ${COMPONENT}
        then
           if RS_STATE=`/usr/cluster/bin/scha_resource_get -O status -R ${RS}`
           then
                RS_STATE=`/usr/bin/echo ${RS_STATE} | /usr/bin/awk '{print $1}'`

                if [ "${RS_STATE}" = "OFFLINE" ]
                then
                   /usr/bin/printf "${RS} already registered, but Offline\n"
                   if /usr/cluster/bin/scrgadm -r -j ${RS}
                   then
                        /usr/bin/printf "${RS} removed\n"
                   else
                        /usr/bin/printf "Fix or cleanup resources before retrying\n"
                        exit 1
                   fi
                else
                   /usr/bin/printf "${RS} already registered and is ${RS_STATE}\n"
                   /usr/bin/printf "Registration of ${RS} failed\n"
                   exit 1
                fi
           fi

           if /opt/SUNWsczone/sczsmf/util/sczsmf_register -f /tmp/${RS}_smf_config
           then
                /usr/bin/printf "Registration of ${RS} succeeded\n"
           else
                /usr/bin/printf "Registration of ${RS} failed\n"
                exit 1
           fi
        fi

        /usr/bin/rm ${TMP_WORK_CONFIG}
fi

exit 0
