#!/usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)control_samba.ksh	1.9	07/06/06 SMI"
#
# Usage GDS: <options> <parameter1> <parameter2>
# 
# Usage SMF: <parameter1> <parameter2> <parameter3>
# 
#       <options>: -R <resource> -G <resourcegroup> etc.
#	parameter1: start | stop | probe
#	parameter2: samba | winbindd
#	parameter3: <FMRI>

MYNAME=`basename $0`
MYDIR=`dirname $0`

typeset opt

while getopts 'R:G:X:B:S:C:U:M:L:H:P:N:Y' opt
do
        case "${opt}" in
		R)      RESOURCE=${OPTARG};;
		G)      RESOURCEGROUP=${OPTARG};;
		X)      SERVICES=${OPTARG};;
		N)	# To provide backward agent support, Samba can now optionally run nmbd using
			# option -N 'YES|NO'. Winbind also has an option -n to disable caching. So, 
			# to accomodate Samba and Winbind using the same control_samba file, we need 
			# to determine if we need to set RUN_NMBD or WINBIND_DISCACHE based on the
			# option -N and it's ${OPTARG} values.
			#	
			# Possible combinations are :
			# -N -Y			(Winbind disable caching and running as a single daemon)
			# -N start|stop|probe	(Winbind disable caching)
			# -N YES|NO		(Samba using nmbd or not)

			if [ "${OPTARG}" ]
			then
			   case ${OPTARG} in
				"-Y")   WINBIND_DISCACHE="-n"
					WINBIND_SINGLEMODE="-Y"
					;;
				start|stop|probe)
					WINBIND_DISCACHE="-n"
					SHIFT=-2
					;;
				*)	RUN_NMBD=${OPTARG}
					RUN_NMBD=`/usr/bin/echo ${RUN_NMBD} | /usr/bin/tr -s '[:lower:]' '[:upper:]'`
					;;
			   esac
			else
			   WINBIND_DISCACHE="-n"
			fi;;
		B)      BINDIR=${OPTARG};;
		S)      SBINDIR=${OPTARG};;
		C)      CFGDIR=${OPTARG};;
		U)      SAMBA_FMUSER=${OPTARG};;
		M)      SAMBA_FMRESOURCE=${OPTARG};;
		L)      SAMBA_LOGDIR=${OPTARG};;
		H)	LHOST=${OPTARG};;
		P)      LDPATH=${OPTARG}
			export LD_LIBRARY_PATH=${LDPATH};;
		N)      WINBIND_DISCACHE="-n";;
		Y)      WINBIND_SINGLEMODE="-Y";;

		*)      exit 1;;
	esac
done

if [ "${OPTIND}" -gt 1 ]
then
	# Called by GDS
	CALLER=GDS

	if [ "${SHIFT}" ]
	then
	   shift $(($OPTIND ${SHIFT}))
	else
	   shift $(($OPTIND -1))
	fi
else
	# Called by SMF
	CALLER=SMF

	. /lib/svc/share/smf_include.sh

	SMF_FMRI=${3}
fi

METHOD=${1}
COMPONENT=${2}

if [ "${CALLER}" = "GDS" ]
then
	# Setting RUN_NMBD provides backward agent support
	if [ "${COMPONENT}" = "samba" ]
	then
	   if [ -z "${SERVICES}" ]
	   then
		[ -z "${RUN_NMBD}" ] && RUN_NMBD=YES

		if [ "${RUN_NMBD}" = "YES" ]
		then
		   SERVICES="nmbd smbd"
		else
		   SERVICES=smbd
		fi
	   else
		if /usr/bin/echo ${SERVICES} | /usr/bin/grep nmbd > /dev/null
		then
		   RUN_NMBD=YES
		else
		   RUN_NMBD=NO
		fi
	   fi
	fi

	# Perform all scha* calls
	if [ "${METHOD}" = "probe" -a "${COMPONENT}" = "samba" ]
	then
	   PROBE_TIMEOUT=`/usr/cluster/bin/scha_resource_get -O Extension \
	      -R ${RESOURCE} -G ${RESOURCEGROUP} Probe_timeout`

           PROBE_TIMEOUT=`/usr/bin/echo ${PROBE_TIMEOUT} | /usr/bin/awk '{print $2}'`
	
	   if [ "${RUN_NMBD}" = "YES" ]
	   then
		if [ -z "${LHOST}" ]
		then
		   SCLH=`/usr/cluster/bin/scha_resource_get -O Network_resources_used \
		      -R ${RESOURCE} -G ${RESOURCEGROUP}`

		   LHOST=`/usr/cluster/bin/scrgadm -pvv -j ${SCLH} | /usr/bin/grep HostnameList | \
        		/usr/bin/grep value: | /usr/bin/awk -F: '{print $4}'`
		fi
	   fi
	fi
else
	if [ "${COMPONENT}" = "samba" ]
	then
	   ### Samba properties are stored within the failover zone's SMF repository ###
	   for i in RESOURCE RESOURCEGROUP SERVICES BINDIR SBINDIR CFGDIR \
		   SAMBA_FMUSER SAMBA_FMPASS SAMBA_FMDOMAIN SAMBA_FMRESOURCE SAMBA_LOGDIR \
		   LDPATH LHOST PROBE_TIMEOUT
	   do
	      export $i=`/usr/bin/svcprop -p parameters/$i ${SMF_FMRI}`
	   done

	   if [ "${SAMBA_FMDOMAIN}" = '""' ]; then
		SAMBA_FMUSER=${SAMBA_FMUSER}%${SAMBA_FMPASS}
	   else
		SAMBA_FMUSER=${SAMBA_FMDOMAIN}'\\\\'${SAMBA_FMUSER}%${SAMBA_FMPASS}
	   fi
	else
	   ### Winbind properties are stored within the failover zone's SMF repository ###
	   for i in RESOURCE RESOURCEGROUP SERVICES BINDIR SBINDIR CFGDIR \
		   SAMBA_FMUSER LDPATH WINBIND_DISCACHE WINBIND_SINGLEMODE 
	   do
	      export $i=`/usr/bin/svcprop -p parameters/$i ${SMF_FMRI}`
	   done
	fi

	SERVICES=`/usr/bin/echo ${SERVICES} | /usr/bin/tr "_"  " "`

	# Setting RUN_NMBD provides backward agent support
	if /usr/bin/echo ${SERVICES} | /usr/bin/grep nmbd > /dev/null
	then
	   RUN_NMBD=YES
	else
	   RUN_NMBD=NO
	fi
fi

. ${MYDIR}/../${COMPONENT}/etc/config
. ${MYDIR}/functions

[ -x /sbin/zonename ] && ZONENAME=`/sbin/zonename`

debug_message "Method: ${MYNAME} - Begin"
${SET_DEBUG}

case "${METHOD}" in
	start)
		case "${COMPONENT}" in
		   samba)	validate_common
				rc1=$?
				validate_samba
				rc2=$?
				rc=1; [ "${rc1}" -eq 0 -a "${rc2}" -eq 0 ] && rc=0
				[ "${rc}" -eq 0 ] && start_samba
				rc=$?
				;;
		   winbind)	validate_common
				rc1=$?
				validate_winbind
				rc2=$?
				rc=1; [ "${rc1}" -eq 0 -a "${rc2}" -eq 0 ] && rc=0
				[ "${rc}" -eq 0 ] && start_winbind
				rc=$?
				;;
		esac
		;;
	stop)
		case "${COMPONENT}" in
		   samba)	stop_samba
				rc=$?
				;;
		   winbind)	stop_winbind
				rc=$?
				;;
		esac
		;;


	probe)
		case "${COMPONENT}" in
		   samba)	check_start "start_samba"
				rc=$?
				[ "${rc}" -eq 100 ] && return ${rc}
				check_samba
				rc=$?
				;;
		   winbind)	check_start "start_winbind"
				rc=$?
				[ "${rc}" -eq 100 ] && return ${rc}
				check_winbind
				rc=$?
				;;
		esac
		;;
esac

exit ${rc}
