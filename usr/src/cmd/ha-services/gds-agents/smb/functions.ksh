#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)functions.ksh	1.12	08/08/06 SMI"
#

PKG=SUNWscsmb
MYNAME=`basename $0`
MYDIR=`dirname $0`
MYTMPDIR=/var/tmp
LOGFILE=${MYTMPDIR}/${RESOURCE}_${COMPONENT}_logfile
SCLOGGER=/usr/cluster/lib/sc/scds_syslog
LOGGER=/usr/bin/logger

NMBD=${SBINDIR}/nmbd
SMBD=${SBINDIR}/smbd
NMBLOOKUP=${BINDIR}/nmblookup
WINBINDD=${SBINDIR}/winbindd
SMBCONF=${CFGDIR}/lib/smb.conf
SMBCLIENT=${BINDIR}/smbclient
NETBIOSNAME=`basename ${CFGDIR}`
NMBDLOCKF=${CFGDIR}/var/locks/nmbd.pid
SMBDLOCKF=${CFGDIR}/var/locks/smbd.pid
SAMBA_LOGDIR_TEMP=${CFGDIR}/logs
WINBINDLOCKF=${CFGDIR}/var/locks/winbindd.pid
NSS=/etc/nsswitch.conf
NODEID_LOCAL=`/usr/sbin/clinfo -n`
STARTWAIT=`expr ${NODEID_LOCAL} \* 2`
TMPF=/var/tmp/${NETBIOSNAME}.tmp

syslog_tag()
{
	${SET_DEBUG}
	print "SC[${PKG:-??}.${COMPONENT:-??}.${METHOD:-??}]:${RESOURCEGROUP:-??}:${RESOURCE:-??}"
}

scds_syslog()
{
	if [ -f ${SCLOGGER} ]
	then
	   ${SCLOGGER} "$@" &
	else
	   while getopts 'p:t:m' opt
	   do
		case "${opt}" in
		   t) TAG=${OPTARG};;
		   p) PRI=${OPTARG};;
		esac
	   done

	   shift $((${OPTIND} - 1))
	   LOG_STRING=`/usr/bin/printf "$@"`
	   ${LOGGER} -p ${PRI} -t ${TAG} ${LOG_STRING}
	fi
}

debug_message()
{
	if [ "${DEBUG}" = "${RESOURCE}" -o "${DEBUG}" = "ALL" ]
	then
	   SET_DEBUG="set -x"
	   DEBUG_TEXT=${1}

	   scds_syslog -p daemon.debug -t $(syslog_tag) -m \
		"%s" "${DEBUG_TEXT}"
	else
	   SET_DEBUG=
	fi
}

log_message()
{
	debug_message "Function: log_message - Begin"
	${SET_DEBUG}

	if [ -s "${LOGFILE}" ]
	then
	   PRIORITY=$1
	   HEADER=$2

	   strings ${LOGFILE} > ${LOGFILE}.copy

	   while read MSG_TXT
	   do
		scds_syslog -p daemon.${PRIORITY} -t $(syslog_tag) -m \
		   "%s - %s" \
		   "${HEADER}" "${MSG_TXT}"
	   done < ${LOGFILE}.copy

	   /usr/bin/cat /dev/null > ${LOGFILE}
	fi
}

stripfunc() 
{
	s1=1
	s2=`echo ${1} | /usr/bin/wc -c`
	s3=""

	while [ ${s1} -lt ${s2} ]
	do
	   s4=`/usr/bin/echo ${1} | /usr/bin/cut -c ${s1}`

	   if [[ "${s4}" != @([0-9]) ]]
	   then
		break
	   else
		s3=${s3}${s4}
	   fi

	   s1=$(($s1+1))
	done

	/usr/bin/echo ${s3}
}

validate_samba() 
{
	debug_message "Function: validate_samba - Begin"
	${SET_DEBUG}

	# Validate - Samba log directory ${SAMBA_LOGDIR} exists
	# Validate - smbd ${SMBD} exists and is executable
	# Validate - nmbd ${NMBD} exists and is executable
	# Validate - Faultmonitor resource ${SAMBA_FMRESOURCE} exists
	# Validate - smbclient ${SMBCLIENT} exists and is executable
	# Validate - Retrieved faultmonitor-user ${USER} from the nameservice
	# Validate - RUN_NMBD=${RUN_NMBD

	rc=0

	if [ ! -d "${SAMBA_LOGDIR}" ]
	then
   	   # SCMSGS
   	   # @explanation
   	   # The Samba log directory does not exist.
   	   # @user_action
   	   # Check the correct pathname for the Samba log directory was
   	   # entered when registering the Samba resource and that the
   	   # directory exists.
   	   scds_syslog -p daemon.error -t $(syslog_tag) -m \
		"Validate - Samba log directory %s does not exist" \
		"${SAMBA_LOGDIR}"
	   rc=1
	else
	   debug_message "Validate - Samba log directory ${SAMBA_LOGDIR} exists" 
	fi

	if [ ! -x "${SMBD}" ]
	then
   	   # SCMSGS
   	   # @explanation
   	   # The Samba executable smbd either doesn't exist or is not
   	   # executable.
   	   # @user_action
   	   # Check the correct pathname for the Samba (s)bin directory was
   	   # entered when registering the resource and that the program exists
   	   # and is executable.
   	   scds_syslog -p daemon.error -t $(syslog_tag) -m \
		"Validate - smbd %s non-existent executable" \
		"${SMBD}"
	   rc=1
	else
	   debug_message "Validate - smbd ${SMBD} exists and is executable"
	fi

	if [ ! -x "${NMBD}" ]
	then
   	   # SCMSGS
   	   # @explanation
   	   # The Samba executable nmbd either doesn't exist or is not
   	   # executable.
   	   # @user_action
   	   # Check the correct pathname for the Samba (s)bin directory was
   	   # entered when registering the resource and that the program exists
   	   # and is executable.
   	   scds_syslog -p daemon.error -t $(syslog_tag) -m \
		"Validate - nmbd %s non-existent executable" \
		"${NMBD}"
	   rc=1
	else
	   debug_message "Validate - nmbd ${NMBD} exists and is executable"
	fi

	if [ -z "`/usr/bin/grep '\[' ${SMBCONF} | /usr/bin/cut -d'[' -f2 |\
	    /usr/bin/cut -d']' -f1 | /usr/bin/grep ${SAMBA_FMRESOURCE}`" ]
	then
   	   # SCMSGS
   	   # @explanation
   	   # The Samba resource could not validate that the fault monitor
   	   # resource exists.
   	   # @user_action
   	   # Check that the Samba instance's smb.conf file has the fault
   	   # monitor resource scmondir defined. Please refer to the data
   	   # service documentation to determine how to do this.
   	   scds_syslog -p daemon.error -t $(syslog_tag) -m \
		"Validate - Faultmonitor-resource <%s> does not exist" \
		"${SAMBA_FMRESOURCE}"
	   rc=1
	else
	   debug_message "Validate - Faultmonitor resource ${SAMBA_FMRESOURCE} exists"
	fi

	if [ ! -x "${SMBCLIENT}" ]
	then
   	   # SCMSGS
   	   # @explanation
   	   # The Samba resource tries to validate that the smbclient exists
   	   # and is executable.
   	   # @user_action
   	   # Check the correct pathname for the Samba bin directory was
   	   # entered when registering the resource and that the program exists
   	   # and is executable.
   	   scds_syslog -p daemon.error -t $(syslog_tag) -m \
		"Validate - smbclient %s non-existent executable" \
		"${SMBCLIENT}"
	   rc=1
	else
	   debug_message "Validate - smbclient ${SMBCLIENT} exists and is executable"
	fi

        USER=$(/usr/bin/echo ${SAMBA_FMUSER} | /usr/bin/awk 'BEGIN { FS="\\" } {print $NF}' | /usr/bin/cut -d'%' -f1)

	if [ -z "`/usr/bin/getent passwd ${USER}`" ]
	then
 	   # SCMSGS
 	   # @explanation
 	   # The Samba resource could not validate that the fault monitor
 	   # userid exists.
 	   # @user_action
 	   # Check that the correct fault monitor userid was used when
 	   # registering the Samba resource and that the userid really exists.
 	   scds_syslog -p daemon.error -t $(syslog_tag) -m \
		"Validate - Couldn't retrieve faultmonitor-user <%s> from the nameservice" \
		"${USER}"
	   rc=1
	else
	   debug_message "Validate - Retrieved faultmonitor-user ${USER} from the nameservice"
	fi

	RUN_NMBD=`/usr/bin/echo ${RUN_NMBD} | /usr/bin/tr -s '[:lower:]' '[:upper:]'`

	if [ "${RUN_NMBD}" = "YES" -o "${RUN_NMBD}" = "NO" ]
	then
	   debug_message "Validate - RUN_NMBD=${RUN_NMBD}"
	else
	   # SCMSGS
	   # @explanation
	   # The Samba resource can run without nmbd, however you must specify
	   # the service when configuring the Samba resource
	   # @user_action
	   # Specify either YES or NO for RUN_NMBD within
	   # /opt/SUNWscsmb/samba/util/samba_config
	   scds_syslog -p daemon.error -t $(syslog_tag) -m \
		"Validate - RUN_NMBD=%s is invalid - specify YES or NO" \
		"${RUN_NMBD}"
	   rc=1
	fi

        debug_message "Function: validate_samba - End"
	return ${rc}
}

validate_winbind()
{
        debug_message "Function: validate_winbind - Begin"
        ${SET_DEBUG}

	# Validate - winbindd ${WINBINDD} exists and is executable
	# Validate - winbind defined in ${NSS} in the passwd section
	# Validate - winbind defined in ${NSS} in the group section

	rc=0

	if [ ! -x "${WINBINDD}" ]
	then
	   # SCMSGS
	   # @explanation
	   # The Samba executable winbindd either doesn't exist or is not
	   # executable.
	   # @user_action
	   # Check the correct pathname for the Samba (s)bin directory was
	   # entered when registering the resource and that the program exists
	   # and is executable.
	   scds_syslog -p daemon.error -t $(syslog_tag) -m \
		"Validate - winbindd %s non-existent executable" \
		"${WINBINDD}"
	   rc=1
	else
	   debug_message "Validate - winbindd ${WINBINDD} exists and is executable"
	fi

	if [ -z "`/usr/bin/grep passwd: ${NSS} | /usr/bin/grep -v '#' | /usr/bin/grep winbind`" ]
	then
	   # SCMSGS
	   # @explanation
	   # Winbinbd is missing from the passwd section in /etc/nsswitch.conf
	   # @user_action
	   # Enter winbind within the passwd section in /etc/nsswitch.conf.
	   scds_syslog -p daemon.error -t $(syslog_tag) -m \
		"Validate - winbind is not defined in %s in the passwd section" \
		"${NSS}"
	   rc=1
	else
	   debug_message "Validate - winbind defined in ${NSS} in the passwd section"
	fi

	if [ -z "`/usr/bin/grep group: ${NSS} | /usr/bin/grep -v '#' | /usr/bin/grep winbind`" ]
	then
	   # SCMSGS
	   # @explanation
	   # Winbinbd is missing from the group section in /etc/nsswitch.conf
	   # @user_action
	   # Enter winbind within the group section in /etc/nsswitch.conf.
	   scds_syslog -p daemon.error -t $(syslog_tag) -m \
		"Validate - winbind is not defined in %s in the group section" \
		"${NSS}"
	   rc=1
	else
	   debug_message "Validate - winbind defined in ${NSS} in the group section"
	fi

	debug_message "Function: validate_winbind - End"
	return ${rc}
}

validate_common()
{
	debug_message "Function: validate_common - Begin"
	${SET_DEBUG}

	# Validate - ${COMPONENT} bin directory ${BINDIR} exists
	# Validate - ${COMPONENT} sbin directory ${SBINDIR} exists
	# Validate - ${COMPONENT} configuration directory ${CFGDIR} exists
	# Validate - smbconf ${SMBCONF} exists
	# Validate - nmblookup ${NMBLOOKUP} exists and is executable
	# Validate - Samba version <${SAMBA_VERSION}.${SAMBA_RELEASE}.${SAMBA_UPDATE}> is being used

	rc=0

	if [ ! -d "${BINDIR}" ]
	then
	   # SCMSGS
	   # @explanation
	   # The Samba bin directory does not exist.
	   # @user_action
	   # Check the correct pathname for the Samba bin directory was
	   # entered when registering the Samba resource and that the
	   # directory exists.
	   scds_syslog -p daemon.error -t $(syslog_tag) -m \
		"Validate - %s bin directory %s does not exist" \
		"${COMPONENT}" "${BINDIR}"
	   rc=1
	else
	   debug_message "Validate - ${COMPONENT} bin directory ${BINDIR} exists"
	fi

	if [ ! -d "${SBINDIR}" ]
	then
	   # SCMSGS
	   # @explanation
	   # The Samba resource could not validate that Samba sbin directory
	   # exists.
	   # @user_action
	   # Check that the correct pathname for the Samba sbin directory was
	   # entered when registering the Samba resource and that the sbin
	   # directory really exists.
	   scds_syslog -p daemon.error -t $(syslog_tag) -m \
		"Validate - %s sbin directory %s does not exist" \
		"${COMPONENT}" "${SBINDIR}"
	   rc=1
	else
	   debug_message "Validate - ${COMPONENT} sbin directory ${SBINDIR} exists"
	fi

	if [ ! -d "${CFGDIR}" ]
	then
	   # SCMSGS
	   # @explanation
	   # The Samba resource could not validate that the Samba
	   # configuration directory exists.
	   # @user_action
	   # Check that the correct pathname for the Samba configuration
	   # directory was entered when registering the Samba resource and
	   # that the configuration directory really exists.
	   scds_syslog -p daemon.error -t $(syslog_tag) -m \
		"Validate - %s configuration directory %s does not exist" \
		"${COMPONENT}" "${CFGDIR}"
	   rc=1
	else
	   debug_message "Validate - ${COMPONENT} configuration directory ${CFGDIR} exists"
	fi

	if [ ! -f "${SMBCONF}" ]
	then
	   # SCMSGS
	   # @explanation
	   # The smb.conf file does not exist.
	   # @user_action
	   # Check the correct pathname for the Samba smb.conf file was
	   # entered when registering the Samba resource and that the smb.conf
	   # file exists.
	   scds_syslog -p daemon.error -t $(syslog_tag) -m \
		"Validate - smbconf %s does not exist" \
		"${SMBCONF}"
	   rc=1
	else
	   debug_message "Validate - smbconf ${SMBCONF} exists"
	fi

	if [ ! -x "${NMBLOOKUP}" ]
	then
   	   # SCMSGS
   	   # @explanation
   	   # The Samba resource tries to validate that the nmblookup program
   	   # exists and is executable.
   	   # @user_action
   	   # Check the correct pathname for the Samba bin directory was
   	   # entered when registering the resource and that the program exists
   	   # and is executable.
   	   scds_syslog -p daemon.error -t $(syslog_tag) -m \
		"Validate - nmblookup %s non-existent executable" \
		"${NMBLOOKUP}"
	   rc=1
	else
	   debug_message "Validate - nmblookup ${NMBLOOKUP} exists and is executable"
	fi

        # Is 2.2.x being used

        ${NMBLOOKUP} -h >/dev/null 2>&1

        if [ $? -eq 0 ]; then
           VERSION=`${NMBLOOKUP} -h | /usr/bin/grep 'Version ' | /usr/bin/awk '{print $2}'`
        else
           VERSION=`${NMBLOOKUP} -V | /usr/bin/awk '{print $2}'`
        fi

	SAMBA_VERSION=`/usr/bin/echo ${VERSION} | /usr/bin/cut -d'.' -f1`
	SAMBA_RELEASE=`/usr/bin/echo ${VERSION} | /usr/bin/cut -d'.' -f2`
	SAMBA_UPDATE=`/usr/bin/echo ${VERSION} | /usr/bin/cut -d'.' -f3`

	debug_message "Validate - Samba version <${SAMBA_VERSION}.${SAMBA_RELEASE}.${SAMBA_UPDATE}> is being used"

	# Strip off any non-digits

	SAMBA_VERSION=`stripfunc ${SAMBA_VERSION}`
	SAMBA_RELEASE=`stripfunc ${SAMBA_RELEASE}`
	SAMBA_UPDATE=`stripfunc ${SAMBA_UPDATE}`

	rc_validate_version=0

        if [ -z "${VERSION}" ]
        then
	   # SCMSGS
	   # @explanation
	   # The Samba resource tries to validate that an acceptable version
	   # of Samba is being deployed, however it was unable to retrieve the
	   # Samba version number.
	   # @user_action
	   # Check that Samba has been installed correctly
	   scds_syslog -p daemon.error -t $(syslog_tag) -m \
		"Validate - Couldn't retrieve Samba version number"
	   rc=1
        elif [ "${SAMBA_VERSION}" -lt 2 ]; then
	   rc_validate_version=1
        elif [ "${SAMBA_VERSION}" -eq 2 -a "${SAMBA_RELEASE}" -le 2 -a "${SAMBA_UPDATE}" -lt 2 ]; then
	   rc_validate_version=1
        fi

        if [ "${rc_validate_version}" -gt 0 ]
	then
	   # SCMSGS
	   # @explanation
	   # The Samba resource check to see that an appropriate version of
	   # Samba is being deployed. Versions below v2.2.2 will generate this
	   # message.
	   # @user_action
	   # Ensure that the Samba version is equal to or above v2.2.2
	   scds_syslog -p daemon.error -t $(syslog_tag) -m \
		"Validate - This version of samba <%s> is not supported with this dataservice" \
		`/usr/bin/printf ${SAMBA_VERSION}.${SAMBA_RELEASE}.${SAMBA_UPDATE}`
	   rc=1
        fi

        debug_message "Function: validate_common - End"
	return ${rc}
}

query_pids()
{
	debug_message "Function: query_pids - Begin"
	${SET_DEBUG}

	# Usage: query_pids "parm1" "parm2"
	#
	# If being run on Solaris 10, query_pids() matches the
	# correct zonename, otherwise zones are ignored for
	# pre-Solaris 10 systems.

	rc=1
	PROCESS=$1
	PATTERN=$2
	
	if [ -x /sbin/zonename ]
	then
	   pids=`/usr/bin/ps -u root -o pid,args,zone | /usr/bin/grep " ${ZONENAME}$" | \
		/usr/bin/grep "${PROCESS} " | /usr/bin/grep ${PATTERN} | /usr/bin/awk '{print $1}'`
	else
	   pids=`/usr/bin/ps -u root -o pid,args | \
		/usr/bin/grep "${PROCESS} " | /usr/bin/grep ${PATTERN} | /usr/bin/awk '{print $1}'`
	fi

	[ "${pids}" ] && rc=0

	debug_message "Function: query_pids - End"
	return ${rc}
}

check_start()
{
	debug_message "Function: check_start - Begin"
	${SET_DEBUG}

	# Allow "wait_for_online" to only test after the specified method
	# for ${RESOURCE} has finished (Note additional winbind check)
	#
	# Here $1 is set when check_start is called from within control_samba, e.g.
	# check_start "start_samba" or check_start "start_winbind"

	component=$1
	typeset component_running=0

	if [ "${component}" = "start_samba" ]
	then
	   if [ "${CALLER}" = "GDS" ]
	   then
		/usr/bin/pgrep -u root -f "${component} .*-R ${RESOURCE} " >/dev/null 2>&1
		[ "$?" -eq 0 ] && component_running=100
	   else
		# Here we are being called by SMF where ${component} has already been broken down,
		# i.e. The SMF start method uses "control_samba start samba ${RESOURCE}" and not
 	  	# start_samba which is being passed as $1, therefore we need to amend ${component}.
	  	#
	   	# Note, that ${RESOURCE}, when called by SMF, reflects the FMRI which when using a
	   	# failover zone is the same as the Sun Cluster resource name.

	   	component=`/usr/bin/echo ${component} | /usr/bin/tr "_" " "`
	   	/usr/bin/pgrep -u root -f "${component} ${RESOURCE} " >/dev/null 2>&1
		[ "$?" -eq 0 ] && component_running=100
	   fi
	else
	   # Here Winbind maybe starting, however we need to make sure that after
	   # winbindd has been started that it is ready to process work. This can 
	   # be delayed if caching is on and the winbind cache time is quite high. 
	   #
	   # If getent passwd ${SAMBA_FMUSER} is successful then it's okay to let
	   # the winbind probe continue, otherwise if unsuccessful we need to check
	   # if gds_svc_start is still running. If it is then we return 100 which 
	   # prevents the winbind probe from running.
	   #
	   # The call sequence is as follows:
	   #
	   # GDS -> /opt/SUNWscsmb/winbind/bin/probe_winbind
	   #		|
	   #		+-> ../../bin/control_samba
	   #			|
	   #			+-> probe winbind (case'd)
	   #		             ^		|
	   #		             |	+-> check_start()
	   #		    	     |		|
	   #			     |		+-> 0 -> check_winbind()
	   #		  	     |		+-> 100         |
	   #			     |	             |          +-> 0|100
	   #		             |		     |               |
	   #		             +---------------+---------------+

	   # Note, Winbind's ${SAMBA_FMUSER}=${FMUSER}, see samba_register.
	   if [ "`/usr/bin/getent passwd ${SAMBA_FMUSER}`" ]
	   then
		component_running=0
	   else
		# Note, if gds_svc_start is not running component_running=0 is returned
		/usr/bin/pgrep -u root -f "gds_svc_start .*-R ${RESOURCE} " >/dev/null 2>&1
		[ "$?" -eq 0 ] && component_running=100
		debug_message "check_start - getent passwd ${SAMBA_FMUSER} failed"
	   fi
	fi
	
	# control_samba() for the probe winbind will not run check_winbind() if this
	# function returns 100. If 0 is returned then check_winbind() is run.

	debug_message "Function: check_start - End"
	return ${component_running}
}

start_samba()
{
        debug_message "Function: start_samba - Begin"
	${SET_DEBUG}
	
	# ${SERVICES} contains the Samba service to start, 
	# which is then evaluated to the value of SMBD or NMBD

	SERVICES=`/usr/bin/echo ${SERVICES} | /usr/bin/tr "," " " | /usr/bin/tr -s '[:lower:]' '[:upper:]'`

	for i in ${SERVICES}
	do
	   eval \${$i} -D -s ${SMBCONF} -l ${SAMBA_LOGDIR}
	   rc=$?
	
	   i=`/usr/bin/echo ${i} | /usr/bin/tr -s '[:upper:]' '[:lower:]'`

	   if [ "${rc}" -eq 0 ]
	   then
		
		debug_message "start_samba - ${i} started"
	   else
		# SCMSGS
		# @explanation
		# The Samba resource could not start the Samba server nmbd
		# process.
		# @user_action
		# The Samba resource will be restarted, however examine the
		# other syslog messages occurring at the same time on the same
		# node, to see if the cause of the problem can be identified.
		scds_syslog -p daemon.error -t $(syslog_tag) -m \
		   "start_samba - Could not start Samba server %s %s daemon" \
		   "${NETBIOSNAME}" "${i}"
		rc_start_samba=1
	   fi
	done

        debug_message "Function: start_samba - End"
	return ${rc_start_samba}
}

stop_samba()
{
        debug_message "Function: stop_samba - Begin"
	${SET_DEBUG}

	# ${SERVICES} contains the Samba service to stop,
	# which is the used to evaluate the correct SMBDLOCKF or NMBDLOCKF.
	# TERM is then sent to the pid within ${LOCKF}, otherwise TERM is 
	# sent to the pids obtained from query_pids.
	
	SERVICES=`/usr/bin/echo ${SERVICES} | /usr/bin/tr "," " " | /usr/bin/tr -s '[:lower:]' '[:upper:]'`

	for i in ${SERVICES}
	do
	   LOCKF=LOCKF
	   eval LOCKF=\$$i${LOCKF}

	   if [ -f "${LOCKF}" ]
	   then
		pid=`/usr/bin/cat ${LOCKF}`
		/usr/bin/kill -TERM ${pid} 2>/dev/null
		/usr/bin/rm -f ${LOCKF}
	   fi

	   process=`/usr/bin/echo ${i} | /usr/bin/tr -s '[:upper:]' '[:lower:]'`

	   query_pids ${process} ${SMBCONF}
	   rc=$?
	   [ "${rc}" -eq 0 ] && /usr/bin/kill -TERM ${pids} 2>/dev/null
	done

        debug_message "Function: stop_samba - End"
	return 0
}

start_winbind()
{
        debug_message "Function: start_winbind - Begin"
	${SET_DEBUG}
	
	# Wait X seconds to prevent file problems when starting
	# winbindd with an empty database in scalable mode

	/usr/bin/sleep ${STARTWAIT}

	if [ -z "${WINBIND_DISCACHE}" ]; then
	   WINBIND_DISCACHE=""
	fi

	if [ -z "${WINBIND_SINGLEMODE}" ]; then
	   WINBIND_SINGLEMODE=""
	fi

	# Running scalable winbind across non-global zones with the same netbios name causes winbind problems
	# when retrieving users and groups. To minimize this a scalable winbind resource can set the following
	# parameter within the smb.conf file.
	#
	#	netbios name = winbind.%$(CLINFO).%h
	#
	# which provides each global and non-global zone to have a unique netbios name, so long as CLINFO
	# is available when winbind is started. Note that NODEID_LOCAL is defined at the start of this file.
	#
	# Finally, even though winbind starts it may still occasionally fail to lookup a user, although having
	# a unique netbois name usually solves the issue, also setting START_TIMEOUT=60 allows the probe to 
	# to restart winbind after a reasonbale timeout.

	env CLINFO=${NODEID_LOCAL} ${WINBINDD} -s ${SMBCONF} ${WINBIND_DISCACHE} ${WINBIND_SINGLEMODE}
	rc=$?

	if [ "${rc}" -eq 0 ]
	then
	   debug_message "start_winbind - winbind started"
	else
	   # SCMSGS
	   # @explanation
	   # The Winbind could not be started.
	   # @user_action
	   # No user action is needed. The Winbind server will be restarted.
	   scds_syslog -p daemon.error -t $(syslog_tag) -m \
		"start_winbind - Could not start winbind"
	fi

        debug_message "Function: start_winbind - End"
	return ${rc}
}

stop_winbind()
{
	debug_message "Function: stop_winbind - Begin"
	${SET_DEBUG}

	# TERM is then sent to the pid within ${WINBINDLOCKF}, otherwise 
	# TERM is sent to the pids obtained from query_pids.

	if [ -f "${WINBINDLOCKF}" ]
	then
	   pid=`/usr/bin/cat ${WINBINDLOCKF}`

	   debug_message "stop_winbind - winbindd lockfile found send TERM to ${pid} and remove lockfile"

	   /usr/bin/kill -TERM ${pid}
	   /usr/bin/rm -f ${WINBINDLOCKF}
	else
	   debug_message "stop_winbind - winbindd lockfile not found send TERM to winbindd"

	   query_pids winbindd ${SMBCONF}
	   rc=$?
	   [ "${rc}" -eq 0 ] && /bin/kill -TERM ${pids} 2>/dev/null
	fi

	debug_message "Function: stop_winbind - End"
	return 0
}

check_samba()
{
	debug_message "Function: check_samba - Begin"
	${SET_DEBUG}

	# ${SERVICES} contains the Samba service to check

	SERVICES=`/usr/bin/echo ${SERVICES} | /usr/bin/tr "," " " | /usr/bin/tr -s '[:upper:]' '[:lower:]'`
	rc=0

	for i in ${SERVICES}
	do
	   check_${i}
	   rc=$?
	   [ ${rc} -ne 0 ] && return 100
	done

        debug_message "Function: check_samba - End"
	return ${rc}
}

check_nmbd()
{
        debug_message "Function: check_nmbd - Begin"
	${SET_DEBUG}

	rc=0

	if [ "${RUN_NMBD}" = "YES" ]
	then
	   for lh in ${LHOST}
	   do
		${NMBLOOKUP} -s ${SMBCONF} -U ${lh} ${NETBIOSNAME} | \
		   /usr/xpg4/bin/grep -i -w -e ERROR -e FAILED > ${TMPF}

		if [ -s "${TMPF}" ]
        	then
        	   # SCMSGS
        	   # @explanation
        	   # nmblookup could not be performed.
        	   # @user_action
        	   # No user action is needed. The Samba server will be
        	   # restarted.
        	   scds_syslog -p daemon.error -t $(syslog_tag) -m \
			"check_nmbd - Nmbd for <%s> is not working, failed to retrieve ipnumber for %s" \
			"${NETBIOSNAME}" "${NETBIOSNAME}"
		   rc=1
		else
        	   debug_message "check_nmbd - nmblookup for ${lh} is working"
        	fi
	   done
	fi

        debug_message "Function: check_nmbd - End"
	return ${rc}
}

check_smbd()
{
        debug_message "Function: check_smbd - Begin"
	${SET_DEBUG}

	rc=0

	# Ask name service if the fault monitor user can be retrieved
	# Note, Samba can have ${SAMBA_FMUSER}=${FMUSER}%${FMPASS} or
	# ${SAMBA_FMUSER}=${SAMBA_DOMAIN}'\\\\'${FMUSER}%${FMPASS}, see 
	# samba_register.

        USER=$(/usr/bin/echo ${SAMBA_FMUSER} | /usr/bin/awk 'BEGIN { FS="\\" } {print $NF}' | /usr/bin/cut -d'%' -f1)

	if [ -z "`/usr/bin/getent passwd ${USER}`" ]
	then
	   # SCMSGS
	   # @explanation
	   # The Samba resource could not validate that the fault monitor
	   # userid exists.
	   # @user_action
	   # Check that the correct fault monitor userid was used when
	   # registering the Samba resource and that the userid really exists.
	   scds_syslog -p daemon.error -t $(syslog_tag) -m \
		"check_smbd - Couldn't retrieve faultmonitor-user <%s> from the nameservice" \
		"${USER}"
	   return 1
	fi

	# If smbclient can't connect, there could be network/server issues causing smbclient to fail.
	# These errors maybe transient and correctable within a few seconds. Therefore before calling
	# an error (which would result in a restart), we will retry the smbclient request within 85% 
	# of the available Probe_timeout less 15 seconds, which is approximately the timeout for the
	# first smbclient failure. 
	#
	# However, doing this is only realistic if Probe_timeout=30 seconds or more. If Probe_timeout
	# is below 30 seconds then we'll just try smbclient once, which was the previous behaviour.

	if [ "${PROBE_TIMEOUT}" -ge 30 ]
	then
	   MAX_PROBE_TIMEOUT=`/usr/bin/expr \( ${PROBE_TIMEOUT} \* 85 \/ 100 \) \- 15 `
	else
	   MAX_PROBE_TIMEOUT=2
	fi

	while [ "${MAX_PROBE_TIMEOUT}" -ge 2 ]
	do
	   if [ "$CALLER" = "GDS" ]
	   then
		${SMBCLIENT} '\\'${NETBIOSNAME}'\'${SAMBA_FMRESOURCE} -s ${SMBCONF} -U `/usr/bin/echo ${SAMBA_FMUSER}` \
		   -c 'pwd;exit' 2>/dev/null | /usr/xpg4/bin/grep -i -w -e ERROR -e FAILED > ${TMPF} 
	   else
		${SMBCLIENT} '//'${NETBIOSNAME}'/'${SAMBA_FMRESOURCE} -s ${SMBCONF} -U `/usr/bin/echo ${SAMBA_FMUSER}` \
	  	   -c 'pwd;exit' 2>/dev/null | /usr/xpg4/bin/grep -i -w -e ERROR -e FAILED > ${TMPF}
	   fi

	   if [ -s "${TMPF}" ]
	   then
  		# SCMSGS
  		# @explanation
  		# The Samba resource's fault monitor checks that the Samba
  		# server is working by using the smbclient program. However
  		# this test failed to connect to the Samba server.
  		# @user_action
  		# No user action is needed. The Samba server will be
  		# restarted. However, examine the other syslog messages
  		# occurring at the same time on the same node, to see if the
  		# cause of the problem can be identified.
  		scds_syslog -p daemon.error -t $(syslog_tag) -m \
		   "check_smbd - Samba server <%s> not working, failed to connect to samba-resource <%s>" \
		   "${NETBIOSNAME}" "${SAMBA_FMRESOURCE}"
                rc=1
		MAX_PROBE_TIMEOUT=`expr ${MAX_PROBE_TIMEOUT} - 2`
		sleep 1
	   else
               	rc=0
		MAX_PROBE_TIMEOUT=0
		debug_message "check_smbd - Samba is working"
	   fi
	done

        debug_message "Function: check_smbd - End"
	return ${rc}
}

check_winbind()
{
        debug_message "Function: check_winbind - Begin"
        ${SET_DEBUG}

	# Ask name service if the fault monitor user can be retrieved
	# Note, Winbind's ${SAMBA_FMUSER}=${FMUSER}, see samba_register.

        if [ -z "`/usr/bin/getent passwd ${SAMBA_FMUSER}`" ]
        then
	   # SCMSGS
	   # @explanation
	   # The Winbind userid cannot be retrieved.
	   # @user_action
	   # No user action is needed. The Samba server will be restarted.
	   scds_syslog -p daemon.error -t $(syslog_tag) -m \
		"check_winbind - User <%s> can't be retrieved by the nameservice" \
		"${SAMBA_FMUSER}"
	   rc=100
	else
	   debug_message "check_winbind - Winbind is working"
	   rc=0
	fi

	debug_message "Function: check_winbind - End"
	return ${rc}
}
