#!/usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)samba_smf_remove.ksh	1.6	07/06/06 SMI"
#

rc=0
MYNAME=`basename $0`

typeset opt

while getopts 'z:f:' opt
do
        case "${opt}" in
                z)      ZONE=${OPTARG};;
                f)      FMRI=${OPTARG};;
                *)      echo "ERROR: ${MYNAME} Option ${OPTARG} unknown"
			echo "Usage: ${MYNAME} -z <zone> -f <FMRI>"
                        exit 1;;
        esac
done

if [ -z ${ZONE} ]
then
	/usr/bin/printf "ERROR: ${MYNAME} -z <zone> not specified\n"
	rc=1
fi

if [ -z ${FMRI} ]
then
	/usr/bin/printf "ERROR: ${MYNAME} -f <FMRI> not specified\n"
	rc=1
fi

[ "${rc}" != 0 ] && exit ${rc}

if /usr/sbin/zlogin ${ZONE} svcadm disable ${FMRI}
then
	/usr/bin/printf "${FMRI} found in ${ZONE}\n"
	/usr/bin/printf "${FMRI} disabled in ${ZONE}\n"
fi
	
if /usr/sbin/zlogin ${ZONE} svccfg delete ${FMRI}
then
	/usr/bin/printf "${FMRI} deleted in ${ZONE}\n"
fi

exit ${rc}
