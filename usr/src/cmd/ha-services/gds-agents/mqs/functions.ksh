#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident   "@(#)functions.ksh 1.11     08/10/10 SMI"
#

PKG=SUNWscmqs
MYNAME=`basename $0`
MYDIR=`dirname $0`
MYTMPDIR=/var/tmp
LOGFILE=${MYTMPDIR}/${RESOURCE}_logfile
TASK_COMMAND=""
PATTERN="\<${QMGR}\>|\<m${QMGR}\>"
RESOURCE_PROJECT_NAME=""
SCLOGGER=/usr/cluster/lib/sc/scds_syslog
LOGGER=/usr/bin/logger

syslog_tag()
{
	${SET_DEBUG}
	print "SC[${PKG:-??}.${COMPONENT:-??}.${METHOD:-??}]:${RESOURCEGROUP:-??}:${RESOURCE:-??}"
}

scds_syslog()
{
	if [ -f ${SCLOGGER} ]
	then
	   ${SCLOGGER} "$@" &
	else
	   while getopts 'p:t:m' opt
	   do
		case "${opt}" in
		   t) TAG=${OPTARG};;
		   p) PRI=${OPTARG};;
		esac
	   done
	
	   shift $((${OPTIND} - 1))
	   LOG_STRING=`/usr/bin/printf "$@"`
	   ${LOGGER} -p ${PRI} -t ${TAG} ${LOG_STRING}
	fi
}

debug_message()
{
	if [ "${DEBUG}" = "${RESOURCE}" -o "${DEBUG}" = "ALL" ]
	then
	   SET_DEBUG="set -x"
	   DEBUG_TEXT=${1}

	   scds_syslog -p daemon.debug -t $(syslog_tag) -m \
	      "%s" "${DEBUG_TEXT}"
	else
	   SET_DEBUG=
	fi
}

log_message()
{
	debug_message "Function: log_message - Begin"
	${SET_DEBUG}

	if [ -s "${LOGFILE}" ]
	then
	   PRIORITY=$1
	   HEADER=$2

	   strings ${LOGFILE} > ${LOGFILE}.copy

	   while read MSG_TXT
	   do
		scds_syslog -p daemon.${PRIORITY} -t $(syslog_tag) -m \
		   "%s - %s" \
		   "${HEADER}" "${MSG_TXT}"
	   done < ${LOGFILE}.copy

	   cat /dev/null > ${LOGFILE}
	fi

	debug_message "Function: log_message - End"
}

set_redirection()
{
	debug_message "Function: set_redirection - Begin"
	${SET_DEBUG}

	if /usr/bin/getent passwd ${USERID} | /usr/bin/awk -F: '{print $7}' | /usr/bin/grep csh > /dev/null
	then
	   OUTPUT=">& ${LOGFILE}"
	else
	   OUTPUT="> ${LOGFILE} 2>&1"
	fi

	debug_message "Function: set_redirection - End"
}

query_pids()
{
	debug_message "Function: query_pids - Begin"
	${SET_DEBUG}

	# Usage: query_pids "parameter"
	# 
	# "parameter" is passed to /usr/xpg4/bin/grep
	#
	# Currently, query_pids() is used as follows,
	#
	# query_pids "-w amqzxma0"
	# query_pids "-v runmqlsr"
	# query_pids "-c -E 'amqzmuc0|amqzxma0'"
	# query_pids "-c -E 'amqzxma0|amqhasmx'"
	# query_pids "-w runmqsc"
	# query_pids "-w amqpcsea"
	# query_pids "-w runmqlsr | /usr/bin/grep '\\-p ${port}' "
	# query_pids "-w 'runmqchi -m ${QMGR} -q ${INITQNAME}' "
	# query_pids "-w 'runmqtrm -m ${QMGR} -q ${TRMQ}' "
	# 
	# Output: rc=0 if the query is TRUE, else rc=1
	#	  ${pids} contains the queried process ids
	#
	# If being run on Solaris 10, query_pids() matches the 
	# correct zonename, otherwise zones are ignored for
	# pre-Solaris 10 systems.
	# 
	# query_pids() matches against the correct queue manager
	# name and will exclude "queue.manager" names, i.e. exclude
	# a similar queue manager name that uses the "." notation.
	#
	# To avoid excluding Channel Initiator or Trigger Monitor
	# queues that use the "." notation, "excluding" is turned 
	# off when running the "test" and "stop" methods for the
	# Channel Initiator or Trigger Monitor queries.
	# 
	# Queries for Channel Initiator or Trigger Monitor supply
	# distinct pattern matches that include the queue manager
	# name, which means we do not need to exclude "." entries.

	rc=1

	EXCLUDE=
	PROCESS=
	[ "${1}" ] && PROCESS="| /usr/xpg4/bin/grep ${1}"

	# If ${QMGR} is not a "queue.manager" notation then 
	# exclude any ${QMGR}"." and ${QMGR}"!" entries

	if ! /usr/bin/echo ${QMGR} | /usr/bin/grep '\.' > /dev/null
	then
	   EXCLUDE="| /usr/xpg4/bin/grep -v -E '${QMGR}\.|${QMGR}\!'"
	fi

	if [ "${METHOD}" != "start" ]
	then
	   if [ "${COMPONENT}" = "chi" -o "${COMPONENT}" = "trm" ]
	   then
		EXCLUDE=
	   fi
	fi
	
	if [ -x /sbin/zonename ]
	then
	   pids=`/usr/bin/ps -u mqm -o pid,args,zone | /usr/bin/grep " ${ZONENAME}$" | \
		eval /usr/xpg4/bin/grep -E '${PATTERN}' ${EXCLUDE} ${PROCESS} | /usr/bin/awk '{print $1}'`
	else
	   pids=`/usr/bin/ps -u mqm -o pid,args | \
		eval /usr/xpg4/bin/grep -E '${PATTERN}' ${EXCLUDE} ${PROCESS} | /usr/bin/awk '{print $1}'`
	fi

	[ "${pids}" ] && rc=0

	debug_message "Function: query_pids - End"
	return ${rc}
}

validate()
{
	debug_message "Function: validate - Begin"
	${SET_DEBUG}

	rc=0

	if /usr/bin/getent passwd mqm > /dev/null
	then
	   debug_message "Validate - User mqm exists"
	else
	   # SCMSGS
	   # @explanation
	   # The userid mqm does not exist.
	   # @user_action
	   # The userid mqm must exist within /etc/passwd.
	   scds_syslog -p daemon.error -t $(syslog_tag) -m \
		"Validate - User mqm does not exist"
	   rc=1
	fi

	if /usr/bin/getent group mqm > /dev/null
	then
	   debug_message "Validate - Group mqm exists"
	else
	   # SCMSGS
	   # @explanation
	   # The group mqm does not exist.
	   # @user_action
	   # The group mqm must exist within /etc/group.
	   scds_syslog -p daemon.error -t $(syslog_tag) -m \
		"Validate - Group mqm does not exist"
	   rc=1
	fi

	if [ "${USERID}" = "mqm" ]
	then
	   debug_message "Validate - Userid is mqm"
	else
	   pgroup=`/usr/bin/getent passwd ${USERID} | /usr/bin/awk -F: '{print $4}'`

	   if [ "${pgroup}" ]
	   then
		if /usr/bin/getent group ${pgroup} | /usr/bin/awk -F: '{ if ($1 == "mqm") print $3}' | \
		   /usr/bin/grep "^${pgroup}$" > /dev/null
		then
		   debug_message "Validate - Primary group for userid ${USERID} is mqm"
		else
		   if /usr/bin/getent group mqm | /usr/bin/awk -F: '{print $4}' | \
		      /usr/bin/grep ${USERID} > /dev/null
		   then
			debug_message "Validate - Secondary group for userid ${USERID} is mqm"
		   else
			scds_syslog -p daemon.error -t $(syslog_tag) -m \
			   "Validate - Userid %s is not a member of group mqm" \
			   "${USERID}"
			rc=1
		   fi
		fi
	   else
		scds_syslog -p daemon.error -t $(syslog_tag) -m \
		   "Validate - Userid %s is not a valid userid" \
		   "${USERID}"
		rc=1
	   fi
	fi

	if [ -d /opt/mqm -a -d /var/mqm ]
	then
	   debug_message "Validate - WebSphere MQ file systems ok"
	else
	   # SCMSGS
	   # @explanation
	   # The required WebSphere MQ file systems, /opt/mqm and /var/mqm are
	   # not defined.
	   # @user_action
	   # Ensure that /opt/mqm and /var/mqm are defined.
	   scds_syslog -p daemon.error -t $(syslog_tag) -m \
		"Validate - WebSphere MQ file systems not defined"
	   rc=1
	fi

	debug_message "Function: validate - End"
	return ${rc}
}

start_qmgr()
{
	debug_message "Function: start_qmgr - Begin"
	${SET_DEBUG}

	/usr/bin/rm ${LOGFILE}

	if [ "${CALLER}" = "GDS" ]
	then
	   # Turn off PMF restart if ${QMGR} has been manually started
	   check_qmgr debug
	   rc=$?

	   if [ "${rc}" -eq 0 ]
	   then
		/usr/bin/sleep 120 &
		/usr/cluster/bin/pmfadm -s ${RESOURCEGROUP},${RESOURCE},0.svc
	
		# SCMSGS
		# @explanation
		# The specified WebSphere MQ queue manager has been started
		# manually.
		# @user_action
		# None required. Informational message.
		scds_syslog -p daemon.notice -t $(syslog_tag) -m \
		   "start_qmgr - WebSphere MQ %s was manually started" \
		   "${QMGR}"

		return 0
	   fi
	fi
		
	SC3_XARM=

	if [ ! -z ${DB2INSTANCE} ]; then
	   SC3_XARM="env DB2INSTANCE=${DB2INSTANCE}"
	fi

	if [ ! -z ${ORACLE_HOME} ]; then
	   SC3_XARM="$SC3_XARM env ORACLE_HOME=${ORACLE_HOME}"
	fi

	if [ ! -z ${ORACLE_SID} ]; then
	   SC3_XARM="$SC3_XARM env ORACLE_SID=${ORACLE_SID}"
	fi

	# Note, mqver is derived within control_mqs and allows us to 
	# differentiate between WebSphere MQ v5.3 and v6.0 or later.

	if [ "${mqver}" -ge 6 ] 
	then
	   SERVICES=`/usr/bin/echo ${SERVICES} | /usr/bin/tr -s '[:lower:]' '[:upper:]'`
	   if [ "${SERVICES}" = "NO" ] 
	   then
		SERVICES=-ns
	   else
		SERVICES=
	   fi
	else
	   SERVICES=
	fi

	if [ "${CALLER}" = "GDS" ]
	then
	   /usr/bin/su - ${USERID} -c "${TASK_COMMAND} env SC3_COMMAND=${START_COMMAND} ${SC3_XARM} strmqm ${SERVICES} ${QMGR} ${OUTPUT}" > /dev/null
	else
	   env SC3_COMMAND=${START_COMMAND} ${SC3_XARM} strmqm ${SERVICES} ${QMGR} > ${LOGFILE} 2>&1
	   rc=$?
	   [ "${rc}" -eq 0 ] && smf_wait_for_online "qmgr NOSU"
	fi

	rc=$?
	log_message notice "strmqm rc(${rc})"

	debug_message "Function: start_qmgr - End"
	return ${rc}
}

check_start()
{
	debug_message "Function: check_start - Begin"
	${SET_DEBUG}

	# Allow "wait_for_online" to only test after the 
	# specified method for ${RESOURCE} has finished

	rc=1

	# Here $1 is set when check_start is called from within control_wmqs, e.g.
	# check_start "start-qmgr stop-qmgr" etc.

	for component in $1
	do
	   if [ "${CALLER}" = "GDS" ]
	   then
		if [ -x /sbin/zonename ]
		then
		   /usr/bin/pgrep -z ${ZONENAME} -f "${component} .*-R ${RESOURCE} " >/dev/null 2>&1
		else
		   /usr/bin/pgrep -u root -f "${component} .*-R ${RESOURCE} " >/dev/null 2>&1
		fi
	   else
		# Here we are being called by SMF where ${component} has already been broken down,
		# i.e. The SMF start method uses "control_wmqs start qmgr ${RESOURCE}" and not
		# start-qmgr which is being passed as $1, therefore we need to amend ${component}.
		#
		# Note, that ${RESOURCE}, when called by SMF, reflects the FMRI which when using a 
		# failover zone is the same as the Sun Cluster resource name.

		component=`/usr/bin/echo ${component} | /usr/bin/tr "-" " "`
		/usr/bin/pgrep -u ${USERID} -f "${component} ${RESOURCE} " >/dev/null 2>&1 
	   fi

	   rc=$?

	   if [ "${rc}" -eq 0 ]
	   then
		rc=100
		break
	   else
		rc=0
	   fi
	done

	debug_message "Function: check_start - End"
	return ${rc}
}

check_qmgr()
{
	# create_tdq does the following
	#       MQCONN - Connect to ${QMGR}
	#       MQOPEN - Open a temporary dynamic queue
	#       MQPUT1 - Put a test message to that queue
	#       MQDISC - Disconnect from ${QMGR}

	debug_message "Function: check_qmgr - Begin"
	${SET_DEBUG}

	NOSU=$1
	LEVEL=notice

	# start_qmgr() also calls this function when the resource is being started, 
	# however when this happens "debug" is passed to us, simply so that we don't
	# output a notice message when the resource is being started.

	[ "${NOSU}" = "debug" ] && LEVEL=debug

	# When called by GDS or via the sczsmf probe component we must "su" as both of
	# those environments are running as root. However this function is also called
	# when the SMF FMRI is enabled, either by sczsmf start component which issues
	# svcadm enable -t <FMRI>. In this environment we are running as ${USERID}, so 
	# we must not "su" again.

	if [ "${NOSU}" != "NOSU" ]
	then
	   check_qmgr_status=`/usr/bin/su ${USERID} -c "${MYDIR}/../mgr/bin/create_tdq ${QMGR}"`
	else
	   check_qmgr_status=`${MYDIR}/../mgr/bin/create_tdq ${QMGR}`
	fi

	rc=$?
	if [ "${rc}" -ne 0 ]
	then
	   # SCMSGS
	   # @explanation
	   # The Queue Manager check has failed.
	   # @user_action
	   # None required. Informational message.
	   scds_syslog -p daemon.${LEVEL} -t $(syslog_tag) -m \
		"check_qmgr - <%s>" \
		"${check_qmgr_status}"
	   rc=100
	fi

	debug_message "Function: check_qmgr - End"
	return ${rc}
}

stop_qmgr()
{
	debug_message "Function: stop_qmgr - Begin"
	${SET_DEBUG}

	# For WMQ v6 or later queue managers, if the Listener (runmqlsr) is started before the 
	# queue manager (regardless if the queue manager was started with -ns) then runmqlsr 
	# starts process amqzmgr0 (Process Controller). 
	# 
	# Because of this we ignore amqzmgr0 when stopping the queue manager, as amqzmgr0
	# gets stopped when runmqlsr is stopped.
	#
	# Note, mqver is derived within control_mqs and allows us to 
	# differentiate between WebSphere MQ v5.3 and v6.0 or later.

	if [ "${mqver}" -ge 6 ] 
	then
	   IGNORE="| /usr/bin/grep -v amqzmgr0"
	else
	   IGNORE=
	fi
	
	if [ "${CALLER}" = "SMF" ]
	then
	   IGNORE="${IGNORE} | /usr/bin/grep -v control_wmqs"
	fi

	for func in endmqm_immediate \
	   	    endmqm_preemptive \
	            cleanup_qmgr
	do
	   case ${func} in
		endmqm_immediate)	((LOOP_TIME=${STOP_TIMEOUT} *40/100));;
		endmqm_preemptive)	((LOOP_TIME=${STOP_TIMEOUT} *30/100));;
		cleanup_qmgr)		((LOOP_TIME=${STOP_TIMEOUT} *5/100));;
	   esac

	   # Bail out of running endmqm if we've lost amqzxma0

	   if [ "${func}" = "endmqm_immediate" -o "${func}" = "endmqm_preemptive" ]
	   then
		if query_pids "-w amqzxma0"
		then
		   if [ "${func}" = "endmqm_immediate" ]
		   then
			run_endmqm -i 
		   else
			run_endmqm -p
		   fi

		   # Test if any ${QMGR} pids are still running

		   while (( ${LOOP_TIME} > 0 ))
		   do
			if query_pids "-v runmqlsr ${IGNORE}"
			then
			   /usr/bin/sleep 1
			   LOOP_TIME=`expr ${LOOP_TIME} - 1`
			else
			   break
			fi
		   done

		   # Log any endmqm messages
		   log_message notice endmqm

		   # Test if ${func} used all it's allocated time and ignore
		   # any runmqlsr pids. 

		   if ! query_pids "-v runmqlsr ${IGNORE}"
		   then
			break
		   fi
		fi
	   else
		${func}
	   fi
	done

	debug_message "Function: stop_qmgr - End"
 	return 0
}

run_endmqm()
{
	debug_message "Function: run_endmqm - Begin"
	${SET_DEBUG}

	OPTION=${1}
	query_pids "-w runmqsc"

	for i in ${pids}
	do
	   /usr/bin/kill -9 ${i}
	   debug_message "MQSC command prompt (runmqsc) for ${QMGR} found and killed"
	done

	# Allow for multiple databases that can be included within global units of work.

	SC3_XARM=

	if [ ! -z ${DB2INSTANCE} ]; then
	   SC3_XARM="env DB2INSTANCE=${DB2INSTANCE}"
	fi

	if [ ! -z ${ORACLE_HOME} ]; then
	   SC3_XARM="${SC3_XARM} env ORACLE_HOME=${ORACLE_HOME}"
	fi

	if [ ! -z ${ORACLE_SID} ]; then
	   SC3_XARM="${SC3_XARM} env ORACLE_SID=${ORACLE_SID}"
	fi

	#
	# See IBM Technote 1159674 for MQCHL_NO_TERM_FFST
	#
	# Turns off FFSTs when a channel job dies before the channel status
	# table is updated. It will not affect any other FDCs being generated
	#

	if [ "${CALLER}" = "GDS" ]
	then
	   /usr/bin/su - ${USERID} -c "${TASK_COMMAND} env MQCHL_NO_TERM_FFST=1 env SC3_COMMAND=${STOP_COMMAND} ${SC3_XARM} endmqm ${OPTION} ${QMGR} ${OUTPUT} &" > /dev/null
	else
	   env MQCHL_NO_TERM_FFST=1 env SC3_COMMAND=${STOP_COMMAND} ${SC3_XARM} endmqm ${OPTION} ${QMGR} > ${LOGFILE} 2>&1 &
	fi

	debug_message "Function: run_endmqm - End"
}

cleanup_qmgr()
{
	debug_message "Function: cleanup_qmgr - Begin"
	${SET_DEBUG}

	# Note, mqver is derived within control_mqs and allows us to 
	# differentiate between WebSphere MQ v5.3 and v6.0 or later.

	if [ "${mqver}" -ge 6 ]
	then
	   pids="amqzmuc0 amqzxma0 amqzfuma amqzlaa0 amqzlsa0 amqzmur0 amqrmppa amqrrmfa amqzdmaa runmqchi amqpcsea"
	   IGNORE="-v runmqlsr | /usr/bin/grep -v amqzmgr0 "
	else
	   pids="amqhasmx amqharmx amqzllp0 amqzlaa0 amqzfuma amqzxma0 amqrrmfa amqzdmaa"
	   IGNORE="-v runmqlsr "
	fi

	for pid in ${pids}
	do
	   query_pids "-w ${pid}"
	   [ "${pids}" ] && /usr/bin/kill -9 ${pids}
	done

	# Cleanup any other pids that may have been missed, while ignoring
	# any runmqlsr processes.

	if [ "${CALLER}" = "SMF" ]
	then
	   IGNORE="${IGNORE} | /usr/bin/grep -v control_wmqs"
	fi

	query_pids "${IGNORE}"
	[ "${pids}" ] && /usr/bin/kill -9 ${pids}

	# If we're in this part of the code then either some critical WMQ processes have failed or
	# the allocated time to stop the queue manager (immediate|preemptive) has expired. In this
	# case we follow a specific order to kill processes (as above). 
	#
	# However, whatever causes this part of the code to be run, the queue manager may incur an 
	# unexpected error (RC 71) and perhaps "AMQ8041 The queue manager cannot be restarted or 
	# deleted because processes, that were previosly connected, are still running."
	#
	# In this regard to try and avoid any unexpected errors, all associated listeners (runmqlsr)
	# for this queue manager are stopped and will be restarted by the lsr componet, if required.
	#	
	# Note: Ending all the associated queue manager listeners will only occur if the queue
	# manager used the allocated stop time or critical WMQ processes failed, as detailed above.

	if [ "${CALLER}" = "GDS" ]
	then
	   /usr/bin/su - ${USERID} -c "${TASK_COMMAND} endmqlsr -w -m ${QMGR} ${OUTPUT}" > /dev/null
	else
	   endmqlsr -w -m ${QMGR} > ${LOGFILE} 2>&1 
	fi
	   
	log_message notice endmqlsr

	# SCMSGS
	# @explanation
	# All the WebSphere MQ processess for the specified Queue Manager have
	# been stopped.
	# @user_action
	# None required. Informational message.
	scds_syslog -p daemon.notice -t $(syslog_tag) -m \
	   "All WebSphere MQ (%s) processes stopped" \
	   "${QMGR}"

	debug_message "Function: cleanup_qmgr - End"
}

cleanup_ipc()
{
	# Only if ${CLEANUP} is YES
	CLEANUP=`/usr/bin/echo ${CLEANUP} | /usr/bin/tr -s '[:lower:]' '[:upper:]'`
	[ "${CLEANUP}" != "YES" ] && return 0

	debug_message "Function: cleanup_ipc - Begin"
	${SET_DEBUG}

	# Cleanup any IPC shared memory segments however only if
	#
	#       - The shared memory segment(s) are owned by
	#               OWNER=mqm and GROUP=mqm
	#       - The shared memory has no attached processes
	#       - The CPID and LPID processes are not running

	flag=
	# Allow some time for IPC to cleanup normally 
	/usr/bin/sleep 2

	if [ "${DEBUG}" = "${RESOURCE}" -o "${DEBUG}" = "ALL" ]
	then
	   debug_message "IPC Status BEFORE removal of non-attached segments created by user mqm"

	   if [ -x /sbin/zonename ]
	   then
		/usr/bin/ipcs -mcopbZ | /usr/bin/grep " ${ZONENAME}$" > ${LOGFILE}
	   else
		/usr/bin/ipcs -mcopb > ${LOGFILE}
	   fi

	   log_message debug ipcs
	fi

	if [ -x /sbin/zonename ]
	then
	   # Perform cleanup on a Solaris 10 node

	   /usr/bin/ipcs -mcopbZ | /usr/bin/grep " ${ZONENAME}$" | /usr/bin/awk ' \
		{if (NF == 13 && $5 == "mqm" && $6 == "mqm" && $9 == 0 ) print $2,$11,$12; else \
		if (NF == 12 && $4 == "mqm" && $5 == "mqm" && $8 == 0 ) print $1,$10,$11 }' | \
	   while read SHMID CPID LPID
	   do
		if /usr/bin/ps -p ${LPID} -o zone | /usr/bin/grep " ${ZONENAME}$" > /dev/null
		then
		   debug_message "WebSphere MQ SHMID: ${SHMID} - LPID ${LPID} is running"
		else
		   if /usr/bin/ps -p ${CPID} -o zone | /usr/bin/grep " ${ZONENAME}$" > /dev/null
		   then
			debug_message "WebSphere MQ SHMID: ${SHMID} - CPID ${CPID} is running"
		   else
			SHMID=`/usr/bin/echo ${SHMID} | /usr/bin/tr 'm' ' '`

			# As the initial ipcs -mcopbZ is only a snapshot in time, the queue manager
			# may have already cleaned up. Therefore the following attempt to remove a 
			# shared memory segment may fail with "not found". To prevent misleading 
			# console messages stdout/stderr is redirected to ${LOGFILE} via ${OUTPUT},
			# refer to set_redirection() for more details. ${LOGFILE} is not output to 
			# syslog and instead is overwritten by other functions that use ${OUTPUT}.

			if [ "${CALLER}" = "GDS" ]
			then
			   /usr/bin/su mqm -c "/usr/bin/ipcrm -z ${ZONENAME} -m ${SHMID} ${OUTPUT}" > /dev/null
			else
			   /usr/bin/ipcrm -z ${ZONENAME} -m ${SHMID} ${OUTPUT}
			fi

			debug_message "WebSphere MQ SHMID: ${SHMID} - removed"

			flag=deleted
		   fi
		fi
	   done
	else
	   # Perform cleanup on a pre-Solaris 10 node

	   /usr/bin/ipcs -mcopb | /usr/bin/awk ' \
		{if (NF == 12 && $5 == "mqm" && $6 == "mqm" && $9 == 0 ) print $2,$11,$12; else \
		if (NF == 11 && $4 == "mqm" && $5 == "mqm" && $8 == 0 ) print $1,$10,$11 }' | \
	   while read SHMID CPID LPID
	   do
		if /usr/bin/ps -p ${LPID} > /dev/null
		then
		   debug_message "WebSphere MQ SHMID: ${SHMID} - LPID ${LPID} is running"
		else
		   if /usr/bin/ps -p ${CPID} > /dev/null
		   then
			debug_message "WebSphere MQ SHMID: ${SHMID} - CPID ${CPID} is running"
		   else
			SHMID=`/usr/bin/echo ${SHMID} | /usr/bin/tr 'm' ' '`

			/usr/bin/su mqm -c "/usr/bin/ipcrm -m ${SHMID} ${OUTPUT}" > /dev/null
			debug_message "WebSphere MQ SHMID: ${SHMID} - removed"

		 	flag=deleted
		   fi
		fi
	   done
	fi

	if [ "${flag}" ]; then
	   # SCMSGS
	   # @explanation
	   # All the WebSphere MQ shared memory segments that were not being
	   # used have been removed.
	   # @user_action
	   # None required. Informational message.
	   scds_syslog -p daemon.notice -t $(syslog_tag) -m \
		"All WebSphere MQ non-attached IPC shared memory segments removed"
	fi

	debug_message "IPC Status AFTER removal of non-attached segments created by user mqm"

	if [ "${DEBUG}" = "${RESOURCE}" -o "${DEBUG}" = "ALL" ]
	then
	   if [ -x /sbin/zonename ]
	   then
		/usr/bin/ipcs -mcopbZ | /usr/bin/grep " ${ZONENAME}$" > ${LOGFILE}
	   else
		/usr/bin/ipcs -mcopb > ${LOGFILE}
	   fi

	   log_message debug ipcs
	fi

	debug_message "Function: cleanup_ipc - End"
	return 0
}

start_lsr()
{
	debug_message "Function: start_lsr - Begin"
	${SET_DEBUG}

	/usr/bin/rm ${LOGFILE} 

	PORT_LIST=`/usr/bin/echo ${PORT} | tr '/' ' '`

	ipaddr=""

	# Note, mqver is derived within control_mqs and allows us to 
	# differentiate between WebSphere MQ v5.3 and v6.0 or later.

	if [ "${mqver}" -ge 6 ]
	then
	   [ ${IPADDR} ] && ipaddr="-i ${IPADDR}"
	fi

	for port in ${PORT_LIST}
	do
	   if [ "${CALLER}" = "GDS" ]
	   then
		/usr/bin/su - ${USERID} -c "${TASK_COMMAND} runmqlsr -t tcp -p ${port} ${ipaddr} -b ${BACKLOG} -m ${QMGR} ${OUTPUT} &" > /dev/null
	   else
		runmqlsr -t tcp -p ${port} ${ipaddr} -b ${BACKLOG} -m ${QMGR} > ${LOGFILE} 2>&1 &
	   fi

	   # SCMSGS
	   # @explanation
	   # The specified WebSphere MQ Listener has been started.
	   # @user_action
	   # None required. Informational message.
	   scds_syslog -p daemon.notice -t $(syslog_tag) -m \
		"WebSphere MQ Listener for port %s started" \
		"${port}"

	   check_output
	   log_message error runmqlsr
	done

	debug_message "Function: start_lsr - End"
	return 0
}

stop_lsr()
{
	debug_message "Function: stop_lsr - Begin"
	${SET_DEBUG}

	# To accommodate multiple port entries within a resource we must
	# kill each runmqlsr/port associated with this resource instead 
	# of issuing endmqlsr which would stop ALL runmqlsr programs for 
	# this ${QMGR}, as there maybe multiple SC listener resources with 
	# different port numbers.

	PORT_LIST=`/usr/bin/echo ${PORT} | tr '/' ' '`

	for port in ${PORT_LIST}
	do
	   query_pids "-w runmqlsr | /usr/bin/grep '\\-p ${port}' "
	   rc=$?

	   if [ "${rc}" -eq 0 ]
	   then
		[ "${pids}" ] && /usr/bin/kill -9 ${pids}

		# SCMSGS
		# @explanation
		# The specified WebSphere MQ Listener has been stopped.
		# @user_action
		# None required. Informational message.
		scds_syslog -p daemon.notice -t $(syslog_tag) -m \
		   "WebSphere MQ Listener for port %s stopped" \
		   "${port}"
	   fi
	done

	debug_message "Function: stop_lsr - End"
}

check_lsr()
{
	debug_message "Function: check_lsr - Begin"
	${SET_DEBUG}

	# For multiple port entries within a resource we need to
	# check each runmqlsr/port combination and restart any
	# failed runmqlsr/port programs from the PROBE method. 
	# A conventional restart (STOP/START) would restart all
	# rummqlsr programs associated with this resource.

	PORT_LIST=`/usr/bin/echo ${PORT} | tr '/' ' '`

	ipaddr=""

	# Note, mqver is derived within control_mqs and allows us to 
	# differentiate between WebSphere MQ v5.3 and v6.0 or later.

	if [ "${mqver}" -ge 6 ]
	then
	   [ ${IPADDR} ] && ipaddr="-i ${IPADDR}"
	fi

	# If a single port is used by the runmqlsr resource then we do not
	# want the resource restarted by the PROBE method. Instead the default
	# restart behaviour is required. 

	SINGLE_PORT=`/usr/bin/echo ${PORT_LIST} | /usr/bin/awk '{if (NF == 1) print $1}'`

	if [ "${SINGLE_PORT}" ]
	then
	   query_pids "-w runmqlsr | /usr/bin/grep '\\-p ${SINGLE_PORT}' "
	   rc=$?
	else
	   rc=0

	   for port in ${PORT_LIST}
	   do
		query_pids "-w runmqlsr | /usr/bin/grep '\\-p ${port}' "
		rc_lsr=$?

		if [ "${rc_lsr}" -ne 0 ]
		then
		   # When called by GDS or via the sczsmf probe component we must "su" as both of
                   # those environments are running as root.

		   /usr/bin/su - ${USERID} -c "${TASK_COMMAND} runmqlsr -t tcp -p ${port} ${ipaddr} -b ${BACKLOG} -m ${QMGR} ${OUTPUT} &" > /dev/null

		   # SCMSGS
		   # @explanation
		   # The specified WebSphere MQ Listener has been restarted.
		   # @user_action
		   # None required. Informational message.
		   scds_syslog -p daemon.notice -t $(syslog_tag) -m \
			"WebSphere MQ Listener for port %s restarted." \
	 		"${port}"

		   check_output
		   log_message error runmqlsr
	   	fi
	   done
	fi

	[ "${rc}" -ne 0 ] && rc=100

	debug_message "Function: check_lsr - End"
	return ${rc}
}

check_output()
{
	debug_message "Function: check_output - Begin"
	${SET_DEBUG}

	# Log any AMQ error messages found in ${LOGFILE}
	# log_message only outputs if ${LOGFILE} is not empty

	/usr/bin/sleep 1
	/usr/bin/grep AMQ ${LOGFILE} | /usr/bin/tee ${LOGFILE} > /dev/null
	rc=0; [ -s "${LOGFILE}" ] && rc=1

	debug_message "Function: check_output - End"
	return ${rc}
}

check_restart()
{
	debug_message "Function: check_restart - Begin"
	${SET_DEBUG}

	# Preserve the restart dependency check
	flag=
	rc=1

	while [ "${rc}" -ne 0 ]
	do
	   # Note, mqver is derived within control_mqs and allows us to 
	   # differentiate between WebSphere MQ v5.3 and v6.0 or later.

	   if [ "${mqver}" -ge 6 ] 
	   then
		query_pids "-c -E 'amqzmuc0|amqzxma0'"
	   else
		query_pids "-c -E 'amqzxma0|amqhasmx'"
	   fi

	   if [ "${pids}" -eq 2 ]
	   then
		if [ "${CALLER}" = "GDS" ]
		then
		   /usr/bin/echo "ping qmgr" | /usr/bin/su - ${USERID} -c "runmqsc ${QMGR} ${OUTPUT}" > /dev/null
		else
		   /usr/bin/echo "ping qmgr" | runmqsc ${QMGR} > ${LOGFILE} 2>&1 
		fi

		rc=$?

		if [ "${rc}" -ne 0 ]
		then
		   # Return 100 for chi, csv and trm test methods
		   if [ "${METHOD}" = "test" ]
		   then
			return 100
		   fi

		   if [ "${flag}" = "set" ]
		   then
			flag=set
			# SCMSGS
			# @explanation
			# A WebSphere MQ component is waiting for the Queue
			# Manager.
			# @user_action
			# None required. Informational message.
			scds_syslog -p daemon.notice -t $(syslog_tag) -m \
			   "Waiting for WebSphere MQ Queue Manager"
		   fi
		   /usr/bin/sleep 5
		fi
	   else
		/usr/bin/sleep 5
		rc=1
	   fi
	done

	if [ "${METHOD}" != "test" ]
	then
	   # SCMSGS
	   # @explanation
	   # The Queue Manager is now available.
	   # @user_action
	   # None required. Informational message.
	   scds_syslog -p daemon.notice -t $(syslog_tag) -m \
		"WebSphere MQ Queue Manager available"
	fi

	debug_message "Function: check_restart - End"
}

start_chi()
{
	debug_message "Function: start_chi - Begin"
	${SET_DEBUG}

	/usr/bin/rm ${LOGFILE}

	check_restart

	if [ "${CALLER}" = "GDS" ]
	then
	   /usr/bin/su - ${USERID} -c "${TASK_COMMAND} runmqchi -m ${QMGR} -q ${INITQNAME} ${OUTPUT} &" > /dev/null
	else
	   runmqchi -m ${QMGR} -q ${INITQNAME} > ${LOGFILE} 2>&1 &
	fi

	# To avoid "AMQ9509: Program cannot open queue manager object."
	# the start is retried every 5% of START_TIMEOUT.
      
	/usr/bin/sleep 2
	check_chi
	rc=$?

	if [ "${rc}" -ne 0 ] 
	then
	   while (( ${START_TIMEOUT} > 0 ))
	   do
		FIVE_PERCENT=`expr \( ${START_TIMEOUT} / 100 \* 5 \)`
		START_TIMEOUT=`expr ${START_TIMEOUT} - ${FIVE_PERCENT} - 2`
		/usr/bin/sleep ${FIVE_PERCENT}

		if [ "${CALLER}" = "GDS" ]
		then
		   /usr/bin/su - ${USERID} -c "${TASK_COMMAND} runmqchi -m ${QMGR} -q ${INITQNAME} ${OUTPUT} &" > /dev/null
		else
		   runmqchi -m ${QMGR} -q ${INITQNAME} > ${LOGFILE} 2>&1 &
		fi
	
		/usr/bin/sleep 2
		check_chi
		rc=$?

		[ "${rc}" -eq 0 ] && break
	   done
	fi

	# SCMSGS
	# @explanation
	# The specified WebSphere MQ Channel Initiator has been started.
	# @user_action
	# None required. Informational message.
	scds_syslog -p daemon.notice -t $(syslog_tag) -m \
	   "WebSphere MQ Channel Initiator %s started" \
	   "${INITQNAME}"

	check_output
	rc=$?
	log_message error runmqchi

	debug_message "Function: start_chi - End"
	return ${rc}
}

stop_chi()
{
	debug_message "Function: stop_chi - Begin"
	${SET_DEBUG}

	query_pids "-w 'runmqchi -m ${QMGR} -q ${INITQNAME}' "
	[ "${pids}" ] && /usr/bin/kill -9 ${pids}

	# SCMSGS
	# @explanation
	# The specified WebSphere MQ Channel Initiator has been stopped.
	# @user_action
	# None required. Informational message.
	scds_syslog -p daemon.notice -t $(syslog_tag) -m \
	   "stop_chi - WebSphere MQ Channel Initiator %s stopped" \
	   "${INITQNAME}"

	debug_message "Function: stop_chi - End"
	return 0
}

check_chi()
{
	debug_message "Function: check_chi - Begin"
	${SET_DEBUG}

	query_pids "-w 'runmqchi -m ${QMGR} -q ${INITQNAME}' "
	rc=$?
	
	[ "${rc}" -ne 0 ] && rc=100

	debug_message "Function: check_chi - End"
	return ${rc}
}

check_csv()
{
	debug_message "Function: check_csv - Begin"
	${SET_DEBUG}

	query_pids "-w amqpcsea"
	rc=$?

	[ "${rc}" -ne 0 ] && rc=100

	debug_message "Function: check_csv - End"
	return ${rc}
}

start_csv()
{
	debug_message "Function: start_csv - Begin"
	${SET_DEBUG}

	/usr/bin/rm ${LOGFILE}

	check_restart

	# For WMQ v6 or later queue managers only.
	#
	# The Command Server can be started as follows, either
	#    strmqm <qmgr>
	#	or
	#    strmqcsv <qmgr>  
	#   
	# In either case, the Command Server (amqpcsea) is started under the Process 
	# Controller (amqzmgr0). However, it appears that amqzmgr0 does not restart
	# amqpcsea if amqpcsea fails, so it is worthwhile making the Command Server
	# highly available using the csv component. 
	# 
	# Previously, i.e. for WMQ v5.3, amqpcsea was started under amqzxma0, however
	# with WMQ v6 or later, amqpcsea is now started under amqzmgr0. So, for WMQ v6
	# or later, if the csv component is being used to manage the Command Server,
	# we will loose our pmftag, so we must start a WMQ v6 or later Command Server
	# without pmf.
	#
	# Furthermore, if the Command Server fails (WMQ v5.3), we do not  want to restart
	# immediately, otherwise "AMQ8510: Command server queue is open, try later"
	# will result. In this sense and considering the above we will not use a pmftag 
	# for the Command Server (WMQ v5.3 or v6 or later).

	if [ "${CALLER}" = "GDS" ]
	then
	   /usr/bin/sleep 120 &
	   /usr/cluster/bin/pmfadm -s ${RESOURCEGROUP},${RESOURCE},0.svc

	   /usr/bin/su - ${USERID} -c "${TASK_COMMAND} strmqcsv ${QMGR} ${OUTPUT}" > /dev/null
	else
	   strmqcsv ${QMGR} > ${LOGFILE} 2>&1
	fi

	# To avoid "AMQ8510: Command server queue is open, try later" 
	# the start is retried every 5% of START_TIMEOUT.
      
	/usr/bin/sleep 2
	check_csv
	rc=$?

	if [ "${rc}" -ne 0 ] 
	then
	   while (( ${START_TIMEOUT} > 0 ))
	   do
		FIVE_PERCENT=`expr \( ${START_TIMEOUT} / 100 \* 5 \)`
		START_TIMEOUT=`expr ${START_TIMEOUT} - ${FIVE_PERCENT} - 2`
		/usr/bin/sleep ${FIVE_PERCENT}
		rc=1

		if [ "${CALLER}" = "GDS" ]
		then
		   /usr/bin/su - ${USERID} -c "${TASK_COMMAND} strmqcsv ${QMGR} ${OUTPUT}" > /dev/null
		else
		   strmqcsv ${QMGR} > ${LOGFILE} 2>&1
		fi

		/usr/bin/sleep 2
		check_csv
		rc=$?

		[ "${rc}" -eq 0 ] && break
	   done
	fi

	rc=$?
	if [ "${rc}" -eq 0 ]
	then
	   log_message notice strmqcsv
	else
	   log_message error strmqcsv
	fi

	debug_message "Function: start_csv - End"
	return ${rc}
}

smf_wait_for_online()
{
	debug_message "Function: smf_wait_for_online - Begin"
	${SET_DEBUG}

	# Provides a wait_for_online feature for SMF 
	component=$1

	while (( ${START_TIMEOUT} > 0 ))
	do
	   if check_${component}
	   then
		rc=0
		break
	   else
		FIVE_PERCENT=`expr \( ${START_TIMEOUT} / 100 \* 5 \) `
		START_TIMEOUT=`expr ${START_TIMEOUT} - ${FIVE_PERCENT}`
		/usr/bin/sleep ${FIVE_PERCENT}
	   fi

	   rc=1
	done

	debug_message "Function: smf_wait_for_online - End"
	return ${rc}
}

stop_csv()
{
	debug_message "Function: stop_csv - Begin"
	${SET_DEBUG}

	query_pids "-w amqpcsea"
	rc=$?

	if [ "${rc}" -eq 0 ]
	then
	   if [ "${CALLER}" = "GDS" ]
	   then
		/usr/bin/su - ${USERID} -c "${TASK_COMMAND} endmqcsv ${QMGR} ${OUTPUT}" > /dev/null
	   else
		# Here the Command Server was started by SMF. To prevent a subsequent SMF
		# [ Method "stop" exited with status 20 ], the stop will always return 0, 
		# but only if the caller was SMF 

		endmqcsv ${QMGR} > ${LOGFILE} 2>&1
	   fi

	   rc=$?
	   if [ "${rc}" -eq 0 ]
	   then
		log_message notice endmqcsv
	   else
	   [ "${CALLER}" = "SMF" ] && rc=0
		log_message error endmqcsv
	   fi
	else
	   # The Command Server process is not running, This avoids the message from endmqcsv
	   # AMQ8196: WebSphere MQ command server already stopped.
	   rc=0
	fi

	debug_message "Function: stop_csv - End"
	return ${rc}
}

start_trm()
{
	debug_message "Function: start_trm - Begin"
	${SET_DEBUG}

	/usr/bin/rm ${LOGFILE}

	check_restart

	TRMQ_LIST=`/usr/bin/echo ${TRIGGER_MONITOR} | /usr/bin/tr -s '[:lower:]' '[:upper:]'`
		
	if [ "${TRMQ_LIST}" = "FILE" ]
	then
	   TRMQ_LIST=
	   while read QUEUE
	   do
		if ! /usr/bin/echo $QUEUE | /usr/bin/grep '^#' > /dev/null
		then
		   TRMQ_LIST="${TRMQ_LIST} ${QUEUE}"
		fi
	   done < `dirname $0`/../trm/etc/${QMGR}_trm_queues
	else
	   TRMQ_LIST=${TRIGGER_MONITOR}
	fi

	for TRMQ in ${TRMQ_LIST}
	do
	   if [ "${CALLER}" = "GDS" ]
	   then
		/usr/bin/su - ${USERID} -c "${TASK_COMMAND} nohup runmqtrm -m ${QMGR} -q ${TRMQ} ${OUTPUT} &" > /dev/null
	   else
		nohup runmqtrm -m ${QMGR} -q ${TRMQ} > ${LOGFILE} 2>&1 &
	   fi

	   # SCMSGS
	   # @explanation
	   # The specified WebSphere MQ Trigger Monitor has been started.
	   # @user_action
	   # None required. Informational message.
	   scds_syslog -p daemon.notice -t $(syslog_tag) -m \
		"WebSphere MQ Trigger Monitor %s started" \
		"${TRMQ}"

	   check_output
	   rc=$?
	   if [ "${rc}" -eq 0 ]
	   then
		log_message notice runmqtrm
	   else
		log_message error runmqtrm
	   fi
	done

	debug_message "Function: start_trm - End"
	return 0
}

check_trm()
{
	debug_message "Function: check_trm - Begin"
	${SET_DEBUG}

	TRMQ_LIST=`/usr/bin/echo ${TRIGGER_MONITOR} | /usr/bin/tr -s '[:lower:]' '[:upper:]'`

	if [ "${TRMQ_LIST}" = "FILE" ]
	then
	   TRMQ_LIST=
	   while read QUEUE
	   do
		if ! /usr/bin/echo $QUEUE | /usr/bin/grep '^#' > /dev/null
		then
		   TRMQ_LIST="${TRMQ_LIST} ${QUEUE}"
		fi
	   done < `dirname $0`/../trm/etc/${QMGR}_trm_queues
	else
	   SINGLE_TRMQ=${TRIGGER_MONITOR}
	fi

	# If a single trigger monitor is used by the runmqtrm resource then we do not
	# want the resource restarted by the PROBE method. Instead the default
	# restart behaviour is required.

	if [ "${SINGLE_TRMQ}" ]
	then
	   query_pids "-w 'runmqtrm -m ${QMGR} -q ${SINGLE_TRMQ}' "
	   rc=$?
	else
	   rc=0
	   for TRMQ in ${TRMQ_LIST}
   	   do
		# Check if ${TRMQ} is already running
		query_pids "-w 'runmqtrm -m ${QMGR} -q ${TRMQ}' "
		rc_trm=$?

		if [ "${rc_trm}" -ne 0 ] 
		then
		   # When called by GDS or via the sczsmf probe component we must "su" as both of
		   # those environments are running as root. 

		   /usr/bin/su - ${USERID} -c "${TASK_COMMAND} nohup runmqtrm -m ${QMGR} -q ${TRMQ} ${OUTPUT} &" > /dev/null

		   # SCMSGS
		   # @explanation
		   # The specified WebSphere MQ Trigger Monitor has been
		   # restarted.
		   # @user_action
		   # None required. Informational message.
		   scds_syslog -p daemon.notice -t $(syslog_tag) -m \
			"WebSphere MQ Trigger Monitor %s restarted" \
			"${TRMQ}"

		   check_output
		   rc_trm=$?
		   if [ "${rc_trm}" -eq 0 ]
		   then
			log_message notice runmqtrm
		   else
			log_message error runmqtrm
		   fi
		fi
	   done
	fi

	[ "${rc}" -ne 0 ] && rc=100

	debug_message "Function: check_trm - End"
	return 0
}

stop_trm()
{
	debug_message "Function: stop_trm - Begin"
	${SET_DEBUG}

	TRMQ_LIST=`/usr/bin/echo ${TRIGGER_MONITOR} | /usr/bin/tr -s '[:lower:]' '[:upper:]'`

	if [ "${TRMQ_LIST}" = "FILE" ]
	then
	   TRMQ_LIST=
	   while read QUEUE
	   do
		if ! /usr/bin/echo $QUEUE | /usr/bin/grep '^#' > /dev/null
		then
		   TRMQ_LIST="${TRMQ_LIST} ${QUEUE}"
		fi
	   done < `dirname $0`/../trm/etc/${QMGR}_trm_queues
	else
	   TRMQ_LIST=${TRIGGER_MONITOR}
	fi

	for TRMQ in ${TRMQ_LIST}
	do
	   query_pids "-w 'runmqtrm -m ${QMGR} -q ${TRMQ}' "
	   [ "${pids}" ] && /usr/bin/kill -9 ${pids}

	   # SCMSGS
	   # @explanation
	   # The specified WebSphere MQ Trigger Monitor has been stopped.
	   # @user_action
	   # None required. Informational message.
	   scds_syslog -p daemon.notice -t $(syslog_tag) -m \
		"stop_trm - WebSphere MQ Trigger Monitor %s stopped" \
		"${TRMQ}"
	done

	debug_message "Function: stop_trm - End"
}

