#!/usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident   "@(#)control_wmqs.ksh 1.5     07/08/07 SMI"
# 
# Usage GDS: <options> <parameter1> <parameter2>
#
# Usage SMF: <parameter1> <parameter2> <parameter3>
# 
#	<options>: -R <resource> -G <resourcegroup> etc.
#	parameter1: start | stop | test 
#	parameter2: chi | csv | qmgr | lsr | trm
#       parameter3: <FMRI>

MYNAME=`basename $0`
MYDIR=`dirname $0`

typeset opt

while getopts 'R:G:Q:U:C:D:O:I:S:E:P:B:M:N:A:' opt
do
        case "${opt}" in
		R)      RESOURCE=${OPTARG};;
		G)      RESOURCEGROUP=${OPTARG};;
		Q)      QMGR=${OPTARG};;
		U)      USERID=${OPTARG};;
		C)      CLEANUP=${OPTARG};;
		D)      DB2INSTANCE=${OPTARG};;
		O)      ORACLE_HOME=${OPTARG};;
		I)      if [ "${CLEANUP}" ] 
			then
			   ORACLE_SID=${OPTARG}
			else
			   INITQNAME=${OPTARG}
			fi;;
		S)      START_COMMAND=${OPTARG};;
		E)      STOP_COMMAND=${OPTARG};;
		P)      PORT=${OPTARG};;
		B)      BACKLOG=${OPTARG};;
		M)      TRIGGER_MONITOR=${OPTARG};;
		N)      SERVICES=${OPTARG};;
		A)      IPADDR=${OPTARG};;

		*)      exit 1;;
	esac
done

if [ "${OPTIND}" -gt 1 ]
then
	# Called by GDS
	CALLER=GDS

	shift $((${OPTIND} -1))
else
	# Called by SMF
	CALLER=SMF

	. /lib/svc/share/smf_include.sh

	SMF_FMRI=${3}
fi

METHOD=${1}
COMPONENT=${2}

if [ "${COMPONENT}" = "qmgr" ]
then
	. ${MYDIR}/../mgr/etc/config
else
	. ${MYDIR}/../${COMPONENT}/etc/config
fi

[ -z "${USERID}" ] && USERID=mqm
[ -z "${CLEANUP}" ] && CLEANUP=YES

if [ "${CALLER}" = "GDS" ]
then
	. ${MYDIR}/functions
	
	# Perform all the scha* calls
	TASK_COMMAND=""

	if [ "${METHOD}" = "stop" -a "${COMPONENT}" = "qmgr" ]
	then
	   STOP_TIMEOUT=`/usr/cluster/bin/scha_resource_get -O Stop_timeout \
	      -R ${RESOURCE} -G ${RESOURCEGROUP} `
	fi

	if [ "${METHOD}" = "start" ]
	then
	   if [ "${COMPONENT}" = "chi" -o "${COMPONENT}" = "csv" ]
	   then
		START_TIMEOUT=`/usr/cluster/bin/scha_resource_get -O Start_timeout \
		   -R ${RESOURCE} -G ${RESOURCEGROUP} `
	   fi
	fi

	# Determine the newtask project for start and stop

	if [ "${METHOD}" != "test" ]
	then
	   if [ `/usr/bin/uname -r` != "5.8" ]
	   then
	      # Retrieve the resource project name
	      RESOURCE_PROJECT_NAME=`/usr/cluster/bin/scha_resource_get \
		-R ${RESOURCE} -G ${RESOURCEGROUP} -O RESOURCE_PROJECT_NAME`
		
	      if [ -z "${RESOURCE_PROJECT_NAME}" -o "${RESOURCE_PROJECT_NAME}" = "default" ]
	      then
		# Retrieve the resource group project name 
		RESOURCE_PROJECT_NAME=`/usr/cluster/bin/scha_resourcegroup_get \
		   -G ${RESOURCEGROUP} -O RG_PROJECT_NAME`
	      fi	
	   fi

	   # Validate that ${USERID} belongs to the ${RESOURCE_PROJECT_NAME}
	   if [ "${RESOURCE_PROJECT_NAME}" ]
	   then
	      PROJ_MEMBER=`/usr/bin/projects ${USERID} | /usr/bin/egrep "^${RESOURCE_PROJECT_NAME} | ${RESOURCE_PROJECT_NAME} | ${RESOURCE_PROJECT_NAME}$|^${RESOURCE_PROJECT_NAME}$"`

	      if [ -z "${PROJ_MEMBER}" ]
	      then
		scds_syslog -p daemon.notice -t $(syslog_tag) -m \
		   "%s - The user %s does not belongs to project %s" \
		   "${MYNAME}" "${USERID}" "${RESOURCE_PROJECT_NAME}"
	      else
		TASK_COMMAND="/usr/bin/newtask -p ${RESOURCE_PROJECT_NAME}"
	      fi
	   fi
	fi
else
	case "${COMPONENT}" in
	   chi) for i in RESOURCE RESOURCEGROUP QMGR INITQNAME
		do
		   export $i=`/usr/bin/svcprop -p parameters/$i ${SMF_FMRI}`
		done
		;;
	   csv) for i in RESOURCE RESOURCEGROUP QMGR
		do
		   export $i=`/usr/bin/svcprop -p parameters/$i ${SMF_FMRI}`
		done
		;;
	   lsr) for i in RESOURCE RESOURCEGROUP QMGR PORT IPADDR BACKLOG
		do
		   export $i=`/usr/bin/svcprop -p parameters/$i ${SMF_FMRI}`
		done
		;;
	  qmgr) for i in RESOURCE RESOURCEGROUP QMGR SERVICES CLEANUP DB2INSTANCE \
		   ORACLE_HOME ORACLE_SID START_COMMAND STOP_COMMAND STOP_TIMEOUT
		do
		   export $i=`/usr/bin/svcprop -p parameters/$i ${SMF_FMRI}`
		done
		;;
	   trm) for i in RESOURCE RESOURCEGROUP QMGR TRIGGER_MONITOR
		do
		   export $i=`/usr/bin/svcprop -p parameters/$i ${SMF_FMRI}`
		done
		;;
	esac

	START_TIMEOUT=`/usr/bin/svcprop -p start/timeout_seconds ${SMF_FMRI}`
	USERID=`/usr/bin/svcprop -p start/user ${SMF_FMRI}`
	PROJECT=`/usr/bin/svcprop -p start/project ${SMF_FMRI}`

	. ${MYDIR}/functions
fi

debug_message "Method: ${MYNAME} - Begin"
${SET_DEBUG}

if [ -x /opt/mqm/bin/dspmqver ] 
then
   CHECKMQ=/opt/mqm/bin/dspmqver
else
   CHECKMQ=/opt/mqm/bin/mqver
fi
	
mqver=`${CHECKMQ} | /usr/bin/grep Version | /usr/bin/awk '{print $2}' | \
   /usr/bin/awk -F. '{print $1}' | /usr/bin/cut -c1`

[ -x /sbin/zonename ] && ZONENAME=`/sbin/zonename`

set_redirection

case "${METHOD}" in
	start)
	   case "${COMPONENT}" in
		chi)    start_chi
			rc=$?
			;;
		csv)    start_csv
			rc=$?
			;;
		lsr)    start_lsr
			rc=$?
			;;
		qmgr)	validate
			rc=$?
			[ "${rc}" -ne 0 ] && return ${rc}
			start_qmgr
			rc=$?
			;;
		trm)    start_trm
			rc=$?
			;;
	esac
	;;

	stop)
	   case "${COMPONENT}" in
		chi)    stop_chi
			rc=$?
			;;
		csv)    stop_csv
			rc=$?
			;;
		lsr)    stop_lsr
			rc=$?
			;;
		qmgr)	stop_qmgr
			cleanup_ipc
			rc=$?
			;;
		trm)    stop_trm
			rc=$?
			;;
	esac
	;;

	test)
	   case "${COMPONENT}" in
		chi)    check_chi
			rc=$?
			;;
		csv)    check_csv
			rc=$?
			;;
		lsr)	check_start "start-lsr stop-lsr"
			rc=$?
			[ "${rc}" -eq 100 ] && return ${rc}
			check_lsr
			rc=$?
			;;
		qmgr) 	check_start "start-qmgr stop-qmgr"
			rc=$?
			[ "${rc}" -eq 100 ] && return ${rc}
			check_qmgr
			rc=$?
			;;
		trm)	check_start "start-trm stop-trm"
			rc=$?
			[ "${rc}" -eq 100 ] && return ${rc}
			check_trm
			rc=$?
			;;
	esac
	;;
esac

debug_message "Method: ${MYNAME} - End"
exit $rc
