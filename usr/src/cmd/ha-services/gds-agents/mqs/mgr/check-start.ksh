#!/usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident   "@(#)check-start.ksh 1.3     07/08/07 SMI"
#

if [ -z "$SC3_COMMAND" ] 
then
	echo "Request to run <$0 "$@"> within Sun Cluster has been refused"
	exit 1
fi

if [ ! -x "$SC3_COMMAND" ]
then
	echo "Bogus request to run non-existent executable: $SC3_COMMAND"
	exit 1
fi

exec "$SC3_COMMAND" "$@"

exit 1
