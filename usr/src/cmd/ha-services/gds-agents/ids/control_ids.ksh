#!/usr/bin/ksh 
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident   "@(#)control_ids.ksh 1.3     08/05/27 SMI"
#
# Usage GDS: <options> <parameter1> <parameter2>
#
#	<options>: -R <resource> -G <resourcegroup> etc.
#	parameter1: start | stop | probe | validate
#	parameter2: ids

MYNAME=$(/usr/bin/basename $0)
MYDIR=$(/usr/bin/dirname $0)

. ${MYDIR}/../etc/config

typeset opt

while getopts 'R:G:U:D:C:S:H:' opt
do
        case "${opt}" in
		R)      RESOURCE=${OPTARG};;
		G)      RESOURCEGROUP=${OPTARG};;
		U)      USERID=${OPTARG};;
		D)	INFORMIXDIR=${OPTARG};;
		C)	ONCONFIG=${OPTARG};;
		S)	INFORMIXSERVER=${OPTARG};;
		H)	INFORMIXSQLHOSTS=${OPTARG};;
		*)      exit 1;;
	esac
done

shift $((${OPTIND} -1))

METHOD=${1}
COMPONENT=${2}

. ${MYDIR}/functions
	
# Perform all the scha* calls

TASK_COMMAND=""

if [ "${METHOD}" = "start" ]
then
   START_TIMEOUT=$(/usr/cluster/bin/scha_resource_get -O START_TIMEOUT \
      -R ${RESOURCE} -G ${RESOURCEGROUP} )
fi

if [ "${METHOD}" = "stop" ]
then
   STOP_TIMEOUT=$(/usr/cluster/bin/scha_resource_get -O STOP_TIMEOUT \
      -R ${RESOURCE} -G ${RESOURCEGROUP} )
fi

# Retrieve the resource project name so that we can run any 
# Informix commands under the specified user's project.

RESOURCE_PROJECT_NAME=$(/usr/cluster/bin/scha_resource_get \
   -R ${RESOURCE} -G ${RESOURCEGROUP} -O RESOURCE_PROJECT_NAME)
		
if [ -z "${RESOURCE_PROJECT_NAME}" -o "${RESOURCE_PROJECT_NAME}" = "default" ]
then
   # Retrieve the resource group project name 

   RESOURCE_PROJECT_NAME=$(/usr/cluster/bin/scha_resourcegroup_get \
	-G ${RESOURCEGROUP} -O RG_PROJECT_NAME)
fi	

# Validate that ${USERID} belongs to the ${RESOURCE_PROJECT_NAME}

if [ -n "${RESOURCE_PROJECT_NAME}" ]
then
   PROJ_MEMBER=$(/usr/bin/projects ${USERID} | /usr/bin/egrep "^${RESOURCE_PROJECT_NAME} | \
	${RESOURCE_PROJECT_NAME} | ${RESOURCE_PROJECT_NAME}$|^${RESOURCE_PROJECT_NAME}$")
fi

if [ -z "${PROJ_MEMBER}" ]
then
   # SCMSGS
   # @explanation
   # The userid does not belong to the specified project.
   # @user_action
   # Ensure the userid exists within the project. Check that 
   # you have the correct userid and project name.
   scds_syslog -p daemon.notice -t $(syslog_tag) -m \
	"%s - The user %s does not belong to project %s" \
	"${MYNAME}" "${USERID}" "${RESOURCE_PROJECT_NAME}"
   return 1
else
   TASK_COMMAND="/usr/bin/newtask -p ${RESOURCE_PROJECT_NAME}"
fi

debug_message "Method: ${MYNAME} - Begin"
${SET_DEBUG}

[ -x /sbin/zonename ] && ZONENAME=$(/sbin/zonename)

set_redirection

case "${METHOD}" in
	start)
		cleanup_ipc
		start_ids
		rc=$?
		;;
		
	stop)
		stop_ids
		rc=0
		;;

	probe)
		get_state
		check_ids
		rc=$?
		;;
	validate)
		validate
		rc=$?
		;;
esac

debug_message "Method: ${MYNAME} - End"
exit ${rc}
