#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)functions.ksh	1.11	09/04/21 SMI"
#

# DHCP VARIABLES


CLINFO="/usr/sbin/clinfo -n"
LOCALNODENAME=$(/bin/uname -n)
IPMPSTATUS="/usr/cluster/bin/scstat -i -h ${LOCALNODENAME}"
INDHCPD="/usr/lib/inet/in.dhcpd -i"
PNTADM="/usr/sbin/pntadm"
PNMPTOR="/usr/cluster/bin/pnmptor"
S34DHCP=/etc/rc3.d/S34dhcp
DSVCLOCKD="/usr/lib/inet/dsvclockd"
TOUPPER="/usr/bin/tr -s '[:lower:]' '[:upper:]'"
DHCPCLIENT=/opt/SUNWscdhc/bin/dhcpclient
DHCPCFG=/etc/inet/dhcpsvc.conf
NETWORKS_ALL="`echo $NETWORKS | tr '/' ' '`"
NETWORKTYPE=nafo
TMPFILE=/tmp/dhcp_${RESOURCE}.tmp
IPNR=$(/bin/getent hosts ${LOCALNODENAME} | /bin/awk '{print $1}' | /bin/sort -u)
IPMPSTAT=/sbin/ipmpstat

# TFTP VARIABLES

TFTP=/bin/tftp
INETDCFG=/etc/inet/inetd.conf
TFTPDACTIVE=`/bin/cat ${INETDCFG} | /bin/grep -v '#' | /bin/grep tftp`
TFTPHOST=localhost
TFTPTIMEOUT=10

# DHCPCLIENT VARIABLES

DHCPCLIENT=/opt/SUNWscdhc/bin/dhcpclient
DHCPCLIENTTIMEOUT=15


# OTHER VARIABLES

SCLOGGER=/usr/cluster/lib/sc/scds_syslog
PKG=SUNWscdhc
METHOD=`basename $0`
HATIMERUN=/usr/cluster/bin/hatimerun

syslog_tag()
{
        #
        # Data Service message format
        #

        $SET_DEBUG

        print "SC[${PKG:-??}.${METHOD:-??}]:${RESOURCEGROUP:-??}:${RESOURCE:-??}"
}

scds_syslog()
{

        #
        # Log a message
        #

        $SET_DEBUG

        $SCLOGGER "$@" &
}

debug_message()
{
        #
        # Output a debug message to syslog if required
        #

        if [ "$DEBUG" = "$RESOURCE" -o "$DEBUG" = "ALL" ]
        then
                SET_DEBUG="set -x"

                DEBUG_TEXT=$1

                scds_syslog -p daemon.debug -t $(syslog_tag) -m \
                        "%s" "$DEBUG_TEXT"
        else
                SET_DEBUG=
        fi
}

validate_options()
{
        #
        # Ensure all options are set
        #

        for i in RESOURCE RESOURCEGROUP NETWORKS
        do
                case $i in
                        RESOURCE)
                        if [ -z $RESOURCE ]; then
                                # SCMSGS
                                # @explanation
                                # The specified option is not set within
                                # either the Start, Stop, Probe or Validate
                                # command
                                # @user_action
                                # The syslog tag identifies the agent script
                                # that produced this message. Fix the relevant
                                # Start, Stop, Probe or Validate command with
                                # the appropriate option. The easiest way to
                                # accomplish this is to reregister the
                                # resource.
                                scds_syslog -p daemon.error -t $(syslog_tag) -m \
                                "ERROR: Option -%s not set" "R"
                                exit 1
                        fi;;

                        RESOURCEGROUP)
                        if [ -z $RESOURCEGROUP ]; then
                                scds_syslog -p daemon.error -t $(syslog_tag) -m \
                                "ERROR: Option -%s not set" "G"
                                exit 1
                        fi;;

                        NETWORKS)
                        if [ -z ${STATICDHCP} -a -z $NETWORKS ]; then
                                scds_syslog -p daemon.error -t $(syslog_tag) -m \
                                "ERROR: Option -%s not set" "N"
                                exit 1
                        fi;;

                esac
        done
}

validate()
{

	#
	# Validate DHCP
	#
	
        debug_message "Function: validate - Begin"
	$SET_DEBUG

	rc_validate=0

	#
	# Validate that dhcpsvc.conf exists
	#

	if [ ! -f "${DHCPCFG}" ]
	then
   		# SCMSGS
   		# @explanation
   		# The DHCP resource could not validate that
   		# /etc/inet/dhcpsvc.conf exists.
   		# @user_action
   		# Ensure that /etc/inet/dhcpsvc.conf exists.
   		scds_syslog -p daemon.error -t $(syslog_tag) -m \
			"Validate - DHCP config file %s does not exist" \
			"${DHCPCFG}"
		rc_validate=1
		return 
	else
		debug_message "Validate - DHCP config file ${DHCPCFG} exists"
	fi

	#
	# Save the old Path and source the config file
	#

	# /etc/inet/dhcpsvc.conf has a PATH variable which gets overwritten
	# when we source /etc/inet/dhcpsvc.conf. PATH gets reinstated at the
	# end of validate or if rc_validate=1 

	OLDPATH=${PATH}

	. ${DHCPCFG}

	#
	# Validate Daemon_enabled is true
	#

	if [ "`/usr/bin/echo ${DAEMON_ENABLED} | ${TOUPPER}`" != "TRUE" ]
	then
   		# SCMSGS
   		# @explanation
   		# The DHCP resource requires that that the
   		# /etc/inet/dhcpsvc.conf file has DAEMON_ENABLED=TRUE.
   		# @user_action
   		# Ensure that /etc/inet/dhcpsvc.conf has DAEMON_ENABLED=TRUE
   		# by configuring DHCP appropriately, i.e. as defined within
   		# the Sun Cluster Data Service for DHCP.
   		scds_syslog -p daemon.error -t $(syslog_tag) -m \
			"Validate - DHCP is not enabled (DAEMON_ENABLED)"
		rc_validate=1
		return 
	else
		debug_message "Validate - DHCP is enabled (DAEMON_ENABLED)"
	fi

	#
	# Validate SUNWfiles or SUNWbinfiles
	#

	if [ "`/usr/bin/echo ${RESOURCE} | ${TOUPPER}`" != "SUNWFILES" -a "`/usr/bin/echo ${RESOURCE} | ${TOUPPER}`" != "SUNWBINFILES" ]
	then
   		# SCMSGS
   		# @explanation
   		# The DHCP resource requires that that the
   		# /etc/inet/dhcpsvc.conf file has RESOURCE=SUNWfiles or
   		# SUNWbinfiles.
   		# @user_action
   		# Ensure that /etc/inet/dhcpsvc.conf has RESOURCE=SUNWfiles or
   		# SUNWbinfiles by configuring DHCP appropriately, i.e. as
   		# defined within the Sun Cluster Data Service for DHCP.
   		scds_syslog -p daemon.error -t $(syslog_tag) -m \
			"Validate - Only SUNWfiles or SUNWbinfiles are supported"
		rc_validate=1
		return 
	else
		debug_message "Validate - SUNWfiles or SUNWbinfiles are defined"
	fi

	#
	# Validate that Run_mode is server
	#

	if [ "`/usr/bin/echo ${RUN_MODE} | ${TOUPPER}`" != "SERVER" ]
	then
   		# SCMSGS
   		# @explanation
   		# The DHCP resource requires that that the
   		# /etc/inet/dhcpsvc.conf file has RUN_MODE=SERVER.
   		# @user_action
   		# Ensure that /etc/inet/dhcpsvc.conf has RUN_MODE=SERVER by
   		# configuring DHCP appropriately, i.e. as defined within the
   		# Sun Cluster Data Service for DHCP.
   		scds_syslog -p daemon.error -t $(syslog_tag) -m \
			"Validate - RUN_MODE has to be server"
		rc_validate=1
		return 
	else
		debug_message "Validate - RUN_MODE is server"
	fi

	#
	# Validate the path is a directory
	#

	if [ ! -d "${PATH}" ]
	then
   		# SCMSGS
   		# @explanation
   		# The DHCP resource could not validate that the DHCP directory
   		# defined in the /etc/inet/dhcpsvc.conf file for the PATH
   		# variable exists.
   		# @user_action
   		# Ensure that /etc/inet/dhcpsvc.conf has the correct entry for
   		# the PATH variable by configuring DHCP appropriately, i.e. as
   		# defined within the Sun Cluster Data Service for DHCP.
   		scds_syslog -p daemon.error -t $(syslog_tag) -m \
			"Validate - DHCP directory %s does not exist" \
			"${PATH}"
		rc_validate=1
		return 
	else
		debug_message "Validate - DHCP directory ${PATH} exists"
	fi
	
	#
	# Validate that S34dhcp is not active
	#

	if [ -f "${S34DHCP}" ]
	then
   		# SCMSGS
   		# @explanation
   		# The DHCP resource validates that /etc/rc3.d/S34dhcp is not
   		# active and achieves this by deleting/etc/rc3.d/S34dhcp.
   		# @user_action
   		# No user action is needed. /etc/rc3.d/S34dhcp will be
   		# deleted.
   		scds_syslog -p daemon.notice -t $(syslog_tag) -m \
			"Validate - De-activating %s, by removing it" \
			"${S34DHCP}"
	
   		/bin/rm -f ${S34DHCP}
	else
		debug_message "Validate - ${S34DHCP} does not exist which is ok"
	fi

	# USE OLDPATH
	PATH=${OLDPATH}

        #
	# Validate that if CGTPUSED is SET the variable INTERFACES has to be set
        #

        if [ ! -z "${CGTPUSED}" -a -z "${INTERFACES}" ]; then

                        # SCMSGS
                        # @explanation
                        # If USE_CGTP=TRUE was set within
                        # /opt/SUNWscdhc/util/dhcp_config then the INTERFACES
                        # within /etc/inet/dhcpsvc.conf needs to be set.
                        # @user_action
                        # If USE_CGTP=TRUE is required, then ensure that the
                        # INTERFACES variable is set within
                        # /etc/inet/dhcpsvc.conf. If USE_CGTP=TRUE is
                        # required, then ensure that the INTERFACES variable
                        # is set within /etc/inet/dhcpsvc.conf. Refer to
                        # dhcpsvc.conf(4) man page for further information on
                        # INTERFACES.
                        scds_syslog -p daemon.notice -t $(syslog_tag) -m \
                        "Validate - The keyword INTERFACES have to be set in %s when -T is being used" \
                        "${DHCPCFG}"

			rc_validate=1
			return 

        else
                debug_message "Validate - The keyword INTERFACES is set when -T is being used"
        fi

        #
	# Validate tftp
	#

	if [ ! -z "${TFTPFILE}" ]; then

		if [ -z "${TFTPDACTIVE}" ]; then
                        # SCMSGS
                        # @explanation
                        # If TFTPTESTFILE= was set within
                        # /opt/SUNWscdhc/util/dhcp_config then the tftp daemon
                        # needs to be activated witihin /etc/inet/inetd.conf.
                        # @user_action
                        # If TFTPTESTFILE= is required, then ensure that tftp
                        # is activated within /etc/inet/inetd.conf.
                        scds_syslog -p daemon.notice -t $(syslog_tag) -m \
                        "Validate - tftpd daemon has to be activated in %s when -F is being used" \
                        "${INETDCFG}"

			rc_validate=1
			return

        	else
               		debug_message "Validate - tftpd is active"
        	fi


		if [ ! -s "${TFTPFILE}" ]; then
                        # SCMSGS
                        # @explanation
                        # The file specified in TFTPTESTFILE= within
                        # /opt/SUNWscdhc/util/dhcp_config doesn't exist or has
                        # a zero length witihin /etc/inet/inetd.conf.
                        # @user_action
                        # If TFTPTESTFILE= is required, then ensure file
                        # specified is correct.
                        scds_syslog -p daemon.notice -t $(syslog_tag) -m \
                        "Validate - The tftp testfile (%s) don't exist in directory (%s) or has filesize of zero bytes" \
                        "`basename ${TFTPFILE}`" "`dirname ${TFTPFILE}`"

			rc_validate=1
			return

        	else
               		debug_message "Validate - tftp testfile (${TFTPFILE}) exists"
        	fi
        fi



	debug_message "Function: validate - End"
}

get_adapter_type()
{
	#
	# Test if CGTP is being used
	#

	debug_message "Function: get_adapter_type - Begin"
	$SET_DEBUG

        if [ ! -z "${CGTPUSED}" ]; then
           NETWORKTYPE=cgtp

	else
		NETWORKTYPE=ipmp
	fi

	debug_message "Function: get_adapter_type - ${NETWORKTYPE} is being used"
	debug_message "Function: get_adapter_type - End"
}
		
get_nafo_adapter()
{
	#
	# Get active adapter and interface ip number
	#

	debug_message "Function: get_nafo_adapter - Begin"
	$SET_DEBUG

     	ADAPTER=`${PNMPTOR} ${NAFO_NODE}`

	debug_message "get_nafo_adapter - Interface=${ADAPTER}"

	debug_message "Function: get_nafo_adapter - End"
}

get_ipmp_adapter()
{
	#
	# Get all interfaces belonging to defined IPMP GROUP
	#
	
	debug_message "Function: get_ipmp_adapter - Begin"
	$SET_DEBUG

        ADAPTER=""
	# Project Clearview changed the previous IPMP behaviour and introduced
	# the /sbin/ipmpstat command.
	# Before Clearview, there was no CLI to retreive the adapter list
	# of a specific IPMP group. Therfore scstat has been used to deduce it.
	# This will no longer work on a system with Clearview.

	if [ -x "${IPMPSTAT}" ]
	then
		ADAPTER_LIST=$(${IPMPSTAT} -o group,groupname -P -g | /bin/nawk -F: -v ipmpgroup="${NAFO_NODE}" '{ if ($2 == ipmpgroup) print $1 }')

	else
        	ADAPTER_LIST=$(${IPMPSTATUS} | /bin/grep "IPMP Group:" | /bin/awk '{print $4,$6,$7}' | /bin/grep ${NAFO_NODE} | /bin/grep Online | /bin/awk '{print $2}')

	fi

        for s1 in $ADAPTER_LIST
        do
          
           if [ -z "${ADAPTER}" ]; then 
             ADAPTER=${s1}
           else
             ADAPTER=${ADAPTER}","${s1}
           fi

  	done

	debug_message "get_ipmp_adapter - Interface=${ADAPTER}"

	debug_message "Function: get_ipmp_adapter - End"
}

get_cgtp_adapter()
{
	#
	# Get all ip-adresses belonging to defined INTERFACES
	#
	
	debug_message "Function: get_cgtp_adapter - Begin"
	$SET_DEBUG

        ADAPTER=${INTERFACES}

	debug_message "get_cgtp_adapter - Interface=${ADAPTER}"

	debug_message "Function: get_cgtp_adapter - End"
}

get_server_ip()
{
	#
	# get the server ip number
	#
	
	debug_message "Function: get_server_ip - Begin"
	$SET_DEBUG

	SRVIPNR=`${PNTADM} -P ${NW} | /usr/bin/awk '{print $4}' | /bin/tail -1`

	rc_pntadm=$?

	if [ "${rc_pntadm}" -ne 0 ]
	then
		scds_syslog -p daemon.error -t $(syslog_tag) -m \ 
			"get_server_ip - pntadm failed rc<%s>" \
			"${rc_pntadm}"
	else
		debug_message "get-server_ip - Server IP=${SRVIPNR}"
	fi

	debug_message "Function: get_server_ip - End"
}

start_dhcp()
{
	#
	# Start DHCP
	#

        debug_message "Function: start_dhcp - Begin"
	$SET_DEBUG
	
	NODEID=`${CLINFO}`
	USED_ADAPTER=""

	if [ -f ${TMPFILE} ]
	then
   		rm -f ${TMPFILE}
	fi

	# IF CGTPUSED AND STATICDHCP IS BEING USED
	# SET USED_ADAPTER=${INTERFACES}

        if [ ! -z "${CGTPUSED}" -a ! -z "${STATICDHCP}" ]; then
          USED_ADAPTER=${INTERFACES}
	fi

	# GET ADEPTER TYPE

	get_adapter_type	

	# PARSE ${NETWORKS_ALL} OPTIONS TO RETRIVE IP-NUMBERS FOR
	# ACTIVE ADAPTERS

	NW_ALL=

	for tab in ${NETWORKS_ALL}
	do
   		NW=`/usr/bin/echo ${tab} | cut -d'@' -f1`
   		NAFO_NODE=`/usr/bin/echo ${tab} | cut -d'@' -f2`
   		ID=`/usr/bin/echo ${tab} | cut -d'@' -f3`

   		if [ "${ID}" = "${NODEID}" ]
		then

			if [ "${NETWORKTYPE}" = "ipmp" ]; then
				get_ipmp_adapter
			elif [ "${NETWORKTYPE}" = "nafo" ]; then
				get_nafo_adapter
                        else
                                get_cgtp_adapter
			fi

			# USED_ADAPTER allows for multiple interfaces so long as we match "${ID}" = "${NODEID}"

			if [ -z "${USED_ADAPTER}" ]
			then
       				USED_ADAPTER=${ADAPTER}
     			else
       				USED_ADAPTER=${USED_ADAPTER}","${ADAPTER}
			fi

			NW_ALL="$NW_ALL $NW"
		fi
	done

	# CHECK IF CHANGE OF OWNER SHIP IS NEEDED

	if [ -z "${STATICDHCP}" ]; then

		for NW in $NW_ALL
		do
			# GET OWNER OF DHCP-TABLES

			get_server_ip

			if [ ! -z "${SRVIPNR}" ]
			then
				# Check which node owns the DHCP table

                		echo ${IPNR} | grep ${SRVIPNR} > /dev/null

				if [ $? -ne 0 ]
				then

					debug_message "start_dhcp - Changing Server IP for network ${NW} from ${SRVIPNR} to ${IPNR}"

					# Dump each client and create a PNTADM batchfile to change the server ip number

					for client in `${PNTADM} -P ${NW} | /usr/bin/awk '{print $3}' | /usr/bin/grep '\.'`
       					do
          					/usr/bin/echo "pntadm -M ${client} -s ${IPNR} ${NW}" >> ${TMPFILE}
       					done
				else
					debug_message "start_dhcp - This node owns the DHCP table entries"
     				fi
			fi
		done
	fi

	# Run the ${TMPFILE} in batch mode to PNTADM

	if [ -f "${TMPFILE}" ]
	then
		debug_message "start_dhcp - Start DHCP batch job to change Server IP address"

  		${PNTADM} -B ${TMPFILE}

		rc_pntadm=$?

		if [ "${rc_pntadm}" -ne 0 ]
		then
			scds_syslog -p daemon.error -t $(syslog_tag) -m \ 
			"start_dhcp - DHCP batch job failed rc<%s>" \
			"${rc_pntadm}"
		else
			debug_message "start_dhcp - DHCP batchjob is finished"
		fi
	fi

	# Start Dhcp daemon

	${INDHCPD} ${USED_ADAPTER}
	St=$?

	if [ ${St} -ne 0 ]
	then
   		# SCMSGS
   		# @explanation
   		# The DHCP resource has tried to start the DHCP server using
   		# in.dhcpd, however this has failed.
   		# @user_action
   		# The DHCP server will be restarted. Examine the other syslog
   		# messages occurring at the same time on the same node, to see
   		# if the cause of the problem can be identified.
   		scds_syslog -p daemon.error -t $(syslog_tag) -m \
			"start_dhcp - %s %s failed" \
			"${INDHCPD}" "${USED_ADAPTER}"
	else
		debug_message "start_dhcp - DHCP started"
	fi

	debug_message "Function: start_dhcp - End"
}

stop_dhcp()
{
	#
	# Stop DHCP
	#

        debug_message "Function: stop_dhcp - Begin"
	$SET_DEBUG

	/bin/pkill -TERM `basename ${DSVCLOCKD}`

	debug_message "Function: stop_dhcp - End"
}

check_dhcp()
{
	# 
	# Probe DHCP
	#

        debug_message "Function: check_dhcp - Begin"
	$SET_DEBUG

	rc_check_dhcp=0
	#
	# Check if the agent is starting
	#

	if [ ! -z "`pgrep start_dhcp`" ]
	then
		debug_message "check_dhcp - DHCP is starting, exiting"
		rc_check_dhcp=100
		return
	fi

        # IF CGTPUSED IS BEING USED DISABLE NETWORKS

        if [ ! -z "${CGTPUSED}" ]; then
          NETWORKS_ALL=""
        fi

	# GET ADAPTER TYPE

	get_adapter_type

	#
	# Check if active adapter/interface in nafo has changed
	#

	NODEID=`${CLINFO}`

	for tab in ${NETWORKS_ALL}
	do
   		NW=`/usr/bin/echo ${tab} | cut -d'@' -f1`
   		NAFO_NODE=`/usr/bin/echo ${tab} | cut -d'@' -f2`
   		ID=`/usr/bin/echo ${tab} | cut -d'@' -f3`

		# Test the ${NETWORKS_ALL} ${ID} with the cluster node number

   		if [ "${ID}" = "${NODEID}" ]
		then

			if [ "${NETWORKTYPE}" = "nafo" ]
			then
				get_nafo_adapter

	     			NAFO_NODE_FILE=/var/tmp/`basename $0`.${NAFO_NODE}

				# The first time through we create ${NAFO_NODE_FILE} with the active interface
				# Subsequent restarts or failovers will then test to see if the interface has changed
	
     				if [ ! -f "${NAFO_NODE_FILE}" ]
				then
					debug_message "check_dhcp - Creating ${NAFO_NODE_FILE} with active interface ${ADAPTER}"

       		 			/bin/echo ${ADAPTER} > ${NAFO_NODE_FILE}
     				else
       		 			OLDADAPTER=`/bin/cat ${NAFO_NODE_FILE}`

					debug_message "check_dhcp - Check if active interface has changed"

       		 			if [ "${ADAPTER}" != "${OLDADAPTER}" ]
					then
       		    				# SCMSGS
       		    				# @explanation
       		    				# The DHCP resource's fault
       		    				# monitor has detected that
       		    				# the active interface has
       		    				# changed.
       		    				# @user_action
       		    				# No user action is needed.
       		    				# The fault monitor will
       		    				# restart the DHCP server.
       		    				scds_syslog -p daemon.notice -t $(syslog_tag) -m \
						"check_dhcp - Active interface has changed from %s to %s" \
						"${OLDADAPTER}" "${ADAPTER}"

						debug_message "check_dhcp - Updating ${NAFO_NODE_FILE} with new active interface ${ADAPTER}"

       		    				/bin/echo ${ADAPTER} > ${NAFO_NODE_FILE}

						rc_check_dhcp=100
						return
					else
						debug_message "check_dhcp - Active interface has not changed"
					fi
       		 		fi
			elif [ "${NETWORKTYPE}" = "ipmp" ]; then
				debug_message "check_dhcp - Don't check for adapter changes when ipmp is being used"
			else
				debug_message "check_dhcp - Don't check for adapter changes when cgtp is being used"
			fi
   		fi
	done

	#
	# Check Dhcp with dhcpclient if ClientID is supplied
	#

	if [ ! -z "${CLIENTID}" ]; then

		debug_message "Function: check_dhcp - Check if in.dhcpd can answer dhcp requests"

		DHCPCLIENTLOGFILE=/tmp/${RESOURCE}.tmp

		# Remove DHCPCLIENTLOGFILE

		rm -f ${DHCPCLIENTLOGFILE}

		# Run dhcpclient under timeout

		${HATIMERUN} -t ${DHCPCLIENTTIMEOUT} ${DHCPCLIENT} -q ${CLIENTID} >${DHCPCLIENTLOGFILE} 2>&1

		St=$?

		#
		# CHECK RESULTS
		#

		# Check for timeouts

		if [ $St -eq 99 ]; then
			# SCMSGS
			# @explanation
			# The dhcpclient has exceeded it's timeout allowance.
			# @user_action
			# None required. Informational message, an immediate
			# failover is being requested.
			scds_syslog -p daemon.error -t $(syslog_tag) -m \
			"check_dhcp - Dhcpclient test timed out exeeded %s seconds" \
			"${DHCPCLIENTTIMEOUT}"

			rc_check_dhcp=201
			return
		else
			debug_message "check_dhcp - Dhcpclient test didn't time out"
		fi

		# Check if Dhcpclient exited non-zero
		
		if [ $St -ne 0 ]; then
			# SCMSGS
			# @explanation
			# The dhcpclient test exited with a non-zero return
			# code.
			# @user_action
			# None required. Informational message, an immediate
			# failover is being requested.
			scds_syslog -p daemon.error -t $(syslog_tag) -m \
			"check_dhcp - Dhcpclient test exited with %s" \
			"${St}"

			rc_check_dhcp=201
			return
		else
			debug_message "check_dhcp - Dhcpclient test exited with ${St}"
		fi

		# Check if Dhcpclient retrieved an IP-address
		
		if [ -z `/bin/grep SERVER ${DHCPCLIENTLOGFILE}` ]; then
			# SCMSGS
			# @explanation
			# The dhcpclient could not retrieve any IP number.
			# @user_action
			# None required. Informational message, an immediate
			# failover is being requested.
			scds_syslog -p daemon.error -t $(syslog_tag) -m \
			"check_dhcp - Dhcpclient didn't retrieve any IP-number" 

			rc_check_dhcp=201
			return
		else
			debug_message "check_dhcp - Dhcpclient did retrieve an IP-number"
		fi
	 fi

	#
	# Check tftp
	#

	if [ ! -z "${TFTPFILE}" ]; then

		debug_message "Function: check_dhcp - Check tftpd"

		# Retrieve filename from TFTPFILE

		TFTPDFILE=`basename ${TFTPFILE}`

		RETRIEVEDFILE=/tmp/${TFTPDFILE}

		TFTPLOGFILE=/tmp/${RESOURCE}.tmp

		# REMOVE RETRIEVEDFILE TFTPLOGFILE

		rm -f ${TFTPLOGFILE} ${RETRIEVEDFILE}

		# Connect to tftp on localhost and retrieve the testfile

	        debug_message "Function: check_dhcp - Connect to ${TFTPHOST} and try to retrieve ${TFTPDFILE}"

		${HATIMERUN} -t ${TFTPTIMEOUT} ${TFTP} > ${TFTPLOGFILE} << EOF
		verbose
		connect ${TFTPHOST}
		mode binary
		get ${TFTPDFILE} ${RETRIEVEDFILE}
		quit
EOF

		#
		# CHECK RESULTS
		#

		# Check for timeouts

		if [ $? -eq 99 ]; then
			# SCMSGS
			# @explanation
			# The tftp transfer has exceeded it's timeout
			# allowance.
			# @user_action
			# None required. Informational message, an immediate
			# failover is being requested.
			scds_syslog -p daemon.error -t $(syslog_tag) -m \
			"check_dhcp - tftp transfer test timed out exeeded %s seconds" \
			"${TFTPTIMEOUT}"

			rc_check_dhcp=201
			return
		else
			debug_message "check_dhcp - tftp transfer test didn't time out"
		fi

		# Check error codes in the logfile

		Msg=`/bin/grep Error ${TFTPLOGFILE}`

		if [ ! -z "${Msg}" ]; then
			# SCMSGS
			# @explanation
			# The tftp transfer exited with a non-zero return
			# code.
			# @user_action
			# None required. Informational message, an immediate
			# failover is being requested.
			scds_syslog -p daemon.error -t $(syslog_tag) -m \
			"check_dhcp - tftp transfer test generated an error code of (%s)" \
			"${Msg}"

			rc_check_dhcp=201
			return
		else
			debug_message "check_dhcp - tftp transfer test didn't generate any error codes"
		fi

		# Check if the retrieved file exist and is non-zero bytes

		if [ ! -s "${RETRIEVEDFILE}" ]; then
			# SCMSGS
			# @explanation
			# The (tftp) retrieved file either does not exist or
			# has filesize of zero bytes
			# @user_action
			# None required. Informational message, an immediate
			# failover is being requested.
			scds_syslog -p daemon.error -t $(syslog_tag) -m \
			"check_dhcp - The retrieved file (%s) don't exist or is zero-bytes long." \
			"${RETRIEVEDFILE}"

			rc_check_dhcp=201
			return
		else
			debug_message "check_dhcp - The retrieved file (${RETRIEVEDFILE}) exist and is not non-zero bytes long"
		fi

	fi



	debug_message "Function: check_dhcp - End"
}
