#!/usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)dhcp_register.ksh	1.4	07/06/06 SMI"
#

. `dirname $0`/dhcp_config

#
# Validation
#

# Validate if both NETWORK and USE_STATIC_DHCP is being set

if [ ! -z "${NETWORK}" -a  "${USE_STATIC_DHCP}" = "TRUE" ]; then

   echo "Configure both NETWORK and USE_STATIC_DHCP is not allowed"
   exit 1
fi

if [ ! -z "${NETWORK}" ]; then
   USED_NETWORK="-N "${NETWORK}
else
   USED_NETWORK="-S"
fi

if [ "${USE_CGTP}" = "TRUE" ]; then
   USE_CGTP="-T"
else
   USE_CGTP=""
fi

if [ ! -z "${TEST_CLIENTID}" ]; then
   TEST_CLIENTID="-D ${TEST_CLIENTID}"
else
   TEST_CLIENTID=""
fi

if [ ! -z "${TFTPTESTFILE}" ]; then
   TFTPTESTFILE="-F ${TFTPTESTFILE}"
else
   TFTPTESTFILE=""
fi



scrgadm -a -j $RS -g $RG -t SUNW.gds \
-x Start_command="/opt/SUNWscdhc/bin/start_dhcp \
-R $RS -G $RG ${USED_NETWORK} ${USE_CGTP} ${TEST_CLIENTID} ${TFTPTESTFILE}" \
-x Stop_command="/opt/SUNWscdhc/bin/stop_dhcp \
-R $RS -G $RG ${USED_NETWORK} ${USE_CGTP} ${TEST_CLIENTID} ${TFTPTESTFILE}" \
-x Probe_command="/opt/SUNWscdhc/bin/probe_dhcp \
-R $RS -G $RG ${USED_NETWORK} ${USE_CGTP} ${TEST_CLIENTID} ${TFTPTESTFILE}" \
-y Port_list=$PORT/tcp -y Network_resources_used=$LH \
-x Stop_signal=9 \
-y Resource_dependencies=$HAS_RS -x Child_mon_level=3
