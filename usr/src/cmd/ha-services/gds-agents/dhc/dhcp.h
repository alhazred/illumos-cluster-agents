/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef __DHCP_H
#define	__DHCP_H

#pragma ident	"@(#)dhcp.h	1.5	07/06/06 SMI"

#ifndef ERR_SOCKET
#define	ERR_SOCKET	-1
#define	ERR_IOCTL	-2
#define	ERR_MEM		-3
#define	ERR_BIND	-4
#endif

struct dhcpmsg {
	unsigned char op;
	unsigned char htype;
	unsigned char hlen;
	unsigned char hops;
	unsigned long xid;
	unsigned short secs;
	unsigned short flags;
	unsigned long ciaddr;
	unsigned long yiaddr;
	unsigned long siaddr;
	unsigned long giaddr;
	unsigned char chaddr[16];
	unsigned char sname[64];
	unsigned char file[128];
	unsigned char options[1236]; /* 312 default. we put 1236 to be 1472 */
	} dhcpmsg;

extern int iDHCPStart(struct in_addr *padd, int raw, int reuse);
extern void vDHCPBuildDiscover(struct dhcpmsg *pdhcpm,
				unsigned char *macaddr, char *clase);

/* DHCP option and value (cf. RFC1533) */
enum
{
	padOption			=	0,
	subnetMask			=	1,
	timerOffset			=	2,
	routersOnSubnet			=	3,
	timeServer			=	4,
	nameServer			=	5,
	dns				=	6,
	logServer			=	7,
	cookieServer			=	8,
	lprServer			=	9,
	impressServer			=	10,
	resourceLocationServer		=	11,
	hostName			=	12,
	bootFileSize			=	13,
	meritDumpFile			=	14,
	domainName			=	15,
	swapServer			=	16,
	rootPath			=	17,
	extentionsPath			=	18,
	IPforwarding			=	19,
	nonLocalSourceRouting		=	20,
	policyFilter			=	21,
	maxDgramReasmSize		=	22,
	defaultIPTTL			=	23,
	pathMTUagingTimeout		=	24,
	pathMTUplateauTable		=	25,
	ifMTU				=	26,
	allSubnetsLocal			=	27,
	broadcastAddr			=	28,
	performMaskDiscovery		=	29,
	maskSupplier			=	30,
	performRouterDiscovery		=	31,
	routerSolicitationAddr		=	32,
	staticRoute			=	33,
	trailerEncapsulation		=	34,
	arpCacheTimeout			=	35,
	ethernetEncapsulation		=	36,
	tcpDefaultTTL			=	37,
	tcpKeepaliveInterval		=	38,
	tcpKeepaliveGarbage		=	39,
	nisDomainName			=	40,
	nisServers			=	41,
	ntpServers			=	42,
	vendorSpecificInfo		=	43,
	netBIOSnameServer		=	44,
	netBIOSdgramDistServer		=	45,
	netBIOSnodeType			=	46,
	netBIOSscope			=	47,
	xFontServer			=	48,
	xDisplayManager			=	49,
	dhcpRequestedIPaddr		=	50,
	dhcpIPaddrLeaseTime		=	51,
	dhcpOptionOverload		=	52,
	dhcpMessageType			=	53,
	dhcpServerIdentifier		=	54,
	dhcpParamRequest		=	55,
	dhcpMsg				=	56,
	dhcpMaxMsgSize			=	57,
	dhcpT1value			=	58,
	dhcpT2value			=	59,
	dhcpClassIdentifier		=	60,
	dhcpClientIdentifier		=	61,
	endOption			=	255
};

#endif /* __DHCP_H */
