/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#pragma ident	"@(#)dhcpc.c	1.7	07/07/31 SMI"

#include <sys/types.h>
#include <poll.h>
#include <stdlib.h>
#include <strings.h>
#include <stdio.h>
#include <ctype.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <errno.h>

#include "dhcp.h"

static int iHelp = 0;
static int iReuse = 0;
static int iVerbose = 0;
static int iSeconds = 10;
static char pcHWClass[1024] = "";


#define	QTYPE_MAC	1
/*
#define	QTYPE_IP	2
#define	QTYPE_HOSTNAME	3
#define	QTYPE_IFNUMBER	4

static int iQifnumber;
static unsigned char pucQIP[4];
static char pcQhostname[1024];
*/

static int iQType = -1;
static unsigned char pucQMAC[6];

static char *ppcHWClasses[4] = {
	"",
	"SUNW.UltraSPARC-IIi-cEngine",
	"SUNW.Ultra-5_10",
	"SUNW.Ultra-60"};

void
vHelp(void)
{
	(void) printf("usage: dhcpc [-q MAC] [-r] [-t secs] [-v] [-c class] "
				"[-?] [-h]\n");
	(void) printf("          -h or -? or no parameters to request this "
				"help.\n");
	(void) printf("          -q To specify the request.\n");
	(void) printf("             Format is XX:XX:XX:XX:XX:XX. Not "
				"neccesary two Xs.\n");
	(void) printf("             Not neccesary uppercase nor lowercase.\n");
	(void) printf("          -r Use SO_REUSEADDR (see man getsockopt). "
				"Default no.\n");
	(void) printf("          -t To specify seconds to sniff answers. "
				"Default 10.\n");
	(void) printf("          -v Verbose report.\n");
	(void) printf("          -c To specify the class.\n");
	(void) printf("             A number will specify the class from "
				"this table:\n");
	(void) printf("               1: SUNW.UltraSPARC-IIi-cEngine\n");
	(void) printf("               2: SUNW.Ultra-5_10\n");
	(void) printf("               3: SUNW.Ultra-60\n");
	(void) printf("             A higher number will disable the class "
				"specification.\n");
	(void) printf("             Anything else will be considered as the "
				"class string.\n");
	(void) printf("             If -c not specified, then \\0 will be "
				"send as class.\n\n");
	(void) printf("   WARNING: Please be sure to instruct the dhcp server "
				"not to check\n");
	(void) printf("            the IP address that is about to offer "
				"using ICMP.\n");
	(void) printf("            For Solaris DHCP daemon is option `-n`.\n");

	exit(1);
}

int
iStrTabIPv4(char *pcStr)
{
	int iNum[4];
	char pcAux[128];
	char *pcAux2;

	(void) strlcpy(pcAux, pcStr, 128);
	pcAux2 = strchr(pcAux, '.');
	if (!pcAux2)
		return (-1);
	*pcAux2 = 0;
	iNum[0] = atoi(pcAux);

	(void) strcpy(pcAux, pcAux2+1);
	pcAux2 = strchr(pcAux, '.');
	if (!pcAux2)
		return (-1);
	*pcAux2 = 0;
	iNum[1] = atoi(pcAux);

	(void) strcpy(pcAux, pcAux2+1);
	pcAux2 = strchr(pcAux, '.');
	if (!pcAux2)
		return (-1);
	*pcAux2 = 0;
	iNum[2] = atoi(pcAux);

	iNum[3] = atoi(pcAux2+1);

	(void) sprintf(pcStr, "%3d.%3d.%3d.%3d", iNum[0], iNum[1], iNum[2],
				iNum[3]);
	return (0);
}

static int
iCountChar(char *txt, char c)
{
	int i = 0;
	do {
		txt = strchr(txt, (int)c);
		if (txt) { i++; txt++; }
	} while (txt);
	return (i);
}

static int
iGetHexVal(char c)
{
	c = (char)toupper(c);
	switch (c) {
		case '0':	case '1':	case '2':	case '3':
		case '4':	case '5':	case '6':	case '7':
		case '8':	case '9': return c-'0';
		case 'A':	case 'B':	case 'C':	case 'D':
		case 'E':	case 'F': return c-'A'+10;
		default:;
	};
	return (-1);
}

/* xx:xx:xx:xx:xx:xx (not neccesarily two Xs per number) */
static int
iConvertMAC(char *opt, unsigned char *mac)
{
	int val;
	int i;
	for (i = 0; i < 6; i++) {
		if (opt[0] == ':') {
			mac[i] = 0;
			opt++;
			continue;
		}
		if (opt[1] == ':') {
			val = iGetHexVal(opt[0]);
			if (val == -1)
				return (1);
			mac[i] = (unsigned char) val;
			opt += 2;
			continue;
			}
		if ((opt[2] != ':') && (opt[2] != 0))
			return (1);
		val = iGetHexVal(opt[0]);
		if (val == -1)
			return (1);
		mac[i] = (unsigned char) (val * 16);
		val = iGetHexVal(opt[1]);
		if (val == -1)
			return (1);
		mac[i] += (unsigned char) val;
		opt += 3;
	}
	return (0);
}

void
vParseParams(int nargs, char *arg[])
{
	extern char *optarg;
	char c;

	while ((c = (char)getopt(nargs, arg, "?hrq:vt:c:")) != EOF) {
	switch (c) {
		case 'q':
			if (iCountChar(optarg, ':') == 5) {
				iQType = QTYPE_MAC;
				if (iConvertMAC(optarg, pucQMAC)) {
					(void) printf("dhcpc: error parsing "
						"mac address.\n");
					iQType = -1;
				}
			}
			break;
		case 'r': iReuse = 1; break;
		case 'v': iVerbose = 1; break;
		case 't':
			iSeconds = atoi(optarg);
			if (!iSeconds)
				iSeconds = 10;
			break;
		case 'c':
			if (!atoi(optarg))
				(void) strcpy(pcHWClass, optarg);
			else {
				if (atoi(optarg) < (int)sizeof (ppcHWClasses)
					/ (int)sizeof (char *))
					(void) strcpy(pcHWClass,
						ppcHWClasses[atoi(optarg)]);
			}
			break;
		case '?':
		case 'h':
		default : iHelp = 1; break;
		}
	}
}

int
send_dhcp_packet(int fd, struct dhcpmsg *payload)
{
	struct sockaddr_in dest;
	(void) memset(&dest, 0, sizeof (dest));
	dest.sin_family = AF_INET;
	dest.sin_port = htons(67);
	dest.sin_addr.s_addr = htonl(INADDR_BROADCAST);
	return sendto(fd, payload, sizeof (struct dhcpmsg), 0,
			(struct sockaddr *)&dest, sizeof (dest));
}


static char *
pcAddr2Str(unsigned long ul)
{
	static char buf[256];
	(void) memset(buf, 0, 256);
	(void) inet_ntop(AF_INET, &ul, buf, 255);
	(void) iStrTabIPv4(buf);
	return (buf);
}

static void
vPrintDHCPMessage(struct dhcpmsg *m)
{
	(void) printf("SERVER: %s\t\tIP: %s\n",
			m->sname, pcAddr2Str(m->yiaddr));
	if (!iVerbose)
		return;

	(void) printf("   Opcode:                 %s\n",
			(m->op == 1)?"BOOTREQUEST":"BOOTREPLY");
	(void) printf("   HW Address Type:        %d\n", m->htype);
	(void) printf("   HW Address Length:      %d\n", m->hlen);
	(void) printf("   Hops:                   %d\n", m->hops);
	(void) printf("   Transaction ID:         %ld\n", m->xid);
	(void) printf("   Seconds:                %d\n", m->secs);
	(void) printf("   Flags:                  0x%04x (%s)\n", m->flags,
			(m->flags & htons(0x8000))?"BROADCAST":"UNICAST");
	(void) printf("   Client IP Address:      %s\n", pcAddr2Str(m->ciaddr));
	(void) printf("   Your IP Address:        %s\n", pcAddr2Str(m->yiaddr));
	(void) printf("   Next Server IP Address: %s\n", pcAddr2Str(m->siaddr));
	(void) printf("   Relay Agent IP Address: %s\n", pcAddr2Str(m->giaddr));
	(void) printf("   Client HW Address:      "
			"%02x:%02x:%02x:%02x:%02x:%02x\n",
			m->chaddr[0], m->chaddr[1], m->chaddr[2], m->chaddr[3],
			m->chaddr[4], m->chaddr[5]);
	(void) printf("   Server Name:            %s\n", m->sname);
	(void) printf("   Boot File Name:         %s\n", m->file);
	(void) printf("   Options:\n");
}

int
main(int nargs, char *arg[])
{
	time_t t1;
	struct dhcpmsg dhcpm;
	int len;
	struct pollfd pfd;

	int n;

	vParseParams(nargs, arg);

	(void) printf("\n");

	if (iHelp) vHelp();
	if (iQType == -1) vHelp();

	n = iDHCPStart(NULL, 0, iReuse);
	if (n < 0) {
		(void) printf("dhcpc: error %d: %s\n",
			errno, strerror(errno));	/*lint !e746 */
		return (5);
	}

	vDHCPBuildDiscover(&dhcpm, pucQMAC, pcHWClass);

	len = send_dhcp_packet(n, &dhcpm);
	if (len <= 0) {
		(void) printf("write on socket failed: %s", strerror(errno));
		(void) close(n);
		return (1);
	}

	t1 = time(NULL);
	while (iSeconds > time(NULL)-t1) {

		(void) memset(&pfd, 0, sizeof (pfd));
		pfd.fd = n;
		pfd.events = POLLIN;
		len = poll(&pfd, 1, 1000);
		if (!len) continue;

		(void) recvfrom(n, (void *)&dhcpm, sizeof (dhcpm), 0, NULL,
				NULL);
		vPrintDHCPMessage(&dhcpm);
	}

	(void) close(n);

	(void) printf("\n");

	return (0);
}
