/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#pragma ident	"@(#)dhcp.c	1.6	07/06/06 SMI"

#include <sys/socket.h>
#include <sys/fcntl.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <inttypes.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>

#include "dhcp.h"

void
vDHCPBuildDiscover(struct dhcpmsg *pdhcpm, unsigned char *macaddr, char *clase)
{
	unsigned char *p = pdhcpm->options + 4;
	unsigned long ulMagicCookie = htonl(0x63825363);
	unsigned short dhcpMsgSize = htons(sizeof (struct dhcpmsg));

	(void) memset(pdhcpm, 0, sizeof (struct dhcpmsg));
	pdhcpm->op = 1;		/* BOOTREQUEST */
	pdhcpm->htype = 1;	/* ethernet */
	pdhcpm->hlen = 6;	/* ethernet */
	pdhcpm->xid = (unsigned long) time(NULL);
	pdhcpm->secs = 0;
	/* to request server response in broadcast */
	pdhcpm->flags = htons(0x8000);
	(void) memcpy(pdhcpm->chaddr, macaddr, 6);
	(void) memcpy(pdhcpm->options, &ulMagicCookie, 4);

	*p++ = dhcpMessageType;
	*p++ = 1;
	*p++ = 1; /* DHCP_DISCOVER */

	if (clase)
		if (strlen(clase)) {
			*p++ = dhcpClassIdentifier;
			*p++ = (unsigned char) strlen(clase);
			(void) memcpy(p, clase, strlen(clase));
			p += strlen(clase);
		}

	*p++ = dhcpParamRequest;
	*p++ = 4;
	*p++ = subnetMask;
	*p++ = routersOnSubnet;
	*p++ = hostName;
	*p++ = vendorSpecificInfo;

	*p++ = dhcpMaxMsgSize;
	*p++ = 2;
	(void) memcpy(p, &dhcpMsgSize, 2);
	p += 2;

	*p = endOption;
}

int
iDHCPStart(struct in_addr *padd, int raw, int reuse)
{
	int res;
	int true;
	struct sockaddr_in clientaddr;
	int udpsocket;

	int type, protocol;

	if (raw) {
		type = SOCK_RAW;
		protocol = IPPROTO_RAW;
		} else {
			type = SOCK_DGRAM;
			protocol = IPPROTO_UDP;
			}

	udpsocket = socket(AF_INET, type, protocol);
	if (udpsocket < 0)
		return (ERR_SOCKET);

	true = 1;
	if (raw)
		if (setsockopt(udpsocket, IPPROTO_IP, IP_HDRINCL, &true,
				sizeof (true))) {
			(void) close(udpsocket);
			return (ERR_IOCTL);
		}

	true = 1;
	if (reuse)
		if (setsockopt(udpsocket, SOL_SOCKET, SO_REUSEADDR, &true,
				sizeof (true))) {
			(void) close(udpsocket);
			return (ERR_IOCTL);
		}

	true = 1;
	if (setsockopt(udpsocket, SOL_SOCKET, SO_BROADCAST, &true,
				sizeof (true))) {
		(void) close(udpsocket);
		return (ERR_IOCTL);
	}

	(void) memset(&clientaddr, 0, sizeof (clientaddr));
	clientaddr.sin_family = AF_INET;
	clientaddr.sin_port = htons(68);
	if (padd == NULL)
		clientaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	else
		clientaddr.sin_addr = *padd;

	res = bind(udpsocket, (struct sockaddr *)&clientaddr,
				sizeof (clientaddr));
	if (res == -1) {
		(void) close(udpsocket);
		return (ERR_BIND);
	}

	return (udpsocket);
}
