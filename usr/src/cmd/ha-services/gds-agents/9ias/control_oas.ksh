#!/usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)control_oas.ksh	1.8	07/06/06 SMI"
#
# Usage GDS: <options> <parameter1> <parameter2>
# 
#	<options>: -R <resource> -G <resourcegroup> etc.
#	parameter1: start | stop | probe
#	parameter2: em | oidldap | oidmon | opmn

MYNAME=`basename $0`
MYDIR=`dirname $0`

typeset opt

while getopts 'R:G:O:S:H:U:P:E:D:C:' opt
do
	case "${opt}" in
		R)      RESOURCE=${OPTARG};;
		G)      RESOURCEGROUP=${OPTARG};;
		O)      OIAS_ORAHOME=${OPTARG};;
		S)      OIAS_ORASID=${OPTARG};;
		H)      OIAS_LHOST=${OPTARG};;
		U)      OIAS_USER=${OPTARG};;
		P)      OIAS_ADMIN=${OPTARG};;
		E)      OIAS_INFRA=${OPTARG};;
		D)      OIAS_FQDN=${OPTARG};;
		C)      OIAS_OPMN=${OPTARG};;
		*)      exit 1;;
	esac
done

if [ "${OPTIND}" -gt 1 ]
then
	# Called by GDS
	CALLER=GDS

	shift $((${OPTIND} -1))
else
	# Called by SMF
	exit 1
fi

METHOD=${1}
COMPONENT=${2}

. ${MYDIR}/../etc/config
. ${MYDIR}/functions

debug_message "Method: ${MYNAME} - Begin"
${SET_DEBUG}

if [ "${CALLER}" = "GDS" ]
then
	# Determine the newtask project for start and stop

	TASK_COMMAND=""

	if [ "${METHOD}" != "probe" ]
	then
	   if [ `/usr/bin/uname -r` != "5.8" ]
	   then
	      # Retrieve the resource project name
	      RESOURCE_PROJECT_NAME=`/usr/cluster/bin/scha_resource_get \
		-R ${RESOURCE} -G ${RESOURCEGROUP} -O RESOURCE_PROJECT_NAME`
		
	      if [ -z "${RESOURCE_PROJECT_NAME}" -o "${RESOURCE_PROJECT_NAME}" = "default" ]
	      then
		# Retrieve the resource group project name 
		RESOURCE_PROJECT_NAME=`/usr/cluster/bin/scha_resourcegroup_get \
		   -G ${RESOURCEGROUP} -O RG_PROJECT_NAME`
	      fi	
	   fi

	   # Validate that ${OIAS_USER} belongs to the ${RESOURCE_PROJECT_NAME}
	   if [ "${RESOURCE_PROJECT_NAME}" ]
	   then
	      PROJ_MEMBER=`/usr/bin/projects ${OIAS_USER} | /usr/bin/egrep "^${RESOURCE_PROJECT_NAME} | ${RESOURCE_PROJECT_NAME} | ${RESOURCE_PROJECT_NAME}$|^${RESOURCE_PROJECT_NAME}$"`

	      if [ -z "${PROJ_MEMBER}" ]
	      then
		# SCMSGS
		# @explanation
		# The specified user does not belong to the project defined by
		# Resource_project_name or Rg_project_name.
		# @user_action
		# Add the user to the defined project in /etc/project.
		scds_syslog -p daemon.notice -t $(syslog_tag) -m \
		   "%s - The user %s does not belongs to project %s" \
		   "${MYNAME}" "${APPSUSER}" "${RESOURCE_PROJECT_NAME}"
	      else
		TASK_COMMAND="/usr/bin/newtask -p ${RESOURCE_PROJECT_NAME}"
	      fi
	   fi
	fi
else
	exit 1
fi

[ -x /sbin/zonename ] && ZONENAME=`/sbin/zonename`
[ "${COMPONENT}" = "oidldap" ] && COMPONENT=oidldapd

set_redirection

case "${METHOD}" in
	start)
		validate
		rc=$?
		[ "${rc}" -ne 0 ] && return ${rc}

		# For opmn, if we're running v9.0.2 or v9.0.3 then
		# we need to to reset the password and reregister
		# before starting opmn

		if [ "${COMPONENT}" = "opmn" ]
		then
		   check_ias_version

		   if [ "${rc_check_ias_version}" -eq 1 ]
		   then
			/usr/bin/sleep 300 &
			/usr/cluster/bin/pmfadm -s ${RESOURCEGROUP},${RESOURCE},0.svc

			resetiAS_passwd
		
			reregister
		   fi
		fi

		execute_${COMPONENT} start

		rc=$?
		;;
	stop)
		if [ "${COMPONENT}" = "opmn" ]
		then
		   execute_${COMPONENT} stopall
		else
		   execute_${COMPONENT} stop
		fi

		rc=$?
		;;
	probe)
		check_${COMPONENT}

		rc=$?
		;;
esac

debug_message "Method: ${MYNAME} - End"
exit ${rc}
