#!/usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)9ias_register.ksh	1.6	07/06/06 SMI"
#

. `dirname $0`/9ias_config

PORT=10000

validate_registration()
{
St=$?
RS=$1
RG=$2
ORACLE_HOME=$3
ORACLE_SID=$4
OIAS_LHOST=$5
OIAS_USER=$6
OIAS_ADMIN=$7
OIAS_INFRA=$8
OIAS_FQDN=$9

if [ "${St}" -ne 0 ]; then
        echo "Registration of resource ${RS} failed, please correct the wrong parameters"
        exit 1
else
        echo "Registration of resource ${RS} succeeded"
fi

# VALIDATE RESOURCE

`dirname $0`/../bin/validate_9ias -R $RS -G $RG \
-O $ORACLE_HOME -S $ORACLE_SID -H $OIAS_LHOST \
-U $OIAS_USER -P $OIAS_ADMIN -E $OIAS_INFRA -D $OIAS_FQDN

St=$?

if [ "${St}" -ne 0 ]; then
        echo "Validation of resource ${RS} failed, please correct the wrong parameters"
        echo "Removing resource ${RS} from the cluster configuration"

        scrgadm -r -j ${RS}
        exit 1
else
        echo "Validation of resource ${RS} succeeded"
fi
}

scrgadm -a -j $RS_OIDMON -g $RG -t SUNW.gds \
-x Start_command="/opt/SUNWsc9ias/bin/start-oidmon \
-R $RS_OIDMON -G $RG -O $ORACLE_HOME -S $ORACLE_SID -H $OIAS_LHOST \
-U $OIAS_USER -P $OIAS_ADMIN -E $OIAS_INFRA -D $OIAS_FQDN " \
-x Stop_command="/opt/SUNWsc9ias/bin/stop-oidmon \
-R $RS_OIDMON -G $RG -O $ORACLE_HOME -S $ORACLE_SID -H $OIAS_LHOST \
-U $OIAS_USER -P $OIAS_ADMIN -E $OIAS_INFRA -D $OIAS_FQDN " \
-x Probe_command="/opt/SUNWsc9ias/bin/probe-oidmon \
-R $RS_OIDMON -G $RG -O $ORACLE_HOME -S $ORACLE_SID -H $OIAS_LHOST \
-U $OIAS_USER -P $OIAS_ADMIN -E $OIAS_INFRA -D $OIAS_FQDN " \
-y Port_list=$PORT/tcp -y Network_resources_used=$RS_LH \
-x Stop_signal=9 \
-y Resource_dependencies=$RS_HAS,$RS_ORACLE,$RS_ORALSR

validate_registration $RS_OIDMON $RG \
$ORACLE_HOME $ORACLE_SID $OIAS_LHOST \
$OIAS_USER $OIAS_ADMIN $OIAS_INFRA $OIAS_FQDN

scrgadm -a -j $RS_OIDLDAP -g $RG -t SUNW.gds \
-x Start_command="/opt/SUNWsc9ias/bin/start-oidldap \
-R $RS_OIDLDAP -G $RG -O $ORACLE_HOME -S $ORACLE_SID -H $OIAS_LHOST \
-U $OIAS_USER -P $OIAS_ADMIN -E $OIAS_INFRA -D $OIAS_FQDN " \
-x Stop_command="/opt/SUNWsc9ias/bin/stop-oidldap \
-R $RS_OIDLDAP -G $RG -O $ORACLE_HOME -S $ORACLE_SID -H $OIAS_LHOST \
-U $OIAS_USER -P $OIAS_ADMIN -E $OIAS_INFRA -D $OIAS_FQDN " \
-x Probe_command="/opt/SUNWsc9ias/bin/probe-oidldap \
-R $RS_OIDLDAP -G $RG -O $ORACLE_HOME -S $ORACLE_SID -H $OIAS_LHOST \
-U $OIAS_USER -P $OIAS_ADMIN -E $OIAS_INFRA -D $OIAS_FQDN " \
-y Port_list=$PORT/tcp -y Network_resources_used=$RS_LH \
-x Stop_signal=9 \
-y Resource_dependencies=$RS_OIDMON

validate_registration $RS_OIDLDAP $RG \
$ORACLE_HOME $ORACLE_SID $OIAS_LHOST \
$OIAS_USER $OIAS_ADMIN $OIAS_INFRA $OIAS_FQDN

scrgadm -a -j $RS_OPMN -g $RG -t SUNW.gds \
-x Start_command="/opt/SUNWsc9ias/bin/start-opmn \
-R $RS_OPMN -G $RG -O $ORACLE_HOME -S $ORACLE_SID -H $OIAS_LHOST \
-U $OIAS_USER -P $OIAS_ADMIN -E $OIAS_INFRA -D $OIAS_FQDN -C $OIAS_OPMN " \
-x Stop_command="/opt/SUNWsc9ias/bin/stop-opmn \
-R $RS_OPMN -G $RG -O $ORACLE_HOME -S $ORACLE_SID -H $OIAS_LHOST \
-U $OIAS_USER -P $OIAS_ADMIN -E $OIAS_INFRA -D $OIAS_FQDN -C $OIAS_OPMN " \
-x Probe_command="/opt/SUNWsc9ias/bin/probe-opmn \
-R $RS_OPMN -G $RG -O $ORACLE_HOME -S $ORACLE_SID -H $OIAS_LHOST \
-U $OIAS_USER -P $OIAS_ADMIN -E $OIAS_INFRA -D $OIAS_FQDN -C $OIAS_OPMN " \
-y Port_list=$PORT/tcp -y Network_resources_used=$RS_LH \
-x Stop_signal=9 -x Probe_timeout=90 -y Retry_interval=600 \
-y Resource_dependencies=$RS_OIDLDAP

validate_registration $RS_OPMN $RG \
$ORACLE_HOME $ORACLE_SID $OIAS_LHOST \
$OIAS_USER $OIAS_ADMIN $OIAS_INFRA $OIAS_FQDN $OIAS_OPMN

if [ ! -z "$RS_EM" ]; then

scrgadm -a -j $RS_EM -g $RG -t SUNW.gds \
-x Start_command="/opt/SUNWsc9ias/bin/start-em \
-R $RS_EM -G $RG -O $ORACLE_HOME -S $ORACLE_SID -H $OIAS_LHOST \
-U $OIAS_USER -P $OIAS_ADMIN -E $OIAS_INFRA -D $OIAS_FQDN " \
-x Stop_command="/opt/SUNWsc9ias/bin/stop-em \
-R $RS_EM -G $RG -O $ORACLE_HOME -S $ORACLE_SID -H $OIAS_LHOST \
-U $OIAS_USER -P $OIAS_ADMIN -E $OIAS_INFRA -D $OIAS_FQDN " \
-x Probe_command="/opt/SUNWsc9ias/bin/probe-em \
-R $RS_EM -G $RG -O $ORACLE_HOME -S $ORACLE_SID -H $OIAS_LHOST \
-U $OIAS_USER -P $OIAS_ADMIN -E $OIAS_INFRA -D $OIAS_FQDN " \
-y Port_list=$PORT/tcp -y Network_resources_used=$RS_LH \
-y Failover_mode=NONE -x Failover_enabled=false \
-x Stop_signal=9 \
-y Resource_dependencies=$RS_OPMN

validate_registration $RS_EM $RG \
$ORACLE_HOME $ORACLE_SID $OIAS_LHOST \
$OIAS_USER $OIAS_ADMIN $OIAS_INFRA $OIAS_FQDN

fi

