#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)Makefile	1.8	07/06/06 SMI"
#
# usr/src/cmd/ha-services/gds-agents/9ias/Makefile
#

# list common shell scripts for $(PKGNAME)/bin
KSH_BIN_SRCS =	control_oas.ksh \
		functions.ksh \
		probe-em.ksh \
		probe-oidldap.ksh \
		probe-oidmon.ksh \
		probe-opmn.ksh \
		start-em.ksh \
		start-oidldap.ksh \
		start-oidmon.ksh \
		start-opmn.ksh \
		stop-em.ksh \
		stop-oidldap.ksh \
		stop-oidmon.ksh \
		stop-opmn.ksh \
		validate_9ias.ksh

# list config file for $(PKGNAME)/etc
CONF = config

# list common util shell scripts for $(PKGNAME)/util
KSH_UTIL_SRCS =	10gas_register.ksh \
		9ias_register.ksh

# list common util config files for $(PKGNAME)/util
UTIL_CONF =	10gas_config \
		9ias_config

PROG = $(KSH_BIN_SRCS:%.ksh=%)

UTILPROG = $(KSH_UTIL_SRCS:%.ksh=%) $(UTIL_CONF)

include $(SRC)/cmd/Makefile.cmd

# Packaging
PKGNAME = SUNWsc9ias
RTRFILE = SUNW.9ias

# Disable I18N stuff, the po filename needs to be unique to support parallel
# builds of the agents below gds-agents
POFILE= $(PKGNAME).po
PIFILES=

# Disable lint
LINTFILES=

.KEEP_STATE:

all: $(PROG) $(UTILPROG)

.PARALLEL: $(PROG) $(UTILPROG)

install: all $(ROOTOPTBINPROG) $(ROOTOPTETCRTR) $(ROOTOPTETCCONF) $(ROOTOPTUTILPROG)

clean:
	$(RM) $(PROG) $(KSH_UTIL_SRCS:%.ksh=%)

include $(SRC)/cmd/Makefile.targ
