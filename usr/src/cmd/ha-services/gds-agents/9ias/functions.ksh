#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)functions.ksh	1.7	07/06/06 SMI"
#

PKG=SUNWsc9ias
MYNAME=`basename $0`
MYDIR=`dirname $0`
LOGFILE=/var/tmp/${RESOURCE}_logfile
LHOSTNAME="LHOSTNAME=${OIAS_LHOST}"
SCLOGGER=/usr/cluster/lib/sc/scds_syslog

syslog_tag()
{
	${SET_DEBUG}
	print "SC[${PKG:-??}.${COMPONENT:-??}.${METHOD:-??}]:${RESOURCEGROUP:-??}:${RESOURCE:-??}"
}

scds_syslog()
{
	${SCLOGGER} "$@" &
}

debug_message()
{
	if [ "${DEBUG}" = "${RESOURCE}" -o "${DEBUG}" = "ALL" ]
	then
	   SET_DEBUG="set -x"
	   DEBUG_TEXT=${1}

	   # SCMSGS
	   # @explanation
	   # Print the message as is.
	   # @user_action
	   # Whenever hadbm fails to even start off, it prints messages first
	   # line starting with "Error:". The messages should be obvious
	   # enough to take corrective action. NOTE: Though the error messages
	   # printed explicitly call out JAVA_HOME, make sure that the
	   # corrective action applies to java in /usr/bin directory.
	   # Unfortunately, our agent is JAVA_HOME ignorant.
	   scds_syslog -p daemon.debug -t $(syslog_tag) -m \
	      "%s" "${DEBUG_TEXT}"
	else
	   SET_DEBUG=
	fi
}

log_message()
{
	debug_message "Function: log_message - Begin"
	${SET_DEBUG}

	if [ -s "${LOGFILE}" ]
	then
	   PRIORITY=${1}
	   HEADER=${2}

	   strings ${LOGFILE} > ${LOGFILE}.copy

	   while read MSG_TXT
	   do
		# SCMSGS
		# @explanation
		# The first %s refers to the calling program, whereas the
		# second %s represents the output produced by that program.
		# Typically, these messages are produced by programs such as
		# strmqm, endmqm rumqtrm etc.
		# @user_action
		# No user action is required if the command was successful.
		# Otherwise, examine the other syslog messages occurring at
		# the same time on the same node to see if the cause of the
		# problem can be identified.
		scds_syslog -p daemon.${PRIORITY} -t $(syslog_tag) -m \
		   "%s - %s" \
		   "${HEADER}" "${MSG_TXT}"
	   done < ${LOGFILE}.copy

	   /usr/bin/cat /dev/null > ${LOGFILE}
	fi

	debug_message "Function: log_message - End"
}

set_redirection()
{
	debug_message "Function: set_redirection - Begin"
	${SET_DEBUG}

	if /usr/bin/getent passwd ${OIAS_USER} | /usr/bin/awk -F: '{print $7}' | /usr/xpg4/bin/grep -q csh
	then
	   OUTPUT=">& ${LOGFILE}"
	else
	   OUTPUT="> ${LOGFILE} 2>&1"
	fi

	debug_message "Function: set_redirection - End"
}

validate()
{
	debug_message "Function: validate - Begin"
	${SET_DEBUG}

	rc=0
	
	if [ ! -d "${OIAS_ORAHOME}" ]
	then
	   # SCMSGS
	   # @explanation
	   # The Oracle E-Business Suite ORACLE_HOME directory does not exist.
	   # @user_action
	   # Check that the correct pathname was entered for the Oracle Home
	   # directory when registering the resource and that the directory
	   # exists.
	   scds_syslog -p daemon.err -t $(syslog_tag) -m \
		"Validate - ORACLE_HOME directory %s does not exist" \
		"${OIAS_ORAHOME}"
	   rc=1
	else
	   debug_message "Validate - ORACLE_HOME dir ${OIAS_ORAHOME} exists"
	fi

	if /usr/bin/cat /etc/passwd | /usr/bin/awk -F: '{print $1}' | /usr/xpg4/bin/grep -q "^${OIAS_USER}$"
	then
	   debug_message "Validate - Infrastructure user ${OIAS_USER} exists"
	else
	   # SCMSGS
	   # @explanation
	   # The value of the Oracle Application Server Infratrucure user id
	   # within the xxx_config file is wrong, where xxx_config is either
	   # 9ias_config for Oracle 9iAS or 10gas_config for Oracle 10gAS.
	   # @user_action
	   # Specify the correct OIAS_USER in the appropriate xxx_config file
	   # and reregister xxx_register, where xxx_register is 9ias_register
	   # for Oracle 9iAS or 10gas_register for Oracle 10gAS.
	   scds_syslog -p daemon.err -t $(syslog_tag) -m \
		"Validate - Infrastructure user <%s> does not exist" \
		"${OIAS_USER}"
	   rc=1
	fi

	if [ ! -d "${OIAS_INFRA}" ]
	then
	   # SCMSGS
	   # @explanation
	   # The value of the Oracle Application Server Infrastructure
	   # directory within the xxx_config file is wrong, where xxx_config
	   # is either 9ias_config for Oracle 9iAS or 10gas_config for Oracle
	   # 10gAS.
	   # @user_action
	   # Specify the correct OIAS_INFRA in the appropriate xxx_config
	   # file.
	   scds_syslog -p daemon.err -t $(syslog_tag) -m \
		"Validate - Infrastructure directory %s does not exist" \
		"${OIAS_INFRA}"
	   rc=1
	else
	   debug_message "Validate - Infrastructure directory ${OIAS_INFRA} exists"
	fi

	debug_message "Function: validate - End"
	return ${rc}
}

check_ias_version()
{
	debug_message "Function: check_ias_version - Begin"
	${SET_DEBUG}

	VERSION=`/usr/bin/grep Version= ${OIAS_ORAHOME}/config/ias.properties | /usr/bin/awk -F= '{print $2}'`

	case "${VERSION}" in
		9.0.4*|10.*)	rc_check_ias_version=0
				;;
		*)		rc_check_ias_version=1
				;;
	esac

	debug_message "Function: check_ias_version - End"
}

resetiAS_passwd()
{
	debug_message "Function: resetiAS_passwd - Begin"
	${SET_DEBUG}

	execute_command resetiASpasswd.sh "cn=orcladmin ${OIAS_ADMIN} ${OIAS_ORAHOME}"

	if [ "${rc_execute_command}" -eq 0 ]
	then
		log_message info "resetiAS_passwd"
	else
		log_message error "resetiAS_passwd"
	fi

	debug_message "Function: resetiAS_passwd - End"
}

reregister()
{
        debug_message "Function: reregister - Begin"
        ${SET_DEBUG}

	PORT=`/usr/bin/grep "Oracle Internet Directory(non-SSL)" ${OIAS_ORAHOME}/install/portlist.ini | /usr/bin/awk -F= '{print $2}'`
	REREGISTER="/tmp/${RESOURCE}_${RESOURCEGROUP}_reregister.ksh"

	execute_command "${OIAS_ORAHOME}/bin/ldapsearch -h ${OIAS_LHOST} -p ${PORT} -D 'cn=orcladmin' -w '${OIAS_ADMIN}' -b 'cn=IAS Infrastructure Databases,cn=IAS,cn=Products,cn=OracleContext' -s sub 'orclResourceName=orasso'" "orclpasswordattribute"

	OSSOPWD=`/usr/bin/cat ${LOGFILE} | /usr/bin/grep orclpasswordattribute | /usr/bin/awk -F= '{print $2}'`

	REREGISTER="/tmp/${RESOURCE}_${RESOURCEGROUP}_reregister.ksh"

	/usr/bin/cat > ${REREGISTER} <<-EOF
	#!/bin/ksh

	${OIAS_ORAHOME}/jdk/bin/java \
	-jar ${OIAS_ORAHOME}/sso/lib/ossoreg.jar \
	-site_name ${OIAS_ORASID}.${OIAS_FQDN} \
	-success_url http://${OIAS_FQDN}:7777/osso_login_success \
	-cancel_url http://${OIAS_FQDN}:7777 \
	-logout_url http://${OIAS_FQDN}:7777/osso_logout_success \
	-home_url http://${OIAS_FQDN}:7777 \
	-config_mod_osso TRUE \
	-oracle_home_path ${OIAS_ORAHOME} \
	-u root \
	-apache_server_root ${OIAS_ORAHOME}/Apache/Apache \
	-config_file ${OIAS_ORAHOME}/Apache/Apache/conf/osso/osso.conf \
	-sso_server_version v1.2 \
	-schema orasso \
	-pass ${OSSOPWD}
	EOF

        /bin/chmod 755 ${REREGISTER}
        /bin/chown ${OIAS_USER} ${REREGISTER}

	execute_command ${REREGISTER} ""

	if [ "${rc_execute_command}" -eq 0 ]
	then
		log_message info "reregister"
	else
		log_message error "reregister"
	fi
		
	debug_message "Function: reregister - End"
}

execute_command()
{
	debug_message "Function: execute_command - Begin"
	${SET_DEBUG}

	COMMAND=${1}
	ARG=${2}

	rc_execute_command=0

	check_ias_version

	if [ "${rc_check_ias_version}" -eq 0 ]
	then
		# Logical Host interposing not required for 9.0.4
		LDP32=""
		LDP64=""
	else
		LDP32="LD_PRELOAD_32=/usr/lib/secure/libschost.so.1"
		LDP64="LD_PRELOAD_64=/usr/lib/secure/64/libschost.so.1"
	fi

	LDLP="LD_LIBRARY_PATH=${OIAS_ORAHOME}/lib"
	OHOME="ORACLE_HOME=${OIAS_ORAHOME}"
	OSID="ORACLE_SID=${OIAS_ORASID}"
	RUN_PATH="PATH=${OIAS_ORAHOME}/bin:${PATH}"
	DISPLAY="DISPLAY=${OIAS_LHOST}:0"
	
	su ${OIAS_USER} -c "${TASK_COMMAND} env ${LHOSTNAME} env ${LDP32} env ${LDP64} env ${LDLP} env ${OHOME} env ${OSID} env ${RUN_PATH} env ${DISPLAY} ${COMMAND} ${ARG} ${OUTPUT}"  > /dev/null 2>&1

	rc_execute_command=$?

	debug_message "Function: execute_command - End"
}

execute_oidmon()
{
	debug_message "Function: execute_oidmon - Begin"
	${SET_DEBUG}
	
	ARG=${1}

        if [ "${ARG}" = "stop" ]; then
	   # Allow oidmon some time to finish processing when stopping
	   sleep 10
	fi
		
	execute_command "${OIAS_ORAHOME}/bin/oidmon sleep=5" ${ARG}

	debug_message "Function: execute_oidmon - End"
}

build_sql()
{
	debug_message "Function: build_sql - Begin"
	${SET_DEBUG}

	SQLSELECT="/tmp/${RESOURCE}_${RESOURCEGROUP}_SQLSelect.ksh"
	SQLTRUNCATE="/tmp/${RESOURCE}_${RESOURCEGROUP}_SQLTruncate.ksh"

	/usr/bin/cat > ${SQLSELECT} <<-EOF
	#!/bin/ksh

	${OIAS_ORAHOME}/bin/sqlplus -s ods/ods@${OIAS_ORASID} <<-END
	set echo off verify off termout off feedback off head off
	select pid from ods.ods_process where configset=0 and instance=1;
	END

	EOF

	/usr/bin/cat > ${SQLTRUNCATE} <<-EOF
	#!/bin/ksh

	${OIAS_ORAHOME}/bin/sqlplus -s ods/ods@${OIAS_ORASID} <<-END
	set echo off verify off termout off feedback off head off
	truncate table ods.ods_process;
	END

	EOF

	/bin/chmod 755 ${SQLSELECT}
	/bin/chmod 755 ${SQLTRUNCATE}
	/bin/chown ${OIAS_USER} ${SQLSELECT}
	/bin/chown ${OIAS_USER} ${SQLTRUNCATE}

	debug_message "Function: build_sql - End"
}

execute_oidldapd()
{
	debug_message "Function: execute_oidldapd - Begin"
	${SET_DEBUG}

	# The oidctl utility issues run-server and stop-server commands that are
	# interpreted and processed by the OID monitor process, via table
	# ods.ods_process.

	ARG=${1}
	SAVEARG=${ARG}
	PID=

        if [ "${ARG}" = "start" ]
        then
	   build_sql
	   execute_command ${SQLSELECT} ""
	   ARG=${SAVEARG}

	   # If ${LOGFILE} is empty then "no rows selected" would have been returned

	   if [ -s ${LOGFILE} ]
	   then
		PID=`/usr/bin/tail -1 ${LOGFILE} | /usr/bin/awk '{print $1}'`

		if [ -x /sbin/zonename ]
		then
		   /usr/bin/ps -o zone -p ${PID} | /usr/xpg4/bin/grep -q " ${ZONENAME}$"
		else
		   /usr/bin/ps -p ${PID} > /dev/null 2>&1
		fi

		rc=$?
		
		# Should only hit rc=1, i.e. On failover ods.ods_process is wrong
		# If we hit rc=0 then just let it run and let oidmon fix it

		case ${rc} in
			0)	continue;;
			*)	# ods.ods_process has lost it's pid or bad sql
				execute_command ${SQLTRUNCATE} ""
				ARG=${SAVEARG}
				;;
		esac
	   fi
	fi

	# On Start, if the oidldap server is already running, oidmon will report 
	# 	*** Instance Number already in use. ***
	# 	*** Please try a different Instance number. ***
	# which is fine as we'll let oidmon sort it out.

	execute_command "${OIAS_ORAHOME}/bin/oidctl server=oidldapd configset=0 instance=1" ${ARG}

	# The oidldapd server gets run under the oidmon ptree, so here we create a pid under
	# the pmftag (to satisfy pmf) and then turn off monitoring that pmftag

	if [ "${ARG}" = "start" ]
	then
	   /usr/bin/sleep 120 &
	   /usr/cluster/bin/pmfadm -s ${RESOURCEGROUP},${RESOURCE},0.svc
	fi

	debug_message "Function: execute_oidldapd - End"
}

execute_em()
{
	debug_message "Function: execute_em - Begin"
	${SET_DEBUG}

	ARG=${1}

	# If the mid tier is also installed onto infrastructure failover nodes
	# then /var/opt/oracle will contain mid tier entries. So, before the
	# mid tier is installed the infrastructure entries in /var/opt/oracle
	# must be saved, i.e cp -rp /var/opt/oracle /var/opt/oracle_infra
	# before installing a mid tier instance. /var/opt/oracle_infra is then
	# referenced when the resource is registered as ${OIAS_INFRA}
	#
	# This needs to be done on each infrastructure failover node where the 
	# mid tier will be installed.
	# 
	# oidctl runs checkActiveEmdRoot() against EMDROOT using /var/opt/oracle
	# entries and as such the infrastructure entry emtab needs to be restored
	# prior to issuing oidctl.

	check_ias_version

	# 10gAS v9.0.4 does not create or use an emtab file

	if [ "${rc_check_ias_version}" -eq 1 ]
	then
	   # Save the current emtab 

	   /usr/bin/cp /var/opt/oracle/emtab /var/opt/oracle/emtab_${RESOURCE}

	   # Copy the infrastructure emtab and allow for 
	   # ${OIAS_INFRA}/emtab to be identical to /var/opt/oracle/emtab
	   # i.e. if ${OIAS_INFRA} is set to /var/opt/oracle

	   /usr/bin/cp ${OIAS_INFRA}/emtab /var/opt/oracle/emtab 2> /dev/null
	fi

	if [ "${ARG}" = "stop" ]
	then
	   if [ "${rc_check_ias_version}" -eq 0 ]
	   then
		execute_command "${OIAS_ORAHOME}/bin/emctl stop iasconsole" "&"
	   else
		execute_command "/usr/bin/echo ${OIAS_ADMIN} | ${OIAS_ORAHOME}/bin/emctl stop" "&"
	   fi

	   sleep 10 

	   # Cleanup the emctl stop if it's still running
	   if [ -x /sbin/zonename ]
	   then
		PIDS=`/usr/bin/pgrep -fz ${ZONENAME} "emctl stop"`
	   else
		PIDS=`/usr/bin/pgrep -f "emctl stop"`
	   fi

	   if [ "${PIDS}" ]
	   then
		for i in ${PIDS}
		do
		   /usr/bin/kill -9 ${i}
		done
	   fi

	   # Cleanup EM if it's still running

	   EMDROOT_PIDS=`/usr/ucb/ps auxww | /usr/bin/grep EMDROOT | /usr/bin/grep ${OIAS_ORAHOME} | \
		/usr/bin/grep -v grep | /usr/bin/awk '{print $2}'`

	   if [ "${EMDROOT_PIDS}" ]
	   then
		if [ -x /sbin/zonename ]
		then
		   for i in ${EMDROOT_PIDS}
		   do
			if /usr/bin/ps -o zone -p ${i} | /usr/xpg4/bin/grep -q " ${ZONENAME}$"
			then
			   /usr/bin/kill -9 ${i}
			fi
		   done
		else
		   /usr/bin/kill -9 ${EMDROOT_PIDS}
	   	fi
	   fi

	else
	   if [ "${rc_check_ias_version}" -eq 0 ]
	   then
		execute_command "${OIAS_ORAHOME}/bin/emctl ${ARG}" "iasconsole"
	   else
		execute_command "${OIAS_ORAHOME}/bin/emctl" ${ARG}
	   fi
	fi

	if [ "${rc_check_ias_version}" -eq 1 ]
	then
	   # Reinstate the previously saved emtab
	   /usr/bin/cp /var/opt/oracle/emtab_${RESOURCE} /var/opt/oracle/emtab
	fi

	debug_message "Function: execute_em - End"
}

execute_opmn()
{
	debug_message "Function: execute_opmn - Begin"
	${SET_DEBUG}

	ARG=${1}

	if [ "${ARG}" = "start" ]
	then
	   OPMN_COMPONENTS=`/usr/bin/echo ${OIAS_OPMN} | /usr/bin/tr "/" " "`

	   for i in ${OPMN_COMPONENTS}
	   do
		if [  "${i}" = "ALL" ]  ||  [ "${i}" = "All" ] || [ "${i}" = "all" ]
		then
		   execute_command "${OIAS_ORAHOME}/opmn/bin/opmnctl" "startall"
		else
		   check_ias_version

		   if [ "${rc_check_ias_version}" -eq 0 ]
		   then
			# Start opmn daemon without starting opmn managed processes 
			if [ -x /sbin/zonename ]
			then
			   /usr/bin/pgrep -fz ${ZONENAME} -u ${OIAS_USER} opmn >/dev/null
			else
			   /usr/bin/pgrep -f -u ${OIAS_USER} opmn >/dev/null
			fi

			rc=$?

			if [ "${rc}" -eq 1 ]
			then
			   execute_command "${OIAS_ORAHOME}/opmn/bin/opmnctl" "start"
			fi

			# Start the ias-component OID as it is a dependent process for other ias-components

			execute_command "${OIAS_ORAHOME}/opmn/bin/opmnctl status | /usr/bin/grep OID | /usr/bin/grep Alive" ""
		
			if [ "${rc_execute_command}" -eq 1 ]
			then
			   execute_command "${OIAS_ORAHOME}/opmn/bin/opmnctl startproc ias-component=OID" ""
			fi

			# Start the ias-componets as listed

			execute_command "${OIAS_ORAHOME}/opmn/bin/opmnctl startproc ias-component=${i}" ""
		   else
			if [  "${i}" = "ohs" ]
			then
			   execute_command "${OIAS_ORAHOME}/dcm/bin/dcmctl start -ct ohs" ""
			else
			   # If we get a typo component, then it will just get ignored
			   execute_command "${OIAS_ORAHOME}/dcm/bin/dcmctl start -co ${i}" ""
			fi
		   fi
		fi
	   done
	else
	   execute_command "${OIAS_ORAHOME}/opmn/bin/opmnctl" ${ARG}
	fi

	debug_message "Function: execute_opmn - End"
}

check_oidldapd()
{
	debug_message "Function: check_oidldapd - Begin"
	${SET_DEBUG}

	rc=0

	PORT=`/usr/bin/grep "Oracle Internet Directory(non-SSL)" ${OIAS_ORAHOME}/install/portlist.ini | /usr/bin/awk -F= '{print $2}'`

	execute_command "${OIAS_ORAHOME}/bin/ldapsearch -v -p ${PORT} -b '' -s base '(objectclass=*)'" ""

	# If oidldap didn't respond then report back 50 to allow oidmon to restart the oidldapd server

	if [ "${rc_execute_command}" -ne 0 ]
	then
		rc=50
	fi

	debug_message "Function: check_oidldapd - End"
	return ${rc}
}

check_em()
{
	debug_message "Function: check_em - Begin"
	${SET_DEBUG}

	rc_check_em=0

	if [ -x /sbin/zonename ]
	then
	   /usr/bin/pgrep -z ${ZONENAME} -f "start-em .*-R ${RESOURCE} " >/dev/null 2>&1
	else
	   /usr/bin/pgrep -u root -f "start-em .*-R ${RESOURCE} " >/dev/null 2>&1
	fi 

	rc=$?

	if [ "${rc}" -eq 0 ]
	then
	   debug_message "Function: check_em - EM is starting, check delayed"
	   rc_check_em=100
	else
	   check_ias_version

	   if [ "${rc_check_ias_version}" -eq 1 ]
	   then
		# Copy the infrastructure emtab and allow for 
		# ${OIAS_INFRA}/emtab to be identical to /var/opt/oracle/emtab
		# i.e. if ${OIAS_INFRA} is set to /var/opt/oracle

		/usr/bin/cp ${OIAS_INFRA}/emtab /var/opt/oracle/emtab 2> /dev/null
	   fi

	   EMDROOT_PIDS=`/usr/ucb/ps auxww | /usr/bin/grep EMDROOT | /usr/bin/grep ${OIAS_ORAHOME} | \
		/usr/bin/grep -v grep | /usr/bin/awk '{print $2}'`

	   if [ -x /sbin/zonename ]
	   then
		for i in ${EMDROOT_PIDS}
		do
		   if /usr/bin/ps -o zone -p ${i} | /usr/xpg4/bin/grep -q " ${ZONENAME}$"
		   then
			rc_check_em=0
		   fi
		done
	   else
		if /usr/bin/ps -p ${EMDROOT_PIDS} > /dev/null 2>&1
		then
		   rc_check_em=0
		fi
	   fi

	   if [ "${rc_check_em}" -eq 0 ]
	   then
		if [ "${rc_check_ias_version}" -eq 0 ]
		then
		   execute_command emctl "status iasconsole"
				
		   if ! /usr/bin/grep "10g Application Server Control is running." ${LOGFILE} > /dev/null
		   then
			rc_check_em=100
		   else
			rc_check_em=0
		   fi
		else
		   execute_command emctl status

		   if ! /usr/bin/grep "EMD is up and running" ${LOGFILE} > /dev/null
		   then
			rc_check_em=100
		   else
			rc_check_em=0
		   fi
		fi
	   else
		rc_check_em=100
	   fi

	   if [ "${rc_check_ias_version}" -eq 1 ]
	   then
		# /var/opt/oracle/emtab_${RESOURCE} should already exist.
		# i.e. It was created when the EM resource was started (see execute_em)

		# Reinstate the previously saved emtab

		/usr/bin/cp /var/opt/oracle/emtab_${RESOURCE} /var/opt/oracle/emtab
	   fi
	fi

	debug_message "Function: check_em - End"
	return ${rc_check_em}
}

check_opmn()
{
	debug_message "Function: check_opmn - Begin"
	${SET_DEBUG}
	
	rc_check_opmn=0

	if [ -x /sbin/zonename ]
	then
	   /usr/bin/pgrep -z ${ZONENAME} -f "start-opmn .*-R ${RESOURCE} " >/dev/null 2>&1
	else
	   /usr/bin/pgrep -u root -f "start-opmn .*-R ${RESOURCE} " >/dev/null 2>&1
	fi 

	rc=$?

	if [ "${rc}" -eq 0 ]
	then
	   debug_message "Function: check_opmn - OPMN is starting, check delayed"
	   rc_check_opmn=100
	else
	   if [ -x /sbin/zonename ]
	   then
		/usr/bin/pgrep -fz ${ZONENAME} -u ${OIAS_USER} opmn >/dev/null
	   else
		/usr/bin/pgrep -f -u ${OIAS_USER} opmn >/dev/null
	   fi

	   rc=$?

	   if [ "${rc}" -eq 1 ]
	   then 
		rc_check_opmn=100
	   else
		check_ias_version

		if [ "${rc_check_ias_version}" -eq 0 ]
		then
		   execute_command "${OIAS_ORAHOME}/opmn/bin/opmnctl" "status" 

		   if [ "${rc_execute_command}" -ne 0 ]
		   then
			rc_check_opmn=100
		   fi
		else
		   execute_command "${OIAS_ORAHOME}/dcm/bin/dcmctl" "getstate -v" 

		   if [ "${rc_execute_command}" -eq 0 ]
		   then
			# Issue a start request for each OPMN component that we expect to be Up, yet is Down
			# If by chance the component was Down and now is Partially up then issuing the start will be ignored
	
			if [  "$OIAS_OPMN" = "ALL" ]  ||  [ "$OIAS_OPMN" = "All" ] || [ "$OIAS_OPMN" = "all" ]
			then
			   OPMN_COMPONENTS="ohs OC4J_DAS OC4J_Demos home"
			else
			   OPMN_COMPONENTS=`echo $OIAS_OPMN | tr "/" " "`
			fi
	
			OPMN_COMPONENTS_UP=`/usr/bin/grep Up ${LOGFILE} | /usr/bin/grep -v Component | /usr/bin/awk '{if (NF == 6) print $4; else if (NF == 5 ) print $2}'`

			for i in ${OPMN_COMPONENTS}
			do
			   if ! /usr/bin/echo ${OPMN_COMPONENTS_UP} | /usr/bin/grep ${i} >/dev/null
			   then
				if [  "${i}" = "ohs" ]
				then
				   execute_command "${OIAS_ORAHOME}/dcm/bin/dcmctl start -ct ohs" ""
				else
				   execute_command "${OIAS_ORAHOME}/dcm/bin/dcmctl start -co ${i}" ""
				fi
			   fi
			done
		   else
			rc_check_opmn=100
		   fi
		fi
	   fi
	fi

	debug_message "Function: check_opmn - End"
	return ${rc_check_opmn}
}

check_oidmon()
{
	debug_message "Function: check_oidmon - Begin"
	${SET_DEBUG}

	rc_check_oidmon=0 

	if [ -x /sbin/zonename ]
	then
	   /usr/bin/pgrep -fz ${ZONENAME} -u ${OIAS_USER} oidmon >/dev/null
	else
	   /usr/bin/pgrep -f -u ${OIAS_USER} oidmon >/dev/null
	fi

	rc=$?

	if [ "${rc}" -eq 1 ]
	then
	   rc_check_oidmon=100
	fi
	
	debug_message "Function: check_oidmon - End"
	return ${rc_check_oidmon}
}
