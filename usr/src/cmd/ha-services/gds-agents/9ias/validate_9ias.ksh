#!/usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)validate_9ias.ksh	1.6	07/06/06 SMI"
#

typeset opt

while getopts 'R:G:O:S:H:U:P:E:D:' opt
do
       case "$opt" in
                R)      RESOURCE=$OPTARG;;
                G)      RESOURCEGROUP=$OPTARG;;
                O)      OIAS_ORAHOME=$OPTARG;;
                S)      OIAS_ORASID=$OPTARG;;
                H)      OIAS_LHOST=$OPTARG;;
                U)      OIAS_USER=$OPTARG;;
                P)      OIAS_ADMIN=$OPTARG;;
                E)      OIAS_INFRA=$OPTARG;;
                D)      OIAS_FQDN=$OPTARG;;
                *)      exit 1;;
       esac
done

. `dirname $0`/functions

validate

if [ "$rc_validate" -ne 0 ]
then
        exit 1
fi

exit 0
