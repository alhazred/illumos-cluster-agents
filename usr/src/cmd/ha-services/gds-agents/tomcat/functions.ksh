#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#ident	"@(#)functions.ksh	1.8	07/06/06 SMI"

SCLOGGER=/usr/cluster/lib/sc/scds_syslog
PKG=SUNWsctomcat
METHOD=`basename $0`
TASK_COMMAND=""
RESOURCE_PROJECT_NAME=""
TASK_COMMAND=""
GETENT=/usr/bin/getent
ZONENAME=/usr/bin/zonename
PGREP=/usr/bin/pgrep


validate_options()
{
	debug_message "Function: validate_options - Begin"
	${SET_DEBUG}

        #
        # Ensure all options are set
        #
	rc_validate_options=0

        SCRIPTNAME=`basename $0`
        for i in RESOURCE RESOURCEGROUP PARFILE
        do
                case $i in
                        RESOURCE)
                        if [ -z $RESOURCE ]; then
   				scds_syslog -p daemon.err -t $(syslog_tag) -m \
                                "validate_options: %s Option -R not set" \
				"${SCRIPTNAME}"
				rc_validate_options=1
                        fi;;

                        RESOURCEGROUP)
                        if [ -z $RESOURCEGROUP ]; then
   				scds_syslog -p daemon.err -t $(syslog_tag) -m \
                                "validate_options: %s Option -G not set" \
				"${SCRIPTNAME}"
				rc_validate_options=1
                        fi;;

                        PARFILE)
                        if [ -z $PARFILE ]; then
   				# SCMSGS
   				# @explanation
   				# The option -N of the Apache Tomcat agent
   				# command $COMMANDNAME is not set,
   				# $COMMANDNAME is either start_sctomcat,
   				# stop_sctomcat or probe_sctomcat.
   				# @user_action
   				# look at previous error messages in the
   				# syslog.
   				scds_syslog -p daemon.err -t $(syslog_tag) -m \
                                "validate_options: %s Option -N not set" \
				"${SCRIPTNAME}"
				rc_validate_options=1
                        fi;;
                esac
        done

	debug_message "Function: validate_options - End"
	return ${rc_validate_options}
}

validate()
{
	#
	# Validate Tomcat
	#
	
        debug_message "Function: validate - Begin"
	$SET_DEBUG

	rc_validate=0

	#
	# Validate that ${filename} exists
	#

	if [ ! -f "${PARFILE}" ]; then
   		# SCMSGS
   		# @explanation
   		# The parameter file of option -N of the start, stop or probe
   		# command does not exist.
   		# @user_action
   		# correct the filename and reregister the dataservice.
   		scds_syslog -p daemon.err -t $(syslog_tag) -m \
			"validate - file %s does not exist" \
			"${PARFILE}"
		rc_validate=1
	else
		debug_message "Validate - ${filename} exists"

		# check if the parameter file is syntax and semantic is correct

		ksh -n ${PARFILE} >/dev/null 2>&1
		if [ $? -ne 0 ]; then
   			scds_syslog -p daemon.err -t $(syslog_tag) -m \
	                "validate: there are syntactical errors in the parameterfile %s " \
			"${PARFILE}"
			rc_validate=1
			return $rc_validate
	        fi
			
		# Test if all the mandatory variables are included and set correctly in the parameterfile 

		PARLIST="EnvScript User Basepath Host Port TestCmd ReturnString Startwait"
		PARAMETERS=`cat ${PARFILE} |grep -v "^#"|grep -v "^ "|nawk -F= '{print $1}'`


		for i in $PARLIST
		do
			if ! `echo $PARAMETERS |grep $i > /dev/null `; then
   				scds_syslog -p daemon.err -t $(syslog_tag) -m \
			        "validate: %s is not set in the parameterfile %s but it is required" \
				"${i}" "${PARFILE}"
				rc_validate=1
			fi
		done	

		# test the semantic of the parameters

		. ${PARFILE}
		for i in `cat ${PARFILE} |grep -v "^#"|grep -v "^ "|nawk -F= '{print $1}'`
		do
			case $i in
	                        EnvScript)
	
				# Test environment script
	                      
	                        if [ -z $EnvScript ]; then
   					# SCMSGS
   					# @explanation
   					# The parameter EnvScript is not set
   					# in the parameter file
   					# @user_action
   					# set the variable EnvScript in the
   					# paramter file mentioned in option -N
   					# to a of the start, stop and probe
   					# command to valid contents.
   					scds_syslog -p daemon.err -t $(syslog_tag) -m \
	                                "validate: EnvScript not set but it is required"
					rc_validate=1
	                        fi
	                        if [ ! -f $EnvScript ]; then
   					# SCMSGS
   					# @explanation
   					# The environment script $Filename is
   					# set in the parameter file but does
   					# not exist.
   					# @user_action
   					# set the variable EnvScript in the
   					# parameter file mentioned in option
   					# -N of the start, stop and probe
   					# command to a valid contents.
   					scds_syslog -p daemon.err -t $(syslog_tag) -m \
	                                "validate: EnvScript %s does not exist but it is required" \
					"${EnvScript}"
					rc_validate=1
	                        fi;;
	
	                        User)
	
				# Test tomcat user
	                      
	                        if [ -z $User ]; then
   					scds_syslog -p daemon.err -t $(syslog_tag) -m \
	                                "validate: User is not set but it is required"
					rc_validate=1
	                        fi
				id ${User} >/dev/null 2>&1
				if [ $? -ne 0 ]; then
   					scds_syslog -p daemon.err -t $(syslog_tag) -m \
	                                "validate: User %s does not exist but it is required" \
					"${User}"
					rc_validate=1
	                        fi;;
	
	                        Basepath)
	
				# Test tomcat basepath 
	                      
	                        if [ -z $Basepath ]; then
   					scds_syslog -p daemon.err -t $(syslog_tag) -m \
	                                "validate: Basepath is not set but it is required"
					rc_validate=1
	                        fi
	                        if [ ! -d $Basepath ]; then
   					# SCMSGS
   					# @explanation
   					# The directory with the name
   					# $Directoryname does not exist
   					# @user_action
   					# set the variable Basepath in the
   					# paramter file mentioned in option -N
   					# to a of the start, stop and probe
   					# command to valid contents.
   					scds_syslog -p daemon.err -t $(syslog_tag) -m \
	                                "validate: Directory %s does not exist but it is required" \
					"${Basepath}"
					rc_validate=1
	                        fi;;
	
	                        Host)
	
				# Test tomcat testhost
	                      
	                        if [ -z $Host ]; then
   					scds_syslog -p daemon.err -t $(syslog_tag) -m \
	                                "validate: Host is not set but it is required"
					rc_validate=1
	                        fi
				${GETENT} hosts ${Host} >/dev/null 2>&1
	                        if [ $? -ne 0 ]; then
   					scds_syslog -p daemon.err -t $(syslog_tag) -m \
	                                "validate: Host %s is not found in /etc/hosts but it is required" \
					"${Host}"
					rc_validate=1
	                        fi;;

	
	                        Port)
	
				# Test tomcats Port
	                      
	                        if [ -z $Port ]; then
   					# SCMSGS
   					# @explanation
   					# The parameter Port is not set in the
   					# parameter file
   					# @user_action
   					# set the variable Port in the
   					# paramter file mentioned in option -N
   					# to a of the start, stop and probe
   					# command to valid contents.
   					scds_syslog -p daemon.err -t $(syslog_tag) -m \
	                                "validate: Port is not set but it is required"
					rc_validate=1
	                        fi;;
	
	                        TestCmd)
	
				# Test tomcats Test Command
	                      
	                        if [ -z $TestCmd ]; then
   					scds_syslog -p daemon.err -t $(syslog_tag) -m \
	                                "validate: TestCmd is not set but it is required"
					rc_validate=1
	                        fi;;
	
	                        ReturnString)

				# Test tomcats Return string
	                      
	                        if [ -z $ReturnString ]; then
   					scds_syslog -p daemon.err -t $(syslog_tag) -m \
	                                "validate: Return String is not set but it is required"
					rc_validate=1
	                        fi;;
	
	                        Startwait)
	
				# Test tomcats Startwait Option
	                      
	                        if [ -z $Startwait ]; then
   					scds_syslog -p daemon.err -t $(syslog_tag) -m \
	                                "validate: Startwait is not set but it is required"
					rc_validate=1
	                        fi;;
	
	                        *)
	
				# All other Variables are invalid
	                      
   				scds_syslog -p daemon.err -t $(syslog_tag) -m \
	                        "validate: An invalid option entered or # is not the first character"
				rc_validate=1
	                        ;;
			esac
		done
	fi

	debug_message "Function: validate - End"
	return ${rc_validate}

}

start_Tomcat()
{
	#
	# Start Tomcat
	#

        debug_message "Function: start_Tomcat - Begin"
	$SET_DEBUG
	
        # Get the project name for the resource

        srm_function $User

        if [ $? -ne 0 ]; then
                # The error message is out of by the function
                TASK_COMMAND=""
        fi

	if [ -z "${SMF_FMRI}" ]
	then
		if getent passwd ${User} | awk -F: '{print $7}' | grep csh > /dev/null
		then
	        	su - ${User} -c "source ${EnvScript}; $TASK_COMMAND ${Basepath}/bin/startup.sh >& ${LOGFILE}" > /dev/null
			rc_start_command=$?
		else
	        	su - ${User} -c ". ${EnvScript}; $TASK_COMMAND ${Basepath}/bin/startup.sh >${LOGFILE} 2>&1" > /dev/null
			rc_start_command=$?
		fi
	else 
		if getent passwd ${User} | awk -F: '{print $7}' | grep csh > /dev/null
		then
	        	source ${EnvScript}; ${Basepath}/bin/startup.sh >& ${LOGFILE}
			rc_start_command=$?
		else
	        	. ${EnvScript}; ${Basepath}/bin/startup.sh >${LOGFILE} 2>&1
			rc_start_command=$?
		fi
	fi

	sleep $Startwait

	debug_message "Function: start_Tomcat - End"
	return ${rc_start_command}
}

stop_Tomcat()
{
	#
	# Stop Tomcat
	#

        debug_message "Function: stop_Tomcat - Begin"
	$SET_DEBUG

        # Get the project name for the resource

        srm_function $User

        if [ $? -ne 0 ]; then
                # The error message is out of by the function
                TASK_COMMAND=""
        fi


	if [ -z "${SMF_FMRI}" ]
	then
		if getent passwd ${User} | awk -F: '{print $7}' | grep csh > /dev/null
		then
	        	su - ${User} -c "source ${EnvScript}; $TASK_COMMAND ${Basepath}/bin/shutdown.sh >& ${LOGFILE} ; sleep 30" > /dev/null
			rc_stop_command=$?
		else
	       		su - ${User} -c ". ${EnvScript}; $TASK_COMMAND ${Basepath}/bin/shutdown.sh >${LOGFILE} 2>&1 ; sleep 30" > /dev/null
			rc_stop_command=$?
		fi
	else
		if getent passwd ${User} | awk -F: '{print $7}' | grep csh > /dev/null
		then
	        	source ${EnvScript}; ${Basepath}/bin/shutdown.sh >& ${LOGFILE} 
			rc_stop_command=$? 
			sleep 30
		else
	       		. ${EnvScript}; ${Basepath}/bin/shutdown.sh >${LOGFILE} 2>&1 
			rc_stop_command=$?
			sleep 30
		fi
	fi

	debug_message "Function: stop_Tomcat - End"
	return ${rc_stop_command}
}

check_Tomcat()
{
	# 
	# Probe Tomcat
	#

        debug_message "Function: check_Tomcat - Begin"
	$SET_DEBUG

	rc_check_Tomcat=0

	# determine if the start command is running or not. This indicates that the probe is started during
	# wait for online algorithm of gds

	# if wait_for_online is set to 1, no diagnostic message is sent to the syslog

	wait_for_online=0
	${PGREP} -u ${User} -f "start_sctomcat .*-R ${RESOURCE} " >/dev/null

	if [ $? -eq 0 ]; then
		wait_for_online=1
	fi 

	# test if monitor host is reachable

	/usr/sbin/ping ${Host} 2 >/dev/null
	rc_ping_command=$?

	# test if Tomcat is operational

	if [ $rc_ping_command -eq 0 ]; then
		mconnect -p ${Port} ${Host} 2>&1 << EOF|grep "${ReturnString}" >/dev/null  
${TestCmd}
EOF
		if [ $? -ne 0 ]; then

			if [ $wait_for_online -eq 0 ]; then

	# drop a message for the first probe

       		         	# SCMSGS
       		         	# @explanation
       		         	# the first sanity check was unsuccessful, may
       		         	# be out of sessions
       		         	# @user_action
       		         	# none required, it is recommended to observe
       		         	# Tomcats number of configured sessions
       		         	scds_syslog -p daemon.notice -t $(syslog_tag) -m \
               		 	"first probe was unsuccessful, try again in 5 seconds" 
			fi

			sleep 5

	# try again

			mconnect -p ${Port} ${Host} 2>&1 << EOF2|grep "${ReturnString}" >/dev/null  
${TestCmd}
EOF2
			if [ $? -ne 0 ]; then
				if [ $wait_for_online -eq 0 ]; then

	# drop a message that Tomcat is unavailable

                			# SCMSGS
                			# @explanation
                			# the last sanity check was
                			# unsuccessful, may be out of sessions
                			# @user_action
                			# none required, it is recommended to
                			# observe Tomcats number of configured
                			# sessions
                			scds_syslog -p daemon.notice -t $(syslog_tag) -m \
                			"last probe failed, Tomcat considered as unavailable" 
				fi
				rc_check_Tomcat=1
			fi
		fi
	else
		rc_check_Tomcat=1
	fi


	debug_message "Function: check_Tomcat - End"
	return ${rc_check_Tomcat}
}

get_fmri_parameters ()
{

# extract the smf properties you need to call your agent commands

        debug_message "Function: get_fmri_parameters - Begin "
        ${SET_DEBUG}

# Resource name

	RESOURCE=`/usr/bin/svcprop -p parameters/Resource ${SMF_FMRI}`

# Resource Group

	RESOURCEGROUP=`/usr/bin/svcprop -p parameters/Resource_group ${SMF_FMRI}`

# Start_timeout

	START_TIMEOUT=`/usr/bin/svcprop -p start/timeout_seconds ${SMF_FMRI}`

# Start Project

	Project=:default
	if /usr/bin/svcprop ${SMF_FMRI}|grep start/project >/dev/null
	then
		Project=`/usr/bin/svcprop -p start/project ${SMF_FMRI}`
		if [ "${Project}" != ":default" ]
		then
			PROJ_OPT=" -P ${Project}"
			# set ZONE_PROJECT, because it is needed for the srm_function
			ZONE_PROJECT=${Project}
		fi
	fi

# PARAMETER File

PARFILE=`/usr/bin/svcprop -p parameters/Parameter_file ${SMF_FMRI}`

        debug_message "Function: get_fmri_parameters - End "
}
