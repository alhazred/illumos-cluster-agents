#!/usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#ident	"@(#)probe_sctomcat.ksh	1.6	07/06/06 SMI"


typeset opt

while getopts 'R:G:N:' opt
do
        case "$opt" in
                R)      RESOURCE=$OPTARG;;
                G)      RESOURCEGROUP=$OPTARG;;
                N)      PARFILE=$OPTARG;;
                *)      exit 1;;
        esac
done

. `dirname $0`/../etc/config
. `dirname $0`/functions
. `dirname $0`/../lib/functions_static

# Set variables
LOGFILE=/var/tmp/${RESOURCE}_logfile

if ! validate_options
then
	exit 100	
fi

debug_message "Method: `basename $0` - Begin"
$SET_DEBUG


if ! validate
then
        debug_message "Method: `basename $0` - End (Exit 100)"
        exit 100
fi

# Source the Tomcat Specific Parameter file
. ${PARFILE}

if ! check_Tomcat
then
	debug_message "Method: `basename $0` - End (Exit 100)"
	exit 100
fi

debug_message "Method: `basename $0` - End (Exit 0)"
exit 0
