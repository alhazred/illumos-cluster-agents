#!/usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#ident	"@(#)probe_smf_sctomcat.ksh	1.6	07/06/06 SMI"

# This method is called by the optional probe script of the smf method
#
# Method for the xxx agents smf manifest it is called from the control script
# only if it is ana smf probe and the project in the smf serfice is not :default
# This assures, that most of the probe is runniing under the correct project.
#
# it is started with 1 parameter
#
# The options are used it smf_xxx is started for the zsh component.
# in example probe_smf_xxx ${SMF_FMRI}
#
# $1 ist the SMF_FMRI identifier which specifies the smf service
#

MYNAME=`basename ${0}`
MYDIR=`dirname ${0}`

if [ -f /lib/svc/share/smf_include.sh ]
then
	. /lib/svc/share/smf_include.sh
fi

. ${MYDIR}/../etc/config
. ${MYDIR}/../lib/functions_static
. ${MYDIR}/functions

debug_message "Method: ${MYNAME} ${1} - Begin"
${SET_DEBUG}

	
# Setting SMF_FMRI 
        
SMF_FMRI=${1}	

# getting the necessary parameters and setting the variables filled
# in from options, if the control script is called in global zone

get_fmri_parameters

# Sourcing the parameter file

. ${PARFILE}

# probe application Tomcat in the right project
check_Tomcat
rc_val=${?}

debug_message "Method: ${MYNAME} ${1} - End"
exit ${rc_val}
