#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#ident	"@(#)sctomcat_register.ksh	1.8	09/03/05 SMI"

#  This script takes 2 options, both are optional.
#  -f filename states a config file different from sctomcat_config.
#     This file will be sourced instead of sctomcat_config if -f filename is specified
#
#  -z smf
#     This option  is used for local zone instances only. smf stands for a 
#     Sun Cluster Agent for Containder SMF Component

MYNAME=`basename ${0}`
MYDIR=`dirname ${0}`

# Set generic variables:

BINDIR=/opt/SUNWsctomcat/bin
UTILDIR=/opt/SUNWsctomcat/util
SMFUTILDIR=/opt/SUNWsczone/sczsmf/util
ECHO=/usr/bin/echo

. ${MYDIR}/../etc/config
. ${MYDIR}/../lib/functions_static
. ${MYDIR}/../bin/functions

global_zone()
{

# function to register a resource in the global zone or on solaris 8 or 9

	start_command="${BINDIR}/start_sctomcat -R $RS -G $RG -N $PFILE "
	stop_command="${BINDIR}/stop_sctomcat -R $RS -G $RG -N $PFILE "
	probe_command="${BINDIR}/probe_sctomcat -R $RS -G $RG -N $PFILE "
	validate_command="${BINDIR}/validate_sctomcat -R $RS -G $RG -N $PFILE "

	if [ ${NETWORK} == "true" ]; then
	
	   # Prepare a dependency string to reflect, that a scalable resource
	   # might not have a storage resource to depend on.

	   if [ -z "${HAS_RS}" ]
	   then
		DEP="${LH}"
	   else
		DEP="${HAS_RS},${LH}"
	   fi

	   /usr/cluster/bin/clresource create -d -g ${RG} -t SUNW.gds \
	   -p Start_command="${start_command}" \
	   -p Stop_command="${stop_command}" \
	   -p Probe_command="${probe_command}" \
	   -p Validate_command="${validate_command}" \
	   -p Port_list=${PORT}/tcp \
	   -p Stop_signal=9 \
	   -p Scalable=${SCALABLE} \
	   -p Resource_dependencies=${DEP} \
	   ${RS}
	   St=$?
	else
	   /usr/cluster/bin/clresource create -d -g ${RG} -t SUNW.gds \
	   -p Start_command="${start_command}" \
	   -p Stop_command="${stop_command}" \
	   -p Probe_command="${probe_command}" \
	   -p Validate_command="${validate_command}" \
	   -p Network_aware=${NETWORK} \
	   -p Stop_signal=9 \
	   -p Resource_dependencies=${HAS_RS} \
	   ${RS}
	   St=$?
	fi

	if [ "${St}" -ne 0 ]; then
	        ${ECHO} "Registration of resource ${RS} failed, please correct the wrong parameters."
	
	        return 1
	else
	        ${ECHO} "Registration of resource ${RS} succeeded."
	fi
	
	return 0
}

local_zone_smf()
{

	SERVICE_TAG=svc:/application/sczone-agents:${RS}

# function to register a smf resource

	if [ ! -f ${SMFUTILDIR}/sczsmf_config ]
	then
		echo  ${SMFUTILDIR}/sczsmf_config does not exist. Make sure, that \
                      Sun Cluster HA for Solaris container is intalled in the global zone
		return 1
        fi

# prepare the config file

	cp ${SMFUTILDIR}/sczsmf_config ${SMFUTILDIR}/sczsmf_config.work
	cat << EOF > ${SMFUTILDIR}/sczsmf_config 
#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# This file will be sourced in by sczsmf_register and the parameters
# listed below will be used.
#
# These parameters can be customized in (key=value) form
#
#               RS - Name of the resource
#               RG - Name of the resource group containing RS
#         SCZBT_RS - Name of the SC Zone boot resource
#             ZONE - Name of the Zone
#
#       For SERVICE, RECURSIVE and STATE, refer to the svcadm(1M)
#                 man page
#
#          SERVICE - {FMRI | pattern}
#               FMRI - Fault management resource identifier
#               pattern - Pattern matching a service
#
#        RECURSIVE - {false | true}     Default: true
#               False - Just enable the service and no dependents
#               True - Enable the service and recursively enable
#                 its dependents
#
#               RECURSIVE=true equates to svcadm enable "-r"
#
#            STATE - {false | true}     Default: true
#               False - Do not wait until service state is reached
#               True - Wait until service state is reached
#
#               STATE=true equates to svcadm enable/disable "-s"
#
#    SERVICE_PROBE - Script to check the SMF service
#
#               The optional parameter, SERVICE_PROBE, provides the
#                 ability to check that the SMF service is working.
#                 This must be a script within the zone and must
#                 adhere to these return codes,
#
#                 0 - The SMF service is working
#               100 - The SMF service should be restarted
#               201 - The SMF service should initiate a failover of
#                       the Resource Group
#
#               Note: That return code 201, requires that this resource
#                 has an appropriate extension property value for
#                 FAILOVER_MODE and FAILOVER_ENABLED=TRUE
#
#               For FAILOVER_MODE refer to the r_properties(5) man page.
#

RS=${RS}
RG=${RG}
SCZBT_RS=${ZONE_BT}
ZONE=${ZONE}
SERVICE=${SERVICE_TAG}
RECURSIVE=false
STATE=true
SERVICE_PROBE="${BINDIR}/control_sctomcat probe ${SERVICE_TAG}"
  
EOF

	# determine if a working copy of the config file was created if yes use the -f option

	REGOPT=""
	if [ -f ${UTILDIR}/sctomcat_config.work ]
	then
		/usr/bin/cat ${UTILDIR}/sctomcat_config.work | /usr/sbin/zlogin ${ZONE} /usr/bin/cat - \>/tmp/sctomcat_config.work
		REGOPT="-f /tmp/sctomcat_config.work"
	else
		/usr/bin/cat ${UTILDIR}/sctomcat_config | /usr/sbin/zlogin ${ZONE} /usr/bin/cat - \>/tmp/sctomcat_config.work
		REGOPT="-f /tmp/sctomcat_config.work"
	fi

	# clean up the manifest / smf resource 

	${UTILDIR}/sctomcat_smf_remove ${REGOPT} 2>/dev/null

	# register the manifest

	if /usr/sbin/zlogin ${ZONE} ${UTILDIR}/sctomcat_smf_register ${REGOPT}
	then
		echo "Manifest ${SERVICE} was created in zone ${ZONE}"
		echo "Registering the zone smf resource"
		ksh ${SMFUTILDIR}/sczsmf_register
		ret_code=${?}
                mv ${SMFUTILDIR}/sczsmf_config.work ${SMFUTILDIR}/sczsmf_config
	else
		echo "The registration of the manifest did not complete, fix the errors and retry"
		ret_code=0
	fi
	return ${ret_code}

}


MYCONFIG=
ZONETYPE=global

typeset opt

while getopts 'f:' opt
do
        case ${opt} in
                f)      MYCONFIG=${OPTARG};;
                *)      echo  "ERROR: ${MYNAME} Option ${OPTARG} unknown - early End. Only -f is valid"
                        exit 1;;
        esac
done

# Sourcing the specified config file, either the default one,
# or the one supplied with -f

if [ -n "${MYCONFIG}" ] && [ -f "${MYCONFIG}" ]
then
	echo "sourcing ${MYCONFIG} and create a working copy under ${UTILDIR}/sctomcat_config.work"
	cp ${MYCONFIG} ${UTILDIR}/sctomcat_config.work
	. ${MYCONFIG}
else
	PKGCONF=`dirname $0`/sctomcat_config
	echo "sourcing ${PKGCONF}"
	. ${PKGCONF}
fi

# Registering the resource

if [ -z "${ZONE}" ];
then
	global_zone
else
	local_zone_smf
fi

if [ -f ${UTILDIR}/sctomcat_config.work ]
then
        echo " remove the working copy ${UTILDIR}/sctomcat_config.work"
	rm ${UTILDIR}/sctomcat_config.work
fi

exit 0
