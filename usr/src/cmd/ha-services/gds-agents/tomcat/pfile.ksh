#!/usr/bin/ksh 
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#ident	"@(#)pfile.ksh	1.5	07/06/06 SMI"

# Set the Apache Tomcat specific environment variables which the start, stop 
# and check fuctions will use 
#
# EnvScript 	Script to set runtime environment for tomcat
# User 		Apache Tomcat User
# Basepath	Absolute path to Tomcat Home directory i.e. 4.x CATALINA_HOME
#               or TOMCAT_HOME for 3.x
# Host		Hostname to test Apache Tomcat 
# Port		Port where Apache Tomcat is configured
# TestCmd	Apache Tomcat test command
# Startwait	Sleeping $Startwait seconds aftercompletion of the 
#               start command
EnvScript=
User=
Basepath=
Host=
Port=8080
TestCmd="get /index.jsp"
ReturnString="CATALINA"
Startwait=20
