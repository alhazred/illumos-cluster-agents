#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)functions.ksh	1.7	07/06/06 SMI"
#

# Generic SunCluster Section

PKG=SUNWscsge
METHOD=`basename $0`
LOGFILE=/var/tmp/${RESOURCE}_logfile
TASK_COMMAND=""

ZONENAME=/usr/bin/zonename

SCLOGGER=/usr/cluster/lib/sc/scds_syslog
SCHA_RESOURCE_GET=/usr/cluster/bin/scha_resource_get
SCHA_RESOURCEGROUP_GET=/usr/cluster/bin/scha_resourcegroup_get

syslog_tag()
{
        #
        # Data Service message format
        #

        ${SET_DEBUG}

        print "SC[${PKG:-??}.${METHOD:-??}]:${RESOURCEGROUP:-??}:${RESOURCE:-??}"
}

scds_syslog()
{

        #
        # Log a message
        #

        ${SET_DEBUG}

        ${SCLOGGER} "$@" &
}

debug_message()
{
        #
        # Output a debug message to syslog if required
        #

        if [ "${DEBUG}" = "${RESOURCE}" -o "${DEBUG}" = "ALL" ]
        then
                SET_DEBUG="set -x"

                DEBUG_TEXT=$1

                scds_syslog -p daemon.debug -t $(syslog_tag) -m \
                        "%s" "${DEBUG_TEXT}"
        else
                SET_DEBUG=
        fi
}

log_message()
{
        #
        # Output a message to syslog as required
        #

        debug_message "Function: log_message - Begin"
        ${SET_DEBUG}

        if [ -s "${LOGFILE}" ]
        then
                PRIORITY=$1
                HEADER=$2

		# 
		# Ensure the while loop only reads a closed file
		#

		strings ${LOGFILE} > ${LOGFILE}.copy
                while read MSG_TXT
                do
                        scds_syslog -p daemon.${PRIORITY} -t $(syslog_tag) -m \
                                "%s - %s" \
                                "${HEADER}" "${MSG_TXT}"
                done < ${LOGFILE}.copy

                cat /dev/null > ${LOGFILE}
		cat /dev/null > ${LOGFILE}.copy
        fi

        debug_message "Function: log_message - End"
}

standard_resource_get()
{
        debug_message "Function: standard_resource_get - Begin"
        ${SET_DEBUG}

        PROPERTY=$1

        ${SCHA_RESOURCE_GET} -R ${RESOURCE} -G ${RESOURCEGROUP} -O ${PROPERTY}

        s1=$?

        if [ "${s1}" -ne 0 ]; then
                debug_message "standard_resource_get - Retrievment of property ${PROPERTY} returned ${s1}"
        fi

        debug_message "Function: standard_resource_get - End"
        return ${s1}
}

standard_resourcegroup_get()
{
        debug_message "Function: standard_resourcegroup_get - Begin"
        ${SET_DEBUG}

        PROPERTY=$1

        ${SCHA_RESOURCEGROUP_GET} -R ${RESOURCE} -G ${RESOURCEGROUP} -O ${PROPERTY}

        s1=$?

        if [ "${s1}" -ne 0 ]; then
                debug_message "standard_resource_get - Retrievment of property ${PROPERTY} returned ${s1}"
        fi

        debug_message "Function: standard_resourcegroup_get - End"

        return ${s1}
}

srm_function()
{

        USER=$1

        debug_message "Function: srm_function - Begin"
        ${SET_DEBUG}


        #
        # If Solaris 8 just return
        #

        if [ `/usr/bin/uname -r` = "5.8" ];
        then
                return 0
        fi

        #
        # Retrieve RESOURCE_PROJECT_NAME
        #

        RESOURCE_PROJECT_NAME=`standard_resource_get RESOURCE_PROJECT_NAME`

        #
        # Retrieve RG_PROJECT_NAME if RESOURCE_PROJECT_NAME is not set
        #

        if [ -z "${RESOURCE_PROJECT_NAME}" ]; then

                RESOURCE_PROJECT_NAME=`standard_resourcegroup_get RG_PROJECT_NAME`
        fi

        #
        # Return if no projects are defined
        #

        if [ -z "${RESOURCE_PROJECT_NAME}" ]; then
                return 0
        fi

        #
        # Validate that $USER belongs to the project defined by
        # ${RESOURCE_PROJECT_NAME}
        #

        PROJ_MEMBER=`/usr/bin/projects ${USER} | /usr/bin/grep -w ${RESOURCE_PROJECT_NAME}`

        if [ -z "${PROJ_MEMBER}" ];
        then
             scds_syslog -p daemon.err -t $(syslog_tag) -m \
                        "srm_function - The user %s does not belongs to project %s" \
                        "${USER}" "${RESOURCE_PROJECT_NAME}" 
                return 1
        else
                debug_message "srm_function - User ${USER} belongs to project ${RESOURCE_PROJECT_NAME}"
        fi

        #
        # Set TASK_COMMAND
        #

        TASK_COMMAND="/usr/bin/newtask -p ${RESOURCE_PROJECT_NAME}"

        debug_message "Function: srm_function - End"

        return 0
}

zone_function()
{
	debug_message "Function: zone_function - Begin"
	${SET_DEBUG}

	#
	# Initialize PZONEOPT as empty
	PZONEOPT=""

	#
	# If Solaris does not have /usr/bin/zonename just return 0
	# else add "-z <zonename>" to PZONEOPT
	#

        if [ -x "${ZONENAME}" ];
        then
		PZONEOPT="-z `${ZONENAME}`"
	fi

	debug_message "Function: zone_function - End"
	return 0
}

validate_options()
{
        #
        # Ensure all options are set
        #

        debug_message "Function: validate_options - Begin"
	${SET_DEBUG}

        for i in RESOURCE RESOURCEGROUP SGE_ROOT SGE_CELL
        do
                case $i in
                        RESOURCE)
                        if [ -z ${RESOURCE} ]; then
				scds_syslog -p daemon.err -t $(syslog_tag) -m \
					"ERROR: Option -%s not set" "R"
                                exit 1
                        fi;;

                        RESOURCEGROUP)
                        if [ -z ${RESOURCEGROUP} ]; then
				scds_syslog -p daemon.err -t $(syslog_tag) -m \
					"ERROR: Option -%s not set" "G"
                                exit 1
                        fi;;

                        SGE_ROOT)
                        if [ -z ${SGE_ROOT} ]
			then
				scds_syslog -p daemon.err -t $(syslog_tag) -m \
					"ERROR: Option -%s not set" "S"
                                exit 1
                        fi;;

                        SGE_CELL)
                        if [ -z ${SGE_CELL} ]
			then
				scds_syslog -p daemon.err -t $(syslog_tag) -m \
					"ERROR: Option -%s not set" "C"
                                exit 1
                        fi;;

                esac
        done

	if [ ! -d ${SGE_ROOT} ]
	then
	    # SCMSGS
	    # @explanation
	    # The SGE_ROOT variable configured within
	    # /opt/SUNWscsge/util/sge_config contains a value whose location
	    # does not exist or is not a directoy.
	    # @user_action
	    # Determine where the Sun Grid Engine software is installed (the
	    # directory containing the installation script 'inst_sge').
	    # Initialize SGE_ROOT with this value in
	    # /opt/SUNWscsge/util/sge_config and try sge_remove and
	    # sge_register afterwards. This will stop, deregister and register
	    # the Sun Grid Engine data services.
	    scds_syslog -p daemon.err -t $(syslog_tag) -m \
	    "validate_options - Fatal: SGE_ROOT %s not a directory" \
	    "${SGE_ROOT}"
	    exit 1
	fi

	if [ ! -d ${SGE_ROOT}/${SGE_CELL} ]
	then
	    # SCMSGS
	    # @explanation
	    # The ${SGE_ROOT}/${SGE_CELL} combination variable produces a
	    # value whose location does not exist.
	    # @user_action
	    # The SGE_CELL value is decided when the Sun Grid Engine data
	    # service is installed. The default value is 'default' and can be
	    # configured in /opt/SUNWscsge/util/sge_config. Run sge_remove and
	    # then sge_register after verifying the value in
	    # /opt/SUNWscsge/util/sge_config is correct.
	    scds_syslog -p daemon.err -t $(syslog_tag) -m \
	    "validate_options - Fatal: SGE_CELL %s/%s not a directory" \
	    "${SGE_ROOT}" "${SGE_CELL}"
	    exit 1
	fi

        debug_message "Function: validate_options - End"
}
