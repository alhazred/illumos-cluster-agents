#!/usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)start_sge_schedd.ksh	1.6	07/06/06 SMI"
#
# Sun Grid Engine Sun Cluster Agent startup script for sge_schedd
#

PATH=/bin:/usr/bin:/sbin:/usr/sbin


typeset opt

while getopts 'R:G:S:C:' opt
do
        case "${opt}" in
                R)      RESOURCE=${OPTARG};;
                G)      RESOURCEGROUP=${OPTARG};;
                S)      SGE_ROOT=${OPTARG};;
                C)      SGE_CELL=${OPTARG};;
                *)      exit 1;;
        esac
done

. `dirname $0`/../../etc/config
. `dirname $0`/../functions
. `dirname $0`/../functions.common6
. `dirname $0`/functions.sge_schedd

validate_options

export SGE_ROOT
export SGE_CELL

debug_message "Method: `basename $0` - Begin"
${SET_DEBUG}

SGE_init

validate_schedd

if [ "${rc_validate}" -ne 0 ]
then
        debug_message "Method: `basename $0` - End (Exit 1)"
        exit 1
fi


#---------------------------------------------------------------------------
# MAIN Procedure
#

start_sge_schedd       

debug_message "Method: `basename $0` - End (Exit ${St})"

exit ${St}
