#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)functions.sge_schedd.ksh	1.6	07/06/06 SMI"
#
# Functions specific to sge_schedd
#

validate_schedd()
{
	#
	# Validate Schedd
	#
	
        debug_message "Function: validate_schedd - Begin"
	${SET_DEBUG}

	rc_validate=0

	validate_common

	if [ ! -x ${bin_dir}/sge_schedd ]
	then
   		# SCMSGS
   		# @explanation
   		# The file sge_schedd can not be found, or is not executable.
   		# @user_action
   		# Confirm the file '${SGE_ROOT}/bin/<arch>/sge_schedd' both
   		# exists at that location, and is executable.
   		scds_syslog -p daemon.err -t $(syslog_tag) -m \
			"Validate - sge_schedd file does not exist or is not executable at %s/sge_schedd" \
		"${bin_dir}"
		rc_validate=1
	else
		debug_message "Validate - sge_schedd file ${bin_dir}/sge_schedd exists"
	fi

	validate_qmaster_schedd_common

	debug_message "Function: validate_schedd - End"
}

start_sge_schedd()
{
	#
	# Start sge_schedd
	#

	debug_message "Function: start_sge_schedd - Begin"
	${SET_DEBUG}

        #
	# Check if sge_schedd is already running.
	#

        if [ -f  ${qmaster_spool_dir}/schedd/schedd.pid ]
	then
		# needed to get ${PZONEOPT} set if zones exist
		zone_function

		if [ "${PZONEOPT}" = "" ]; then
			/bin/ps -p `/bin/cat  ${qmaster_spool_dir}/schedd/schedd.pid` \
				-o pid,args | /bin/grep "sge_schedd\$" > /dev/null
		else
			/bin/ps ${PZONEOPT} -o pid,args | /bin/grep "sge_schedd\$" | \
				/bin/grep -w `/bin/cat ${qmaster_spool_dir}/schedd/schedd.pid` > /dev/null
		fi

		St=$?

		if [ ${St} -eq 0 ]; then
			# SCMSGS
			# @explanation
			# An attempt was made to start sge_schedd by bringing
			# the sge_schedd-rs resource online, with a sge_schedd
			# process already running.
			# @user_action
			# Terminate the running sge_schedd process and retry
			# bringing the resource online.
			scds_syslog -p daemon.err -t $(syslog_tag) -m \
				"sge_schedd already running; start_sge_schedd aborted."
			St=1
			exit 1
		fi
	fi

	${bin_dir}/sge_schedd >${LOGFILE} 2>&1
	St=$?

	log_message info "sge_schedd ${RESOURCE} Start Messages: "

	if [ ${St} -ne 0 ]
	then
   		# SCMSGS
   		# @explanation
   		# The process sge_schedd failed to start for reasons other
   		# than it was already running.
   		# @user_action
   		# Check /var/adm/messages for any relevant cluster messages.
   		# Respond accordingly, then retry bringing the resource
   		# online.
   		scds_syslog -p daemon.err -t $(syslog_tag) -m \
			"start_sge_schedd failed"
	else
		debug_message "start_sge_schedd - sge_schedd started"
	fi

	debug_message "Function: start_sge_schedd - End"
}

stop_sge_schedd()
{
	#
	# Stop sge_schedd
	#

        debug_message "Function: stop_sge_schedd - Begin"
	$SET_DEBUG

	# 
	# Kill sge_schedd
	#

	# Manually set because validate is not called from stop

	qmaster_spool_dir=`QmasterSpoolDir`

	if [ "${qmaster_spool_dir}" = "" ]
	then
	   scds_syslog -p daemon.err -t $(syslog_tag) -m \
	   "Stop - can't determine Qmaster Spool dir"
	   St=1
	   return
	fi

        if [ -f  ${qmaster_spool_dir}/schedd/schedd.pid ]
	then
            # Send SIGTERM to schedd
            Shutdown sge_schedd ${qmaster_spool_dir}/schedd/schedd.pid
	    # copy Shutdowns failure status to St, which is 
	    # parsed by stop_sge_xx script
	    St=${failme}

        else
	    # SCMSGS
	    # @explanation
	    # The file '${qmaster_spool_dir}/schedd.pid' was not found.
	    # '${qmaster_spool_dir}/schedd.pid' is the second argument to the
	    #  'Shutdown' function. 'Shutdown' is called within the stop
	    #  script of the sge_schedd-rs resource.
	    # @user_action
	    # Confirm the file 'schedd.pid' exists. Said file should be found
	    # in the respective spool directory of the daemon in question.
	    scds_syslog -p daemon.err -t $(syslog_tag) -m \
	    "Stop - can't determine sge_schedd pid file - %s/schedd/schedd.pid does not exist." \
		"${qmaster_spool_dir}"
	    St=1
	fi

	debug_message "Function: stop_sge_schedd - End"
}

check_sge_schedd()
{
	# 
	# Monitor SGE schedd
	#

        debug_message "Function: check_sge_schedd - Begin"
	${SET_DEBUG}

	rc_check_sge_schedd=0
	#
	# Check if the agent is starting
	#

	# needed to get ${PZONEOPT} set if zones exist
	zone_function

	if [ ! -z "`pgrep ${PZONEOPT} -f \"start_sge_schedd -R ${RESOURCE} \"`" ]
	then
		debug_message "check_sge_schedd - sge_schedd is currently starting, exiting"
		rc_check_sge_schedd=1
		return
	fi

	# Manually set because validate is not called from stop

	qmaster_spool_dir=`QmasterSpoolDir`

	if [ -f ${qmaster_spool_dir}/schedd/schedd.pid ]
	then
		pid=`cat ${qmaster_spool_dir}/schedd/schedd.pid`
	else
		# SCMSGS
		# @explanation
		# The file ${qmaster_spool_dir}/schedd.pid was not found.
		# ${qmaster_spool_dir}/schedd.pid is the second argument to
		# the 'Shutdown' function. 'Shutdown' is called within the
		# stop script of the sge_schedd resource.
		# @user_action
		# Confirm the file 'schedd.pid' exists. Said file should be
		# found in the respective spool directory of the daemon in
		# question.
		scds_syslog -p daemon.err -t $(syslog_tag) -m \
		"Probe - Fatal: Can't determine Schedd pid file - %s/schedd/schedd.pid does not exist." \
		"${qmaster_spool_dir}"
		rc_check_sge_schedd=1
		return
	fi

	if [ -x ${utilbin_dir}/checkprog ]
	then
		${utilbin_dir}/checkprog ${pid} sge_schedd > /dev/null
	else
		# SCMSGS
		# @explanation
		# The binary file ${SGE_ROOT}/utilbin/<arch>/checkprog does
		# not exist, or is not executable.
		# @user_action
		# Confirm the binary file ${SGE_ROOT}/utilbin/<arch>/checkprog
		# both exists in that location, and is executable.
		scds_syslog -p daemon.err -t $(syslog_tag) -m \
		"Probe - Fatal: %s/checkprog not found or not executable" \
		"${utilbin_dir}"
		rc_check_sge_schedd=1
		return
	fi
	St=$?

	if [ ${St} -ne 0 ]
	then
   	   rc_check_sge_schedd=1
	   debug_message "check_sge_schedd - Probe determined sge_schedd died"
	   return
	fi

	if [ -x ${bin_dir}/qconf ]
	then
		scheddstat=`${bin_dir}/qconf -sss`
	else
		# SCMSGS
		# @explanation
		# The binary file qconf can not be found, or is not
		# executable.
		# @user_action
		# Confirm the binary file ${SGE_ROOT}/bin/<arch>/qconf both
		# exists in that location, and is executable.
		scds_syslog -p daemon.err -t $(syslog_tag) -m \
		"Probe - Fatal: %s/qconf not found or not executable" \
		"${bin_dir}"
		rc_check_sge_schedd=1
		return
	fi

	if [ "${scheddstat}" != "${HOST}" ]
	then
   	   rc_check_sge_schedd=1
	   debug_message "check_sge_schedd - ${bin_dir}/qconf -sss determined sge_schedd died"
	   return
	fi

	debug_message "Function: check_sge_schedd - End"
}
