#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)sge_remove.ksh	1.6	07/06/06 SMI"
#

MYCONFIG=""
MYNAME=`basename $0`

typeset opt

while getopts 'f:' opt
do
	case "${opt}" in
		f)	MYCONFIG=${OPTARG};;
		*)	echo "ERROR: ${MYNAME} Option ${OPTARG} unknown - early End. Only the option -f <filename> is valid."
			exit 1;;
	esac
done

if [ -n "${MYCONFIG}" ] && [ -f "${MYCONFIG}" ]
then
	echo "sourcing ${MYCONFIG}"
	. ${MYCONFIG}
else
	PKGCONF=`dirname $0`/sge_config
	echo "sourcing ${PKGCONF}"
	. ${PKGCONF}
fi

/usr/cluster/bin/scswitch -n -j ${SCHEDDRS}
/usr/cluster/bin/scswitch -n -j ${QMASTERRS}

/usr/cluster/bin/scrgadm -r -j ${SCHEDDRS}
/usr/cluster/bin/scrgadm -r -j ${QMASTERRS}
