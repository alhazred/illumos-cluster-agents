#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)functions.sge_qmaster.ksh	1.5	07/06/06 SMI"
#
# Functions specific to sge_qmaster
#

validate_qmaster()
{
	#
	# Validate Qmaster
	#
	
        debug_message "Function: validate_qmaster - Begin"
	$SET_DEBUG

	rc_validate=0

	validate_common
	
	if [ ! -x ${bin_dir}/sge_qmaster ]
	then
   		# SCMSGS
   		# @explanation
   		# The file sge_qmaster cannot be found, or is not executable.
   		# @user_action
   		# Confirm the file ${SGE_ROOT}/bin/<arch>/sge_qmaster both
   		# exists at that location, and is executable.
   		scds_syslog -p daemon.err -t $(syslog_tag) -m \
			"Validate - sge_qmaster file does not exist or is not executable at %s/sge_qmaster" \
			"${bin_dir}"
		rc_validate=1
	else
		debug_message "Validate - sge_qmaster file ${bin_dir}/sge_qmaster exists"
	fi
	
	validate_qmaster_schedd_common

	debug_message "Function: validate_qmaster - End"
}

start_sge_qmaster()
{
	#
	# Start sge_qmaster
	#

	debug_message "Function: start_sge_qmaster - Begin"
	${SET_DEBUG}

	# re-write act_qmaster file, to allow exec hosts to
	# find this GridEngine Master host.
	# This technique allows a configuration where the cluster 
	# nodes can also be execution hosts themselves. The alternative
	# to use the GridEngine host_aliases file and a act_qmaster 
	# containing the logicalhost does not allow cluster nodes
	# to be execution hosts. 

	# ${HOST} is set by SGE_init called from all start/stop/probe scripts

	echo ${HOST} > ${SGE_ROOT}/${SGE_CELL}/common/act_qmaster 

	#       
	# Check if sge_qmaster already running.  
	#

        if [ -f ${qmaster_spool_dir}/qmaster.pid ]
	then
		# needed to get ${PZONEOPT} set if zones exist
		zone_function

		if [ "${PZONEOPT}" = "" ]; then
			/bin/ps -p `/bin/cat ${qmaster_spool_dir}/qmaster.pid` \
				-o pid,args | /bin/grep "sge_qmaster\$" > /dev/null
		else
			/bin/ps ${PZONEOPT} -o pid,args | /bin/grep "sge_qmaster\$" | \
				/bin/grep -w `/bin/cat ${qmaster_spool_dir}/qmaster.pid` > /dev/null
		fi

		St=$?

		if [ ${St} -eq 0 ]; then

		# SCMSGS
		# @explanation
		# An attempt was made to start sge_qmaster by bringing the
		# sge_qmaster-rs resource online, with an sge_qmaster process
		# already running.
		# @user_action
		# Terminate the running sge_qmaster process and retry bringing
		# the resource online.
		scds_syslog -p daemon.err -t $(syslog_tag) -m \
			"sge_qmaster already running; start_sge_qmaster aborted."
			St=1
			exit 1                        
		fi	
	fi

	${bin_dir}/sge_qmaster >${LOGFILE} 2>&1
	St=$?

	log_message info "sge_qmaster ${RESOURCE} Start Messages: "

	if [ ${St} -ne 0 ]
	then
   		# SCMSGS
   		# @explanation
   		# The process sge_qmaster failed to start for reasons other
   		# than it was already running.
   		# @user_action
   		# Check /var/adm/messages for any relevant cluster messages.
   		# Respond accordingly, then retry.
   		scds_syslog -p daemon.err -t $(syslog_tag) -m \
			"start_sge_qmaster failed"
	else
		debug_message "start_sge_qmaster - sge_qmaster started"
	fi

	debug_message "Function: start_sge_qmaster - End"
}

stop_sge_qmaster()
{
	#
	# Stop sge_qmaster
	#

        debug_message "Function: stop_sge_qmaster - Begin"
	${SET_DEBUG}
	St=0

	# 
	# Kill sge_qmaster
	#

	# Manually set because valiate is not called from stop

	qmaster_spool_dir=`QmasterSpoolDir`

	if [ "${qmaster_spool_dir}" = "" ]
	then
	   # SCMSGS
	   # @explanation
	   # The qmaster spool directory could not be determined, using the
	   # 'QmasterSpoolDir' function.
	   # @user_action
	   # Use the command 'qconf -sconf' to determine the value of
	   # 'qmaster_spool_dir'. Update/correct the value if necessary using
	   # 'qconf -mconf'.
	   scds_syslog -p daemon.err -t $(syslog_tag) -m \
	   "Stop - can't determine Qmaster Spool dir"
	   St=1
	   return
	fi

        if [ -f ${qmaster_spool_dir}/qmaster.pid ]
	then
            # Send SIGTERM to qmaster
            Shutdown sge_qmaster ${qmaster_spool_dir}/qmaster.pid
	    # copy Shutdowns failure status to St, which is 
	    # parsed by stop_sge_xx script
	    St=${failme}
        else
	    # SCMSGS
	    # @explanation
	    # The file '${qmaster_spool_dir}/qmaster.pid' was not found.
	    # '${qmaster_spool_dir}/qmaster.pid' is the second argument to the
	    # 'Shutdown' function. 'Shutdown' is called within the stop script
	    # of the sge_qmaster-rs resource.
	    # @user_action
	    # Confirm the file 'qmaster.pid' exists. Said file should be found
	    # in the respective spool directory of the daemon in question.
	    scds_syslog -p daemon.err -t $(syslog_tag) -m \
	    "Stop - can't determine Qmaster pid file - %s/qmaster.pid does not exist." \
	    "${qmaster_spool_dir}"
	    St=1
	fi
		
	debug_message "Function: stop_sge_qmaster - End"
}

check_sge_qmaster()
{
	# 
	# Monitor SGE qmaster
	#

        debug_message "Function: check_sge_qmaster - Begin"
	${SET_DEBUG}

	rc_check_sge_qmaster=0
	#
	# Check if the agent is starting
	#

	# needed to get ${PZONEOPT} set if zones exist
	zone_function

	if [ ! -z "`pgrep ${PZONEOPT} -f \"start_sge_qmaster -R ${RESOURCE} \"`" ]
	then
		debug_message "check_sge_qmaster - sge_qmaster is currently starting, exiting"
		rc_check_sge_qmaster=1
		return
	fi

	#
	# Check if sge_qmaster is communicating
	#

	${bin_dir}/qping -info ${HOST} ${SGE_QMASTER_PORT} qmaster 1 > /dev/null
	St=$?

	if [ ${St} -ne 0 ]
	then
   	   rc_check_sge_qmaster=1
	   debug_message "check_sge_qmaster - Probe determined sge_qmaster died"
	   return
	fi

	debug_message "Function: check_sge_qmaster - End"
	
}
