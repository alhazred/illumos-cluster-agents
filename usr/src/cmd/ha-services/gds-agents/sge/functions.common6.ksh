#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)functions.common6.ksh	1.5	07/06/06 SMI"
#
# Common Functions specific to SGE 6.0
#

# GetPathToBinaries
#    echo the name of the bin_dir on this system
#    The check is fullfilled if we can access the qstat binary
#    echo "none" if we can't determine the binary path
GetPathToBinaries()
{
        debug_message "Function: GetPathToBinaries - Begin"
	${SET_DEBUG}

	cfgname=${SGE_ROOT}/${SGE_CELL}/common/bootstrap

   	base=none

   	if [ -f ${cfgname} ]
   	then
	   base=`grep binary_path ${cfgname} | awk '{ print $2 }'`
	   if [ -f ${base}/qstat ]
	   then
	      :
	   elif [ -f ${SGE_ROOT}/util/arch ]
	   then
	     arch=`${SGE_ROOT}/util/arch`
	     if [ -f ${base}/${arch}/qstat ]
	     then
	        base=${base}/${arch}
	     fi
	   fi
	fi

	echo ${base}

	debug_message "Function: GetPathToBinaries - End"
}

# GetPathToUtilbin
#    echo the path to the binaires in utilbin
#    The check is fullfilled if we can access the "gethostname" binary
#    echo "none" if we can't determine the binary path
#
GetPathToUtilbin()
{
	debug_message "Function: GetPathToUtilbin - Begin"
	${SET_DEBUG}

	base=none

	if [ -f ${SGE_ROOT}/util/arch ]
	then
	   utilbindir=${SGE_ROOT}/utilbin

	   arch=`${SGE_ROOT}/util/arch`
	   if [ -f ${utilbindir}/${arch}/gethostname ]
	   then
              base=${utilbindir}/${arch}
	   fi
	fi

        echo ${base}

        debug_message "Function: GetPathToUtilbin - End"
}

# QmasterSpoolDir
#    Return qmaster spool directory
#
QmasterSpoolDir()
{
        debug_message "Function: QmasterSpoolDir - Begin"
	${SET_DEBUG}

	qma_spool_dir=`grep qmaster_spool_dir \
                      ${SGE_ROOT}/${SGE_CELL}/common/bootstrap | \
                      awk '{ print $2 }'`
	echo ${qma_spool_dir}

        debug_message "Function: QmasterSpoolDir - End"
}

# GetQmasterTCPPort
#    Return configured portnumber from the Port_list property to be used
#    as SGE_QMASTER_PORT.
#
GetQmasterTCPPort()
{
	debug_message "Function: GetQmasterTCPPort - Begin"
	${SET_DEBUG}

	sge_qmaster_tcp_port=`standard_resource_get Port_list | \
				/bin/awk -F/ '{print $1}'`

	echo ${sge_qmaster_tcp_port}

	debug_message "Function: GetQmasterTCPPort - End"
}

# SGE_init
#  	Setup Environment
#
SGE_init()
{
        debug_message "Function: SGE_init - Begin"
	${SET_DEBUG}

	ARCH=`${SGE_ROOT}/util/arch -bin`

	shlib_path_name=`${SGE_ROOT}/util/arch -lib`
	old_value=`eval echo '$'${shlib_path_name}`

	if [ x${old_value} = x ]
	then
	   eval ${shlib_path_name}=${SGE_ROOT}/lib/${ARCH}
	else
	   eval ${shlib_path_name}=${old_value}:${SGE_ROOT}/lib/${ARCH}
	fi
	export ${shlib_path_name}

	unset CODINE_ROOT GRD_ROOT COD_CELL GRD_CELL

	# availability of bin_dir and utilbin_dir not checked again here,
	# done at start time by validate_XX()
	
	bin_dir=`GetPathToBinaries`

	utilbin_dir=`GetPathToUtilbin`

	HOST=`${utilbin_dir}/gethostname -aname`

	SGE_QMASTER_PORT=`GetQmasterTCPPort`
	export SGE_QMASTER_PORT

        debug_message "Function: SGE_init - End"
}

# Shared validate section, used by all daemons validation

validate_common()
{
	#
	# Validate Common Functions
	#
	
        debug_message "Function: validate_common - Begin"
	${SET_DEBUG}

	# rc_validate must have been reset by calling function already

	bin_dir=`GetPathToBinaries`

	if [ "${bin_dir}" = "none" ]
	then
		# SCMSGS
		# @explanation
		# The Sun Grid Engine binary qstat was not found in
		# ${binary_path} nor ${binary_path}/<arch>. qstat is used only
		# representatively. If it is not found, the other Sun Grid
		# Engine binaries are presumed misplaced also. ${binary_path}
		# is reported by the command 'qconf -sconf'.
		# @user_action
		# Find 'qstat' in the Sun Grid Engine installation. Then
		# update ${binary_path} using the command 'qconf -mconf' if
		# appropriate.
		scds_syslog -p daemon.err -t $(syslog_tag) -m \
		"Validate - can't determine path to Grid Engine binaries"
		rc_validate=1
	fi

	utilbin_dir=`GetPathToUtilbin`

	if [ "${utilbin_dir}" = "none" ]
	then
		# SCMSGS
		# @explanation
		# The Sun Grid Engine binary 'gethostname' was not found in
		# ${SGE_ROOT}/utilbin/<arch>. 'gethostname' is used only
		# representatively. If it is not found the other Sun Grid
		# Engine utilities are presumed misplaced also.
		# @user_action
		# Find 'gethostname' in the Sun Grid Engine installation. Make
		# certain it is in a location conforming to
		# ${SGE_ROOT}/utilbin/<arch>, where <arch> is the result of
		# running ${SGE_ROOT}/util/arch.
		scds_syslog -p daemon.err -t $(syslog_tag) -m \
		"Validate - can't determine path to Grid Engine utility binaries"
		rc_validate=1
	fi

	HOST=`${utilbin_dir}/gethostname -aname`

	if [ "${HOST}" = "" ]
	then
		# SCMSGS
		# @explanation
		# ${SGE_ROOT}/utilbin/<arch>/gethostname when executed does
		# not return a name for the local host.
		# @user_action
		# Make certain the /etc/hosts and/or /etc/nsswitch.conf
		# file(s) is/are properly configured.
		scds_syslog -p daemon.err -t $(syslog_tag) -m \
		"Validate - can't resolve local hostname"
		rc_validate=1
	fi

	if [ ! -x ${SGE_ROOT}/util/arch ]
	then
        	# SCMSGS
        	# @explanation
        	# The file '${SGE_ROOT}/util/arch' was not found or is not
        	# executable.
        	# @user_action
        	# Make certain the shell script file '${SGE_ROOT}/util/arch'
        	# is both in that location, and executable.
        	scds_syslog -p daemon.err -t $(syslog_tag) -m \
	        "functions - Fatal: %s/util/arch not found or not executable" \
		"${SGE_ROOT}"
		rc_validate=1
	fi

       	debug_message "Function: validate_common - End"
}

# Shutdown
# Send SIGTERM to process name $1 with pid in file $2
# Used to shutdown sge_qmaster and sge_schedd
#
Shutdown()
{
   debug_message "Function: Shutdown - Begin"
   ${SET_DEBUG}

   name=$1
   pidfile=$2
   failme=0

   if [ -f ${pidfile} ]
   then
      pid=`cat ${pidfile}`
      if [ -x ${utilbin_dir}/checkprog ]
      then
	  maxretries=6
	  i=0
	  while [ ${i} -lt ${maxretries} ]; do
		${utilbin_dir}/checkprog ${pid} ${name} > /dev/null
		if [ "$?" = 0 ]; then
			kill ${pid}
		else
			break
		fi
		sleep `expr 2 + ${i}`
		i=`expr ${i} + 1`
	  done

	  ${utilbin_dir}/checkprog ${pid} ${name} > /dev/null

      	  if [ "$?" = 0 ]
          then
              kill -KILL ${pid}
	  fi

      else
	# SCMSGS
	# @explanation
	# The binary file ${SGE_ROOT}/utilbin/<arch>/checkprog does not exist,
	# or is not executable.
	# @user_action
	# Confirm the binary file ${SGE_ROOT}/utilbin/<arch>/checkprog both
	# exists in that location, and is executable.
	scds_syslog -p daemon.err -t $(syslog_tag) -m \
	"Shutdown - can't execute %s/checkprog binaries" \
	"${utilbin_dir}"
        failme=1
      fi
   else 
     # SCMSGS
     # @explanation
     # The file ${pidfile} was not found. ${pidfile} is the second argument to
     # the 'Shutdown' function. 'Shutdown' is called within the stop scripts
     # of sge_qmaster-rs and sge_schedd-rs resources.
     # @user_action
     # Confirm the file schedd.pid or qmaster.pid exists. Said file should be
     # found in the respective spool directory of the daemon in question.
     scds_syslog -p daemon.err -t $(syslog_tag) -m \
     "Shutdown - can't access pidfile %s of process %s" \
     "${pidfile}" "${name}"
     failme=1
   fi
	
   debug_message "Function: Shutdown - end"
}

validate_qmaster_schedd_common()
{
	#
	# Validate Qmaster/Sched common functions
	#
	
        debug_message "Function: validate_qmaster_schedd_common - Begin"
	${SET_DEBUG}

	if [ ! -x ${bin_dir}/qping ]
	then
   		# SCMSGS
   		# @explanation
   		# The binary file qping can not be found, or is nor excutable.
   		# @user_action
   		# Confirm the binary file '${SGE_ROOT}/bin/<arch>/qping'
   		# exists at that location, and is executable. qping was
   		# introduced with Sun Grid Engine 6.0.
   		scds_syslog -p daemon.err -t $(syslog_tag) -m \
		"Validate - qping file does not exist or is not executable at %s/qping" \
		"${bin_dir}"
		rc_validate=1
	fi

	if [ ! -x ${bin_dir}/qconf ]
	then
   		# SCMSGS
   		# @explanation
   		# The binary file qconf can not be found, or is not
   		# executable.
   		# @user_action
   		# Confirm the binary file ${SGE_ROOT}/bin/<arch>/qconf both
   		# exists in that location, and is executable.
   		scds_syslog -p daemon.err -t $(syslog_tag) -m \
		"Validate - qconf file does not exist or is not executable at %s/qconf" \
		"${bin_dir}"
		rc_validate=1
	fi

  	if [ ! -x ${utilbin_dir}/checkprog ]
	then
   		# SCMSGS
   		# @explanation
   		# The binary file ${SGE_ROOT}/utilbin/<arch>/checkprog does
   		# not exist, or is not executable.
   		# @user_action
   		# Confirm the binary file ${SGE_ROOT}/utilbin/<arch>/checkprog
   		# both exists in that location, and is executable.
   		scds_syslog -p daemon.err -t $(syslog_tag) -m \
		"Validate - checkprog file does not exist or is not executable at %s/checkprog" \
		"${utilbin_dir}"
		rc_validate=1
	fi

  	if [ ! -x ${utilbin_dir}/getservbyname ]
	then
   		# SCMSGS
   		# @explanation
   		# The file 'getservbyname' can not be found, or is not
   		# executable.
   		# @user_action
   		# Confirm the file '${SGE_ROOT}/utilbin/<arch>/getservbyname'
   		# both exists in that location, and is executable.
   		scds_syslog -p daemon.err -t $(syslog_tag) -m \
		"Validate - getservbyname file does not exist or is not executable at %s/getservbyname" \
		"${utilbin_dir}"
		rc_validate=1
	fi

	qmaster_spool_dir=`QmasterSpoolDir`

	if [ "${qmaster_spool_dir}" = "" ]
	then
		# SCMSGS
		# @explanation
		# The qmaster spool directory could not be determined, using
		# the 'QmasterSpoolDir' function.
		# @user_action
		# Use the command 'qconf -sconf' to determine the value of
		# '${qmaster_spool_dir}'. Update/correct the value if
		# necessary using the command 'qconf -mconf'.
		scds_syslog -p daemon.err -t $(syslog_tag) -m \
		"Validate - can't determine Qmaster Spool dir"
		rc_validate=1
	fi

        debug_message "Function: validate_qmaster_schedd_common - End"
}
