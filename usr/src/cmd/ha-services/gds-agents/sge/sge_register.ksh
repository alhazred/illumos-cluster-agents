#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)sge_register.ksh	1.9	07/06/06 SMI"
#

MYCONFIG=""
MYNAME=`basename $0`
GDSTYPE=SUNW.gds

typeset opt

while getopts 'f:' opt
do
	case "${opt}" in
		f)	MYCONFIG=${OPTARG};;
		*)	echo "ERROR: ${MYNAME} Option ${OPTARG} unknown - early End. Only the option -f <filename> is valid."
			exit 1;;
	esac
done

if [ -n "${MYCONFIG}" ] && [ -f "${MYCONFIG}" ]
then
         echo "sourcing ${MYCONFIG}"
         . ${MYCONFIG}
else
        PKGCONF=`dirname $0`/sge_config
        echo "sourcing ${PKGCONF}"
        . ${PKGCONF}
fi

if [ "${SGE_VER}" = "6.0" ]; then
	# Register resource for sge_qmaster
	/usr/cluster/bin/scrgadm -a -j ${QMASTERRS} -g ${MASTERRG} -t ${GDSTYPE} \
-x Start_command="/opt/SUNWscsge/bin/sge_qmaster6/start_sge_qmaster \
-R ${QMASTERRS} -G ${MASTERRG} -S ${SGE_ROOT} -C ${SGE_CELL}" \
-x Stop_command="/opt/SUNWscsge/bin/sge_qmaster6/stop_sge_qmaster \
-R ${QMASTERRS} -G ${MASTERRG} -S ${SGE_ROOT} -C ${SGE_CELL}" \
-x Probe_command="/opt/SUNWscsge/bin/sge_qmaster6/probe_sge_qmaster \
-R ${QMASTERRS} -G ${MASTERRG} -S ${SGE_ROOT} -C ${SGE_CELL}" \
-y Port_list=${MASTERPORT}/tcp -y Network_resources_used=${MASTERLH} \
-x Stop_signal=9 \
-x probe_timeout=90 -y Thorough_probe_interval=120 \
-y retry_count=2 -y retry_interval=900 \
-y Resource_dependencies=${MASTERHASP}
	St=$?
	if [ "${St}" -ne 0 ]; then
		echo "Error: Registration of resource ${QMASTERRS} failed, please correct the wrong parameters"
		exit 1
	else
		echo "Registration of resource ${QMASTERRS} succeeded"
	fi

	# Register resource for sge_schedd
	/usr/cluster/bin/scrgadm -a -j ${SCHEDDRS} -g ${MASTERRG} -t ${GDSTYPE} \
-x Start_command="/opt/SUNWscsge/bin/sge_schedd6/start_sge_schedd \
-R ${SCHEDDRS} -G ${MASTERRG} -S ${SGE_ROOT} -C ${SGE_CELL}" \
-x Stop_command="/opt/SUNWscsge/bin/sge_schedd6/stop_sge_schedd \
-R ${SCHEDDRS} -G ${MASTERRG} -S ${SGE_ROOT} -C ${SGE_CELL}" \
-x Probe_command="/opt/SUNWscsge/bin/sge_schedd6/probe_sge_schedd \
-R ${SCHEDDRS} -G ${MASTERRG} -S ${SGE_ROOT} -C ${SGE_CELL}" \
-y Port_list=${MASTERPORT}/tcp -y Network_resources_used=${MASTERLH} \
-x Stop_signal=9 \
-x probe_timeout=90 -y Thorough_probe_interval=120 \
-y retry_count=2 -y retry_interval=900 \
-y Resource_dependencies=${QMASTERRS}
	St=$?
	if [ "${St}" -ne 0 ]; then
		echo "Error: Registration of resource ${SCHEDDRS} failed, please correct the wrong parameters and try again."
		echo "Removing resource ${QMASTERRS}."
		/usr/cluster/bin/scrgadm -r -j ${QMASTERRS}
		exit 1
	else
		echo "Registration of resource ${SCHEDDRS} succeeded"
	fi
else
	echo "Fatal: Please set variable SGE_VER properly in `dirname $0`/sge_config!"
	exit 1
fi
