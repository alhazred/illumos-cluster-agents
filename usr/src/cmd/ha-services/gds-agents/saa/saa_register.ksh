#!/usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#ident	"@(#)saa_register.ksh	1.7	07/06/06 SMI"

. `dirname $0`/saa_config

if [ "${PERFORM_REAL_MONITORING}" -eq 0 ]; then
	PROBEFLAG=""
elif [ "${PERFORM_REAL_MONITORING}" -eq 1 ]; then
	PROBEFLAG="-P"
else
	echo "Error: PERFORM_REAL_MONITORING must be set to either 0 or 1!"
	exit 1
fi

/usr/cluster/bin/scrgadm -a -j $RS -g $RG -t SUNW.gds \
-x Start_command="/opt/SUNWscsaa/bin/start_SAA \
-R $RS -G $RG $PROBEFLAG " \
-x Stop_command="/opt/SUNWscsaa/bin/stop_SAA \
-R $RS -G $RG $PROBEFLAG " \
-x Probe_command="/opt/SUNWscsaa/bin/probe_SAA \
-R $RS -G $RG $PROBEFLAG " \
-y Port_list=$PORT/tcp -y Network_resources_used=$LH \
-x Stop_signal=9 \
-y Resource_dependencies=$HAS_RS
