#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#
#ident	"@(#)functions.ksh	1.10	08/07/21 SMI"
#

ZONENAME=/usr/bin/zonename

SCLOGGER=/usr/cluster/lib/sc/scds_syslog
SCHA_RESOURCE_GET=/usr/cluster/bin/scha_resource_get
SCHA_RESOURCE_SETSTATUS=/usr/cluster/bin/scha_resource_setstatus
PMFADM=/usr/cluster/bin/pmfadm

PKG=SUNWscsaa
METHOD=`basename $0`
LOGFILE=/var/tmp/${RESOURCE}_logfile
export PATH=/usr/bin:/bin:/usr/sbin
export USR_SWA=/usr/swa                 # SWA instance information
export ALL_ADM INST_NAME
export ARCH=SunOS

syslog_tag()
{
        #
        # Data Service message format
        #

        ${SET_DEBUG}

        print "SC[${PKG:-??}.${METHOD:-??}]:${RESOURCEGROUP:-??}:${RESOURCE:-??}"
}

scds_syslog()
{

        #
        # Log a message
        #

        $SET_DEBUG

        $SCLOGGER "$@" &
}

debug_message()
{
        #
        # Output a debug message to syslog if required
        #

        if [ "$DEBUG" = "$RESOURCE" -o "$DEBUG" = "ALL" ]
        then
                SET_DEBUG="set -x"

                DEBUG_TEXT=$1

                scds_syslog -p daemon.debug -t $(syslog_tag) -m \
                        "%s" "$DEBUG_TEXT"
        else
                SET_DEBUG=
        fi
}

log_message()
{
        #
        # Output a message to syslog as required
        #

        debug_message "Function: log_message - Begin"
        $SET_DEBUG

        if [ -s "${LOGFILE}" ]
        then
                PRIORITY=$1
                HEADER=$2

		#
		# Ensure the while loop only reads a closed file
		#

		strings ${LOGFILE} > ${LOGFILE}.copy
                while read MSG_TXT
                do
                        scds_syslog -p daemon.${PRIORITY} -t $(syslog_tag) -m \
                                "%s - %s" \
                                "${HEADER}" "${MSG_TXT}"
                done < ${LOGFILE}.copy

		cat /dev/null > ${LOGFILE} > /dev/null
		cat /dev/null > ${LOGFILE}.copy
        fi

        debug_message "Function: log_message - End"
}

zone_function()
{
	debug_message "Function: zone_function - Begin"
	${SET_DEBUG}

	#
	# Initialize PZONEOPT as empty
	PZONEOPT=""

	#
	# If Solaris does not have /usr/bin/zonename just return 0
	# else add "-z <zonename>" to PZONEOPT
	#
	if [ -x "${ZONENAME}" ];
	then
		PZONEOPT="-z `${ZONENAME}`"
	fi

	debug_message "Function: zone_function - End"
	return 0
}

validate_options()
{
        #
        # Ensure all options are set
        #

        for i in RESOURCE RESOURCEGROUP 
        do
                case $i in
                        RESOURCE)
                        if [ -z ${RESOURCE} ]; then
                                scds_syslog -p daemon.err -t $(syslog_tag) -m \
                                "ERROR: Option -%s not set" "R"
                                exit 1
                        fi;;

                        RESOURCEGROUP)
                        if [ -z ${RESOURCEGROUP} ]; then
                                scds_syslog -p daemon.err -t $(syslog_tag) -m \
                                "ERROR: Option -%s not set" "G"
                                exit 1
                        fi;;
                esac
        done
}

validate()
{
	#
	# Validate SAA
	#
	
        debug_message "Function: validate - Begin"
	${SET_DEBUG}

	rc_validate=0

	#
	# Validate that ${filename} exists
	#
	filename="${USR_SWA}/insts"

	if [ ! -f "${filename}" ]
	then
   		scds_syslog -p daemon.error -t $(syslog_tag) -m \
			"Validate - file %s does not exist" \
			"${filename}"
		rc_validate=1
	else
		debug_message "Validate - ${filename} exists"
	fi

	#
	# For SAA 6.2 enforce that the agent does only support one
	# SAA instance.
	#
	if [ "$(/bin/cat ${filename} | /bin/awk '{print $2}' | /bin/cut -c 2-4 | /bin/uniq)" = "6.2" ]
	then
		if [ "$(/bin/cat ${filename} | /bin/wc -l)" -ne 1 ]
		then
			# SCMSGS
			# @explanation
			# The agent does only support exactly one
			# SWIFTAlliance Access instance.
			# @user_action
			# Verify that the file /usr/swa/insts does only contain
			# one line describing the SWIFTAlliance Access instance.
			scds_syslog -p daemon.error -t $(syslog_tag) -m \
				"Validate - %s must contain exactly one SWIFTAlliance Access instance" \
				"${filename}"
			rc_validate=1
		fi
	fi

	debug_message "Function: validate - End"
}

start_dce()
{
	#
	# Start dce
	#

        debug_message "Function: start_dce - Begin"
	$SET_DEBUG
	
	#
	# This should take care of sh, ksh, csh, tcsh and bash
	#
	/opt/dce/tcl/start.dce >> $LOGFILE 2>&1
	rc_start_dce=$?

	debug_message "Function: start_dce - End"
}

stop_dce()
{
	#
	# Stop dce
	#

        debug_message "Function: stop_dce - Begin"
	$SET_DEBUG

	/opt/dce/tcl/stop.dce >> $LOGFILE 2>&1
	rc_stop_dce=$?
	
	debug_message "Function: stop_dce - End"
}

check_dce()
{
	# 
	# Probe dce
	#

        debug_message "Function: check_dce - Begin"
	$SET_DEBUG

	rc_check_dce=0

	RESULT=`/opt/dce/tcl/show.cfg | grep RPC | awk ' {print $3}'`

	case "$RESULT" in
		'Not')
        		#can not connect to dced assuming it is dead
        		#print "ERROR    dced is dead"
        		rc_check_dce=1
        	;;
		'Running')
        		#no problemmo  dced is running fine
        		rc_check_dce=0
        	;;
	esac
	
	debug_message "Function: check_dce - End"
}

get_SAA_version()
{
	debug_message "Function: get_SAA_version - Begin"
	${SET_DEBUG}

	#
	# as of version 5.9 of SAA, no more dce client needed... so version is checked here..
	# to determine correct startup
	#
	saa_version=`cat ${USR_SWA}/insts | awk '{print $2}' |cut -c 2-4 |uniq`
	saa_major=`echo $saa_version | cut -d "." -f 1`
	saa_minor=`echo $saa_version | cut -d "." -f 2`
	if [ $saa_major -le 5 ] ; then
		# rev is 5, checking for special version ( pre 6 )
		if [ $saa_minor -eq 9 ] && [ $saa_major -eq 5 ]; then
			# found the pre-6 release of saa... turning off dce
			use_dce=0
		else
			# release is below 5.9... using dce
			use_dce=1
		fi
	else
		#revision is higer than 5... no need for dce anymore
		use_dce=0
	fi
	export use_dce

	debug_message "Function: get_SAA_version - End"
}

start_SAA()
{
	#
	# Start SAA
	#

        debug_message "Function: start_SAA - Begin"
	${SET_DEBUG}

	# initialize PZONEOPT
	zone_function

	while read TEMP1 TEMP2 TEMP3 ALL_ADM TEMP5 INST_NAME TEMP7
	do
		( 
		if [ -f ${USR_SWA}/alliance_init ]
		then
        		. ${USR_SWA}/alliance_init -s -i "${INST_NAME}"
			${ALLIANCE}/INS/bin/${ARCH}/rc.alliance start &
			rc_start_command=$?
		else
			rc_start_command=100
			
		fi

		#
		# First wait for a_start_server to appear.
		#
		while true
		do
			if pgrep ${PZONEOPT} -u ${ALL_ADM} '^a_start_server$' > /dev/null
			then
				break
			else 
				sleep 3
			fi
		done

		#
		# Loop until a_start_server disappears again.
		# To be sure not to return until saa is really started.
		#
		while true
		do
			if pgrep ${PZONEOPT} -u ${ALL_ADM} '^a_start_server$' > /dev/null
			then
				debug_message "saa server still starting... wait"
				sleep 3
			else
				debug_message "saa server start script is not running anymore"
				break
			fi
		done ) >> $LOGFILE 2>&1
		
	done < ${USR_SWA}/insts

	debug_message "Function: start_SAA - End"
}

check_hostrename()
{
	debug_message "Function: check_hostrename - Begin"
	${SET_DEBUG}

	rc_check_hostrename=0
	while read TEMP1 TEMP2 TEMP3 ALL_ADM TEMP5 INST_NAME TEMP7
	do
		if [ -f "${USR_SWA}/alliance_init" ]; then
			# call routine to see if the instance is running
			check_inst_running
				if [ "$rc_check_running" -eq 0 ]; then
					debug_message "Instance $INST_NAME servers is running"
				else
					/bin/su $ALL_ADM -c '
					export HOME=~$ALL_ADM
					. $USR_SWA/alliance_init -s -i "$INST_NAME"
					# Clean-up Alliance db (rename host)

					echo "Making SWIFTAlliance $INST_NAME ready"
					TO_HOST=`hostname`
					FROM_HOST=`cat $ALLIANCE_DB/.alliance_host`
					$ALLIANCE/BSS/bin/$ARCH/alliance hostrename "$FROM_HOST" "$TO_HOST"
					exit
					' >> $LOGFILE 2>&1
					debug_message "Ended SWIFTAlliance host rename"
				fi
		else
			debug_message "Can not find ${USR_SWA}/alliance_init for instance ${INST_NAME}"
			rc_check_hostrename=100
		fi
	done < $USR_SWA/insts

	debug_message "Function: check_hostrename - End"
}

check_inst_running()
{	
	debug_message "Function: check_inst_running - Begin"
	$SET_DEBUG

	/bin/su ${ALL_ADM} -c '
 		export HOME=~${ALL_ADM}
		. ${USR_SWA}/alliance_init -s -i "${INST_NAME}"

		echo "Checking if SWIFTAlliance instance ${INST_NAME} is running"
		export LIBPATH=${ALLIANCE}/common/lib/${ARCH}:${ALLIANCE}/BSS/lib/${ARCH}
		${ALLIANCE}/BSS/bin/${ARCH}/alliance -E ${ALLIANCE}/BSS/bin/${ARCH}/is_listening -- -- -P /BS_csys -T 10' >> ${LOGFILE} 2>&1

	if [ $? -ne 0 ]
	then
		debug_message "SWIFTAlliance instance ${INST_NAME} not running"
		rc_check_running=100
	else
		debug_message "SWIFTAlliance instance ${INST_NAME} is running"
		rc_check_running=0
	fi

	debug_message "Function: check_inst_running - End"
}

start_swa_boot()
{
	debug_message "Function: start_swa_boot - Begin"
	$SET_DEBUG

	# Check if /etc/init.d/rc.swa_boot exists
	# needed for 5.9 and higher
	if [ -f /etc/init.d/rc.swa_boot ]
	then
		debug_message "Starting SWIFTAlliance swa_boot process"
		/etc/init.d/rc.swa_boot start >> $LOGFILE 2>&1
		rc_start_swa_boot=$?
	else
		debug_message "Can not find /etc/init.d/rc.swa_boot"
		rc_start_swa_boot=100
	fi

	debug_message "Function: start_swa_boot - End"
}

stop_swa_boot()
{
	debug_message "Function: stop_swa_boot - Begin"
	$SET_DEBUG

	# Check if /etc/init.d/rc.swa_boot exists
	# needed for 5.9 and higher
	if [ -f /etc/init.d/rc.swa_boot ]
	then
		debug_message "Starting SWIFTAlliance swa_boot process"
		/etc/init.d/rc.swa_boot stop >> $LOGFILE 2>&1
		rc_stop_swa_boot=$?
	else
		debug_message "Can not find /etc/init.d/rc.swa_boot"
		rc_stop_swa_boot=100
	fi

	debug_message "Function: stop_swa_boot - End"
}

start_swa_rpcd()
{
	debug_message "Function: start_swa_rpcd - Begin"
	$SET_DEBUG

	#check if file exists
	if [ -f $USR_SWA/swa_rpcd ]
	then 
		debug_message "Starting SWIFTAlliance rpc process"
		$USR_SWA/swa_rpcd >> $LOGFILE 2>&1
		rc_start_swa_rpcd=$?
	else
		debug_message "can not find $USR_SWA/swa_rpcd"
		rc_start_swa_rpcd=100
	fi

	debug_message "Function: start_swa_rpcd - End"
}

stop_swa_rpcd()
{
	debug_message "Function: stop_swa_rpcd - Begin"
	${SET_DEBUG}

	# initialize PZONEOPT
	zone_function

	SWA_PID=`pgrep ${PZONEOPT} -u ${ALL_ADM} '^swa_rpcd$'`
	if [ -n "${SWA_PID}" ]
	then
		if [ -z ${SWA_PID} ]
		then
			debug_message "no swa_rpcd running"
			rc_stop_swa_rpcd=0
		else
        		kill -KILL ${SWA_PID} >> ${LOGFILE} 2>&1
        		if [ $? -ne 0 ]
        		then
        		        debug_message "Cannot kill swa_rpcd process (pid ${SWA_PID})"
        		        rc_stop_swa_rpcd=100
        		else
        		        sleep 2
				rc_stop_swa_rpcd=0
        		fi
		fi
	fi

	debug_message "Function: stop_swa_rpcd - End"
}

stop_SAA()
{
	#
	# Stop SAA
	#

        debug_message "Function: stop_SAA - Begin"
	$SET_DEBUG

	# initialize PZONEOPT
	zone_function

	rc_stop_command=0

	while read TEMP1 TEMP2 TEMP3 ALL_ADM TEMP5 INST_NAME TEMP7
	do
		(
		check_inst_running
		if [ "$rc_check_running" -ne 0 ]
		then
			# empty line
			echo "SWIFTAlliance instance $INST_NAME not running"
			do_nothing=0
		else
			echo "Stopping SWIFTAlliance instance $INST_NAME"
			. $USR_SWA/alliance_init -s -i "$INST_NAME"
			/bin/su $ALL_ADM -c ' $ALLIANCE/BSS/bin/$ARCH/alliance stop_server

			if [ $? -ne 0 ]
			then
				echo "Could not stop SWIFTAlliance instance ${INST_NAME}"
				rc_stop_command=1
				return 1
			else
				echo "Successfully stopped SWIFTAlliance instance $INST_NAME"
			fi

			if [ -f $ALLIANCE/BSS/bin/$ARCH/select_swnet_inst ]
			then
				SELECTED=`$ALLIANCE/BSS/bin/$ARCH/alliance -E $ALLIANCE/BSS/bin/$ARCH/select_swnet_inst -- -- -E -R 2>&1 1>/dev/null`
				if [ $? -ne 0 ]
				then
					echo "SWIFTAlliance $SELECTED - no SNL"
				else
					echo "Setting SNL environment - $SELECTED"
					eval $SELECTED
					export PATH=$PATH:$SWNET_HOME/bin
					. swiftnet init

					echo "Stopping SNL"
					swiftnet stop

					# clean_ipc does not exist when RA is used
					if [ -f $SWNET_HOME/bin/clean_ipc ]
					then
						clean_ipc 1>/dev/null
					fi
				fi
				
			fi '
		fi ) >> $LOGFILE 2>&1
		
		#### loop over the processes until they dont exist.
		###################################################
		while true
		do
			if pgrep ${PZONEOPT} -u ${ALL_ADM} '^a_stop_server$' > /dev/null
			then
				debug_message "In STOP waiting for stop of processes"
				sleep 10
			else
				break
			fi
		done
	
	done < $USR_SWA/insts

	debug_message "Function: stop_SAA - End"
}

check_SAA()
{
	# 
	# Probe SAA
	#

        debug_message "Function: check_SAA - Begin"
	$SET_DEBUG

	###################################
	# As required by SWIFT, no monitoring of the application is done by
	# default since application control is kept in the SAA admin console.
	# The cluster is used to be able to automate the SAA failover in case of 
	# failures of the framework. Sun Cluster will not care about the
	# application but fail it over, on demand, or when network or
	# hardware failure occurs.
	#
	# If a full probe is required, and then manual application intervention
	# is prohibited, a full probe can get achieved by setting
	# PERFORM_REAL_MONITORING=1
	# within /opt/SUNWscsaa/util/saa_config and re-registering the resource.
	###################################

	while read TEMP1 TEMP2 TEMP3 ALL_ADM TEMP5 INST_NAME TEMP7
	do
		if pgrep -u root -f "start_SAA .*-R ${RESOURCE} " >/dev/null
		then
			rc_check_SAA=100
			return 100
		else
			rc_check_SAA=0
		fi

		check_inst_running
	done < $USR_SWA/insts

	if [ "${rc_check_running}" -ne 0 ]; then
		${SCHA_RESOURCE_SETSTATUS} -G ${RESOURCEGROUP} -R ${RESOURCE} -s DEGRADED -m "SAA Instance offline"

		if [ "${PERFORM_REAL_MONITORING}" -ne 0 ]; then
			rc_check_SAA=100
		fi
	else
		${SCHA_RESOURCE_SETSTATUS} -G ${RESOURCEGROUP} -R ${RESOURCE} -s OK -m "SAA Instance online"
	fi

	debug_message "Function: check_SAA - End"
}

start_DB_SAA()
{
	#
	# Start swa_boot, embedded database and SAA instance
	#

	debug_message "Function: start_DB_SAA - Begin"
	${SET_DEBUG}

	rc_start_command=0

	# Make sure the required entries within /etc/services exist
	# by calling /usr/swa/apply_alliance_ports
	APPLY_ALLIANCE_PORTS_CMD=${USR_SWA}/apply_alliance_ports
	if [ -x ${APPLY_ALLIANCE_PORTS_CMD} ]
	then
		${APPLY_ALLIANCE_PORTS_CMD} >> ${LOGFILE} 2>&1
	else
		# SCMSGS
		# @explanation
		# The command /usr/swa/apply_alliance_ports does not
		# exist but is required for SWIFTAlliance Access to
		# properly function.
		# @user_action
		# Verify the SWIFTAlliance Access instrallation and make
		# sure /usr/swa/apply_alliance_ports does exist.
		scds_syslog -p daemon.error -t $(syslog_tag) -m \
			"start_DB_SAA - Command %s does not exist" \
			"${APPLY_ALLIANCE_PORTS_CMD}"
		rc_start_command=1
		debug_message "Function: start_DB_SAA - End"
		return 1
	fi

	# Initialize instance information from /usr/swa/insts into variables
	/bin/cat ${USR_SWA}/insts | read INST_HOME INST_VERSION T1 SAA_OWNER T2 INST_NAME T3

	# start swa_boot process
	if [ -x ${INST_HOME}/bin/saa_bootstrap ]
	then
		/bin/su ${SAA_OWNER} -c "${INST_HOME}/bin/saa_bootstrap -nameservice start" >> ${LOGFILE} 2>&1
		if [ $? -ne 0 ]
		then
			rc_start_command=1
			debug_message "Function: start_DB_SAA - End"
			return 1
		fi
	else
		# SCMSGS
		# @explanation
		# The referenced command is not executable or does not
		# exist, but is required for SWIFTAlliance Access to
		# properly function.
		# @user_action
		# Verify the SWIFTAlliance Access instrallation and make
		# sure the referenced command does exist and is executable.
		scds_syslog -p daemon.error -t $(syslog_tag) -m \
			"start_DB_SAA - Command %s does not exist or is not executable" \
			"${INST_HOME}/bin/saa_bootstrap"
		rc_start_command=1
		debug_message "Function: start_DB_SAA - End"
		return 1
	fi

	# start SAA embedded DB and instance
	if [ -x ${USR_SWA}/alliance_init ]
        then
		. ${USR_SWA}/alliance_init -s -i "${INST_NAME}" >> ${LOGFILE} 2>&1
		/bin/su ${SAA_OWNER} -c "${INST_HOME}/bin/saa_bootstrap -saastart start" >> ${LOGFILE} 2>&1
		rc_start_command=$?
	else
		scds_syslog -p daemon.error -t $(syslog_tag) -m \
			"start_DB_SAA - Command %s does not exist or is not executable" \
			"${USR_SWA}/alliance_init"
		rc_start_command=1
		debug_message "Function: start_DB_SAA - End"
		return 1
	fi
	
	debug_message "Function: start_DB_SAA - End"
}

stop_DB_SAA()
{
	#
	# Stop SAA instance, embedded database and swa_boot
	#

	debug_message "Function: stop_DB_SAA - Begin"
	${SET_DEBUG}

	rc_stop_command=0

	# Initialize instance information from /usr/swa/insts into variables
	/bin/cat ${USR_SWA}/insts | read INST_HOME INST_VERSION T1 SAA_OWNER T2 INST_NAME T3

	# stop SAA embedded DB and instance
	if [ -x ${USR_SWA}/alliance_init -a -x ${INST_HOME}/bin/saa_bootstrap ]
        then
		. ${USR_SWA}/alliance_init -s -i "${INST_NAME}" >> ${LOGFILE} 2>&1
		/bin/su ${SAA_OWNER} -c "${INST_HOME}/bin/saa_bootstrap stop" >> ${LOGFILE} 2>&1
		rc_stop_command=$?
	else
		# SCMSGS
		# @explanation
		# The referenced commands are either not executable or do not
		# exist, but are required for SWIFTAlliance Access to
		# properly function.
		# @user_action
		# Verify the SWIFTAlliance Access instrallation and make
		# sure the referenced commands do exist and are executable.
		scds_syslog -p daemon.error -t $(syslog_tag) -m \
			"stop_DB_SAA - Command %s and/or %s do not exist or are not executable" \
			"${USR_SWA}/alliance_init" "${INST_HOME}/bin/saa_bootstrap"
		rc_stop_command=1
		debug_message "Function: stop_DB_SAA - End"
		return 1
	fi

	# stop swa_boot process
	/bin/su ${SAA_OWNER} -c "${INST_HOME}/bin/saa_bootstrap -nameservice stop" >> ${LOGFILE} 2>&1
	if [ $? -ne 0 ]
	then
		rc_stop_command=1
	fi

	debug_message "Function: stop_DB_SAA - End"
}

check_DB_SAA()
{
	#
	# Check SAA instance and implicitly the embedded database
	#

	debug_message "Function: check_DB_SAA - Begin"
	${SET_DEBUG}

	###################################
	# As required by SWIFT, no monitoring of the application is done by
	# default since application control is kept in the SAA admin console.
	# The cluster is used to be able to automate the SAA failover in case
	# of failures of the framework. Sun Cluster will not care about the
	# application but fail it over, on demand, or when network or
	# hardware failure occurs.
	#
	# If a full probe is required, and then manual application intervention
	# is prohibited, a full probe can get achieved by setting
	# PERFORM_REAL_MONITORING=1
	# within /opt/SUNWscsaa/util/saa_config and re-registering the resource.
	###################################
	rc_check_SAA=0

	# check if the start_SAA script is still running
	# this implements wait-for-online, to make sure the start_SAA script
	# finished before further probing the SAA instance
	if pgrep -u root -f "start_SAA .*-R ${RESOURCE} " >/dev/null
	then
		rc_check_SAA=100
		debug_message "Function: check_DB_SAA - End"
		return 100
	fi

	# Initialize instance information from /usr/swa/insts into variables
	/bin/cat ${USR_SWA}/insts | read INST_HOME INST_VERSION T1 SAA_OWNER T2 INST_NAME T3

	# Check if the SAA instance is running. This implicitly checks also
	# if the embedded database is running.
	# On faliure, by default only update the status message to indicate
	# if the SAA instance is online or offline.
	if [ -x ${USR_SWA}/alliance_init -a -x ${INST_HOME}/BSS/bin/${ARCH}/alliance -a -x ${INST_HOME}/BSS/bin/${ARCH}/is_listening ]
        then
		. ${USR_SWA}/alliance_init -s -i "${INST_NAME}" > /dev/null 2>&1
		/bin/su ${SAA_OWNER} -c "${INST_HOME}/BSS/bin/${ARCH}/alliance -E ${INST_HOME}/BSS/bin/${ARCH}/is_listening -- -- -P /BS_csys -T 10" > /dev/null 2>&1
		if [ $? -ne 0 ]
		then
			${SCHA_RESOURCE_SETSTATUS} -G ${RESOURCEGROUP} -R ${RESOURCE} -s DEGRADED -m "SAA Instance offline"
			if [ "${PERFORM_REAL_MONITORING}" -ne 0 ]
			then
				rc_check_SAA=100
			fi
		else
			${SCHA_RESOURCE_SETSTATUS} -G ${RESOURCEGROUP} -R ${RESOURCE} -s OK -m "SAA Instance online"
		fi
	fi

	debug_message "Function: check_DB_SAA - End"
}
