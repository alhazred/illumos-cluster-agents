#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)saa_config.ksh	1.7	07/06/06 SMI"
# 
# This file will be sourced in by SAA_register and the parameters
# listed below will be used.
#
# These parameters can be customized in (key=value) form
#
#        RS - name of the resource for the application
#        RG - name of the resource group containing RS
#      PORT - name of the port number
#        LH - name of the LogicalHostname SC resource
#    HAS_RS - name of the HAStoragePlus SC resource
#
#  PERFORM_REAL_MONITORING - Controls if real SAA monitoring is performed.
#                            The default is 0, which means the SAA fault
#                            monitor will not trigger restarts or failover.
#                            This will enable being able to perform manual
#                            intervention to the SAA application.
#
#                 IMPORTANT: This behaviour was explicitly requested by SWIFT.
#                 ========== It will allow users unfamiliar with the Sun Cluster
#                            software to operate the application as if it is
#                            not running on a cluster:
#                            - Operator triggering manually SWIFTAlliance Access
#                              the restart function (e.g. to run SWIFTAlliance
#                              Access in housekeeping mode).
#                            - automatic/scheduled SWIFTAlliance Access restart
#                              (e.g. to run database backup and other
#                              maintenance/end-of-day processes).
#                            - any graceful SWIFTAlliance Access restart /
#                              recovery in case of an SWIFTAlliance Access
#                              transient local error.
#
#                            Enabling PERFORM_REAL_MONITORING=1 is NOT
#                            recommended by Swift. 
#
#                            If this variable is set to 1, the fault monitor
#                            will also trigger restarts and failover,
#                            depending on the configuration for the
#                            resource properties Retry_count, Failover_enabled
#                            and Failover_mode.
#
#                 IMPORTANT: Setting PERFORM_REAL_MONITORING=1
#                 ========== mandates only using the Sun Cluster administrative
#                            commands in order to enable/disable the resource
#                            and thus starting and stopping the SAA application.
#                            The SAA application then must never get started
#                            or stopped through a different mechanism.
#

RS=
RG=
PORT=
LH=
HAS_RS=
PERFORM_REAL_MONITORING=0
