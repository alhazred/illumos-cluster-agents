#!/usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)stop_SAA.ksh	1.8	08/07/21 SMI"
#

PERFORM_REAL_MONITORING=0

typeset opt

while getopts 'R:G:P' opt
do
        case "$opt" in
                R)      RESOURCE=$OPTARG;;
                G)      RESOURCEGROUP=$OPTARG;;
		P)	PERFORM_REAL_MONITORING=1;;
                *)      exit 1;;
        esac
done

. `dirname $0`/../etc/config
. `dirname $0`/functions

validate_options

debug_message "Method: `basename $0` - Begin"
$SET_DEBUG

get_SAA_version

if [ "${saa_version}" = "6.2" ]; then
	stop_DB_SAA
else
	stop_SAA

	if [ -z "$use_dce" ];then
		log_message err "use_dce var is empty , error in agent"
		exit 1
	fi

	if [ "$use_dce" -eq 1 ]; then

		stop_dce
		if [ "$rc_stop_dce" -eq 0 ]
		then
			log_message notice "stopped dce rc<$rc_stop_dce>"
		else
			rc_stop_command=1
			log_message err "stop dced failed rc<$rc_stop_dce>"
		fi

		stop_swa_rpcd
		if [ "$rc_stop_swa_rpcd" -ne 0 ]
		then
			log_message err "could not kill swa_rpcd"
		fi
	else
		stop_swa_boot
		if [ "$rc_stop_swa_boot" -ne 0 ]
		then
			log_message err "could not kill swa_boot"
		fi
	fi
fi

# make sure the dummy sleep started in start_SAA did already
# finish, otherwise kill it.
if ${PMFADM} -q ${RESOURCEGROUP},${RESOURCE},0.svc
then
	${PMFADM} -s ${RESOURCEGROUP},${RESOURCE},0.svc KILL 2> /dev/null
fi

if [ "$rc_stop_command" -eq 0 ]
then
        log_message notice "stopped SAA rc<$rc_stop_command>"
	debug_message "Method: `basename $0` - End (Exit 0)"
	exit 0
else
        log_message err "stop SAA failed rc<$rc_stop_command>"
	debug_message "Method: `basename $0` - End (Exit 1)"
	exit 1
fi
