#!/usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)probe_SAA.ksh	1.8	08/07/21 SMI"
#

PERFORM_REAL_MONITORING=0

typeset opt

while getopts 'R:G:P' opt
do
        case "$opt" in
                R)      RESOURCE=$OPTARG;;
                G)      RESOURCEGROUP=$OPTARG;;
		P)	PERFORM_REAL_MONITORING=1;;
                *)      exit 1;;
        esac
done

. `dirname $0`/../etc/config
. `dirname $0`/functions

validate_options

debug_message "Method: `basename $0` - Begin"
$SET_DEBUG

validate

if [ "${rc_validate}" -ne 0 ]
then
        debug_message "Method: `basename $0` - End (Exit 100)"
        exit 100
fi

get_SAA_version

if [ "${saa_version}" = "6.2" ]; then
	check_DB_SAA
else
	if [ -z "$use_dce" ]; then
		log_message err "Error: Can not determine SAA version!"
		debug_message "Method: `basename $0` - End (Exit 100)"
		exit 100
	fi

	if [ "$use_dce" -eq 1 ]; then
		check_dce
		if [ "${rc_check_dce}" -ne 0 ]
		then
			debug_message "Method: `basename $0` - End (Exit 100)"
			exit 100
		fi
	fi

	check_SAA
fi

if [ "${rc_check_SAA}" -ne 0 ]
then
	debug_message "Method: `basename $0` - End (Exit 100)"
	exit 100
fi

debug_message "Method: `basename $0` - End (Exit 0)"
exit 0
