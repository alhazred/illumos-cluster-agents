#!/usr/bin/ksh 
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)start_SAA.ksh	1.9	09/01/26 SMI"

PERFORM_REAL_MONITORING=0

typeset opt

while getopts 'R:G:P' opt
do
        case "$opt" in
                R)      RESOURCE=$OPTARG;;
                G)      RESOURCEGROUP=$OPTARG;;
		P)	PERFORM_REAL_MONITORING=1;;
                *)      exit 1;;
        esac
done

. `dirname $0`/../etc/config
. `dirname $0`/functions

validate_options

debug_message "Method: `basename $0` - Begin"
$SET_DEBUG

rm $LOGFILE 2>/dev/null

validate

if [ "$rc_validate" -ne 0 ]
then
        debug_message "Method: `basename $0` - End (Exit 1)"
        exit 1
fi

# make sure there is a pid left for pmf until start_SAA does finish
START_TIMEOUT=`${SCHA_RESOURCE_GET} -O START_TIMEOUT -R ${RESOURCE} -G ${RESOURCEGROUP}`
/usr/bin/sleep ${START_TIMEOUT} &

# disable pmf tag
if ${PMFADM} -q ${RESOURCEGROUP},${RESOURCE},0.svc
then
	${PMFADM} -s ${RESOURCEGROUP},${RESOURCE},0.svc
fi

get_SAA_version

if [ "${saa_version}" = "6.2" ]; then
	start_DB_SAA
else
	if [ -z "$use_dce" ]; then
		log_message err "Error: use_dce not set - problem in agent"
		exit 1
	fi

	if [ "$use_dce" -eq 1 ];then
		debug_message "using dce rpc for version < 5.9"
		start_dce

		if [ "$rc_start_dce" -ne 0 ]; then
			log_message err "start_dce rc<$rc_start_dce>"
			debug_message "Method: `basename $0` - End (Exit $rc_start_dce)"
			exit $rc_start_dce
		fi

		# first step in starting is stopping swa_rpcd if not alredy stopped
		stop_swa_rpcd
	
		if [ "$rc_stop_swa_rpcd" -ne 0 ]; then
			log_message err "could not kill swa_rpcd"
			debug_message "Method: `basename $0` - End (Exit 1)"
			exit 1
		fi	

		check_hostrename
		if [ "$rc_check_hostrename" -ne 0 ]; then
			log_message err "error in configuration of SAA"
			debug_message "Method: `basename $0` - End (Exit 1)"
			exit 1
		fi

		start_swa_rpcd
		if [ "$rc_start_swa_rpcd" -ne 0 ]; then
			log_message err "could not start swa_rpcd, aborting"
			debug_message "Method: `basename $0` - End (Exit 1)"
			exit 1
		fi
	else
		stop_swa_boot
		if [ "$rc_stop_swa_boot" -ne 0 ]; then
			log_message err "could not kill swa_boot"
			debug_message "Method: `basename $0` - End (Exit 1)"
			exit 1
		fi

		if [ -x /usr/swa/apply_alliance_ports ]; then
			/usr/swa/apply_alliance_ports >> ${LOGFILE} 2>&1
		fi

		check_hostrename
		if [ "$rc_check_hostrename" -ne 0 ]; then
			log_message err "error in configuration of SAA"
			debug_message "Method: `basename $0` - End (Exit 1)"
			exit 1
		fi

		start_swa_boot
		if [ "$rc_start_swa_boot" -ne 0 ]
		then
			log_message err "swa_boot returned: $rc_start_swa_boot"
			debug_message "Method: `basename $0` - End (Exit 1)"
			exit 1
		fi
	fi

	start_SAA
fi

if [ "$rc_start_command" -eq 0 ]
then
        log_message notice "SAA start result code : $rc_start_command"

        debug_message "Method: `basename $0` - End (Exit 0)"
        exit 0
else
        log_message err "start_command rc<$rc_start_command>"
	debug_message "Method: `basename $0` - End (Exit $rc_start_command)"
	exit $rc_start_command
fi
