#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

# ident   "@(#)pgs_config.ksh 1.6     08/05/06 SMI"
# 
# This file will be sourced in by pgs_register and the parameters
# listed below will be used.
#
# These parameters can be customized in (key=value) form
#
#                RS - name of the resource for the application.
#                RG - name of the resource group containing RS.
#              PORT - name of the port number.
#                LH - name of the LogicalHostname SC resource.
#                     Do not set the LH variable if you plan to have a network 
#		      unaware installation.
#            HAS_RS - Name of the HAStoragePlus SC resource.
#             PFILE - Parameter file which contains the PostgreSQL specific 
#                     parameters, this file will be created by the register script.
#
# The following variables need to be set only if the agent runs in a 
# failover zone
#
#              ZONE - Zonename where the zsmf component should be registered
#           ZONE_BT - Resource name of the zone boot component
#           PROJECT - A project in the zone, that will be used for the PostgreSQL
#                     smf service.
#           PROJECT - A project in the zone, that will be used for the PostgreSQL
#                     smf service. 
#                     If the variable is not set it will be translated as :default for  
#	              the smf credentialss.
#                     Optional
#

RS=
RG=
PORT=
LH=
HAS_RS=
PFILE=

# failover zone specific options

ZONE=
ZONE_BT=
PROJECT=

# 
# Content for the parameter file
#
#            USER - The Solaris user who owns the PostgreSQL database.
#          PGROOT - Contains the path to the PostgreSQL directory. Below this
#                   directory the postgres binaries are located in the ./bin
#                   directory.
#          PGDATA - Contains the path to the databases of this specific PostgreSQL
#                   instance.
#          PGPORT - Port where the postmaster process will be listening to.
#          PGHOST - Hostname where the postmaster process is listening to, or a directory where the
#                   Unix socket file is stored.
#                   If set to a valid hostname, the PGHOST variable forces the probe to
#                   traverse the TCP/IP stack. If the PGHOST variable is empty or starts with a "/",
#                   the probe will use a socket. If the PGHOST variable starts with a "/", the entry must
#                   be the directory which contains the socket file.
#       PGLOGFILE - Logfile where the log messages of the postmaster will be stored.
# LD_LIBRARY_PATH - This path contains all the necessary libraries for this PostgreSQL
#                   installation.
#                   Optional
#       ENVSCRIPT - Script to contain PostgreSQL specific runtime variables.
#                   Optional
#            SCDB - This database will be monitored.
#          SCUSER - PostgresSQL user to connect to the $SCDB database.
#         SCTABLE - Table name in the $SCDB database. This table name will be manipulated 
#                   to check if PostgreSQL is alive. This table will be generated at database
#                   preparation time.
#          SCPASS - Password of the SCUSER
#                   Optional
#        NOCONRET - Return code for connection errors. This return code has to follow the rules
#                   for the generic data service. The value has to be between 1 and 100.
#                   100/NOCONRET defines the number of consecutive probes to ignore for failed 
#                   connections. A restart or failover will occur, if the number is exeeded within
#                   the retry interval.

USER=
PGROOT=
PGDATA=
PGPORT=
PGHOST=
PGLOGFILE=
LD_LIBRARY_PATH=
ENVSCRIPT=
SCDB=
SCUSER=
SCTABLE=
SCPASS=
NOCONRET=10

# The following parameters need to be configured only if logfile shipping is configured to ship
# the PosgreSQL WAL logs between a designated primary and a designated standby resource.
# They need to be configured only on the primary.

#        STDBY_RS  The resource name of the PostgreSQL standby resource.
#        STDBY_RG  The resource group name of the PostgreSQL standby resource group.
#      STDBY_USER  User which is the owner of the standby postgres database.
#      STDBY_HOST  Resolvable name of the standby host or the standby zone,
#                  this name has to be reachable via ssh.
#   STDBY_PARFILE  The standbys postgres parameter file to get the rest of the necessary parameters.
#   STDBY_PING     The number of of packets the primary uses to ping the standby host. If this variable is
#                  empty, it will be set to 5 packets.
#   ROLECHG_RS     The rolechangers resource name. 
#  SSH_PASSDIR     A directory where the ssh passphrase is stored in a the file <resourcename>-sshpass.
#                  This parameter is needed only if you configured WAL file shipping and secured your
#                  ssh key with a passphrase.
#                  Leave it undefined if the passprase is empty.
#    
# If you configure the logfile shipping in a shared nothing topology, do not set the LH parameter.
#
# Configure the following paramters on the primary host.

STDBY_RS=
STDBY_RG=
STDBY_USER=
STDBY_HOST=
STDBY_PARFILE=
STDBY_PING=
#
# Configure the following paramters on the standby host.
#
ROLECHG_RS=
#
# Configure the following parameter on both hosts.
#
SSH_PASSDIR=

