#!/usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#ident	"@(#)pgs_register.ksh	1.9	09/03/05 SMI"

#
#  This script takes 1 options.
#  -f filename states a config file different from pgs_config.
#     This file will be sourced instead of pgs_config if -f filename is specified
#


# Set generic variables:

BINDIR=/opt/SUNWscPostgreSQL/bin
UTILDIR=/opt/SUNWscPostgreSQL/util
SMFUTILDIR=/opt/SUNWsczone/sczsmf/util
DIRNAME=/usr/bin/dirname
ECHO=/usr/bin/echo
STTY=/usr/bin/stty

MYNAME=`basename ${0}`
MYDIR=`dirname ${0}`

. ${MYDIR}/../etc/config
. ${MYDIR}/../lib/functions_static
. ${MYDIR}/../bin/functions


global_zone()
{
	# function to register a resource in the Solaris 10 global zone or on Solaris 8 or 9
	
	RESOURCEGROUP=${RG}

	if ! create_pfile
	then
	      return 1
	fi 

	# define your start, stop, probe an validate command

	start_command="${BINDIR}/control_pgs -R ${RS} -G ${RG} -P ${PFILE} start"
	stop_command="${BINDIR}/control_pgs -R ${RS} -G ${RG} -P ${PFILE} stop"
	probe_command="${BINDIR}/control_pgs -R ${RS} -G ${RG} -P ${PFILE} probe"
	validate_command="${BINDIR}/control_pgs -R ${RS} -G ${RG} -P ${PFILE} validate"

	# determine whether the resource runs in the global zone or in a zone node
	# and set the zcommand to the appropriate zlogin comand

	zcommand=
	rgs_zone=$(rgs_zonename)
	if [ -n "${rgs_zone}" ]
	then
		zcommand="${ZLOGIN} ${rgs_zone} " 
	fi
	
	# create the <resorce name>-phrase if required

	if [ -n "${SSH_PASSDIR}" ] 
	then
		if ! store_passphrase
		then
			return 1
		fi
	fi

	# register your resource

	if [ -n "${LH}" ]
	then
		/usr/cluster/bin/clresource create -d -g ${RG} -t SUNW.gds \
		-p Start_command="${start_command}" \
		-p Stop_command="${stop_command}" \
		-p Probe_command="${probe_command}" \
		-p Validate_command="${validate_command}" \
		-p Port_list=${PORT}/tcp \
		-p Stop_signal=9 -p Retry_interval=900 \
		-p Resource_dependencies=${HAS_RS},${LH} \
		${RS}
		
		St=$?
	else
		/usr/cluster/bin/clresource create -d -g ${RG} -t SUNW.gds \
		-p Start_command="${start_command}" \
		-p Stop_command="${stop_command}" \
		-p Probe_command="${probe_command}" \
		-p Validate_command="${validate_command}" \
		-p Network_aware=false \
		-p Stop_signal=9 -p Retry_interval=900 \
		-p Resource_dependencies=${HAS_RS} \
		${RS}
		
		St=$?
	fi
	
	if [ "${St}" -ne 0 ]; then
	        ${ECHO} "Registration of resource ${RS} failed, please correct the wrong parameters."
	
	        return 1
	else
	        ${ECHO} "Registration of resource ${RS} succeeded."
	fi
	
	return 0

}

local_zone_smf()
{

# function to register a smf resource

	SERVICE_TAG=svc:/application/sczone-agents:${RS}

	if [ ! -f ${SMFUTILDIR}/sczsmf_config ]
	then
		${ECHO}  ${SMFUTILDIR}/sczsmf_config does not exist. Make sure, that \
		      Sun Cluster HA for Solaris container is intalled in the global zone
		return 1
	fi

	
	# create the <resorce name>-phrase if required

	zcommand="${ZLOGIN} ${ZONE} " 
	
	# create the <resorce name>-phrase if required

	if [ -n "${SSH_PASSDIR}" ] 
	then
		if  store_passphrase
		then
			${zcommand} ${CHOWN} ${USER} ${SSH_PASSDIR}/${RS}-phrase
		else
			return 1
		fi
	fi
		

# prepare the config file

	cp ${SMFUTILDIR}/sczsmf_config ${SMFUTILDIR}/sczsmf_config.work
	cat << EOF > ${SMFUTILDIR}/sczsmf_config 

#
# Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# This file will be sourced in by sczsmf_register and the parameters
# listed below will be used.
#
# These parameters can be customized in (key=value) form
#
#               RS - Name of the resource
#               RG - Name of the resource group containing RS
#         SCZBT_RS - Name of the SC Zone boot resource
#             ZONE - Name of the Zone
#
#       For SERVICE, RECURSIVE and STATE, refer to the svcadm(1M)
#                 man page
#
#          SERVICE - {FMRI | pattern}
#               FMRI - Fault management resource identifier
#               pattern - Pattern matching a service
#
#        RECURSIVE - {false | true}     Default: true
#               False - Just enable the service and no dependents
#               True - Enable the service and recursively enable
#                 its dependents
#
#               RECURSIVE=true equates to svcadm enable "-r"
#
#            STATE - {false | true}     Default: true
#               False - Do not wait until service state is reached
#               True - Wait until service state is reached
#
#               STATE=true equates to svcadm enable/disable "-s"
#
#    SERVICE_PROBE - Script to check the SMF service
#
#               The optional parameter, SERVICE_PROBE, provides the
#                 ability to check that the SMF service is working.
#                 This must be a script within the zone and must
#                 adhere to these return codes,
#
#                 0 - The SMF service is working
#               100 - The SMF service should be restarted
#               201 - The SMF service should initiate a failover of
#                       the Resource Group
#
#               Note: That return code 201, requires that this resource
#                 has an appropriate extension property value for
#                 FAILOVER_MODE and FAILOVER_ENABLED=TRUE
#
#               For FAILOVER_MODE refer to the r_properties(5) man page.
#

RS=${RS}
RG=${RG}
SCZBT_RS=${ZONE_BT}
ZONE=${ZONE}
SERVICE=${SERVICE_TAG}
RECURSIVE=false
STATE=true
SERVICE_PROBE="${BINDIR}/control_pgs probe ${SERVICE_TAG}"
  
EOF

	# determine if a working copy of the config file was created if yes use the -f option

	REGOPT=""
	if [ -f ${UTILDIR}/pgs_config.work ]
	then
	        /usr/bin/cat ${UTILDIR}/pgs_config.work | /usr/sbin/zlogin ${ZONE} /usr/bin/cat - \>/tmp/pgs_config.work 
		REGOPT="-f /tmp/pgs_config.work"
	else
	        /usr/bin/cat ${UTILDIR}/pgs_config | /usr/sbin/zlogin ${ZONE} /usr/bin/cat - \>/tmp/pgs_config.work 
		REGOPT="-f /tmp/pgs_config.work"
	fi

        ${ECHO} clean up the manifest / smf resource
        ${UTILDIR}/pgs_smf_remove ${REGOPT} 2>/dev/null

	# register the manifest

	if /usr/sbin/zlogin ${ZONE} ${UTILDIR}/pgs_smf_register ${REGOPT}
	then
		${ECHO} "Manifest svc:/application/sczone-agents:${RS} was created in zone ${ZONE}"
		${ECHO} "Registering the zone smf resource"
		ksh ${SMFUTILDIR}/sczsmf_register
		ret_code=${?}
		if [ ${ret_code} -eq 0 ]
		then
			${ECHO} "set the Retry_interval to 900"
			/usr/cluster/bin/clresource set -p Retry_interval=900 ${RS} 
		fi
                mv ${SMFUTILDIR}/sczsmf_config.work ${SMFUTILDIR}/sczsmf_config
	else
		${ECHO} "The registration of the manifest did not complete, fix the errors and retry"
		ret_code=0
	fi

	/usr/sbin/zlogin ${ZONE} /usr/bin/rm /tmp/pgs_config.work
	
	return ${ret_code}

}

store_passphrase()
{
	
	# create the <resorce name>-phrase 

	${zcommand} ${TEST} ${SSH_PASSDIR}	
	if [ ${?} -ne 0 ]
	then
		${ECHO} "The directory ${SSH_PASSDIR} which is necessary to store the passphrase file does not exist"
		return 1
		
	fi
	${zcommand} ${RM} ${SSH_PASSDIR}/${RS}-phrase 2>/dev/null
	${zcommand} ${TOUCH} ${SSH_PASSDIR}/${RS}-phrase
	${zcommand} ${CHMOD} 600 ${SSH_PASSDIR}/${RS}-phrase
	if [ -n "${zcommand}" ]
	then
		${zcommand} ${ECHO} ${passphrase} \>\>${SSH_PASSDIR}/${RS}-phrase
	else
		${ECHO} ${passphrase} >>${SSH_PASSDIR}/${RS}-phrase
	fi
	${zcommand} ${CHMOD} 400 ${SSH_PASSDIR}/${RS}-phrase
	return 0
		
}

MYCONFIG=
ZONETYPE=global

typeset opt

while getopts 'f:' opt
do
        case "${opt}" in
                f)      MYCONFIG=${OPTARG};;
                *)      ${ECHO} "ERROR: ${MYNAME} Option ${OPTARG} unknown - early End. Only -f is valid"
                        exit 1;;
        esac
done

# Sourcing the specified config file, either the default one,
# or the one supplied with -f

if [ -n "${MYCONFIG}" ] && [ -f "${MYCONFIG}" ]
then
	${ECHO} "sourcing ${MYCONFIG} and create a working copy under ${UTILDIR}/pgs_config.work"
	cp ${MYCONFIG} ${UTILDIR}/pgs_config.work
	. ${MYCONFIG}
else
	PKGCONF=`dirname $0`/pgs_config
	${ECHO} "sourcing ${PKGCONF}"
	. ${PKGCONF}
fi

# getting the passphrase if required

if [ -n "${SSH_PASSDIR}" ]
then

	${ECHO} "Enter the passphrase of your PostgreSQL user."  
	${STTY} -echo
	read passphrase
	${STTY} echo
	if [ -z "${passphrase}" ]
	then
		${ECHO} "The passphrase is missing, rerun the pgs_register script"
		exit 1
	fi
	${ECHO} "Verify your passphrase"
	${STTY} -echo
	read passphrase2
	${STTY} echo
	if [ "${passphrase}" != "${passphrase2}" ]
	then
		${ECHO} "The passphrase verification does not match, rerun the pgs_register script"
		exit 1
	fi
	
fi

# Registering the resource

if [ -n "${ZONE}" ]
then
	local_zone_smf
else
	global_zone
fi

if [ -f ${UTILDIR}/pgs_config.work ]
then
        ${ECHO} " remove the working copy ${UTILDIR}/pgs_config.work"
	rm ${UTILDIR}/pgs_config.work
fi

exit 0
