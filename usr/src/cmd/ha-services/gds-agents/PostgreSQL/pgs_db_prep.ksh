#!/usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#ident	"@(#)pgs_db_prep.ksh	1.7	07/06/06 SMI"
#
# This utility prepares the PostreSQL database to be monitored by the Sun Cluster HA for PostgreSQL agent.
# If the database does not exist, it will be created.
#
#
#  This script takes 1 option.
#  -f filename states a config file different from pgs_config.
#     This file will be sourced instead of pgs_config if -f filename is specified

# Set generic variables:

BINDIR=/opt/SUNWscPostgreSQL/bin
UTILDIR=/opt/SUNWscPostgreSQL/util
EGREP=/usr/bin/grep
ECHO=/usr/bin/echo

MYCONFIG=

typeset opt

while getopts 'f:' opt
do
        case "${opt}" in
                f)      MYCONFIG=${OPTARG};;
                *)      ${ECHO} "ERROR: ${MYNAME} Option ${OPTARG} unknown - early End. Only -f is valid"
                        exit 1;;
        esac
done

# Sourcing the specified config file, either the default one,
# or the one supplied with -f

if [ -n "${MYCONFIG}" ] && [ -f "${MYCONFIG}" ]
then
	${ECHO} "sourcing ${MYCONFIG} "
	. ${MYCONFIG}
else
	PKGCONF=`dirname $0`/pgs_config
	${ECHO} "sourcing ${PKGCONF}"
	. ${PKGCONF}
fi

if [ -z "${LD_LIBRARY_PATH}" ]
then
	unset LD_LIBRARY_PATH
fi



# validate the pgroot variable

if [ -z "${PGROOT}" ]
then
	${ECHO} " ERROR: The PGROOT directory variable is not set"
	exit 1
fi

if [ ! -d ${PGROOT} ]
then
	${ECHO} "ERROR:  The PGROOT directory ${PGROOT} does not exist or is not a directory"
	exit 1
fi

if [ ! -f ${PGROOT}/bin/psql ]
then
	${ECHO} "ERROR:  The PGROOT directory ${PGROOT} does not contain the psql command"
	exit 1
fi

# validate by connecting to the database

${PGROOT}/bin/psql -p ${PGPORT} -l >/dev/null 2>&1
if  [ $? -ne 0 ]
then
	${ECHO} "ERROR: A connect to PostgreSQL is impossible, check 2 conditions: "
	${ECHO} "ERROR: 1. is the postmaster running?"
	${ECHO} "ERROR: 2. does it listen to port ${PGPORT}?"
	exit 1
fi

# Validate, that SCDB SCUSER SCTABLE are set

if [ -z "${SCDB}" ] -o [ -z "${SCUSER}" ] -o [ -z "${SCTABLE}" ]
then
	${ECHO} "ERROR: At least one of the variables SCDB=${SCDB}, SCUSER=${SCUSER} or SCTABLE=${SCTABLE} is not set"
	exit 1
fi

if ! ${PGROOT}/bin/psql -p ${PGPORT} -l |${EGREP} " ${SCDB} " >/dev/null 2>&1
then
        export PGPORT PGDATA
	${ECHO} "create the database ${SCDB}"
	${PGROOT}/bin/createdb ${SCDB}
fi

# Check if the user does exist

if ${PGROOT}/bin/psql -p ${PGPORT} -d ${SCDB} -c "select usename from pg_user"|${EGREP} " ${SCUSER}$| ${SCUSER} " >/dev/null 2>&1
then
	echo "Warning the user ${SCUSER} exists"
else
	# create the user 

	${PGROOT}/bin/psql -p ${PGPORT} -d ${SCDB} -c "create user ${SCUSER}"

fi

if [ -n "${SCPASS}" ]
then
	${ECHO} "set the password ${SCPASS} for the user ${SCUSER}"
	PGPASSWORD=${SCPASS}
	${PGROOT}/bin/psql -p ${PGPORT} -d ${SCDB} -c "alter user ${SCUSER} with password '${SCPASS}'"
fi

# create the table

${ECHO} "create the table ${SCTABLE}"
${PGROOT}/bin/psql -p ${PGPORT} -d ${SCDB} -U ${SCUSER} -c "create table ${SCTABLE} (sccol varchar(30))"

exit 0
