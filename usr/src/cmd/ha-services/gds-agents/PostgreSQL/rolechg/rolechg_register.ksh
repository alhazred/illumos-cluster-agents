#!/usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

# ident   "@(#)rolechg_register.ksh 1.3     09/03/05 SMI"

#
#  This script takes 1 options.
#  -f filename states a config file different from rolechg_config.
#     This file will be sourced instead of rolechg_config if -f filename is specified
#


# Set generic variables:

BINDIR=/opt/SUNWscPostgreSQL/rolechg/bin
UTILDIR=/opt/SUNWscPostgreSQL/rolechg/util
SMFUTILDIR=/opt/SUNWsczone/sczsmf/rolechg/util
DIRNAME=/usr/bin/dirname
ECHO=/usr/bin/${ECHO}

MYNAME=`basename ${0}`
MYDIR=`dirname ${0}`

. ${MYDIR}/../etc/config
. ${MYDIR}/../../lib/functions_static
. ${MYDIR}/../bin/functions


global_zone()
{
	# function to register a resource in the Solaris 10 global zone or on Solaris 9
	
	# define your start, stop, probe an validate command

	start_command="${BINDIR}/control_rolechg -R ${RS} -G ${RG} -H ${STDBY_HOST} -P ${STDBY_PFILE} -T ${TRIGGER} -W ${WAIT} start"
	stop_command="${BINDIR}/control_rolechg -R ${RS} -G ${RG} -H ${STDBY_HOST} -P ${STDBY_PFILE} -T ${TRIGGER} -W ${WAIT} stop"
	probe_command="${BINDIR}/control_rolechg -R ${RS} -G ${RG} -H ${STDBY_HOST} -P ${STDBY_PFILE} -T ${TRIGGER} -W ${WAIT} probe"
	validate_command="${BINDIR}/control_rolechg -R ${RS} -G ${RG} -H ${STDBY_HOST} -P ${STDBY_PFILE} -T ${TRIGGER} -W ${WAIT} validate"

	# set the LOCAL_NODE dependencies to the standby database and if defined to the primary 
	# database resources.
        
	DEP=${STDBY_RS}{ANY_NODE}
	
	if [ -n "${PRI_RS}" ]
	then
		DEP=${DEP},${PRI_RS}{ANY_NODE}
	fi
	
	# register your resource


	if [ -n "${LH}" ]
	then
		/usr/cluster/bin/clresource create -d -g ${RG} -t SUNW.gds \
		-p Start_command="${start_command}" \
		-p Stop_command="${stop_command}" \
		-p Probe_command="${probe_command}" \
		-p Validate_command="${validate_command}" \
		-p Port_list=${PORT}/tcp \
		-p Stop_signal=9 -p Retry_interval=900 \
		-p Resource_dependencies=${HAS_RS},${LH} \
		-p Resource_dependencies_weak=${DEP} \
		${RS}
		
		St=$?
	else
		/usr/cluster/bin/clresource create -d -g ${RG} -t SUNW.gds \
		-p Start_command="${start_command}" \
		-p Stop_command="${stop_command}" \
		-p Probe_command="${probe_command}" \
		-p Validate_command="${validate_command}" \
		-p Network_aware=false \
		-p Stop_signal=9 -p Retry_interval=900 \
		-p Resource_dependencies=${HAS_RS} \
		-p Resource_dependencies_weak=${DEP} \
		${RS}
		
		St=$?
	fi
	
	if [ "${St}" -ne 0 ]; then
	        ${ECHO} "Registration of resource ${RS} failed, please correct the wrong parameters."
	
	        return 1
	else
	        ${ECHO} "Registration of resource ${RS} succeeded."
	fi
	
	return 0

}

MYCONFIG=
ZONETYPE=global

typeset opt

while getopts 'f:' opt
do
        case "${opt}" in
                f)      MYCONFIG=${OPTARG};;
                *)      ${ECHO} "ERROR: ${MYNAME} Option ${OPTARG} unknown - early End. Only -f is valid"
                        exit 1;;
        esac
done

# Sourcing the specified config file, either the default one,
# or the one supplied with -f

if [ -n "${MYCONFIG}" ] && [ -f "${MYCONFIG}" ]
then
	${ECHO} "sourcing ${MYCONFIG} and create a working copy under ${UTILDIR}/rolechg_config.work"
	/usr/bin/cp ${MYCONFIG} ${UTILDIR}/rolechg_config.work
	. ${MYCONFIG}
else
	PKGCONF=`dirname $0`/rolechg_config
	${ECHO} "sourcing ${PKGCONF}"
	. ${PKGCONF}
fi

# Registering the resource

global_zone


if [ -f ${UTILDIR}/rolechg_config.work ]
then
        ${ECHO} " remove the working copy ${UTILDIR}/rolechg_config.work"
	${RM} ${UTILDIR}/rolechg_config.work
fi

exit 0
