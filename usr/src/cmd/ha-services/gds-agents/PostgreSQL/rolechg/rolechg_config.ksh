#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

# ident   "@(#)rolechg_config.ksh 1.2     08/05/06 SMI"
# 
# This file will be sourced in by rolechg_register and the parameters
# listed below will be used.
#
# These parameters can be customized in (key=value) form
#
#                RS - name of the resource for the application.
#                RG - name of the resource group containing RS.
#              PORT - name of the port number.
#                LH - name of the LogicalHostname SC resource.
#                     Do not set the LH variable if you plan to have a network 
#		      unaware installation.
#            HAS_RS - Name of the HAStoragePlus SC resource.
#          STDBY_RS - The resource name of designated standby database
#            PRI_RS - The resource name of designated primary database
#        STDBY_HOST - Hostname  or zonename of the standby host. If empty, a role switch
#                     will be initiated on any host. 
#        SDBY_PFILE - Parameter file which contains the PostgreSQL specific 
#                     parameters for the standby database. This file is mentioned in 
#                     the Start_command of the PostgreSQL standby resource.
#           TRIGGER - The filename which will get created to tell pg_standby to end the 
#                     recovery mode, this filename is mentioned in the recovery.conf file 
#                     of the PostgreSQL standby database.
#              WAIT - A number of seconds the start method waits before it touches
#                     the trigger file. this little break is necessary because, if the 
#                     trigger file should be touched before, or in the middle of the 
#                     PostgreSQL start process, it would get removed automatically.
#

RS=
RG=
PORT=
LH=
HAS_RS=
STDBY_RS=
PRI_RS=
STDBY_HOST=
STDBY_PFILE=
TRIGGER=
WAIT=
