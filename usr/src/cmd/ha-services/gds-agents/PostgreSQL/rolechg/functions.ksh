#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

# ident   "@(#)functions.ksh 1.3     09/01/26 SMI"

PKG=SUNWscPostgreSQL
METHOD=`basename $0`
TASK_COMMAND=""

ZONENAME=/usr/bin/zonename
DIRNAME=/usr/bin/dirname

SCLOGGER=/usr/cluster/lib/sc/scds_syslog
PMFADM=/usr/cluster/bin/pmfadm
GETENT=/usr/bin/getent
UNAME=/usr/bin/uname
MCONNECT=/usr/bin/mconnect
SLEEP=/usr/bin/sleep
TOUCH=/usr/bin/touch
CAT=/usr/bin/cat
GREP=/usr/bin/grep
ENV=/usr/bin/env
PGREP=/usr/bin/pgrep
ECHO=/usr/bin/echo
SU=/usr/bin/su
RM=/usr/bin/rm
AWK=/usr/bin/awk
NAWK=/usr/bin/nawk

set_shell_specifics()
{

	# Set variables to construct sourcing and output redirection depending 
	# on the login shell of the user

        debug_message "Function: set_shell_specifics - Begin"
        ${SET_DEBUG}

	ENVSC=

        if ${GETENT} passwd ${USER} | ${AWK} -F: '{print $7}' | ${GREP} csh > /dev/null
        then

		# C shell specifics

		# sourcing

		if [ -n "${ENVSCRIPT}" ]
		then
			ENVSC="source ${ENVSCRIPT};"
		fi

		# brackets for subshells

		OPEN_BRACKET="("
		CLOSE_BRACKET=")"

		# redirection

		OUTPUT_APP=">>& ${LOGFILE}"
		OUTPUT=">& ${LOGFILE}"
		CAT_OUTPUT="> /tmp/${RESOURCE}-${USER}-cat-out"
		CAT_ERRPUT=">& /tmp/${RESOURCE}-${USER}-cat-err"
        else

		# Korn shell specifics

		# sourcing

		if [ -n "${ENVSCRIPT}" ]
		then
			ENVSC=". ${ENVSCRIPT};"
		fi

		# no subshell needed in a ksh 

		OPEN_BRACKET=
		CLOSE_BRACKET=

		# redirection

	        OUTPUT_APP=">> ${LOGFILE} 2>&1"
	        OUTPUT="> ${LOGFILE} 2>&1"
		CAT_OUTPUT="> /tmp/${RESOURCE}-${USER}-cat-out"
		CAT_ERRPUT="2> /tmp/${RESOURCE}-${USER}-cat-err"
        fi


        debug_message "Function: set_shell_specifics - End"
}

validate_options()
{
        debug_message "Function: validate_options - Begin"
	${SET_DEBUG}

        #
        # Ensure all mandatory options are set
        #

        for i in RESOURCE RESOURCEGROUP STB_PARFILE TRIGGER WAIT
        do
                case ${i} in
                        RESOURCE)
                        if [ -z "${RESOURCE}" ]; then
                                scds_syslog -p daemon.err -t $(syslog_tag) -m \
                                	"Function: validate_options: %s Option %s not set" \
                                	"${METHOD}" "-R"
                                return 1
                        fi;;

                        RESOURCEGROUP)
                        if [ -z "${RESOURCEGROUP}" ]; then
                                scds_syslog -p daemon.err -t $(syslog_tag) -m \
                                	"Function: validate_options: %s Option %s not set" \
                                	"${METHOD}" "-G"
                                return 1
                        fi;;

                        STB_PARFILE)
                        if [ -z "${STB_PARFILE}" ]; then
                                scds_syslog -p daemon.err -t $(syslog_tag) -m \
                                	"Function: validate_options: %s Option %s not set" \
                                	"${METHOD}" "-P"
                                return 1
                        fi;;

                        TRIGGER)
                        if [ -z "${TRIGGER}" ]; then
                                scds_syslog -p daemon.err -t $(syslog_tag) -m \
                                	"Function: validate_options: %s Option %s not set" \
                                	"${METHOD}" "-T"
                                return 1
                        fi;;

                        WAIT)
                        if [ -z "${WAIT}" ]; then
                                scds_syslog -p daemon.err -t $(syslog_tag) -m \
                                	"Function: validate_options: %s Option %s not set" \
                                	"${METHOD}" "-W"
                                return 1
                        fi;;
                esac
        done

	debug_message "Function: validate_options - End"
}

validate()
{
	#
	# Validate
	#
	
        debug_message "Function: validate - Begin"
	${SET_DEBUG}

	rc_validate=0

	# Validating the Standby Host
	
	if [ -n "${STB_HOST}" ]
	then
		if ! ${GETENT} hosts ${STB_HOST} >/dev/null 2>&1
		then
						
			scds_syslog -p daemon.err -t $(syslog_tag) -m \
				"Function: validate - The standby host %s is not resolvable" \
				"${STB_HOST}"
			rc_validate=1
						
		else
			debug_message "Function: validate - STB_HOST resolvable"	
		fi
		
	else
		debug_message "Function: validate - STB_HOST OK"	
	fi

	# Validating The WAIT parameter

	if ! let x=${WAIT} >/dev/null 2>&1
	then
					
	   	# SCMSGS
	 	# @explanation
		# The value specified in the WAIT variable
		# is not numeric
 		# @user_action
 		# Fix the WAIT variable in the
 		# Configuration file.
 		scds_syslog -p daemon.err -t $(syslog_tag) -m \
			"Function: validate: WAIT %s is not numeric" \
			"${WAIT}"
		rc_validate=1
					
	else
		debug_message "Function: validate - WAIT numeric"	
	fi
		
	
	# Validating the standby database parameter file. We are checking the existance, if it has to exist,
	# and if it is a valid parameter file. The content is validated by the standby PostgreSQL resource.
	# The validation can only be successful if the parameter file is available.
	# The precondition for the validation is that either the standby host is configured and matches 
	# the hostname, or the standby host is empty. 

	val_pfile=1
	if [ -z "${STB_HOST}" ] 
	then
		val_pfile=0
	else
		hostname=`${UNAME} -n`
		if [ "${hostname}" == "${STB_HOST}" ]
		then
			val_pfile=0
		fi
	fi
	
	if [ ${val_pfile} -eq 0 ] 
	then

		if val_parfile ${STB_PARFILE} "USER PGROOT PGDATA PGPORT PGLOGFILE SCDB SCUSER SCTABLE NOCONRET"
		then

			# Source the standbys parmeter file.

			. ${STB_PARFILE}

			# Validating the trigger file.
			# The trigger file has to be present in the pg_standby command specified in the 
			# recovery.conf file. This will be checked only if the recovery.conf file exists.
			
			if [ -f {PGDATA}/recovery.conf ]
			then

				if ! ${GREP} ${TRIGGER} ${PGDATA}/recovery.conf >/dev/null 2>&1 
				then
					# SCMSGS
					# @explanation
					# The trigger file specified in the configuration file
					# is not mentioned in the recovery.conf.
					# @user_action
					# Fix the configuration file or the recovery.conf.
					scds_syslog -p daemon.err -t $(syslog_tag) -m \
	                                	"Function: validate: The trigger file %s not set in %s" \
						"${TRIGGER}" "${PGDATA}/recovery.conf"
					rc_validate=1
	
				fi
			fi
		else

			rc_validate=1
		
		fi
	fi

	debug_message "Function: validate - End"
	return ${rc_validate}
}

val_parfile()
{
	# Common valdation for parameter file
	#
	# If the parameter is in $2, there will be an intensive validation.
	# If $2 is empty, the validation will just check for the existance of the file.
	
        debug_message "Function: val_parfile - Begin"
	${SET_DEBUG}

	rc_val_parfile=0

        # Validate that parameter file exists

	PARFILE=${1}
        PARLIST=${2}

        if [ ! -f "${PARFILE}" ]; then
                scds_syslog -p daemon.err -t $(syslog_tag) -m \
                  "Function: val_parfile - File %s does not exist" \
                  "${PARFILE}"
                rc_val_parfile=1
	else
		debug_message "Function: val_parfile - ${PARFILE} exists"	
 	fi

	# Test the semantics only, if the parameter list is specified.
	# This should not be done when called from validate_probe.

	if [ -n "${2}" ]
	then
	
		# Test if the parameter file is a valid ksh script
	
		if ! ksh -n ${PARFILE} >/dev/null 2>&1
		then
	        	scds_syslog -p daemon.err -t $(syslog_tag) -m \
	                  "Function: validate - Syntax errors in %s" \
	               	  "${PARFILE}"
	               rc_val_parfile=1
		else
			debug_message "Function: val_parfile - validated ${PARFILE}"	
	       	fi
	
		# Test if all the mandatory variables are included and set correctly in the parameter file
	
		PARAMETERS=`${CAT} ${PARFILE} |${GREP} -v "^#"|${GREP} -v "^ "|${NAWK} -F= '{print $1}'`
	
		for i in ${PARLIST}
		do
			if ! `${ECHO} ${PARAMETERS} |${GREP} ${i} > /dev/null `; then
	                	scds_syslog -p daemon.err -t $(syslog_tag) -m \
	                          "Function: val_parfile - %s not specified in %s, which is required" \
	                          "${i}" "${PARFILE}"
				rc_val_parfile=1
			else
				debug_message "Function: val_parfile - ${i} included in ${PARFILE}"	
	                fi
		done
	fi

	debug_message "Function: val_parfile - End"
	return ${rc_val_parfile}
}

start_rolechg()
{
	#
	# Start PostgreSQL
	#

        debug_message "Function: start_rolechg - Begin"
	${SET_DEBUG}

	rc_start_command=0	

	# disable the pmf tag and run a sleep in the background, to make sure, that there will be a valid 
	# pmftag during the start phase.

	START_TIMEOUT=`${SCHA_RESOURCE_GET} -O START_TIMEOUT -R ${RESOURCE} -G ${RESOURCEGROUP}`
	${SLEEP} ${START_TIMEOUT} &

	${PMFADM} -s ${RESOURCEGROUP},${RESOURCE},0.svc

	# remove output files to satisfy noclobber
	
	${RM} /tmp/${RESOURCE}-${USER}-cat-out >/dev/null 2>&1
	${RM} /tmp/${RESOURCE}-${USER}-cat-err >/dev/null 2>&1

	hostname=`${UNAME} -n`
	if [ "${hostname}" == "${STB_HOST}" ]
	then

		# check if postgres is reacting on PGPORT. 
	
		${ECHO} |${MCONNECT} -p ${PGPORT} ${PGHOST} >/dev/null 2>&1
	
		if [ ${?} -eq 0 ]
		then
	
			# If Postgres is listening on PGPORT, check if Postgres is acting as a primary.
	
			set_shell_specifics
			
			srm_function ${USER}
	
			# Set the necessary environment variables.
		        LIBPATH=
			if [ -n "${LD_LIBRARY_PATH}" ]
			then
				LIBPATH="${ENV} LD_LIBRARY_PATH=${LD_LIBRARY_PATH}"
			fi
			
			# The variable PASSWORD is set to the database user login password.
			
			PASSWORD=
			if [ -n "${SCPASS}" ]
			then
				PASSWORD="${ENV} PGPASSWORD=${SCPASS}"
			fi
			PORT="PGPORT=${PGPORT}"
			DATA="PGDATA=${PGDATA}"
			
			if [ -n "${PGHOST}" ]
			then
				HOST="${ENV} PGHOST=${PGHOST}"
			fi
	
			#  First check if Postgres accepts accepts inserts
	 
			DBINSROLE="insert into ${SCTABLE} (sccol) values('rolechangers standby test');" 
			${SU} ${USER} -c "${OPEN_BRACKET} ${TASK_COMMAND} ${LIBPATH} $HOST ${PASSWORD} ${ENV} ${DATA} ${ENV} ${PORT} ${PGROOT}/bin/psql -d ${SCDB} -U ${SCUSER} -c \"${DBINSROLE}\" ${CAT_OUTPUT} ${CLOSE_BRACKET} ${CAT_ERRPUT}" >/dev/null
	
	
			if [ ${?} -ne 0 ]
			then
				# Only if PostgreSQL does not accept inserts, touch the trigger file
				# to initate a role change from standby to primary.
	
				# Wait on a just starting standby.
	
				${SLEEP} ${WAIT}
	
				${TOUCH} ${TRIGGER}
	
				# Wait until PostgreSQL changed its role to primary.
	
				while [ -f ${PGDATA}/recovery.conf ]
				do
					${SLEEP} 1
				done
				
			fi
		fi
	fi
	debug_message "Function: start_rolechg - End"
	return ${rc_start_command}
}

stop_rolechg()
{
	#
	# Stop the role changer
	#

        debug_message "Function: stop_rolechg - Begin"
	${SET_DEBUG}

	# Just eliminate a probable pending sleep which may be executed by the start

	# Send a kill to any pids under the PMFtag
	${PMFADM} -s ${RESOURCEGROUP},${RESOURCE},0.svc KILL 2> /dev/null

	debug_message "Function: stop_rolechg - End"
	return ${rc_stop_command}
}
