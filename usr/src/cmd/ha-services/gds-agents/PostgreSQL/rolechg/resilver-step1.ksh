#!/usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

# ident   "@(#)resilver-step1.ksh 1.2     08/05/06 SMI"

# Script to create a base backup of a Postgres database and sync it to an other host.
# The normal usage is when a designated standby is running as a primary and the dba
# wants to resilver the designated primary with the data on the designated standby.

# The resilver-step1 script has to be called as the postgres user and it has some 
# assumptions about the PostgreSQL configuration:
#
# 1. 	The file postgresql.conf is linked to an other directory than PGDATA
# 	example: postgresql.conf -> ../conf/postgresql.conf. 
# 2.	The file recovery.conf/recovery.done is linked to an other directoy than PGDATA
#       example: recovery.conf -> ../conf/recovery.conf.
# 3.    Every other configration file in PGDATA which has to differ between
#	the designated primary and the designate standby is linked to an other directory
#       than PGDATA.
# 4.	The Postgres users on the primary and on the standby are identical and trust
#       each other on a ssh login without password request.
# 5.	It assumes that each PostgreSQL installation is configured with an appropriate 
# 	archive command and with an appropriate recovery.conf/done file.
#
# Example for the designated primary:
# archive command in postgresql.conf:
# archive_command = '/usr/local/bin/rsync -arv %p standby:/pgs/82_walarchives/%f </dev/null'
# contents of recovery.conf/done:
# restore_command = 'cp /pgs/82_walarchives/%f %p'
#
# Example for the designated standby:
#
# archive command postgresql.conf:
# archive_command = '/usr/local/bin/rsync -arv %p primary:/pgs/82_walarchives/%f </dev/null'
# contents of recovery.conf/done:
# restore_command = '/pgs/postgres-8.2.5/bin/pg_standby -k 10 -t /pgs/data/failover /pgs/82_walarchives %f %p'
# 

# You have to customize the following variables:

# SOURCE_DATA Is the PGDATA directry of the current node, for normal use it would be the 
#             one on the designated standby node.
# Example:    SOURCE_DATA=/pgs/data
#
# TARGET_DATA Is the PGDATA of the target node, for normal use it would be the
#             one on the designated primary node.
# Example:    TARGET_DATA=/pgs/data
#
# TARGET      Is the name of the targe tnode. for normal usage it would be the name of the 
#             designated primary node.
# Example:    TARGET=primary-host
#
# PGS_BASE    Is the Postgres Base directory, where the Postgres binaries are located
# Example:    PGS_BASE=/pgs/postgres-8.2.5 for a custom build PostgreSQL
# Example:    PGS_BASE=/usr/postgres/8.2 for a PostgreSQL delivered with Solaris 10 8/07
#
# PRI_GRP     Resource group which contains the cluster resource of the designated primary.
# Example:    PRI_GRP=primary-rg
#
# STDBY_GRP   Resource group which contains the cluster resource of the designated standby.
# Example:    STDBY_GRP=standby-rg
#
# STDBY_RS    Resource name which of the designated standby resource. This name should be unique on your standby.
#             The resilver-step1 script will generate the file under /var/tmp/${STDBY_RS}-resilver which is validated
#             by resilver-step2.
# Example:    STDBY_RS=standby-rs
#
# PGPORT      The The port the database is listening to.
# Example:    PGPORT=5432
#
# ROLECHG_GRP The resource group which contains the rolechanger resource.
# Example:    ROLECHG_GRP=rolechg-rg
#
# RSYNC       Absolute path to the RSYNC command including the necessary options.
# Example:    RSYNC="/usr/local/bin/rsync -rav"
#
# SSH_PASSPHRASE       True or false wether or not your ssh key is secured by a passphrase.
# Example:    		SSH_PASSPHRASE=false

#####  Customize the following variables ##########

SOURCE_DATA=
TARGET_DATA=
TARGET=
PGS_BASE=
PRI_GRP=
STDBY_GRP=
STDBY_RS=
PGPORT=
ROLECHG_GRP=
RSYNC=
SSH_PASSPHRASE=false

#####  End of customizations  ##########

# generic variables

TR=/usr/xpg4/bin/tr 
SSH_AGENT=/usr/bin/ssh-agent
SSH_ADD=/usr/bin/ssh-add
SSH=/usr/bin/ssh
RM=/usr/bin/rm
ECHO=/usr/bin/echo
TOUCH=/usr/bin/touch

# Check if the varables are set

custom_ok=0
if [ -z "${SOURCE_DATA}" ]
then
	${ECHO} "ERROR: customize the SOURCE_DATA variable"
	custom_ok=1
fi

if [ -z "${TARGET_DATA}" ]
then
	${ECHO} "ERROR: customize the TARGET_DATA variable"
	custom_ok=1
fi

if [ -z "${TARGET}" ]
then
	${ECHO} "ERROR: customize the TARGET variable"
	custom_ok=1
fi

if [ -z "${PGS_BASE}" ]
then
	${ECHO} "ERROR: customize the PGS_BASE variable"
	custom_ok=1
fi

if [ -z "${PRI_GRP}" ]
then
	${ECHO} "ERROR: customize the PRI_GRP variable"
	custom_ok=1
fi

if [ -z "${PGPORT}" ]
then
	${ECHO} "ERROR: customize the PGPORT variable"
	custom_ok=1
fi

if [ -z "${RSYNC}" ]
then
	${ECHO} "ERROR: customize the RSYNC variable"
	custom_ok=1
fi

# perform the following checks onl on clusters

if [ -d /etc/cluster ]
then
	if [ -z "${STDBY_GRP}" ]
	then
		${ECHO} "ERROR: customize the STDBY_GRP variable"
		custom_ok=1
	fi
	
	if [ -z "${STDBY_RS}" ]
	then
		${ECHO} "ERROR: customize the STDBY_RS variable"
		custom_ok=1
	fi
	
	if [ -z "${ROLECHG_GRP}" ]
	then
		${ECHO} "ERROR: customize the ROLECHG_GRP variable"
		custom_ok=1
	fi
fi

if [ ${custom_ok} -ne 0 ]
then
	exit 1
fi

# start the ssh-agent and decrypt the private key depending on the SSH_PASSPHRASE variable

SSH_PASSPHRASE=`${ECHO} ${SSH_PASSPHRASE}|${TR} "[:upper:]" "[:lower:]"`

if [ "${SSH_PASSPHRASE}" == "true" ]
then
	eval `${SSH_AGENT} -s` >/dev/null 2>&1
	${SSH_ADD}
	if [ ${?} -ne 0 ]
	then
		exit 1
	fi
fi

# Start a base backup.

${PGS_BASE}/bin/psql --port=${PGPORT} -d postgres -c "select pg_start_backup('failover');"

# Wipe out the targets pgdata.

$SSH ${TARGET} ${RM} -rf ${TARGET_DATA}/*

# Rsync the pgdata to the target.

${RSYNC} ${SOURCE_DATA}/* ${TARGET}:${TARGET_DATA}
if [ ${?} -ne 0 ]
then
	${ECHO} ERROR: Problems at the rsync, fix this first.
	${ECHO} ERROR: Only problems in the pg_xlog directory can be ignored,
	${ECHO} ERROR: it will be wiped out anyway.
fi

# Remove the targets pg_xlog.

$SSH ${TARGET} ${RM} -rf ${TARGET_DATA}/pg_xlog/*

# stop the base backup

${PGS_BASE}/bin/psql --port=${PGPORT} -d postgres -c "select pg_stop_backup();"

if [ -d /etc/cluster ]
then
	${ECHO} 
	${ECHO} "Shutdown the PostgreSQL source database resource group ${STDBY_GRP} and the role changer ${ROLECHG_GRP}"
	${ECHO} "with cluster methods and call resilver-step2."
	${ECHO} 
	${ECHO} "Example: In the global zone as root call the three commands below:"
	${ECHO} "clrg offline ${ROLECHG_GRP}"
	${ECHO} "clrg offline ${STDBY_GRP}"
	${ECHO} "clrg status"
else
	${ECHO} 
	${ECHO} "Shut down the PostgreSQL source database with PostgreSQL methods and call resilver-step2."
	${ECHO} 
	${ECHO} "Example: As postgres user on this node call the commansd below:"
	${ECHO} "${PGS_BASE}/bin/pg_ctl -D ${TARGET_DATA} stop -m fast"
fi
${ECHO} 
${ECHO} "As postgres user on the designated standby node/zone call resilver-step2."

# stop the ssh-agent depending on the SSH_PASSPHRASE variable

if [ "${SSH_PASSPHRASE}" == "true" ]
then
	${SSH_AGENT} -k >/dev/null 2>&1 
fi

# touch the /var/tmp/${STDBY_RS}-resilver file. it is required for step 2
${TOUCH} /var/tmp/${STDBY_RS}-resilver
