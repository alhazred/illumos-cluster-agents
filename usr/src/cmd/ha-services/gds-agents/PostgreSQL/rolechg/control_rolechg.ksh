#!/usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.

# ident   "@(#)control_rolechg.ksh 1.2     08/05/06 SMI"
#
# Method for the PostrgresSQL rolchanger agent. 
#
# This method is called by the GDS.
#
# It is started with options and one parameter:
#
#
# $1 start, stop or validate.
#

MYNAME=`basename ${0}`
MYDIR=`dirname ${0}`

. ${MYDIR}/../etc/config
. ${MYDIR}/../../lib/functions_static
. ${MYDIR}/functions

debug_message "Method: ${MYNAME} ${*} - Begin"
${SET_DEBUG}

# get the options for gds 

while getopts 'R:G:H:P:T:W:' opt
do
        case "${opt}" in
                R)      RESOURCE=${OPTARG};;
                G)      RESOURCEGROUP=${OPTARG};;
                H)      STB_HOST=${OPTARG};;
                P)      STB_PARFILE=${OPTARG};;
                T)      TRIGGER=${OPTARG};;
                W)      WAIT=${OPTARG};;
                *)      exit 1;;
        esac
done

# if no option is set ($OPTIND is 1 in this case) the following validation will fail.

if [ ${OPTIND} -gt 1 ]
then
	shift $((${OPTIND} - 1))
fi

# set some generic variables

LOGFILE=/var/tmp/${RESOURCE}_logfile

case ${1} in
start)

	# start the rolechanger for PostgreSQL

	# touch a "lockfile" to prevent a a remote PostgreSQL to start in primary mode 
	# after the rolchanger starts his work.

	/usr/bin/touch /tmp/${RESOURCE}_rolechg.lck
	# exit from start, if the options are wrong

	validate_options
	rc_val=${?}
	if [ ${rc_val} -ne 0 ]
	then
		terminate ${1} ${rc_val}
	fi
	
	${RM} ${LOGFILE} 2>/dev/null
	
	# check the content of the options 

	if validate
	then

		start_rolechg
		rc_val=${?}
		
		if [ ${rc_val} -eq 0 ]
		then

			# remove the rolechangers "lockfile"

			${RM} /tmp/${RESOURCE}_rolechg.lck

		        log_message notice "start_command rc<${rc_val}>"
		        debug_message "Method: ${MYNAME} - End (Exit 0)"
		else
		        log_message err "start_command rc<${rc_val}>"
		fi
	else
	        debug_message "Method: ${MYNAME} - End (Exit 1)"
		rc_val=1
	fi;;

probe)

	# Probe the rolechanger for PostgreSQL

	# This is a placeholder for future extentionto avoid reregistration.
	# It will always return 0 at the current point in time.

	rc_val=0;;

stop)

	# stop the rolechanger for PostgreSQL

	# exit from stop, if the options are wrong


	validate_options
	rc_val=${?}
	if [ ${rc_val} -ne 0 ]
	then
		terminate ${1} ${rc_val}
	fi

	${RM} ${LOGFILE} 2>/dev/null

	stop_rolechg
	rc_val=${?}
	
	if [ "${rc_val}" -eq 0 ]
	then
	        log_message notice "stop_command rc<${rc_val}>"
	else
	        log_message err "stop_command rc<${rc_val}>"
	fi
	rc_val=0;;

validate)

	# validate the parameters for application PostgreSQL

	validate_options
	rc_val=${?}
	if [ ${rc_val} -ne 0 ]
	then
		terminate ${1} ${rc_val}
	fi
	
	${RM} ${LOGFILE} 2>/dev/null
	
	if validate
	then
	        rc_val=0
	else
	        rc_val=1
	fi;;
	
esac

# terminate with the right return code, either with an smf specific or the gds/zsh based
# return code

debug_message "Method: ${MYNAME} ${*} - End terminating"
terminate ${1} ${rc_val}
