#!/usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

# ident   "@(#)resilver-step2.ksh 1.4     08/11/04 SMI"

# Script to copy the current remaining WAL logs from the standby after the resilver-step1 
# script has run.
# It assumes that PostgreSQL is configured with an appropriate archive command and 
# with a recovery.conf file.
#
# The resilver-step2 script has to be called as the postgres user and has some assuptions 
# about the PostgreSQL configuration:
#
# 1.    The file postgresql.conf is linked to an other directoy than PGDATA
#       example: postgresql.conf -> ../conf/postgresql.conf.
# 2.    The file recovery.conf/recovery.done is linked to an other directoy than PGDATA
#       example: recovery.conf -> ../conf/recovery.conf.
# 3.    Every other configration file in PGDATA which has to differ between
#       the designated primary and the designte standby is linked to an other directory
#       than PGDATA.
# 4.    The Postgres users on the primary and on the standby are identical and trust
#       each other on a ssh login without password request.
# 5.    It assumes that PostgreSQL is configured with an appropriate archive command and
#       with an appropriate recovery.conf/done file.
# 6.    It assumes that the PostgreSQL databases on both nodes, the designated standby and 
#       the designated primaries are shut down, and all recovery.conf commands are renamed 
#       to recovery.done.
#
# Example for the designated primary:
# Archive command in postgresql.conf:
# archive_command = '/usr/local/bin/rsync -arv %p standby:/pgs/82_walarchives/%f </dev/null'
# Contents of recovery.conf/done:
# restore_command = 'cp /pgs/82_walarchives/%f %p'
#
# Example for the designated secondary:
#
# Archive command postgresql.conf:
# archive_command = '/usr/local/bin/rsync -arv %p primary:/pgs/82_walarchives/%f </dev/null'
# Contents of recovery.conf/done:
# restore_command = '/pgs/postgres-8.2.5/bin/pg_standby -k 10 -t /pgs/data/failover /pgs/82_walarchives %f %p'
#

# You have to customize the following variables:

# SOURCE_DATA Is the PGDATA directry of the current node, for normal use it would be the
#             designated standby node.
# Example:    SOURCE_DATA=/pgs/data
#
# SOURCE      Is the name of the source node. For normal usage it would be the name of the
#             designated standby node.
# Example:    SOURCE=standby-host
#
# TARGET_DATA Is the PGDATA of the target node, for normal use it would be the
#             designated primary node.
# Example:    TARGET_DATA=/pgs/data
#
# TARGET      Is the name of the target node. For normal usage it would be the name of the
#             designated primary node.
# Example:    TARGET=primary-host
#
# PGS_BASE    Is the Postgres Base directory, where the Postgres binaries are located
# Example:    PGS_BASE=/pgs/postgres-8.2.5 for a custom build PostgreSQL
# Example:    PGS_BASE=/usr/postgres/82 for a PostgreSQL delivered with Solaris 10 8/07
#
# PRI_GRP     Resource group which contains the cluster resource of the designated primary.
# Example:    PRI_GRP=primary-rg
#
# STDBY_GRP   Resource group which contains the cluster resource of the designated standby.
# Example:    STDBY_GRP=standby-rg
#
# STDBY_RS    Resource name which of the designated standby resource. This name should be unique on your standby.
#             The resilver-step1 script generated the file under /var/tmp/${STDBY_RS}-resilver resilver-step2 
#             requires this file.
# Example:    STDBY_RS=standby-rs

# ROLECHG_GRP The resource Group which contains the rolechanger resource.
# Example:    ROLECHG_GRP=rolechg-rg
#
# PRI_NODE    The nodename of the designated primary host/zone
# Example:    PRI_NODE=primary-host:primary-zone
#
# RSYNC       Absolute path to the RSYNC command including the necessary options.
# Example:    RSYNC="/usr/local/bin/rsync -rav"
#
# SSH_PASSPHRASE	True or false wether or not your ssh key is secured by a passphrase.
# Example:		SSH_PASSPHRASE=false


#####  Customize the following variables ##########

SOURCE_DATA=
SOURCE=
TARGET_DATA=
TARGET=
PGS_BASE=
PRI_GRP=
STDBY_GRP=
STDBY_RS=
ROLECHG_GRP=
PRI_NODE=
RSYNC=
SSH_PASSPHRASE=false

#####  End of customizations  ##########

# generic variables

TR=/usr/xpg4/bin/tr
SSH_AGENT=/usr/bin/ssh-agent
SSH_ADD=/usr/bin/ssh-add
SSH=/usr/bin/ssh
RM=/usr/bin/rm
ECHO=/usr/bin/echo
MV=/usr/bin/mv
CUT=/usr/bin/cut
AWK=/usr/bin/awk

# Check if the varables are set

custom_ok=0
if [ -z "${SOURCE}" ]
then
	${ECHO} "ERROR: customize the SOURCE variable"
	custom_ok=1
fi

if [ -z "${SOURCE_DATA}" ]
then
	${ECHO} "ERROR: customize the SOURCE_DATA variable"
	custom_ok=1
fi

if [ -z "${TARGET_DATA}" ]
then
	${ECHO} "ERROR: customize the TARGET_DATA variable"
	custom_ok=1
fi

if [ -z "${TARGET}" ]
then
	${ECHO} "ERROR: customize the TARGET variable"
	custom_ok=1
fi

if [ -z "${PGS_BASE}" ]
then
	${ECHO} "ERROR: customize the PGS_BASE variable"
	custom_ok=1
fi

if [ -z "${RSYNC}" ]
then
	${ECHO} "ERROR: customize the RSYNC variable"
	custom_ok=1
fi

if [ -d /etc/cluster ]
then
	if [ -z "${PRI_GRP}" ]
	then
		${ECHO} "ERROR: customize the PRI_GRP variable"
		custom_ok=1
	fi
	
	if [ -z "${STDBY_GRP}" ]
	then
		${ECHO} "ERROR: customize the STDBY_GRP variable"
		custom_ok=1
	fi

	if [ -z "${STDBY_RS}" ]
	then
		${ECHO} "ERROR: customize the STDBY_RS variable"
		custom_ok=1
	fi
	
	if [ -z "${ROLECHG_GRP}" ]
	then
		${ECHO} "ERROR: customize the ROLECHG_GRP variable"
		custom_ok=1
	fi
	
	if [ -z "${PRI_NODE}" ]
	then
		${ECHO} "ERROR: customize the PRI_NODE variable"
		custom_ok=1
	fi
fi

if [ ${custom_ok} -ne 0 ]
then
	exit 1
fi

if [ ! -f /var/tmp/${STDBY_RS}-resilver ]
then
	${ECHO} "ERROR: run resilver-step1 first"
	exit 1
fi

# start the ssh-agent and decrypt the private key depending on the SSH_PASSPHRASE variable

SSH_PASSPHRASE=`${ECHO} ${SSH_PASSPHRASE}|${TR} "[:upper:]" "[:lower:]"`

if [ "${SSH_PASSPHRASE}" == "true" ]
then
	eval `${SSH_AGENT} -s` >/dev/null 2>&1
	${SSH_ADD}
	if [ ${?} -ne 0 ]
	then
		exit 1
	fi
fi

# Configure the source to start in recovery mode.

${MV} ${SOURCE_DATA}/recovery.done ${SOURCE_DATA}/recovery.conf 

# Sync the currrent wal logs.

${RSYNC} ${SOURCE_DATA}/pg_xlog/* ${TARGET}:${TARGET_DATA}/pg_xlog
if [ ${?} -ne 0 ]
then
	${ECHO} ERROR: Problems at the rsync, these need to be fixed.
	exit 1
fi

# reconfigure the target to come up in recovery mode

${SSH} ${TARGET} ${MV} ${TARGET_DATA}/recovery.done ${TARGET_DATA}/recovery.conf

# determine the Postmaster version and release

pgs_version_string=$(${PGS_BASE}/bin/postmaster --version|${AWK} '{print $3}')
pgs_version=$(${ECHO} ${pgs_version_string}| ${CUT} -d . -f 1)
pgs_release=$(${ECHO} ${pgs_version_string}| ${CUT} -d . -f 2)

if [ -d /etc/cluster ]
then
	${ECHO} 
	${ECHO} "Step 1 As PostgreSQL user on the primary node/zone, start the target database manually "
	${ECHO} "       and check the logs that it gets to the ready state."
	${ECHO} "Step 2 As PostgreSQL user on the primary node/zone, shutdown the target database manually" 
	${ECHO} "       to allow the cluster to take control."
	${ECHO} 
	${ECHO} "Example:"
	${ECHO} "${PGS_BASE}/bin/pg_ctl -D ${TARGET_DATA} start"
	${ECHO} "${PGS_BASE}/bin/pg_ctl -D ${TARGET_DATA} stop -m fast"

	# There is a reverse rsync necessary to guarantee the same timeline between
	# priamry and standby database in 8.3 and below.

	if [ "${pgs_version}" -eq "8" ] 
	then
		if [ "${pgs_release}" -le "3" ]
		then

			${ECHO} 
			${ECHO} "Guarantee the same timeline between standby and primary."
			${ECHO} "As PostgreSQL user on the primary node/zone, sync the differences from the previous"
			${ECHO} "recovery during startup."
			${ECHO} 
			${ECHO} "Example:"
			${ECHO} "${RSYNC} ${TARGET_DATA}/* ${SOURCE}:${SOURCE_DATA}"
			${ECHO} 
			${ECHO} "As PostgreSQL user on the standby node/zone, remove the recovery.done file."
			${ECHO} 
			${ECHO} "Example:"
			${ECHO} "rm ${SOURCE_DATA}/recovery.done"
	
		fi
	fi
	${ECHO} 
	${ECHO} "Step 3 Start your cluster resource groups now"
	${ECHO} "Example: In the global zone as root call:"
	${ECHO} 
	${ECHO} "clrg online ${PRI_GRP}"
	${ECHO} "clrg online ${STDBY_GRP}"
	${ECHO} "clrg online -n ${PRI_NODE} ${ROLECHG_GRP}"
else
	${ECHO} 
	${ECHO} "Step 1 As PostgreSQL user on the primary node/zone, start the target database manually "
	${ECHO} "       and check the logs that it gets to the ready state."
	${ECHO} 
	${ECHO} "Example:"
	${ECHO} "${PGS_BASE}/bin/pg_ctl -D ${TARGET_DATA} start"

	# There is a reverse rsync necessary to guarantee the same timeline between
	# priamry and standby database in 8.3 and below.

	if [ "${pgs_version}" -eq "8" ] 
	then
		if [ "${pgs_release}" -le "3" ]
		then

			${ECHO} 
			${ECHO} "Guarantee the same timeline between standby and primary."
			${ECHO} "As PostgreSQL user on the primary node/zone, sync the differences from the previous"
			${ECHO} "recovery during startup."
			${ECHO} 
			${ECHO} "Example:"
			${ECHO} "${RSYNC} ${TARGET_DATA}/* ${SOURCE}:${SOURCE_DATA}"
			${ECHO} 
			${ECHO} "As PostgreSQL user on the standby node/zone, remove the recovery.done file."
			${ECHO} 
			${ECHO} "Example:"
			${ECHO} "rm ${SOURCE_DATA}/recovery.done"
	
		fi
	fi
	${ECHO} 
	${ECHO} "Step 2 As PostgreSQL user on the standby node/zone, start the database manually "
	${ECHO} "       and check the logs that it starts in recovery mode state."
	${ECHO} 
	${ECHO} "Example:"
	${ECHO} "${PGS_BASE}/bin/pg_ctl -D ${TARGET_DATA} start"
fi

# stop the ssh-agent depending on the SSH_PASSPHRASE variable

if [ "${SSH_PASSPHRASE}" == "true" ]
then
	${SSH_AGENT} -k >/dev/null 2>&1
fi

# remove the /var/tmp/${STDBY_RS}-resilver file
${RM} /var/tmp/${STDBY_RS}-resilver 2>/dev/null
