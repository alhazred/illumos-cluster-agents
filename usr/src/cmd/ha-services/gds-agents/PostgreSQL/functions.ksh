#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

# ident   "@(#)functions.ksh 1.14     09/01/26 SMI"

PKG=SUNWscPostgreSQL
METHOD=`basename $0`
TASK_COMMAND=""

ZONENAME=/usr/bin/zonename
DIRNAME=/usr/bin/dirname

SCLOGGER=/usr/cluster/lib/sc/scds_syslog
PMFADM=/usr/cluster/bin/pmfadm
GETENT=/usr/bin/getent
AWK=/usr/bin/awk
NAWK=/usr/bin/nawk
CAT=/usr/bin/cat
EGREP=/usr/bin/egrep
GREP=/usr/bin/grep
ENV=/usr/bin/env
HEAD=/usr/bin/head
PS=/usr/bin/ps
PGREP=/usr/bin/pgrep
KILL=/usr/bin/kill
IPCS=/usr/bin/ipcs
IPCRM=/usr/bin/ipcrm
ECHO=/usr/bin/echo
TR=/usr/bin/tr
SU=/usr/bin/su
RM=/usr/bin/rm
ZLOGIN=/usr/sbin/zlogin
TEST=/usr/bin/test
PING=/usr/sbin/ping
SED=/usr/bin/sed
IFCONFIG=/usr/sbin/ifconfig
SSH=/usr/bin/ssh
SSH_AGENT=/usr/bin/ssh-agent
PKILL=/usr/bin/pkill
WC=/usr/bin/wc
SLEEP=/usr/bin/sleep
TOUCH=/usr/bin/touch
CHOWN=/usr/bin/chown
CP=/usr/bin/cp

get_fmri_parameters ()
{

# extract the smf properties, you need to call your agent commands

        debug_message "Function: get_fmri_parameters - Begin "
        ${SET_DEBUG}

# Resource name

	RESOURCE=`/usr/bin/svcprop -p parameters/Resource ${SMF_FMRI}`

# Resource Group

	RESOURCEGROUP=`/usr/bin/svcprop -p parameters/Resource_group ${SMF_FMRI}`

# Start Project

	Project=:default
	if /usr/bin/svcprop ${SMF_FMRI}|grep start/project >/dev/null
	then
		Project=`/usr/bin/svcprop -p start/project ${SMF_FMRI}`
		if [ "${Project}" != ":default" ]
		then
			PROJ_OPT=" -P ${Project}"
			ZONE_PROJECT=${Project}
		fi
	fi

# Parameter file

	PARFILE=`/usr/bin/svcprop -p parameters/Parameter_File ${SMF_FMRI}`

        debug_message "Function: get_fmri_parameters - End "
}

set_shell_specifics()
{

	# Set variables to construct sourcing and output redirection depending 
	# on the login shell of the user

        debug_message "Function: set_shell_specifics - Begin"
        ${SET_DEBUG}

	ENVSC=

        if /usr/bin/getent passwd ${USER} | /usr/bin/awk -F: '{print $7}' | /usr/bin/grep csh > /dev/null
        then

		# C shell specifics

		# sourcing

		if [ -n "${ENVSCRIPT}" ]
		then
			ENVSC="source ${ENVSCRIPT};"
		fi

		# brackets for subshells

		OPEN_BRACKET="("
		CLOSE_BRACKET=")"

		# redirection

		OUTPUT_APP=">>& ${LOGFILE}"
		OUTPUT=">& ${LOGFILE}"
		CAT_OUTPUT="> /tmp/${RESOURCE}-${USER}-cat-out"
		CAT_ERRPUT=">& /tmp/${RESOURCE}-${USER}-cat-err"
		TBL_OUTPUT="> /tmp/${RESOURCE}-${USER}-tbl-out"
		TBL_ERRPUT=">& /tmp/${RESOURCE}-${USER}-tbl-err"
        else

		# Korn shell specifics

		# sourcing

		if [ -n "${ENVSCRIPT}" ]
		then
			ENVSC=". ${ENVSCRIPT};"
		fi

		# no subshell needed in a ksh 

		OPEN_BRACKET=
		CLOSE_BRACKET=

		# redirection

	        OUTPUT_APP=">> ${LOGFILE} 2>&1"
	        OUTPUT="> ${LOGFILE} 2>&1"
		CAT_OUTPUT="> /tmp/${RESOURCE}-${USER}-cat-out"
		CAT_ERRPUT="2> /tmp/${RESOURCE}-${USER}-cat-err"
		TBL_OUTPUT="> /tmp/${RESOURCE}-${USER}-tbl-out"
		TBL_ERRPUT="2> /tmp/${RESOURCE}-${USER}-tbl-err"
        fi


        debug_message "Function: set_shell_specifics - End"
}

create_pfile()
{

	# Creation of the parameter file. This function is used at registration
	# time only.

        debug_message "Function: create_pfile - Begin "
        ${SET_DEBUG}

	if [ -z "${PFILE}" ]
	then
		${ECHO} "ERROR: set the variable parameter file"
		return 1
	fi
	
	# determine wether the parameter file needs to be created in a zone or not

	zonecmd=

	pfile_tmp="/tmp/pgspfile.${RS}"

	# get the zonename of the resource group

	target_zone=$(rgs_zonename) 
	if [ -n "${target_zone}" ]
	then
		zonecmd="${ZLOGIN} ${target_zone}"
	fi

	# check the directory of the parameter file

	pdir=`${zonecmd} ${DIRNAME} ${PFILE}`

	# test for the existance of the parameter files directory either in the local or
	# the global zone according to the zone entry in the nodelist

	if ! ${zonecmd} ${TEST} -d ${pdir}
	then
		${ECHO} "ERROR: set the variable parameter file in an existing directory"
		return 1
	fi

	# create the parameter file 

	${ECHO} "Prepare the parameter file ${PFILE}"

	${CAT} <<EOF > ${pfile_tmp}
	
#
# Content for the parameter file
#
#            USER - The Solaris user, which owns the PostgreSQL database
#          PGROOT - Contains the path to the PostgreSQL directory. Below this
#                   directory the postgres binaries are located in the ./bin
#                   directory.
#          PGDATA - Contains the path to the databases of this specific PostgreSQL
#                   instance. The pg_hba.conf needs to be here.
#          PGPORT - Port where the postmaster process will be listening to.
#          PGHOST - Hostname where the postmaster process is listening to, or a directory where the
#                   Unix socket file is stored.
#                   If set to a valid hostname, the PGHOST variable forces the probe to 
#                   traverse the TCP/IP stack. If the PGHOST variable is empty or starts with a "/", 
#                   the probe will use a socket. If the PGHOST variable starts with a "/", the entry must
#                   be the directory which contains the socket file. 
#       PGLOGFILE - Logfile where the log messages of the postmaster will be stored.
# LD_LIBRARY_PATH - This path contains all the necessary libraries for this PostgreSQL
#                   installation.
#                   Optional
#       ENVSCRIPT - Script to contain PostgreSQL specific runtime variables.
#                   Optional
#            SCDB - This specific PostgreSQL database will be monitored.
#          SCUSER - PostgresSQL user to connect to the $SCDB database.
#         SCTABLE - Table name in the $SCDB database. This table name will be manipulated
#                   to check if PostgreSQL is alive. This table will be generated at database
#                   preparation time.
#          SCPASS - Password of the SCUSER. If no password is provided the authentication method 
#                   for the SCDB database needs to be trusted for requests from the localhost.
#                   Optional
#        NOCONRET - Return code for connection errors. This return code has to follow the rules
#                   for the generic data service. The value has to be between 1 and 100.
#                   100/NOCONRET defines the number of consecutive probes to ignore for failed
#                   connections. A restart or failover will occur, if the number is exeeded within
#                   the retry interval.


USER=${USER}
PGROOT=${PGROOT}
PGDATA=${PGDATA}
PGPORT=${PGPORT}
PGHOST=${PGHOST}
PGLOGFILE=${PGLOGFILE}
LD_LIBRARY_PATH=${LD_LIBRARY_PATH}
ENVSCRIPT=${ENVSCRIPT}
SCDB=${SCDB}
SCUSER=${SCUSER}
SCTABLE=${SCTABLE}
SCPASS=${SCPASS}
NOCONRET=${NOCONRET}

# The following parameters need to be configured only if logfile shipping is configured to ship
# the PosgreSQL WAL logs between a designated primary and a designated standby resource.
# They need to be configured only on the primary.

#        STDBY_RS  The resource name of the PostgreSQL standby resource.
#        STDBY_RG  The resource group name of the PostgreSQL standby resource group.
#      STDBY_USER  User which is the owner of the standby postgres database.
#      STDBY_HOST  Resolvable name of the standby host or the standby zone, 
#                  this name has to be reachable via ssh.
#   STDBY_PARFILE  The standbys postgres parameter file to get the rest of the necessary parameters.
#   STDBY_PING     The number of packets the primary uses to ping the standby host. If this variable is
#                  empty, it will be set to 5 packets.
#   ROLECHG_RS     The rolechangers resource name.
#  SSH_PASSDIR     A directory where the ssh passphrase is stored in a the file <resourcename>-phrase.
#                  This parameter is needed only if you configured WAL file shipping and secured your
#                  ssh key with a passphrase.
#                  Leave it undefined if the passprase is empty.
#
# If you configure the logfile shipping in a shared nothing topology, do not set the LH parameter.
#
# Configure the following paramters on the primary host.

STDBY_RS=${STDBY_RS}
STDBY_RG=${STDBY_RG}
STDBY_USER=${STDBY_USER}
STDBY_HOST=${STDBY_HOST}
STDBY_PARFILE=${STDBY_PARFILE}
STDBY_PING=${STDBY_PING}
#
# Configure the following paramter on the standby host.
#
ROLECHG_RS=${ROLECHG_RS}
#
# Configure the following parameter on both hosts.
#
SSH_PASSDIR=${SSH_PASSDIR}

EOF
	if [ $? -ne 0 ]
	then
		${ECHO} "ERROR: could not create the temporary parameter file ${pfile_tmp}"
		return 1
	fi

	# create the parameter file either in the global or in the prepared target zone

	if [ -n "${target_zone}" ]
	then
		${CAT} ${pfile_tmp} | ${zonecmd} ${CAT} - \>${PFILE} 
		if [ $? -ne 0 ]
		then
			${ECHO} "ERROR: could not create the parameter file ${PFILE}"
			return 1
		fi
	else
		${CAT} ${pfile_tmp} > ${PFILE}
		if [ $? -ne 0 ]
		then
			${ECHO} "ERROR: could not create the parameter file ${PFILE}"
			return 1
		fi
	fi
		
        debug_message "Function: create_pfile - End "
	return 0
}

validate_options()
{
        debug_message "Function: validate_options - Begin"
	${SET_DEBUG}

        #
        # Ensure all mandatory options are set
        #

        for i in RESOURCE RESOURCEGROUP PARFILE
        do
                case ${i} in
                        RESOURCE)
                        if [ -z "${RESOURCE}" ]; then
                                # SCMSGS
                                # @explanation
                                # The start, stop or probe command requires an
                                # option which is not set.
                                # @user_action
                                # Fix the start, stop or probe command in the
                                # SUNW.gds resource.
                                scds_syslog -p daemon.err -t $(syslog_tag) -m \
                                	"Function: validate_options: %s Option %s not set" \
                                	"${METHOD}" "-R"
                                return 1
                        fi;;

                        RESOURCEGROUP)
                        if [ -z "${RESOURCEGROUP}" ]; then
                                scds_syslog -p daemon.err -t $(syslog_tag) -m \
	                                "Function: validate_options: %s Option %s not set" \
                                	"${METHOD}" "-G"
                                return 1
                        fi;;

                        PARFILE)
                        if [ -z "${PARFILE}" ]; then
                                scds_syslog -p daemon.err -t $(syslog_tag) -m \
	                                "Function: validate_options: %s Option %s not set" \
                                	"${METHOD}" "-P"
                                return 1
                        fi;;
                esac
        done

	debug_message "Function: validate_options - End"
}

validate()
{
	#
	# Validate
	#
	
        debug_message "Function: validate - Begin"
	${SET_DEBUG}

	rc_validate=0

	if ! val_parfile ${PARFILE} "USER PGROOT PGDATA PGPORT PGLOGFILE SCDB SCUSER SCTABLE NOCONRET"
	then
		debug_message "Function: validate - End"
		rc_validate=1
		return ${rc_validate}
	fi

	. ${PARFILE}

	for i in `${CAT} ${PARFILE} |grep -v "^#"|grep -v "^ "|nawk -F= '{print $1}'`
	do
		case $i in
			USER)

			# Test the PostgresSQL OS user

                        if [ -z "${USER}" ]; then
				# SCMSGS
				# @explanation
				# A mandatory variable is unset in the
				# parameter file.
				# @user_action
				# Fix the parameter file and provide a value
				# for the variable in the parameter file
				scds_syslog -p daemon.err -t $(syslog_tag) -m \
					"Function: validate: The %s variable is not set, but it is required" \
					"USER"
				rc_validate=1
			else
				id ${USER} >/dev/null 2>&1
				if [ $? -ne 0 ]; then
   					# SCMSGS
   					# @explanation
   					# The user mentioned in the parameter
   					# file is not defined in the OS.
   					# @user_action
   					# Fix the parameter file and provide
   					# an existing user for the variable
   					# USER.
   					scds_syslog -p daemon.err -t $(syslog_tag) -m \
						"Function: validate: User %s does not exist, an existing user is required" \
						"${USER}"
					rc_validate=1
				else 
					debug_message "Function: validate - USER OK"	
                        	fi
                        fi;;

			PGROOT)

			# Test the PG Root variable
	                      
                        if [ -z "${PGROOT}" ]; then
   				scds_syslog -p daemon.err -t $(syslog_tag) -m \
                                	"Function: validate: The %s variable is not set, but it is required" \
					"PGROOT"
				rc_validate=1
			else
	                        if [ ! -d ${PGROOT} ]; then
	   				# SCMSGS
	   				# @explanation
	   				# The directory mentioned in the
	   				# parameter file for the PGROOT or
	   				# PGDATA variable does not exist.
	   				# @user_action
	   				# Fix the parameter file and provide
	   				# an existing directoy for the
	   				# variable PGROOT or PGDATA.
	   				scds_syslog -p daemon.err -t $(syslog_tag) -m \
	                                	"Function: validate: Directory %s does not exist, an existing directory is required" \
						"${PGROOT}"
					rc_validate=1
	                        fi

				# test if it is a postgres installation

				if [ ! -f ${PGROOT}/bin/pg_ctl ]
				then
	   				# SCMSGS
	   				# @explanation
	   				# The directory mentioned in the
	   				# PGROOT variable does not contain the
	   				# PostgreSQL binaries in its bin
	   				# directory.
	   				# @user_action
	   				# Provide the directory which does
	   				# contain at least the PostgreSQL
	   				# binaries in the path ./bin.
	   				scds_syslog -p daemon.err -t $(syslog_tag) -m \
	                                	"Function: validate: Directory %s does not contain the PostgreSQL binaries" \
						"${PGROOT}"
					rc_validate=1
				else 
					debug_message "Function: validate - PGROOT OK"	
	                        fi
                        fi;;

			PGDATA)

			# Test the PG Data variable
	                      
                        if [ -z "${PGDATA}" ]; then
   				scds_syslog -p daemon.err -t $(syslog_tag) -m \
                                	"Function: validate: The %s variable is not set, but it is required" \
					"PGDATA"
				rc_validate=1
			else
	                        if [ ! -d ${PGDATA} ]; then
	   				scds_syslog -p daemon.err -t $(syslog_tag) -m \
	                                	"Function: validate: Directory %s does not exist, an existing directory is required" \
						"${PGDATA}"
					rc_validate=1
	                        fi

				# test if it is a postgres database cluster

				if [ ! -f ${PGDATA}/postgresql.conf ]
				then
	   				# SCMSGS
	   				# @explanation
	   				# A directory is specified in the
	   				# PGDATA variable which does not
	   				# contain a postgresql.conf file.
	   				# @user_action
	   				# Specify a directory in the PGDATA
	   				# variable in the parmeter file, which
	   				# contains the postgresql.conf file.
	   				scds_syslog -p daemon.err -t $(syslog_tag) -m \
	                                	"Function: validate: Directory %s does not contain the PostgreSQL configuration files" \
						"${PGDATA}"
					rc_validate=1
				else 
					debug_message "Function: validate - PGDATA OK"	
	                        fi
                        fi;;

			PGPORT) 

			# Test the PG Port variable
	                      
                        if [ -z "${PGPORT}" ]; then
   				scds_syslog -p daemon.err -t $(syslog_tag) -m \
                                	"Function: validate: The %s variable is not set, but it is required" \
					"PGPORT"
				rc_validate=1
			else

				# test if the port is numeric

	                        if  ! let x=${PGPORT} >/dev/null 2>&1
				then
	   				# SCMSGS
	   				# @explanation
	   				# In the parameter file, there is a
	   				# non numeric character in the value
	   				# for the PGPORT variable.
	   				# @user_action
	   				# Fix the PGPORT variable in the
	   				# parameter file.
	   				scds_syslog -p daemon.err -t $(syslog_tag) -m \
	                                	"Function: validate: Port %s is not numeric" \
						"${PGPORT}"
					rc_validate=1
				else 
					debug_message "Function: validate - PGPORT OK"	
	                        fi
                        fi;;

			PGHOST)

			# test the PGHOST variable ony if it is defined

			if [ -n "${PGHOST}" ]
			then

				# strip of leading spaces

				PGHOST=`print ${PGHOST}|${SED} 's/^ *//'`

				if ${ECHO} ${PGHOST} | ${GREP} "^/" >/dev/null 2>&1
				then
			
					if [ ! -d "${PGHOST}" ]
					then
						
						# SCMSGS
						# @explanation
						# The directory specified in the PGHOST variable does not
						# exist.
						# @user_action
						# Create the directory. None if it was a lost mount.
						scds_syslog -p daemon.err -t $(syslog_tag) -m \
							"Function: validate - Directory for the socket file %s does not exist" \
							"${PGHOST}"
						rc_validate=1
			
					else
						debug_message "Function: validate - PGHOST OK"	
					fi
				
				else
					if ! ${GETENT} hosts ${PGHOST} >/dev/null 2>&1
					then
						
						# SCMSGS
						# @explanation
						# The host specified in the PGHOST variable is not
						# resolvable.
						# @user_action
						# Create the the host entry in /etc/hosts.
						scds_syslog -p daemon.err -t $(syslog_tag) -m \
							"Function: validate - The host %s is not resolvable" \
							"${PGHOST}"
						rc_validate=1
						
					else
						debug_message "Function: validate - PGHOST resolvable"	
					fi

					# Validate if the address of the PGHOST variable is configured UP
					# on a adapter of the host. This validation works in
					# global zones, local zones and in failover zones.
					# This method was preferred over checking the dependency tree,
					# because it works in failover zones, even if there is no logical
					# host configured. 

					# Set the laguage to C to avoid localisation problems with ifconfig

					LANG=C

					addr=`${GETENT} hosts ${PGHOST}| ${AWK} '{print $1}'`

					# Check if the address is configured up on the node
	
					adapter_success=1
					for i in `${IFCONFIG} -a |${GREP} UP|${AWK} '{print $1}' |${EGREP} -v "zone|inet"|${GREP} ":$"|${SED} s/:$//`
					do

						${IFCONFIG} ${i}|${GREP} ${addr} >/dev/null 2>&1
						if [ ${?} -eq 0 ]
						then
							adapter_success=0
						fi

					done

					if [ ${adapter_success} -ne 0 ]
					then
						
						# SCMSGS
						# @explanation
						# The host specified in the PGHOST variable is not
						# configured up on the hosts adapters.
						# @user_action
						# Fix either the network configuration or the PGHOST variable
						scds_syslog -p daemon.err -t $(syslog_tag) -m \
							"Function: validate - The host %s is not configured UP on the hosts adapters" \
							"${PGHOST}"
						rc_validate=1
					else
						debug_message "Function: validate - PGHOST is configured on the nodes adapters"	
					fi
				fi
			fi;;

			PGLOGFILE)

			# Test the Logfile variable
	                      
                        if [ -z "${PGLOGFILE}" ]; then
   				scds_syslog -p daemon.err -t $(syslog_tag) -m \
                                	"Function: validate: The %s variable is not set, but it is required" \
					"PGLOGFILE"
				rc_validate=1
			else

				# test if the directory for Logfile exists

				Logdir=`/usr/bin/dirname ${PGLOGFILE}`
	                        if [ ! -d ${Logdir} ]; then
	   				# SCMSGS
	   				# @explanation
	   				# The path to the filename variable
	   				# PGLOGFILE does not exist.
	   				# @user_action
	   				# Qualify the filename of the parameter
	   				# files PGLOGFILE variable in an
	   				# existing directory.
	   				scds_syslog -p daemon.err -t $(syslog_tag) -m \
	                                	"Function: validate: Directory for logfile %s does not exist, an existing directory is required" \
						"${PGLOGFILE}"
					rc_validate=1
				else 
					debug_message "Function: validate - PGLOGFILE OK"	
	                        fi
                        fi;;

			LD_LIBRARY_PATH)

			# test LD_LIBRARY_PATH if it is set

			if [ -n "${LD_LIBRARY_PATH}" ]
			then
				export LD_LIBRARY_PATH
				if ! ${PGROOT}/bin/psql --help >/dev/null 2>&1
				then
	   				# SCMSGS
	   				# @explanation
	   				# The LD_LIBRARY_PATH is not valid to
	   				# call the postgres binary.
	   				# @user_action
	   				# Qualify the LD_LIBRARY_PATH in the
	   				# parameter file until it is
	   				# sufficient for the psql binary.
	   				scds_syslog -p daemon.err -t $(syslog_tag) -m \
	                                	"Function: validate: The LD_LIBRARY_PATH %s is not valid for this PostgreSQL installation" \
						"${LD_LIBRARY_PATH}"
					rc_validate=1
				else 
					debug_message "Function: validate - LD_LIBRARY_PATH OK"	
				fi
			fi;;

			ENVSCRIPT) 

			# test if the environment script is a syntactically valid script
			# In the smf context it needs to be a ksh script.
			# In the GDS context the script type ksh/csh is dependent from the login shell of the user.

			if [ -n "${ENVSCRIPT}" ]
			then
				if [ -f ${ENVSCRIPT} ]
				then
					if ${GETENT} passwd ${USER} | ${AWK} -F: '{print $7}' | ${GREP} "csh" > /dev/null  && [ -z "${SMF_FMRI}" ]
					then
						if ! /usr/bin/csh -n ${ENVSCRIPT} > /dev/null 2>&1
						then
			   				# SCMSGS
			   				# @explanation
			   				# The environment
			   				# script specified in
			   				# the parameter file
			   				# needs to be a valid
			   				# c shell script,
			   				# because the login
			   				# shell of the
			   				# PostgreSQL user is c
			   				# shell compliant.
			   				# @user_action
			   				# Fix the environment
			   				# script until it
			   				# passes csh -n
			   				# scriptname.
			   				scds_syslog -p daemon.err -t $(syslog_tag) -m \
			                                "Function: validate: The Environment script %s is not a valid c shell script" \
							"${ENVSCRIPT}"
							rc_validate=1
						else 
							debug_message "Function: validate - ENVSCRIPT OK"	
						fi
					else
						if ! /usr/bin/ksh -n ${ENVSCRIPT} >/dev/null 2>&1
						then
		   					# SCMSGS
		   					# @explanation
		   					# The environment
		   					# script specified in
		   					# the parameter file
		   					# needs to be a valid
		   					# korn shell script,
		   					# because the login
		   					# shell of the
		   					# PostgreSQL user is
		   					# korn shell
		   					# compliant.
		   					# @user_action
		   					# Fix the environment
		   					# script until it
		   					# passes ksh -n
		   					# scriptname.
		   					scds_syslog -p daemon.err -t $(syslog_tag) -m \
		               	 	                	"Function: validate: The Environment script %s is not a valid korn shell script" \
								"${ENVSCRIPT}"
							rc_validate=1
						else 
							debug_message "Function: validate - ENVSCRIPT OK"	
						fi
					fi
				else
			   		# SCMSGS
			   		# @explanation
			   		# The filename specified in the
			   		# parameter files ENVSCRIPT variable
			   		# does not exist.
			   		# @user_action
			   		# Fix the parameter file and specify a
			   		# valid Environment script.
			   		scds_syslog -p daemon.err -t $(syslog_tag) -m \
			                	"Function: validate: The Environment script %s does not exist" \
						"${ENVSCRIPT}"
					rc_validate=1
			
				fi
			fi;;

			SCDB) 

			# Test if the test database variable is defined
	                      
                        if [ -z "${SCDB}" ]; then
   				scds_syslog -p daemon.err -t $(syslog_tag) -m \
                                	"Function: validate: The %s variable is not set, but it is required" \
					"SCDB"
				rc_validate=1
			else 
				debug_message "Function: validate - SCDB OK"	
			fi;;

			SCUSER)

			# Test if the database user variable is defined
	                      
                        if [ -z "${SCUSER}" ]; then
   				scds_syslog -p daemon.err -t $(syslog_tag) -m \
                                	"Function: validate: The %s variable is not set, but it is required" \
					"SCUSER"
				rc_validate=1
			else 
				debug_message "Function: validate - SCUSER OK"	
			fi;;

			SCTABLE)

			# Test if the database table variable is defined
	                      
                        if [ -z "${SCTABLE}" ]; then
   				scds_syslog -p daemon.err -t $(syslog_tag) -m \
                                	"Function: validate: The %s variable is not set, but it is required" \
					"SCTABLE"
				rc_validate=1
			else 
				debug_message "Function: validate - SCTABLE OK"	
			fi;;

			NOCONRET)

			# Test the No Connection return code  variable
	                      
                        if [ -z "${NOCONRET}" ]; then
   				scds_syslog -p daemon.err -t $(syslog_tag) -m \
                                	"Function: validate: The %s variable is not set, but it is required" \
					"NOCONRET"
				rc_validate=1
			else

				# test if the NOCONRET is numeric

	                        if  ! let x=${NOCONRET} >/dev/null 2>&1
				then
	   				# SCMSGS
	   				# @explanation
	   				# The value for the NOCONRET variable
	   				# contains a non numeric character.
	   				# @user_action
	   				# Fix the NOCONRET variable in the
	   				# parameter file.
	   				scds_syslog -p daemon.err -t $(syslog_tag) -m \
	                                	"Function: validate: Return code for failed connections %s is not numeric" \
						"${NOCONRET}"
					rc_validate=1
				else 
					if [ ${NOCONRET} -gt 100 ]; then
	   					# SCMSGS
	   					# @explanation
	   					# The value of the NOCONRET
	   					# variable in the parameter
	   					# file exeeds 100.
	   					# @user_action
	   					# Fix the parameter file with
	   					# a value below 100.
	   					scds_syslog -p daemon.err -t $(syslog_tag) -m \
	                                		"Function: validate: Return code for failed connections %s is greater 100" \
							"${NOCONRET}"
						rc_validate=1
					else
						debug_message "Function: validate - NOCONRET OK"	
					fi
	                        fi
                        fi;;

			STDBY_HOST) 

			# Test the standby host variable and the neccessary other variables if the standby 
			# host variable is not set.
	                      
			if [ -n "${STDBY_HOST}" ]; then
				if ! ${GETENT} hosts ${STDBY_HOST} >/dev/null 2>&1
				then
			
					# SCMSGS
					# @explanation
					# The host specified in the STDBY_HOST variable is not
					# resolvable.
					# @user_action
					# Add the host to one of your configured name services, 
					# so it can get listed with getent.
					scds_syslog -p daemon.err -t $(syslog_tag) -m \
						"Function: validate - The standby host %s is not resolvable" \
						"${STDBY_HOST}"
					rc_validate=1
						
				else
					debug_message "Function: validate - STDBY_HOST resolvable"	
				fi

				# Test if all the other standby related variables are set.
				# There will be no other validation, because it may be that the necessary
				# resources are not up right now, and then the validation will fail without 
				# a valid reason which is related to the parameter configuration.

				if [ -z "${STDBY_RS}" ]
				then
   					scds_syslog -p daemon.err -t $(syslog_tag) -m \
						"Function: validate: The %s variable is not set, but it is required" \
						"STDBY_RS"
					rc_validate=1
				else
					debug_message "Function: validate - STDBY_RS OK"
				fi

				if [ -z "${STDBY_RG}" ]
				then
   					scds_syslog -p daemon.err -t $(syslog_tag) -m \
						"Function: validate: The %s variable is not set, but it is required" \
						"STDBY_RG"
					rc_validate=1
				else
					debug_message "Function: validate - STDBY_RG OK"
				fi

				if [ -z "${STDBY_USER}" ]
				then
   					scds_syslog -p daemon.err -t $(syslog_tag) -m \
						"Function: validate: The %s variable is not set, but it is required" \
						"STDBY_USER"
					rc_validate=1
				else
					debug_message "Function: validate - STDBY_USER OK"
				fi

				if [ -z "${STDBY_PARFILE}" ]
				then
   					scds_syslog -p daemon.err -t $(syslog_tag) -m \
						"Function: validate: The %s variable is not set, but it is required" \
						"STDBY_PARFILE"
					rc_validate=1
				else
					debug_message "Function: validate - STDBY_PARFILE OK"
				fi

				if [ -n "${STDBY_PING}" ]
				then
	                        	if  ! let x=${STDBY_PING} >/dev/null 2>&1
					then
						# SCMSGS
						# @explanation
						# In the parameter file, there is a
						# non numeric character in the value
						# for the STDBY_PING variable.
						# @user_action
						# Fix the STDBY_PING variable in the
						# parameter file.

   						scds_syslog -p daemon.err -t $(syslog_tag) -m \
							"Function: validate: The STDBY_PING value of %s is not numeric" \
							"${STDBY_PING}"
						rc_validate=1
					else
						debug_message "Function: validate - STDBY_PING OK"
					fi
				else
					# set a default of 5 packets
					STDBY_PING=5
					debug_message "Function: validate - STDBY_PING OK, it was set to 5 packets"
				fi

			fi;;
		esac
	done

	debug_message "Function: validate - End"
	return ${rc_validate}
}

validate_probe()
{
	# Validate ony for non existant files or lost directories

        debug_message "Function: validate_probe - Begin"
	${SET_DEBUG}
	
	rc_val_probe=0
	
	# If the parameter file does not pass the short validation, try a failover
	# A failover is the appropriate action, because the directory existed
	# at startup of the resource.

	if ! val_parfile ${PARFILE}
	then


                # SCMSGS
               	# @explanation
               	# The file specified in the PARFILE variable does not
		# exist any more.
               	# @user_action
               	# Create the directory and restore the parameter file. None if it was a lost mount.
               	scds_syslog -p daemon.err -t $(syslog_tag) -m \
               		"Function: validate_probe - Directory for the parameter file %s does not exist any more, a faiover will occur" \
              		"${PARFILE}"

		rc_val_probe=201
	fi

	# source the parameter file

	. ${PARFILE}

	# Check the PGHOST variable.
	# If the PGHOST variable starts with a / then it should be a directory which contains the socket file.
	# If this directory is not available, a failover will be initiated.
	# If it does not contain a / then it has to be a hostname or an address, in this case the hostname 
	# has to respond to ping. 

	if [ -n "${PGHOST}" ]
	then

		# strip of leading spaces

		PGHOST=`print ${PGHOST}|${SED} 's/^ *//'`

		if ${ECHO} ${PGHOST} | ${GREP} "^/" >/dev/null 2>&1
		then
	
			if [ ! -d "${PGHOST}" ]
			then
				
	                	# SCMSGS
	                	# @explanation
	                	# The directory specified in the PGHOST variable does not
				# exist any more.
	                	# @user_action
	                	# Create the directory. None if it was a lost mount.
	                	scds_syslog -p daemon.err -t $(syslog_tag) -m \
	                  		"Function: validate_probe - Directory for the socket file %s does not exist any more, a faiover will occur" \
	                  		"${PGHOST}"
				rc_val_probe=201
			else
				debug_message "Function: validate_probe - the directory in PGHOST exists"
			fi
		
		else

			# If an address is specified, test it with ping. Initiate a failover if the address 
			# is unavailable.	
			# This needs to be done, because an unpingable address will cause the psql commands to hang
			# The test will detect accidentally unconfigured interfaces, just a failover or restart of 
			# the resource group can cure this problem.

			# This ping test is here to prevent a predictable timeout, it is needed as long as 
			# SUNW.LogicalHost does not probe the ipaddresses. As soon as this changes the
			# ping test should be removed.

			if ! ${PING} ${PGHOST} 1 >/dev/null 2>&1
			then
				# SCMSGS
				# @explanation
				# The address specified in the PGHOST variable
				# is unavailable.
				# @user_action
				# None, a failover will occur
				scds_syslog -p daemon.err -t $(syslog_tag) -m \
 	            			"check_pgs: The host %s is not accessible, a failover will occur" \
	      	       			"${PGHOST}"
	
				rc_val_probe=201
			else
				debug_message "Function: validate_probe - the host in PGHOST is pingable"
			fi
		fi
	fi

        debug_message "Function: validate_probe - End"
	return $rc_val_probe
}

val_parfile()
{
	# Common valdation for parameter file
	#
	# If the parameter is in $2, there will be an intensive validation.
	# If $2 is empty, the validation will just check for the existance of the file.
	
        debug_message "Function: val_parfile - Begin"
	${SET_DEBUG}

	rc_val_parfile=0

        # Validate that parameter file exists

	PARFILE=${1}
        PARLIST=${2}

        if [ ! -f "${PARFILE}" ]; then
                # SCMSGS
                # @explanation
                # The parameter file does not exist in the parameter file
                # directory.
                # @user_action
                # Restore the parameter file or re-register the resource.
                scds_syslog -p daemon.err -t $(syslog_tag) -m \
                  "Function: val_parfile - File %s does not exist" \
                  "${PARFILE}"
                rc_val_parfile=1
	else
		debug_message "Function: val_parfile - ${PARFILE} exists"	
 	fi

	# Test the semantics only, if the parameter list is specified.
	# This should not be done when called from validate_probe.

	if [ -n "${2}" ]
	then
	
		# Test if the parameter file is a valid ksh script
	
		if ! ksh -n ${PARFILE} >/dev/null 2>&1
		then
	        	# SCMSGS
	        	# @explanation
	        	# The parameter file is not a valid shell script.
	        	# @user_action
	        	# Correct the parameter file. It must pass ksh -n <file name>.
	        	scds_syslog -p daemon.err -t $(syslog_tag) -m \
	                  "Function: validate - Syntax errors in %s" \
	               	  "${PARFILE}"
	               rc_val_parfile=1
		else
			debug_message "Function: val_parfile - validated ${PARFILE}"	
	       	fi
	
		# Test if all the mandatory variables are included and set correctly in the parameter file
	
		PARAMETERS=`${CAT} ${PARFILE} |grep -v "^#"|grep -v "^ "|nawk -F= '{print $1}'`
	
		for i in ${PARLIST}
		do
			if ! `echo ${PARAMETERS} |grep ${i} > /dev/null `; then
	                	# SCMSGS
	                	# @explanation
	                	# The referenced necessary parameter is not mentioned
	                	# in the parameter file.
	                	# @user_action
	                	# Specify the parameter as a key value pair in the
	                	# parameter file.
	                	scds_syslog -p daemon.err -t $(syslog_tag) -m \
	                          "Function: val_parfile - %s not specified in %s, which is required" \
	                          "${i}" "${PARFILE}"
				rc_val_parfile=1
			else
				debug_message "Function: val_parfile - ${i} included in ${PARFILE}"	
	                fi
		done
	fi

	debug_message "Function: val_parfile - End"
	return ${rc_val_parfile}
}


Remove_shared_memory()
{
        debug_message "Function: Remove_shared_memory - Start called with the following arguments "$*
	${SET_DEBUG}

	User=$1

	if [ -n "${DEBUG}" ]
	then
		debug_message "IPC Status of the current zone BEFORE removal of non-attached segments created by user ${User}"

		# remove the logfile, before creating it to survive potential noclobber settings

		${RM} ${LOGFILE} 2>/dev/null
		${IPCS} -mcopb > ${LOGFILE}

		log_message debug ipcs
		${RM} ${LOGFILE}
	fi

	${IPCS} -mcopb |${GREP} " ${User} "| ${AWK} ' \
	{if (NF == 12 && $9 == 0 ) print $2,$5,$6,$11,$12; else \
	if (NF == 11  && $8 == 0 ) print $1,$4,$5,$10,$11 }' | \
	while read SHMID SHMUSER GROUP CPID LPID
	do
		if ${PS} -p ${CPID} > /dev/null
		then
			debug_message "PostgresSQL SHMID: ${SHMID} - CPID ${CPID} is running"
		else
			if ${PS} -p ${LPID} > /dev/null
			then
				debug_message "PostgresSQL SHMID: ${SHMID} - LPID ${LPID} is running"
			else
				SHMID=`${ECHO} ${SHMID} | ${TR} 'm' ' '`
				${IPCRM} -m ${SHMID} > /dev/null
				debug_message "PostgreSQL SHMID: ${SHMID} - removed"
				flag=deleted
			fi
		fi
	done

	if [ "${flag}" ]; then
		# SCMSGS
		# @explanation
		# Remaining IPC shared memory segments have been removed.
		# These segements are a leftover of the previous PostgreSQL
		# instance.
		# @user_action
		# None
		scds_syslog -p daemon.notice -t $(syslog_tag) -m \
			"All PostgreSQL non-attached IPC shared memory segments removed"
	fi

	if [ -n "${DEBUG}" ]
	then
		debug_message "IPC Status AFTER removal of non-attached segments created by user ${User}"

		${IPCS} -mcopb > ${LOGFILE}

		log_message debug ipcs
		$RM ${LOGFILE}
	fi

        debug_message "Function: Remove_shared_memory - End"
}

start_pgs()
{
	#
	# Start PostgreSQL
	#

        debug_message "Function: start_pgs - Begin"
	${SET_DEBUG}
	SECONDS=0

	# construct the necessary redirection and source variables

	set_shell_specifics

	# Define the appropriate newtask command.
	# The Project will be derived according to the call method,
	# either from the smf service or from the cluster resource / resource group

	srm_function ${USER}

	# remove shared memory which may be there from a killed postmaster

	Remove_shared_memory ${USER}

	# construct the necessary environment variables

	LIBPATH=
	if [ -n "${LD_LIBRARY_PATH}" ]
	then
		LIBPATH="${ENV} LD_LIBRARY_PATH=${LD_LIBRARY_PATH}"
	fi

	PORT="PGPORT=${PGPORT}"
	DATA="PGDATA=${PGDATA}"

	# remove the logfile, before creating it to survive potential noclobber settings

	${RM} ${LOGFILE} 2>/dev/null
	
	do_start=0

	# start an ssh-agent and store the decrypted private key if SSH_PASSDIR is configured

	
	if [ -n "${SSH_PASSDIR}" ]
	then
	
		${CP} -p ${SSH_PASSDIR}/${RESOURCE}-phrase /tmp/${RESOURCE}-phrase

		if [ -z "${SMF_FMRI}" ]
		then
			${CHOWN} ${USER} /tmp/${RESOURCE}-phrase
			${SU} ${USER} -c " ${TASK_COMMAND} ${MYDIR}/${MYNAME} -R ${RESOURCE} -G ${RESOURCEGROUP} -P ${PARFILE} start_ssh_agent "> /dev/null 2>&1
			if [ ${?} -ne 0 ]
			then
				do_start=1
			fi
		else
			${MYDIR}/${MYNAME} -R ${RESOURCE} -G ${RESOURCEGROUP} -P ${PARFILE} start_ssh_agent > /dev/null 2>&1
			if [ ${?} -ne 0 ]
			then
				do_start=1
			fi
		fi

		${RM} /tmp/${RESOURCE}-phrase


		# store the SSH_AUTH_SOCK value in the SOCK variable

		SOCK=
		if [ ${do_start} -eq 0 ]
		then
			SOCK=`${GREP} SSH_AUTH_SOCK /tmp/${RESOURCE}-ssh`
			SOCK="${ENV} ${SOCK}"
		fi
	fi

	# If a standby host is configured, check if the standby host is available and configured/acting
	# as a standby. If everything is ok continue with the startup. If not, do not start the PostgreSQL
	# resource.

	if [ -n "${STDBY_HOST}" ] && [ ${do_start} -eq 0 ]
	then

		if ${PING} ${STDBY_HOST} ${STDBY_PING} >/dev/null 2>&1
		then
			debug_message "Function: start_pgs - the host  ${STDBY_HOST} is accessible"
		
			# As the PostgreSQL user perform a check on the standby host, to determine if it
			# is configured as a primary, it runs as a primary, or it runs as a standby database.
	
			if [ -z "${SMF_FMRI}" ]
			then
	
				${SU} ${USER} -c " ${TASK_COMMAND} ${SOCK} ${SSH} ${STDBY_USER}@${STDBY_HOST} ${MYDIR}/${MYNAME} -R ${STDBY_RS} -G ${STDBY_RG} -P ${STDBY_PARFILE} check_stdby "> /dev/null 2>&1
				if [ ${?} -ne 0 ]
				then
					do_start=1
				fi
			else
				${SOCK} ${SSH} ${STDBY_USER}@${STDBY_HOST} ${MYDIR}/${MYNAME} -R ${STDBY_RS} -G ${STDBY_RG} -P ${STDBY_PARFILE} check_stdby ${OUTPUT} >/dev/null 2>&1
				if [ ${?} -ne 0 ]
				then
					do_start=1
				fi
	
			fi
			if [ $do_start -ne 0 ]
			then 
				# SCMSGS
				# @explanation
				# The PostgreSQL resource on the standby host is not 
				# configured as a standby or acting as a primary.
				# @user_action
				# Consult the logs on the standby host and insure, that
				# the standby host is configured as a standby database, 
				# if it is running as a primary, reconfigure and restart it.
				scds_syslog -p daemon.err -t $(syslog_tag) -m \
					"Function: start_pgs - resource %s on host %s is not configured/running as a standby database" \
					"${STDBY_RS}" "${STDBY_HOST}"
			else
				debug_message "Function: start_pgs - the resource ${STDBY_RS} on host ${STDBY_HOST} is configured/running as a standby database"
			fi
	
		else
			# SCMSGS
			# @explanation
			# The standby host is not up and running.
			# @user_action
			# Consult the logs on the standby host and insure, that
			# the standby host is answering on ping requests of the primary host.
			scds_syslog -p daemon.err -t $(syslog_tag) -m \
				"Function: start_pgs - the standby host %s is not answering on ping" \
				"${STDBY_HOST}"
			do_start=1
		fi
	fi

	# continue on the start process

	if [ ${do_start} -eq 0 ] 
	then
		
		# Start PostgreSql, the postmaster searches for databases in the directory of the PGDATA variable,
		# and it will listen on the port of the PGORT varaible. The logfile will be stored in the file name
		# contained in the PGLOGFILE variable.
		# The postmaster is started via the pg_ctl utility.
		#
	
		if [ -z "${SMF_FMRI}" ]
		then
			${SU} ${USER} -c " ${ENVSC} ${TASK_COMMAND} ${SOCK} ${LIBPATH} ${ENV} ${DATA} ${ENV} ${PORT} ${PGROOT}/bin/pg_ctl start -l ${PGLOGFILE} ${OUTPUT}" > /dev/null
			rc_start_command=$?
		else
	
			# start under the right user in an smf manifest you source a ksh script, regardless of the users login shell
	
			if [ -n "${ENVSCRIPT}" ]
			then
				. ${ENVSCRIPT}
			fi
	
			${SOCK} ${LIBPATH} ${ENV} ${DATA} ${ENV} ${PORT} ${PGROOT}/bin/pg_ctl start -l ${PGLOGFILE} >${LOGFILE} 2>&1
			rc_start_command=$?
		fi
		
		# if an ssh-agent was started, remember the postmaster pid in /tmp/${RESOURCE}-pid

		if [ -n "${SOCK}" ] && [ ${rc_start_command} -eq 0 ]
		then
			START_TIMEOUT=`${SCHA_RESOURCE_GET} -O START_TIMEOUT -R ${RESOURCE} -G ${RESOURCEGROUP}`
			MAX_START=`expr ${START_TIMEOUT} \* 70 \/ 100`
			
			# give PostgreSQL a chance to construct the ${PGDATA}/postmaster pid, it is 
			# done asynchronously

			PID_LINES=0
			while [ ${SECONDS} -lt ${MAX_START} ] && [ ${PID_LINES} -eq 0 ]
			do
				if [ -f ${PGDATA}/postmaster.pid ]
				then
					PID_LINES=`${WC} -l ${PGDATA}/postmaster.pid|${AWK} '{print $1}'`
				fi
				${SLEEP} 2
			done

			${RM} /tmp/${RESOURCE}-pid 2>/dev/null
			${HEAD} -1 ${PGDATA}/postmaster.pid >/tmp/${RESOURCE}-pid
		fi
	else
		rc_start_command=1

		# disable the pmf tag and run a sleep in the background, to assure, that there will be a valid pmftag during the start phase

		START_TIMEOUT=`${SCHA_RESOURCE_GET} -O START_TIMEOUT -R ${RESOURCE} -G ${RESOURCEGROUP}`
		${SLEEP} ${START_TIMEOUT} &
		${PMFADM} -s ${RESOURCEGROUP},${RESOURCE},0.svc

		# As a workaround until RFE 6629606 is implemented, kill the gds_svc start method
		# to speed up the transition into START_FAILED.
		# If gds_svc_start gets killed the resource transition into START_FAILED and does not
		# wait until the start_timeout expires.

		${PKILL} -u root -f "gds_svc_start .*-R ${RESOURCE} " >/dev/null
	fi

	debug_message "Function: start_pgs - End"
	return ${rc_start_command}
}

stop_pgs()
{
	#
	# Stop PostgreSQL
	#

        debug_message "Function: stop_pgs - Begin"
	${SET_DEBUG}

	# construct the necessary redirection and source variables

	set_shell_specifics

	# Define the appropriate newtask command.
	# The Project will be derived according to the call method,
	# either from the smf service or from the cluster resource / resource group

	srm_function ${USER}

	# construct the necessary environment variables

	LIBPATH=
	if [ -n "${LD_LIBRARY_PATH}" ]
	then
		LIBPATH="${ENV} LD_LIBRARY_PATH=${LD_LIBRARY_PATH}"
	fi

	PORT="PGPORT=${PGPORT}"
	DATA="PGDATA=${PGDATA}"

	# remember the process id of the parent postmaster

	pgrspid=`${HEAD} -1 ${PGDATA}/postmaster.pid`

	# remove the logfile, before creating it to survive potential noclobber settings

	${RM} ${LOGFILE} 2>/dev/null

	# Perform a fast shutdown first. The server is specified by the triple user, PGPORT and the PGDATA variable.
	# The fast shutdown will disconnect the clients and stop the server processes.

	# SCMSGS
	# @explanation
	# The PostgreSQL database server is shut down with the specified
	# option.
	# @user_action
	# None
	scds_syslog -p daemon.notice -t $(syslog_tag) -m \
             "stop_pgs: Stop PostgreSQL with the option %s " \
             "fast"
	
	if [ -z "${SMF_FMRI}" ]
	then
		${SU} ${USER} -c " ${ENVSC} ${TASK_COMMAND} ${LIBPATH} ${ENV} ${DATA} ${ENV} ${PORT} ${PGROOT}/bin/pg_ctl stop -m fast ${OUTPUT}" > /dev/null
		rc_stop_command=$?
	else

		# stop under the right user in a smf manifest

		if [ -n "${ENVSCRIPT}" ]
		then
			. ${ENVSCRIPT}
		fi

		${LIBPATH} ${ENV} ${DATA} ${ENV} ${PORT} ${PGROOT}/bin/pg_ctl stop -m fast >${LOGFILE} 2>&1
		rc_stop_command=$?
	fi

	# Determine if postgres is really stopped, if not perform an immediate shutdown.
	# The server is specified by the triple user, PGPORT and the PGDATA variable.
	# The immediate shutdown stops the server processes without disconnecting the clients.
	# The immediate shutdown will be performd only, if the postgres database is not configured as
	# a standby database, because then an immediate shutdown will not succeed in a standby configuration.
	
	
	if [ ! -f ${PGDATA}/recovery.conf ]
	then
		if ${PS} -u ${USER} -o pid,ppid |${GREP} -w ${pgrspid}>/dev/null
		then
			
			scds_syslog -p daemon.notice -t $(syslog_tag) -m \
		             "stop_pgs: Stop PostgreSQL with the option %s " \
		             "immediate"
			
			if [ -z "${SMF_FMRI}" ]
			then
				${SU} ${USER} -c "${ENVSC} ${TASK_COMMAND} ${LIBPATH} ${ENV} ${DATA} ${ENV} ${PORT} ${PGROOT}/bin/pg_ctl stop -m immediate ${OUTPUT_APP}" > /dev/null
				rc_stop_command=$?
			else
		
				# stop under the right user in a smf manifest
		
				if [ -n "${ENVSCRIPT}" ]
				then
					. ${ENVSCRIPT}
				fi
	
				${LIBPATH} ${ENV} ${DATA} ${ENV} ${PORT} ${PGROOT}/bin/pg_ctl stop -m immediate >>${LOGFILE} 2>&1
				rc_stop_command=$?
			fi
		fi
	else
		debug_message "Function: stop_pgs - It is a standby database, do not stop with immediate"
	fi

	# determine if postgres is really stopped, if not kill the parent of the postmaster with -9
	
	if ${PS} -u ${USER} -o pid,ppid |${GREP} -w ${pgrspid}>/dev/null
	then
		# SCMSGS
		# @explanation
		# The previous stop attempts for PostgreSQL failed. The server
		# is now killed with kill -9.
		# @user_action
		# None
		scds_syslog -p daemon.notice -t $(syslog_tag) -m \
	             "stop_pgs: Stop PostgreSQL process %s with kill -9 " \
	             "${pgrspid}"

		${KILL} -9 ${pgrspid}
		rc_stop_command=$?
	fi

	if [ -n "${SSH_PASSDIR}" ]
	then
		# stop the ssh-agent

		SSH_PID=
		if  [ -f /tmp/${RESOURCE}-ssh ]
		then
			debug_message "Function: stop_pgs - stop the ssh agent"

			# source /tmp/${RESOURCE}-ssh to get the variable SSH_AGENT_PID

			. /tmp/${RESOURCE}-ssh
			
			SSH_PID="${ENV} SSH_AGENT_PID=${SSH_AGENT_PID}"
	
			if [ -z "${SMF_FMRI}" ]
			then
				${SU} ${USER} -c "${TASK_COMMAND} ${SSH_PID} ${SSH_AGENT} -k ${OUTPUT_APP}"
				rc_stop_command=$?
			else
				${SSH_PID} ${SSH_AGENT} -k >>${LOGFILE}
				rc_stop_command=$?
			fi

			# if the ssh-agent process is still there, kill it with -9

			if ${PS} -p ${SSH_AGENT_PID} >/dev/null 2>&1
			then
				debug_message "Function: stop_pgs - The ssh-agent ${SSH_AGENT_PID} survived his stop request, kill it with -9"
				${KILL} -9 ${SSH_AGENT_PID}
			fi
		else
			# SCMSGS
			# @explanation
			# It can not be determined which ssh agent is working for the user/database,
			# it is now killed by PMF using the configured stop signal.
			# @user_action
			# None
			scds_syslog -p daemon.notice -t $(syslog_tag) -m \
		             "stop_pgs: Allow pmf to stop the remaining ssh-agent for the user %s" \
		             "${USER}"
		fi

	fi

	debug_message "Function: stop_pgs - End"
	return ${rc_stop_command}
}

check_pgs()
{
	# 
	# Probe PostgreSQL
	#

        debug_message "Function: check_pgs - Begin"
	${SET_DEBUG}

	rc_check_command=0

	# construct the necessary redirection and source variables

	set_shell_specifics

	# Define the appropriate newtask command.
	# The Project will be derived according to the call method,
	# either from the smf service or from the cluster resource / resource group

	srm_function ${USER}

	# determine the parent postmaster pid

	pgrspid=""
	if [ -f ${PGDATA}/postmaster.pid ]
	then
		pgrspid=`${HEAD} -1 ${PGDATA}/postmaster.pid`
	else

		# the pid store is created only if a ssh passphrase is defined
		# in this case get the pid from /tmp/${RESOURCE}-pid

		if [ -n "${SSH_PASSDIR}" ]
		then
			pgrspid=`${CAT} /tmp/${RESOURCE}-pid`
		fi
	fi

	# Determine if the gds start process is already completed, or the database start is far enough to allow connections
	# The criteria is the existence of the postmaster.pid file in the PGDATA directory.
	# As long as the GDS start is not complete, probe error messages will be suppressed.

	wait_for_online=0
	
	${PGREP} -u root -f "gds_svc_start .*-R ${RESOURCE} " >/dev/null
	if [ ${?} -eq 0 ]
	then
		debug_message "Function: check_pgs - wait for online detected" 
		wait_for_online=1
		if [ -n "${pgrspid}" ]
		then
			if ! ${PS} -u ${USER} -o pid,ppid |${GREP} -w ${pgrspid}>/dev/null
			then
				debug_message "Function: check_pgs - wait for online detected, postmaster is not running" 
				rc_check_command=100
			else
				debug_message "Function: check_pgs - wait for online detected, postmaster is running proceed with normal checks" 
			fi
		else
			debug_message "Function: check_pgs - wait for online detected, postmaster pid file ${PGDATA}/postmaster.pid not found" 
			rc_check_command=100
		fi
	fi

	# If a passphrase is defined a ssh agent is running together with the PostgreSQL deamon
	# each of the daemons satisfies pmf/smf, so we have to check manually for the processes to run.
	# If either the ssh-agent or the PostgreSQL daemons are not running, exit with 100.
	# This has to be done, because pmf/smf will not trigger a restart if only one of the process trees are gone.

	if [ -n "${SSH_PASSDIR}" ] && [ -n "${pgrspid}" ]
	then
		
		if ! ${PS} -u ${USER} -o pid,ppid |${GREP} -w ${pgrspid}>/dev/null
		then
			# SCMSGS
			# @explanation
			# The PostgreSQL process is not running.
			# @user_action
			# None
			scds_syslog -p daemon.err -t $(syslog_tag) -m \
	             		"check_pgs: The PostgreSQL process %s is not running" \
	             		"${pgrspid}"
			rc_check_command=100
			return ${rc_check_command}
				
		fi
		
		# Determine if the necessary ssh-agent is running. If PostgreSQL is running 
		# without the ssh-agent, the replication is not working, so we have to return 100

		if [ -f /tmp/${RESOURCE}-ssh ]
		then

			sshpid=`${GREP} SSH_AGENT_PID /tmp/${RESOURCE}-ssh|${AWK} -F= '{print $2}'`
			if ! ${PS} -u ${USER} -o pid,ppid |${GREP} -w ${sshpid}>/dev/null
			then
				# SCMSGS
				# @explanation
				# The ssh-agent process associated with the PostgreSQL database 
				# is not running.
				# @user_action
				# None
				scds_syslog -p daemon.err -t $(syslog_tag) -m \
		             		"check_pgs: The ssh-agent process %s is not running" \
		             		"${sshpid}"
				rc_check_command=100
				return ${rc_check_command}
			fi

		else

			# SCMSGS
			# @explanation
			# The file /tmp/<resourcename>-ssh containing the necessary information to manage 
			# the ssh-agent together with the PostgreSQL database is unavailable.
			# @user_action
			# None
			scds_syslog -p daemon.err -t $(syslog_tag) -m \
	             		"check_pgs: The ssh-agent information file %s is unavailable" \
	             		"/tmp/${RESOURCE}-ssh"
			rc_check_command=100
			return ${rc_check_command}

		fi
	
	fi

	# If there is no postmaster.pid during wait for online, or the database will not
	# accept updates, because it is configured as a standby database, terminate the probe.
	# To trigger an early exit of the probe, it is enough, that a recover.conf exists in $PGDATA.
	# So effectively the agent relies on process monitoring if a database is configured as a standby database.


	if [ ${rc_check_command} -ne 0 ] || [ -f ${PGDATA}/recovery.conf ]
	then
		debug_message "Function: check_pgs - End"
		return ${rc_check_command}
	fi

	# construct the necessary environment variables

	LIBPATH=
	if [ -n "${LD_LIBRARY_PATH}" ]
	then
		LIBPATH="${ENV} LD_LIBRARY_PATH=${LD_LIBRARY_PATH}"
	fi

	# the variable PASSWORD is set to the database user login passowrd

	PASSWORD=
	if [ -n "${SCPASS}" ]
	then
		PASSWORD="${ENV} PGPASSWORD=${SCPASS}"
	fi
	PORT="PGPORT=${PGPORT}"
	DATA="PGDATA=${PGDATA}"

	if [ -n "${PGHOST}" ]
	then
		HOST="${ENV} PGHOST=${PGHOST}"
	fi

	# remove the temporary output files before creating them to survive potential noclobber settings

	${RM} /tmp/${RESOURCE}-${USER}-cat-out 2>/dev/null
	${RM} /tmp/${RESOURCE}-${USER}-cat-err 2>/dev/null
	${RM} /tmp/${RESOURCE}-${USER}-tbl-out 2>/dev/null
	${RM} /tmp/${RESOURCE}-${USER}-tbl-err 2>/dev/null

	# construct the sql commands for the test

	DBCAT="select datname from pg_database"
        DBTRUNC="truncate ${SCTABLE}"
        DBINS="insert into ${SCTABLE} (sccol) values('hello im there')"
        DBSEL="select * from ${SCTABLE}"

	# check if the catalog of the postgres database is accessible

	debug_message "Function: check_pgs - check if the database catalog is accessible with ${SCDB} "

	${SU} ${USER} -c "${OPEN_BRACKET} ${TASK_COMMAND} ${LIBPATH} $HOST ${PASSWORD} ${ENV} ${DATA} ${ENV} ${PORT} ${PGROOT}/bin/psql -d ${SCDB} -U ${SCUSER} -c \"${DBCAT}\" ${CAT_OUTPUT} ${CLOSE_BRACKET} ${CAT_ERRPUT} " >/dev/null
	rc_check_command=$?

	# Return ${NOCONRET} if the connect fails, otherwise proceed with the check

	if [ ${rc_check_command} -ne 0 ]
	then

		debug_message "Function: check_pgs - connect to database ${SCDB} failed"

		if [ ${wait_for_online} -eq 0 ]
		then
			cp /tmp/${RESOURCE}-${USER}-cat-err ${LOGFILE}
			log_message err "check_pgs: rc<${NOCONRET}>"
		fi

		debug_message "Function: check_pgs - End"
		rc_check_command=${NOCONRET} 

	else

		# check if the test database is in the database catalog

		if ! ${EGREP} " ${SCDB}$| ${SCDB} " /tmp/${RESOURCE}-${USER}-cat-out >/dev/null 2>&1
		then

			debug_message "Function: check_pgs - The test database ${SCDB} is not in the database catalog"

			if [ ${wait_for_online} -eq 0 ]
			then
				# SCMSGS
				# @explanation
				# The database to monitor does not exist in
				# the database catalog.
				# @user_action
				# Fix the parameter file to contain the right
				# database name in the SCDB variable, or
				# prepare the database.
				scds_syslog -p daemon.err -t $(syslog_tag) -m \
		             		"check_pgs: The test database %s is not in the database catalog" \
		             		"${SCDB}"
			fi

			rc_check_command=100
		else
			debug_message "Function: check_pgs - check if the database catalog is accessible with ${SCDB} was successful"
		fi
	fi

	# manipulate the table SCTABLE and exit 100 if the inserted string is not there, exit 10 if the connection to
	# the database fails

	if [ ${rc_check_command} -eq 0 ]
	then

		debug_message "Function: check_pgs - manipulate the table ${SCTABLE}"

		${SU} ${USER} -c "${OPEN_BRACKET} ${TASK_COMMAND} ${LIBPATH} $HOST ${PASSWORD} ${ENV} ${DATA} ${ENV} ${PORT} ${PGROOT}/bin/psql -d ${SCDB} -U ${SCUSER} -c \"${DBTRUNC};${DBINS};${DBSEL}\" ${TBL_OUTPUT} ${CLOSE_BRACKET} ${TBL_ERRPUT} " >/dev/null
		rc_check_command=$?

		# Return ${NOCONRET} if the connect fails, otherwise proceed with the check

		if [ ${rc_check_command} -ne 0 ]
		then

			debug_message "Function: check_pgs - connect to database ${SCDB} failed"

			if [ ${wait_for_online} -eq 0 ]
			then
				cp /tmp/${RESOURCE}-${USER}-tbl-err ${LOGFILE}
				log_message err "check_pgs: rc<${NOCONRET}>"
			fi

			rc_check_command=${NOCONRET}

		else

			# check if the test table could be manipulated successfully


			if ! ${GREP} "hello im there" /tmp/${RESOURCE}-${USER}-tbl-out >/dev/null 2>&1
			then

				if [ ${wait_for_online} -eq 0 ]
				then
					# SCMSGS
					# @explanation
					# The monitoring action on the
					# specified table failed.
					# @user_action
					# None
					scds_syslog -p daemon.err -t $(syslog_tag) -m \
			             		"check_pgs: The test database manipulation failed for database %s, user %s and table %s" \
			             		"${SCDB}" "${SCUSER}" "${SCTABLE}"
				fi

				rc_check_command=100
				debug_message "Function: check_pgs - manipulation of the table ${SCTABLE} failed"

			else

				debug_message "Function: check_pgs - manipulation of the table ${SCTABLE} successful"

			fi
		fi
	fi	

	debug_message "Function: check_pgs - End"
	return ${rc_check_command}
}

check_stdby()
{
	# 
	# Check if the current postgres database is configured or running as a standby database.
	# This function is called from the start command of the remote host.
	# The error messages are processed by scds_syslog, and they are transported via
	# standard out to the remote host.
	#
	# This function will generate up to two error messages in the postgres logfile.

        debug_message "Function: check_stdby - Begin"
	${SET_DEBUG}

	rc_check_stdby_command=0
	if [ -f ${PGDATA}/recovery.conf ]
	then

		# We are running as the PostgreSQL user, so exporting the environment is sufficient.

		export PGPORT

		# The variable PASSWORD is set to the database user login password.

		if [ -n "${SCPASS}" ]
		then
			export PGPASSWORD=${SCPASS}
		fi

		if [ -n "${PGHOST}" ]
		then
			export PGHOST
		fi
		export PGPORT
		export LD_LIBRARY_PATH
		
		# check the database if it accepts inserts

		${PGROOT}/bin/psql -d ${SCDB} -U ${SCUSER} -c "insert into ${SCTABLE} (sccol) values('standby test');" >/dev/null 2>&1
		if [ ${?} -eq 0 ]
		then
			# SCMSGS
			# @explanation
			# The database is running as a primary database, but is configured to be a standby.
			# @user_action
			# If you want to start the old primary database, reload the primary and restart 
			# the standby database.
			scds_syslog -p daemon.err -t $(syslog_tag) -m \
	             		"check_stdby: The database in %s is not running as a standby database, reload the primary and restart the standby database." \
	             		"${PGDATA}" 
			rc_check_stdby_command=1

		else

			# Check if the rolchanger is currently doing his work.

			if [ -f /tmp/${ROLECHG_RS}_rolechg.lck ]
			then

				# SCMSGS
				# @explanation
				# The database is running as a standby database, but is currently reconfigured to run as a primary.
				# @user_action
				# If you want to start the old primary database, reload the primary and restart 
				# the standby database.
				scds_syslog -p daemon.err -t $(syslog_tag) -m \
	             			"check_stdby: The database in %s is reconfigured to run as a primary database, reload the primary and restart the standby database." \
	             			"${PGDATA}" 
				rc_check_stdby_command=1
			else
				debug_message "Function: check_stdby - the database in ${PGDATA} is configured or running as a standby database"
			fi

		fi
		
	else

		# SCMSGS
		# @explanation
		# The database is not configured as a standby database.
		# @user_action
		# Create an appropriate recovery.conf command, reload the primary 
		# and restart the standby database.
		scds_syslog -p daemon.err -t $(syslog_tag) -m \
             		"check_stdby: The database in %s is not configured as a standby database, create the recovery.conf command" \
             		"${PGDATA}" 
		rc_check_stdby_command=1

	fi

	debug_message "Function: check_stdby - End"
	return ${rc_check_stdby_command}
}
