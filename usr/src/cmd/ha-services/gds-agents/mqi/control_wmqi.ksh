#!/usr/bin/ksh 
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident   "@(#)control_wmqi.ksh 1.6     07/08/07 SMI"
# 
# Usage GDS: <options> <parameter1> <parameter2>
#
# Usage SMF: <parameter1> <parameter2> <parameter3>
# 
#	<options>: -R <resource> -G <resourcegroup> etc.
#	parameter1: start | stop | test 
#	parameter2: broker | uns

MYNAME=`basename $0`
MYDIR=`dirname $0`

typeset opt

while getopts 'R:G:Q:I:O:U:B:D:S:E:' opt
do
	case "${opt}" in
		R)      RESOURCE=${OPTARG};;
		G)      RESOURCEGROUP=${OPTARG};;
		Q)      QMGR=${OPTARG};;
		I)      IN_QUEUE=${OPTARG};;
		O)      OUT_QUEUE=${OPTARG};;
		U)      MQSIUSER=${OPTARG};;
		B)      BROKER=${OPTARG};;
		D)      DBUSER=${OPTARG};;
		S)      START_COMMAND=${OPTARG};;
		E)      STOP_COMMAND=${OPTARG};;
		*)      exit 1;;
	esac
done

if [ "${OPTIND}" -gt 1 ]
then
	# Called by GDS
	CALLER=GDS

	shift $((${OPTIND} -1))
else
	# Called by SMF
	exit 1
fi

METHOD=${1}
[ "${2}" = "broker" ] && COMPONENT=sib
[ "${2}" = "uns" ] && COMPONENT=siu

. ${MYDIR}/../${COMPONENT}/etc/config

[ -z "${MQSIUSER}" ] && MQSIUSER=mqm
[ -z "${BROKER}" ] && BROKER=UserNameServer

if [ "${CALLER}" = "GDS" ]
then
	. ${MYDIR}/functions
	
	# Perform all the scha* calls
	TASK_COMMAND=""

	if [ "${METHOD}" = "stop" ]
	then
	   STOP_TIMEOUT=`/usr/cluster/bin/scha_resource_get -O Stop_timeout \
	      -R ${RESOURCE} -G ${RESOURCEGROUP} `
	fi

	# Determine the newtask project for start and stop

	if [ "${METHOD}" != "test" ]
	then
	   if [ `/usr/bin/uname -r` != "5.8" ]
	   then
	      # Retrieve the resource project name
	      RESOURCE_PROJECT_NAME=`/usr/cluster/bin/scha_resource_get \
		-R ${RESOURCE} -G ${RESOURCEGROUP} -O RESOURCE_PROJECT_NAME`
		
	      if [ -z "${RESOURCE_PROJECT_NAME}" -o "${RESOURCE_PROJECT_NAME}" = "default" ]
	      then
		# Retrieve the resource group project name 
		RESOURCE_PROJECT_NAME=`/usr/cluster/bin/scha_resourcegroup_get \
		   -G ${RESOURCEGROUP} -O RG_PROJECT_NAME`
	      fi	
	   fi

	   # Validate that ${MQSIUSER} belongs to the ${RESOURCE_PROJECT_NAME}
	   if [ "${RESOURCE_PROJECT_NAME}" ]
	   then
	      PROJ_MEMBER=`/usr/bin/projects ${MQSIUSER} | /usr/bin/egrep "^${RESOURCE_PROJECT_NAME} | ${RESOURCE_PROJECT_NAME} | ${RESOURCE_PROJECT_NAME}$|^${RESOURCE_PROJECT_NAME}$"`

	      if [ -z "${PROJ_MEMBER}" ]
	      then
		scds_syslog -p daemon.notice -t $(syslog_tag) -m \
		   "%s - The user %s does not belongs to project %s" \
		   "${MYNAME}" "${MQSIUSER}" "${RESOURCE_PROJECT_NAME}"
	      else
		TASK_COMMAND="/usr/bin/newtask -p ${RESOURCE_PROJECT_NAME}"
	      fi
	   fi
	fi
else
	exit 1
fi

debug_message "Method: ${MYNAME} - Begin"
${SET_DEBUG}

[ -x /sbin/zonename ] && ZONENAME=`/sbin/zonename`

set_redirection

case "${METHOD}" in
	start)	validate
		rc=$?
		[ "${rc}" -ne 0 ] && return ${rc}
		start_broker
		rc=$?
		;;

	stop)	stop_broker
		rc=$?
		;;

	test)
	   case "${COMPONENT}" in
		sib) 	check_start "start-broker stop-broker"
			rc=$?
			[ "${rc}" -eq 100 ] && return ${rc}
			
			# There are some dependencies that need to be observed in the event
			# of either the Broker, Broker Queue Manager or Broker RDBMS being
			# restarted, which are.
			#
			#       Failure         Intended Action         Actual Action
			#       -------         ---------------         -------------
			#       Broker          Broker Start            SC Broker Resource Restarted
			#
			#       Broker QMGR     Broker Stop             SC QMGR Resource Restarted
			#                       Broker QMGR Start       SC Broker Resource Restarted
			#                       Broker Start            
			#
			#       Broker RDBMS    Broker Stop             SC RDBMS Resource Restarted
			#                       Broker QMGR Stop	SC QMGR Resource Restarted
			#                       Broker RDBMS Start	SC Broker Resource Restarted
			#                       Broker QMGR Start
			#                       Broker Start
			#
			# The above is achieved using Resource_dependencies_restart, i.e.
			#
			# scrgadm -c -j <BROKER resource> -y Resource_dependencies_restart=<QMGR resource>
			# scrgadm -c -j <QMGR resource> -y Resource_dependencies_restart=<RDBMS resource>

			check_broker
			rc=$?
			[ "${rc}" -ne 0 ] && rc=100
			;;
		siu)	check_start "start-uns stop-uns"
			rc=$?
			[ "${rc}" -eq 100 ] && return ${rc}

			query_pids "bipservice"
			rc=$?
			[ "${rc}" -ne 0 ] && rc=100
			;;
	   esac
	   ;;
esac

debug_message "Method: ${MYNAME} - End"
exit $rc
