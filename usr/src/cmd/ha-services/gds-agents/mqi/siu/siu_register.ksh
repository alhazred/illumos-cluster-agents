#!/usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident   "@(#)siu_register.ksh 1.3     07/08/07 SMI"
#

. `dirname $0`/siu_config

scrgadm -a -j ${RS} -g ${RG} -t SUNW.gds \
-x Start_command="/opt/SUNWscmqi/siu/bin/start-uns \
-R '${RS}' -G '${RG}' -Q '${QMGR}' -U '${MQSI_ID}' \
-S '${START_CMD}' -E '${STOP_CMD}' " \
-x Stop_command="/opt/SUNWscmqi/siu/bin/stop-uns \
-R '${RS}' -G '${RG}' -Q '${QMGR}' -U '${MQSI_ID}' \
-S '${START_CMD}' -E '${STOP_CMD}' " \
-x Probe_command="/opt/SUNWscmqi/siu/bin/test-uns \
-R '${RS}' -G '${RG}' -Q '${QMGR}' -U '${MQSI_ID}' \
-S '${START_CMD}' -E '${STOP_CMD}' " \
-y Port_list=1414/tcp -y Network_resources_used=${LH} \
-x Stop_signal=9 \
-y Resource_dependencies=${QMGR_RS}
