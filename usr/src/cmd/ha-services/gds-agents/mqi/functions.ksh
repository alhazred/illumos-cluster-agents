#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident   "@(#)functions.ksh 1.6     07/08/07 SMI"
#

PKG=SUNWscmqi
MYNAME=`basename $0`
MYDIR=`dirname $0`
MYTMPDIR=/var/tmp
LOGFILE=${MYTMPDIR}/${RESOURCE}_logfile
TASK_COMMAND=""
PATTERN="\<${BROKER}\>"
RESOURCE_PROJECT_NAME=""
SCLOGGER=/usr/cluster/lib/sc/scds_syslog
LOGGER=/usr/bin/logger

syslog_tag()
{
	${SET_DEBUG}
	print "SC[${PKG:-??}.${COMPONENT:-??}.${METHOD:-??}]:${RESOURCEGROUP:-??}:${RESOURCE:-??}"
}

scds_syslog()
{
	if [ -f ${SCLOGGER} ]
	then
	   ${SCLOGGER} "$@" &
	else
	   while getopts 'p:t:m' opt
	   do
		case "${opt}" in
		   t) TAG=${OPTARG};;
		   p) PRI=${OPTARG};;
		esac
	   done
	
	   shift $((${OPTIND} - 1))
	   LOG_STRING=`/usr/bin/printf "$@"`
	   ${LOGGER} -p ${PRI} -t ${TAG} ${LOG_STRING}
	fi
}

debug_message()
{
	if [ "${DEBUG}" = "${RESOURCE}" -o "${DEBUG}" = "ALL" ]
	then
	   SET_DEBUG="set -x"
	   DEBUG_TEXT=${1}

	   scds_syslog -p daemon.debug -t $(syslog_tag) -m \
	      "%s" "${DEBUG_TEXT}"
	else
	   SET_DEBUG=
	fi
}

log_message()
{
	debug_message "Function: log_message - Begin"
	${SET_DEBUG}

	if [ -s "${LOGFILE}" ]
	then
	   PRIORITY=$1
	   HEADER=$2

	   strings ${LOGFILE} > ${LOGFILE}.copy

	   while read MSG_TXT
	   do
		scds_syslog -p daemon.${PRIORITY} -t $(syslog_tag) -m \
		   "%s - %s" \
		   "${HEADER}" "${MSG_TXT}"
	   done < ${LOGFILE}.copy

	   cat /dev/null > ${LOGFILE}
	fi

	debug_message "Function: log_message - End"
}

set_redirection()
{
	debug_message "Function: set_redirection - Begin"
	${SET_DEBUG}

	if /usr/bin/getent passwd ${MQSIUSER} | /usr/bin/awk -F: '{print $7}' | /usr/bin/grep csh > /dev/null
	then
	   OUTPUT=">& ${LOGFILE}"
	else
	   OUTPUT="> ${LOGFILE} 2>&1"
	fi

	debug_message "Function: set_redirection - End"
}

query_pids()
{
	debug_message "Function: query_pids - Begin"
	${SET_DEBUG}

	# Usage: query_pids "parameter"
	# 
	# "parameter" is passed to /usr/xpg4/bin/grep
	#
	# If being run on Solaris 10, query_pids() matches the 
	# correct zonename, otherwise zones are ignored for
	# pre-Solaris 10 systems.
	# 
	# query_pids() matches against the correct broker name
	# and will exclude "broker.name" names, i.e. exclude
	# a similar broker name that uses the "." notation.

	rc=1

	EXCLUDE=
	PROCESS=
	[ "${1}" ] && PROCESS="| /usr/xpg4/bin/grep ${1}"

	# If ${BROKER} is not a "broker.name" notation then 
	# exclude any ${BROKER}"." and ${BROKER}"!" entries

	if ! /usr/bin/echo ${BROKER} | /usr/bin/grep '\.' > /dev/null
	then
	   EXCLUDE="| /usr/xpg4/bin/grep -v -E '${BROKER}\.|${BROKER}\!'"
	fi

	if [ -x /sbin/zonename ]
	then
	   pids=`/usr/bin/ps -u ${MQSIUSER} -o pid,args,zone | /usr/bin/grep " ${ZONENAME}$" | \
		eval /usr/xpg4/bin/grep -E '${PATTERN}' ${EXCLUDE} ${PROCESS} | /usr/bin/awk '{print $1}'`
	else
	   pids=`/usr/bin/ps -u ${MQSIUSER} -o pid,args | \
		eval /usr/xpg4/bin/grep -E '${PATTERN}' ${EXCLUDE} ${PROCESS} | /usr/bin/awk '{print $1}'`
	fi

	[ "${pids}" ] && rc=0

	debug_message "Function: query_pids - End"
	return ${rc}
}

validate()
{
	debug_message "Function: validate - Begin"
	${SET_DEBUG}
	
	rc=0
	
	if /usr/bin/cat /etc/group | /usr/bin/awk -F: '{print $1}' | /usr/bin/grep mqbrkrs > /dev/null
	then
	   debug_message "Validate - Group mqbrkrs exists"
	else
	   # SCMSGS
	   # @explanation
	   # The WebSphere MQ Broker resource failed to validate that the
	   # group mqbrkrs exists.
	   # @user_action
	   # Ensure that the group mqbrkrs exists.
	   scds_syslog -p daemon.error -t $(syslog_tag) -m \
		"Validate - Group mqbrkrs does not exist"
	   rc=1
	fi

	pgroup=`/usr/bin/grep "^${MQSIUSER}:" /etc/passwd | /usr/bin/awk -F: '{print $4}'`
	
	if [ ! -z ${pgroup} ]
	then
	   if /usr/bin/cat /etc/group | /usr/bin/awk -F: '{ if ($1 == "mqm") print $3}' | /usr/bin/grep "^${pgroup}$" > /dev/null
	   then
		debug_message "Validate - Primary group for userid ${MQSIUSER} is mqm"
	   else
		if /usr/bin/cat /etc/group | /usr/bin/awk -F: '{ if ($1 == "mqm") print $4}' | /usr/bin/grep ${MQSIUSER} > /dev/null
		then
		   debug_message "Validate - Secondary group for userid ${MQSIUSER} is mqm"
		else
		   # SCMSGS
		   # @explanation
		   # The specified userid is not a member of group mqm.
		   # @user_action
		   # The specified user must be a member of grouo mqm.
		   scds_syslog -p daemon.error -t $(syslog_tag) -m \
			"Validate - Userid %s is not a member of group mqm" \
			"${MQSIUSER}"
		   rc=1
		fi
	   fi

	   if /usr/bin/cat /etc/group | /usr/bin/awk -F: '{ if ($1 == "mqbrkrs") print $3}' | /usr/bin/grep "^${pgroup}$" > /dev/null
	   then
		debug_message "Validate - Primary group for userid $MQSIUSER is mqbrkrs"
	   else
		if /usr/bin/cat /etc/group | /usr/bin/awk -F: '{ if ($1 == "mqbrkrs") print $4}' | /usr/bin/grep ${MQSIUSER} > /dev/null
		then
		   debug_message "Validate - Secondary group for userid ${MQSIUSER} is mqbrkrs"
		else
		   scds_syslog -p daemon.error -t $(syslog_tag) -m \
			"Validate - Userid %s is not a member of group mqm" \
			"${MQSIUSER}"
		   rc=1
		fi
	   fi
	else
	   # SCMSGS
	   # @explanation
	   # The specified userid does not exist within /etc/passwd.
	   # @user_action
	   # Ensure that the userid has been added.
	   scds_syslog -p daemon.error -t $(syslog_tag) -m \
		"Validate - Userid %s is not a valid userid" \
		"${MQSIUSER}"
	   rc=1
	fi

	if [ -d /opt/mqsi -a -d /var/mqsi ]
	then
	   debug_message "Validate - WebSphere MQ Broker file systems ok"
	else
	   # SCMSGS
	   # @explanation
	   # The WebSphere MQ Broker file systems (/opt/mqsi and /var/mqsi)
	   # are not defined.
	   # @user_action
	   # Ensure that the WebSphere MQ Broker file systems are defined
	   # correctly.
	   scds_syslog -p daemon.error -t $(syslog_tag) -m \
		"Validate - WebSphere MQ Broker file systems not defined"
	   rc=1
	fi

	debug_message "Function: validate - End"
	return ${rc}
}

check_start()
{
	debug_message "Function: check_start - Begin"
	${SET_DEBUG}

	# Allow "wait_for_online" to only test after the
	# specified method for ${RESOURCE} has finished.

	rc=1

	# Here $1 is set when check_start is called from within control-wmqi, e.g.
	# check_start "start-broker stop-broker" or check_start "start-uns stop-uns"

	for component in $1
	do
	   if [ "${CALLER}" = "GDS" ]
	   then
		if [ -x /sbin/zonename ]
		then
		   /usr/bin/pgrep -z ${ZONENAME} -f "${component} .*-R ${RESOURCE} " >/dev/null 2>&1
		else
		   /usr/bin/pgrep -u root -f "${component} .*-R ${RESOURCE} " >/dev/null 2>&1
		fi 
	   fi

	   rc=$?

	   if [ "${rc}" -eq 0 ]
	   then
		rc=100
		break
	   else
		rc=0
	   fi
	done

	debug_message "Function: check_start - End"
	return ${rc}
}

start_broker()
{
        # Start the Broker
	#
	# Here, we test for the existence of some semaphores
	#
	#  (a) MQSeriesIntegrator2BrokerResourceTableLockSempahore
	#  (b) MQSeriesIntegrator2RetainedPubsTableLockSemaphore
	# 
	# If either (a) or (b) exists and their respective 
	# semaphore id is not present on that node then we remove 
	# that lock file entry. This prevents 
	#  
	#  (a) Execution Group termination on startup with BIP2123
	#  (b) bipbroker termination on startup with BIP2088 
	#

	debug_message "Function: start_broker - Begin"
	${SET_DEBUG}

	/usr/bin/rm ${LOGFILE}

	# Test the MQSI Semaphores

	for semaphore in BrokerResourceTableLockSempahore RetainedPubsTableLockSemaphore
	do
	   if [ -r /var/mqsi/locks/MQSeriesIntegrator2${semaphore} ]
	   then
		semid_with_zeros=`/usr/bin/cat /var/mqsi/locks/MQSeriesIntegrator2${semaphore}`
		semid=`expr $semid_with_zeros / 1`

		if /usr/bin/ipcs -s | /usr/bin/grep $semid > /dev/null
		then
		   continue
		else
		   # SCMSGS
		   # @explanation
		   # The WebSphere Broker fault monitor checks to see if
		   # MQSeriesIntegrator2BrokerResourceTableLockSempahore or
		   # MQSeriesIntegrator2RetainedPubsTableLockSemaphore exists
		   # within /var/mqsi/locks and that their respective
		   # semaphore id exists.
		   # @user_action
		   # No user action is needed. If either MQSeriesIntegrator2%s
		   # file exists without an IPC semaphore entry, then the
		   # MQSeriesIntegrator2%s file is deleted. This prevents (a)
		   # Execution Group termination on startup with BIP2123 and
		   # (b) bipbroker termination on startup with BIP2088.
		   scds_syslog -p daemon.notice -t $(syslog_tag) -m \
			"MQSeriesIntegrator2%s exists without an IPC semaphore entry" \
			"${semaphore}"
		
		   # SCMSGS
		   # @explanation
		   # The WebSphere Broker fault monitor checks to see if
		   # MQSeriesIntegrator2BrokerResourceTableLockSempahore or
		   # MQSeriesIntegrator2RetainedPubsTableLockSemaphore exists
		   # within /var/mqsi/locks and that their respective
		   # semaphore id exists.
		   # @user_action
		   # No user action is needed. If either MQSeriesIntegrator2%s
		   # file exists without an IPC semaphore entry, then the
		   # MQSeriesIntegrator2%s file is deleted. This prevents (a)
		   # Execution Group termination on startup with BIP2123 and
		   # (b) bipbroker termination on startup with BIP2088.
		   scds_syslog -p daemon.notice -t $(syslog_tag) -m \
			"MQSeriesIntegrator2%s file deleted" \
			"${semaphore}"

		   /usr/bin/rm /var/mqsi/locks/MQSeriesIntegrator2${semaphore}
		fi
	   fi
	done
		
	if [ "${CALLER}" = "GDS" ]
	then
	   /usr/bin/su - ${MQSIUSER} -c "${TASK_COMMAND} env SC3_COMMAND=${START_COMMAND} mqsistart ${BROKER} ${OUTPUT}" > /dev/null
	fi

	rc=$?

	debug_message "Function: start_broker - End"
	return ${rc}
}

stop_broker()
{
	# Orderly stop of services

	debug_message "Function: stop_broker - Begin"
	${SET_DEBUG}

	for func in stop_broker_immediate cleanup_broker
	do
	   case ${func} in
		stop_broker_immediate)	((LOOP_TIME=$STOP_TIMEOUT *30/100));;
		cleanup_broker)		((LOOP_TIME=$STOP_TIMEOUT *5/100));;
	   esac

	   # Bail out of running mqsistop if bipservice is lost

	   if [ "${func}" = "stop_broker_immediate" ]
	   then
		if query_pids "-w bipservice"
		then
	   	   ${func}

	   	   while (( ${LOOP_TIME} > 0 ))
		   do
			# Match only broker and effective id and exclude all mqsi<commands>

			if query_pids "-v mqsi"
			then
			   /usr/bin/sleep 1
			   LOOP_TIME=`expr ${LOOP_TIME} - 1`
			else
			   break
			fi
	   	   done

	   	   # Log any mqsistop messages
	   	   log_message notice mqsistop

	   	   if ! query_pids "-v mqsi"
	   	   then
			break
		   fi
		fi
	   else
		${func}
	   fi
	done

	debug_message "Function: stop_broker - End"
	return 0
}

stop_broker_immediate()
{
	# Immediately stop the Broker

	debug_message "Function: stop_broker_immediate - Begin"
	${SET_DEBUG}

	if [ "${CALLER}" = "GDS" ]
	then
	   /usr/bin/su - ${MQSIUSER} -c "${TASK_COMMAND} env SC3_COMMAND=${STOP_COMMAND} mqsistop -i ${BROKER} ${OUTPUT} &" > /dev/null
	fi

	debug_message "Function: stop_broker_immediate - End"
}

cleanup_broker()
{
	# Cleanup the MQSI Broker

	debug_message "Function: cleanup_broker - Begin"
	${SET_DEBUG}
	
	# query_pids matches against the correct ${BROKER}, which can be
	# a Broker, UserNameServer or Configuration Manager. Only a matched
	# process name for the correct ${BROKER} is killed.

	pids="bipservice bipbroker DataFlowEngine biphttplistener bipconfigmgr bipuns"

	for pid in ${pids}
	do
	   query_pids "-w ${pid}"
	   [ "${pids}" ] && /usr/bin/kill -9 ${pids}
	done

	# SCMSGS
	# @explanation
	# The WebSphere MQ Broker has been successfully stopped.
	# @user_action
	# No user action is needed.
	scds_syslog -p daemon.notice -t $(syslog_tag) -m \
	   "All WebSphere MQ Broker processes stopped"

	debug_message "Function: cleanup_broker - End"
}

check_broker()
{
	# A simple message flow ensures that the Broker is
	# working properly. Here we simply test the following
	#
	# 	a. Check $IN_QUEUE and $OUT_QUEUE are valid
	# 	b. Empty out $OUT_QUEUE
	# 	c. Put a test message to $IN_QUEUE
	# 	d. Verify the message flow from $IN_QUEUE to $OUT_QUEUE
	#
	# If any of these programs (sc3inq, sc3get, sc3put) fail
	# then it indicates either a Queue Manager problem or an
	# Broker problem. In either case we return a restart
	# request via rc=1. This will cause the Broker to get
	# restarted.

	debug_message "Function: check_broker - Begin"
	${SET_DEBUG}

	rc=0

	# Allow NONE entries for $IN_QUEUE and $OUT_QUEUE 

	if [ "${IN_QUEUE}" = "NONE" ] && [ "${OUT_QUEUE}" = "NONE" ]
	then
	   # Only if bipservice is running

	   query_pids "bipservice"
	   rc=$?

	   debug_message "Function: check_broker - End"
	   return ${rc}
	fi

	# Check ${IN_QUEUE} and ${OUT_QUEUE} are valid and empty them out
	
	for q in ${IN_QUEUE} ${OUT_QUEUE} 
	do
	   if ERR_LOGFILE=`/usr/bin/su ${MQSIUSER} -c "/opt/SUNWscmqi/sib/bin/sc3get ${q} ${QMGR}"`
	   then
		debug_message "Function: check_broker - sc3get ${q} OK"
	   else
		log_message error "check_broker - sc3get ${q}"
		rc=1
	   fi
	done

	# Put a test message to $IN_QUEUE

	if ERR_LOGFILE=`/usr/bin/echo "Testing message flow from ${IN_QUEUE} to ${OUT_QUEUE}" | \
	   /usr/bin/su ${MQSIUSER} -c "/opt/SUNWscmqi/sib/bin/sc3put ${IN_QUEUE} ${QMGR}"`
	then
	   debug_message "Function: check_broker - sc3put ${IN_QUEUE} OK"
	else
	   log_message error "check_broker - sc3put ${IN_QUEUE}"
	   rc=1
	fi
		
	# Verify the message flow from ${IN_QUEUE} to ${OUT_QUEUE}

	# Allow some time for the message flow
	sleep 2

	if ERR_LOGFILE=`/usr/bin/su ${MQSIUSER} -c "/opt/SUNWscmqi/sib/bin/sc3inq ${OUT_QUEUE} ${QMGR}"`
	then
	   # Test that Current queue depth = 1
	   CURDEPTH=`/usr/bin/echo ${ERR_LOGFILE} | /usr/bin/awk -F= '{print $2}'`

	   if [ "${CURDEPTH}" -eq 1 ]	
	   then
		debug_message "Function: check_broker - sc3inq ${OUT_QUEUE} OK"
	   else
		# SCMSGS
		# @explanation
		# The WebSphere Broker fault monitor checks to see if the
		# message flow was successful, by inquiring on the current
		# queue depth for the output queue within the simple message
		# flow.
		# @user_action
		# No user action is needed. The fault monitor displays the
		# current queue depth until it successfully checks that the
		# simple message flow has worked.
		scds_syslog -p daemon.error -t $(syslog_tag) -m \
		   "check_broker - sc3inq %s CURDEPTH(%s)" \
		   "${OUT_QUEUE}" "${CURDEPTH}"

		rc=1 
	   fi
	else
		log_message error "check_broker - sc3inq ${OUT_QUEUE}"
		rc=1
	fi

	debug_message "Function: check_broker - End"
	return ${rc}
}
