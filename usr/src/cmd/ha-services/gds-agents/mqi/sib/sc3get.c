/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#pragma ident   "@(#)sc3get.c 1.3     07/08/07 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <cmqc.h>

int
main(int argc, char **argv)
{
	MQOD		od = {MQOD_DEFAULT};
	MQMD		md = {MQMD_DEFAULT};
	MQGMO		gmo = {MQGMO_DEFAULT};
	MQHCONN		Hcon;
	MQHOBJ		Hobj;
	MQLONG		CompCode;
	MQLONG		OpenCode;
	MQLONG		O_options;
	MQLONG		C_options;
	MQLONG		Reason;
	MQLONG		CReason;
	MQLONG		messlen;
	MQLONG		bufflen;
	char		buffer[101];
	char		QMName[50];

	if (argc < 2) {
		(void) printf("Missing Queue Name\n");
		exit(99);
	}

	if (argc < 3) {
		(void) printf("Missing Queue Manager Name \n");
		exit(99);
	}

	/* Setup the Object Descriptor */
	(void) strcpy(od.ObjectName, argv[1]);
	(void) strcpy(od.ObjectQMgrName, argv[2]);
	(void) strcpy(QMName, argv[2]);

	/* Connect to the Queue Manager */
	MQCONN(QMName,
	&Hcon,
	&CompCode,
	&CReason);

	if (CompCode == MQCC_FAILED) {
		(void) printf("MQCONN ended with reason code %ld\n", CReason);
		exit((int)CReason);
	}

	/* Open the Queue */

	O_options = MQOO_INPUT_AS_Q_DEF  + MQOO_FAIL_IF_QUIESCING;

	MQOPEN(Hcon,
	&od,
	O_options,
	&Hobj,
	&OpenCode,
	&Reason);

	if (Reason != MQRC_NONE) {
		(void) printf("MQOPEN ended with reason code %ld\n", Reason);
	}

	if (OpenCode == MQCC_FAILED) {
		(void) printf("unable to open queue for input\n");
	}

	/*
	 * Set the Message Descriptor and Get Message Options
	 *  md.Msgid		- No message id
	 *  md.CorrelId		- No correlation id
	 *  md.Encoding		- Native machine coding
	 *  md.CodedCharSetId	- Qmgr character set
	 *  gmo.Options		- convert but don't wait for a msg
	 *			accept truncated messages and
	 *			fail if the qmgr is quiescing
	 */

	md.Encoding	= MQENC_NATIVE;

	md.CodedCharSetId = MQCCSI_Q_MGR;

	gmo.Options = MQGMO_CONVERT + MQGMO_NO_WAIT
		    + MQGMO_ACCEPT_TRUNCATED_MSG
		    + MQGMO_FAIL_IF_QUIESCING;

	/* Get the message */

	while (CompCode != MQCC_FAILED) {
		bufflen = sizeof (buffer) - 1;

		(void) memcpy(md.MsgId, MQMI_NONE, sizeof (md.MsgId));

		(void) memcpy(md.CorrelId, MQCI_NONE, sizeof (md.CorrelId));

		MQGET(Hcon,
		Hobj,
		&md,
		&gmo,
		bufflen,
		buffer,
		&messlen,
		&CompCode,
		&Reason);

		if (Reason != MQRC_NONE) {
			if (Reason == MQRC_NO_MSG_AVAILABLE) {
				(void) printf("No more messages\n");
			}
			else
			{
				(void) printf("MQGET ended with rc %ld\n",
				Reason);

				if (Reason == MQRC_TRUNCATED_MSG_FAILED) {
					CompCode = MQCC_FAILED;
				}
			}
		}

		if (CompCode != MQCC_FAILED) {
			buffer[messlen] = '\0';
			(void) printf("message <%s>\n", buffer);
		}
	}

	/* Close the Queue */

	if (OpenCode != MQCC_FAILED) {
		C_options = MQCO_NONE;

		MQCLOSE(Hcon,
		&Hobj,
		C_options,
		&CompCode,
		&Reason);

		if (Reason != MQRC_NONE) {
			(void) printf("MQCLOSE ended with reason code %ld\n",
			Reason);
		}
	}

	/* Disconnect from the Queue Manager */

	if (CReason != MQRC_ALREADY_CONNECTED) {
		MQDISC(&Hcon,
		&CompCode,
		&Reason);

		if (Reason != MQRC_NONE) {
			(void) printf("MQCLOSE ended with reason code %ld\n",
			Reason);
		}
	}

	return (0);
}
