/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#pragma ident   "@(#)sc3put.c 1.3     07/08/07 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <cmqc.h>

int
main(int argc, char **argv)
{
	FILE		*fp;
	MQOD		od = {MQOD_DEFAULT};
	MQMD		md = {MQMD_DEFAULT};
	MQPMO		pmo = {MQPMO_DEFAULT};
	MQHCONN		Hcon;
	MQLONG		CompCode;
	MQLONG		Reason;
	MQLONG		CReason;
	MQLONG		messlen;
	char		buffer[100];
	char		QMName[50];

	if (argc < 2) {
		(void) printf("Missing Queue Name\n");
		exit(99);
	}

	if (argc < 3) {
		(void) printf("Missing Queue Manager Name \n");
		exit(99);
	}

	/* Setup the  Object Descriptor */
	(void) strcpy(od.ObjectName, argv[1]);
	(void) strcpy(od.ObjectQMgrName, argv[2]);
	(void) strcpy(QMName, argv[2]);

	/* Connect to the Queue Manager */
	MQCONN(QMName,
	&Hcon,
	&CompCode,
	&CReason);

	if (CompCode == MQCC_FAILED) {
		(void) printf("MQCONN ended with reason code %ld\n", CReason);
		exit((int)CReason);
	}

	/* Read one line of stdin and copy to buffer */

	fp = stdin;

	if (fgets(buffer, sizeof (buffer), fp) != NULL) {
		/* Replace new-line with null & reduce messlen  */
		messlen = (MQLONG)strlen(buffer);
		if (buffer[messlen-1] == '\n') {
			buffer[messlen-1]  = '\0';
			--messlen;
		}
	} else messlen = 0;

	/*
	 * Set the Message Descriptor and Put Message Options
	 *  md.MsgType		- Message doe not require a reply
	 *  md.Format		- Character string
	 *  md.Msgid		- No message id
	 *  md.CorrelId		- No correlation id
	 *  pmo.Options		- Fail if the qmgr is quiescing
	 */

	md.MsgType  = MQMT_DATAGRAM;

	(void) memcpy(md.Format, MQFMT_STRING, (size_t)MQ_FORMAT_LENGTH);

	(void) memcpy(md.MsgId, MQMI_NONE, sizeof (md.MsgId));

	(void) memcpy(md.CorrelId, MQCI_NONE, sizeof (md.CorrelId));

	pmo.Options = MQPMO_FAIL_IF_QUIESCING;

	/* Put the message */
	MQPUT1(Hcon,
	&od,
	&md,
	&pmo,
	messlen,
	buffer,
	&CompCode,
	&Reason);

	if (Reason != MQRC_NONE) {
		(void) printf("MQPUT1 ended with reason code %ld\n",
			Reason);
	}

	/* Disconnect from the Queue Manager */

	if (CReason != MQRC_ALREADY_CONNECTED) {
		MQDISC(&Hcon,
		&CompCode,
		&Reason);

	if (Reason != MQRC_NONE) {
		(void) printf("MQCLOSE ended with reason code %ld\n",
			Reason);
	}
	}

return (0);
}
