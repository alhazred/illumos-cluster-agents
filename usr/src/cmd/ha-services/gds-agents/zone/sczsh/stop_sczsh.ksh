#!/usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)stop_sczsh.ksh	1.5	07/06/06 SMI"
#

# Stops the shell-based service in a zone.  The paramters are passed in options.

MYNAME=`basename ${0}`
MYDIR=`dirname ${0}`

typeset opt

while getopts 'R:G:P:' opt
do
        case "${opt}" in
                R)      RESOURCE=${OPTARG};;
                G)      RESOURCEGROUP=${OPTARG};;
                P)      PARDIR=${OPTARG};;
                *)      exit 1;;
        esac
done

. ${MYDIR}/../etc/config		# debugging flags
. ${MYDIR}/functions			# agent specific functions
. ${MYDIR}/../../lib/functions		# functions common for all the agents
. ${MYDIR}/../../lib/functions_common	# functions common for all the zone components

debug_message "Method: ${MYNAME} - Begin"
${SET_DEBUG}

# check the syntax of the options

if ! validate_options
then
        debug_message "Method: ${MYNAME} - early End (Exit 1)"
        exit 1
fi

# source the sczsh specific parameter file
. ${PARFILE}

if stop_sczsh
then
        log_message notice "stop_sczsh rc<${rc_stop_sczsh}>"
else
        log_message err "stop_sczsh rc<${rc_stop_sczsh}>"
fi

debug_message "Method: ${MYNAME} - End (Exit ${rc_stop_sczsh})"
exit ${rc_stop_sczsh}
