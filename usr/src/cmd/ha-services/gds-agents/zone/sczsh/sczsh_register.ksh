#!/usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)sczsh_register.ksh	1.4	07/06/06 SMI"
#
#  This script has the optional -f option:
#  -f <filename> sources a user definable config file different from
#  sczsh_config.
#

MYCONFIG=""
MYNAME=`basename $0`

typeset opt

while getopts 'f:' opt
do
	case "${opt}" in
		f)      MYCONFIG=${OPTARG};;
		*)      echo "ERROR: ${MYNAME} Option ${OPTARG} unknown - early End. Only the option -f <filename> is valid."
			exit 1;;
	esac
done

if [ -n "${MYCONFIG}" ] && [ -f "${MYCONFIG}" ]
then
	 echo "sourcing ${MYCONFIG}"
	 . ${MYCONFIG}
else
	PKGCONF=`dirname $0`/sczsh_config
	echo "sourcing ${PKGCONF}"
	. ${PKGCONF}
fi

if [ ! -d ${PARAMETERDIR} ]; then
	echo "The value given for PARAMETERDIR (${PARAMETERDIR}) in sczsh_config is not a directory!"
	exit 1
fi

if [ ! -f ${PARAMETERDIR}/sczsh_${RS} ]; then
	cp /opt/SUNWsczone/sczsh/etc/pfile-header ${PARAMETERDIR}/sczsh_${RS}
	echo "# this parameterfile is automatically created by sczsh_register" >> ${PARAMETERDIR}/sczsh_${RS}
	echo "Zonename=\"${Zonename}\"" >>  ${PARAMETERDIR}/sczsh_${RS}
	echo "ServiceStartCommand=\"${ServiceStartCommand}\"" >>  ${PARAMETERDIR}/sczsh_${RS}
	echo "ServiceStopCommand=\"${ServiceStopCommand}\"" >>  ${PARAMETERDIR}/sczsh_${RS}
	echo "ServiceProbeCommand=\"${ServiceProbeCommand}\"" >>  ${PARAMETERDIR}/sczsh_${RS}
else
	echo "The parameterfile ${PARAMETERDIR}/sczsh_${RS} already exists! Will not overwrite and exit."
	exit 1
fi

/usr/cluster/bin/scrgadm -a -j ${RS} -g ${RG} -t SUNW.gds \
-x Start_command="/opt/SUNWsczone/sczsh/bin/start_sczsh \
-R ${RS} -G ${RG} -P ${PARAMETERDIR}" \
-x Stop_command="/opt/SUNWsczone/sczsh/bin/stop_sczsh \
-R ${RS} -G ${RG} -P ${PARAMETERDIR}" \
-x Probe_command="/opt/SUNWsczone/sczsh/bin/probe_sczsh \
-R ${RS} -G ${RG} -P ${PARAMETERDIR}" \
-x Stop_signal=9 -x Network_aware=false \
-x Failover_enabled=FALSE \
-y Failover_mode=NONE \
-y Resource_dependencies_restart=${SCZBT_RS}

St=$?

if [ "${St}" -ne 0 ]; then
        echo "Registration of resource ${RS} failed, please correct the wrong parameters."
        echo "Removing parameterfile ${PARAMETERDIR}/sczsh_${RS} for resource ${RS}."
	rm ${PARAMETERDIR}/sczsh_${RS}
        exit 1
else
        echo "Registration of resource ${RS} succeeded."
fi

# VALIDATE RESOURCE

/opt/SUNWsczone/sczsh/bin/validate_sczsh -R ${RS} -G ${RG} -P ${PARAMETERDIR}

St=$?

if [ "${St}" -ne 0 ]; then
	echo "Validation of resource ${RS} failed, check the syslog for the wrong parameters."
        echo "Removing resource ${RS} from the cluster configuration."
        /usr/cluster/bin/scrgadm -r -j ${RS}
        echo "Removing parameterfile ${PARAMETERDIR}/sczsh_${RS} for resource ${RS}."
	rm ${PARAMETERDIR}/sczsh_${RS}
        exit 1
else
        echo "Validation of resource ${RS} succeeded."
fi

exit 0
