#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)functions.ksh	1.7	09/01/20 SMI"
#

validate_options()
{
	debug_message "Function: validate_options - Begin"
	${SET_DEBUG}

	#
	# Ensure all options are set
	#
	rc_validate_options=0

	for opt in RESOURCE RESOURCEGROUP PARDIR
	do
		case ${opt} in
			RESOURCE)
			  if [ -z ${RESOURCE} ]; then
				# SCMSGS
				# @explanation
				# The referenced script needs a mandatory
				# option which got not set.
				# @user_action
				# Verify the options upon registration for the
				# referenced script and make sure all
				# mandatory options got provided.
				scds_syslog -p daemon.err -t $(syslog_tag) -m \
				  "validate_options: %s Option %s not set" \
				  "${MYNAME}" "-R"
				rc_validate_options=1
			  fi;;

			RESOURCEGROUP)
			  if [ -z ${RESOURCEGROUP} ]; then
				scds_syslog -p daemon.err -t $(syslog_tag) -m \
				  "validate_options: %s Option %s not set" \
				  "${MYNAME}" "-G"
				rc_validate_options=1
			  fi;;

			PARDIR)
			  if [ -z ${PARDIR} ]; then
				scds_syslog -p daemon.err -t $(syslog_tag) -m \
				  "validate_options: %s Option %s not set" \
				  "${MYNAME}" "-P"
				rc_validate_options=1
			  else
				# construct the variable PARFILE
				PARFILE=${PARDIR}/sczsh_${RESOURCE}
			  fi;;
		esac
	done

	debug_message "Function: validate_options returns ${rc_validate_options} - End"
	return ${rc_validate_options}
}

validate()
{
	#
	# Validate sczsh
	#
	
	debug_message "Function: validate - Begin"
	${SET_DEBUG}

	rc_validate=0

	#
	# Validate that prameter file exists and is syntactically correct
	#

	if ! val_parfile ${PARFILE} "Zonename ServiceStartCommand ServiceStopCommand ServiceProbeCommand" ${PARDIR}
	then
		debug_message "Function: validate - End"
		rc_validate=1
		return ${rc_validate}
	fi

	# test the semantic of the parameters

	. ${PARFILE}

	for param in `cat ${PARFILE} | grep -v "^#" | grep -v "^ " | nawk -F= '{print $1}'`
	do
		case ${param} in

			Zonename)
			  # is the zone name specified
			  if [ -z ${Zonename} ]; then
				scds_syslog -p daemon.err -t $(syslog_tag) -m \
				  "Function: validate - Zone name not set in %s" \
				  "${PARFILE}"
				rc_validate=1
			  else
				debug_message "Function: validate - Zone name (${Zonename}) set"
			  fi

			  # is the zone state valid 
			  get_zone_state ${Zonename}

			  if [ "${ZONE_STATE}" = "installed" -o "${ZONE_STATE}" = "ready" -o "${ZONE_STATE}" = "running" ]
			  then
				debug_message "Function: validate - ${Zonename} state is ${ZONE_STATE}"
			  else
				scds_syslog -p daemon.err -t $(syslog_tag) -m \
				  "Function: validate - %s state is %s" \
				  "${Zonename}" "${ZONE_STATE}"
				rc_validate=1
			  fi;;
			
			ServiceStartCommand)
			  # validate that the start command is called with full qualified path
			  echo ${ServiceStartCommand} | /usr/xpg4/bin/grep -q "^/"
			  if [ ${?} -ne 0 ]
			  then
				# SCMSGS
				# @explanation
				# The command specified for variable
				# ServiceStartCommand within the
				# /opt/SUNWsczone/sczsh/util/sczsh_config
				# configuration file is not containing the
				# full qualified path to it.
				# @user_action
				# Make sure the full qualified path is
				# specified for the ServiceStartCommand, e.g.
				# "/full/path/to/mycommand" rather than just
				# "mycommand". This full qualified path must
				# be accessible within the zone that command
				# is being called.
				scds_syslog -p daemon.err -t $(syslog_tag) -m \
				  "Function: validate - ServiceStartCommand (%s) not a fully qualified path." \
				  "${ServiceStartCommand}"
				rc_validate=1
			  else
				debug_message "Function: validate - ${ServiceStartCommand} is valid"
			  fi;;

			ServiceStopCommand)
			  # validate that the stop command is called with full qualified path
			  echo ${ServiceStopCommand} | /usr/xpg4/bin/grep -q "^/"
			  if [ ${?} -ne 0 ]
			  then
				# SCMSGS
				# @explanation
				# The command specified for variable
				# ServiceStopCommand within the
				# /opt/SUNWsczone/sczsh/util/sczsh_config
				# configuration file is not containing the
				# full qualified path to it.
				# @user_action
				# Make sure the full qualified path is
				# specified for the ServiceStopCommand, e.g.
				# "/full/path/to/mycommand" rather than just
				# "mycommand". This full qualified path must
				# be accessible within the zone that command
				# is being called.
				scds_syslog -p daemon.err -t $(syslog_tag) -m \
				  "Function: validate - ServiceStopCommand (%s) not a fully qualified path." \
				  "${ServiceStopCommand}"
				rc_validate=1
			  else
				debug_message "Function: validate - ${ServiceStopCommand} is valid"
			  fi;;

			ServiceProbeCommand)
			  # validate that the probe command is called with full qualified path
			  echo ${ServiceProbeCommand} | /usr/xpg4/bin/grep -q "^/"
			  if [ ${?} -ne 0 ]
			  then
				# SCMSGS
				# @explanation
				# The command specified for variable
				# ServiceProbeCommand within the
				# /opt/SUNWsczone/sczsh/util/sczsh_config
				# configuration file is not containing the
				# full qualified path to it.
				# @user_action
				# Make sure the full qualified path is
				# specified for the ServiceProbeCommand, e.g.
				# "/full/path/to/mycommand" rather than just
				# "mycommand". This full qualified path must
				# be accessible within the zone that command
				# is being called.
				scds_syslog -p daemon.err -t $(syslog_tag) -m \
				  "Function: validate - ServiceProbeCommand (%s) not a fully qualified path." \
				  "${ServiceProbeCommand}"
				rc_validate=1
			  else
				debug_message "Function: validate - ${ServiceProbeCommand} is valid"
			  fi;;
		esac
	done

	# Validate that we are in the global zone

	val_in_global
	if [ ${?} -ne 0 ]; then
		rc_validate=1
	fi

	debug_message "Function: validate - End"
	return ${rc_validate}
}


start_sczsh()
{
	#
	# Start sczsh
	#

	debug_message "Function: start_sczsh - Begin"
	${SET_DEBUG}

	rc_start_sczsh=0

	# validate that the command exists and is executable
	mystartcmd=`echo ${ServiceStartCommand} | awk '{print $1}'`

	if ! ${ZLOGIN} ${Zonename} /usr/bin/test -x ${mystartcmd}
	then
		# SCMSGS
		# @explanation
		# The command specified for variable ServiceStartCommand
		# within the /opt/SUNWsczone/sczsh/util/sczsh_config
		# configuration file is not executable or does not exist in
		# the specified zone.
		# @user_action
		# Make sure that the command specified for variable
		# ServiceStartCommand within the
		# /opt/SUNWsczone/sczsh/util/sczsh_config configuration file
		# is existing and executable for user root in the specified
		# zone. If you do not want to re-register the resource, make
		# sure the variable ServiceStartCommand is properly set within
		# the ${PARAMETERDIR}/sczsh_${RS} parameterfile.
		scds_syslog -p daemon.err -t $(syslog_tag) -m \
		  "Function: start_sczsh - %s does not exist or is not executable in zone %s - early End" \
		  "${mystartcmd}" "${Zonename}"

		rc_start_sczsh=100
		return ${rc_start_sczsh} 
	else
		debug_message "Function: validate - ${mystartcmd} is executable"
	fi

	# start the service
	( ${ZLOGIN} ${Zonename} ${ServiceStartCommand} ) > ${LOGFILE} 2>&1
	rc_start_sczsh=${?}

	# make sure there is a pid left for pmf until start_sczsh() does finish
	START_TIMEOUT=`${SCHA_RESOURCE_GET} -O START_TIMEOUT -R ${RESOURCE} -G ${RESOURCEGROUP}`
	/usr/bin/sleep ${START_TIMEOUT} &

	# disable pmf tag
	if ${PMFADM} -q ${RESOURCEGROUP},${RESOURCE},0.svc
	then
		${PMFADM} -s ${RESOURCEGROUP},${RESOURCE},0.svc
	fi
		
	debug_message "Function: start_sczsh - End"
	return ${rc_start_sczsh}
}

stop_sczsh()
{
	#
	# Stop sczsh
	#

	debug_message "Function: stop_sczsh - Begin"
	${SET_DEBUG}

	rc_stop_sczsh=0

	# validate that the command exists and is executable
	mystopcmd=`echo ${ServiceStopCommand} | awk '{print $1}'`

	if ! ${ZLOGIN} ${Zonename} /usr/bin/test -x ${mystopcmd}
	then
		# SCMSGS
		# @explanation
		# The command specified for variable ServiceStopCommand within
		# the /opt/SUNWsczone/sczsh/util/sczsh_config configuration
		# file is not executable or does not exist in the specified
		# zone.
		# @user_action
		# Make sure that the command specified for variable
		# ServiceStopCommand within the
		# /opt/SUNWsczone/sczsh/util/sczsh_config configuration file
		# is existing and executable for user root in the specified
		# zone. If you do not want to re-register the resource, make
		# sure the variable ServiceStopCommand is properly set within
		# the ${PARAMETERDIR}/sczsh_${RS} parameterfile.
		scds_syslog -p daemon.err -t $(syslog_tag) -m \
		  "Function: stop_sczsh - %s does not exist or is not executable in zone %s - early End" \
		  "${mystopcmd}" "${Zonename}"

		rc_stop_sczsh=100
		return ${rc_stop_sczsh} 
	else
		debug_message "Function: validate - ${mystopcmd} is executable"
	fi

	( ${ZLOGIN} ${Zonename} ${ServiceStopCommand} ) > ${LOGFILE} 2>&1
	rc_stop_sczsh=${?}

	# make sure the dummy sleep started in start_sczsh() did already
	# finish, otherwise kill it.
	if ${PMFADM} -q ${RESOURCEGROUP},${RESOURCE},0.svc
	then
		${PMFADM} -s ${RESOURCEGROUP},${RESOURCE},0.svc KILL 2> /dev/null
	fi

	debug_message "Function: stop_sczsh - End"
	return ${rc_stop_sczsh}
}

check_sczsh()
{
	# 
	# Probe sczsh
	#

        debug_message "Function: check_sczsh - Begin"
	${SET_DEBUG}

	rc_check_sczsh=0

	# check if start program is running
	if pgrep -z global -f "start_sczsh -R ${RESOURCE} " >/dev/null 2>&1
	then
		debug_message "Function: check_sczsh - start_sczsh is still running - early End"

		rc_check_sczsh=100
		return ${rc_check_sczsh} 
	fi

	# validate that the command exists and is executable
	myprobecmd=`echo ${ServiceProbeCommand} | awk '{print $1}'`

	if ! ${ZLOGIN} ${Zonename} /usr/bin/test -x ${myprobecmd}
	then
		# SCMSGS
		# @explanation
		# The command specified for variable ServiceProbeCommand
		# within the /opt/SUNWsczone/sczsh/util/sczsh_config
		# configuration file is not executable or does not exist in
		# the specified zone.
		# @user_action
		# Make sure that the command specified for variable
		# ServiceProbeCommand within the
		# /opt/SUNWsczone/sczsh/util/sczsh_config configuration file
		# is existing and executable for user root in the specified
		# zone. If you do not want to re-register the resource, make
		# sure the variable ServiceProbeCommand is properly set within
		# the ${PARAMETERDIR}/sczsh_${RS} parameterfile.
		scds_syslog -p daemon.err -t $(syslog_tag) -m \
		  "Function: check_sczsh - %s does not exist or is not executable in zone %s - early End" \
		  "${myprobecmd}" "${Zonename}"

		rc_check_sczsh=100
		return ${rc_check_sczsh}
	else
		debug_message "Function: validate - ${myprobecmd} is executable"
	fi

	${ZLOGIN} ${Zonename} ${ServiceProbeCommand}
	rc_check_sczsh=${?}

	if [ \( ${rc_check_sczsh} -ne 0 \) -a \( ${rc_check_sczsh} -ne 201 \) ]
	then
		rc_check_sczsh=100
	fi

	debug_message "Function: check_sczsh - End"
	return ${rc_check_sczsh}
}
