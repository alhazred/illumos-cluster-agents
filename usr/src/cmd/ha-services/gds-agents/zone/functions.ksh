#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)functions.ksh	1.4	07/06/06 SMI"
#

PKG=SUNWsczone
METHOD=`basename $0`
LOGFILE=/var/tmp/${RESOURCE}_logfile

SCLOGGER=/usr/cluster/lib/sc/scds_syslog

syslog_tag()
{
        #
        # Data Service message format
        #

        ${SET_DEBUG}

        print "SC[${PKG:-??}.${METHOD:-??}]:${RESOURCEGROUP:-??}:${RESOURCE:-??}"
}

scds_syslog()
{

        #
        # Log a message
        #

        ${SET_DEBUG}

        ${SCLOGGER} "$@" &
}

debug_message()
{
        #
        # Output a debug message to syslog if required
        #

        if [ "${DEBUG}" = "${RESOURCE}" -o "${DEBUG}" = "ALL" ]
        then
                SET_DEBUG="set -x"

                DEBUG_TEXT=$1

                scds_syslog -p daemon.debug -t $(syslog_tag) -m \
                        "%s" "${DEBUG_TEXT}"
        else
                SET_DEBUG=
        fi
}

log_message()
{
        #
        # Output a message to syslog as required
        #

        debug_message "Function: log_message - Begin"
        ${SET_DEBUG}

        if [ -s "${LOGFILE}" ]
        then
                PRIORITY=$1
                HEADER=$2

		# 
		# Ensure the while loop only reads a closed file
		#

		strings ${LOGFILE} > ${LOGFILE}.copy
                while read MSG_TXT
                do
                        scds_syslog -p daemon.${PRIORITY} -t $(syslog_tag) -m \
                                "%s - %s" \
                                "${HEADER}" "${MSG_TXT}"
                done < ${LOGFILE}.copy

                cat /dev/null > ${LOGFILE} > /dev/null
		cat /dev/null > ${LOGFILE}.copy
        fi

        debug_message "Function: log_message - End"
}
