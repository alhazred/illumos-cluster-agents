#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)clear_zone.ksh	1.6	09/01/20 SMI"
#

#
# The clear_zone script is only called from the stop_sczbt() function
# within /opt/SUNWsczone/sczbt/bin/functions and should not be invoked
# directly.
#
# Synopsis: clear_zone <zonerootpath> <resourcegroupname> <resourcename>
#
# Example: clear_zone /failover/myzone zone-rg myzone-rs
#
# The purpose is to kill any possible process blocking a filesystem
# mounted within the non-global zone, that is running from the global
# zone and was thus not already killed by a "zoneadm halt" of the
# non-global zone.
#
# It is a separate script in order to enable its call via hatimerun.
#

# define variables
MYDIR=`dirname ${0}`
MOUNT=/usr/sbin/mount
NAWK=/usr/bin/nawk
FUSER=/usr/sbin/fuser
KILL=/usr/bin/kill

Zonepath="$1"
RESOURCEGROUP="$2"
RESOURCE="$3"

. ${MYDIR}/../etc/config
. ${MYDIR}/../../lib/functions

get_zone_mounts()
{
	#
	# get_zone_mounts reports back to stdout all file systems
	# mounted within a non-global zones rootpath.
	#

        debug_message "Function: get_zone_mounts - Begin"
	${SET_DEBUG}

	${MOUNT} | ${NAWK} '($1 ~ "^'${Zonepath}'/root") { print $1 }'

	debug_message "Function: get_zone_mounts - End"
}

clear_zone()
{
	#
	# kill the processes which are hanging on the file systems of the non-global zone
	#

        debug_message "Function: clear_zone - Begin"
	${SET_DEBUG}

	rem_mount="`get_zone_mounts`"
	for fs in ${rem_mount}
	do
		# SCMSGS
		# @explanation
		# The non-global zones shutdown command did not complete in
		# time or zoneadm halt was not able to bring the non-global zone
		# into state "installed". A fuser -cs 15 is submitted against
		# each of the zones file systems. The reported processes
		# will be send a SIGTERM.
		# @user_action
		# None.
		scds_syslog -p daemon.err -t $(syslog_tag) -m \
       		  "Function: clear_zone - Killing processes with fuser -cs 15 on the file system %s" \
		  "${fs}"

		${FUSER} -cs 15 ${fs} >/dev/null 2>&1
	done

	# allow some time for processes to end
	/usr/bin/sleep 2

	# now send SIGKILL to any remaining processes
	rem_mount="`get_zone_mounts`"
	for fs in ${rem_mount}
	do
		# SCMSGS
		# @explanation
		# The non-global zones shutdown command did not complete in
		# time or zoneadm halt was not able to bring the non-global zone
		# into state "installed". A fuser -ck is submitted against
		# each of the zones file systems. The reported processes
		# will be send a SIGKILL.
		# @user_action
		# None.
		scds_syslog -p daemon.err -t $(syslog_tag) -m \
       		  "Function: clear_zone - Killing processes with fuser -ck on the file system %s" \
		  "${fs}"

		${FUSER} -ck ${fs} >/dev/null 2>&1
	done

	debug_message "Function: clear_zone - End"
}

clear_zone
