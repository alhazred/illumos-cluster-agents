#!/usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)sczbt_register.ksh	1.9	09/01/20 SMI"
#
#  This script has the optional -f option:
#  -f <filename> sources a user definable config file
#  different from sczbt_config.

MYNAME=`basename ${0}`

typeset opt

while getopts 'f:' opt
do
        case "${opt}" in
                f)      MYCONFIG=${OPTARG};;
                *)      echo "ERROR: ${MYNAME} Option ${OPTARG} unknown - early End. Only -f is valid"
                        exit 1;;
        esac
done

# Sourcing the specified config file, either the default one,
# or the one supplied with -f

if [ -n "${MYCONFIG}" ] && [ -f "${MYCONFIG}" ]
then
        echo "sourcing ${MYCONFIG} "
        . ${MYCONFIG}
else
        PKGCONF=`dirname $0`/sczbt_config
        echo "sourcing ${PKGCONF}"
        . ${PKGCONF}
fi

# constructing the parameter file

if [ ! -d ${PARAMETERDIR} ]; then
        echo "The value given for PARAMETERDIR (${PARAMETERDIR}) in sczbt_config is not a directory!"
        exit 1
fi

if [ ! -f ${PARAMETERDIR}/sczbt_${RS} ]; then
	/bin/cat > ${PARAMETERDIR}/sczbt_${RS} <<EOF
#!/usr/bin/ksh
#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#
# Parameters for sczbt (Zone Boot)
# 
# Zonename	Name of the zone
# Zonebrand     Brand of the zone. Current supported options are
#		"native" (default), "lx", "solaris8", "solaris9" or "ipkg"
# Zonebootopt	Zone boot options ("-s" requires that Milestone=single-user)
# Milestone	SMF Milestone which needs to be online before the zone is
#		considered as booted. This option is only used for the
#		"native" or "ipkg" Zonebrand.
# LXrunlevel	Runlevel which needs to get reached before the zone is
#		considered booted. This option is only used for the "lx"
#		Zonebrand.
# SLrunlevel	Solaris legacy runlevel which needs to get reached before the
#		zone is considered booted. This option is only used for the
#		"solaris8" or "solaris9" Zonebrand.
# Mounts        Mounts is a list of directories and their mount options,
#               which are loopback mounted from the global zone into the
#               newly booted zone. The mountpoint in the local zone can
#		be different to the mountpoint from the global zone.
#
#               The Mounts parameter format is as follows,
#
#		Mounts="/<global zone directory>:/<local zone directory>:<mount options>"
#
#		The following are valid examples for the "Mounts" variable
#
#		Mounts="/globalzone-dir1:/localzone-dir1:rw"
#		Mounts="/globalzone-dir1:/localzone-dir1:rw /globalzone-dir2:rw"
#		The only required entry is the /<global zone directory>, the
#		/<local zone directory> and <mount options> can be omitted.
#
#		Omitting /<local zone directory> will make the local zone
#		mountpoint the same as the global zone directory.
#
#		Omitting <mount options> will not provide any mount options
#		except the default options from the mount command.
#
#		Note: You must manually create any local zone mountpoint
#		      directories that will be used within the Mounts variable,
#		      before registering this resource within Sun Cluster.
#

Zonename="${Zonename}"
Zonebrand="${Zonebrand}"
Zonebootopt="${Zonebootopt}"
Milestone="${Milestone}"
LXrunlevel="${LXrunlevel}"
SLrunlevel="${SLrunlevel}"
Mounts="${Mounts}"
EOF

else
        echo "The parameterfile ${PARAMETERDIR}/sczbt_${RS} already exists! Will not overwrite and exit."
        exit 1
fi


# Checking dependencies for SC_NETWORK=true

SC_NETWORK=`echo ${SC_NETWORK} | /usr/xpg4/bin/tr [:upper:] [:lower:]`

if [ "${SC_NETWORK}" == "true" -a -z "${SC_LH}" ]
then
	echo "Error: SC_LH is required with SC_NETWORK=true"
	/bin/rm ${PARAMETERDIR}/sczbt_${RS}
	exit 1
fi

# If the configured zone has set ip-type=exclusive, then assigning a
# SUNW.LogicalHostname to this zone is not possible.

IPTYPE=`/usr/sbin/zonecfg -z ${Zonename} info | /bin/grep "^ip-type:" | /bin/awk '{print $2}'`

if [ "${IPTYPE}" == "exclusive" ] && [ "${SC_NETWORK}" == "true" ]
then
	echo "Error: The zone ${Zonename} has set ip-type=exclusive. This can not be combined with setting SC_NETWORK=true."
	/bin/rm ${PARAMETERDIR}/sczbt_${RS}
	exit 1
fi

# Checking dependencies for FAILOVER=true

FAILOVER=`echo ${FAILOVER} | /usr/xpg4/bin/tr [:upper:] [:lower:]`

if [ "${FAILOVER}" == "true" -a -z "${HAS_RS}" ]
then
	echo "Error: HAS_RS is required with FAILOVER=true"
	/bin/rm ${PARAMETERDIR}/sczbt_${RS}
	exit 1
fi

# Setting Resource_dependencies

if  [ "${SC_LH}" ] 
then
	RESOURCE_DEPENDENCIES="-y Resource_dependencies=${SC_LH}"
else
	RESOURCE_DEPENDENCIES=
fi
 
if  [ "${HAS_RS}" ]
then
	if [ "${RESOURCE_DEPENDENCIES}" ]
	then
		RESOURCE_DEPENDENCIES="$RESOURCE_DEPENDENCIES,${HAS_RS}"
	else
		RESOURCE_DEPENDENCIES="-y Resource_dependencies=${HAS_RS}"
	fi
fi
 
if [ ${SC_NETWORK} == "true" ]; then

/usr/cluster/bin/scrgadm -a -j ${RS} -g ${RG} -t SUNW.gds \
-x Start_command="/opt/SUNWsczone/sczbt/bin/start_sczbt \
-R ${RS} -G ${RG} -P ${PARAMETERDIR} " \
-x Stop_command="/opt/SUNWsczone/sczbt/bin/stop_sczbt \
-R ${RS} -G ${RG} -P ${PARAMETERDIR}" \
-x Probe_command="/opt/SUNWsczone/sczbt/bin/probe_sczbt \
-R ${RS} -G ${RG} -P ${PARAMETERDIR}" \
-y Port_list=10000/tcp -y Network_resources_used=${SC_LH} \
-x Stop_signal=9 ${RESOURCE_DEPENDENCIES}

else

/usr/cluster/bin/scrgadm -a -j ${RS} -g ${RG} -t SUNW.gds \
-x Start_command="/opt/SUNWsczone/sczbt/bin/start_sczbt \
-R ${RS} -G ${RG} -P ${PARAMETERDIR}" \
-x Stop_command="/opt/SUNWsczone/sczbt/bin/stop_sczbt \
-R ${RS} -G ${RG} -P ${PARAMETERDIR}" \
-x Probe_command="/opt/SUNWsczone/sczbt/bin/probe_sczbt \
-R ${RS} -G ${RG} -P ${PARAMETERDIR}" \
-x Network_aware=false \
-x Stop_signal=9 ${RESOURCE_DEPENDENCIES}

fi

St=$?

if [ "${St}" -ne 0 ]; then
	echo "Registration of resource ${RS} failed, please correct the wrong parameters."
	echo "Removing parameterfile ${PARAMETERDIR}/sczbt_${RS} for resource ${RS}."
	/bin/rm ${PARAMETERDIR}/sczbt_${RS}
        exit 1
else
        echo "Registration of resource ${RS} succeeded."
fi

# VALIDATE RESOURCE

/opt/SUNWsczone/sczbt/bin/validate_sczbt -R ${RS} -G ${RG} -P ${PARAMETERDIR}

St=$?

if [ "${St}" -ne 0 ]; then
	echo "Validation of resource ${RS} failed, check the syslog for the wrong parameters."
        echo "Removing resource ${RS} from the cluster configuration."

        /usr/cluster/bin/scrgadm -r -j ${RS}
	echo "Removing parameterfile ${PARAMETERDIR}/sczbt_${RS} for resource ${RS}."
	/bin/rm ${PARAMETERDIR}/sczbt_${RS}

        exit 1
else
        echo "Validation of resource ${RS} succeeded."
fi

exit 0
