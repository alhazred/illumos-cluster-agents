#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)functions.ksh	1.14	09/03/12 SMI"
#

# define variables
SLEEP=/usr/bin/sleep
IPMPSTAT=/sbin/ipmpstat

validate_options()
{
        debug_message "Function: validate_options - Begin"
	${SET_DEBUG}

        #
        # Ensure all options are set
        #
	rc_validate_options=0

        for i in RESOURCE RESOURCEGROUP PARDIR
        do
                case ${i} in
                        RESOURCE)
                        if [ -z ${RESOURCE} ]; then
                                # SCMSGS
                                # @explanation
                                # The option is not specified in the start,
                                # stop or probe command.
                                # @user_action
                                # Make sure that the appropriate config file
                                # is correct and reregister the resource.
                                scds_syslog -p daemon.err -t $(syslog_tag) -m \
                                "Function: validate_options - %s Option %s not set" \
                                "${MYNAME}" "-R"
                                rc_validate_options=1
                        fi;;

                        RESOURCEGROUP)
                        if [ -z ${RESOURCEGROUP} ]; then
                                scds_syslog -p daemon.err -t $(syslog_tag) -m \
                                "Function: validate_options - %s Option %s not set" \
                                "${MYNAME}" "-G"
                                rc_validate_options=1
                        fi;;

                        PARDIR)
                        if [ -z ${PARDIR} ]; then
                                scds_syslog -p daemon.err -t $(syslog_tag) -m \
                                "Function: validate_options - %s Option %s not set" \
                                "${MYNAME}" "-P"
                                rc_validate_options=1
			else
				# construct the variable PARFILE
				PARFILE=${PARDIR}/sczbt_${RESOURCE}
                        fi;;

                esac
        done

	debug_message "Function: validate_options returns ${rc_validate_options} - End"
	return ${rc_validate_options}
}

validate()
{
	#
	# Validate sczbt
	#
	
        debug_message "Function: validate - Begin"
	${SET_DEBUG}

	rc_validate=0

        #
        # Validate that prameter file exists and is syntactically correct
        #

	if ! val_parfile ${PARFILE} "Zonename Zonebootopt Milestone" ${PARDIR}
	then
		debug_message "Function: validate - End"
		rc_validate=1
		return ${rc_validate}
	fi

       # test the semantic of the parameters

       . ${PARFILE}

	for i in `/bin/cat ${PARFILE} | /bin/grep -v "^#"| /bin/grep -v "^ "| /bin/nawk -F= '{print $1}'`
        do
                case ${i} in

                        Zonename)
			
			# test the Zonename

			# is the zone name specified

			if [ -z ${Zonename} ]; then
                                # SCMSGS
                                # @explanation
                                # The variable Zonename does not contain a
                                # value.
                                # @user_action
                                # Review the components configuration file and
				# make sure the variable Zonename is properly
				# defined.
                                scds_syslog -p daemon.err -t $(syslog_tag) -m \
                                  "Function: validate - Zone name not set in %s" \
                                  "${PARFILE}"
                		rc_validate=1
			else
				debug_message "Function: validate - Zone name (${Zonename}) set"
			fi

			# is the current zone state valid 

			get_zone_state ${Zonename}

			if [ "${ZONE_STATE}" = "installed" -o "${ZONE_STATE}" = "ready" -o "${ZONE_STATE}" = "running" ]
			then
				debug_message "Function: validate - ${Zonename} state is ${ZONE_STATE}"
			else
                                # SCMSGS
                                # @explanation
                                # The referenced zone is not in the state
                                # installed, ready or running.
                                # @user_action
                                # Make sure that the referenced zone name is
                                # configured properly. Check if you have done
                                # a zlogin -C. Make sure that the name in the
                                # appropriate config file is correct.
                                scds_syslog -p daemon.err -t $(syslog_tag) -m \
                                  "Function: validate - %s state is %s" \
                                  "${Zonename}" "${ZONE_STATE}"
                		rc_validate=1
			fi

			# is the autobooot set to false

			if ! ${ZONECFG} -z ${Zonename} info | /bin/grep autoboot| /bin/grep false >/dev/null 2>&1
			then
                                # SCMSGS
                                # @explanation
                                # The referenced zone is configured with
                                # autoboot = true. This option needs to be
                                # set to false.
                                # @user_action
                                # Configure the autoboot variable of the
                                # configured zone to false. You need to run
                                # the zoncfg command to complete this task.
                                scds_syslog -p daemon.err -t $(syslog_tag) -m \
                                  "Function: validate - %s configured with autoboot true, it needs to be false" \
                                  "${Zonename}"
                		rc_validate=1
			else
				debug_message "Function: validate - ${Zonename} configured with autoboot false"
                        fi;;

			Zonebrand)

			# verify supported zone brand

			case ${Zonebrand} in
				native)
					debug_message "Function: validate - Zonebrand configured as ${Zonebrand}"
					ZONE_BRAND=`${ZONECFG} -z ${Zonename} info | /bin/grep "^brand:" | /bin/awk '{print $2}'`
					if [ -n "${ZONE_BRAND}" ]; then
						if [ "${ZONE_BRAND}" = "native" ]; then
							debug_message "Function: validate - Zonebrand verified as being ${ZONE_BRAND}"
						else
							# SCMSGS
							# @explanation
							# Zonebrand is set to
							# "native". But the
							# zone is configured
							# as a different brand
							# type, for example lx,
							# according to zonecfg
							# information.
							# @user_action
							# Review the components
							# configuration file and
							# make sure the variable
							# Zonebrand defines the
							# actual zone brand
							# type for the Zone.
							scds_syslog -p daemon.err \
							  -t $(syslog_tag) -m \
							  "Function: validate - Zonebrand for zone %s is configured as native, but the zone is configured as brand type %s." \
							  "${Zonename}" "${ZONE_BRAND}"
							rc_validate=1
						fi
					fi;;

				lx)
					debug_message "Function: validate - Zonebrand configured as ${Zonebrand}"
					if [ "`/usr/bin/uname -p`" != "i386" ]; then
							# SCMSGS
							# @explanation
							# The lx brand for zones
							# is only supported on
							# the i386 platform.
							# @user_action
							# Review the components
							# configuration file and
							# make sure the variable
							# Zonebrand defines "lx"
							# only on a i386 system.
							scds_syslog -p daemon.err \
							  -t $(syslog_tag) -m \
							  "Function: validate - Zonebrand for zone %s is configured as %s on a non-i386 system. It is only supported on i386 systems." \
							  "${Zonename}" "${Zonebrand}"
							rc_validate=1
					else
						ZONE_BRAND=`${ZONECFG} -z ${Zonename} info | /bin/grep "^brand:" | /bin/awk '{print $2}'`
						if [ "${ZONE_BRAND}" = "lx" ]; then
							debug_message "Function: validate - Zonebrand verified as being ${ZONE_BRAND}"
						else
							# SCMSGS
							# @explanation
							# Zonebrand is set to
							# "lx". But the
							# zone is not configured
							# as brand type lx
							# according to zonecfg
							# information.
							# @user_action
							# Review the components
							# configuration file and
							# make sure the variable
							# Zonebrand defines "lx"
							# only for a Zone that
							# got really setup with
							# brand type "lx".
							scds_syslog -p daemon.err \
							  -t $(syslog_tag) -m \
							  "Function: validate - Zonebrand for zone %s is configured as lx, but the zone is configured as brand type %s." \
							  "${Zonename}" "${ZONE_BRAND}"
							rc_validate=1
						fi
					fi;;

				solaris8|solaris9)
					debug_message "Function: validate - Zonebrand configured as ${Zonebrand}"
					if [ "`/usr/bin/uname -p`" != "sparc" ]; then
							# SCMSGS
							# @explanation
							# The solaris8 and
							# solaris9 brand for
							# zones is only
							# supported on the
							# sparc platform.
							# @user_action
							# Review the components
							# configuration file and
							# make sure the variable
							# Zonebrand defines
							# "solaris8" or
							# "solaris9" only on a
							# sparc system.
							scds_syslog -p daemon.err \
							  -t $(syslog_tag) -m \
							  "Function: validate - Zonebrand for zone %s is configured as %s on a non-sparc system. It is only supported on sparc systems." \
							  "${Zonename}" "${Zonebrand}"
							rc_validate=1
					else
						ZONE_BRAND=`${ZONECFG} -z ${Zonename} info | /bin/grep "^brand:" | /bin/awk '{print $2}'`
						if [ "${ZONE_BRAND}" = "${Zonebrand}" ]; then
							debug_message "Function: validate - Zonebrand verified as being ${ZONE_BRAND}"
						else
							# SCMSGS
							# @explanation
							# Zonebrand is set to
							# "solaris8" or
							# "solaris9". But the
							# zone is not configured
							# as brand type solaris8
							# or solaris9
							# according to zonecfg
							# information.
							# @user_action
							# Review the components
							# configuration file and
							# make sure the variable
							# Zonebrand defines
							# "solaris8" or
							# "solaris9" only for a
							# Zone that got really
							# setup with brand type
							# "solaris8" or
							# "solaris9".
							scds_syslog -p daemon.err \
							  -t $(syslog_tag) -m \
							  "Function: validate - Zonebrand for zone %s is configured as %s, but the zone is configured as brand type %s." \
							  "${Zonename}" "${Zonebrand}" "${ZONE_BRAND}"
							rc_validate=1
						fi
					fi;;

				ipkg)
					debug_message "Function: validate - Zonebrand configured as ${Zonebrand}"
					ZONE_BRAND=`${ZONECFG} -z ${Zonename} info | /bin/grep "^brand:" | /bin/awk '{print $2}'`
					if [ -n "${ZONE_BRAND}" ]; then
						if [ "${ZONE_BRAND}" = "ipkg" ]; then
							debug_message "Function: validate - Zonebrand verified as being ${ZONE_BRAND}"
						else
							# SCMSGS
							# @explanation
							# Zonebrand is set to
							# "ipkg". But the
							# zone is configured
							# as a different brand
							# type, for example lx,
							# according to zonecfg
							# information.
							# @user_action
							# Review the components
							# configuration file and
							# make sure the variable
							# Zonebrand defines the
							# actual zone brand
							# type for the Zone.
							scds_syslog -p daemon.err \
							  -t $(syslog_tag) -m \
							  "Function: validate - Zonebrand for zone %s is configured as ipkg, but the zone is configured as brand type %s." \
							  "${Zonename}" "${ZONE_BRAND}"
							rc_validate=1
						fi
					fi;;

				*)
						# SCMSGS
						# @explanation
						# Currently Zonebrand can only
						# be set to "native", "lx",
						# "solaris8", "solaris9" or
						# "ipkg".
						# @user_action
						# Review the components
						# configuration file and make
						# sure the variable Zonebrand
						# defines either "native", "lx"
						# "solaris8", "solaris9" or
						# "ipkg".
						scds_syslog -p daemon.err \
						  -t $(syslog_tag) -m \
						  "Function: validate - Zonebrand for zone %s is configured as %s. Valid values are native, lx, solaris8, solaris9 and ipkg." \
						  "${Zonename}" "${Zonebrand}"
						rc_validate=1;;
			esac;;

                        Zonebootopt)

			# test the Zones boot options

			# is the Zone boot option specified and a supported one

			if [ ! -z ${Zonebootopt} ] && [ "${Zonebootopt}" != "-s" ]; then

	                        # SCMSGS
	                        # @explanation
	                        # The specified boot option is not allowed.
	                        # @user_action
	                        # Consult the manpage of zoneadm which boot
	                        # options are allowed and specify one of
	                        # them.
	                        scds_syslog -p daemon.err -t $(syslog_tag) -m \
	                          "Function: validate - %s invalid boot option [%s]" \
	                          "${Zonename}" "${Zonebootopt}"
	                	rc_validate=1
			else
				debug_message "Function: validate - ${Zonename} boot option (${Zonebootopt})"
                        fi;;

                        Milestone)

			# test the smf service which indicate that the zone is online

			# is the Milestone specified

			if [ -z "${Milestone}" ] && [ "${Zonebrand}" = "native" -o "${Zonebrand}" = "ipkg" ]; then
                                # SCMSGS
                                # @explanation
                                # The Milestone variable is not set in the
                                # parameter file sczbt_<resource name>.
				# The native and ipkg zone brand type require
				# this variable to be set.
                                # @user_action
                                # Set the Milestone variable in the parameter
                                # file sczbt_<resource name>.
                                scds_syslog -p daemon.err -t $(syslog_tag) -m \
                                  "Function: validate - Milestone not set in %s" \
                                  "${PARFILE}"
                		rc_validate=1
			else
				debug_message "Function: validate - Milestone (${Milestone}) set"
			fi;;

			LXrunlevel)

			# verify that LXrunlevel is setup if Zonebrand="lx"

			if [ -z "${LXrunlevel}" ] && [ "${Zonebrand}" = "lx" ]; then
				# SCMSGS
				# @explanation
                                # The LXrunlevel variable is not set in the
                                # parameter file sczbt_<resource name>.
                                # @user_action
                                # Set the LXrunlevel variable in the parameter
                                # file sczbt_<resource name>.
                                scds_syslog -p daemon.err -t $(syslog_tag) -m \
                                  "Function: validate - LXrunlevel not set in %s" \
                                  "${PARFILE}"
                		rc_validate=1
			else
				debug_message "Function: validate - LXrunlevel (${LXrunlevel}) set"
			fi;;

			SLrunlevel)

			# verify that SLrunlevel is setup if Zonebrand is set
			# to "solaris8" or "solaris9"

			if [ -z "${SLrunlevel}" ] && [ "${Zonebrand}" = "solaris8" -o "${Zonebrand}" = "solaris9" ]; then
				# SCMSGS
				# @explanation
                                # The SLrunlevel variable is not set in the
                                # parameter file sczbt_<resource name>.
                                # @user_action
                                # Set the SLrunlevel variable in the parameter
                                # file sczbt_<resource name>.
                                scds_syslog -p daemon.err -t $(syslog_tag) -m \
                                  "Function: validate - SLrunlevel not set in %s" \
                                  "${PARFILE}"
                		rc_validate=1
			else
				debug_message "Function: validate - SLrunlevel (${SLrunlevel}) set"
			fi;;

		esac
	done

	# validates the Zonepath

	# get the zone path

	get_zonepath ${Zonename}

	# is the Zonepath a directory

	if [ ! -d ${Zonepath} ]; then
                # SCMSGS
                # @explanation
                # The zone path extracted from the zones configuration is not
                # present or not a directory.
                # @user_action
                # Make sure that the filesystem for the zone path is mounted.
                # Consider using a SUNW.HAStoragePlus resource. If already used,
		# check if the resource dependency to it is in place.
                scds_syslog -p daemon.err -t $(syslog_tag) -m \
                  "Function: validate - Zonepath %s needs to be a directory" \
                  "${Zonepath}" 
                rc_validate=1
	else
		debug_message "Function: validate - Zonepath ${Zonepath} is a directory"
	fi

	# Validate that we are in the global zone

	val_in_global
	if [ ${?} -ne 0 ]; then
		rc_validate=1
	fi 

	# Get list of configured SUNW.LogicalHostname resources
	LH_LIST=`${SCHA_RESOURCE_GET} -O NETWORK_RESOURCES_USED -R ${RESOURCE} -G ${RESOURCEGROUP}`

	# Check if ip-type=exclusive is set for the Zone. In that case
	# ${LH_LIST} must be empty, since assigning an IP by setting the zone
	# flag to the Zone does only work if ip-type=shared.
	IPTYPE=`${ZONECFG} -z ${Zonename} info | /bin/grep "^ip-type:" | /bin/awk '{print $2}'`

	if [ "${IPTYPE}" = "exclusive" ] && [ -n "${LH_LIST}" ]
	then
		# SCMSGS
		# @explanation
		# The configured non-global zone has set ip-type=exclusive
		# within its zone config, and the resource was configured
		# to depend on a SUNW.LogicalHostname resource. It is not
		# possible to assign the IP address from this
		# SUNW.LogicalHostname resource for the non-global zone if
		# ip-type=exclusive is set.
		# @user_action
		# Either set ip-type=shared for the non-global zone config,
		# or remove the dependency to the SUNW.LogicalHostname resource.
		scds_syslog -p daemon.err -t $(syslog_tag) -m \
			"Function: validate - Zone %s is configured with ip-type=exclusive, and the sczbt resource is dependand on a SUNW.LogicalHostname resource. This combination does not work." \
			"${Zonename}"
		rc_validate=1
	fi

	# Check if any IPMP_HOST_IP has been configured with the Zone

	for LH in ${LH_LIST}
	do
		get_ipmp_state ${LH}

		# Test if the IPMP_HOST_IP has been configured within the Zone
	
		ZONE_NET_INFO=/tmp/${RESOURCE}_zoneinfo.txt

		echo info net | ${ZONECFG} -z ${Zonename} > ${ZONE_NET_INFO}

		for j in ${IPMP_HOST_IP}
		do
			for i in `/bin/grep address ${ZONE_NET_INFO} | /bin/awk '{print $2}' | /bin/awk -F/ '{print $1}'`
			do
				if [ "${i}" = "${j}" ]
				then
					# SCMSGS
					# @explanation
					# There is an IP address conflict
					# between the SUNW.LogicalHostname
					# resource and the configured zone.
					# @user_action
					# Remove the IP address from the zones
					# configuration with the zonecfg
					# command.
					scds_syslog -p daemon.err -t $(syslog_tag) -m \
       			  	  	  "Function: validate - %s configured with address %s, please remove the address using zonecfg" \
			  	  	  "${Zonename}" "${j}" 
					rc_validate=1
				fi
			done
		done

		# Test if the IPMP_HOST has been configured within the Zone

		for j in ${IPMP_HOST}
		do
			for i in `/bin/grep address ${ZONE_NET_INFO} | /bin/awk '{print $2}' | /bin/awk -F/ '{print $1}'`
			do
				if [ "${i}" = "${j}" ]
				then
					scds_syslog -p daemon.err -t $(syslog_tag) -m \
       			  	  	  "Function: validate - %s configured with address %s, please remove the address using zonecfg" \
			  	  	  "${Zonename}" "${j}" 
					rc_validate=1
				fi
			done
		done

		# Test if another zone is already using this IPMP_HOST_IP

		for ADAPTER in ${ADAPTER_LIST}
		do
			for i in `${IFCONFIG} -a | /bin/grep ${ADAPTER} | /bin/awk -F: '{print $2}' | /bin/grep -v flags`
			do
				IPMP_ADAPTER_IP=`${IFCONFIG} ${ADAPTER}:${i} | /bin/grep -w inet | /bin/awk '{print $2}'`

				for j in ${IPMP_HOST_IP}
				do
					if [ "${IPMP_ADAPTER_IP}" = "${j}" ]
					then
						if ${IFCONFIG} ${ADAPTER}:${i} | /bin/grep -w zone > /dev/null
						then
							ZONE_ADAPTER=`${IFCONFIG} ${ADAPTER}:${i} | /bin/grep -w zone | /bin/awk '{print $2}'`
	
							# SCMSGS
							# @explanation
							# The logical host is
							# used in another zone
							# already. A logical
							# host can not be used
							# in two zones at the
							# same time.
							# @user_action
							# Resolve the
							# conflicting IP address
							# configuration for
							# your zones.
							scds_syslog -p daemon.err -t $(syslog_tag) -m \
							  "Function: validate - %s (%s) already in use on %s:%s for %s, please resolve this conflict" \
							  "${LH}" "${j}" "${ADAPTER}" "${i}" "${ZONE_ADAPTER}"
							rc_validate=1
						fi
					fi
				done
			done
		done
	done

	# Check Zonebootopt="-s" has Milestone=single-user
	
	if [ "${Zonebootopt}" = "-s" ] 
	then
		case ${Zonebrand} in
		   native|ipkg)
			if ! echo ${Milestone} | /bin/grep single-user > /dev/null
			then
				# SCMSGS
				# @explanation
				# The Zoneboot variable is set to -s. Every
				# milestone other than single-user is invalid.
				# @user_action
				# Correct the milestone in the paramter file
				# sczbt_<resource name>. You need to specify
				# single user together with the boot option -s.
				scds_syslog -p daemon.err -t $(syslog_tag) -m \
				  "Function: validate - Milestone (%s) is invalid with Zonebootopt=%s, needs single-user" \
				  "${Milestone}" "${Zonebootopt}"
				rc_validate=1
			fi;;

		   lx)
			if [ "${LXrunlevel}" != "unknown" ]
			then
				# SCMSGS
				# @explanation
				# The Zoneboot variable is set to -s. Every
				# LXrunlevel other than "unknown" is invalid.
				# @user_action
				# Correct the LXrunlevel variable in the
				# paramter file sczbt_<resource name>. You need
				# to specify "unknown" together with the boot
				# option -s.
				scds_syslog -p daemon.err -t $(syslog_tag) -m \
				  "Function: validate - LXrunlevel (%s) is invalid with Zonebootopt=%s, needs single-user" \
				  "${LXrunlevel}" "${Zonebootopt}"
				rc_validate=1
			fi;;

		   solaris8|solaris9)
			if [ "${SLrunlevel}" != "S" ]
			then
				# SCMSGS
				# @explanation
				# The Zoneboot variable is set to -s. Every
				# SLrunlevel other than "S" is invalid.
				# @user_action
				# Correct the SLrunlevel variable in the
				# paramter file sczbt_<resource name>. You need
				# to specify "S" together with the boot
				# option -s.
				scds_syslog -p daemon.err -t $(syslog_tag) -m \
				  "Function: validate - SLrunlevel (%s) is invalid with Zonebootopt=%s, needs single-user" \
				  "${SLrunlevel}" "${Zonebootopt}"
				rc_validate=1
			fi;;

		esac
	fi

	# Check that lofs is not excluded, first check if an exclude lofs in in /etc/system

	if /bin/grep -w exclude /etc/system | /bin/grep lofs >/dev/null 2>&1
	then
	
	# if yes check if it is commented out

		if ! /bin/grep -w exclude /etc/system |/bin/grep lofs | /bin/egrep "^\*|^ +\*|^#|^ +|	+#|^	+\*" >/dev/null 2>&1
		then
   			# SCMSGS
   			# @explanation
   			# The kernel module for the lofs filesystem is still
			# excluded in /etc/system.
   			# @user_action
   			# Place a &star; in front of the exlude: lofs line and
   			# reboot the node.
   			scds_syslog -p daemon.err -t $(syslog_tag) -m \
			  "Function: validate - exclude: lofs found in /etc/system" 
			rc_validate=1
		else
			debug_message "Function: validate - exclude: lofs commented out in /etc/system"
		fi
	else
		debug_message "Function: validate - No exclude: lofs found in /etc/system"
	fi

	if [ -n "${Mounts}" ]
	then
		for i in ${Mounts}
		do
			directory=
			localdir=

			if echo ${i} | /bin/grep : >/dev/null 2>&1
			then
				echo ${i} | /bin/awk -F: '{ print NF,$1,$2 }' | read field_count val1 val2

				case ${field_count} in
					2)	# Support <global zone directory>:<local zone directory>
					  	# Support <global zone directory>:<mount options>
					  	directory=${val1}

						if echo ${val2} | /bin/grep / > /dev/null 2>&1
						then
							localdir=${Zonepath}/root${val2}
						else
							localdir=${Zonepath}/root${val1}
						fi
						;;
					3)	# Support <global zone directory>:<local zone directory>:<mount options>
					  	# Support <global zone directory>:<local zone directory>:
						directory=${val1}
						localdir=${Zonepath}/root${val2}
						;;
					*)	scds_syslog -p daemon.err -t $(syslog_tag) -m \
						   "Function: validate - Mounts=%s is invalid" 
						   "${Mounts}"
						rc_validate=1
						;;
				esac
			else
				# Support just <global zone directory> without any further entries
				directory=${i}
				localdir=${Zonepath}/root${i}
			fi

			if [ ! -d ${directory} ]
			then
	   			# SCMSGS
	   			# @explanation
	   			# The directory used as a mountpoint in the
	   			# global zone is missing.
	   			# @user_action
	   			# Create the mountpoint in the global zone.
	   			scds_syslog -p daemon.err -t $(syslog_tag) -m \
				  "Function: validate - Global zones mountpoint %s does not exist" \
				  "${directory}"
				rc_validate=1
			else
				debug_message "Function: validate - mountpoint ${directory} exists in the global zone"
			fi
	
			if [ ! -d ${localdir} ]
			then
	   			# SCMSGS
	   			# @explanation
	   			# The directory used as a mountpoint in the
	   			# non-global zone is missing.
	   			# @user_action
	   			# Create the mountpoint in the non-global zone.
	   			scds_syslog -p daemon.err -t $(syslog_tag) -m \
				  "Function: validate - Non-global zones mountpoint %s does not exist" \
				  "${localdir}"
				rc_validate=1
			else
				debug_message "Function: validate - mountpoint ${localdir} exists in the non-global zone"
			fi
	
		done
	fi

	debug_message "Function: validate - End"
	return ${rc_validate}
}

lofs_mounts()
{
        debug_message "Function: lofs_mount - Begin"
	${SET_DEBUG}

	lofs_mounts_rc=0

	if [ "${Zonebrand}" = "native" -o "${Zonebrand}" = "ipkg" ]; then
		# wait until svc:/system/filesystem/minimal:default gets online
		get_svc_state ${Zonename} svc:/system/filesystem/minimal:default

		while [ "${SVC_STATE}" != "online" ]
		do
			debug_message "wait until svc:/system/filesystem/minimal:default gets online in zone ${Zonename}"
			${SLEEP} 2
			get_svc_state ${Zonename} svc:/system/filesystem/minimal:default
		done
	fi

	# start with the mounts

	get_zonepath ${Zonename}

	for i in ${Mounts}
	do
		source=
		target=
		mountopt=

		if echo ${i} | /bin/grep : >/dev/null 2>&1
		then
			echo ${i} | /bin/awk -F: '{ print NF,$1,$2,$3 }' | read field_count val1 val2 val3

			case ${field_count} in
				2)	# Support <global zone directory>:<local zone directory>
				  	# Support <global zone directory>:<mount options>
				  	source=${val1}

					if echo ${val2} | /bin/grep / > /dev/null 2>&1
					then
						target=${Zonepath}/root${val2}
					else
						target=${Zonepath}/root${val1}
						[ ${val2} ] && mountopt="-o ${val2}"
					fi
					;;
				3)	# Support <global zone directory>:<local zone directory>:<mount options>
				  	# Support <global zone directory>:<local zone directory>:
					source=${val1}
					target=${Zonepath}/root${val2}
					[ ${val3} ] && mountopt="-o ${val3}"
					;;
				*)	scds_syslog -p daemon.err -t $(syslog_tag) -m \
					   "Function: validate - Mounts=%s is invalid" 
					   "${Mounts}"
					;;
			esac
		else
			# Support just <global zone directory> without any further entries
			source=${i}
			target=${Zonepath}/root${i}
		fi

		debug_message "Function: lofs_mounts - will mount ${source} to ${target} with the options ${mountopt}"

		if /usr/sbin/mount -F lofs ${mountopt} ${source} ${target} >>${LOGFILE} 2>&1
		then 
	   		# SCMSGS
	   		# @explanation
	   		# The mount of the directory into the root path of the
	   		# non-global zone was successful.
	   		# @user_action
	   		# None.
	   		scds_syslog -p daemon.notice -t $(syslog_tag) -m \
			  "Function: lofsmount - Non-global zones mount from %s to %s with options %s successful" \
			  "${source}" "${target}" "${mountopt}"
		else
	   		# SCMSGS
	   		# @explanation
	   		# The mount of the directory into the root path of the
	   		# non-global zone failed.
	   		# @user_action
	   		# Watch the verbose error message in the zones start
	   		# messages and fix the issue with appropriate methods.
	   		scds_syslog -p daemon.err -t $(syslog_tag) -m \
			  "Function: lofsmount - Non-global zones mount from %s to %s with options %s failed" \
			  "${source}" "${target}" "${mountopt}"
			lofs_mount_rc=1
		fi
	done
	
        debug_message "Function: lofs_mount - End"
	return ${lofs_mount_rc}
}

get_zonepath()
{
        debug_message "Function: get_zonepath - Begin"
	${SET_DEBUG}

	myzone=${1}
	Zonepath=`${ZONECFG} -z ${myzone} info | /bin/grep ^zonepath: | /bin/awk '{print $2}'`

        debug_message "Function: zonepath - End"
}

get_ipmp_hosts()
{
        debug_message "Function: get_ipmp_hosts - Begin"
	${SET_DEBUG}

	LH=$1

	IPMP_HOST=`${SCHA_RESOURCE_GET} -O Extension -R ${LH} -G ${RESOURCEGROUP} HostnameList | /bin/tail +2`
	IPMP_HOST_IP=`/bin/getent hosts ${IPMP_HOST} | /bin/awk '{print $1}'`

        debug_message "Function: get_ipmp_hosts - End"
}

get_ipmp_state()
{
	# Retreive IPMP information for each Network_resources_used entry

        debug_message "Function: get_ipmp_state - Begin"
	${SET_DEBUG}

	LH=$1

	get_ipmp_hosts ${LH}

	NODEID=`${CLINFO} -n`
	
	# Get the correct IPMP group, i.e. <IPMP>@${NODEID}

	IPMP_LIST=`${SCHA_RESOURCE_GET} -O Extension -R ${LH} -G ${RESOURCEGROUP} NetIfList | /bin/tail +2`

	for i in ${IPMP_LIST}
	do
		if [ `echo ${i} | /bin/awk -F@ '{print $2}'` -eq "${NODEID}" ]
		then
			IPMP_GROUP=`echo ${i} | /bin/awk -F@ '{print $1}'`
		fi
	done
		
	# Project Clearview changed the previous IPMP behaviour and introduced
	# the /sbin/ipmpstat command.
	#
	# Before Clearview, there was no CLI to retrieve the adapter list
	# of a specific IPMP group. Therfore scstat has been used to deduce it.
	# ADAPTER_LIST did get setup with the adapters being part of the
	# configured IPMP group.
	#
	# This will no longer work on a system with Clearview.
	# With Clearview, ADAPTER_LIST will just contain the ipmp<number>
	# pseudo device corresponding to the configured IPMP group.
	if [ -x "${IPMPSTAT}" ]
        then
		ADAPTER_LIST=$(${IPMPSTAT} -o group,groupname -P -g | /bin/nawk -F: -v ipmpgroup="${IPMP_GROUP}" '{ if ($2 == ipmpgroup) print $1 }')
	else
		# Get the Online IPMP adapter list for that IPMP Group
		LOCALNODENAME=$(${SCHA_CLUSTER_GET} -O NODENAME_LOCAL)

		ADAPTER_LIST=$(/bin/env LC_ALL=POSIX ${SCSTAT} -i -h ${LOCALNODENAME} | /bin/grep " ${IPMP_GROUP} " | /bin/grep Online | /bin/awk '{print $6}')
	fi

        debug_message "Function: get_ipmp_state - End"
}

start_sczbt()
{
	#
	# Start sczbt
	#

        debug_message "Function: start_sczbt - Begin"
	${SET_DEBUG}

	# If the zone is of brand type "solaris8" or "solaris9" and if the
	# platform changed from where the zone was previously started,
	# the s8_p2v or s9_p2v script needs to get called prior boot to make
	# sure that any platform specific libraries are properly setup within
	# the zone rootpath.
	# The script needs to run only once per new platform. The file
	# .platform.orig stores the platform names for which the s8_p2v or
	# s9_p2v script had already run. Only if a new platform gets introduced
	# into the cluster, the s8_p2v or s9_p2v script gets called on that
	# node again.
	if [ "${Zonebrand}" = "solaris8" -o "${Zonebrand}" = "solaris9" ]; then

		case ${Zonebrand} in
			solaris8)
				P2V_SCRIPT=/usr/lib/brand/solaris8/s8_p2v
				;;
			solaris9)
				P2V_SCRIPT=/usr/lib/brand/solaris9/s9_p2v
				;;
		esac

		get_zonepath ${Zonename}

		myplatform=`/bin/uname -i`
		myplatformfile=${Zonepath}/root/.platform.orig
		need_p2v=true

		# if there is no .platform.orig file or if the current
		# platform is not contained, s8_p2v needs to run
		if [ -f ${myplatformfile} ]; then

			if /usr/bin/grep "^${myplatform}$" ${myplatformfile} > /dev/null 2>&1
			then
				need_p2v=false
			fi
		fi
		
		if [ -x "${P2V_SCRIPT}" -a "${need_p2v}" = "true" ]; then
			${P2V_SCRIPT} ${Zonename} >${LOGFILE} 2>&1
			if [ $? -ne 0 ]; then
				# SCMSGS
				# @explanation
				# Unable to perform platform specific
				# setup for the solaris8 or solaris9 zone.
				# @user_action
				# Disable the resource and manually run
				# '/usr/lib/brand/solaris8/s8_p2v 
				# <zonename>' for solaris8 or
				# '/usr/lib/brand/solaris9/s9_p2v <zonename>'
				# for solaris9 on the node where the start
				# failed. Correct any errors reported, then
				# restart the resource.
				scds_syslog -p daemon.err -t $(syslog_tag) -m \
					"Function: start_sczbt - Running command %s %s failed. Unable to start zone." \
					"${P2V_SCRIPT}" "${Zonename}"
				rc_start_command=1
				return 1
			fi

			# Only update the file if it is not a symbolic link.
			# Otherwise it would be a security issue.
			if [ ! -h ${myplatformfile} ]; then
				# Add platform to the .platform.orig file
				echo ${myplatform} >> ${myplatformfile}
			fi
		fi

		# Only update the file if it is not a symbolic link.
		# Otherwise it would be a security issue.
		if [ ! -h ${Zonepath}/root/.host.orig ]; then
			# This update is necessary to make sure the
			# solaris8 or solaris9 zone can get booted without
			# manual intervention.
			/usr/bin/hostid > ${Zonepath}/root/.host.orig
		fi
	fi

	# boot the zone
	${ZONEADM} -z ${Zonename} boot ${Zonebootopt} >>${LOGFILE} 2>&1

	rc_start_command=${?}

        # disable the pmf tag and run a sleep in the background, to assure, that there will be a valid pmftag during the start phase

        START_TIMEOUT=`${SCHA_RESOURCE_GET} -O START_TIMEOUT -R ${RESOURCE} -G ${RESOURCEGROUP}`
        ${SLEEP} ${START_TIMEOUT} &

        ${PMFADM} -s ${RESOURCEGROUP},${RESOURCE},0.svc

	# mount the requested loopbakcmount to the local zone

	if ! lofs_mounts
	then
		rc_start_command=1
	fi

	# assign zone flag to registered interfaces of the SUNW.LogicalHostname resources

	if [ "${LH_LIST}" ]
	then
		for LH in ${LH_LIST}
		do
			get_ipmp_state ${LH}

			# Place the IPMP ADAPTER into the local zone

			for ADAPTER in ${ADAPTER_LIST}
			do
				for j in ${IPMP_HOST_IP}
				do
					for i in `${IFCONFIG} -a | /bin/grep ${ADAPTER} | /bin/awk -F: '{print $2}' | /bin/grep -v flags`
					do
						IPMP_ADAPTER_IP=`${IFCONFIG} ${ADAPTER}:${i} | /bin/grep -w inet | /bin/awk '{print $2}'`
		
						if [ "${IPMP_ADAPTER_IP}" = "${j}" ]
						then
							if ! ${IFCONFIG} ${ADAPTER}:${i} zone ${Zonename}
							then
								rc_start_command=1
							fi
		
		   					# SCMSGS
		   					# @explanation
		   					# The interface of the
		   					# logical host
		   					# resource is placed
		   					# by the start command
		   					# into the referenced
		   					# zone.
		   					# @user_action
		   					# None.
		   					scds_syslog -p daemon.notice -t $(syslog_tag) -m \
					  	  	  "Function: start_sczbt - logical interface %s:%s placed into %s" \
						  	  "${ADAPTER}" "${i}" "${Zonename}"
						fi
					done
				done
			done
		done
	fi
		
	debug_message "Function: start_sczbt - End"
	return ${rc_start_command}
}

stop_sczbt()
{
	#
	# Stop sczbt
	#

        debug_message "Function: stop_sczbt - Begin"
	${SET_DEBUG}

	rc_stop_command=0

	# RETRIEVE STOP_TIMEOUT
	STOP_TIMEOUT=`${SCHA_RESOURCE_GET} -O STOP_TIMEOUT -R ${RESOURCE} -G ${RESOURCEGROUP}`

	# 60 % of the STOP_TIMEOUT can be spend on waiting for normal shutdown
	MAX_STOP_TIMEOUT=`expr ${STOP_TIMEOUT} \* 60 \/ 100`

	# 15 % of the STOP_TIMEOUT can be spend on waiting for clear_zone()
	CLEAR_STOP_TIMEOUT=`expr ${STOP_TIMEOUT} \* 15 \/ 100`

	# reset SECONDS to zero
	SECONDS=0

	# Place the IPMP ADAPTER back into the global zone
	LH_LIST=`${SCHA_RESOURCE_GET} -O NETWORK_RESOURCES_USED -R ${RESOURCE} -G ${RESOURCEGROUP}`

	if [ "${LH_LIST}" ]
	then
		for LH in ${LH_LIST}
		do
			get_ipmp_state ${LH}

			for ADAPTER in ${ADAPTER_LIST}
			do
				for j in ${IPMP_HOST_IP}
				do
					for i in `${IFCONFIG} -a | /bin/grep ${ADAPTER} | /bin/awk -F: '{print $2}' | /bin/grep -v flags`
					do
						# For each ${ADAPTER}:${i} check "${IPMP_ADAPTER_IP}" = "${j}" , ${j} is one of the list of ip addresses
						# If there's a match, then for each ${ADAPTER}:${i} check the zone flag = "${Zonename}"
						# If there's a match then place the ${ADAPTER}:${i} back into the global zone
		
						IPMP_ADAPTER_IP=`${IFCONFIG} ${ADAPTER}:${i} | /bin/grep -w inet | /bin/awk '{print $2}'`
		
						if [ "${IPMP_ADAPTER_IP}" = "${j}" ]
						then
							if ${IFCONFIG} ${ADAPTER}:${i} | /bin/grep -w zone > /dev/null
							then
								ZONE_ADAPTER=`${IFCONFIG} ${ADAPTER}:${i} | /bin/grep -w zone | /bin/awk '{print $2}'`
		
								if [ "${ZONE_ADAPTER}" = "${Zonename}" ]
								then
									${IFCONFIG} ${ADAPTER}:${i} -zone
		
									# SCMSGS
									# @explanation
									# The
									# interface
									# of
									# the
									# associated
									# logical
									# host
									# is
									# placed
									# back
									# into
									# the
									# global
									# zone
									# by
									# the
									# stop
									# command
									# of
									# the
									# sczbt
									# component.
									# @user_action
									# None
									scds_syslog -p daemon.notice -t $(syslog_tag) -m \
									  "Function: stop_sczbt - Logical interface %s:%s placed into the global zone" \
									  "${ADAPTER}" "${i}"
								fi
							fi
						fi
					done
				done
			done
		done
	fi

	# bring down the zone
	debug_message "Function: stop_sczbt - Shuting down non-global zone ${Zonename}"
	case ${Zonebrand} in
		native|solaris8|solaris9|ipkg)
			${ZLOGIN} ${Zonename} /usr/sbin/shutdown -y -g0 -i0 >${LOGFILE} 2>&1
			;;
		lx)
			${ZLOGIN} ${Zonename} /sbin/shutdown -h now >${LOGFILE} 2>&1
			;;
	esac

	#
	# run a test loop to determine if the zone reached state "installed"
	# or if the timeout for proper shutdown is exceeded
	#
	while  [ ${SECONDS} -lt ${MAX_STOP_TIMEOUT} ]
	do
		# check if the zone reached state "installed"
		get_zone_state ${Zonename}
		debug_message "Function: stop_sczbt - ${Zonename} state is ${ZONE_STATE}, time passed while waiting for shutdown: ${SECONDS}"
		if [ "${ZONE_STATE}" = "installed" ]; then
		   SECONDS=${MAX_STOP_TIMEOUT}
		else
		   # wait 4 seconds
		   ${SLEEP} 4
		fi
	done

	#
	# bring the zone down at all costs if the shutdown has exceeded
	# its time limit and did not reach state "installed"
	#

	if [ "${ZONE_STATE}" != "installed" ]; then

		debug_message "Function: stop_sczbt - Halting non-global zone ${Zonename}"

		#
		# According to
		# http://docs.sun.com/app/docs/doc/819-2450/6n4o5mde2?a=view
		# zoneadm halt will bring down all processes running within
		# the non-global zone, devices are unconfigured, network
		# interfaces are unplumbed, file systems are unmounted, and the
		# kernel data structures are destroyed.
		#
		# However, processes running within the global zone could
		# block a non-global zones file system (e.g. a backup app),
		# for that reason after the first zoneadm halt got issued,
		# but the non-global zone did not yet reach state "installed",
		# the clear_zone script will get called. It will try to kill
		# any processes blocking the non-global zones file systems
		# from the global zone.
		#
		# Afterwards zoneadm halt is called a second time to bring
		# the non-global zone into state "installed. If this is
		# not possible, an error message indicates the need for
		# manual intervention through the systems administrator
		# in order to cleanup the non-global zone.
		# 
		${ZONEADM} -z ${Zonename} halt >>${LOGFILE} 2>&1
		rc_stop_command=$?

		get_zone_state ${Zonename}

		debug_message "Function: stop_sczbt - ${Zonename} state is ${ZONE_STATE}"

		if [ "${ZONE_STATE}" != "installed" ]; then
			SECONDS=0
			get_zonepath ${Zonename}
			/usr/cluster/bin/hatimerun -t ${CLEAR_STOP_TIMEOUT} /opt/SUNWsczone/sczbt/bin/clear_zone ${Zonepath} ${RESOURCEGROUP} ${RESOURCE} >>${LOGFILE}
			debug_message "Function: stop_sczbt - ${Zonename} state is ${ZONE_STATE}, time passed while waiting for clear_zone: ${SECONDS}"

			# give some time to let killed processes finish
			${SLEEP} 2

			debug_message "Function: stop_sczbt - Halting non-global zone ${Zonename} again"
			${ZONEADM} -z ${Zonename} halt >>${LOGFILE} 2>&1
			rc_stop_command=$?

			get_zone_state ${Zonename}
			if [ "${ZONE_STATE}" != "installed" ]; then
				# SCMSGS
				# @explanation
				# The non-global zones shutdown command did not
				# complete in time or zoneadm halt was not able
				# to bring the non-global zone into state
				# "installed". In order to prevent failed
				# starts of the non-global zone on this
				# node, manual cleanup is necessary.
				# @user_action
				# Follow the Solaris System Administrator Guide
				# for Zones to bring the non-global zone into
				# state "installed" before trying to restart
				# the resource on this node.
				scds_syslog -p daemon.err -t $(syslog_tag) -m \
				 "Function: stop_sczbt - Manual intervention needed for non-global zone %s - unable to get into state installed." \
				 "${Zonename}"
			else
				debug_message "Function: stop_sczbt - ${Zonename} state is ${ZONE_STATE}"
				echo "Successfully halted the non-global zone ${Zonename}, it is in state ${ZONE_STATE}" >>${LOGFILE} 2>&1
				# SCMSGS
				# @explanation
				# The second zoneadm halt was able to get the
				# non-global zone into state "installed".
				# @user_action
				# None
				scds_syslog -p daemon.notice -t $(syslog_tag) -m \
				 "Function: stop_sczbt - The non-global zone %s was successfully halted and reached state %s" \
				 "${Zonename}" "${ZONE_STATE}"
			fi
		fi
	fi

        # Send a kill to any pids under the PMFtag
        ${PMFADM} -s ${RESOURCEGROUP},${RESOURCE},0.svc KILL 2> /dev/null
	
	debug_message "Function: stop_sczbt - End"
	return ${rc_stop_command}
}

check_sczbt()
{
	# 
	# Probe sczbt
	#

        debug_message "Function: check_sczbt - Begin"
	${SET_DEBUG}

	rc_check_sczbt=0

	# check if start program is running

	if /bin/pgrep -z global -f "start_sczbt -R ${RESOURCE} " >/dev/null 2>&1
	then
		debug_message "Function: check_sczbt - Start program is still running "
		rc_check_sczbt=100
		return ${rc_check_sczbt}
	fi

	# do a reduced check as long as the gds_svc_start is running otherwise do an extensive check

	if /bin/pgrep -z global -f "gds_svc_start .*-R ${RESOURCE} " >/dev/null 2>&1
	then

		# check if the scheduler is running
	
		if ! /bin/pgrep -z ${Zonename} zsched>/dev/null 2>&1
		then
			rc_check_sczbt=100
			debug_message "Function: check_sczbt - Early end"
			return ${rc_check_sczbt}
		fi
	
		# check if zone service is online 

		case ${Zonebrand} in
		   native|ipkg)
			get_svc_state ${Zonename} ${Milestone}

			if [ "${SVC_STATE}" != "online"  -a  "${SVC_STATE}" != "degraded" ]
			then
				rc_check_sczbt=100
				debug_message "Function: check_sczbt - ${Zonename} milestone (${Milestone}) state is ${SVC_STATE}"
				return ${rc_check_sczbt}
			fi;;
		   lx)
			if get_lx_state ${Zonename}
			then
				if [ "${LX_STATE}" != "${LXrunlevel}" ]
				then
					rc_check_sczbt=100
					debug_message "Function: check_sczbt - ${Zonename} runlevel (${LXrunlevel}) state is ${LX_STATE}"
					return ${rc_check_sczbt}
				fi
			else
				rc_check_sczbt=100
				debug_message "Function: check_sczbt - runlevel did not return successfully (return code ${rc_get_lx_state}) for the non-global zone {Zonename}"
				return ${rc_check_sczbt}
			fi;;

		   solaris8|solaris9)
			if get_solaris_legacy_state ${Zonename}
			then
				if [ "${SOLARIS_LEGACY_STATE}" != "${SLrunlevel}" ]
				then
					rc_check_sczbt=100
					debug_message "Function: check_sczbt - ${Zonename} runlevel (${SLrunlevel}) state is ${SOLARIS_LEGACY_STATE}"
					return ${rc_check_sczbt}
				fi
			else
				rc_check_sczbt=100
				debug_message "Function: check_sczbt - runlevel did not return successfully (return code ${rc_get_solaris_legacy_state}) for the non-global zone {Zonename}"
				return ${rc_check_sczbt}
			fi;;
		esac

	else

		# check if the scheduler is running

		if ! /bin/pgrep -z ${Zonename} zsched>/dev/null 2>&1
		then
	             	# SCMSGS
	             	# @explanation
	             	# The zsched prcess of the referenced zone is not
	             	# running. The zone is not operable.
	             	# @user_action
	             	# None. The resource will be restarted or the resource
	             	# group will be failed over.
	             	scds_syslog -p daemon.err -t $(syslog_tag) -m \
	               		"Function: check_sczbt - Zone scheduler for %s not running" \
				"${Zonename}" 
			rc_check_sczbt=100
			debug_message "Function: check_sczbt - Early end"
			return ${rc_check_sczbt}
		fi
	
		# check if the ip addresses of the logical hoists are online
		
		LH_LIST=`${SCHA_RESOURCE_GET} -O NETWORK_RESOURCES_USED -R ${RESOURCE} -G ${RESOURCEGROUP}`

		if [ "${LH_LIST}" ]
		then

			ip_config=`${IFCONFIG} -a`

			for LH in ${LH_LIST}
			do
   				get_ipmp_hosts ${LH}

				for i in ${IPMP_HOST_IP}
				do
					if ! echo ${ip_config} | /bin/grep -w ${i} >/dev/null 2>&1
	   				then
		             			# SCMSGS
		             			# @explanation
		             			# The referenced zones ip
		             			# address is unplumbed.
		             			# @user_action
		             			# None. The resource will be
		             			# restarted or the resource
		             			# group will be failed over.
		             			scds_syslog -p daemon.err -t $(syslog_tag) -m \
		               				"Function: check_sczbt - Zones %s ip address %s is not running" \
							"${Zonename}" "${i}"
	         				rc_check_sczbt=201
						debug_message "Function: check_sczbt - immediate failover"
						
						return ${rc_check_sczbt}
	   				fi
				done
			done
		fi

		# check if zone service is online if not try for 60 percent of the probe timout

		# RETRIEVE PROBE_TIMEOUT

		PROBE_TIMEOUT=`${SCHA_RESOURCE_GET} -O Extension -R ${RESOURCE} -G ${RESOURCEGROUP} PROBE_TIMEOUT |tail -1`

		# 60 % OF THE PROBE_TIMEOUT CAN BE SPEND ON WAITING

		MAX_PROBE_TIMEOUT=`expr ${PROBE_TIMEOUT} \* 60 \/ 100`
		SECONDS=0

		# test for 60 percent of the probe timeoput if the zone is online
		case ${Zonebrand} in
		   native|ipkg)
			while [ ${SECONDS} -lt ${MAX_PROBE_TIMEOUT} ]
			do
				get_svc_state ${Zonename} ${Milestone}

				if [ "${SVC_STATE}" != "online" -a "${SVC_STATE}" != "degraded" ]
				then
       			      		# SCMSGS
       			      		# @explanation
       			      		# The milestone is not online or degraded. The
       			      		# state is checked again in 5 seconds.
       			      		# @user_action
       			      		# None.
       			      		scds_syslog -p daemon.err -t $(syslog_tag) -m \
       		        			"Function: check_sczbt - %s Milestone [%s] not online, try again in 5 seconds" \
						"${Zonename}" "${Milestone}"
					rc_check_sczbt=100
					${SLEEP} 5
				else
					SECONDS=${MAX_PROBE_TIMEOUT}
					rc_check_sczbt=0
				fi
			done;;

		   lx)
			while [ ${SECONDS} -lt ${MAX_PROBE_TIMEOUT} ]
			do
				if get_lx_state ${Zonename}
				then
					if [ "${LX_STATE}" != "${LXrunlevel}" ]
					then
						# SCMSGS
						# @explanation
						# The runlevel is not equal to the
						# configured LXrunlevel. The state is
						# checked again in 5 seconds.
						# @user_action
						# None.
						scds_syslog -p daemon.err -t $(syslog_tag) -m \
							"Function: check_sczbt - %s runlevel [%s] not online, runlevel is %s, try again in 5 seconds" \
							"${Zonename}" "${LXrunlevel}" "${LX_STATE}"
						rc_check_sczbt=100
						${SLEEP} 5
					else
						SECONDS=${MAX_PROBE_TIMEOUT}
						rc_check_sczbt=0
					fi
				else
					# SCMSGS
					# @explanation
					# The runlevel command did not run
					# successfully in the configured
					# non-global zone. The state is
					# checked again in 5 seconds.
					# @user_action
					# None.
					scds_syslog -p daemon.err -t $(syslog_tag) -m \
						"Function: check_sczbt - runlevel did not run successfully (return code %s) in the non-global zone %s, try again in 5 seconds" \
						"${rc_get_lx_state}" "${Zonename}"
					rc_check_sczbt=100
					${SLEEP} 5
				fi
			done;;

		   solaris8|solaris9)
			while [ ${SECONDS} -lt ${MAX_PROBE_TIMEOUT} ]
			do
				if get_solaris_legacy_state ${Zonename}
				then
					if [ "${SOLARIS_LEGACY_STATE}" != "${SLrunlevel}" ]
					then
						# SCMSGS
						# @explanation
						# The legacy runlevel is not
						# equal to the configured
						# SLrunlevel. The state is
						# checked again in 5 seconds.
						# @user_action
						# None.
						scds_syslog -p daemon.err -t $(syslog_tag) -m \
							"Function: check_sczbt - %s legacy runlevel [%s] not online, runlevel is %s, try again in 5 seconds" \
							"${Zonename}" "${SLrunlevel}" "${SOLARIS_LEGACY_STATE}"
						rc_check_sczbt=100
						${SLEEP} 5
					else
						SECONDS=${MAX_PROBE_TIMEOUT}
						rc_check_sczbt=0
					fi
				else
					# SCMSGS
					# @explanation
					# The who -r command did not run
					# successfully in the configured
					# non-global zone. The state is
					# checked again in 5 seconds.
					# @user_action
					# None.
					scds_syslog -p daemon.err -t $(syslog_tag) -m \
						"Function: check_sczbt - who -r did not run successfully (return code %s) in the non-global zone %s, try again in 5 seconds" \
						"${rc_get_solaris_legacy_state}" "${Zonename}"
					rc_check_sczbt=100
					${SLEEP} 5
				fi
			done;;

		esac
	fi

	debug_message "Function: check_sczbt - End"
	return ${rc_check_sczbt}
}
