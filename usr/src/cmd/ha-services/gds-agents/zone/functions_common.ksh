#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)functions_common.ksh	1.7	08/07/25 SMI"
#

SCHA_RESOURCE_GET=/usr/cluster/bin/scha_resource_get
SCHA_CLUSTER_GET=/usr/cluster/bin/scha_cluster_get
SCRGADM=/usr/cluster/bin/scrgadm
SCSTAT=/usr/cluster/bin/scstat

ZONEADM=/usr/sbin/zoneadm
ZONECFG=/usr/sbin/zonecfg
ZLOGIN=/usr/sbin/zlogin

PMFADM=/usr/cluster/bin/pmfadm
CLINFO=/usr/sbin/clinfo
IFCONFIG=/usr/sbin/ifconfig
PING=/usr/sbin/ping

get_zone_state()
{
        # Elaborates the zone state of a given zone
        # returns the state in the variable ZONE_STATE

        debug_message "Function: get_zone_state - Begin"
        ${SET_DEBUG}

        ZONE_STATE=`${ZONEADM} -z ${1} list -p | awk -F: '{print $3}'`
	rc_get_zone_state=$?

        debug_message "Function: get_zone_state - End"
	return ${rc_get_zone_state}
}

get_svc_state()
{
	# Elaborates the status of a given smf service in a zone
	# returns the state in the variable SVC_STATE

        debug_message "Function: get_svc_state - Begin"
	${SET_DEBUG}

	SVC_STATE=`(${ZLOGIN} ${1} /bin/svcs -H ${2} 2> /dev/null ) | /bin/awk '{print $1}'`
	rc_get_svc_state=$?

	debug_message "Function: get_svc_state - End"
	return ${rc_get_svc_state}
}

get_lx_state()
{
	# Elaborates the status of a given Linux runlevel in a zone
	# returns the state in the variable LX_STATE

	debug_message "Function: get_lx_state - Begin"
	${SET_DEBUG}

	LX_STATE=`(${ZLOGIN} ${1} /sbin/runlevel ) | awk '{print $2}'`
	rc_get_lx_state=$?

	debug_message "Function: get_lx_state - End"
	return ${rc_get_lx_state}
}

get_solaris_legacy_state()
{
	# Elaborates the status of a given Solaris legacy runlevel in a zone
	# returns the state in the variable SOLARIS_LEGACY_STATE

	debug_message "Function: get_solaris_legacy_state - Begin"
	${SET_DEBUG}

	SOLARIS_LEGACY_STATE=`(${ZLOGIN} ${1} /bin/who -r ) | awk '{print $3}'`
	rc_get_solaris_legacy_state=$?

	debug_message "Function: get_solaris_legacy_state - End"
	return ${rc_get_solaris_legacy_state}
}

val_parfile()
{
	# Common valdation for parameter file
	
        debug_message "Function: val_parfile - Begin"
	${SET_DEBUG}

	rc_val_pfile=0

        # Validate that parameter file exists

	PARFILE=${1}
        PARLIST=${2}
        PARDIR=${3}

        if [ ! -d "${PARDIR}" ]; then
                # SCMSGS
                # @explanation
                # The parameter file directory you specified in the config
                # file does not exist.
                # @user_action
                # Fix the appropriate config file, either sczbt_config or
                # sczsh_config with an existing directory.
                scds_syslog -p daemon.err -t $(syslog_tag) -m \
        	  "Function: val_parfile - Directory %s does not exist or is not a directory" \
                  "${PARDIR}"
                rc_val_parfile=1
	else
		debug_message "Function: val_parfile - ${PARDIR} exists"	
 	fi

        if [ ! -f "${PARFILE}" ]; then
                scds_syslog -p daemon.err -t $(syslog_tag) -m \
                  "Function: val_parfile - File %s does not exist" \
                  "${PARFILE}"
                rc_val_parfile=1
	else
		debug_message "Function: val_parfile - ${PARFILE} exists"	
 	fi

	if ! ksh -n ${PARFILE} >/dev/null 2>&1
	then
        	scds_syslog -p daemon.err -t $(syslog_tag) -m \
                  "Function: validate - Syntax errors in %s" \
               	  "${PARFILE}"
               rc_val_parfile=1
	else
		debug_message "Function: val_parfile - validated ${PARFILE}"	
       	fi

	# Test if all the mandatory variables are included and set correctly in the parameter file

	PARAMETERS=`cat ${PARFILE} |grep -v "^#"|grep -v "^ "|nawk -F= '{print $1}'`

	for i in ${PARLIST}
	do
		if ! `echo ${PARAMETERS} |grep ${i} > /dev/null `; then
                	scds_syslog -p daemon.err -t $(syslog_tag) -m \
                          "Function: val_parfile - %s not specified in %s, which is required" \
                          "${i}" "${PARFILE}"
		rc_val_parfile=1
		else
			debug_message "Function: val_parfile - ${i} included in ${PARFILE}"	
                fi
	done

	debug_message "Function: val_parfile - End"
	return ${rc_val_parfile}
}

val_in_global()
{
	# Validate that we are in the global zone
	
        debug_message "Function: val_in_global - Begin"
	${SET_DEBUG}

	rc_val_in_global=0

	if ! /usr/bin/zonename | grep "^global$" >/dev/null 2>&1
	then
   		# SCMSGS
   		# @explanation
   		# A start, stop or probe command was used from the local zone.
   		# @user_action
   		# Use the start, stop and probe commands from th eglobal zone
   		# only.
   		scds_syslog -p daemon.err -t $(syslog_tag) -m \
		  "Function: val_in_global - Not in the global zone" 
		rc_val_in_global=1
	else
		debug_message "Function: val_in_global - Global zone"
	fi

	debug_message "Function: val_in_global - End"
	return ${rc_val_in_global}
}
