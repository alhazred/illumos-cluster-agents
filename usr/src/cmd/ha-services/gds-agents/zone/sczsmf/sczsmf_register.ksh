#!/usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)sczsmf_register.ksh	1.4	07/06/06 SMI"
#
#  This script has the optional -f option:
#  -f <filename> sources a user definable config file different from
#  sczsmf_config.
#

MYCONFIG=""
MYNAME=`basename $0`

typeset opt

while getopts 'f:' opt
do
	case "${opt}" in
		f)	MYCONFIG=${OPTARG};;
		*)	echo "ERROR: ${MYNAME} Option ${OPTARG} unknown - early End. Only the option -f <filename> is valid."
			exit 1;;
	esac
done

if [ -n "${MYCONFIG}" ] && [ -f "${MYCONFIG}" ]
then
	echo "sourcing ${MYCONFIG}"
	. ${MYCONFIG}
else
	PKGCONF=`dirname $0`/sczsmf_config
	echo "sourcing ${PKGCONF}"
	. ${PKGCONF}
fi

RES_STATE=`/usr/cluster/bin/scha_resource_get -O RESOURCE_STATE -R ${SCZBT_RS}`

if [ "${RES_STATE}" != "ONLINE" ]
then
        echo "Registration and Validation requires that ${SCZBT_RS} is ONLINE"
        echo "Currently ${SCZBT_RS} is ${RES_STATE}, please fix ${SCZBT_RS} first"
        exit 1
fi

if [ -z "${ZONE}" ]
then
	echo "Error: ZONE not set, this is required"
	exit 1
fi

# Set OPTIONS

if [ "${RECURSIVE}" = "true" ]; then
	OPTIONS=tr
elif [ "${RECURSIVE}" = "false" ]; then
	OPTIONS=t
elif [ "${RECURSIVE}" != "true" -a "${RECURSIVE}" != "false" ]; then
	echo "Error: Invalid RECURSIVE value"
	exit 1
fi

if [ "${STATE}" = "true" ]; then
	OPTIONS=${OPTIONS}s
elif [ "${STATE}" = "false" ]; then
	continue
elif [ "${STATE}" != "true" -a "${STATE}" != "false" ]; then
	echo "Error: Invalid STATE value"
	exit 1
fi

if [ "${SERVICE_PROBE}" ]
then
	SERVICE_PROBE="-P ${SERVICE_PROBE}"
else
	SERVICE_PROBE="-P /usr/bin/true"
fi

/usr/cluster/bin/scrgadm -a -j ${RS} -g ${RG} -t SUNW.gds \
-x Start_command="/opt/SUNWsczone/sczsmf/bin/start_sczsmf \
-R ${RS} -G ${RG} -Z ${ZONE} -S ${SERVICE} -O ${OPTIONS} '${SERVICE_PROBE}' " \
-x Stop_command="/opt/SUNWsczone/sczsmf/bin/stop_sczsmf \
-R ${RS} -G ${RG} -Z ${ZONE} -S ${SERVICE} -O ${OPTIONS} '${SERVICE_PROBE}' " \
-x Probe_command="/opt/SUNWsczone/sczsmf/bin/probe_sczsmf \
-R ${RS} -G ${RG} -Z ${ZONE} -S ${SERVICE} -O ${OPTIONS} '${SERVICE_PROBE}' " \
-x Stop_signal=9 -x Network_aware=false \
-x Failover_enabled=false  -y Failover_mode=NONE \
-y Resource_dependencies_restart=${SCZBT_RS}

St=$?

if [ "${St}" -ne 0 ]; then
        echo "Registration of resource ${RS} failed, please correct the wrong parameters."
        exit 1
else
        echo "Registration of resource ${RS} succeeded."
fi

# VALIDATE RESOURCE

/opt/SUNWsczone/sczsmf/bin/validate_sczsmf -R ${RS} -G ${RG} -Z ${ZONE} -S ${SERVICE} -O ${OPTIONS} ${SERVICE_PROBE}

St=$?

if [ "${St}" -ne 0 ]; then
        echo "Validation of resource ${RS} failed, check the syslog for the wrong parameters."
        echo "Removing resource ${RS} from the cluster configuration."

        /usr/cluster/bin/scrgadm -r -j ${RS}
        exit 1
else
        echo "Validation of resource ${RS} succeeded."
fi

exit 0
