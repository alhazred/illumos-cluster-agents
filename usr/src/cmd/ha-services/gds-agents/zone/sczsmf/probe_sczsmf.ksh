#!/usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)probe_sczsmf.ksh	1.5	07/06/06 SMI"
#

MYNAME=`basename ${0}`
MYDIR=`dirname ${0}`

typeset opt

while getopts 'R:G:Z:S:O:P:' opt
do
        case "${opt}" in
                R)      RESOURCE=${OPTARG};;
                G)      RESOURCEGROUP=${OPTARG};;
                Z)      ZONE=${OPTARG};;
                S)      SERVICE=${OPTARG};;
                O)      OPTIONS=${OPTARG};;
                P)      SERVICE_PROBE=${OPTARG};;
                *)      exit 1;;
        esac
done

. ${MYDIR}/../etc/config                    # debug flags
. ${MYDIR}/functions                        # agent functions
. ${MYDIR}/../../lib/functions              # common functions
. ${MYDIR}/../../lib/functions_common       # common functions

validate_options

debug_message "Method: ${MYNAME} - Begin"
${SET_DEBUG}

#if ! validate
#then
#        debug_message "Method: ${MYNAME} - End (Exit 1)"
#        exit 1
#fi
#
check_sczsmf

debug_message "Method: ${MYNAME} - End (Exit ${rc_check_sczsmf})"
exit ${rc_check_sczsmf}
