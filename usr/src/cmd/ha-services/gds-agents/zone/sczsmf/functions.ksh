#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)functions.ksh	1.11	09/03/06 SMI"
#

validate_options()
{
	debug_message "Function: validate_options - Begin"
	${SET_DEBUG}

	rc_validate_options=0

	for i in RESOURCE RESOURCEGROUP ZONE SERVICE OPTIONS SERVICE_PROBE
	do
	   case $i in
		RESOURCE)
		   if [ -z ${RESOURCE} ]; then
			scds_syslog -p daemon.err -t $(syslog_tag) -m \
			   "Function: validate_options - %s Option %s not set" \
			   "${MYNAME}" "-R"
			rc_validate_options=1
		   fi;;

		RESOURCEGROUP)
		   if [ -z ${RESOURCEGROUP} ]; then
			scds_syslog -p daemon.err -t $(syslog_tag) -m \
			   "Function: validate_options - %s Option %s not set" \
			   "${MYNAME}" "-G"
			rc_validate_options=1
		   fi;;

		ZONE)
		   if [ -z ${ZONE} ]; then
			scds_syslog -p daemon.err -t $(syslog_tag) -m \
			   "Function: validate_options - %s Option %s not set" \
			   "${MYNAME}" "-Z"
			rc_validate_options=1
		   fi;;

		SERVICE)
		   if [ -z ${SERVICE} ]; then
			scds_syslog -p daemon.err -t $(syslog_tag) -m \
			   "Function: validate_options - %s Option %s not set" \
			   "${MYNAME}" "-S"
			rc_validate_options=1
		   fi;;

		OPTIONS)
		   if [ -z ${OPTIONS} ]; then
			scds_syslog -p daemon.err -t $(syslog_tag) -m \
			   "Function: validate_options - %s Option %s not set" \
			   "${MYNAME}" "-O"
			rc_validate_options=1
		   fi;;

		SERVICE_PROBE)
		   if [ -z ${OPTIONS} ]; then
			scds_syslog -p daemon.err -t $(syslog_tag) -m \
			   "Function: validate_options - %s Option %s not set" \
			   "${MYNAME}" "-P"
			rc_validate_options=1
		   fi;;
	   esac
	done

	debug_message "Function: validate_options -returns ${rc_validate_options} - End"
	return ${rc_validate_options}
}

validate()
{
	debug_message "Function: validate - Begin"
	${SET_DEBUG}

	rc_validate=0

	##################
	# Validate ${ZONE}

	ZONEBRAND=`${ZONECFG} -z ${ZONE} info | /bin/grep "^brand:" | /bin/awk '{print $2}'`
	if [ -n "${ZONEBRAND}" ] && [ "${ZONEBRAND}" != "native" -a "${ZONEBRAND}" != "ipkg" ]
	then
		# SCMSGS
		# @explanation
		# The sczsmf component does not work with zones of brand type
		# other than "native" or "ipkg", since they have no SMF feature
		# implemented.
		# @user_action
		# Don't configure the sczsmf component for a zone of brand type
		# other than "native" or "ipkg". Use the sczsh component instead.
		scds_syslog -p daemon.err -t $(syslog_tag) -m \
		  "Function: validate - Zone %s has brand type %s. No SMF available, sczsmf does not work for the configured Zone." \
		  "${ZONE}" "${ZONEBRAND}"
		rc_validate=1

		# every other validation check afterwards will fail, so exit here
		return ${rc_validate}
	fi

	get_zone_state ${ZONE}

	if [ "${ZONE_STATE}" = "installed" -o "${ZONE_STATE}" = "ready" -o "${ZONE_STATE}" = "running" ]
	then
	   debug_message "Function: validate - ${ZONE} state is ${ZONE_STATE}"
	else
	   # SCMSGS
	   # @explanation
	   # The SMF service state is not installed or ready.
	   # @user_action
	   # If the SMF service is required to be managed by the Sun Cluster
	   # the SMF satte must be either installed or ready.
	   scds_syslog -p daemon.err -t $(syslog_tag) -m \
		"Function: validate - %s state is %" \
		"${ZONE}" "${ZONE_STATE}"
	   rc_validate=1
	fi
		
	#####################
	# Validate ${SERVICE}

	# Validate the service exists and any services it's dependent on are online
	# Output any svcs errors to ${LOGFILE}
	# Output any dependents and their state to ${STATE}
	# Note: The resource will not be allowed to register if validate fails

	if STATE=$(env LANG=POSIX ${ZLOGIN} ${ZONE} svcs -Hd ${SERVICE} 2> ${LOGFILE} | /usr/bin/awk '{if ($1 != "online") print $3 " which is " $1}')
	then
	   if [ -s "${LOGFILE}" ]
	   then
		# e.g. svcs: Pattern 'X' doesn't match any instances
		log_message error validate
		rc_validate=1
	   fi

	   if [ "$STATE" ]
	   then
		# e.g. X is dependent on Y which is disbaled
		/usr/bin/echo "${SERVICE} is dependent on $STATE" > ${LOGFILE}
		log_message error validate
		rc_validate=1
	   else
		debug_message "Function: validate - ${SERVICE} is valid - dependent services are online"
	   fi
	else
	   # SCMSGS
	   # @explanation
	   # The SMF service state for %s is not known.
	   # @user_action
	   # Review the /opt/SUNWsczone/sczsmf/util/sczsmf_config
	   # configuration file and make sure you have specified the correct
	   # SMF service name.
	   scds_syslog -p daemon.err -t $(syslog_tag) -m \
		"Function: validate - Failied to get serivce state for %s" \
		"${SERVICE}"
	   rc_validate=1
	fi

	###########################
	# Validate ${SERVICE_PROBE}

	myprobecmd=`/usr/bin/echo ${SERVICE_PROBE} | /usr/bin/awk '{print $1}'`

	if ${ZLOGIN} ${ZONE} /usr/bin/test -x ${myprobecmd}
	then
	   debug_message "Function: validate - ${myprobecmd} is executable"
	else
	   # SCMSGS
	   # @explanation
	   # The Zone SMF Probe Script is not executable.
	   # @user_action
	   # Review the /opt/SUNWsczone/sczsmf/util/sczsmf_config
	   # configuration file and make sure you have specified the correct
	   # SERVICE_PROBE.
	   scds_syslog -p daemon.err -t $(syslog_tag) -m \
		"Function: validate - %s is not executable" \
		"${myprobecmd}"
	   rc_validate=1
	fi	

	debug_message "Function: validate - End"
	return ${rc_validate}
}

check_start_sczsmf()
{
	debug_message "Function: check_start_sczsmf - Begin"
	${SET_DEBUG}

	rc_check_start_sczsmf=1

	${ZLOGIN} ${ZONE} /usr/bin/diff /tmp/${RESOURCE}_start_CTID /tmp/${RESOURCE}_probe_CTID > /dev/null 2>&1
	rc=$?

	case ${rc} in
	   1)	${ZLOGIN} ${ZONE} /usr/bin/rm /tmp/${RESOURCE}_start_CTID 2> /dev/null
		${ZLOGIN} ${ZONE} /usr/bin/rm /tmp/${RESOURCE}_probe_CTID 2> /dev/null
		
		debug_message "Function: start_sczsmf - NO-OP START being performed"
		rc_check_start_sczsmf=0
		;;
	   *)	${ZLOGIN} ${ZONE} /usr/bin/rm /tmp/${RESOURCE}_start_CTID 2> /dev/null
		${ZLOGIN} ${ZONE} /usr/bin/rm /tmp/${RESOURCE}_probe_CTID 2> /dev/null
		;;
	esac

	START_TIMEOUT=`${SCHA_RESOURCE_GET} -O START_TIMEOUT -R ${RESOURCE} -G ${RESOURCEGROUP}`
	/usr/bin/sleep ${START_TIMEOUT} &

	if /usr/cluster/bin/pmfadm -q ${RESOURCEGROUP},${RESOURCE},0.svc
	then
	   /usr/cluster/bin/pmfadm -s ${RESOURCEGROUP},${RESOURCE},0.svc	
	fi

	debug_message "Function: check_start_sczsmf - End"
	return ${rc_check_start_sczsmf}
}

start_sczsmf()
{
	debug_message "Function: start_sczsmf - Begin"
	${SET_DEBUG}

	${ZLOGIN} ${ZONE} /usr/sbin/svcadm enable -${OPTIONS} ${SERVICE} > $LOGFILE 2>&1
	rc_start_sczsmf=$?
	
	debug_message "Function: start_sczsmf - End"
	return ${rc_start_sczsmf}
}

stop_sczsmf()
{
	debug_message "Function: stop_sczsmf - Begin"
	${SET_DEBUG}

	${ZLOGIN} ${ZONE} /usr/bin/diff /tmp/${RESOURCE}_start_CTID /tmp/${RESOURCE}_probe_CTID > /dev/null 2>&1
	rc=$?

	if [ "${rc}" = 1 ]
	then
	   debug_message "Function: stop_sczsmf - NO-OP STOP being performed"
	else
	   OPTIONS=`/usr/bin/echo ${OPTIONS} | tr -d "r"`

	   if ${ZLOGIN} ${ZONE} /usr/sbin/svcadm disable -${OPTIONS} ${SERVICE} > $LOGFILE 2>&1
	   then
		rc_stop_sczsmf=0

		STOP_TIMEOUT=`${SCHA_RESOURCE_GET} -O STOP_TIMEOUT -R ${RESOURCE} -G ${RESOURCEGROUP}`
		MAX_STOP_TIMEOUT=`/usr/bin/expr ${STOP_TIMEOUT} \* 60 \/ 100`

		while  [ "${MAX_STOP_TIMEOUT}" -ge 2 ]
		do
		   /usr/bin/sleep 2
		   if get_svc_state ${ZONE} ${SERVICE}
		   then
			debug_message "Function stop_sczsmf: ${SERVICE} state is ${SVC_STATE}, timeout ${MAX_STOP_TIMEOUT}"

			if [ "${SVC_STATE}" = "disabled" ]
			then
			   MAX_STOP_TIMEOUT=0
			else
			   MAX_STOP_TIMEOUT=`/usr/bin/expr ${MAX_STOP_TIMEOUT} - 3`
			fi
		   else
			MAX_STOP_TIMEOUT=`/usr/bin/expr ${MAX_STOP_TIMEOUT} - 3`
		   fi
		done
	   else
		rc_stop_sczsmf=1
	   fi

	   if [ "${SVC_STATE}" != "disabled" ] -o [ "${rc_stop_sczsmf}" = 1 ]
	   then
		if PIDS=`${ZLOGIN} ${ZONE} /usr/bin/svcs -o fmri -p ${SERVICE} | /usr/bin/awk '{if (NF == 3) print $2}'`
		then
		   # Kill the service pids as reported by svcs
		   for i in `/usr/bin/echo $PIDS`
		   do
			/usr/bin/kill -9 $i
		   done

		   /usr/bin/echo "${SERVICE} pids killed as reported by svcs" > ${LOGFILE}
		else
		   # Kill the service pids as reported by pgrep
		   /usr/bin/pkill -9 -f -z ${ZONE} ${SERVICE}

		   /usr/bin/echo "${SERVICE} pids killed as reported by pgrep" > ${LOGFILE}
		fi
	   fi
	fi

	if /usr/cluster/bin/pmfadm -q ${RESOURCEGROUP},${RESOURCE},0.svc
	then
	   /usr/cluster/bin/pmfadm -s ${RESOURCEGROUP},${RESOURCE},0.svc KILL 2> /dev/null
	fi

	debug_message "Function: stop_sczsmf - End"
	return 0
}

check_sczsmf()
{
	debug_message "Function: check_sczsmf - Begin"
	${SET_DEBUG}

	rc_check_sczsmf=0

	if /bin/pgrep -z global -f "start_sczsmf -R ${RESOURCE} " >/dev/null 2>&1
	then
	   debug_message "Function: check_sczsmf - start program is still running "
	   rc_check_sczsmf=100
	else
	   get_svc_state ${ZONE} ${SERVICE}
	   debug_message "Function: check_sczsmf - ${SERVICE}: State is ${SVC_STATE}"

	   if [ "${SVC_STATE}" = "disabled" ]
	   then
		rc_check_sczsmf=100
	   else
		# Don't react if ${SERVICE} is being restarted by the SMF
		# restarter. i.e. An asterisk (*) is appended for instances
		# in transition, e.g. online* or offline*

		if [ "${SVC_STATE}" = "online*" -o "${SVC_STATE}" = "offline*" ]
		then
		   rc_check_sczsmf=0
		else
		   # Check to see if the service's primary contract ID has changed since startup.
		   # As the probe runs within the non-global zone, the saved CTID output will only 
		   # reside within the non-global zone. 

		   # Check if this is a Transient service, i.e. one that does not have a CTID.

		   if [ "`${ZLOGIN} ${ZONE} /usr/bin/svcs -Ho CTID ${SERVICE} | /usr/bin/awk '{print $1}'`" = "-" ]
		   then
			rc_check_sczsmf=0
		   else
			if ${ZLOGIN} ${ZONE} /usr/bin/test -f /tmp/${RESOURCE}_start_CTID 
			then
			   eval ${ZLOGIN} ${ZONE} '"/usr/bin/svcs -Ho CTID ${SERVICE} > /tmp/${RESOURCE}_probe_CTID"'
			else
			   eval ${ZLOGIN} ${ZONE} '"/usr/bin/svcs -Ho CTID ${SERVICE} > /tmp/${RESOURCE}_start_CTID"'
			fi

			if ${ZLOGIN} ${ZONE} /usr/bin/test -f /tmp/${RESOURCE}_probe_CTID
			then
			   ${ZLOGIN} ${ZONE} /usr/bin/diff /tmp/${RESOURCE}_start_CTID /tmp/${RESOURCE}_probe_CTID > /dev/null 2>&1
			   rc=$?
			
			   if [ "${rc}" = 1 ]
			   then
	 			# Although the default SMF master restarter has restarted the service we must pass that event
				# back to RGM to process any Sun Cluster resources that have Resource_dependencies_restart set
				# against this resource. 
				# 
				# However, we do not need to reprocess the restart just performed by SMF, instead we signal back 
				# to RGM that a failure has occurred which in turn will trigger the STOP and START methods of this 
				# resource. However, for this restart we will simply perform a "no-op restart", i.e. The STOP and 
				# START methods will indicate a successful return.
				# 
				# Doing a "no-op restart" will cause the restart to percolate through RGM and subsequently trigger
	
				rc_check_sczsmf=100
			   fi
		   	fi
		   fi

		   if [ "${rc_check_sczsmf}" = 0 ]
		   then
			${ZLOGIN} ${ZONE} ${SERVICE_PROBE} >/dev/null
			rc_check_sczsmf=$?
		   fi
		fi
	   fi
	fi

	debug_message "Function: check_sczsmf - End"
	return ${rc_check_sczsmf}
}
