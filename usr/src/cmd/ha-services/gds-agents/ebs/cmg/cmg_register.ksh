#!/usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)cmg_register.ksh	1.6	08/08/06 SMI"
#
# Note: Port_List=23/tcp is a required entry but ignored

. `dirname $0`/cmg_config

[ ${#ORASVR_RS} -ne 0 ] && ORASVR_RS="${ORASVR_RS}{ANY_NODE}"
[ ${#ORALSR_RS} -ne 0 ] && ORALSR_RS="${ORALSR_RS}{ANY_NODE}"

scrgadm -a -j ${RS} -g ${RG} -t SUNW.gds \
-x Start_command="/opt/SUNWscebs/cmg/bin/start_cmg \
-R '${RS}' -G '${RG}' -C '${COMNTOP}' -U '${APPSUSER}' -P '${APPS_PASSWD}' \
-V '${VERSION}' -S '${APP_SID}' -O '${ORACLE_HOME}' -L '${CON_LIMIT}' " \
-x Stop_command="/opt/SUNWscebs/cmg/bin/stop_cmg \
-R '${RS}' -G '${RG}' -C '${COMNTOP}' -U '${APPSUSER}' -P '${APPS_PASSWD}' \
-V '${VERSION}' -S '${APP_SID}' -O '${ORACLE_HOME}' -L '${CON_LIMIT}' " \
-x Probe_command="/opt/SUNWscebs/cmg/bin/probe_cmg \
-R '${RS}' -G '${RG}' -C '${COMNTOP}' -U '${APPSUSER}' -P '${APPS_PASSWD}' \
-V '${VERSION}' -S '${APP_SID}' -O '${ORACLE_HOME}' -L '${CON_LIMIT}' " \
-x Validate_command="/opt/SUNWscebs/cmg/bin/validate_cmg \
-R '${RS}' -G '${RG}' -C '${COMNTOP}' -U '${APPSUSER}' -P '${APPS_PASSWD}' \
-V '${VERSION}' -S '${APP_SID}' -O '${ORACLE_HOME}' -L '${CON_LIMIT}' " \
-y Port_list=23/tcp -x Stop_signal=9 \
-y Resource_dependencies_restart=${HAS_RS},${LH},${LSR_RS},${ORASVR_RS},${ORALSR_RS}
