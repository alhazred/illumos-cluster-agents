#!/usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident "@(#)opmn_register.ksh 1.5     09/01/21 SMI"
#
# Note: Port_List=23/tcp is a required entry but ignored

. `dirname $0`/opmn_config

case `/usr/bin/echo ${OPMN_COMPONENTS} |  /usr/bin/tr "/" " " | /usr/bin/tr '[:upper:]' '[:lower:]'` in
	all|opmn|http_server|oacore|forms|oafm) continue;;
	*)	/usr/bin/printf "Unexpected error OPMN_COMPONENTS is invalid\n"
		exit 1;;
esac

scrgadm -a -j ${RS} -g ${RG} -t SUNW.gds \
-x Start_command="/opt/SUNWscebs/opmn/bin/start_opmn \
-R '${RS}' -G '${RG}' -C '${COMNTOP}' -U '${APPSUSER}' -P '${APPS_PASSWD}' \
-V '${VERSION}' -S '${APP_SID}' -X '${OPMN_COMPONENTS}' -H '${HOSTNAME}' " \
-x Stop_command="/opt/SUNWscebs/opmn/bin/stop_opmn \
-R '${RS}' -G '${RG}' -C '${COMNTOP}' -U '${APPSUSER}' -P '${APPS_PASSWD}' \
-V '${VERSION}' -S '${APP_SID}' -X '${OPMN_COMPONENTS}' -H '${HOSTNAME}' " \
-x Probe_command="/opt/SUNWscebs/opmn/bin/probe_opmn \
-R '${RS}' -G '${RG}' -C '${COMNTOP}' -U '${APPSUSER}' -P '${APPS_PASSWD}' \
-V '${VERSION}' -S '${APP_SID}' -X '${OPMN_COMPONENTS}' -H '${HOSTNAME}' " \
-x Validate_command="/opt/SUNWscebs/opmn/bin/validate_opmn \
-R '${RS}' -G '${RG}' -C '${COMNTOP}' -U '${APPSUSER}' -P '${APPS_PASSWD}' \
-V '${VERSION}' -S '${APP_SID}' -X '${OPMN_COMPONENTS}' -H '${HOSTNAME}' " \
-y Port_list=23/tcp -x Stop_signal=9 \
-y Resource_dependencies_restart=${HAS_RS},${LH}
