#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)functions.ksh	1.11	09/01/21 SMI"
#

PKG=SUNWscebs
MYNAME=`basename $0`
MYDIR=`dirname $0`
MYTMPDIR=/var/tmp
LOGFILE=/var/tmp/${RESOURCE}_logfile
TMPFILE=/var/tmp/${RESOURCE}_sqlout
SCLOGGER=/usr/cluster/lib/sc/scds_syslog

syslog_tag()
{
	${SET_DEBUG}
	print "SC[${PKG:-??}.${COMPONENT:-??}.${METHOD:-??}]:${RESOURCEGROUP:-??}:${RESOURCE:-??}"
}

scds_syslog()
{
	${SCLOGGER} "$@" &
}

debug_message()
{
	if [ "${DEBUG}" = "${RESOURCE}" -o "${DEBUG}" = "ALL" ]
	then
	   SET_DEBUG="set -x"
	   DEBUG_TEXT=${1}

	   scds_syslog -p daemon.debug -t $(syslog_tag) -m \
	      "%s" "$DEBUG_TEXT"
        else
	   SET_DEBUG=
        fi
}

log_message()
{
	debug_message "Function: log_message - Begin"
	${SET_DEBUG}

	if [ -s "${LOGFILE}" ]
	then
	   PRIORITY=$1
	   HEADER=$2

	   strings ${LOGFILE} > ${LOGFILE}.copy

	   while read MSG_TXT
	   do
		scds_syslog -p daemon.${PRIORITY} -t $(syslog_tag) -m \
		   "%s - %s" \
		   "${HEADER}" "${MSG_TXT}"
	   done < ${LOGFILE}.copy

	   /usr/bin/cat /dev/null > ${LOGFILE}
	fi

        debug_message "Function: log_message - End"
}

set_redirection()
{
	debug_message "Function: set_redirection - Begin"
	${SET_DEBUG}

	if /usr/bin/getent passwd ${APPSUSER} | /usr/bin/awk -F: '{print $7}' | /usr/bin/grep csh > /dev/null
	then
	   OUTPUT=">& ${LOGFILE}"
	else
	   OUTPUT="> ${LOGFILE} 2>&1"
	fi

	debug_message "Function: set_redirection - End"
}

query_pids()
{
	debug_message "Function: query_pids - Begin"
	${SET_DEBUG}

	# Usage: query_pids "parameter"
	#
	# Currently, query_pids() is used as follows,
	#
	# query_pids "-w FND"
	# query_pids "-c -w FND"
	# query_pids "-w FNDFSFNDFS"
	# query_pids "-w CPMGR"
	# query_pids "-E 'FND|POXCON|RCVOLTM|INCTM'"
	#
	# Output: rc=0 if the query is TRUE, else rc=1
	#         ${pids} contains the queried process ids

	rc=1

	PROCESS=${1}

	if [ -x /sbin/zonename ]
	then
	   pids=`/usr/bin/ps -u ${APPSUSER} -o pid,args,zone | /usr/bin/grep " ${ZONENAME}$" | \
		eval /usr/xpg4/bin/grep ${PROCESS} | /usr/bin/grep -v grep | /usr/bin/awk '{print $1}'`
	else
	   pids=`/usr/bin/ps -u ${APPSUSER} -o pid,args | \
		eval /usr/xpg4/bin/grep ${PROCESS} | /usr/bin/grep -v grep | /usr/bin/awk '{print $1}'`
	fi

	[ "${pids}" ] && rc=0

	debug_message "Function: query_pids - End"
	return ${rc}
}

validate()
{
	debug_message "Function: validate - Begin"
	${SET_DEBUG}

	rc=0

	if [ ! -d "${COMNTOP}" ]
	then
	   # SCMSGS
	   # @explanation
	   # The Oracle E-Business Suite applications directory does not
	   # exist.
	   # @user_action
	   # Check that the correct pathname was entered for the applications
	   # directory when registering the resource and that the directory
	   # exists.
	   scds_syslog -p daemon.err -t $(syslog_tag) -m \
		"Validate - Applications directory %s does not exist" \
		"${COMNTOP}"
	   rc=1
	else
	   debug_message "Validate - Applications directory ${COMNTOP} exists"
	fi

	if [ ! -d "${ADSCRIPT_PATH}" ]
	then
	   scds_syslog -p daemon.err -t $(syslog_tag) -m \
		"Validate - Applications directory %s does not exist" \
		"${ADSCRIPT_PATH}"
	   rc=1
	else
	   debug_message "Validate - Applications directory ${ADSCRIPT_PATH} exists"
	fi

	if /usr/bin/cat /etc/passwd | /usr/bin/awk -F: '{print $1}' | /usr/bin/grep "^${APPSUSER}$" > /dev/null
	then
	   debug_message "Validate - Application user ${APPSUSER} exists"
	else
	   # SCMSGS
	   # @explanation
	   # The Oracle E-Business Suite applications userid was not found in
	   # /etc/passwd.
	   # @user_action
	   # Ensure that a local applications userid is defined on all nodes
	   # within the cluster.
	   scds_syslog -p daemon.warning -t $(syslog_tag) -m \
		"Validate - Application user <%s> does not exist" \
		"${APPSUSER}"
	fi

	if [ "${COMPONENT}" = "cmg" ]
	then
	   if [ -f /opt/SUNWscebs/.${SID}_passwd ]
	   then
		FILEPERM="$(/usr/bin/ls -l /opt/SUNWscebs/.${SID}_passwd | /usr/bin/awk '{print $1,$3}')"

		if [ "${FILEPERM}" != "-r-------- root" ]
		then
		   # SCMSGS
		   # @explanation
		   # The restricted passwd file used to start and stop the Concurrent Manager 
		   # is not restricted to be read only by root.
		   # @user_action
		   # Change the owner and permissions so that the restricted file
		   # is only readable by owner root.
		   scds_syslog -p daemon.warning -t $(syslog_tag) -m \
			"Validate - /opt/SUNWscebs/.%s_passwd is not restricted to be read only by root" \
			"${SID}"
		fi
	   fi

	   if [ ${#APPS_PASSWD} -eq 0 ]
	   then
		# SCMSGS
		# @explanation
		# A password used to start and stop the Concurrent Manager was not set.
		# @user_action
		# Either define the password within /opt/SUNWscebs/cmg/cmg_config as the value
		# to the APPS_PASSWD keyword or define the password within the restricted file
		# /opt/SUNWscebs/.${APP_SID}_passwd that is only readable by root.
		scds_syslog -p daemon.err -t $(syslog_tag) -m \
		   "Validate - Concurrent Manager password not set" 
	 	rc=1
	   fi

	   if [ ! -d "${ORACLE_HOME}" ]
     	   then
	      scds_syslog -p daemon.err -t $(syslog_tag) -m \
		"Validate - ORACLE_HOME directory %s does not exist" \
		"${ORACLE_HOME}"
	      rc=1
	   else
	      debug_message "Validate - ORACLE_HOME directory ${ORACLE_HOME} exists"
	   fi

	   if [ "${CON_LIMIT}" -gt "100" ]
	   then
	      # SCMSGS
	      # @explanation
	      # The value specified for CON_LIMIT is invalid.
	      # @user_action
	      # Either accept the default value of CON_LIMIT=50 or change the
	      # value of CON_LIMIT to be less than or equal to 100 when
	      # registering the resource. CON_LIMIT=50 will now be used.
	      scds_syslog -p daemon.info -t $(syslog_tag) -m \
		"Validate - CON_LIMIT=%s is incorrect, default CON_LIMIT=50 is being used" \
		"${CON_LIMIT}"

	      CON_LIMIT=50
	   else
	      debug_message "Validate - CON_LIMIT=${CON_LIMIT}"
	   fi
	fi

        debug_message "Function: validate - End"
	return ${rc}
}

orderly_stop()
{
	COMPONENT=${1}
	SECONDS=0

	typeset TWOTASK_SID=
	typeset CONTEXT_NAME=
	FOUND=0

	debug_message "Function: orderly_stop_${COMPONENT} - Begin"
	${SET_DEBUG}

	case ${COMPONENT} in
	   	cmg)	PATTERN="FND"
			;;
		cmglsr)	PATTERN="tnslsnr"
			;;
	   	frm) 	PATTERN="-w f60webmx"
			;;
	   	rep) 	PATTERN="-w rwmts60"
			;;
		opmn)   PATTERN="-w opmn"
			;;
	esac

	for func in stopebs cleanup 
	do
	   case ${func} in
		stopebs) 	((FORCE_TIME=${STOP_TIMEOUT} *75/100));;
		cleanup)	((FORCE_TIME=${STOP_TIMEOUT} *5/100));;
	   esac

	   ${func} ${COMPONENT} "${PATTERN}"

	   while [ ${SECONDS} -lt ${FORCE_TIME} ]
	   do
		query_pids "${PATTERN}"
		rc=$?

		if [ "${rc}" -eq 0 ]
		then
		   if [ "${COMPONENT}" = "cmg" ]
		   then
			query_pids "-c FND"
			if [ "${pids}" -le 1 ]
			then
			   # If we are down to the last FND and that
			   # process is CPMGR then it's safe to remove it
			   query_pids "-w CPMGR"
			   /usr/bin/kill ${pids}
			fi
		   fi
		   if [ "${COMPONENT}" = "opmn" ]
		   then
			for pid in ${pids}
			do
				TWOTASK_SID=$(/usr/bin/pargs -e ${pid} \
				    | /usr/bin/grep TWO_TASK \
				        | /usr/bin/awk -F= '{print $2}')
				CONTEXT_NAME=$(/usr/bin/pargs -e ${pid} \
				    | /usr/bin/grep CONTEXT_NAME \
				        | /usr/bin/awk -F= '{print $2}')
				if [ "${TWOTASK_SID}"  = "${SID}" ] && \
				    [ "${CONTEXT_NAME}" = "${APP_SID}" ]
				then
					FOUND=1
				fi
			done
			if [ $FOUND -eq 0 ]
			then
				# Didn't find any pids with matching TWO_TASK
				# and CONTEXT_NAME
				# break out of the loop
				break
			fi
		   fi
		   sleep 2
		else
			break
		fi
	   done
	   if [ $FOUND -eq 0 ]
	   then
		# Didn't find any pids with matching TWO_TASK
		# and CONTEXT_NAME.
		# No need to go to cleanup, break out of
		# the for loop
                break
	   fi


	   # Test if we broke out of the loop or the time
	   # allocated for the function has expired.

	   query_pids "${PATTERN}"
	   rc=$?

	   if [ "${rc}" -ne 0 ]
	   then
		# Break out of running all the functions
		break
	   fi
	done

	debug_message "Function: orderly_stop_${COMPONENT} - End"
	return 0
}
	
cleanup()
{
	COMPONENT=${1}
	PATTERN=${2}
	
	debug_message "Function: cleanup_${COMPONENT} - Begin"
	${SET_DEBUG}

	typeset TWOTASK_SID=
	typeset CONTEXT_NAME=

	query_pids "${PATTERN}"

	for pid in ${pids}
	do
	   TWOTASK_SID=$(/usr/bin/pargs -e ${pid} | /usr/bin/grep TWO_TASK | /usr/bin/awk -F= '{print $2}')
	   CONTEXT_NAME=$(/usr/bin/pargs -e ${pid} | /usr/bin/grep CONTEXT_NAME | /usr/bin/awk -F= '{print $2}')
	   if [ "${TWOTASK_SID}"  = "${SID}" ] && [ "${CONTEXT_NAME}" = "${APP_SID}" ]
	   then
		/usr/bin/kill -9 ${pid}
	   fi
	done

	if [ "${COMPONENT}" = "cmg" ]
	then
	   query_pids "-E 'FND|POXCON|RCVOLTM|INCTM'"

	   for pid in ${pids}
	   do
		TWOTASK_SID=$(/usr/bin/pargs -e ${pid} | /usr/bin/grep TWO_TASK | /usr/bin/awk -F= '{print $2}')
		[ "${TWOTASK_SID}"  = "${SID}" ] && /usr/bin/kill -9 ${pid}
	   done
	fi

	debug_message "Function: cleanup_${COMPONENT} - End"
}

startebs()
{
	COMPONENT=${1}

	debug_message "Function: startebs ${COMPONENT} - Begin"
	${SET_DEBUG}
	
	/usr/bin/rm ${LOGFILE}

	# Turn off PMF restart if the Component has been manually started
	checkebs ${COMPONENT} 
	rc=$?

	if [ "${rc}" -eq 0 ]
	then
	   /usr/bin/sleep 120 &
	   /usr/cluster/bin/pmfadm -s ${RESOURCEGROUP},${RESOURCE},0.svc

	   # SCMSGS
	   # @explanation
	   # The specified Oracle E-Business Suite component has been started
	   # manually.
	   # @user_action
	   # None required. Informational message.
	   scds_syslog -p daemon.notice -t $(syslog_tag) -m \
		"startebs - %s was manually started" \
		"${COMPONENT}" 
	
	   return 0
	fi
		
	# LHOSTNAME and LDPRELOAD are set within control_ebs

	case ${COMPONENT} in
		cmg)	su ${APPSUSER} -c "${TASK_COMMAND} env ${LHOSTNAME} env ${LDPRELOAD} ${ADSCRIPT_PATH}/adcmctl.sh start apps/${APPS_PASSWD} ${OUTPUT}" > /dev/null
			rc=$?
			;;
	   	cmglsr)	su ${APPSUSER} -c "${TASK_COMMAND} env ${LHOSTNAME} env ${LDPRELOAD} ${ADSCRIPT_PATH}/adalnctl.sh start ${OUTPUT}" > /dev/null
			rc=$?
			;;
	   	frm)	su ${APPSUSER} -c "${TASK_COMMAND} env ${LHOSTNAME} env ${LDPRELOAD} ${ADSCRIPT_PATH}/adfrmctl.sh start ${OUTPUT}" > /dev/null
			rc=$?
			;;
	   	rep)	su ${APPSUSER} -c "${TASK_COMMAND} env ${LHOSTNAME} env ${LDPRELOAD} ${ADSCRIPT_PATH}/adrepctl.sh start ${OUTPUT}" > /dev/null
			rc=$?
			;;
		opmn)	# New for v12 as most services are stared under Oracle Process Manager (OPMN)
			# Note that the Concurrent Manager Listener and Concurrent Manager remain the 
			# same and are not started under OPMN. 
			#
			# Note that we always the OPMN service.

			su ${APPSUSER} -c "${TASK_COMMAND} env ${LHOSTNAME} env ${LDPRELOAD} \
			   ${ADSCRIPT_PATH}/adopmnctl.sh start ${OUTPUT}" > /dev/null
			rc=$?

			OPMN_COMPONENTS=`/usr/bin/echo ${OPMN_COMPONENTS} | /usr/bin/tr "/" " " | /usr/bin/tr '[:upper:]' '[:lower:]'`

			if [ "${OPMN_COMPONENTS}" = "all" ]
			then
				OPMN_COMPONENTS="opmn http_server oacore forms oafm"
			fi

			for component in ${OPMN_COMPONENTS}
			do
			   case ${component} in
				opmn)   # Oracle Process Manager is mandatory and was started earlier
					;;
				http_server) # Oracle HTTP Server
					su ${APPSUSER} -c "${TASK_COMMAND} env ${LHOSTNAME} env ${LDPRELOAD} \
					   ${ADSCRIPT_PATH}/adapcctl.sh start ${OUTPUT}" > /dev/null
					;;
				oacore) # OACORE OC4J Instance
					su ${APPSUSER} -c "${TASK_COMMAND} env ${LHOSTNAME} env ${LDPRELOAD} \
					   ${ADSCRIPT_PATH}/adoacorectl.sh start ${OUTPUT}" > /dev/null
					;;
				forms)  # FORMS OC4J Instance
					su ${APPSUSER} -c "${TASK_COMMAND} env ${LHOSTNAME} env ${LDPRELOAD} \
					   ${ADSCRIPT_PATH}/adformsctl.sh start ${OUTPUT}" > /dev/null
					;;
				oafm)   # OAFM OC4J Instance
					su ${APPSUSER} -c "${TASK_COMMAND} env ${LHOSTNAME} env ${LDPRELOAD} \
					   ${ADSCRIPT_PATH}/adoafmctl.sh start ${OUTPUT}" > /dev/null
					;;
			   esac
			done

			;;
	esac

	[ "${rc}" -ne 0 ] && log_message err startebs ${COMPONENT}

	debug_message "Function: startebs ${COMPONENT} - End"
	return ${rc}
}

stopebs()
{
	COMPONENT=${1}

	debug_message "Function: stopebs ${COMPONENT} - Begin"
	${SET_DEBUG}
	
	typeset TWOTASK_SID=

	# LHOSTNAME and LDPRELOAD are set within control_ebs

	case ${COMPONENT} in
		cmg)	# Manually Stop any FNDFSFNDFS processes

			query_pids "-w FNDFSFNDFS"

			for pid in ${pids}
			do
			   TWOTASK_SID=$(/usr/bin/pargs -e ${pid} | /usr/bin/grep TWO_TASK | /usr/bin/awk -F= '{print $2}')
			   [ "${TWOTASK_SID}"  = "${SID}" ] && /usr/bin/kill -9 ${pid}
			done 
		
			su ${APPSUSER} -c "${TASK_COMMAND} env ${LHOSTNAME} env ${LDPRELOAD} ${ADSCRIPT_PATH}/adcmctl.sh stop apps/${APPS_PASSWD} ${OUTPUT}" > /dev/null
			rc=$?
			;;
	   	cmglsr)	su ${APPSUSER} -c "${TASK_COMMAND} env ${LHOSTNAME} env ${LDPRELOAD} ${ADSCRIPT_PATH}/adalnctl.sh stop ${OUTPUT}" > /dev/null
			rc=$?
			;;
		frm)	su ${APPSUSER} -c "${TASK_COMMAND} env ${LHOSTNAME} env ${LDPRELOAD} ${ADSCRIPT_PATH}/adfrmctl.sh stop ${OUTPUT}" > /dev/null
			rc=$?
			;;
		rep)	su ${APPSUSER} -c "${TASK_COMMAND} env ${LHOSTNAME} env ${LDPRELOAD} ${ADSCRIPT_PATH}/adrepctl.sh stop ${OUTPUT}" > /dev/null
			rc=$?
			;;
		opmn)   su ${APPSUSER} -c "${TASK_COMMAND} env ${LHOSTNAME} env ${LDPRELOAD} ${ADSCRIPT_PATH}/adopmnctl.sh stopall ${OUTPUT}" > /dev/null
			rc=$?
			;;
	esac

	[ "${rc}" -ne 0 ] && log_message err stopebs ${COMPONENT}

	debug_message "Function: stopebs ${COMPONENT} - End"
}

checkebs()
{
	COMPONENT=${1}

	debug_message "Function: checkebs ${COMPONENT} - Begin"
	${SET_DEBUG}

	rc=0

	case ${COMPONENT} in
	   	cmg)	#
			# As wait_for_online processing also calls the probe
			#  we only try the full probe if there are a few 
			#  concurrent manager processes running
			#
	
			query_pids "-c -w FND"

			if [ "${pids}" -le 1 ]
			then
			   return 100
			fi

			#
			# Check for the Internal Manager pid
			#  (Ensures a quick bail out if the IM goes down)
			#

			query_pids "-w CPMGR"
			rc=$?

			if [ "${rc}" -eq 0 ]
			then
			   debug_message "check_cmg - Internal Manager found"
			else
			   return 100
			fi

			# 
			# Test if we can still connect to the database
			#  otherwise we request a restart after  
			#  two failures within the probe_interval
			#

			case "${VERSION}" in
			   12.*)        ORA_ENVFILE=`/usr/bin/grep ORA_ENVFILE= ${ADSCRIPT_PATH}/adalnctl.sh | \
					   /usr/bin/awk -F= '{print $2}' | /usr/bin/tr -d '"'`
		
					TNSADMIN=`/usr/bin/grep TNS_ADMIN= ${ORA_ENVFILE} | \
					   /usr/bin/awk -F= '{print $2}' | /usr/bin/tr -d '"'`

					ORAENV='env ORACLE_HOME=${ORACLE_HOME} env TNS_ADMIN=${TNSADMIN}'
					;;
			   *)           . ${ORACLE_HOME}/${APP_SID}.env
					ORAENV=
					;;
			esac

			/usr/bin/echo exit | eval ${ORAENV} \
       		 	   ${ORACLE_HOME}/bin/sqlplus -s apps/${APPS_PASSWD}@${SID} > ${TMPFILE}

			if /usr/bin/grep ORA- $TMPFILE >/dev/null
			then
			   # SCMSGS
			   # @explanation
			   # While probing the Oracle E-Business Suite
			   # concurrent manager, a test to connect to the
			   # database failed.
			   # @user_action
			   # None, if two successive failures occur the
			   # concurrent manager resource will be restarted.
			   scds_syslog -p daemon.err -t $(syslog_tag) -m \
				"check_cmg - Database connect to %s failed" \
				"${SID}"

			   return 50
			else
			   debug_message "check_cmg - Database connect ${SID} OK"
			fi
	
			#
			# Calculate the number of concurrent processes running as a percentage
			# 	the maximum number of concurrent processes allowed
			#
	
			output="set echo off verify off termout off feedback off head off"

			sqlrun="select count(*) Actual from apps.fnd_v\$process, apps.fnd_concurrent_processes \
			where Pid = Oracle_Process_ID And Spid = Os_Process_ID \
			And (Process_Status_Code in ('A', 'C', 'T'));"

			sqlmax="select sum(MAX_PROCESSES) Target from apps.Fnd_Concurrent_Queues ;"

			# Find number of concurrent processes running

			eval ${ORAENV} ${ORACLE_HOME}/bin/sqlplus -s apps/${APPS_PASSWD}@${SID} <<-END > ${TMPFILE}
			${output}
			${sqlrun}
			END

			integer FNDRUN=`/usr/bin/tail -1 ${TMPFILE} | /usr/bin/awk '{print $1}'`

			# Find maximum number of concurrent processes

			eval ${ORAENV} ${ORACLE_HOME}/bin/sqlplus -s apps/${APPS_PASSWD}@${SID} <<-END > ${TMPFILE}
			${output}
			${sqlmax}
			END

			integer FNDMAX=`/usr/bin/tail -1 ${TMPFILE} | /usr/bin/awk '{print $1}'`

			[ ${FNDMAX} -eq 0 ] && return 100

			# Calculate the percentage

			integer ACTUAL=`/usr/bin/echo "${FNDRUN} / ${FNDMAX} * 100" | bc -l`

			# Test if actual percentage is less than the user defined limit

			if [ "${FNDRUN}" -lt "${FNDMAX}" ] 
			then
			   if [ ${ACTUAL} -lt ${CON_LIMIT} ]
			   then
       		 		# SCMSGS
       		 		# @explanation
       		 		# While probing the Oracle E-Business Suite
       		 		# concurrent manager, the actual percentage of
       		 		# processes running is below the user defined
       		 		# acceptable limit set by CON_LIMIT when the
       		 		# resource was registered.
       		 		# @user_action
       		 		# Determine why the number of actual processes
       		 		# for the concurrent manager is below the
       		 		# limit set by CON_LIMIT. The concurrent
       		 		# manager resource will be restarted.
       		 		scds_syslog -p daemon.err -t $(syslog_tag) -m \
       		 		   "check_cmg - Actual (%s) FND processes running is below limit (%s)" \
				   "${ACTUAL}%" "${CON_LIMIT}%"

       		 		# SCMSGS
       		 		# @explanation
       		 		# While probing the Oracle E-Business Suite
       		 		# concurrent manager, the actual percentage of
       		 		# processes running is below the user defined
       		 		# acceptable limit set by CON_LIMIT when the
       		 		# resource was registered.
       		 		# @user_action
       		 		# Determine why the number of actual processes
       		 		# for the concurrent manager is below the
       		 		# limit set by CON_LIMIT. The concurrent
       		 		# manager resource will be restarted.
       		 		scds_syslog -p daemon.err -t $(syslog_tag) -m \
       		 		   "check_cmg - FUNDRUN =  %s, FNDMAX = %s" \
				   "${FNDRUN}" "${FNDMAX}"

				rc=50
		  	   else
				debug_message "check_cmg - FNDRUN = ${FNDRUN}, FNDMAX = ${FNDMAX}"
		  	   fi
			else
			   debug_message "check_cmg - FNDRUN = ${FNDRUN}, FNDMAX = ${FNDMAX}"
			fi
			;;
	   	cmglsr)	ORA_ENVFILE=$(/usr/bin/grep ORA_ENVFILE= ${ADSCRIPT_PATH}/adalnctl.sh |\
				/usr/bin/awk -F= '{print $2}' | /usr/bin/tr -d '"')

			listener_name=$(/usr/bin/grep listener_name= ${ADSCRIPT_PATH}/adalnctl.sh |\
				 /usr/bin/awk -F= '{print $2}' | /usr/bin/tr -d '"')

			. ${ORA_ENVFILE}

			lsnrctl status $listener_name > ${TMPFILE}
			rc=$?

			if [ "${rc}" -ne 0 ]
			then
			   if /usr/bin/grep TNS-01169: ${TMPFILE}
			   then
				rc=0
			   else
				rc=100
			   fi
			fi
			
			;;
		frm)	query_pids "-w f60srvm ${USESID}"
			rc=$?
	
			if [ "${rc}" -eq 0 ]
			then
			   query_pids "-w f60webmx ${USESID}"
			   rc=$?

			   [ "${rc}" -ne 0 ] && rc=50
			else
			   rc=100
		 	fi
			;;
		rep)	query_pids "-w rwmts60 ${USESID}"
			rc=$?
	
			[ "${rc}" -ne 0 ] && rc=100
			;;
		opmn)   # Here we need to determine if the OPMN resource is currently starting or
			# if it has successfully started. This is required so that we can probe
			# against different process type states, i.e.
			#
			# When starting, process type state = "Alive"
			#
			# After successful start, process type states = "Init Alive"
			#
			# We tolerate "Init" because OPMN will attempt to restart a failed process
			# type after it has been successfully started. In this regard we let OPMN
			# attempt a restart and hence tolerate "Init".

			status_check=

			/usr/bin/pgrep -u root -f "start_opmn .*-R ${RESOURCE} " >/dev/null 2>&1
			rc=$?

			if [ "${rc}" -eq 0 ]
			then
			   # If we are starting then we should allow OPMN some time to start the 
			   # process types as opposed to keep checking the status. In this regard 
			   # we'll wait 10 seconds between each "wait-for-online" probe.
			   sleep 10
			   status_check="Alive"
			else
			   status_check="Init|Alive"
			fi

			OPMN_COMPONENTS=`/usr/bin/echo ${OPMN_COMPONENTS} | /usr/bin/tr "/" " " | /usr/bin/tr '[:upper:]' '[:lower:]'`

			if [ "${OPMN_COMPONENTS}" = "all" ]
			then
			   OPMN_COMPONENTS="opmn http_server oacore forms oafm"
			fi

			processtype=

			for component in ${OPMN_COMPONENTS}
			do
			   case ${component} in
				opmn)           # Don't use opmn as a process type
						;;
				http_server)    processtype="${processtype} HTTP_Server";;
				*)              processtype="${processtype} ${component}";;
			   esac
			done

			# Check if a ${processtype} is "Init" or "Alive", if not then 25 is
			# added to ${rc} which then represents any cumulative failure. So,
			# if only one ${processtype} is not "Init" or "Alive" then rc=25.
			#
			# This allows OPMN upto "4 probes" to recover a failed process
			# type, i.e. If the failure persists after each probe, then rc=25
			# is added to the failure history, therefore after 4 successive
			# failures the failure history will sum 100.
			#
			# Also, if all ${processtype}'s are not "Init" or "Alive", then rc=100
			# will be returned which will trigger a restart of the OPMN resource.
			#
			# This also ensures that the OPMN resource comes "Online" only
			# after all OPMN components are "Alive" when starting.
			#
			# The following sample OPMN status output may help understand the process
			# type states.
			# -------------------+--------------------+---------+---------
			# ias-component      | process-type       |     pid | status
			# -------------------+--------------------+---------+---------
			# OC4J               | oafm               |   12112 | Alive
			# OC4J               | forms              |   11231 | Alive
			# OC4J               | oacore             |   10063 | Alive
			# HTTP_Server        | HTTP_Server        |    9517 | Alive
			# ASG                | ASG                |     N/A | Down

			su ${APPSUSER} -c "${TASK_COMMAND} env ${LHOSTNAME} env ${LDPRELOAD} \
			   ${ADSCRIPT_PATH}/adopmnctl.sh status ${OUTPUT}" > /dev/null

			rc=0

			for pt in ${processtype}
			do
			   # ${LOGFILE} is encapsulated within ${OUTPUT}, refer to set_redirection().
			   # ${status_check} is set earlier on within this function.

			   status=`/usr/bin/grep -w ${pt} ${LOGFILE} | /usr/bin/awk -F"|" '{print $4}' | /usr/bin/tr -d ' '`

			   [ "${status}" ] || status=Unknown

			   if /usr/bin/echo "${status}" | /usr/xpg4/bin/grep -qE ${status_check}
			   then
				# Setting rc=1 simply triggers a degraded status for the fault monitor. In
				# this scenario, if "Init" is an acceptable state (because the process type
				# has been successfully started before) then if "Init" is found we return
				# a non-zero return code which will cause the fault monitor to be degraded.
				#
				# Either OPMN will restart the process type or not. However, during this
				# period the OPMN resource will show as degraded until either all the
				# process types are successfully restarted or a restart/failover is
				# successful. 
				# 
				# In this regard cumulative rc=1's is more than enough time for OPMN to 
				# restart a resource, i.e. "Init" will either succeed or an internal OPMN
				# timeout will occur causing the process type state to be down, which in
				# turn will trigger a OPMN resource restart.

				[ "${status}" = "Init" ] && rc=1
				rc=`expr ${rc} + 0`
			   else
				rc=`expr ${rc} + 25`
			   fi

			   debug_message "Function: checkebs opmn - ${pt} is ${status}"

			done

			/usr/bin/cat /dev/null > ${LOGFILE}
			;;
	esac

	debug_message "Function: checkebs ${COMPONENT} - End"
	return ${rc}
}
