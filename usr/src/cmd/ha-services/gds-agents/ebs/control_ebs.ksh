#!/usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
# 
# ident	"@(#)control_ebs.ksh	1.10	09/01/21 SMI"
#
# Usage GDS: <options> <parameter1> <parameter2>
# 
#	<options>: -R <resource> -G <resourcegroup> etc.
#	parameter1: start | stop | probe | validate
#	parameter2: cmg | cmglsr | frm | opmn | rep 

MYNAME=`basename $0`
MYDIR=`dirname $0`

typeset opt

while getopts 'R:G:C:U:P:S:V:O:L:M:X:H:' opt
do
        case "${opt}" in
		R)      RESOURCE=${OPTARG};;
		G)      RESOURCEGROUP=${OPTARG};;
		C)      COMNTOP=${OPTARG};;
		U)      APPSUSER=${OPTARG};;
		P)      APPS_PASSWD=${OPTARG};;
		S)      APP_SID=${OPTARG};;
		V)      VERSION=${OPTARG};;
		O)      ORACLE_HOME=${OPTARG};;
		L)      CON_LIMIT=${OPTARG};;
		M)      # Backward compatability 
			;;
		X)	OPMN_COMPONENTS=${OPTARG};;
		H)	HOSTNAME=${OPTARG};;

		*)      exit 1;;
	esac
done

if [ "${OPTIND}" -gt 1 ]
then
	# Called by GDS
	CALLER=GDS

	shift $((${OPTIND} -1))
else
	# Called by SMF
	exit 1
fi

METHOD=${1}
COMPONENT=${2}

. ${MYDIR}/../${COMPONENT}/etc/config
. ${MYDIR}/functions

debug_message "Method: ${MYNAME} - Begin"
${SET_DEBUG}

if [ "${CALLER}" = "GDS" ]
then
	# Perform all the scha* calls
	TASK_COMMAND=""

	# The docs require that the user creates a symlink from 
	# /usr/cluster/lib/libschost.so.1 to /usr/lib/secure/libschost.so.1
	# 
	# Additionally, refer to the security section within the 
	# libschost.so.1(1) man page.

	LDPRELOAD="LD_PRELOAD_32=/usr/lib/secure/libschost.so.1"

	if [ "${METHOD}" = "stop" ]
	then
	   STOP_TIMEOUT=`/usr/cluster/bin/scha_resource_get -O Stop_timeout \
	      -R ${RESOURCE} -G ${RESOURCEGROUP} `
	fi

	SCLH=`/usr/cluster/bin/scha_resource_get -O Network_resources_used \
	   -R ${RESOURCE} -G ${RESOURCEGROUP} `

	# If ${SCLH} is empty the VALIDATE method is running at resource creation.
	# In order to determine the correct SUNW.LogicalHostname resource associated
	# with the resource being created, the /var/cluster/logs/commandlog is used.
	# Note, this is a one time activity as once the resource is created then
	# scha_resource_get will derive the corect Network_resources_used.

	if [ ${#SCLH} -eq 0 ]
	then
	   SCLH_LIST=$(/usr/bin/tail -3 /var/cluster/logs/commandlog | /usr/bin/grep START | \
		/usr/bin/grep ${RESOURCE} | /usr/bin/grep Resource_dependencies_restart= |\
		/usr/bin/sed -e 's/^.*-y "Resource_dependencies_restart=//' | /usr/bin/awk '{print $1}' |\
		/usr/bin/tr -d '"' | /usr/bin/tr ',' ' ')

	   for res in ${SCLH_LIST}
	   do
		RT=$(/usr/cluster/bin/scha_resource_get -O Type -R ${res} -G ${RESOURCEGROUP})

		if /usr/bin/echo ${RT} | /usr/xpg4/bin/grep -q SUNW.LogicalHostname
		then
		   SCLH=${res}
		   debug_message "control_ebs - SCLH=${res} retrieved from commandlog"
		   break
		fi
	   done
	
	   if [ ${#SCLH} -eq 0 ]
	   then
		# SCMSGS
		# @explanation
		# Unable to determine the SUNW.LogicalHostname resource within the
		# Resource_dependencies list for the resource being created.
		# @user_action
		# Check that you have specified a value for the LH keyword within the
		# /opt/SUNWscebs/xxx/util/xxx_config, where xxx represents the Oracle
		# E-Business Suite component - cmg, cmglsr, frm, opmn and rep.
		scds_syslog -p daemon.err -t $(syslog_tag) -m \
		   "Validate - Unable to determine the SUNW.LogicalHostname resource for %s" \
		   "${RESOURCE}"
		return 1
	   fi
	fi

	LHOST=`/usr/cluster/bin/scha_resource_get -O Extension -R ${SCLH} -G ${RESOURCEGROUP} \
	   HostnameList | /usr/bin/tail +2`

	LHOSTNAME="SC_LHOSTNAME=${LHOST}"

	# Because EBS v12 introduces changes to the pathname for the admin scripts, we set the
	# appropriate scripts pathname here. Also to maintain backward compatability we still
	# use ${COMNTOP}.

	case "${VERSION}" in
	      11.5.8)	debug_message "Validate - ${VERSION}"
			# With 11.5.8 the SID didn't change
			SID=${APP_SID}
			ADSCRIPT_PATH=${COMNTOP}/admin/scripts/${APP_SID}
			;;
	      11.5.*)	debug_message "Validate - ${VERSION}"
			# With 11.5.9 the SID changed to SID_<lhost>
			# for some directory structures and files
			SID=${APP_SID}
			APP_SID=${APP_SID}_${LHOST}
			ADSCRIPT_PATH=${COMNTOP}/admin/scripts/${APP_SID}
			;;
	      12.*)     debug_message "Validate - ${VERSION}"
			# With 12.0 the pathname to the admin scripts changes
			# from ${COMNTOP}/admin/scripts/${APP_SID}
			# to <basedir>/inst/apps/${APP_SID}/admin/scripts
			# where <basedir> = s_base variable within the
			# conf_<SID>.txt file built by rapidwiz.
			# Nevertheless we will use a relative pathname from ${COMNTOP}.
			SID=${APP_SID}
			if [ -z $HOSTNAME ]
			then
				APP_SID=${APP_SID}_${LHOST}
			else
				APP_SID=${APP_SID}_${HOSTNAME}
			fi
			ADSCRIPT_PATH=${COMNTOP}/../../../inst/apps/${APP_SID}/admin/scripts
			;;
	      *)	scds_syslog -p daemon.err -t $(syslog_tag) -m \
			   "Validate - %s is invalid" \
			   ${VERSION}
			;;
	esac
	
	# The application password required to start/stop the Concurrent Manager can now
	# be stored within a restricted file instead of supplying the password when the
	# resource is created. Later on validation will issue a warning message if the 
	# restricted file is not owned by root with read only permissions and that a
	# password is set within ${APPS_PASSWD}.

	if [ "${COMPONENT}" = "cmg" -a ${#APPS_PASSWD} -eq 0 ]
	then
	   APPS_PASSWD=$(/usr/bin/cat /opt/SUNWscebs/.${SID}_passwd)
	fi

	# Determine the newtask project for start and stop

	if [ "${METHOD}" != "probe" ]
	then
	   if [ `/usr/bin/uname -r` != "5.8" ]
	   then
	      # Retrieve the resource project name
	      RESOURCE_PROJECT_NAME=`/usr/cluster/bin/scha_resource_get \
		-R ${RESOURCE} -G ${RESOURCEGROUP} -O RESOURCE_PROJECT_NAME`
		
	      if [ -z "${RESOURCE_PROJECT_NAME}" -o "${RESOURCE_PROJECT_NAME}" = "default" ]
	      then
		# Retrieve the resource group project name 
		RESOURCE_PROJECT_NAME=`/usr/cluster/bin/scha_resourcegroup_get \
		   -G ${RESOURCEGROUP} -O RG_PROJECT_NAME`
	      fi	
	   fi

	   # Validate that ${APPSUSER} belongs to the ${RESOURCE_PROJECT_NAME}
	   if [ "${RESOURCE_PROJECT_NAME}" ]
	   then
	      PROJ_MEMBER=`/usr/bin/projects ${APPSUSER} | /usr/bin/egrep "^${RESOURCE_PROJECT_NAME} | ${RESOURCE_PROJECT_NAME} | ${RESOURCE_PROJECT_NAME}$|^${RESOURCE_PROJECT_NAME}$"`

	      if [ -z "${PROJ_MEMBER}" ]
	      then
		scds_syslog -p daemon.notice -t $(syslog_tag) -m \
		   "%s - The user %s does not belongs to project %s" \
		   "${MYNAME}" "${APPSUSER}" "${RESOURCE_PROJECT_NAME}"
	      else
		TASK_COMMAND="/usr/bin/newtask -p ${RESOURCE_PROJECT_NAME}"
	      fi
	   fi
	fi
else
	exit 1
fi

[ -x /sbin/zonename ] && ZONENAME=`/sbin/zonename`

set_redirection

case "${METHOD}" in
	start)
		validate
		rc=$?
		[ "${rc}" -ne 0 ] && return ${rc}
		
		startebs ${COMPONENT}
		rc=$?
		;;
	stop)
		if [ "${COMPONENT}" = "cmg" ]
		then

		   # If the Internal Manager is lost 
		   # then tidy up any loose pids

		   query_pids "-w CPMGR"
		   rc=$?

		   if [ "${rc}" -ne 0 ]
		   then
			cleanup cmg "FND"
			return 0
		   fi
		fi

		orderly_stop ${COMPONENT}
		rc=$?
		;;
	probe)
		checkebs ${COMPONENT}
		rc=$?
		;;
	validate)
		validate
		rc=$?
		;;
esac

debug_message "Method: ${MYNAME} - End"
exit ${rc}
