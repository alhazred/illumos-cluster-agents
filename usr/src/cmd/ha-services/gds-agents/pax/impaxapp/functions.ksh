#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#
# Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)functions.ksh	1.7	07/06/06 SMI"
#


SCLOGGER=/usr/cluster/lib/sc/scds_syslog
PKG=SUNWscpax
METHOD=`basename $0`
LOGFILE=/var/tmp/${RESOURCE}_logfile

SCHA_RESOURCE_GET=/usr/cluster/bin/scha_resource_get
SCHA_RESOURCEGROUP_GET=/usr/cluster/bin/scha_resourcegroup_get
TASK_COMMAND=""

syslog_tag()
{
        #
        # Data Service message format
        #

        $SET_DEBUG

        print "SC[${PKG:-??}.${METHOD:-??}]:${RESOURCEGROUP:-??}:${RESOURCE:-??}"
}

scds_syslog()
{

        #
        # Log a message
        #

        $SET_DEBUG

        $SCLOGGER "$@" &
}

debug_message()
{
        #
        # Output a debug message to syslog if required
        #

        if [ "$DEBUG" = "$RESOURCE" -o "$DEBUG" = "ALL" ]
        then
                SET_DEBUG="set -x"

                DEBUG_TEXT=$1

                scds_syslog -p daemon.debug -t $(syslog_tag) -m \
                        "%s" "$DEBUG_TEXT"
        else
                SET_DEBUG=
        fi
}

log_message()
{
        #
        # Output a message to syslog as required
        #

        debug_message "Function: log_message - Begin"
        $SET_DEBUG

        if [ -s "${LOGFILE}" ]
        then
                PRIORITY=$1
                HEADER=$2

		# 
		# Ensure the while loop only reads a closed file
		#

		strings ${LOGFILE} > ${LOGFILE}.copy
                while read MSG_TXT
                do
                        scds_syslog -p daemon.${PRIORITY} -t $(syslog_tag) -m \
                                "%s - %s" \
                                "${HEADER}" "${MSG_TXT}"
                done < ${LOGFILE}.copy

                su $USERID -c "cat /dev/null > $LOGFILE" > /dev/null
		cat /dev/null > ${LOGFILE}.copy
        fi

        debug_message "Function: log_message - End"
}

standard_resource_get()
{
        debug_message "Function: standard_resource_get - Begin"
        $SET_DEBUG

        PROPERTY=$1

        ${SCHA_RESOURCE_GET} -R ${RESOURCE} -G ${RESOURCEGROUP} -O ${PROPERTY}

        s1=$?

        if [ "${s1}" -ne 0 ]; then
                debug_message "standard_resource_get - Retrievment of property ${PROPERTY} returned ${s1}"
        fi

        debug_message "Function: standard_resource_get - End"
        return ${s1}
}

standard_resourcegroup_get()
{
        debug_message "Function: standard_resourcegroup_get - Begin"
        $SET_DEBUG

        PROPERTY=$1

        ${SCHA_RESOURCEGROUP_GET} -R ${RESOURCE} -G ${RESOURCEGROUP} -O ${PROPERTY}

        s1=$?

        if [ "${s1}" -ne 0 ]; then
                debug_message "standard_resource_get - Retrievment of property ${PROPERTY} returned ${s1}"
        fi

        debug_message "Function: standard_resourcegroup_get - End"

        return ${s1}
}

srm_function()
{

        USER=$1

        debug_message "Function: srm_function - Begin"
        $SET_DEBUG


        #
        # If Solaris 8 just return
        #

        if [ `/usr/bin/uname -r` = "5.8" ];
        then
                return 0
        fi

        #
        # Retrieve RESOURCE_PROJECT_NAME
        #

        RESOURCE_PROJECT_NAME=`standard_resource_get RESOURCE_PROJECT_NAME`

        #
        # Retrieve RG_PROJECT_NAME if RESOURCE_PROJECT_NAME is not set
        #

        if [ -z "${RESOURCE_PROJECT_NAME}" ]; then

                RESOURCE_PROJECT_NAME=`standard_resourcegroup_get RG_PROJECT_NAME`
        fi

        #
        # Return if no projects are defined
        #

        if [ -z "${RESOURCE_PROJECT_NAME}" ]; then
                return 0
        fi

        #
        # Validate that $USER belongs to the project defined by
        # ${RESOURCE_PROJECT_NAME}
        #

        PROJ_MEMBER=`/usr/bin/projects ${USER} | /usr/bin/grep -w ${RESOURCE_PROJECT_NAME}`

        if [ -z "${PROJ_MEMBER}" ];
        then
             scds_syslog -p daemon.error -t $(syslog_tag) -m \
                        "srm_function - The user %s does not belongs to project %s" \
                        "${USER}" "${RESOURCE_PROJECT_NAME}" 
                return 1
        else
                debug_message "srm_function - User ${USER} belongs to project ${RESOURCE_PROJECT_NAME}"
        fi

        #
        # Set TASK_COMMAND
        #

        TASK_COMMAND="/usr/bin/newtask -p ${RESOURCE_PROJECT_NAME}"

        debug_message "Function: srm_function - End"

        return 0
}



Check_InterRG_dependency()
{
        if [ -z "`/usr/cluster/bin/scrgadm -pvv -j ${RESOURCE} | /bin/grep \
        Resource_dependencies_restart`" ]; then

                # SCMSGS
                # @explanation
                # No restart dependencies are available.
                # @explanation-2
                # Informational : no inter rg dependency defined in the config
                # @explanation-3
                # Informational : no inter rg dependency defined in the config
                # @user_action
                # None
                # @user_action-2
                # none required
                # @user_action-3
                # none required
                scds_syslog -p daemon.error -t ${syslog_tag} -m \
               "No Inter RG-dependency found, using internal dependency" 

                USE_INTERNAL_DEP=TRUE
        fi
}


check_restart_dependency()
{

        $SET_DEBUG
        debug_message "Function: check_restart_dependency - Start called with the following arguments "$*

        #
        # Put here the specific application code to report if the dependent
	# application has been restarted or not
        #
	# Use hatimerun when calling an external program.
	#
        # Return 0 if the dependent resource has NOT been restarted.
        # Return 1 if the dependent resource has been restarted.


        return 0

        debug_message "Function: check_restart_dependency - End"

}


check_start_dependency()
{

	$SET_DEBUG
	debug_message "Function: check_start_dependency - Start called with the following arguments "$*

        #
        # Put here the specific application code to report if the dependent
        # application has been started or not
	#
	# Use hatimerun when calling an external program.
        #
        # Return 0 if the dependent resource is online
        # Return 1 if the dependent resource is not online



	return 0

	debug_message "Function: check_start_dependency - End"
	
}

start_dependency()
{
	debug_message "Function: start_dependency - Begin"
	$SET_DEBUG

	# RETRIEVE START_TIMEOUT

	START_TIMEOUT=`/usr/cluster/bin/scha_resource_get -O START_TIMEOUT -R ${RESOURCE} -G ${RESOURCEGROUP}`

	# 80 % OF THE START-TIMEOUT CAN BE SPEND ON WAITING

	MAX_START_TIMEOUT=`expr ${START_TIMEOUT} \* 80 \/ 100`

	# GET CURRENT TIME IN SEC ON 24H BASE
		
	CUR_HOUR=`date '+%H'`
	CUR_MIN=`date '+%M'`
	CUR_SEC=`date '+%S'`
	CUR_TIME=`expr ${CUR_HOUR} \* 3600 + ${CUR_MIN} \* 60 + ${CUR_SEC}`

	# RUN A TEST LOOP UNTIL THE DEPENDENT RESOURCE IS UP OR 
	# A TIMEOUT HAS OCCURED

	while [ 1 -eq 1 ]
	do

		# GET NEW CURRENT TIMEOUT
		NEW_HOUR=`date '+%H'`
		NEW_MIN=`date '+%M'`
		NEW_SEC=`date '+%S'`
		NEW_TIME=`expr ${NEW_HOUR} \* 3600 + ${NEW_MIN} \* 60 + ${NEW_SEC}`

		# HAVE WE EXEEDED TIMEOUT

		s1=`expr ${CUR_TIME} + ${MAX_START_TIMEOUT}`

		if [ ${s1} -le ${NEW_TIME} ]; then
			# SCMSGS
			# @explanation
			# a resource which the resource depends on has not
			# come online within the specified time
			# @user_action
			# check dependencies, check the resource indicated as
			# the resource depends from.
			scds_syslog -p daemon.error -t $(syslog_tag) -m \
       			"start_dependency: Exeeded %s seconds for waiting on dependent resource for resource %s to come online" "${MAX_START_TIMEOUT}" "${RESOURCE}"

			St=1
			break
		fi

		# CALL check_start_dependency

		debug_message "Function: start_dependency - Call check_start_dependency function with argument "$*

		check_start_dependency $*
		St=$?

		if [ ${St} -eq 0 ]; then
		  St=0
		  break
		fi

		# Wait 5 seconds

		sleep 5
	done


	debug_message "Function: start_dependency - End"

	return ${St}
}

restart_dependency()
{
	debug_message "Function: restart_dependency - Begin"
	$SET_DEBUG


	# CALL check_restart_dependency

	debug_message "Function: start_dependency - Call check_restart_dependency function with argument "$*
	check_restart_dependency $*
	St=$?

	if [ ${St} -ne 0 ]; then
           scds_syslog -p daemon.error -t $(syslog_tag) -m \
           "restart_dependency - Dependent resource to resource %s has been restarted, restart this resource %s" \
           "${RESOURCE}" "${RESOURCE}"

	   St=100
        else
           St=0
	fi

	debug_message "Function: restart_dependency - End"

	return ${St}
}


validate_options()
{
        #
        # Ensure all options are set
        #

	SCRIPTNAME=`basename $0`
        for i in RESOURCE RESOURCEGROUP USE_INTERNAL_DEP
        do
                case $i in
                        RESOURCE)
                        if [ -z $RESOURCE ]; then
                                # SCMSGS
                                # @explanation
                                # The option -R of the agent
                                # command $COMMANDNAME is not set,
                                # @user_action
                                # look at previous error messages in the
                                # syslog.
                                scds_syslog -p daemon.err -t $(syslog_tag) -m \
                                "validate_options: %s Option -R not set" \
                                "${SCRIPTNAME}"
                                exit 1
                        fi;;

                        RESOURCEGROUP)
                        if [ -z $RESOURCEGROUP ]; then
                                # SCMSGS
                                # @explanation
                                # The option -G of the agent
                                # command $COMMANDNAME is not set,
                                # @user_action
                                # look at previous error messages in the
                                # syslog.
                                scds_syslog -p daemon.err -t $(syslog_tag) -m \
                                "validate_options: %s Option -G not set" \
                                "${SCRIPTNAME}"
                                exit 1
                        fi;;

                        USE_INTERNAL_DEP)
                        if [ ${USE_INTERNAL_DEP} = "FALSE" ]; then
				Check_InterRG_dependency
                        fi;;
                esac
        done
}

validate()
{
	#
	# Validate impax
	#
	
        debug_message "Function: validate - Begin"
	$SET_DEBUG

	rc_validate=0

	#
	# Validate that ${filename} exists
	#

	if [ ! -f "${filename}" ]
	then
   		# SCMSGS
   		# @explanation
   		# validation that file indicated has not been found by the
   		# agent.
   		# @user_action
   		# Check all required files are available on both nodes,
   		# readable and in the same location.
   		scds_syslog -p daemon.err -t $(syslog_tag) -m \
			"Validate - file %s does not exist" \
			"${filename}"
		rc_validate=0
	else
		debug_message "Validate - ${filename} exists"
	fi

	#
	# More validate checks here
	#

	debug_message "Function: validate - End"
}

start_impax()
{
	#
	# Start impax
	#

        debug_message "Function: start_impax - Begin"
	$SET_DEBUG

	#
	# If internal Inter-RG dependencies are needed for start dependencies 
	# then uncomment the lines that starts with 
	# Internal Inter-RG dependency.

	# You have to provide the code for checking if the dependent resource
	# is online in function check_start_dependency()
	#
	# If needed add any options to the start_dependency function call.
	# The start_dependency function will call the check_start_dependency 
	# function.

	# Internal Inter-RG dependency
	#
	# if [ ${USE_INTERNAL_DEP} = "TRUE" ]; then
	#	start_dependency # Add any options
	#
	#	St=$?
	#
	#	if [ ${St} -ne 0 ]; then
	#		rc_start_command=1
	#		return
	#	fi
	# fi
	#	   
	# database is taken care off by the Oracle agent
	# This should take care of sh, ksh, csh, tcsh and bash
	#
	#@TG@
        debug_message "start_impax - checking Impax licenses"
        # this needs to be done to ensure the correct license is in place
	check_impax_license

	if [ -d /etc/impaxrc ]; then
		# ok the impaxrc script dir exists
		debug_message "found impaxrc script"
	else
		# wrong bail out ... no custom rc scripts available
		debug_message "no impaxrc dir found... see install guide"
		exit 1
	fi
	# check existence and start each sub part in turn...
	for i in `ls -1 /etc/impaxrc/S??*`
	do
	if [ -f $i ]; then
		# ok found first step
		$i start
		if [ $? != 0 ]; then
			# something is wrong, start of $i failed
			debug_message " start of $i failed"
			if [ $i == "S49IMPAXsrvw" ];then
                        #this is only the web server... only service tools will not function
                        debug_message " Warning could not find start script for web server, service tools might be unavailable"
                                                                                      
                
                	else

				exit 1
			fi
		fi
	else 
		# script not found!
		debug_message " $i start script not found"
		if [ $i == "S49IMPAXsrvw" ];then
			#this is only the web server... only service tools will not function
			debug_message " Warning could not find start script for web server, service tools might be unavailable"

		
		else
			exit 1
		fi
	fi
	done

	
	# all hunky dory....
	# start a looping script... to fool pmfadm
	if [ -f /opt/SUNWscpax/impaxapp/bin/loops ]; then
		/opt/SUNWscpax/impaxapp/bin/loops &
	fi
		
	debug_message "Function: start_impax - End"
}

stop_impax()
{
	#
	# Stop impax
	#

        debug_message "Function: stop_impax - Begin"
	$SET_DEBUG

	#
	# This should take care of sh, ksh, csh, tcsh and bash
	#
	#if [ -f /usr/local/bin/kb_stop_impax ]; then
	#	# ok stop it
	#	/usr/local/bin/kb_stop_impax
	#	if [ $? != 0 ]; then
	#		# error ! 
	#		log_message " stop of impax app failed bail out"
	#		return 1
	#	fi
	#fi
	#pkill -9 kb_loops

	#reverse the order and stop the impax scripts... some sanity checks
	if [ -d /etc/impaxrc ]; then
		# ok impaxrc dir is in the place we want it
		debug_message " found the impaxrc in stop method"
	
	else
		# big problem bail out no impaxrc
		debug_message " did not find impaxrc in stop method"
		exit 1
	fi
	
	for i in `ls -1 /etc/impaxrc/S??* | sort -r`
	do
	
	if [ -f $i ];then
		#found the script
		$i stop
		if [ $? != 0 ]; then
			# return code not ok
			debug_message " stop of $i failed"
		fi
	else
		# script not found or error
		debug_message " did not find the script to stop the $i"
	fi
	done


	debug_message "Function: stop_impax - End"
}

check_impax_license()
{
        # called as part of check_impax processing
        if [ -x /usr/mvf/bin/manage_cluster_mvf_licenses ]; then
             debug_message "dispatching /usr/mvf/bin/manage_cluster_mvf_licenses"
             /usr/mvf/bin/manage_cluster_mvf_licenses
        else
             debug_message "/usr/mvf/bin/manage_cluster_mvf_licenses not found"
	     # we should bail out after this part..
	     # as IMPAX will not function with a correct license
        fi
                                                                                                                   
}


check_impax()
{
	# 
	# Probe impax
	#

        debug_message "Function: check_impax - Begin"
	$SET_DEBUG

	rc_check_impax=0

        #
        # If internal Inter-RG dependencies are needed for restart dependencies
        # then uncomment the lines that starts with
        # Internal Inter-RG dependency.

        # You have to provide the code for checking if the dependent resource
        # has been restarted in function check_restart_dependency()
	#
	# You probably have to increase the Probe_timeout value from the
	# default value of 30 seconds.
        #
        # If needed add any options to the restart_dependency function call.
        # The start_dependency function will call the check_restart_dependency
        # function.

        # Internal Inter-RG dependency
        #
        # if [ ${USE_INTERNAL_DEP} = "TRUE" ]; then
        #       restart_dependency # Add any options
        #
        #       St=$?
        #
        #       if [ ${St} -ne 0 ]; then
        #               rc_check_command=100
        #               return
        #       fi
        # fi
        #
	#
	# This should take care of sh, ksh, csh, tcsh and bash
	#
	#if getent passwd ${USERID} | awk -F: '{print $7}' | grep csh > /dev/null
	#then
	#	su ${USERID} -c "check_command >& ${LOGFILE}" > /dev/null
	#	rc_check_command=$?
	#else
	#	su ${USERID} -c "check_command >${LOGFILE} 2>&1" > /dev/null
	#	rc_check_command=$?
	#fi
        debug_message "Checking Impax license..."
                                                                                                                   
        check_impax_license


	debug_message "Function: check_impax - End"
}

