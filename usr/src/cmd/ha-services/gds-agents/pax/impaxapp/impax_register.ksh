#!/usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)impax_register.ksh	1.5	07/06/06 SMI"
#


. `dirname $0`/impax_config

if [ "${USE_INTERNAL_DEP}" = "FALSE" ]; then
   DEPFLAG="-Z"
else
   DEPFLAG=""
fi

scrgadm -a -j $RS -g $RG -t SUNW.gds \
-x Start_command="/opt/SUNWscpax/impaxapp/bin/start_impax \
-R $RS -G $RG ${DEPFLAG} " \
-x Stop_command="/opt/SUNWscpax/impaxapp/bin/stop_impax \
-R $RS -G $RG ${DEPFLAG} " \
-x Probe_command="/opt/SUNWscpax/impaxapp/bin/probe_impax \
-R $RS -G $RG ${DEPFLAG} " \
-x Stop_signal=9 \
-y Resource_dependencies=$RES_DEP \
-x Child_mon_level=$CHILDMON \
-y Port_list=$PORT/tcp 
