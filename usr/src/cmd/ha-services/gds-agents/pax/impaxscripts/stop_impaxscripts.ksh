#!/usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#
#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)stop_impaxscripts.ksh	1.5	07/06/06 SMI"
#



typeset opt

while getopts 'R:G:' opt
do
        case "$opt" in
                R)      RESOURCE=$OPTARG;;
                G)      RESOURCEGROUP=$OPTARG;;
                *)      logger -p daemon.err \
                        "ERROR: `basename $0` Option $OPTARG unknown"
                        exit 1;;
        esac
done

. `dirname $0`/../etc/config
. `dirname $0`/functions

validate_options

debug_message "Method: `basename $0` - Begin"
$SET_DEBUG

validate ${CLEANKILL}

if [ "$rc_validate" -ne 0 ]
then
        debug_message "validate failed: `basename $0` -  ${CLEANKILL} does not exist"
        exit 1
fi
validate ${ORACLEPRE}

if [ "$rc_validate" -ne 0 ]
then
        debug_message "validate failed: `basename $0` - ${ORACLEPRE} does not exist"
        exit 1
fi

stop_impaxscripts 

if [ "$rc_stop_command" -eq 0 ]
then
        log_message notice "stop_command rc<$rc_stop_command>"
        debug_message "Method: `basename $0` - End (Exit 0)"
        exit 0
else
        log_message error "stop_command failed !!!! rc<$rc_stop_command>"
	exit 1
fi

debug_message "Method: `basename $0` - End (Exit 0)"
exit 0
