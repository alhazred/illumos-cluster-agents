#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#
#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)functions.ksh	1.6	07/06/06 SMI"
#



SCLOGGER=/usr/cluster/lib/sc/scds_syslog
PKG=SUNWscpax
METHOD=`basename $0`
LOGFILE=/var/tmp/${RESOURCE}_logfile

syslog_tag()
{
        #
        # Data Service message format
        #

        $SET_DEBUG

        print "SC[${PKG:-??}.${METHOD:-??}]:${RESOURCEGROUP:-??}:${RESOURCE:-??}"
}

scds_syslog()
{

        #
        # Log a message
        #

        $SET_DEBUG

        $SCLOGGER "$@" &
}

debug_message()
{
        #
        # Output a debug message to syslog if required
        #

        if [ "$DEBUG" = "$RESOURCE" -o "$DEBUG" = "ALL" ]
        then
                SET_DEBUG="set -x"

                DEBUG_TEXT=$1

                scds_syslog -p daemon.debug -t $(syslog_tag) -m \
                        "%s" "$DEBUG_TEXT"
        else
                SET_DEBUG=
        fi
}

log_message()
{
        #
        # Output a message to syslog as required
        #

        debug_message "Function: log_message - Begin"
        $SET_DEBUG

        if [ -s "${LOGFILE}" ]
        then
                PRIORITY=$1
                HEADER=$2

                while read MSG_TXT
                do
                        scds_syslog -p daemon.${PRIORITY} -t $(syslog_tag) -m \
                                "%s - %s" \
                                "${HEADER}" "${MSG_TXT}"
                done < $LOGFILE

                su - $USERID -c "cat /dev/null > $LOGFILE" > /dev/null
        fi

        debug_message "Function: log_message - End"
}

validate_options()
{
        #
        # Ensure all options are set
        #

        for i in RESOURCE RESOURCEGROUP 
        do
                case $i in
                        RESOURCE)
                        if [ -z $RESOURCE ]; then
                                # SCMSGS
                                # @explanation
                                # Need explanation of this message!
                                # @user_action
                                # Need a user action for this message.
                                logger -p daemon.err \
                                "ERROR: `basename $0` Option -R not set"
                                exit 1
                        fi;;

                        RESOURCEGROUP)
                        if [ -z $RESOURCEGROUP ]; then
                                # SCMSGS
                                # @explanation
                                # Need explanation of this message!
                                # @user_action
                                # Need a user action for this message.
                                logger -p daemon.err \
                                "ERROR: `basename $0` Option -G not set"
                                exit 1
                        fi;;
                esac
        done
}

validate()
{
	#
	# Validate impaxscripts
	#
	
        debug_message "Function: validate - Begin"
	$SET_DEBUG

	rc_validate=0
	filename=$1
	#
	# Validate that ${filename} exists
	#

	if [ ! -f "${filename}" ]
	then
   		scds_syslog -p daemon.error -t $(syslog_tag) -m \
			"Validate - file %s does not exist" \
			"${filename}"
		rc_validate=1
	else
		debug_message "Validate - ${filename} exists"
	fi

	#
	# More validate checks here
	#

	debug_message "Function: validate - End"
}

start_impaxscripts()
{
	#
	# Start  pre work for impax to contain the oracle tune-init script
	# and anything else that needs to be added...
	#

        debug_message "Function: start_impaxscripts - Begin"
	$SET_DEBUG
	
	#
	# This should take care of sh, ksh, csh, tcsh and bash
	#

	if getent passwd ${USERID} | awk -F: '{print $7}' | grep csh > /dev/null
	then
		su - ${USERID} -c "${ORACLEPRE} >& ${LOGFILE}" > /dev/null
		rc_start_command=$?
	else
		su - ${USERID} -c "${ORACLEPRE} >${LOGFILE} 2>&1" > /dev/null
		rc_start_command=$?
	fi

        # start a looping script... to fool pmfadm, as it is monitored by pmf, we dont care about killing it afterwards
	# thats automagically... ;)
        if [ -f /opt/SUNWscpax/impaxapp/bin/loops ]; then
                /opt/SUNWscpax/impaxapp/bin/loops &
        fi
 


	debug_message "Function: start_impaxscripts - End"
}

stop_impaxscripts()
{
	#
	# Stop impaxscripts
	#

        debug_message "Function: stop_impaxscripts - Begin"
	$SET_DEBUG

	#
	# This should take care of sh, ksh, csh, tcsh and bash
	#

	if getent passwd ${USERID} | awk -F: '{print $7}' | grep csh > /dev/null
	then
		su - ${USERID} -c "${CLEANKILL} >& ${LOGFILE}" > /dev/null
		rc_stop_command=$?
	else
		su - ${USERID} -c "${CLEANKILL} >${LOGFILE} 2>&1" > /dev/null
		rc_stop_command=$?
	fi

	debug_message "Function: stop_impaxscripts - End"
}

check_impaxscripts()
{
	# 
	# no need for probe-ing... as these are one time actions
	#

        debug_message "Function: check_impaxscripts - Begin"
	$SET_DEBUG

	rc_check_impaxscripts=0

	#
	# This should take care of sh, ksh, csh, tcsh and bash
	#

	

	debug_message "Function: check_impaxscripts - End"
}

