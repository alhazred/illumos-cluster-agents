/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 1998-2002 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * dns_probe.c - Probe for highly available dns
 */

#pragma ident	"@(#)dns_probe.c	1.15	07/06/06 SMI"

#include <signal.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <netdb.h>
#include <strings.h>
#include "dns.h"

int
main(int argc, char *argv[])
{
	scds_handle_t	handle;
	int		probe_result;
	hrtime_t	ht1, ht2;
	unsigned long	dt;

	if (scds_initialize(&handle, argc, argv) != SCHA_ERR_NOERR)
		return (1);

	for (;;) {

		/*
		 * sleep for a duration of thorough_probe_interval between
		 *  successive probes.
		 */
		(void) scds_fm_sleep(handle,
		    scds_get_rs_thorough_probe_interval(handle));

		probe_result = 0;
		ht1 = gethrtime(); /* Latch probe start time */

		probe_result = svc_probe(handle, B_TRUE);

		ht2 = gethrtime();

		/* Convert to milliseconds */
		dt = (ulong_t)((ht2 - ht1) / 1e6);

		/*
		 * Compute failure history and take action if needed
		 */
		(void) scds_fm_action(handle, probe_result, (long)dt);

	} 	/* Keep probing forever */
}
