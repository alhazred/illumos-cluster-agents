/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 1998-2002 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * dns_svc_start.c - Start method for highly available DNS
 */

#pragma ident	"@(#)dns_svc_start.c	1.18	07/06/06 SMI"

#include <string.h>
#include "dns.h"

/*
 * The start method for HA-DNS. Does some sanity checks on
 * the resource settings then starts the DNS under PMF with
 * a action script.
 * XXX Maybe better do all this in prenet_start?.
 */

int
main(int argc, char *argv[])
{
	scds_handle_t handle;
	int	retval;
	char *mode;
	scha_extprop_value_t *mode_used;

	if (scds_initialize(&handle, argc, argv) != SCHA_ERR_NOERR)
		return (1);

	if (scds_get_ext_property(handle, DNS_MODE_USED, SCHA_PTYPE_STRING,
	    &mode_used) != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Property %s is not set.", DNS_MODE_USED);
		scds_close(&handle);
		return (1);
	} else {
		/* make a copy of the mode and free the ext property */
		mode = strdup(mode_used->val.val_str);
		scds_free_ext_property(mode_used);
		if (mode == NULL) {
			scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
				"Out of memory");
			scds_close(&handle);
			return (1);
		}
	}

	/* Start the server */
	retval = svc_start(handle, mode);
	free(mode);
	if (retval != 0) {
		scds_close(&handle);
		return (retval);
	}
	scds_syslog_debug(DBG_LEVEL_HIGH,
		"Done with svc_start, calling svc_wait.");

	/*
	 * Now wait until the server is operational,
	 * indicated by its ability to respond to probes.
	 */
	retval = svc_wait(handle);
	scds_syslog_debug(DBG_LEVEL_HIGH,
		"Done with svc_wait which returned <%d>.", retval);

	scds_close(&handle);
	return (retval);
}
