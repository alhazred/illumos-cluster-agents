/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 1998-2002 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * dns_monitor_check.c - Monitor Check method for highly available DNS
 */

#pragma ident	"@(#)dns_monitor_check.c	1.14	07/06/06 SMI"

#include <string.h>
#include "dns.h"

/*
 * There is nothing to be done here for HA-DNS
 */

int
main(int argc, char *argv[])
{
	scds_handle_t handle;
	char *mode;
	scha_extprop_value_t *mode_used;

	if (scds_initialize(&handle, argc, argv) != SCHA_ERR_NOERR)
		return (1);

	if (scds_get_ext_property(handle, DNS_MODE_USED, SCHA_PTYPE_STRING,
	    &mode_used) != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Property %s is not set.", DNS_MODE_USED);
		scds_close(&handle);
		return (1); /* failed */
	} else {
		/* make a copy of the mode and free the ext property */
		mode = strdup(mode_used->val.val_str);
		scds_free_ext_property(mode_used);
		if (mode == NULL) {
			scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
				"Out of memory");
			scds_close(&handle);
			return (1);
		}
	}

	if (svc_validate(handle, mode, B_FALSE) != SCHA_ERR_NOERR) {
		free(mode);
		scds_syslog(LOG_ERR, "Failed to validate configuration.");
		scds_close(&handle);
		return (1);	/* Validation failure */
	}

	scds_syslog(LOG_INFO, "Completed successfully.");
	scds_close(&handle);
	free(mode);

	return (0);
}
