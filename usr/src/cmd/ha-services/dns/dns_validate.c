/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 1998-2002 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * dns_validate.c - validate method for highly available DNS
 */

#pragma ident	"@(#)dns_validate.c	1.16	07/06/06 SMI"

#include <string.h>
#include <stdio.h>
#include <locale.h>
#include <libintl.h>
#include "dns.h"

/*
 * Do some basic sanity check of the configuration here.
 */

int
main(int argc, char *argv[])
{
	scds_handle_t handle;
	char *mode;
	scha_extprop_value_t *mode_used;
	int rc;

	/* I18N stuff */
	(void) setlocale(LC_ALL, "");
	(void) textdomain(TEXT_DOMAIN);
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	if ((rc = scds_initialize(&handle, argc, argv)) != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to retrieve the resource "
			"handle: %s", scds_error_string(rc));
		(void) fprintf(stderr, gettext("Failed to retrieve the "
			"resource handle: %s\n"),
			gettext(scds_error_string(rc)));
		return (1);
	}

	if (scds_get_ext_property(handle, DNS_MODE_USED, SCHA_PTYPE_STRING,
	    &mode_used) != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Property %s is not set.", DNS_MODE_USED);
		(void) fprintf(stderr, gettext("Property %s is not set.\n"),
			DNS_MODE_USED);
		scds_close(&handle);
		return (1);
	} else {
		/* make a copy of the mode and free the ext property */
		mode = strdup(mode_used->val.val_str);
		scds_free_ext_property(mode_used);
		if (mode == NULL) {
			scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
				"Out of memory");
			(void) fprintf(stderr, gettext("INTERNAL ERROR: %s.\n"),
				gettext("Out of memory"));
			scds_close(&handle);
			return (1);
		}
	}

	if (svc_validate(handle, mode, B_TRUE) != SCHA_ERR_NOERR) {
		free(mode);
		scds_syslog(LOG_ERR, "Failed to validate configuration.");
		(void) fprintf(stderr, gettext("Failed to validate "
			"configuration.\n"));
		scds_close(&handle);
		return (1);	/* Validation failure */
	}

	scds_syslog(LOG_INFO, "Completed successfully.");
	free(mode);
	scds_close(&handle);

	return (0);
}
