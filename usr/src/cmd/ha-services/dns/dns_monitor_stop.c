/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 1998-2002 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * dns_monitor_stop.c - Monitor Stop method for highly available DNS
 */

#pragma ident	"@(#)dns_monitor_stop.c	1.9	07/06/06 SMI"

#include "dns.h"

/*
 * This method stops the fault monitor for a HA-DNS resource.
 * This is done via PMF. The PMF tag for the fault monitor is
 * constructed based on RG-name_RS-name.mon.
 */

int
main(int argc, char *argv[])
{
	int rc;

	scds_handle_t handle;

	if (scds_initialize(&handle, argc, argv) != SCHA_ERR_NOERR)
		return (1);

	rc = svc_fm_stop(handle);
	scds_close(&handle);
	return (rc);
}
