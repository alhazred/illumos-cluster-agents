#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#ident	"@(#)Makefile	1.22	07/08/15 SMI"
#
# cmd/ha-services/dns/Makefile
#


SRCS =	dns_svc_start.c \
	dns_svc_stop.c \
	dns_validate.c \
	dns_update.c \
	dns_monitor_start.c \
	dns_monitor_stop.c \
	dns_monitor_check.c \
	dns_probe.c

PROG = $(SRCS:%.c=%)

OBJS = $(SRCS:%.c=%.o)

DS_COMMON = $(SRC)/cmd/ha-services/common
LIBDS_COMMON_ARCH = $(MACH)

DS_COMMON_SRCS = $(DS_COMMON)/ds_utils.c \
	$(DS_COMMON)/ds_http.c

COMMON_SRCS = dns.c
COMMON_OBJS = $(COMMON_SRCS:%.c=%.o)

OBJECTS = $(OBJS) $(COMMON_OBJS)

include ../../Makefile.cmd

PKGNAME = SUNWscdns
RTRFILE = SUNW.dns

TEXT_DOMAIN = SUNW_SC_DNS
MESSAGE_DIR = /opt/$(PKGNAME)/lib/locale
PIFILES = $(SRCS:%.c=%.pi) $(COMMON_SRCS:%.c=%.pi) \
	  $(DS_COMMON_SRCS:%.c=%.pi)
POFILE = dns-ds.po

CPPFLAGS += -I$(DS_COMMON)

LDLIBS  += -ldsdev -lscha
LDLIBS  += -L$(DS_COMMON)/$(LIBDS_COMMON_ARCH) -lds_common

$(POST_S9_BUILD)COPTFLAG += -DPOST_S9_BUILD

LINTFLAGS += -I$(DS_COMMON)
LINTFILES += $(SRCS:%.c=%.ln) $(COMMON_SRCS:%.c=%.ln)

.KEEP_STATE:

all: $(COMMON_OBJS) $(PROG) $(ROOTOPTETCRTR)

.PARALLEL: $(PROG)

install: all $(ROOTOPTBINPROG)

include ../../Makefile.targ

$(SRCS:%.c=%): $(COMMON_OBJS) $$(@:%=%.o)
	$(LINK.c) -o $@ $(@:%=%.c) $(COMMON_OBJS) $(LDFLAGS) $(LDLIBS)
	$(POST_PROCESS)

clean:
	$(RM) $(PROG) $(OBJS) $(COMMON_OBJS)
