/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 1998-2002 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * dns_update.c - Update method for highly available DNS
 */

#pragma ident	"@(#)dns_update.c	1.15	07/06/06 SMI"

#include "dns.h"

/*
 * If parameters related to the fault monitor are changed,
 * just restarting the monitor should be enough.
 * For DNS related configuration changes (e.g. if the user has
 * edited named.conf in some way...) we need to restart the
 * DNS daemon as well?
 */

int
main(int argc, char *argv[])
{
	scds_handle_t handle;
	int result;

	if (scds_initialize(&handle, argc, argv) != SCHA_ERR_NOERR)
		return (1);

	/*
	 * Some of the properties have been changed. Restart the fault monitor
	 * atleast.
	 */

	result = scds_pmf_restart_fm(handle, 0);
	if (result != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to restart fault monitor.");
		scds_close(&handle);
		return (1);
	}

	scds_syslog(LOG_INFO, "Completed successfully.");
	scds_close(&handle);

	return (0);
}
