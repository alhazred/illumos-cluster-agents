/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_DNS_COMMON_H
#define	_DNS_COMMON_H

#pragma ident	"@(#)dns.h	1.13	09/01/04 SMI"

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <rgm/libdsdev.h>

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Application name defined for use in scds_syslog messages.
 * Using this allows identical messages to be defined in more
 * than one data service, hashing to one message explanation.
 */
#define	APP_NAME "DNS Server"

#define	DNS_BINARY "/usr/sbin/in.named"

/* Location of alternate BIND 9 binary for Solaris 9 */
#define	DNS_ALT_BINARY	"/usr/lib/dns/named"

#define	 DNS_MODE_USED	"DNS_MODE"
#define	DEFAULT_MODE	"conf"

#define	SCDS_ARRAY_SIZE	1024

/* Debug levels for error messages */
#define	DBG_LEVEL_HIGH		9
#define	DBG_LEVEL_MED		5
#define	DBG_LEVEL_LOW		1

int svc_validate(scds_handle_t handle,
	char *mode, boolean_t print_messages);

int svc_start(scds_handle_t handle,
	char *mode);

int svc_stop(scds_handle_t handle);

int svc_fm_start(scds_handle_t handle);

int svc_fm_stop(scds_handle_t handle);

int svc_probe(scds_handle_t handle, boolean_t arg_syslog_msgs);

int svc_wait(scds_handle_t handle);

#ifdef	__cplusplus
}
#endif

#endif /* _DNS_COMMON_H */
