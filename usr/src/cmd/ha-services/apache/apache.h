/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 1998-2003 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_APACHE_COMMON_H
#define	_APACHE_COMMON_H

#pragma ident	"@(#)apache.h	1.31	07/06/06 SMI"

#include <rgm/libdsdev.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Debug levels for error messages */
#define	DBG_LEVEL_HIGH		9
#define	DBG_LEVEL_MED		5
#define	DBG_LEVEL_LOW		1

#define	SCDS_CMD_SIZE		(8*1024)

#define	SCDS_ARRAY_SIZE		1024

/*
 * Application name defined for use in scds_syslog messages.
 * Using this allows identical messages to be defined in more
 * than one data service, hashing to one message explanation.
 */
#define	APP_NAME		"Apache Web Server"

typedef enum {ST_ERROR = -1, REGULAR, MOD_SSL, APACHE_SSL} server_type;


int svc_validate(scds_handle_t scds_handle, boolean_t print_messages);

int svc_start(scds_handle_t scds_handle);

int svc_stop(scds_handle_t scds_handle);

int svc_wait(scds_handle_t scds_handle);

int mon_start(scds_handle_t scds_handle);

int mon_stop(scds_handle_t scds_handle);

int svc_probe(scds_handle_t scds_handle, char *hostname,
	int port, int timeout, boolean_t arg_syslog_msgs);

server_type webserver_type(scds_handle_t scds_handle);

int probe_uri(scds_handle_t scds_handle, char *uri, int timeout,
    boolean_t log_messages);

#ifdef __cplusplus
}
#endif

#endif /* _APACHE_COMMON_H */
