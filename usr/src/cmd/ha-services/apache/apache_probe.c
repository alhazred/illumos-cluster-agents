/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 1998-2003 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * apache_probe.c - Probe for highly available apache
 */

#pragma ident	"@(#)apache_probe.c	1.36	07/06/06 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <rgm/libdsdev.h>
#include "apache.h"

int
main(int argc, char *argv[])
{
	scds_handle_t	scds_handle;
	scha_err_t	err = SCHA_ERR_NOERR;
	int		timeout, probe_interval,
			port, i, probe_result;
	hrtime_t	ht1, ht2;
	long		dt;
	char		*hostname = NULL;
	scds_netaddr_list_t *netaddr;
	scha_extprop_value_t *urilist = NULL;
	scha_str_array_t *uris = NULL;

	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR)
		return (1);

	/* Get the ip addresses available for this resource */
	err = scds_get_netaddr_list(scds_handle, &netaddr);
	if (err != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
			"Error in trying to access the "
			"configured network resources : %s.",
			scds_error_string(err));
		scds_close(&scds_handle);
		return (1);
	}

	/* Return an error if there are no network resources */
	if (netaddr == NULL || netaddr->num_netaddrs == 0) {
		scds_syslog(LOG_ERR,
			"No network address resource in resource group.");
		return (1);
	}

	/* Retrieve the list of uris that we were told to probe */
	err = scds_get_ext_property(scds_handle, "Monitor_Uri_List",
			SCHA_PTYPE_STRINGARRAY, &urilist);
	if ((err != SCHA_ERR_NOERR) && (err != SCHA_ERR_PROP)) {
		/* failed with something other than SCHA_ERR_PROP */
		scds_syslog(LOG_ERR,
				"Failed to retrieve the "
				"property %s: %s.",
				"Monitor_Uri_List", scds_error_string(err));
		return (1);
	}

	if (urilist != NULL) {
		uris = urilist->val.val_strarray;
	}

	/*
	 * Get the timeout from the extension props. This means that
	 * each probe iteration will get a full timeout on each network
	 * resource without chopping up the timeout between all of the
	 * network resources configured for this resource.
	 */
	timeout = scds_get_ext_probe_timeout(scds_handle);

	/* Get interval for sleep between probes */
	probe_interval = scds_get_rs_thorough_probe_interval(scds_handle);

	for (;;) {

		/*
		 * sleep for a duration of thorough_probe_interval between
		 * successive probes.
		 */
		(void) scds_fm_sleep(scds_handle, probe_interval);

		/*
		 * Now probe all netaddress we use.
		 * For each of the netaddress that is probed,
		 * compute the failure history.
		 */
		probe_result = 0;
		/*
		 * Iterate through all the netaddrs calling svc_probe()
		 */
		for (i = 0; i < netaddr->num_netaddrs; i++) {
			/*
			 * Grab the hostname and port on which the
			 * health has to be monitored.
			 */
			hostname = netaddr->netaddrs[i].hostname;
			port = netaddr->netaddrs[i].port_proto.port;
			ht1 = gethrtime(); /* Latch probe start time */

			probe_result = svc_probe(scds_handle, hostname, port,
				timeout, B_TRUE);

			ht2 = gethrtime();

			/* Convert to milliseconds */
			dt = (long)((ht2 - ht1) / 1e6);

			/*
			 * Compute failure history and take action if needed
			 */
			(void) scds_fm_action(scds_handle, probe_result, dt);

		}	/* Each netaddr */

		/* probe all uris (if any) that were supplied */
		if (uris) {
			for (i = 0; i < (int)uris->array_cnt; i++) {
				ht1 = gethrtime();
				probe_result = probe_uri(scds_handle,
				    uris->str_array[i], timeout, B_TRUE);
				ht2 = gethrtime();

				dt = (long)((ht2 - ht1) / 1e6);

				(void) scds_fm_action(scds_handle, probe_result,
				    dt);
			}
		}

	} 	/* Keep probing forever */
}
