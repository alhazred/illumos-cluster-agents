#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# ident	"@(#)Makefile	1.8	07/06/06 SMI"
#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#
# cmd/ha-services/apache/dsconfig/Makefile
#

include $(SRC)/cmd/Makefile.cmd

PKGNAME=SUNWscapc/dsconfig

COMMON=$(SRC)/cmd/ha-services/common/dsconfig

CONFIG_FILE =   ApacheServiceConfig.xml \
		ScalableApacheServiceConfig.xml

BINARIES = haapache_config.jar

ROOTOPTETC_CONFIG=$(CONFIG_FILE:%=$(ROOTOPTETC)/%)

ROOTOPTLIB_JAR=$(BINARIES:%=$(ROOTOPTLIB)/%)

DS_COMMON = $(COMMON)/src/

JAVA_SRCS=find src -type d -name SCCS -prune -o -name ,\* -o -name \*.java -print; \
          find $(DS_COMMON) -type d -name SCCS -prune -o -name ,\* -o -name \*.java -print

CLASS_FILES=find * -type d -name SCCS -prune -o -name \*.class -print

CLASSES_DIRECTORY=classes

.PARALLEL: all

CLASSPATH=$(DSCONFIG_REF_PROTO)/usr/cluster/lib/cmass/cmas_agent_dataservices.jar:.

DOC_DIR=javadoc
JAVADOCS=$(DOC_DIR)/index.html

TEXT_DOMAIN = SUNW_SC_APACHE_DSCONFIG

MESSAGE_DIR = /opt/$(PKGNAME)/lib/locale

POFILE = apache_dsconfig.po

PIFILES  = 

all: $(BINARIES) .WAIT $(JAVADOCS)

$(BINARIES):    $(JAVA_SRCS:sh)
	@echo `$(JAVA_SRCS)` > java-file-list
	mkdir -p $(CLASSES_DIRECTORY); \
	CMD="$(JAVAC) $(JAVAFLAGS) -classpath $(CLASSPATH) -d $(CLASSES_DIRECTORY) @java-file-list";\
	echo $$CMD; \
	$$CMD
	cd $(CLASSES_DIRECTORY); \
	$(JAR) -cf $(BINARIES) `$(CLASS_FILES)`; \
	mv $(BINARIES) ..; \
	cd .. \

$(DOC_DIR)/index.html: $(BINARIES)
	mkdir -p $(DOC_DIR); \
	$(JAVADOC) -sourcepath . \
	-classpath $(CLASSPATH) \
	-d $(DOC_DIR) \
	-doctitle 'HA Apache config classes' \
	-windowtitle 'Dsconfig HA-Apache' \
	-author -version -use \
	`$(JAVA_SRCS)`

.KEEP_STATE:

install: all $(ROOTOPTETC_CONFIG) $(ROOTOPTLIB_JAR)

clean:
	$(RM) $(BINARIES) java-file-list
	$(RM) -rf $(DOC_DIR) $(CLASSES_DIRECTORY)

include $(SRC)/cmd/Makefile.targ
