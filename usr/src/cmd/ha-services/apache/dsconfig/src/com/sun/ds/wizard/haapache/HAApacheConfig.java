/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident        "@(#)HAApacheConfig.java 1.19     08/11/03 SMI"
 */


package com.sun.ds.wizard.haapache;

// JDK
import java.io.*;
import java.util.*;
import java.util.regex.*;
import java.net.*;
import com.sun.ds.wizard.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.sun.cluster.agent.dataservices.common.ErrorValue;
import com.sun.cluster.agent.dataservices.common.ServiceConfig;
import com.sun.cluster.agent.dataservices.utils.Util;

/**
 * This class implements the ServiceConfigInterface to provide auto-discovery
 * and validation implmentation for HA-Apache
 */

public class HAApacheConfig implements ServiceConfig {

        static Logger logger = null;
        // PATH for Edited Conf File, Control File
        private static final String APACHE_TMP_CONF =
            "/opt/cluster/lib/ds/history/tmp_httpd.conf";
        private static final String APACHE_TMP_CNTL =
            "/opt/cluster/lib/ds/history/tmp_apachectl";

        // PATH for apache configuration script
        private static final String APACHE_CONFIG_SCRIPT =
            "/usr/cluster/lib/ds/apache/configureApache.ksh";

        // Relative PATH for apachectl File
        private static final String APACHECTL_BIN = "/bin/apachectl";

        // Default path for apachectl file
        private static final String APACHECTL_BIN_DEFAULT =
            "/usr/apache/bin/apachectl";

        // Default path for apache2 dirrectory
        private static final String APACHE2_BINDIR_DEFAULT =
            "/usr/apache2/";

	// Default path for httpd conf file
	private static final String APACHE_EXAMPLE_CONF_FILE =
		"/etc/apache/httpd.conf-example";

	// Default path for apache2 httpd conf file directory
	private static final String APACHE2_CONF_FILE_DIR =
		"/etc/apache2/";

	// HTTPD Variable in apachectl file
	private static final String HTTPD = "HTTPD=";

	// Supported Apache versions. When new version is to be added
        // add it to this array.
        private String[] SupportedVersions = { "1.", "2." };

	/**
	 * This function will return the names of the properties that can be
	 * discovered for HA-Apache dataservice.
	 * @return Array of property names
	 */
	public String[] getDiscoverableProperties() {
		String[] properties = { "APACHE_INSTALLATION_TYPE",
					"ZONE_ENABLE"};

		if (logger != null)
			logger.log(Level.FINE, "{0}", properties);

		return properties;
	}


	/**
	 * This function will return the names of all the
	 * properties that are user configurable.
	 * @return Array of user configurable property names
	 */
	public String[] getAllProperties() {
		String[] properties = { "APACHE_HOME",
			"APACHE_DOC",
			"APACHE_CONF_FILE",
			"APACHE_PORT"};

			if (logger != null)
				logger.log(Level.FINE, "{0} {1} {2} {3}",
						properties);

			return properties;
	}

        /**
         * To test if the port is available
         * @param hostName Hostname for e.g local host, ipaddress , hostname
         * @param portNo Port number to test
         * @return False if port is used by another process
         * True if not
         */
	boolean isPortInUse(String hostName, int portNo) {
            // Create a socket with a timeout
            try {
		InetAddress addr = InetAddress.getByName(hostName);
		SocketAddress sockaddr = new InetSocketAddress(addr, portNo);

                // Create an unbound socket
                Socket sock = new Socket();

                // This method will block no more than timeoutMs.
                // If the timeout occurs, SocketTimeoutException is thrown.
                int timeoutMs = 2000;   // 2 seconds
                sock.connect(sockaddr, timeoutMs);
		sock.close();
                return true;
            } catch (UnknownHostException e) {
                // We do not expect a unknown host here
                return false;
            } catch (SocketTimeoutException e) {
                // Expect a socket timeout if the host is unplumbed
                return false;
	    } catch (IOException e) {
                // Unable to open the socket, port is not being used
                return false;
            } catch (IllegalArgumentException e) {
		// port out of range
		return false;
	    }
        }


	/**
	 * This function will discover the property values
	 * from the httpd.conf file.
	 * @param httpdLoc Location of httpd.conf file
	 * @param directive Property name
	 * @return Array of String values obtained from httpd.conf
	 */
	String[] getApachePropertyValue(String httpdLoc, String directive) {
		String match = null;
		if (directive.equals("APACHE_HOME"))
			match = "ServerRoot";
		else
			if (directive.equals("APACHE_DOC"))
				match = "DocumentRoot";
			else
				if (directive.equals("APACHE_PORT"))
					match = "Port";
				else
				    if (directive.equals(Util.PIDFILE_DIR))
					    match = "ErrorLog";
				    else
					return null;
		Pattern exp = Pattern.compile(match);
		Matcher matcher = exp.matcher(match);
		ArrayList directiveValues = new ArrayList();
		try {
			final File fp = new File(httpdLoc);
			LineNumberReader lineReader = null;

			lineReader = new LineNumberReader(new FileReader(fp));
			String line = null;
			while ((line = lineReader.readLine()) != null) {
				matcher.reset(line); // reset the input
				if (matcher.find()) {
					String[] result = line.split("\\s");
					StringBuffer buffer =
						new StringBuffer(match);
                                        String value =
                                            result[1].replace('\"', ' ').trim();
					if (result[0].contentEquals(buffer) &&
					   (!directiveValues.contains(value))) {
						directiveValues.add(value);
					}
				}
			}
			if (lineReader != null)
				lineReader.close();

		} catch (FileNotFoundException fex) {
			return null;
		} catch (IOException ioex) {
			return null;
		}

		String[] str = new String [directiveValues.size()];
		directiveValues.toArray(str);
		return str;
	}

	/**
	 * Determines if the apache RC scripts are present in the system
	 * @throws java.lang.NullPointerException Thrown if file name is null
	 * @return returns if the RC scripts are present or not, this will
	 * allow the cacao agent to disable the apache autostart.
	 */

	boolean isRCScriptPresent() throws NullPointerException {

		File file = null;

		file = new File("/etc/rc3.d/S50apache");
		if (file.exists())
			return true;
		else
			return false;
	}

	/**
	 * Discovers possible apache configuration files that are
	 * present on the system to be used a template for the apache
	 * configuration. The code discovers apache 1.0 and apache 2.0
	 * example configuration files. The zone path is passed in as
	 * argument if the nodelist contains a zone.
	 * @return returns a string array of all the files that are
	 * discovered
	 */
	private String[] discoverApacheConfFiles(Object zonePath) {

            ArrayList apacheHomeList = null;
            String apacheHome = null;
	    String zoneExportPath = null;

	    if (zonePath != null) {
		zoneExportPath = (String)zonePath;
                if (!zoneExportPath.endsWith("/")) {
                    zoneExportPath = zoneExportPath + "/";
		}
	    }

	    File apache2bindir = new File(APACHE2_BINDIR_DEFAULT);

	    String binFile = "apachectl";
            apacheHomeList = new ArrayList();
	    if (apache2bindir.exists()) {
            	apacheHomeList = searchFile(apache2bindir, binFile,
						apacheHomeList, zoneExportPath);
	    }
            // Prepend ZonePath if needed
            if (zoneExportPath != null) {
                apacheHome = zoneExportPath + APACHECTL_BIN_DEFAULT;
		File bindir = new File(apacheHome);
		if (bindir.exists())
                	apacheHomeList.add(apacheHome);
            } else {
                apacheHome = APACHECTL_BIN_DEFAULT;
		File bindir = new File(apacheHome);
		if (bindir.exists())
                	apacheHomeList.add(apacheHome);
            }

	    /*
	     * Discover Apache2.* configuration files
	     */
	    File apache2confdir = new File(APACHE2_CONF_FILE_DIR);

	    ArrayList confFilePathList = new ArrayList();
	    String confFile = "httpd.conf";
	    if (apache2confdir.exists()) {
            	confFilePathList = searchFile(apache2confdir, confFile,
					confFilePathList, zoneExportPath);
	    }

            // Look for apachectl at /usr/apache/bin (which is the default
            // installation location)
            String confFilePath = null;
            for (int i = 0; i < apacheHomeList.size(); i++) {
                apacheHome = (String)apacheHomeList.get(i);
                File controlFile = new File(apacheHome);
                if (controlFile.exists()) {
                    try {
                    // Parse the file for Conf File
                    FileReader confFileReader = new FileReader(
                            apacheHome);
                    BufferedReader fileInput =
			new BufferedReader(confFileReader);
                    String text;
                    while ((text = fileInput.readLine()) != null) {
                        // Match for HTTPD='/usr/apache/bin/httpd -f
			// <file-name>'. The discovery will not work
			// for any other pattern and ArrayIndexOutOfBounds
			// exception is generated.
                        if (text.indexOf(HTTPD) == 0) {
                            confFilePath = (text.split("="))[1];
                            confFilePath = confFilePath.split(" ")[2];
                            confFilePath = confFilePath.replace('\'', ' ');
                            confFilePath = confFilePath.trim();
                            break;
                        }
                    }
                    fileInput.close();
                    } catch (IOException ioe) {
                        confFilePath = null;
                    } catch (ArrayIndexOutOfBoundsException aoe) {
                        confFilePath = null;
                    } catch (Exception e) {
                        confFilePath = null;
                    }
                }

                // Check for confFile existence
		if ((confFilePath != null) &&
                        (isFile(confFilePath, zoneExportPath))) {
                        confFilePathList.add(confFilePath);
		}
            }


            // Check for example Files
            String exampleFile =  APACHE_EXAMPLE_CONF_FILE;

                if (isFile(exampleFile, zoneExportPath))
                    confFilePathList.add(exampleFile);

            return ((String[])confFilePathList.toArray(new String[0]));
	}


	/**
	 * This function will return all the possible values
	 * for a given array of property names.
	 * Inputs: The propertyNames string array is given as
	 * input to this method.Optionally, a helperData Object can be
	 * provided to aid in the discovery process. Discovering possible
	 * values of APACHE_HOME, given a httpd.conf file locaton is a
	 * example of such input.
	 * Outputs: The method returns HashMap of array of strings indexed
	 * by the property name, containing all the possible values of the
	 * property in the local cluster node.
	 * @param propertyNames The propertyNames string array is given as
	 * input to this function
	 * @param helperData helperData Object to aid in the discovery process.
	 * @return Hash map of array of strings containing possible values
	 */
	public HashMap discoverPossibilities(String[] propertyNames,
		Object helperData) {

		HashMap properties = new HashMap();

		for (int i = 0; i < propertyNames.length; i++) {
			if (propertyNames[i].equalsIgnoreCase
					("APACHE_INSTALLATION_TYPE")) {
				if (isRCScriptPresent()) {
					properties.put(propertyNames[i], "yes");
				} else {
					properties.put(propertyNames[i], "no");
				}
			} else if (propertyNames[i].
					equalsIgnoreCase("ZONE_ENABLE")) {
				// Apache dataservice support zones.
				// Return false to disable zones support.
				properties.put("ZONE_ENABLE",
						new Boolean(true));
			} else if (propertyNames[i].
				equalsIgnoreCase(Util.APACHE_CONF_FILE)) {
				 properties.put(propertyNames[i],
					discoverApacheConfFiles(helperData));
			} else if (propertyNames[i].
					equalsIgnoreCase(Util.APACHE_VERSION)) {
				 String ver = null;
				 try {
					ver = getApacheVersion(
						(String)helperData);
				 } catch (Exception e) {
					ver = null;
				 }
				 properties.put(propertyNames[i], ver);
			} else {
				properties.put(propertyNames[i],
					getApachePropertyValue
					((String)helperData, propertyNames[i]));
			}
		}
		if (properties.isEmpty()) {
			return null;
		} else {
			return  properties;
		}
	}

        /**
         * Get the version of the httpd
         * @param String serverRoot
         */
	String getApacheVersion(String serverRoot) throws
							Exception {
            String  cmd[] = { "/bin/ksh", serverRoot + "/bin/httpd -v "
			};
            Exception exp = null;
            Exec httpdCmd = null;
            try {
                httpdCmd = new Exec(cmd, null);
                httpdCmd.streamGlobbler(httpdCmd.getInputStream(), "Output");
                httpdCmd.streamGlobbler(httpdCmd.getErrorStream(), "Error");
                httpdCmd.waitFor();
                httpdCmd.join();
            } catch (SecurityException ex) {
                exp = ex;
            } catch (IOException ioex) {
                exp = ioex;
            } catch (IllegalArgumentException iex) {
                exp = iex;
            } catch (InterruptedException intex) {
                exp = intex;
	    }
            if (httpdCmd.exitValue() != 0 || exp != null) {
		throw new Exception(httpdCmd.getResult("Error"));
	    }
	    String output = (String) httpdCmd.getResult("Output");
	    // We grep for the Supported Version in the command output
	    // Add the supported versions to the "SupportedVersions" array
	    // to add support for more versions

	    // The expected output is :
	    // Server version: Apache/<version number>

	    boolean bSupported = false;
	    int i;
	    for (i = 0; i < SupportedVersions.length; i++) {
		if (output.indexOf("/" + SupportedVersions[i]) != -1) {
		    bSupported = true;
		    break;
		}
	    }
	    if (bSupported) {
		return SupportedVersions[i];
	    } else {
		return "";
	    }
	}

	/*
	 * Discovery of Apache 2.* configuration files.
	 * @param File apachedir Directory where apache configuration
	 * file/binary file is present.
	 * @param String confFile Name of the configuration file/binary file.
	 * @param ArrayList confFileArray Return Array where the absolute
	 * pathname of the files discovered is stored.
	 * @param String zoneExportPath
	 */
	ArrayList searchFile(File apachedir, String confFile,
				ArrayList confFileArray,
				String zoneExportPath) {

	   File[] directories = apachedir.listFiles();
		for (int i = 0; i < directories.length; i++)
		{
		   if (directories[i].isDirectory())
		   {
			if (directories[i].listFiles() != null)
				searchFile(directories[i], confFile,
					confFileArray, zoneExportPath);
		   } else {
			if (directories[i].getName().startsWith(confFile) &&
				directories[i].exists() &&
				directories[i].canRead()) {
			   if (zoneExportPath != null) {
				confFileArray.add(
				directories[i].getAbsolutePath().replaceFirst(
						zoneExportPath, ""));
			   } else {
				confFileArray.add(
				directories[i].getAbsolutePath());
			   }
			}
		   }
		}
	   return (confFileArray);
	}

        /**
         * Check the version of the httpd
         * @param String serverRoot
         */
        ErrorValue checkServerRoot(String serverRoot) {

	    String version;
            ErrorValue error = new ErrorValue();
	    try {
		version = getApacheVersion(serverRoot);
	    } catch (Exception e) {
                error.setErrorString(e.getMessage());
                error.setReturnVal(new Boolean(false));
                return error;
	    }
	    if (version.length() == 0) {
		error.setErrorString(
			"The server root that is specified in the " +
			"configuration file corresponds to an Apache " +
			"version that is not supported by the wizard.");
		error.setReturnVal(new Boolean(false));
		return error;
	    }
	    error.setReturnVal(new Boolean(true));
	    return error;
        }

	/**
	 * Check the Syntax of the httpd.conf file using httpd -f -t conf_file
	 * @param apacheHomeLocation httpd location
	 * @param configFile http.conf file path
	 * @return error object with the error value set appropriately
	 */

	ErrorValue checkSyntaxOfConfigurationFile(String configFile,
		String apacheHomeLocation) {

		String  cmd[] = { "/bin/ksh", apacheHomeLocation +
				"/bin/httpd -t -f " + configFile};

		Exception exp = null;
		ErrorValue error = new ErrorValue();
		Exec httpdCmd = null;
		try {
			httpdCmd = new Exec(cmd, null);
			httpdCmd.streamGlobbler(httpdCmd.getInputStream(),
								"Output");
			httpdCmd.streamGlobbler(httpdCmd.getErrorStream(),
								"Error");
			httpdCmd.waitFor();
			httpdCmd.join();
		} catch (SecurityException ex) {
			exp = ex;
		} catch (IOException ioex) {
			exp = ioex;
		} catch (IllegalArgumentException iex) {
			exp = iex;
		} catch (InterruptedException intex) {
			exp = intex;
		}

		Object[] result = {httpdCmd.getResult("Error"),
			String.valueOf(httpdCmd.exitValue())};
		if (logger != null)
			logger.log(Level.FINE, "Result = {0} Exit Status = {1}",
				result);

		error.setErrorString(httpdCmd.getResult("Error"));
		if (httpdCmd.exitValue() != 0 || exp != null) {
			error.setReturnVal(new Boolean(false));
			return error;
		}
		error.setReturnVal(new Boolean(true));
		return error;
	}


	/**
	 * This function will validate the user given value from the wizard and
	 * return a ErrorValue object.
	 * Inputs: The propertyNames string array is given as input to this
	 * method. userInputs contains a hash map of the values that the user
	 * has chosen to set indexed by the property.
	 * Optionally, a helperData Object can be provided to aid in the
	 * validation process.
	 * Outputs: The method returns ErrorValue object that contains the
	 * result of the validation
	 * @param propertyNames string array is given as input to this method.
	 * @param userInputs hash map of the values that the user has chosen to
	 * set indexed by the property
	 * @param helperData provided to aid in the validation process
	 * @return Error object containing the result of validation
	 */
	public ErrorValue validateInput(String[] propertyNames,
		HashMap userInputs, Object helperData) {

		List strList = Arrays.asList(propertyNames);
		ErrorValue error = new ErrorValue();
		error.setReturnVal(new Boolean(true));

		if (strList.contains("APACHE_CONF_FILE")) {
                    // Check if the ServerRoot points to the supported
                    // Apache Version
                    ErrorValue serverRootErrValue = checkServerRoot(
                            (String)helperData);
		    if (!serverRootErrValue.getReturnValue().booleanValue()) {
			return serverRootErrValue;
		    } else {
			// Check configuration file's syntax
			ErrorValue confFileSyntaxErrValue =
				checkSyntaxOfConfigurationFile(
				(String)userInputs.get("Conf_File"),
				(String)helperData);
			return confFileSyntaxErrValue;
		    }
		}
		if (strList.contains("APACHE_PORT")) {
			String[] portNos = ((String)
					userInputs.get("Port_List")).split(",");
			String hostName = (String)userInputs.get("Host_Name");
			StringBuffer invalidPorts = new StringBuffer();
			StringBuffer inusePorts = new StringBuffer();
			for (int i = 0; i < portNos.length; i++) {
			    int iPortNo = -1;
			    try {
				iPortNo = Integer.parseInt(portNos[i].trim());
				if (iPortNo < 0 || iPortNo > 65535) {
				    iPortNo = -1;
				}
			    } catch (NumberFormatException nfe) {
			    }

			    boolean portInUse = false;
			    if (iPortNo != -1) {
				portInUse = isPortInUse(hostName, iPortNo);
			    }
			    if (portInUse || iPortNo == -1) {
				error.setReturnVal(new Boolean(false));
			    }
			    if (iPortNo == -1) {
				if (invalidPorts.length() > 0) {
				    invalidPorts.append(",");
				}
				invalidPorts.append(portNos[i]);
			    } else if (portInUse) {
				if (inusePorts.length() > 0) {
				    inusePorts.append(",");
				}
				inusePorts.append(portNos[i]);
			    }
			}
			StringBuffer errStr = new StringBuffer();
			if (inusePorts.length() > 0) {
			    errStr.append("Port number(s) "+
					inusePorts.toString() + "in use\n");
			}
			if (invalidPorts.length() > 0) {
			    errStr.append("Port number(s) "+
					invalidPorts.toString() + " not valid");
			}
			error.setErrorString(errStr.toString());

		}
		return error;
	}


	/**
	 * Enable the logging mechanism for the HA-Apache data service.
	 * @param log logger is initialised by the cacao agent and is set.
	 */
	public void setLogger(Logger log) {
		logger = log;
	}


	/**
	 * This method will filter a list of discovered values to be
	 * presented for user selection.
	 * Inputs: The propertyName string is given as input to this method.
	 * unfilteredValues contains the hash map of the nodenames and
	 * a array of discovered values on that particular node.
	 * helperData Object can be provided to aid in the filtering process.
	 * Outputs: The method returns a array of strings containing all the
	 * possible values of the property in all of cluster.
	 *
	 * This is a dummy implementation and returns null
	 * @param propertyName string[] is given as input to this method
	 * @param unfilteredValues hash map of the nodenames and a array of
	 * discovered values on that particular node
	 * @param helperData provided to aid in the filtering process
	 * @return Array of String containing all the possible values of the
	 * propertyin all of cluster
	 */
	public String[] aggregateValues(String propertyName,
		Map unfilteredValues, Object helperData) {
		return null;
	}

	/**
	 * Configures Apache control file and configuration file
	 * on the cluster node based on the applicationConfiguration data.
	 * This method is run on the node which the wizard was connecting to.
	 *
	 *
	 * The apachectl file is read from the node(or zone using the zone path)
	 * and the HTTPD variable is changed. The edited file is stored in the
	 * ds-config temp location. The configureApache script copies this file
	 * to the HA mount point (which may be mounted on the same node or
	 * any other node)
	 *
	 * The user specified configuration file is read from the node(or zone)
	 * and the Listen, Document Root directives are changed. The edited file
	 * is stored in the ds-config temp location.
	 * The configureApache script copies this file to the HA mount point
	 * (which may be mounted on the same node or any other node)
	 *
	 */
	public ErrorValue applicationConfiguration(Object helperData) {

            try {
                ErrorValue errorValue = null;

                Map confMap = (HashMap)helperData;

                String mountPoint = (String)confMap.get(
                        "APACHE_MOUNT");
                String apacheServerRoot = (String)confMap.get(
                        "APACHE_HOME");
                String apachePort = (String)confMap.get(
                        "APACHE_PORT");
                String apacheListen = (String)confMap.get(
                        "APACHE_LISTEN");
                String apacheConfFilePath = (String)confMap.get(
                        "APACHE_CONF_FILE");
                String documentRoot = (String)confMap.get(
                        "APACHE_DOC");
                String zoneExportPath = (String)confMap.get(
                        "APACHE_ZONE");
                String apacheResName = (String)confMap.get(
                        "APACHE_RESOURCE_NAME");
                String pidFileLocation = (String)confMap.get(
                        Util.PIDFILE);

		// Construct LockFile directive from PidFile directive
                // as the directory is same for both the directives.
                String lockFileLocation = null;
		if (pidFileLocation != null &&
		    pidFileLocation.length() > 0) {
                	File fp = new File(pidFileLocation);
                	String lockFileDir = fp.getParent();
                	if (lockFileDir != null) {
				StringBuffer lockFileBuf = new
						StringBuffer(lockFileDir);
				lockFileBuf.append(Util.SLASH);
				lockFileBuf.append("accept.lock");
				lockFileLocation = lockFileBuf.toString();
			}
		}

                String tmpFilePath = APACHE_TMP_CONF;
                String confFileName = apacheConfFilePath.substring(
                        apacheConfFilePath.lastIndexOf('/')+1,
                        apacheConfFilePath.length());


		/**
		 * Change:
		 * HTTPD variable in the apachectl file to take
		 * the conf file,server root from a new path
		 *
		 * HTTPD=<serverroot>/bin/httpd would be changed to
		 * HTTPD=<serverroot>/bin/httpd -f
		 *		<HAMount>/etc/apache/httpd.conf
		 *
		 */
		StringBuffer newApacheConfFilePath = new StringBuffer();
		newApacheConfFilePath.append(mountPoint);
		newApacheConfFilePath.append(Util.SLASH);
		newApacheConfFilePath.append(apacheResName);
		newApacheConfFilePath.append("/etc/apache/");
		newApacheConfFilePath.append(confFileName);
		errorValue = editControlFile(apacheServerRoot,
		newApacheConfFilePath.toString(), zoneExportPath);
		if (!errorValue.getReturnValue().booleanValue()) {
			return errorValue;
		}


		/**
		 *
		 * Change :
		 * DocRoot,Listen,Port in the httpd.conf file
		 *
		 */
		documentRoot = documentRoot.replace('\"', ' ');
		documentRoot = documentRoot.trim();
		if (documentRoot.indexOf(mountPoint, 0) != 0) {
		// As Doc root has no relation with the mountpoint
		// DocRoot will be copied under the mountpoint

		// Tar the document root directory and place it in temp
		String documentRootToTar;
		if (zoneExportPath != null) {
			documentRootToTar = zoneExportPath + documentRoot;
		} else {
			documentRootToTar = documentRoot;
		}

		    // Remove trailing slash
		    if (documentRootToTar.endsWith("/")) {
			documentRootToTar = documentRootToTar.
					substring(0,
					documentRootToTar.length()-1);
		    }
		String lastDirInDocRoot = documentRootToTar.substring(
                            documentRootToTar.lastIndexOf('/')+1,
                            documentRootToTar.length());
		String dirToCD = documentRootToTar.substring(0,
                            documentRootToTar.lastIndexOf('/'));
		String  cmd[] = {APACHE_CONFIG_SCRIPT,
					"copyDocumentRoot",
					dirToCD,
					lastDirInDocRoot};
                    Exception exp = null;
                    ErrorValue error = new ErrorValue();
                    Exec httpdCmd = null;
                    try {
                        httpdCmd = new Exec(cmd, null);
                        httpdCmd.streamGlobbler(httpdCmd.getInputStream(),
								"Output");
                        httpdCmd.streamGlobbler(httpdCmd.getErrorStream(),
								"Error");
                        httpdCmd.waitFor();
                        httpdCmd.join();
                    } catch (SecurityException ex) {
			exp = ex;
		    } catch (IOException ioex) {
			exp = ioex;
		    } catch (IllegalArgumentException iex) {
			exp = iex;
		    } catch (InterruptedException intex) {
			exp = intex;
		    }

		    if (httpdCmd.exitValue() != 0 || exp != null) {
			error.setErrorString(httpdCmd.getResult("Error"));
			error.setReturnVal(new Boolean(false));
			return error;
		    }

		    // Construct the new document root, which has
		    // to be mentioned in the conf file
		    StringBuffer docRootBuffer = new StringBuffer();
		    docRootBuffer.append(mountPoint);
		    docRootBuffer.append(Util.SLASH);
		    docRootBuffer.append(apacheResName);
		    docRootBuffer.append(documentRoot);
		    documentRoot = docRootBuffer.toString();
		}
		errorValue = editApacheConfFile(
			apacheConfFilePath,
			tmpFilePath,
			apacheListen,
			apachePort,
			documentRoot, pidFileLocation, lockFileLocation);
		if (!errorValue.getReturnValue().booleanValue()) {
			return errorValue;
		}
	    } catch (Exception e) {
		System.out.println(e.getMessage());
	    }
	    return new ErrorValue(Boolean.TRUE, "");
	}

	/**
	 * Discovers the possible values for the given set of properties
	 * and Helper Data
	 *
	 * @param nodeList String array of nodes on which the discovery
	 * has to run.
	 * @param propertyNames String array of names of properties
	 * whose values are to be discovered.
	 * @param helperData  The HelperData that would be used for discovering
	 * the property values.
	 *
	 * @return A Map containing propertyNames as Key with their discovered
	 * values as Values.
	 */
        public HashMap discoverPossibilities(String[] nodeList,
                String[] propertyNames,	Object helperData) { return null; }


	/**
	 * Edits ths apache control file at the serverroot, so that the httpd
	 * takes the new conf file path. The edited file is stored in the
	 * ds-config temp location as tmp_apachectl with 400 permissions.
	 *
	 * @param serverRoot The Root of Apache Server, apachectl would
	 * be searched <serverRoot>/bin/
	 * @param newApacheConfFilePath New Conf File Path to which
	 * the file should be updated
	 * @param zoneExportPath Path of the zone to be added to the
	 * control file	that needs to be read
	 * @return ErrorValue
	 */
	private ErrorValue editControlFile(
		String serverRoot,
		String newApacheConfFilePath,
		String zoneExportPath) {

	    try {
		// Remove any ", spaces
		serverRoot = serverRoot.replace('\"', ' ');
		serverRoot = serverRoot.trim();

		// Add the full path
		String source = serverRoot+APACHECTL_BIN;
		String destination = APACHE_TMP_CNTL;

		// Add Zone Path if needed
		if (zoneExportPath != null) {
			source = zoneExportPath + source;
		}

		File sourceFile = new File(source);
		if (!sourceFile.canRead()) {
			throw new Exception("Cannot read " + source);
		}

		// To Read Apache Control File
		FileReader confFileReader = new FileReader(source);
		BufferedReader fileInput = new BufferedReader(confFileReader);
		// To Write Apache Control File
		FileWriter confFileWriter = new FileWriter(destination);
		PrintWriter fileOutput = new PrintWriter(confFileWriter);

		// Read the conf file in the specified directory
		// Replace the required lines in the destination file
		String text;
		while ((text = fileInput.readLine()) != null) {
		    // Match for HTTPD=
		    if (text.indexOf("HTTPD=") == 0) {
			text = "HTTPD='"+serverRoot+"/bin/httpd -f "
				+newApacheConfFilePath+"'";
			fileOutput.println(text);
		    }
		    // Write the text as it is
		    else {
			fileOutput.println(text);
		    }
		}
		fileInput.close();
		fileOutput.close();

		// Change permissions to "read-only" by root

		String  cmd[] = { "/usr/bin/chmod",
		"400",
		APACHE_TMP_CNTL};
		Exception exp = null;
		ErrorValue error = new ErrorValue();
		Exec httpdCmd = null;
		try {
		    httpdCmd = new Exec(cmd, null);
		    httpdCmd.streamGlobbler(httpdCmd.getInputStream(),
								"Output");
		    httpdCmd.streamGlobbler(httpdCmd.getErrorStream(), "Error");
		    httpdCmd.waitFor();
		    httpdCmd.join();
		} catch (SecurityException ex) {
			exp = ex;
		} catch (IOException ioex) {
			exp = ioex;
		} catch (IllegalArgumentException iex) {
			exp = iex;
		} catch (InterruptedException intex) {
			exp = intex;
		}

		if (httpdCmd.exitValue() != 0 || exp != null) {
		    error.setErrorString(httpdCmd.getResult("Error"));
		    error.setReturnVal(new Boolean(false));
		    return error;
		}

	    } catch (Exception e) {
		return new ErrorValue(Boolean.FALSE, e.getMessage());
	    }
	    return new ErrorValue(Boolean.TRUE, "");
	}

	/**
	 * This method would change the Listen, Document Root in
	 * a source Apache Configuration file.
	 *
	 */
	private ErrorValue editApacheConfFile(
		String source,
		String destination,
		String listenValue,
		String portValue,
		String documentRoot,
		String pidFileLoc,
		String lockFileLoc) {

	    try {
		// To Read Apache Conf File
		FileReader confFileReader = new FileReader(source);
		BufferedReader fileInput = new BufferedReader(confFileReader);

		// To Write Apache ConfFile
		FileWriter confFileWriter = new FileWriter(destination);
		PrintWriter fileOutput = new PrintWriter(confFileWriter);

		String hostNamesToListen[] = listenValue.split(",");
		String portsToListen[] = portValue.split(",");

		// Read the conf file in the specified directory
		// Replace the required lines in the destination file
		String text;
		boolean listenEdited = false;
		boolean docRootEdited = false;
		boolean pidFileEdited = false;
		boolean lockFileEdited = false;
		while ((text = fileInput.readLine()) != null) {
		    // Match for DocumentRoot
		    if (text.indexOf("DocumentRoot") == 0 ||
			text.indexOf("#DocumentRoot") == 0) {
			if (!docRootEdited) {
			    text = "DocumentRoot"+" \""+documentRoot+"\"";
			    fileOutput.println(text);
			    docRootEdited = true;
			}
		    } else if (text.indexOf("PidFile") == 0 ||
			    text.indexOf("#PidFile") == 0) {
			  if (!pidFileEdited && pidFileLoc != null) {
				text = "PidFile"+" \""+pidFileLoc+"\"";
				fileOutput.println(text);
				pidFileEdited = true;
			  }
		    }
		    // Match for lockFile.This is required for the apache
		    // daemons to create the accept.lock file.
		    else if (text.indexOf("LockFile") == 0 ||
			text.indexOf("#LockFile") == 0) {
			if (!lockFileEdited && lockFileLoc != null) {
			    text = "LockFile"+" \""+lockFileLoc+"\"";
			    fileOutput.println(text);
			    lockFileEdited = true;
			}
		    }
		    // Match for Listen
		    else if (text.indexOf("Listen") == 0 ||
				text.indexOf("#Listen") == 0) {
			if (!listenEdited) {
			    // For every hostname/port pair, add a line
			    // Listen <hostname>:<port>
			    for (int i = 0; i < portsToListen.length; i++) {
				for (int j = 0; j < hostNamesToListen.length;
									j++) {
				    StringBuffer buff = new StringBuffer();
				    buff.append("Listen ");
				    buff.append(hostNamesToListen[j]);
				    buff.append(":");
				    buff.append(portsToListen[i]);
				    text = buff.toString();
				    fileOutput.println(text);
				}
			    }
			    listenEdited = true;
			}
		    }
		    // Write the text as it is
		    else {
			fileOutput.println(text);
		    }
		}
		/*
		 * If PidFile directive is missing in the configuration
		 * file, append PidFile directive to the end of the file
		 * in order to avoid Apache daemons to pick up the default
		 * value of PidFile.
		 */
		 if (!pidFileEdited && pidFileLoc != null) {
			text = "#\n" + "PidFile"+" \""+pidFileLoc+"\""
				+ "\n#\n";
			fileOutput.println(text);
		 }
		/*
		 * If LockFile directive is missing in the configuration
		 * file, append LockFile directive to the end of the file
		 * in order to avoid Apache daemons to pick up the default
		 * value of LockFile.
		 */
		if (!lockFileEdited && lockFileLoc != null) {
			text = "#\n" + "LockFile"+" \""+lockFileLoc+"\""
				+ "\n#\n";
			fileOutput.println(text);
		}

		// Close file
		fileInput.close();
		fileOutput.close();

		// Change permissions to "read-only" by root
		String  cmd[] = { "/usr/bin/chmod",
		"400",
		APACHE_TMP_CONF};
		Exception exp = null;
		ErrorValue error = new ErrorValue();
		Exec httpdCmd = null;
		try {
		    httpdCmd = new Exec(cmd, null);
		    httpdCmd.streamGlobbler(httpdCmd.getInputStream(),
								"Output");
		    httpdCmd.streamGlobbler(httpdCmd.getErrorStream(), "Error");
		    httpdCmd.waitFor();
		    httpdCmd.join();
		} catch (SecurityException ex) {
		    exp = ex;
		} catch (IOException ioex) {
		    exp = ioex;
		} catch (IllegalArgumentException iex) {
		    exp = iex;
		} catch (InterruptedException intex) {
		    exp = intex;
		}

		if (httpdCmd.exitValue() != 0 || exp != null) {
		    error.setErrorString(httpdCmd.getResult("Error"));
		    error.setReturnVal(new Boolean(false));
		    return error;
		}


	    } catch (Exception e) {
		System.out.println(e.getMessage());
		return new ErrorValue(Boolean.FALSE,
			"apache.summary.confFileError");
	    }
	    return new ErrorValue(Boolean.TRUE, "");
	}



        private boolean isFile(String fileName, String zoneExportPath) {

            boolean retVal = false;
            File tmpFile = null;

            if (zoneExportPath == null) {
                tmpFile = new File(fileName);
            } else {
                StringBuffer buffer = new StringBuffer(zoneExportPath);
                buffer.append(fileName);
                tmpFile = new File(buffer.toString());
            }

            if (tmpFile.exists() &&
                    tmpFile.canRead() &&
                    !tmpFile.isDirectory()) {
                retVal = true;
            }

            tmpFile = null;

            return retVal;
        }
}
