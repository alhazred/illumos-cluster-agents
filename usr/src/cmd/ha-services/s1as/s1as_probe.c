/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2002 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * s1as_probe.c - Probe for s1as
 */

#pragma ident	"@(#)s1as_probe.c	1.4	07/06/06 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <rgm/libdsdev.h>
#include "s1as.h"

int
main(int argc, char *argv[])
{
	scds_handle_t	scds_handle;
	scha_err_t 	rc;

	int		timeout;
	int		port, ip, probe_result;
	hrtime_t	ht1, ht2;
	long		dt;

	scds_netaddr_list_t	*netaddr;
	char			*hostname;
	scha_str_array_t	*uris = NULL;
	scha_extprop_value_t	*prop = NULL;


	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR)
		return (1);

	/* Get the ip addresses available for this resource */
	if (scds_get_netaddr_list(scds_handle, &netaddr)) {
		scds_syslog(LOG_ERR,
			"Error retrieving network address resource in "
			"resource group.");
		scds_close(&scds_handle);
		return (1);
	}

	/* Return an error if there are no network resources */
	if (netaddr == NULL || netaddr->num_netaddrs == 0) {
		scds_syslog_debug(DBG_LEVEL_HIGH,
			"No network address resource in resource group.");
		/*
		 * This leaks memory but we don't want to free netaddr
		 * - it would trigger a 3.0 U3 bug.  The amount is
		 * tiny and it's is leaked just this once.
		 */
		netaddr = NULL;
	}

	rc = scds_get_ext_property(scds_handle, "Monitor_Uri_List",
		SCHA_PTYPE_STRINGARRAY, &prop);
	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
			"Error retrieving the extension property %s: %s.",
			"Monitor_Uri_List", scds_error_string(rc));
		return (1);
	}

	if (prop == NULL || prop->val.val_strarray->array_cnt < 1) {
		scds_syslog_debug(DBG_LEVEL_HIGH,
			"No URIs specified in Monitor_Uri_List");
		uris = NULL;
	} else {
		uris = prop->val.val_strarray;
	}

	if (uris == NULL && netaddr == NULL) {
		scds_syslog(LOG_ERR, "Must set at least one of Port_List "
			"or Monitor_Uri_List.");
		return (1);
	}

	/*
	 * Get the timeout from the extension props. This means that
	 * each probe iteration will get a full timeout on each network
	 * resource without chopping up the timeout between all of the
	 * network resources configured for this resource.
	 */
	timeout = scds_get_ext_probe_timeout(scds_handle);

	for (;;) {

		/*
		 * sleep for a duration of thorough_probe_interval between
		 *  successive probes.
		 */
		(void) scds_fm_sleep(scds_handle,
		    scds_get_rs_thorough_probe_interval(scds_handle));

		probe_result = 0;
		ht1 = gethrtime(); /* Latch probe start time */

		/*
		 * Iterate through all the netaddrs calling svc_probe()
		 */
		if (netaddr == NULL)
			goto probe_uris;

		for (ip = 0; ip < netaddr->num_netaddrs; ip++) {
			/*
			 * Grab the hostname and port on which the
			 * health has to be monitored.
			 */
			hostname = netaddr->netaddrs[ip].hostname;
			port = netaddr->netaddrs[ip].port_proto.port;

			scds_syslog_debug(DBG_LEVEL_HIGH,
				"Starting probe (%s:%d)", hostname, port);

			probe_result += svc_probe(scds_handle, hostname, port,
			    timeout);

			scds_syslog_debug(DBG_LEVEL_HIGH,
				"Finished probe (%s:%d success %d)",
				hostname, port, probe_result);

			if (probe_result >= SCDS_PROBE_COMPLETE_FAILURE)
				goto action;

		}	/* Each netaddr */

probe_uris:
		/*
		 * Iterate through all the monitored URIs calling
		 * svc_probe_uri().
		 */
		if (uris == NULL)
			continue;

		for (ip = 0; ip < (int)uris->array_cnt; ip++) {
			char *uri = uris->str_array[ip];

			ht1 = gethrtime(); /* Latch probe start time */

			scds_syslog_debug(DBG_LEVEL_HIGH,
				"Starting probe (%s)", uri);

			probe_result += svc_probe_uri(scds_handle, uri,
				timeout);

			scds_syslog_debug(DBG_LEVEL_HIGH,
				"Finished probe (%s success %d)",
				uri, probe_result);

			if (probe_result >= SCDS_PROBE_COMPLETE_FAILURE)
				goto action;
		}

action:
		ht2 = gethrtime();

		/* Convert to milliseconds */
		dt = (long)((ht2 - ht1) / 1e6);

		/*
		 * Compute failure history and take action if needed
		 */
		(void) scds_fm_action(scds_handle, probe_result, dt);

	} 	/* Keep probing forever */
}
