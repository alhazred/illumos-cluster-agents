/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * s1as.c - Common utilities for s1as
 */

#pragma ident	"@(#)s1as.c	1.12	07/06/06 SMI"

/*
 * This utility has the methods for performing the validation,
 * starting and stopping the data service and the fault monitor. It
 * also contains the method to probe the health of the data service.
 * The probe just returns either success or failure. Action is taken
 * based on this returned value in the method found in the file
 * s1as_probe.c
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <libintl.h>
#include <sys/stat.h>
#include <scha.h>
#include <rgm/libdsdev.h>
#include <dlfcn.h>
#include "s1as.h"
#include "../common/ds_common.h"

/*
 * The initial timeout allowed  for the s1as dataservice to
 * be fully up and running. We will wait for for 10 % (SVC_WAIT_PCT)
 * of the start_timeout time before probing the service.
 *
 * This will with the default setting of 300 seconds for start timeout
 * give us a wait time of 30 seconds, about how long S1AS takes to
 * start up on a E3500.
 */
#define	SVC_WAIT_PCT		3

/*
 * We need to use 95% of probe_timeout to connect to the port and the
 * remaining time is used to disconnect from port in the svc_probe function.
 */
#define	SVC_CONNECT_TIMEOUT_PCT		95

/*
 * Wait for SVC_WAIT_TIME between probes while waiting for service to
 * start.
 */

#define	SVC_WAIT_TIME		5

/*
 * This value will be used as disconnect timeout, if there is no
 * time left from the probe_timeout.
 */

#define	SVC_DISCONNECT_TIMEOUT_SECONDS		2

#define	SVC_SMOOTH_PCT		80

scha_err_t validate_uri(int print,
	scds_net_resource_list_t *snrlp, const char *uri);
int validate_hasp(int print, scds_handle_t handle);
int remove_pidlog(const char *config_filename);

/*
 * svc_validate():
 *
 * Do s1as specific validation of the resource configuration.
 *
 */

int
svc_validate(scds_handle_t scds_handle, boolean_t print_messages)
{
	int	err;
	int	rc = 0;
	int	hasprc;
	uint_t	i;
	int	portlist_empty = 0, urilist_empty = 0;

	scha_str_array_t		*confdirs = NULL;
	scds_net_resource_list_t	*snrlp = NULL;
	scds_port_list_t		*portlist = NULL;
	scha_extprop_value_t		*uri_list = NULL;

	/*
	 * Validate HAStoragePlus resources and determine if the
	 * storage/filesystems are online on this node.
	 *
	 * If they are online validate_hasp() returns 1 in which case
	 * we proceed with the rest of validate.  If they are not
	 * online we skip the rest of the validation, leaving the
	 * validate to the other node(s) that do have access to the
	 * storage.
	 *
	 * validate_hasp() logs and prints appropriate messages for
	 * us.
	 */
	hasprc = validate_hasp(print_messages, scds_handle);

	if (hasprc == -1) {
		rc = 1;		/* error occured */
		goto finished;
	} else if (hasprc == 0) {
		rc = 0;		/* storage OK, not online on this node */
		goto finished;	/* so skip rest of the checks */
	} else if (hasprc == 1) {
		rc = 0;		/* storage OK, online on this node */
	} else {
		ds_internal_error("unexpected return (%d) from validate_hasp",
			hasprc);
		rc = 1;
		goto finished;
	}

	/* Get Confdir_list extension property */
	confdirs = scds_get_ext_confdir_list(scds_handle);

	/* Return an error if there are no config_dirs x prop */
	if (confdirs == NULL || confdirs->array_cnt == 0) {
		scds_syslog(LOG_ERR, "Property %s is not set.",
			"Confdir_list");
		if (print_messages) {
			(void) fprintf(stderr, gettext("Property %s is not "
						"set.\n"), "Confdir_list");
		}
		return (1);
	}

	if (confdirs->array_cnt > 1) {
		scds_syslog(LOG_ERR,
			"Failover %s data services must have exactly "
			"one value for extension property %s.",
			APP_NAME, "Confdir_list");

		if (print_messages) {
			(void) fprintf(stderr, gettext("Failover %s data "
					"services must have exactly "
					"one value for extension "
					"property %s.\n"),
				APP_NAME, "Confdir_list");
		}
		return (1);
	}

	/* Check that Confdir_list[0] is an absolute path. */
	if (confdirs->str_array[0][0] != '/') {
		scds_syslog(LOG_ERR,
			"Confdir_list must be an absolute path.");
		if (print_messages) {
			(void) fprintf(stderr, gettext("Confdir_list must "
					"be an absolute path."));
		}
		return (1);
	}

	/*
	 * Require that Port_list or Monitor_URI_list be set.
	 *
	 * When S1AS is being run as a RGM Scalable resource the
	 * resource will not have a network resource.  In that case
	 * for the probe to have anything to do Monitor_URI_List must
	 * be set.  If the customer does not have an application that
	 * they want monitored they should set the URI list to
	 * something like http://localhost/ .
	 */

	rc = scds_get_port_list(scds_handle, &portlist);
	if (rc != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * An error occured reading the indicated property.
		 * @user_action
		 * Check syslog messages for errors logged from other system
		 * modules. If error persists, please report the problem.
		 */
		scds_syslog(LOG_ERR,
			"Error retrieving the resource property %s: %s.",
			SCHA_PORT_LIST, scds_error_string(rc));
		if (print_messages)
			(void) fprintf(stderr,
				gettext("Error retrieving the resource "
					"property %s: %s."),
				SCHA_PORT_LIST,
				scds_error_string(rc));
		goto finished;
	}

	if (portlist == NULL || portlist->num_ports < 1) {
		portlist_empty++;
		scds_syslog_debug(DBG_LEVEL_HIGH,
		    "Property %s is not set.", SCHA_PORT_LIST);
	}

	rc = scds_get_ext_property(scds_handle, "Monitor_Uri_List",
		SCHA_PTYPE_STRINGARRAY, &uri_list);
	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
			"Error retrieving the extension property %s: %s.",
			"Monitor_Uri_List", scds_error_string(rc));
		if (print_messages)
			(void) fprintf(stderr,
				gettext("Error retrieving the extension "
					"property %s: %s."),
				"Monitor_Uri_List", scds_error_string(rc));
		goto finished;
	}

	if (uri_list == NULL || uri_list->val.val_strarray->array_cnt < 1) {
		scds_syslog_debug(DBG_LEVEL_HIGH,
			"Property Monitor_URI_List is not set");
		urilist_empty++;
	}

	if (portlist_empty && urilist_empty) {
		/*
		 * SCMSGS
		 * @explanation
		 * When creating the resource a Port_List or Monitor_Uri_List
		 * must be specified.
		 * @user_action
		 * Run the resource creation again specifying either a
		 * Port_List or Monitor_Uri_List.
		 */
		scds_syslog(LOG_ERR, "Must set at least one of Port_List "
			"or Monitor_Uri_List.");
		if (print_messages)
			(void) fprintf(stderr,
				gettext("Must set at least one of Port_List "
					"or Monitor_Uri_List must be set."));
		rc = 1;
		goto finished;
	}

	/*
	 * Return an error if there is an error when trying to get the
	 * available network address resources for this resource.
	 *
	 * Don't check to see if any are actually defined since if
	 * S1AS is running in RGM Scalable mode none will be defined.
	 */
	if ((rc = scds_get_rs_hostnames(scds_handle, &snrlp))
		!= SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		    "Error in trying to access the configured network "
		    "resources : %s.", scds_error_string(rc));
		if (print_messages) {
			(void) fprintf(stderr,
				gettext("Error in trying to access the "
					"configured network "
					"resources : %s."),
				scds_error_string(rc));
		}
		goto finished;
	}

	/*
	 * Validate the URIs in Monitor_Uri_List.
	 */
	for (i = 0; i < uri_list->val.val_strarray->array_cnt; i++) {
		char	*uri = uri_list->val.val_strarray->str_array[i];

		rc = validate_uri(print_messages, snrlp, uri);
		if (rc != SCHA_ERR_NOERR) {
			/* Message already logged and printed */
			goto finished;
		}
	}

	/*
	 * Check that the start and stop commands are executable and
	 * readable.
	 */
	err = ds_validate_file(print_messages, S_IXUSR | S_IRUSR,
		"%s/bin/startserv", confdirs->str_array[0]);
	if (err != SCHA_ERR_NOERR) {
		rc = 1;
		goto finished;
	}

	err = ds_validate_file(print_messages, S_IXUSR | S_IRUSR,
		"%s/bin/stopserv", confdirs->str_array[0]);
	if (err != SCHA_ERR_NOERR) {
		rc = 1;
		goto finished;
	}

	/* Check that we can read the init.conf file */

	err = ds_validate_file(print_messages, S_IRUSR,
		"%s/config/init.conf", confdirs->str_array[0]);
	if (err != SCHA_ERR_NOERR) {
		rc = 1;
		goto finished;
	}

	/* All validation checks were successful */

finished:

	/*
	 * We do not free any of the properties allocated because
	 * doing so caused a problem on a previous version of cluster.
	 *
	 * s1as_svc_start is a short lived process and the properties
	 * are not allocated repeatedly so this does not pose a
	 * serious memory leak problem.
	 */

	return (rc);		/* return result of validation */
}

/*
 * svc_start():
 */

int
svc_start(scds_handle_t scds_handle)
{
	int	rc = 0;
	char	*cmd = NULL;
	char	*init_conf = NULL;

	scha_str_array_t	*confdirs = NULL;

	scds_syslog(LOG_NOTICE, "Starting %s.", APP_NAME);

	if ((confdirs = scds_get_ext_confdir_list(scds_handle)) == NULL) {
		rc = 1;
		goto finished;
	}

	/*
	 * Remove the PidLog file if need be.
	 *
	 * Problems with removing the PidLog are logged but otherwise
	 * ignored.
	 */

	init_conf = ds_string_format("%s/config/init.conf",
		confdirs->str_array[0]);
	if (init_conf == NULL)
		ds_internal_error("unable to create path name to init.conf");
	else
		(void) remove_pidlog(init_conf);

	/*
	 * Build the startserv command, then start under PMF.
	 */

	cmd = ds_string_format("%s/bin/startserv",
		confdirs->str_array[0]);
	if (cmd == NULL) {
		rc = 1;
		ds_internal_error("unable to create path name to startserv");
		goto finished;
	}

	rc = scds_pmf_start(scds_handle, SCDS_PMF_TYPE_SVC, 0, cmd, -1);

	if (rc == SCHA_ERR_NOERR) {
		scds_syslog(LOG_NOTICE,
			"Start of %s completed successfully.", cmd);
	} else {
		scds_syslog(LOG_ERR,
			"Failed to start %s.", cmd);
		goto finished;
	}


finished:
	free(cmd);
	return (rc); /* return Success/failure status */
}

/*
 * svc_stop():
 *
 * Stop the s1as server
 * Return 0 on success, > 0 on failures.
 *
 */
int
svc_stop(scds_handle_t scds_handle)
{
	int	rc = 0, cmd_exit_code = 0;
	char	*command = NULL;
	int	stop_smooth_timeout;

	scha_str_array_t	*confdirs = NULL;

	scds_syslog(LOG_NOTICE, "Stopping %s.", APP_NAME);

	stop_smooth_timeout = (scds_get_rs_stop_timeout(scds_handle)
		* SVC_SMOOTH_PCT) / 100;

	confdirs = scds_get_ext_confdir_list(scds_handle);
	if (confdirs == NULL || confdirs->array_cnt < 1) {
		/*
		 * SCMSGS
		 * @explanation
		 * The property has not been set by the user and must be.
		 * @user_action
		 * Reissue the scrgadm command with the required property and
		 * value.
		 */
		scds_syslog(LOG_ERR, "Property %s is not set - %s.",
			"Confdir_list", "Sending SIGKILL now");
		goto send_kill;
	}

	/*
	 * First take the command out of PMF monitoring, so that it
	 * doesn't keep restarting it.
	 */
	rc = scds_pmf_stop_monitoring(scds_handle,
		SCDS_PMF_TYPE_SVC, 0);
	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to take "
			"the resource out of PMF control. "
			"Sending SIGKILL now.");
		goto send_kill;
	}

	/*
	 * Build the stopserv command and run it under a timeout to
	 * attempt a smooth shutdown of the appserver.
	 */
	command = ds_string_format("%s/bin/stopserv", confdirs->str_array[0]);
	if (command == NULL) {
		scds_syslog(LOG_ERR, "Unable to compose %s path. "
			"Sending SIGKILL now.", "stopserv");
		goto send_kill;
	}

	rc = scds_timerun(scds_handle, command, stop_smooth_timeout,
		SIGKILL, &cmd_exit_code);

	if (rc != 0 || cmd_exit_code != 0) {
		scds_syslog(LOG_ERR,
			"The stop command <%s> failed to stop the "
			"application. Will now use SIGKILL to stop the "
			"application.",  command);
	}

	/*
	 * Regardless of whether the command succeeded or not we send
	 * KILL signal to the pmf tag. This will ensure that the
	 * process tree goes away if it still exists. If it doesn't
	 * exist by then, we return NOERR.
	 */

send_kill:
	/*
	 * Since all else failed, send SIGKILL to stop the
	 * application.  Notice that this call will return with
	 * success, even if the tag does not exist by now.
	 *
	 * Timeout of FINAL_STOP_TIMEOUT will wait until PMF succeeds
	 * or we are timed out by RGM.
	 */
	if ((rc = scds_pmf_stop(scds_handle, SCDS_PMF_TYPE_SVC, 0,
			SIGKILL, FINAL_STOP_TIMEOUT)) != SCHA_ERR_NOERR) {
		/*
		 * Failed to stop the application even with SIGKILL,
		 * bail out now.
		 */
		scds_syslog(LOG_ERR,
			"Failed to stop the application with SIGKILL. "
			"Returning with failure from stop method.");
	}

	if (rc == SCHA_ERR_NOERR)
		scds_syslog(LOG_NOTICE,
			"Successfully stopped the application");

	return (rc); /* Successfully stopped */
}

/*
 * svc_wait():
 *
 * wait for the data service to start up fully and make sure it is running
 * healthy
 */

int
svc_wait(scds_handle_t scds_handle)
{
	int rc, svc_start_timeout, probe_timeout;
	scds_netaddr_list_t	*netaddr;
	scds_pmf_status_t	status;
	scha_err_t		err;
	scha_str_array_t	*uris = NULL;
	char			*uri;
	char			*scheme = NULL, *hostname = NULL;
	char			*portstr = NULL, *path = NULL;
	int			port;
	scha_extprop_value_t	*prop;

	/* Get the ip addresses available for this resource */
	if (scds_get_netaddr_list(scds_handle, &netaddr)) {
		/*
		 * SCMSGS
		 * @explanation
		 * An error occured reading the indicated extension property.
		 * @user_action
		 * Check syslog messages for errors logged from other system
		 * modules. If error persists, please report the problem.
		 */
		scds_syslog(LOG_ERR,
			"Error retrieving network address resource in "
			"resource group.");
		return (1);
	}

	/* Return an error if there are no network resources */
	if (netaddr == NULL || netaddr->num_netaddrs == 0) {
		scds_syslog_debug(DBG_LEVEL_HIGH,
			"No network address resource in resource group.");
		/*
		 * This leaks memory but we don't want to free netaddr
		 * because it caused a problem on a previous version
		 * of cluster. The amount is tiny and it is leaked
		 * just this once.
		 */
		netaddr = NULL;
	}

	rc = scds_get_ext_property(scds_handle, "Monitor_Uri_List",
		SCHA_PTYPE_STRINGARRAY, &prop);
	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
			"Error retrieving the extension property %s: %s.",
			"Monitor_Uri_List", scds_error_string(rc));
		return (1);
	}

	if (prop == NULL || prop->val.val_strarray->array_cnt < 1) {
		scds_syslog_debug(DBG_LEVEL_HIGH,
			"No URIs specified in Monitor_Uri_List");
		uris = NULL;
	} else {
		uris = prop->val.val_strarray;
	}

	if (uris == NULL && netaddr == NULL) {
		scds_syslog(LOG_ERR, "Must set at least one of Port_List "
			"or Monitor_Uri_List.");
		return (1);
	}

	/*
	 * Figure out a hostname and port to ping while waiting for
	 * application to startup.
	 */
	if (netaddr != NULL) {
		hostname = netaddr->netaddrs[0].hostname;
		port = netaddr->netaddrs[0].port_proto.port;
	} else {
		uri = uris->str_array[0];
		rc = ds_parse_simple_uri(uri, &scheme, &hostname,
			&portstr, &path);

		if (rc != SCHA_ERR_NOERR) {
			scds_syslog(LOG_ERR, "Error parsing URI: %s (%s)",
				uri, scds_error_string(rc));
			return (1);
		}

		if (hostname == NULL) {
			scds_syslog(LOG_ERR, "Error parsing URI: %s (%s)",
				uri, "unable to determine hostname");
			return (1);
		}

		if (portstr == NULL)
			port = 80;
		else
			port = atoi(portstr);

		if (port < 1) {
			scds_syslog(LOG_ERR, "Port (%d) determined from "
				"Monitor_Uri_List is invalid", port);
			return (1);
		}
	}

	/*
	 * Get the Start method timeout and the Probe timeout value
	 */
	svc_start_timeout = scds_get_rs_start_timeout(scds_handle);
	probe_timeout = scds_get_ext_probe_timeout(scds_handle);

	scds_syslog(LOG_NOTICE, "Waiting for %s to startup", APP_NAME);
	/*
	 * sleep for SVC_WAIT_PCT percentage of start_timeout time
	 * before actually probing the dataservice. This is to allow
	 * the dataservice to be fully up in order to reply to the
	 * probe. NOTE: the value for SVC_WAIT_PCT could be different
	 * for different dataservices.
	 */
	if (scds_svc_wait(scds_handle, (svc_start_timeout * SVC_WAIT_PCT)/100)
		!= SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to start service.");
		return (1);
	}

	do {
		/*
		 * probe the data service on the IP address of the
		 * network resource and the portname
		 */
		rc = svc_probe(scds_handle, hostname, port, probe_timeout);
		if (rc == SCHA_ERR_NOERR) {
			/* Success. Free up resources and return */
			scds_free_netaddr_list(netaddr);
			return (SCHA_ERR_NOERR);
		}

		err = scds_pmf_get_status(scds_handle,
		    SCDS_PMF_TYPE_SVC, SCDS_PMF_SINGLE_INSTANCE, &status);
		if (err != SCHA_ERR_NOERR) {
			scds_syslog(LOG_ERR,
				"Failed to retrieve process monitor "
				"facility tag.");
			return (1);
		}

		/* Check if the dataservice is still up and running */
		if (status != SCDS_PMF_MONITORED) {
			scds_syslog(LOG_ERR,
				"Application failed to stay up. "
				"Start method Failure.");
			return (1);
		}

		/*
		 * Dataservice is still trying to come up. Sleep for a while
		 * before probing again.
		 */
		scds_syslog(LOG_NOTICE, "Waiting for %s to startup",
			APP_NAME);
		if (scds_svc_wait(scds_handle, SVC_WAIT_TIME)
			!= SCHA_ERR_NOERR) {
			scds_syslog(LOG_ERR, "Failed to start service.");
			return (1);
		}

	/* We rely on RGM to timeout and terminate the program */
	} while (1);
}

/*
 * This function starts the fault monitor for a s1as resource.  This
 * is done by starting the probe under PMF. The PMF tag is derived as
 * <RG-name,RS-name,instance_number.mon>. The restart option of PMF is
 * used but not the "infinite restart". Instead interval/retry_time is
 * obtained from the RTR file.
 */

int
mon_start(scds_handle_t scds_handle)
{
	scha_err_t	err;

	scds_syslog_debug(DBG_LEVEL_HIGH,
	    "Calling MONITOR_START method for resource <%s>.",
	    scds_get_resource_name(scds_handle));

	/*
	 * The probe s1as_probe is assumed to be available in the same
	 * subdirectory where the other callback methods for the RT
	 * are installed. The last parameter to scds_pmf_start denotes
	 * the child monitor level. Since we are starting the probe
	 * under PMF we need to monitor the probe process only and
	 * hence we are using a value of 0.
	 */

	err = scds_pmf_start(scds_handle, SCDS_PMF_TYPE_MON,
		SCDS_PMF_SINGLE_INSTANCE, "s1as_probe", 0);

	if (err != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to start fault monitor.");
		return (err);
	}

	scds_syslog(LOG_INFO, "Started the fault monitor.");

	return (SCHA_ERR_NOERR); /* Successfully started Monitor */
}


/*
 * This function stops the fault monitor for a s1as resource.
 * This is done via PMF. The PMF tag for the fault monitor is
 * constructed based on <RG-name_RS-name,instance_number.mon>.
 */

int
mon_stop(scds_handle_t scds_handle)
{
	scha_err_t	err;

	scds_syslog_debug(DBG_LEVEL_HIGH, "Calling scds_pmf_stop method");

	err = scds_pmf_stop(scds_handle, SCDS_PMF_TYPE_MON,
		SCDS_PMF_SINGLE_INSTANCE, SIGKILL,
		FINAL_STOP_TIMEOUT);

	if (err != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to stop fault monitor.");
		return (err);
	}

	scds_syslog(LOG_INFO, "Stopped the fault monitor.");

	return (SCHA_ERR_NOERR); /* Successfully stopped monitor */
}


/*
 * svc_probe(): Do data service specific probing. Return a float value
 * between 0 (success) and 100(complete failure).
 *
 * The probe does a simple socket connection to the s1as server on the
 * specified port which is configured as the resource extension
 * property (Port_list) and pings the dataservice. If the probe fails
 * to connect to the port, we return a value of 100 indicating that
 * there is a total failure. If the connection goes through and the
 * disconnect to the port fails, then a value of 50 is returned
 * indicating a partial failure.
 *
 * Used for simple connect/disconnect probe of a port.  For probing an
 * URI see svc_probe_uri().
 *
 */
int
svc_probe(scds_handle_t scds_handle, char *hostname, int port, time_t timeout)
{
	int	rc = 0;
	int	probe_status = 0;
	ulong_t	t1, t2;
	int	sock;
	int	time_used, time_remaining;
	long	connect_timeout;

	/*
	 * probe the dataservice by doing a socket connection to the
	 * port specified in the port_list property to the host that
	 * is serving the s1as dataservice. If the s1as service which
	 * is configured to listen on the specified port, replies to
	 * the connection, then the probe is successful. Else we will
	 * wait for a time period set in probe_timeout property before
	 * concluding that the probe failed.
	 */

	/*
	 * Use the SVC_CONNECT_TIMEOUT_PCT percentage of timeout
	 * to connect to the port
	 */
	connect_timeout = (SVC_CONNECT_TIMEOUT_PCT * timeout)/100;
	t1 = (ulong_t)(gethrtime()/1E9);

	/*
	 * the probe makes a connection to the specified hostname and port.
	 * The connection is timed for 95% of the actual probe_timeout.
	 */
	rc = scds_fm_tcp_connect(scds_handle, &sock, hostname, port,
		connect_timeout);
	if (rc) {
		scds_syslog(LOG_ERR,
		    "Failed to connect to the host <%s> and port <%d>.",
		    hostname, port);
		/* this is a complete failure */
		probe_status = SCDS_PROBE_COMPLETE_FAILURE;
		goto finished;
	}

	t2 = (ulong_t)(gethrtime()/1E9);

	/*
	 * Compute the actual time it took to connect. This should be less than
	 * or equal to connect_timeout, the time allocated to connect.
	 * If the connect uses all the time that is allocated for it,
	 * then the remaining value from the probe_timeout that is passed to
	 * this function will be used as disconnect timeout. Otherwise, the
	 * the remaining time from the connect call will also be added to
	 * the disconnect timeout.
	 *
	 */

	time_used = (int)(t2 - t1);

	/*
	 * Use the remaining time(timeout - time_took_to_connect) to disconnect
	 */

	time_remaining = timeout - (int)time_used;

	/*
	 * If all the time is used up, use a small hardcoded timeout
	 * to still try to disconnect. This will avoid the fd leak.
	 */
	if (time_remaining <= 0) {
		scds_syslog_debug(DBG_LEVEL_LOW,
		    "svc_probe used entire timeout of "
		    "%d seconds during connect operation and exceeded the "
		    "timeout by %d seconds. Attempting disconnect with timeout"
		    " %d ",
		    connect_timeout,
		    abs(time_used),
		    SVC_DISCONNECT_TIMEOUT_SECONDS);

		time_remaining = SVC_DISCONNECT_TIMEOUT_SECONDS;
	}

	/*
	 * Return partial failure in case of disconnection failure.
	 * Reason: The connect call is successful, which means
	 * the application is alive. A disconnection failure
	 * could happen due to a hung application or heavy load.
	 * If it is the later case, don't declare the application
	 * as dead by returning complete failure. Instead, declare
	 * it as partial failure. If this situation persists, the
	 * disconnect call will fail again and the application will be
	 * restarted.
	 */
	rc = scds_fm_tcp_disconnect(scds_handle, sock, time_remaining);
	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		    "Failed to disconnect from port %d of resource %s.",
		    port, scds_get_resource_name(scds_handle));
		/* this is a partial failure */
		probe_status = SCDS_PROBE_COMPLETE_FAILURE/2;
		goto finished;
	}

	t2 = (ulong_t)(gethrtime()/1E9);
	time_used = (int)(t2 - t1);
	time_remaining = timeout - time_used;

	/*
	 * If there is no time left return
	 * SCDS_PROBE_COMPLETE_FAILURE/2. This will make sure that if
	 * this timeout persists, server will be restarted.
	 */
	if (time_remaining <= 0) {
		scds_syslog(LOG_ERR, "Probe timed out.");
		probe_status = SCDS_PROBE_COMPLETE_FAILURE/2;
		goto finished;
	}

finished:

	return (probe_status);
}

/*
 * svc_probe_uri(): Do data service specific probing. Return a float
 * value between 0 (success) and 100(complete failure).
 *
 * The probe does a HTTP GET to the s1as server with the specified URI
 * which is configured as the resource extension property
 * (Monitor_Uri_List).  We return a value between 0 (for success) and
 * 100 (complete failure).
 *
 */
int
svc_probe_uri(scds_handle_t scds_handle, char *uri, time_t timeout)
{
	int	rc;
	int	status = -1;
	int	level = 0;	/* success level */

	scds_syslog_debug(DBG_LEVEL_HIGH, "Entered svc_probe_uri(%x, %s, %d)",
		scds_handle, uri, timeout);

	rc = ds_http_get_status(scds_handle, uri, &status, timeout, B_TRUE);
	scds_syslog_debug(DBG_LEVEL_HIGH, "svc_probe_uri(): "
		"%s status=%d rc=%d (%s)", uri, status, rc,
		scds_error_string(rc));

	switch (rc) {
	case SCHA_ERR_NOERR:
		/*
		 * Status Code 500 is internal server error.  Other
		 * status codes indicate a problem with the client
		 * (asking for a resource that does not exist, badly
		 * formed request syntax) or the server had an issue
		 * with some other server (504 - Gateway timeout).
		 *
		 * Those other problems should not be held against the
		 * server we are monitoring.
		 *
		 * If the status code is less then 0, that means the
		 * Status Code of the response was malformed.
		 * Consider it a tiny failure.
		 */
		if (status == 500) {
			level = SCDS_PROBE_COMPLETE_FAILURE;
			scds_syslog(LOG_ERR, "HTTP GET Response Code for "
				"probe of %s is %d. "
				"Failover will be in progress",
				uri, status);
		} else if (status < 0) {
			level = SCDS_PROBE_COMPLETE_FAILURE / 4;
			/* error already logged */
		} else
			level = 0;
		break;
	case SCHA_ERR_NOMEM:
	case SCHA_ERR_INVAL:
	case SCHA_ERR_INTERNAL:
		level = 0;
		break;
	case SCHA_ERR_TIMEOUT:
		level = SCDS_PROBE_COMPLETE_FAILURE/2;
		break;
	case SCHA_ERR_STATE:
		/* Connection refused */
		level = SCDS_PROBE_COMPLETE_FAILURE;
		break;
	default:
		level = 0;
		ds_internal_error("unknown error code from "
			"ds_http_get_status: %d (%s)", rc,
			scds_error_string(rc));

		break;
	}

	return (level);
}

scha_err_t
validate_uri(int print, scds_net_resource_list_t *snrlp, const char *uri)
{
	int	rc;
	char	*scheme = NULL, *hostname = NULL;
	char	*port = NULL, *path = NULL;
	int	i, j;

	scds_syslog_debug(DBG_LEVEL_HIGH, "Validating %s", uri);

	rc = ds_parse_simple_uri(uri, &scheme, &hostname, &port, &path);

	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Error parsing URI: %s (%s)", uri,
			scds_error_string(rc));
		if (print)
			(void) fprintf(stderr,
				gettext("Error parsing URI: %s (%s)\n"),
				uri, scds_error_string(rc));
		goto finished;
	}

	if (scheme == NULL) {
		scds_syslog(LOG_ERR, "Error parsing URI: %s (%s)", uri,
			"unable to determine scheme");
		if (print)
			(void) fprintf(stderr,
				gettext("Error parsing URI: %s (%s)\n"),
				uri, gettext("unable to determine scheme"));
		rc = SCHA_ERR_INVAL;
		goto finished;
	}

	if (hostname == NULL) {
		scds_syslog(LOG_ERR, "Error parsing URI: %s (%s)", uri,
			"unable to determine hostname");
		if (print)
			(void) fprintf(stderr,
				gettext("Error parsing URI: %s (%s)\n"),
				uri, gettext("unable to determine hostname"));
		rc = SCHA_ERR_INVAL;
		goto finished;
	}

	if (strcasecmp("http", scheme) != 0) {
		scds_syslog(LOG_ERR, "URI (%s) must be an absolute http URI.",
			uri);
		if (print)
			(void) fprintf(stderr,
				gettext("URI (%s) must be an absolute http "
					"URI\n."), uri);
		rc = SCHA_ERR_INVAL;
		goto finished;
	}

	/*
	 * If configured as RGM Scalable, monitoring happens on
	 * localhost.
	 *
	 * We also allow localhost's IPv4 address, 127.0.0.1.  We
	 * document using "localhost" but this allows a little
	 * leniency.  We could do the checking using
	 * inet_pton(AF_INET, ...)  and inet_pton(AF_INET6, ...) to
	 * make sure we get all IPv4 and IPv6 loopback address text
	 * representations, but it's not worth the complexity.
	 */
	if (strcasecmp(hostname, "localhost") == 0 ||
	    strcmp(hostname, "127.0.0.1") == 0) {
		goto finished;
	}

	/*
	 * Return an error if there are no network address resources.
	 * Do this here because an RGM Scalable resource will not have
	 * any network resources - it uses and is monitored by the
	 * localhost address.
	 */
	if (snrlp == NULL || snrlp->num_netresources == 0) {
		scds_syslog(LOG_ERR,
		    "No network address resource in resource group.");
		if (print) {
			(void) fprintf(stderr,
				gettext("No network address resource in "
					"resource group."));
		}
		rc = SCHA_ERR_INVAL;
		goto finished;
	}

	for (i = 0; i < snrlp->num_netresources; i++) {
		for (j = 0; j < snrlp->netresources[i].num_hostnames; j++) {
			if (strcasecmp(hostname,
			    snrlp->netresources[i].hostnames[j]) == 0) {
				scds_syslog_debug(DBG_LEVEL_HIGH,
					"URI validation: match "
					"on %s [%d][%d]", hostname, i, j);
				goto finished;
			}
		}
	}


	/*
	 * If we get here we did not find a match for the hostname in
	 * any of the network resource hostnames.  Log and if
	 * requested print a message and set rc to SCHA_ERR_INVAL for
	 * the return.
	 */

	scds_syslog(LOG_ERR, "The hostname in %s is not a network address "
		"resource in this resource group.", uri);
	if (print)
		(void) fprintf(stderr, gettext("The hostname in %s is not "
				"a network address resource in this "
				"resource group."), uri);
	rc = SCHA_ERR_INVAL;


finished:
	free(scheme);
	free(hostname);
	free(port);
	free(path);

	return (rc);
}

/*
 * Do HASP validation.
 *
 * Returns
 *	-1	error
 *	0	OK, but storage not online on this node
 *	1	OK, storage online on this node.  Do filesystem checks.
 */
int
validate_hasp(int print, scds_handle_t handle)
{
	int				err, rc = 0;
	scds_hasp_status_t		hasp_status;

	err = scds_hasp_check(handle, &hasp_status);

	if (err != SCHA_ERR_NOERR) {
		/*
		 * scha_hasp_check() logs a message to syslog when it
		 * fails
		 */
		if (print) {
			(void) fprintf(stderr,
				gettext("INTERNAL ERROR: %s.\n"),
				gettext("scds_hasp_check failed"));
		}
		/*
		 * validation has failed for this resource
		 */
		return (-1);
	}

	switch (hasp_status) {

	case SCDS_HASP_NO_RESOURCE:
		/*
		 * We do not depend on any SUNW.HAStoragePlus resources
		 */
		scds_syslog(LOG_INFO,
			"This resource does not depend on any "
			"SUNW.HAStoragePlus resources. Proceeding with "
			"normal checks.");
		rc = 1;
		break;

	case SCDS_HASP_ERR_CONFIG:
		/*
		 * Configuration error, SUNW.HAStoragePlus resource is
		 * in a different RG. Fail the validation.
		 */
		scds_syslog(LOG_ERR,
			"One or more of the SUNW.HAStoragePlus resources "
			"that this resource depends on is in a different "
			"resource group. Failing validate method "
			"configuration checks.");
		if (print) {
			(void) fprintf(stderr,
				gettext("One or more of the "
					"SUNW.HAStoragePlus resources that "
					"this resource depends on is in a "
					"different resource "
					"group. Failing validate method "
					"configuration checks."));
		}
		rc = -1;
		break;

	case SCDS_HASP_NOT_ONLINE:
		/*
		 * There is at least one SUNW.HAStoragePlus resource not
		 * online anywhere.
		 */
		scds_syslog(LOG_ERR,
			"One or more of the SUNW.HAStoragePlus resources "
			"that this resource depends on is not online "
			"anywhere. Failing validate method.");
		if (print) {
			(void) fprintf(stderr,
				gettext("One or more of the "
					"SUNW.HAStoragePlus resources that "
					"this resource depends on is not "
					"online anywhere. "
					"Failing validate method."));
		}
		rc = -1;
		break;

	case SCDS_HASP_ONLINE_NOT_LOCAL:
		/*
		 * Not all SUNW.HAStoragePlus we need, are online locally.
		 */
		scds_syslog(LOG_INFO,
			"All the SUNW.HAStoragePlus resources that this "
			"resource depends on are not online on the local "
			"node. Skipping the checks for the existence "
			"and permissions of the start/stop/probe commands.");
		rc = 0;
		break;

	case SCDS_HASP_ONLINE_LOCAL:
		/*
		 * All SUNW.HAStoragePlus resources we need are available on
		 * this node.
		 */
		scds_syslog(LOG_INFO,
			"All the SUNW.HAStoragePlus resources that this "
			"resource depends on are online on the local node. "
			"Proceeding with the checks for the existence and "
			"permissions of the start/stop/probe commands.");
		rc = 1;
		break;

	default:
		/* Unknown status code */
		scds_syslog(LOG_ERR, "Unknown status code %d.", hasp_status);
		if (print) {
			(void) fprintf(stderr,
				gettext("Unknown status code %d."),
				hasp_status);
		}
		/*
		 * Since the scds_hasp_check() returned an unknown
		 * status code, we return 0  which is "OK, but not
		 * online".
		 */
		rc = 0;
		break;
	}

	return (rc);
}

/*
 * remove_pidlog()
 *
 * Remove the PidLog file as specified in config_filename.
 *
 * Returns 1 on error, otherwise 0.
 */
int
remove_pidlog(const char *config_filename)
{
	char	pidfile[MAXPATHLEN+1];

	if (ds_get_magnus_value(config_filename, "PidLog", pidfile) == 0) {

		/*
		 * If the return is non zero there was some issue,
		 * either there was no PidLog entry or the PidLog
		 * value was larger then MAXPATHLEN.
		 *
		 * In either case we don't want to unlink whatever was
		 * returned in pidfile.  Just skip and let the app
		 * server sort it out.
		 */

		if (unlink(pidfile) == 0) {
			scds_syslog_debug(DBG_LEVEL_HIGH,
				"Removed PidLog at %s\n", pidfile);
		} else {
			/*
			 * It's not an error if the PidLog is not
			 * there.
			 */
			/* No prototype for ___errno() */
			if (errno != ENOENT) { /*lint !e746 */
				/*
				 * SCMSGS
				 * @explanation
				 * The resource was not able to remove the
				 * application's PidLog before starting it.
				 * @user_action
				 * Check that PidLog is set correctly and that
				 * the PidLog file is accessible. If needed
				 * delete the PidLog file manually and start
				 * the the resource group.
				 */
				scds_syslog(LOG_ERR,
					"Error deleting PidLog <%s> "
					"(%s) for service with "
					"config file <%s>.",
					pidfile, strerror(errno),
					config_filename);
				return (1);
			}
		}
	}

	return (0);
}
