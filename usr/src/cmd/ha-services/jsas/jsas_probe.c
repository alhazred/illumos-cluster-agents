/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * jsas_probe.c - Probe for s1as
 */

#pragma ident	"@(#)jsas_probe.c	1.8	07/06/06 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <rgm/libdsdev.h>
#include "jsas.h"

int
main(int argc, char *argv[])
{
	scds_handle_t	scds_handle;
	scha_err_t 	rc, err;

	int		timeout;
	int		ip, probe_result;
	hrtime_t	ht1, ht2;
	long		dt;
	static char	*rgname = NULL;
	static char	*rsname = NULL;

	scha_str_array_t	*uris = NULL;
	scha_extprop_value_t	*prop = NULL;
	scds_pmf_status_t	status;

	as_extn_props_t		asextnprops;

	scds_syslog_debug(DBG_LEVEL_HIGH,
				"In jsas_probe main");


	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR)
		return (1);

	rc = scds_get_ext_property(scds_handle, "Monitor_Uri_List",
		SCHA_PTYPE_STRINGARRAY, &prop);

	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
			"Error retrieving the extension property %s: %s.",
			"Monitor_Uri_List", scds_error_string(rc));
		return (1);
	}

	if (prop == NULL || prop->val.val_strarray->array_cnt < 1) {
		scds_syslog_debug(DBG_LEVEL_HIGH,
			"No URIs specified in Monitor_Uri_List");
		scds_syslog(LOG_INFO, "<Monitor_Uri_List> extension property "
			"is not set. Fault Monitor will not do HTTP probing. "
			"asadmin list-domains command will be used for "
			"probing the health of the server");
		uris = NULL;
	} else {
		uris = prop->val.val_strarray;
	}

	/* Get the App Server extension properties */

	if (get_as_extn_props(scds_handle, &asextnprops, B_FALSE) != 0) {
		/* Error message already logged */
		return (1);
	}

	/*
	 * Get the timeout from the extension props. This means that
	 * each probe iteration will get a full timeout on each network
	 * resource without chopping up the timeout between all of the
	 * network resources configured for this resource.
	 */
	timeout = scds_get_ext_probe_timeout(scds_handle);

	for (;;) {

		/*
		 * sleep for a duration of thorough_probe_interval between
		 *  successive probes.
		 */
		(void) scds_fm_sleep(scds_handle,
		    scds_get_rs_thorough_probe_interval(scds_handle));


		if (uris != NULL) {
			goto probe_uris;
		} else {
			probe_result = 0;
			ht1 = gethrtime(); /* Latch probe start time */

			rc = get_das_status(scds_handle, timeout,
						&asextnprops);

			if (rc == 1) {
				scds_syslog_debug(DBG_LEVEL_HIGH,
					"get_das_status failed and "
					"returned %d", rc);
				/*
				 * SCMSGS
				 * @explanation
				 * The probe method checked the status of DAS
				 * by running the asadmin list-domains
				 * command. The command lists the status of
				 * the DAS as "not running". This is
				 * considered complete failure and the probe
				 * will take action.
				 * @user_action
				 * None
				 */
				scds_syslog(LOG_ERR,
					"asadmin list-domains command "
					"lists the status of %s domain "
					"as not running ",
					asextnprops.domain_name);
				probe_result = SCDS_PROBE_COMPLETE_FAILURE;
				goto action;
			}

			if (rc == 2) {
				/*
				 * Out of Memory error.
				 * Message already logged.
				 */

				probe_result = SCDS_PROBE_COMPLETE_FAILURE;
				goto action;
			}

			/*
			 * CR 6401735.When list-domains does not return
			 * "running" status, probe was restarting DAS
			 * even though domain process was up.
			 * In order to handle this we check for
			 * "requires restart" message from list-domains
			 * in jsas_utils.sh script and return 3.
			 * Based on the return value, we do not restart
			 * the domain but inform the user that domain
			 * requires restart by syslog message.
			 */
			if (rc == 3) {
				probe_result = 0;
				/*
				 * SCMSGS
				 * @explanation
				 * Domain Admin Server resource requires
				 * a re-start by cluster admin. DAS agent
				 * probe will not take any action.
				 * @user_action
				 * Restart the Domain Admin Server
				 * resource.
				 */
				scds_syslog(LOG_NOTICE,
					"Domain Admin Server resource "
					"needs a re-start by an admin."
					"The Agent probe will not take "
					"any action");
				goto action;
			}
			scds_syslog_debug(DBG_LEVEL_HIGH,
				"get_das_status says DAS %s is running",
				asextnprops.domain_name);

			err = scds_pmf_get_status(scds_handle,
				SCDS_PMF_TYPE_SVC, SCDS_PMF_SINGLE_INSTANCE,
				&status);
			if (err != SCHA_ERR_NOERR) {
				scds_syslog(LOG_ERR,
					"Failed to retrieve process monitor "
					"facility tag.");
				probe_result = SCDS_PROBE_COMPLETE_FAILURE;
				goto action;
			}
			/* Check if the dataservice is still up and running */
			if (status != SCDS_PMF_MONITORED) {
				scds_syslog(LOG_ERR,
					"Application failed to stay up. "
					"Start method Failure.");
				probe_result = SCDS_PROBE_COMPLETE_FAILURE;
			}
			goto action;

		} /* Just PMF Tag monitoring */

probe_uris:
		/*
		 * Iterate through all the monitored URIs calling
		 * svc_probe_uri().
		 */
			probe_result = 0;
			ht1 = gethrtime(); /* Latch probe start time */

		for (ip = 0; ip < (int)uris->array_cnt; ip++) {
			char *uri = uris->str_array[ip];

			scds_syslog_debug(DBG_LEVEL_HIGH,
				"Starting probe on (%s)", uri);

			probe_result += svc_probe_uri(scds_handle, uri,
				timeout);

			scds_syslog_debug(DBG_LEVEL_HIGH,
				"Finished probe on %s which returned "
				"probe_result=%d",
				uri, probe_result);

			if (probe_result >= SCDS_PROBE_COMPLETE_FAILURE)
				goto action;
		}

action:
		ht2 = gethrtime();

		/* Convert to milliseconds */
		dt = (long)((ht2 - ht1) / 1e6);

		/*
		 * Compute failure history and take action if needed
		 */
		(void) scds_fm_action(scds_handle, probe_result, dt);

	} 	/* Keep probing forever */
}
