/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * jsas.c - Common utilities for jsas
 */

#pragma ident	"@(#)jsas.c	1.9	09/04/14 SMI"

/*
 * This utility has the methods for performing the validation,
 * starting and stopping the data service and the fault monitor. It
 * also contains the method to probe the health of the data service.
 * The probe just returns either success or failure. Action is taken
 * based on this returned value in the method found in the file
 * jsas_probe.c
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <unistd.h>
#include <ctype.h>
#include <libintl.h>
#include <sys/stat.h>
#include <sys/statvfs.h>
#include <scha.h>
#include <rgm/libdsdev.h>
#include <dlfcn.h>
#include "jsas.h"
#include "../common/ds_common.h"

/*
 * The initial timeout allowed  for the jsas dataservice to
 * be fully up and running. We will wait for 10 % (SVC_WAIT_PCT)
 * of the start_timeout time before probing the service.
 *
 * This will with the default setting of 300 seconds for start timeout
 * give us a wait time of 30 seconds, about how long S1AS takes to
 * start up on a E3500.
 */
#define	SVC_WAIT_PCT		3

/*
 * We need to use 95% of probe_timeout to connect to the port and the
 * remaining time is used to disconnect from port in the svc_probe function.
 */
#define	SVC_CONNECT_TIMEOUT_PCT		95

/*
 * Wait for SVC_WAIT_TIME between probes while waiting for service to
 * start.
 */

#define	SVC_WAIT_TIME		8

/*
 * This value will be used as disconnect timeout, if there is no
 * time left from the probe_timeout.
 */

#define	SVC_DISCONNECT_TIMEOUT_SECONDS		2

#define	SVC_SMOOTH_PCT		80

#define	ASADMIN_COMMAND_PCT		10
boolean_t pre_glassfish;

scha_err_t validate_uri(int print,
	scds_net_resource_list_t *snrlp, const char *uri);
int validate_hasp(int print, scds_handle_t handle);

/*
 * svc_validate():
 *
 * Do jsas specific validation of the resource configuration.
 *
 */

int
svc_validate(scds_handle_t scds_handle, as_extn_props_t *asextnprops,
		boolean_t print_messages)
{
	int	err, j, k;
	int	rc = 0;
	int	hasprc;
	uint_t	i;
	int	urilist_empty = 0;
	char 	*file;
	char    *rgname = NULL;
	char    asadmin_bin[SCDS_ARRAY_SIZE];

	scds_net_resource_list_t	*snrlp = NULL;
	scds_net_resource_list_t	*snrlp_rg = NULL;
	scha_extprop_value_t		*uri_list = NULL;
	struct statvfs			vfsbuf;
	struct stat			statbuf;

	/*
	 * Validate HAStoragePlus resources and determine if the
	 * storage/filesystems are online on this node.
	 *
	 * If they are online validate_hasp() returns 1 in which case
	 * we proceed with the rest of validate.  If they are not
	 * online we skip the rest of the validation, leaving the
	 * validate to the other node(s) that do have access to the
	 * storage.
	 *
	 * validate_hasp() logs and prints appropriate messages for
	 * us.
	 */
	hasprc = validate_hasp(print_messages, scds_handle);

	if (hasprc == -1) {
		rc = 1;		/* error occured */
		goto finished;
	} else if (hasprc == 0) {
		rc = 0;		/* storage OK, not online on this node */
		goto finished;	/* so skip rest of the checks */
	} else if (hasprc == 1) {
		rc = 0;		/* storage OK, online on this node */
	} else {
		ds_internal_error("unexpected return (%d) from validate_hasp",
			hasprc);
		rc = 1;
		goto finished;
	}

	/*
	 * Return an error if there is an error when trying to get the
	 * available network address resources for this resource
	 * and if no Failover IP resources are created in this RG.
	 */
	if ((rc = scds_get_rs_hostnames(scds_handle, &snrlp))
		!= SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		    "Error in trying to access the configured network "
		    "resources : %s.", scds_error_string(rc));
		if (print_messages) {
			(void) fprintf(stderr,
				gettext("Error in trying to access the "
					"configured network "
					"resources : %s."),
				scds_error_string(rc));
		}
		rc = 1;
		goto finished;
	}

	/*
	 * If there are no Failover IP resources in the RG then
	 * it is an error in the configuration. Since DAS is a
	 * Failover Data Service there should be atleast one
	 * Failover IP in this RG.
	 */

	if (snrlp->num_netresources <= 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * Since DAS resource is a Failover Resource, the Failover IP
		 * address that DAS uses must be created in this Resource
		 * Group.
		 * @user_action
		 * Create the Failover IP address resource in this Resource
		 * Group.
		 */
		scds_syslog(LOG_ERR,
			"There should be at least one Network Resource "
			"in this resource group");
		if (print_messages) {
			(void) fprintf(stderr,
				gettext("There should be atleast "
				    "one Network Resource in this Resource "
				    "Group"));
		}
		rc = 1;
		goto finished;
	}

		/*
		 * Validate if the failover IP used by the resource exists
		 * in the resource group. If not, return an error.By Iterating
		 * through number of network resources in the resource group
		 * and number of hostnames in the resource, comparison is
		 * against the network_resources_used property.If network_
		 * resources_used property is not set, the iteration is done
		 * for all the resources present in the resource group.
		 */
		rgname = (char *)scds_get_resource_group_name(scds_handle);
		rc = scds_get_rg_hostnames(rgname, &snrlp_rg);

		for (j = 0; j < snrlp->num_netresources; j++) {
			for (k = 0; k < snrlp_rg->num_netresources; k++) {
				rc = 1;
				if (strcmp(snrlp->netresources[j].name,
				snrlp_rg->netresources[k].name) == 0) {
					rc = 0;
					break;
				}
			}
			if (rc) break;
		}

		if (rc) {
		/*
		 * SCMSGS
		 * @explanation
		 * Failover IP address entered in Network_resources_used
		 * property is not part of this resource group.
		 * @user_action
		 * Check the failover IP resource and enter the value.
		 */
			scds_syslog(LOG_ERR,
				" Failover IP resource does not exist"
				" in the resource group %s", rgname);
			if (print_messages) {
				(void) fprintf(stderr,
				gettext("Failover IP resource "
					"does not exist in the "
					"resource group %s\n"),
					rgname);
			}
			rc = 1;
			goto finished;
		}

	/* Get the Monitor_Uri_List extension property */

	rc = scds_get_ext_property(scds_handle, "Monitor_Uri_List",
		SCHA_PTYPE_STRINGARRAY, &uri_list);
	if (rc != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * An error occured reading the indicated extension property.
		 * @user_action
		 * Check syslog messages for errors logged from other system
		 * modules. If error persists, please report the problem.
		 */
		scds_syslog(LOG_ERR,
			"Error retrieving the extension property %s: %s.",
			"Monitor_Uri_List", scds_error_string(rc));
		if (print_messages)
			(void) fprintf(stderr,
				gettext("Error retrieving the extension "
					"property %s: %s."),
				"Monitor_Uri_List", scds_error_string(rc));
		goto finished;
	}

	if (uri_list == NULL || uri_list->val.val_strarray->array_cnt < 1) {
		scds_syslog_debug(DBG_LEVEL_HIGH,
			"Property Monitor_URI_List is not set");
		urilist_empty++;
	}

	if (urilist_empty) {
		/*
		 * SCMSGS
		 * @explanation
		 * This is just an info message. If the Monitor_uri_list
		 * extension property is set, the JSAS Domain Admin Server
		 * probe will do thorough probing by sending an HTTP request
		 * to DAS and then reading the response. If this extension
		 * property is not set, the asadmin list-domains command will
		 * be used to get the status of DAS.
		 * @user_action
		 * If HTTP probing is desired, create a HTTP listener for the
		 * DAS and then set the Monitor_uri_list extension property.
		 */
		scds_syslog(LOG_INFO, "<Monitor_Uri_List> extension property "
			"is not set. Fault Monitor will not do HTTP probing. "
			"asadmin list-domains command will be used for "
			"probing the health of the server");
	} else {

		/*
		 * Validate the URIs in Monitor_Uri_List.
		 */
		for (i = 0; i < uri_list->val.val_strarray->array_cnt; i++) {
			char	*uri = uri_list->val.val_strarray->str_array[i];

			rc = validate_uri(print_messages, snrlp, uri);
			if (rc != SCHA_ERR_NOERR) {
				/* Message already logged and printed */
				goto finished;
			}
			scds_syslog_debug(DBG_LEVEL_HIGH,
				"%s URI is validated", uri);
		}
	}

	/*
	 * Validate the Install directory
	 */

	err = ds_validate_file(print_messages, S_IFDIR,
			asextnprops->install_dir);
	if (err != SCHA_ERR_NOERR) {
		rc = 1;
		goto finished; /* Message logged in ds_validate_file */
	}

	/*
	 * Check and Make sure the asadmin command is executable and
	 * readable.
	 */

	pre_glassfish = B_TRUE;
	(void) sprintf(asadmin_bin, "%s/appserver/bin/asadmin",
		asextnprops->install_dir);
		/*
		 * Check to see if the version is pre-9.1. If stat fails,
		 * it could be application server 9.1. Else it could be
		 * Application server 8.*.
		 */
		if (stat(asadmin_bin, &statbuf) != 0) {
			pre_glassfish = B_FALSE;
			(void) sprintf(asadmin_bin, "%s/bin/asadmin",
			asextnprops->install_dir);
		}
	err = ds_validate_file(print_messages, S_IXUSR | S_IRUSR,
		asadmin_bin);
	if (err != SCHA_ERR_NOERR) {
		rc = 1;
		goto finished;
	}

	/*
	 * Check and make sure the domain.xml file is available in the
	 * domain directory.
	 */
	file = ds_string_format("%s/%s/config/domain.xml",
			asextnprops->domain_dir, asextnprops->domain_name);
	if (file == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * An internal error has occurred.
		 * @user_action
		 * Save a copy of the /var/adm/messages files on all nodes.
		 * Contact your authorized Sun service provider for assistance
		 * in diagnosing the problem.
		 */
		scds_syslog(LOG_ERR,
			"INTERNAL ERROR: "
			"Failed to create the path to the %s file.",
			"domain.xml");
		if (print_messages) {
			(void) fprintf(stderr, gettext("INTERNAL ERROR: "
			"Failed to create the path to the %s file.\n"),
			"domain.xml");
		}
		return (1);
	}

	if (statvfs(file, &vfsbuf) != 0) {
		scds_syslog(LOG_ERR,
			"Cannot access file: %s (%s)",
			file, strerror(errno)); /*lint !e746 */
		if (print_messages) {
			(void) fprintf(stderr,
				gettext("Cannot access file: %s (%s)\n"),
				file, strerror(errno));
		}
		free(file);
		return (1);
	}
	scds_syslog_debug(DBG_LEVEL_HIGH,
		"%s is validated", file);

	free(file);

	/* Validate the admin password file */
	err = ds_validate_file(print_messages, S_IFREG | S_IXUSR |
		S_IRUSR, asextnprops->password_file);
	if (err != SCHA_ERR_NOERR) {
		rc = 1;
		goto finished;
	}

	scds_syslog_debug(DBG_LEVEL_HIGH,
		"%s is validated", asextnprops->password_file);

	/* All validation checks were successful */

finished:

	/*
	 * We do not free any of the properties allocated  because
	 * doing so caused a problem on a previous version of cluster.
	 *
	 * jsas_svc_start is a short lived process and the properties
	 * are not allocated repeatedly so this does not pose a
	 * serious memory leak problem.
	 */

	return (rc);		/* return result of validation */
}

/*
 * svc_start():
 */

int
svc_start(scds_handle_t scds_handle, as_extn_props_t *asextnprops)
{
	int	rc = 0;
	char	*cmd = NULL;

	/*
	 * Build the DAS start command, then start under PMF.
	 */

	if (pre_glassfish) {
		cmd = ds_string_format("%s/appserver/bin/asadmin start-domain "
			"--domaindir %s --user %s --passwordfile %s %s",
			asextnprops->install_dir,
			asextnprops->domain_dir,
			asextnprops->admin_user,
			asextnprops->password_file,
			asextnprops->domain_name);
	} else {
		cmd = ds_string_format("%s/bin/asadmin start-domain "
			"--domaindir %s --user %s --passwordfile %s %s",
			asextnprops->install_dir,
			asextnprops->domain_dir,
			asextnprops->admin_user,
			asextnprops->password_file,
			asextnprops->domain_name);
	}

	if (cmd == NULL) {
		rc = 1;
		ds_internal_error("unable to create the start command");
		goto finished;
	}
	scds_syslog_debug(DBG_LEVEL_HIGH, "The command used to start "
			"JSAS is %s", cmd);

	rc = scds_pmf_start(scds_handle, SCDS_PMF_TYPE_SVC, 0, cmd, -1);

	if (rc == SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * This is just an info message indicating that the Sun
		 * Cluster PMF is now starting the Domain Admin Server.
		 * @user_action
		 * None
		 */
		scds_syslog(LOG_NOTICE,
			"Starting the JSAS Domian Admin Server "
			"under pmf");
	} else {
		scds_syslog(LOG_ERR,
			"Failed to start %s.", cmd);
		goto finished;
	}


finished:
	free(cmd);
	return (rc); /* return Success/failure status */
}

/*
 * svc_stop():
 *
 * Stop the jsas server
 * Return 0 on success, > 0 on failures.
 *
 */
int
svc_stop(scds_handle_t scds_handle, as_extn_props_t *asextnprops)
{
	int	rc = 0, cmd_exit_code = 0;
	char	*command = NULL;
	int	stop_smooth_timeout;
	char    asadmin_bin[SCDS_ARRAY_SIZE];
	struct stat		statbuf;

	scds_syslog(LOG_NOTICE, "Stopping %s.", APP_NAME);

	stop_smooth_timeout = (scds_get_rs_stop_timeout(scds_handle)
		* SVC_SMOOTH_PCT) / 100;

	/*
	 * First take the command out of PMF monitoring, so that it
	 * doesn't keep restarting it.
	 */
	rc = scds_pmf_stop_monitoring(scds_handle,
		SCDS_PMF_TYPE_SVC, 0);
	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to take "
			"the resource out of PMF control. "
			"Sending SIGKILL now.");
		goto send_kill;
	}

	/*
	 * Build the DAS stop command and run it under a timeout to
	 * attempt a smooth shutdown of the appserver.
	 */
	pre_glassfish = B_TRUE;
	(void) sprintf(asadmin_bin, "%s/appserver/bin/asadmin",
		asextnprops->install_dir);
		if (stat(asadmin_bin, &statbuf) != 0) {
			pre_glassfish = B_FALSE;
			(void) sprintf(asadmin_bin, "%s/bin/asadmin",
			asextnprops->install_dir);
		}

	if (pre_glassfish) {
		command = ds_string_format("%s/appserver/bin/asadmin "
			"stop-domain --domaindir %s %s > /dev/null 2>&1",
			asextnprops->install_dir,
			asextnprops->domain_dir,
			asextnprops->domain_name);
	} else {
		command = ds_string_format("%s/bin/asadmin stop-domain "
			"--domaindir %s %s > /dev/null 2>&1",
			asextnprops->install_dir,
			asextnprops->domain_dir,
			asextnprops->domain_name);
	}
	if (command == NULL) {
		scds_syslog(LOG_ERR, "Unable to compose %s path. "
			"Sending SIGKILL now.", "asadmin stop-domain");
		goto send_kill;
	}

	scds_syslog_debug(DBG_LEVEL_HIGH,
		"%s command will be used to stop the App Server", command);

	rc = scds_timerun(scds_handle, command, stop_smooth_timeout,
		SIGKILL, &cmd_exit_code);

	if (rc != 0 || cmd_exit_code != 0) {
		scds_syslog(LOG_NOTICE,
			"The stop command <%s> failed to stop the "
			"application. Will now use SIGKILL to stop the "
			"application.",  command);
	}

	/*
	 * Regardless of whether the command succeeded or not we send
	 * KILL signal to the pmf tag. This will ensure that the
	 * process tree goes away if it still exists. If it doesn't
	 * exist by then, we return NOERR.
	 */

send_kill:
	/*
	 * Since all else failed, send SIGKILL to stop the
	 * application.  Notice that this call will return with
	 * success, even if the tag does not exist by now.
	 *
	 * Timeout of FINAL_STOP_TIMEOUT will wait until PMF succeeds
	 * or we are timed out by RGM.
	 */
	if ((rc = scds_pmf_stop(scds_handle, SCDS_PMF_TYPE_SVC, 0,
			SIGKILL, FINAL_STOP_TIMEOUT)) != SCHA_ERR_NOERR) {
		/*
		 * Failed to stop the application even with SIGKILL,
		 * bail out now.
		 */
		scds_syslog(LOG_ERR,
			"Failed to stop the application with SIGKILL. "
			"Returning with failure from stop method.");
	}

	if (rc == SCHA_ERR_NOERR)
		scds_syslog(LOG_NOTICE,
			"Successfully stopped the application");

	return (rc); /* Successfully stopped */
}

/*
 * svc_wait():
 *
 * wait for the data service to start up fully and make sure it is running
 * healthy
 */

int
svc_wait(scds_handle_t scds_handle, as_extn_props_t *asextnprops)
{
	int rc, svc_start_timeout, probe_timeout;
	int			asadmin_command_timeout;
	scds_pmf_status_t	status;
	scha_err_t		err;
	scha_str_array_t	*uris = NULL;
	char			*uri;
	char			*scheme = NULL, *hostname = NULL;
	char			*portstr = NULL, *path = NULL;
	int			port;
	scha_extprop_value_t	*prop;

	scds_syslog_debug(DBG_LEVEL_HIGH, "In svc_wait()");
	/*
	 * Get the Start method timeout and the Probe timeout value
	 */
	svc_start_timeout = scds_get_rs_start_timeout(scds_handle);
	probe_timeout = scds_get_ext_probe_timeout(scds_handle);

	asadmin_command_timeout = (svc_start_timeout * ASADMIN_COMMAND_PCT)/100;

	scds_syslog_debug(DBG_LEVEL_HIGH, "The value of "
		"asadmin_command_timeout is %d", asadmin_command_timeout);

	rc = scds_get_ext_property(scds_handle, "Monitor_Uri_List",
		SCHA_PTYPE_STRINGARRAY, &prop);
	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
			"Error retrieving the extension property %s: %s.",
			"Monitor_Uri_List", scds_error_string(rc));
		return (1);
	}

	if (prop == NULL || prop->val.val_strarray->array_cnt < 1) {
		scds_syslog_debug(DBG_LEVEL_HIGH,
			"No URIs specified in Monitor_Uri_List");
		uris = NULL;
		scds_syslog(LOG_INFO, "<Monitor_Uri_List> extension property "
			"is not set. Fault Monitor will not do HTTP probing. "
			"asadmin list-domains command will be used for probing "
			"the health of the server");
	} else {
		uris = prop->val.val_strarray;
	}

	if (uris == NULL) {
		scds_syslog_debug(DBG_LEVEL_HIGH, "In svc_wait() inside "
			"the while loop checking for DAS running status");
		do {

			/* Get the DAS status. Check if DAS is running */

			rc = get_das_status(scds_handle,
					asadmin_command_timeout,
					asextnprops);
				scds_syslog_debug(DBG_LEVEL_HIGH,
					"get_das_status returned %d", rc);
				if (rc == 2) {
					/*
					 * Out of memory.
					 * Message already logged.
					 */
					return (1);
				}
				if (rc == 0) {
					scds_syslog_debug(DBG_LEVEL_HIGH,
						"get_das_status says DAS "
						"is running");
					return (0); /* DAS is running */
				}

			err = scds_pmf_get_status(scds_handle,
			SCDS_PMF_TYPE_SVC, SCDS_PMF_SINGLE_INSTANCE, &status);
			if (err != SCHA_ERR_NOERR) {
				scds_syslog(LOG_ERR,
					"Failed to retrieve process monitor "
					"facility tag.");
				return (1);
			}

			/* Check if the dataservice is still up and running */
			if (status != SCDS_PMF_MONITORED) {
				/*
				 * SCMSGS
				 * @explanation
				 * The Application failed to stay start up.
				 * @user_action
				 * Look in /var/adm/messages for the cause of
				 * failure. Try to start the Weblogic Server
				 * manually and check if it can be started
				 * manually. Make sure the logical host is UP
				 * before starting the DAS manually. If it
				 * fails to start manually then check your
				 * configuration and make sure it can be
				 * started manually before it can be started
				 * by the agent. Make sure the extension
				 * properties are correct. Make sure the port
				 * is not already in use. Save a copy of the
				 * /var/adm/messages files on all nodes.
				 * Contact your authorized Sun service
				 * provider for assistance in diagnosing the
				 * problem.
				 */
				scds_syslog(LOG_ERR,
					"Application failed to stay up. "
					"Start method Failure.");
				return (1);
			}


			/*
			 * Dataservice is still trying to come up.
			 * Sleep for a while before probing again.
			 */
			scds_syslog(LOG_NOTICE, "Waiting for %s to startup",
				APP_NAME);
			if (scds_svc_wait(scds_handle, SVC_WAIT_TIME)
				!= SCHA_ERR_NOERR) {
				scds_syslog(LOG_ERR,
					"Failed to start service.");
				return (1);
			}

		} while (1);
	}

	/*
	 * If we are here the the Monitor_uri_list is set. So
	 * do the detailed probing.
	 */

	/*
	 * Get the  hostname and port to ping while waiting for
	 * application to startup.
	 */
	uri = uris->str_array[0];
	rc = ds_parse_simple_uri(uri, &scheme, &hostname,
		&portstr, &path);

	if (rc != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * There was an error parsing the URI for the reason given.
		 * @user_action
		 * Fix the syntax of the URI.
		 */
		scds_syslog(LOG_ERR, "Error parsing URI: %s (%s)",
			uri, scds_error_string(rc));
		return (1);
	}

	if (hostname == NULL) {
		scds_syslog(LOG_ERR, "Error parsing URI: %s (%s)",
			uri, "unable to determine hostname");
		return (1);
	}

	if (portstr == NULL)
		port = 80;
	else
		port = atoi(portstr);

	if (port < 1) {
		/*
		 * SCMSGS
		 * @explanation
		 * The indicated port is not valid.
		 * @user_action
		 * Correct the port specified in Monitor_Uri_List.
		 */
		scds_syslog(LOG_ERR, "Port (%d) determined from "
			"Monitor_Uri_List is invalid", port);
		return (1);
	}


	scds_syslog(LOG_NOTICE, "Waiting for %s to startup", APP_NAME);
	/*
	 * sleep for SVC_WAIT_PCT percentage of start_timeout time
	 * before actually probing the dataservice. This is to allow
	 * the dataservice to be fully up in order to reply to the
	 * probe. NOTE: the value for SVC_WAIT_PCT could be different
	 * for different dataservices.
	 */
	if (scds_svc_wait(scds_handle, (svc_start_timeout * SVC_WAIT_PCT)/100)
		!= SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to start service.");
		return (1);
	}

	do {
		scds_syslog_debug(DBG_LEVEL_HIGH,
			"In svc_wait() inside the while "
			"loop checking for Monitor_uri_list status");
		/*
		 * probe the data service on the IP address of the
		 * network resource and the portname
		 */
		rc = svc_probe(scds_handle, hostname, port, probe_timeout);
		if (rc == SCHA_ERR_NOERR) {
			/* Success. Free up resources and return */
			return (SCHA_ERR_NOERR);
		}

		err = scds_pmf_get_status(scds_handle,
		    SCDS_PMF_TYPE_SVC, SCDS_PMF_SINGLE_INSTANCE, &status);
		if (err != SCHA_ERR_NOERR) {
			scds_syslog(LOG_ERR,
				"Failed to retrieve process monitor "
				"facility tag.");
			return (1);
		}

		/* Check if the dataservice is still up and running */
		if (status != SCDS_PMF_MONITORED) {
			scds_syslog(LOG_ERR,
				"Application failed to stay up. "
				"Start method Failure.");
			return (1);
		}

		/*
		 * Dataservice is still trying to come up. Sleep for a while
		 * before probing again.
		 */
		scds_syslog(LOG_NOTICE, "Waiting for %s to startup",
			APP_NAME);
		if (scds_svc_wait(scds_handle, SVC_WAIT_TIME)
			!= SCHA_ERR_NOERR) {
			scds_syslog(LOG_ERR, "Failed to start service.");
			return (1);
		}

	/* We rely on RGM to timeout and terminate the program */
	} while (1);
}

/*
 * This function starts the fault monitor for a jsas resource.  This
 * is done by starting the probe under PMF. The PMF tag is derived as
 * <RG-name,RS-name,instance_number.mon>. The restart option of PMF is
 * used but not the "infinite restart". Instead interval/retry_time is
 * obtained from the RTR file.
 */

int
mon_start(scds_handle_t scds_handle)
{
	scha_err_t	err;

	scds_syslog_debug(DBG_LEVEL_HIGH,
	    "Calling MONITOR_START method for resource <%s>.",
	    scds_get_resource_name(scds_handle));

	/*
	 * The probe jsas_probe is assumed to be available in the same
	 * subdirectory where the other callback methods for the RT
	 * are installed. The last parameter to scds_pmf_start denotes
	 * the child monitor level. Since we are starting the probe
	 * under PMF we need to monitor the probe process only and
	 * hence we are using a value of 0.
	 */

	err = scds_pmf_start(scds_handle, SCDS_PMF_TYPE_MON,
		SCDS_PMF_SINGLE_INSTANCE, "jsas_probe", 0);

	if (err != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to start fault monitor.");
		return (err);
	}

	scds_syslog(LOG_INFO, "Started the fault monitor.");

	return (SCHA_ERR_NOERR); /* Successfully started Monitor */
}


/*
 * This function stops the fault monitor for a jsas resource.
 * This is done via PMF. The PMF tag for the fault monitor is
 * constructed based on <RG-name_RS-name,instance_number.mon>.
 */

int
mon_stop(scds_handle_t scds_handle)
{
	scha_err_t	err;

	scds_syslog_debug(DBG_LEVEL_HIGH, "Calling scds_pmf_stop method");

	err = scds_pmf_stop(scds_handle, SCDS_PMF_TYPE_MON,
		SCDS_PMF_SINGLE_INSTANCE, SIGKILL,
		FINAL_STOP_TIMEOUT);

	if (err != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to stop fault monitor.");
		return (err);
	}

	scds_syslog(LOG_INFO, "Stopped the fault monitor.");

	return (SCHA_ERR_NOERR); /* Successfully stopped monitor */
}


/*
 * svc_probe(): Do data service specific probing. Return a float value
 * between 0 (success) and 100(complete failure).
 *
 * The probe does a simple socket connection to the jsas server on the
 * specified port which is configured as the resource extension
 * property (Port_list) and pings the dataservice. If the probe fails
 * to connect to the port, we return a value of 100 indicating that
 * there is a total failure. If the connection goes through and the
 * disconnect to the port fails, then a value of 50 is returned
 * indicating a partial failure.
 *
 * Used for simple connect/disconnect probe of a port.  For probing an
 * URI see svc_probe_uri().
 *
 */
int
svc_probe(scds_handle_t scds_handle, char *hostname, int port, time_t timeout)
{
	int	rc = 0;
	int	probe_status = 0;
	ulong_t	t1, t2;
	int	sock;
	int	time_used, time_remaining;
	long	connect_timeout;

	/*
	 * probe the dataservice by doing a socket connection to the
	 * port specified in the port_list property to the host that
	 * is serving the jsas dataservice. If the jsas service which
	 * is configured to listen on the specified port, replies to
	 * the connection, then the probe is successful. Else we will
	 * wait for a time period set in probe_timeout property before
	 * concluding that the probe failed.
	 */

	/*
	 * Use the SVC_CONNECT_TIMEOUT_PCT percentage of timeout
	 * to connect to the port
	 */
	connect_timeout = (SVC_CONNECT_TIMEOUT_PCT * timeout)/100;
	t1 = (ulong_t)(gethrtime()/1E9);

	/*
	 * the probe makes a connection to the specified hostname and port.
	 * The connection is timed for 95% of the actual probe_timeout.
	 */
	rc = scds_fm_tcp_connect(scds_handle, &sock, hostname, port,
		connect_timeout);
	if (rc) {
		/*
		 * SCMSGS
		 * @explanation
		 * Unable to connect to the port at hostname and port.
		 * @user_action
		 * If the problem persists Sun Cluster will restart or
		 * failover the resource.
		 */
		scds_syslog(LOG_ERR,
		    "Failed to connect to the host <%s> and port <%d>.",
		    hostname, port);
		/* this is a complete failure */
		probe_status = SCDS_PROBE_COMPLETE_FAILURE;
		goto finished;
	}

	t2 = (ulong_t)(gethrtime()/1E9);

	/*
	 * Compute the actual time it took to connect. This should be less than
	 * or equal to connect_timeout, the time allocated to connect.
	 * If the connect uses all the time that is allocated for it,
	 * then the remaining value from the probe_timeout that is passed to
	 * this function will be used as disconnect timeout. Otherwise, the
	 * the remaining time from the connect call will also be added to
	 * the disconnect timeout.
	 *
	 */

	time_used = (int)(t2 - t1);

	/*
	 * Use the remaining time(timeout - time_took_to_connect) to disconnect
	 */

	time_remaining = timeout - (int)time_used;

	/*
	 * If all the time is used up, use a small hardcoded timeout
	 * to still try to disconnect. This will avoid the fd leak.
	 */
	if (time_remaining <= 0) {
		scds_syslog_debug(DBG_LEVEL_LOW,
		    "svc_probe used entire timeout of "
		    "%d seconds during connect operation and exceeded the "
		    "timeout by %d seconds. Attempting disconnect with timeout"
		    " %d ",
		    connect_timeout,
		    abs(time_used),
		    SVC_DISCONNECT_TIMEOUT_SECONDS);

		time_remaining = SVC_DISCONNECT_TIMEOUT_SECONDS;
	}

	/*
	 * Return partial failure in case of disconnection failure.
	 * Reason: The connect call is successful, which means
	 * the application is alive. A disconnection failure
	 * could happen due to a hung application or heavy load.
	 * If it is the later case, don't declare the application
	 * as dead by returning complete failure. Instead, declare
	 * it as partial failure. If this situation persists, the
	 * disconnect call will fail again and the application will be
	 * restarted.
	 */
	rc = scds_fm_tcp_disconnect(scds_handle, sock, time_remaining);
	if (rc != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * Unable to connect to the port at hostname and port.
		 * @user_action
		 * If the problem persists Sun Cluster will restart or
		 * failover the resource.
		 */
		scds_syslog(LOG_ERR,
		    "Failed to disconnect from port %d of resource %s.",
		    port, scds_get_resource_name(scds_handle));
		/* this is a partial failure */
		probe_status = SCDS_PROBE_COMPLETE_FAILURE/2;
		goto finished;
	}

	t2 = (ulong_t)(gethrtime()/1E9);
	time_used = (int)(t2 - t1);
	time_remaining = timeout - time_used;

	/*
	 * If there is no time left return
	 * SCDS_PROBE_COMPLETE_FAILURE/2. This will make sure that if
	 * this timeout persists, server will be restarted.
	 */
	if (time_remaining <= 0) {
		scds_syslog(LOG_ERR, "Probe timed out.");
		probe_status = SCDS_PROBE_COMPLETE_FAILURE/2;
		goto finished;
	}

finished:

	return (probe_status);
}

/*
 * svc_probe_uri(): Do data service specific probing. Return a float
 * value between 0 (success) and 100(complete failure).
 *
 * The probe does a HTTP GET to the jsas server with the specified URI
 * which is configured as the resource extension property
 * (Monitor_Uri_List).  We return a value between 0 (for success) and
 * 100 (complete failure).
 *
 */
int
svc_probe_uri(scds_handle_t scds_handle, char *uri, time_t timeout)
{
	int	rc;
	int	status = -1;
	int	level = 0;	/* success level */

	scds_syslog_debug(DBG_LEVEL_HIGH, "Entered svc_probe_uri(%x, %s, %d)",
		scds_handle, uri, timeout);

	rc = ds_http_get_status(scds_handle, uri, &status, timeout, B_TRUE);
	scds_syslog_debug(DBG_LEVEL_HIGH, "svc_probe_uri(): "
		"%s status=%d rc=%d (%s)", uri, status, rc,
		scds_error_string(rc));

	switch (rc) {
	case SCHA_ERR_NOERR:
		/*
		 * Status Code 500 is internal server error.  Other
		 * status codes indicate a problem with the client
		 * (asking for a resource that does not exist, badly
		 * formed request syntax) or the server had an issue
		 * with some other server (504 - Gateway timeout).
		 *
		 * Those other problems should not be held against the
		 * server we are monitoring.
		 *
		 * If the status code is less then 0, that means the
		 * Status Code of the response was malformed.
		 * Consider it a tiny failure.
		 */
		if (status == 500) {
			level = SCDS_PROBE_COMPLETE_FAILURE;
			/*
			 * SCMSGS
			 * @explanation
			 * The status code of the response to a HTTP GET probe
			 * that indicates the HTTP server has failed. It will
			 * be restarted or failed over.
			 * @user_action
			 * This message is informational; no user action is
			 * needed.
			 */
			scds_syslog(LOG_ERR, "HTTP GET Response Code for "
				"probe of %s is %d. "
				"Failover will be in progress",
				uri, status);
		} else if (status < 0) {
			level = SCDS_PROBE_COMPLETE_FAILURE / 4;
			/* error already logged */
		} else
			level = 0;
		break;
	case SCHA_ERR_NOMEM:
	case SCHA_ERR_INVAL:
	case SCHA_ERR_INTERNAL:
		level = 0;
		break;
	case SCHA_ERR_TIMEOUT:
		level = SCDS_PROBE_COMPLETE_FAILURE/2;
		break;
	case SCHA_ERR_STATE:
		/* Connection refused */
		level = SCDS_PROBE_COMPLETE_FAILURE;
		break;
	default:
		level = 0;
		ds_internal_error("unknown error code from "
			"ds_http_get_status: %d (%s)", rc,
			scds_error_string(rc));

		break;
	}

	return (level);
}

scha_err_t
validate_uri(int print, scds_net_resource_list_t *snrlp, const char *uri)
{
	int	rc;
	char	*scheme = NULL, *hostname = NULL;
	char	*port = NULL, *path = NULL;
	int	i, j;

	scds_syslog_debug(DBG_LEVEL_HIGH, "Validating %s", uri);

	rc = ds_parse_simple_uri(uri, &scheme, &hostname, &port, &path);

	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Error parsing URI: %s (%s)", uri,
			scds_error_string(rc));
		if (print)
			(void) fprintf(stderr,
				gettext("Error parsing URI: %s (%s)\n"),
				uri, scds_error_string(rc));
		goto finished;
	}

	if (scheme == NULL) {
		scds_syslog(LOG_ERR, "Error parsing URI: %s (%s)", uri,
			"unable to determine scheme");
		if (print)
			(void) fprintf(stderr,
				gettext("Error parsing URI: %s (%s)\n"),
				uri, gettext("unable to determine scheme"));
		rc = SCHA_ERR_INVAL;
		goto finished;
	}

	if (hostname == NULL) {
		scds_syslog(LOG_ERR, "Error parsing URI: %s (%s)", uri,
			"unable to determine hostname");
		if (print)
			(void) fprintf(stderr,
				gettext("Error parsing URI: %s (%s)\n"),
				uri, gettext("unable to determine hostname"));
		rc = SCHA_ERR_INVAL;
		goto finished;
	}

	if (strcasecmp("http", scheme) != 0) {
		scds_syslog(LOG_ERR, "URI (%s) must be an absolute http URI.",
			uri);
		if (print)
			(void) fprintf(stderr,
				gettext("URI (%s) must be an absolute http "
					"URI\n."), uri);
		rc = SCHA_ERR_INVAL;
		goto finished;
	}


	/*
	 * Return an error if there are no network address resources.
	 * DAS needs atleast one Network resource in this RG.
	 */
	if (snrlp == NULL || snrlp->num_netresources == 0) {
		scds_syslog(LOG_ERR,
		    "No network address resource in resource group.");
		if (print) {
			(void) fprintf(stderr,
				gettext("No network address resource in "
					"resource group."));
		}
		rc = SCHA_ERR_INVAL;
		goto finished;
	}

	for (i = 0; i < snrlp->num_netresources; i++) {
		for (j = 0; j < snrlp->netresources[i].num_hostnames; j++) {
			if (strcasecmp(hostname,
			    snrlp->netresources[i].hostnames[j]) == 0) {
				scds_syslog_debug(DBG_LEVEL_HIGH,
					"URI validation: match "
					"on %s [%d][%d]", hostname, i, j);
				goto finished;
			}
		}
	}


	/*
	 * If we get here we did not find a match for the hostname in
	 * any of the network resource hostnames.  Log and if
	 * requested print a message and set rc to SCHA_ERR_INVAL for
	 * the return.
	 */

	scds_syslog(LOG_ERR, "The hostname in %s is not a network address "
		"resource in this resource group.", uri);
	if (print)
		(void) fprintf(stderr, gettext("The hostname in %s is not "
				"a network address resource in this "
				"resource group."), uri);
	rc = SCHA_ERR_INVAL;


finished:
	free(scheme);
	free(hostname);
	free(port);
	free(path);

	return (rc);
}

/*
 * Do HASP validation.
 *
 * Returns
 *	-1	error
 *	0	OK, but storage not online on this node
 *	1	OK, storage online on this node.  Do filesystem checks.
 */
int
validate_hasp(int print, scds_handle_t handle)
{
	int				err, rc = 0;
	scds_hasp_status_t		hasp_status;

	err = scds_hasp_check(handle, &hasp_status);

	if (err != SCHA_ERR_NOERR) {
		/*
		 * scha_hasp_check() logs a message to syslog when it
		 * fails
		 */
		if (print) {
			(void) fprintf(stderr,
				gettext("INTERNAL ERROR: %s.\n"),
				gettext("scds_hasp_check failed"));
		}
		/*
		 * validation has failed for this resource
		 */
		return (-1);
	}

	switch (hasp_status) {

	case SCDS_HASP_NO_RESOURCE:
		/*
		 * We do not depend on any SUNW.HAStoragePlus resources
		 */
		/*
		 * SCMSGS
		 * @explanation
		 * The resource does not depend on any HAStoragePlus
		 * filesystems. The validation will continue with it's other
		 * checks.
		 * @user_action
		 * This message is informational; no user action is needed.
		 */
		scds_syslog(LOG_INFO,
			"This resource does not depend on any "
			"SUNW.HAStoragePlus resources. Proceeding with "
			"normal checks.");
		rc = 1;
		break;

	case SCDS_HASP_ERR_CONFIG:
		/*
		 * Configuration error, SUNW.HAStoragePlus resource is
		 * in a different RG. Fail the validation.
		 */
		/*
		 * SCMSGS
		 * @explanation
		 * The HAStoragePlus resource that this resource depends on
		 * must be configured into the same resource group.
		 * @user_action
		 * Move the HAStoragePlus resource into this resource's
		 * resource group.
		 */
		scds_syslog(LOG_ERR,
			"One or more of the SUNW.HAStoragePlus resources "
			"that this resource depends on is in a different "
			"resource group. Failing validate method "
			"configuration checks.");
		if (print) {
			(void) fprintf(stderr,
				gettext("One or more of the "
					"SUNW.HAStoragePlus resources that "
					"this resource depends on is in a "
					"different resource "
					"group. Failing validate method "
					"configuration checks."));
		}
		rc = -1;
		break;

	case SCDS_HASP_NOT_ONLINE:
		/*
		 * There is at least one SUNW.HAStoragePlus resource not
		 * online anywhere.
		 */
		/*
		 * SCMSGS
		 * @explanation
		 * The HAStoragePlus resource this resource needs is not
		 * online anywhere in the cluster.
		 * @user_action
		 * Bring the HAStoragePlus resource online.
		 */
		scds_syslog(LOG_ERR,
			"One or more of the SUNW.HAStoragePlus resources "
			"that this resource depends on is not online "
			"anywhere. Failing validate method.");
		if (print) {
			(void) fprintf(stderr,
				gettext("One or more of the "
					"SUNW.HAStoragePlus resources that "
					"this resource depends on is not "
					"online anywhere. "
					"Failing validate method."));
		}
		rc = -1;
		break;

	case SCDS_HASP_ONLINE_NOT_LOCAL:
		/*
		 * Not all SUNW.HAStoragePlus we need, are online locally.
		 */
		/*
		 * SCMSGS
		 * @explanation
		 * The HAStoragePlus resources that this resource depends on
		 * are online on another node. Validation checks will be done
		 * on that other node.
		 * @user_action
		 * This message is informational; no user action is needed.
		 */
		scds_syslog(LOG_INFO,
			"All the SUNW.HAStoragePlus resources that this "
			"resource depends on are not online on the local "
			"node. Skipping the checks for the existence "
			"and permissions of the start/stop/probe commands.");
		rc = 0;
		break;

	case SCDS_HASP_ONLINE_LOCAL:
		/*
		 * All SUNW.HAStoragePlus resources we need are available on
		 * this node.
		 */
		/*
		 * SCMSGS
		 * @explanation
		 * The HAStoragePlus resource that this resource depends on is
		 * local to this node. Proceeding with the rest of the
		 * validation checks.
		 * @user_action
		 * This message is informational; no user action is needed.
		 */
		scds_syslog(LOG_INFO,
			"All the SUNW.HAStoragePlus resources that this "
			"resource depends on are online on the local node. "
			"Proceeding with the checks for the existence and "
			"permissions of the start/stop/probe commands.");
		rc = 1;
		break;

	default:
		/* Unknown status code */
		/*
		 * SCMSGS
		 * @explanation
		 * Checking for HAStoragePlus resources returned an unknown
		 * status code.
		 * @user_action
		 * Contact your authorized Sun service provider to determine
		 * whether a workaround or patch is available.
		 */
		scds_syslog(LOG_ERR, "Unknown status code %d.", hasp_status);
		if (print) {
			(void) fprintf(stderr,
				gettext("Unknown status code %d."),
				hasp_status);
		}
		/*
		 * It's not clear what should be done here.  The
		 * scdsbuilder code sets the return code to 1, but
		 * then proceeds to do the rest of the checks except
		 * the file existence checks.
		 *
		 * For now do the equivalent: return 0 "OK, but not
		 * online".
		 */
		rc = 0;
		break;
	}

	return (rc);
}


/*
* get_as_extn_props()
*
* Get the extension properties set for this App Server resource and
* store it in the asextnprops structure. The Monitor_uri_list extension
* property is not retrieved now as this is used only by the probe
* and hence will be retrieved in the probe function.
*
*/

int
get_as_extn_props(scds_handle_t scds_handle, as_extn_props_t *asextnprops,
			boolean_t print_messages)
{
	scha_str_array_t 	*confdirs = NULL;
	scha_extprop_value_t	*extnprop = NULL;
	int	err, rc = 0;
	size_t	size = 0;
	char	*default_domain_dir = NULL;

	scds_syslog_debug(DBG_LEVEL_HIGH, "In get_as_extn_props() function");

	/*
	 * Get the path to the JSAS Home directory which is the
	 * confdir_list.
	 */

	confdirs = scds_get_ext_confdir_list(scds_handle);

	/*
	 * Return an error if config_dirs extn prop is not set
	 * or if the array_cnt is not equal to 1.
	 * The array_cnt is expected to be 1 as each resource
	 * should configure only one AS Home directory.
	 */

	if (confdirs->str_array == NULL) {
		scds_syslog(LOG_ERR,
			"Property Confdir_list is not set.");
		if (print_messages) {
			(void) fprintf(stderr, gettext("Property Confdir_list "
			    "is not set."));
		}
		return (1);
	}
	if (confdirs->array_cnt != 1) {
		scds_syslog(LOG_ERR,
			"Only a single path to the JSAS install directory "
			"has to be set in Confdir_list");
		if (print_messages) {
			(void) fprintf(stderr, gettext("Only a single path "
				"to the JSAS install directory has to be "
				"set in Confdir_list.\n"));
		}
		return (1);
	}

	/* Check that Confdir_list[0] is an absolute path. */
	if (confdirs->str_array[0][0] != '/') {
		scds_syslog(LOG_ERR,
			"Confdir_list must be an absolute path.");
		if (print_messages) {
			(void) fprintf(stderr, gettext("Confdir_list must "
					"be an absolute path."));
		}
		return (1);
	}

	/*
	 * Copy the confdirs extension property into the install_dir
	 * variable of the asextnprops structure.
	 */

	asextnprops->install_dir = confdirs->str_array[0];

	/* Get the Admin User extension property */

	err = scds_get_ext_property(scds_handle, "Adminuser",
				SCHA_PTYPE_STRING, &extnprop);

	if (err != SCHA_ERR_NOERR || extnprop == NULL) {
		scds_syslog(LOG_ERR,
			"Failed to retrieve the property %s: %s.",
			"Admin_User", scds_error_string(err));
		if (print_messages) {
			(void) fprintf(stderr, gettext("Failed to retrieve the "
			    "extension property %s: %s.\n"), "Admin_User",
				scds_error_string(err));
		}
		return (1);
	}

	/* return error if Admin_user is not set */

	if (extnprop->val.val_str[0] == '\0') {
		scds_syslog(LOG_ERR,
			"Extension property <%s> has a value of <%s>",
			"Admin_User", "NULL");
		if (print_messages) {
			(void) fprintf(stderr, gettext("Extension property "
				"<%s> has a value of <%s>.\n"),
				"Admin_User", "NULL");
		}
		return (1);
	}

	/* Save the extension property in the asextnprops structure */

	asextnprops->admin_user = extnprop->val.val_str;

	/* Get the extension property Admin_Password_File */

	err = scds_get_ext_property(scds_handle, "Passwordfile",
				SCHA_PTYPE_STRING, &extnprop);

	if (err != SCHA_ERR_NOERR || extnprop == NULL) {
		scds_syslog(LOG_ERR,
			"Failed to retrieve the property %s: %s.",
			"admin_passwordFile", scds_error_string(err));
		if (print_messages) {
			(void) fprintf(stderr, gettext("Failed to retrieve "
					"the extension property %s: %s.\n"),
					"admin_passwordFile",
					scds_error_string(err));
		}
		return (1);
	}

	/* Save the extension property in the asextnprops structure */

	asextnprops->password_file = extnprop->val.val_str;

	/* Get the extension property Domain_dir */

	err = scds_get_ext_property(scds_handle, "Domaindir",
				SCHA_PTYPE_STRING, &extnprop);

	if (err != SCHA_ERR_NOERR || extnprop == NULL) {
		scds_syslog(LOG_ERR,
			"Failed to retrieve the property %s: %s.",
			"Domaindir", scds_error_string(err));
		if (print_messages) {
			(void) fprintf(stderr, gettext("Failed to retrieve "
					"the extension property %s: %s.\n"),
					"Domaindir",
					scds_error_string(err));
		}
		return (1);
	}

	/*
	 * If the user has not set the extension property "domain_dir"
	 * then use the default path. The Default path is set in the
	 * file asenv.conf under the install directory.
	 */

	if (extnprop->val.val_str[0] == '\0') {
		/* Get the Default Value from the  asenv.conf file */

		rc = get_default_domain_dir(asextnprops->install_dir,
				&default_domain_dir, print_messages);

		if ((rc == 1) || (default_domain_dir == NULL)) {
			return (1); /* Message already logged */
		}
		/*
		 * Set the Domain directory to the default directory
		 * In the asenv.conf file the variable and its value
		 * are set as shown below
		 * AS_DEF_DOMAINS_PATH = "/SUNWappserver/domains"
		 * So the string we got "default_domain_dir"
		 * contains the double quotes. We need to remove
		 * them now. Replace the doubel quote at the end
		 * with '\0' and then copy the string address
		 * right after the first double quote.
		 */
		/* Get the length of the string we got */
		size = strlen(default_domain_dir);

		/* Replace the last quote by '\0' i.e NULL terminate */
		default_domain_dir[size-1] = '\0';

		/* Now skip the first quote before copying */
		asextnprops->domain_dir = ++default_domain_dir;

	} else {
		/* Set the Domain Directory to whatever the user has set */

		asextnprops->domain_dir = extnprop->val.val_str;
	}

	/* Get the extension property Domain Name */

	err = scds_get_ext_property(scds_handle, "Domain_name",
				SCHA_PTYPE_STRING, &extnprop);

	if (err != SCHA_ERR_NOERR || extnprop == NULL) {
		scds_syslog(LOG_ERR,
			"Failed to retrieve the property %s: %s.",
					"Domain_name",
					scds_error_string(err));
		if (print_messages) {
			(void) fprintf(stderr, gettext("Failed to retrieve "
			    "extension property %s.\n"), "Domain_name");
		}
		return (1);
	}

	/* Save the extension property in the asextnprops structure */

	asextnprops->domain_name = extnprop->val.val_str;

	/*
	 *If we are here then we have all the extension properties needed
	 */

	scds_syslog_debug(DBG_LEVEL_LOW,
		"The App Server Install directory is <%s> "
		"the Admin User name is <%s> "
		"the Admin Password File is <%s> "
		"the Domain Directory is <%s> "
		"the Domain name is <%s>",
		asextnprops->install_dir,
		asextnprops->admin_user,
		asextnprops->password_file,
		asextnprops->domain_dir,
		asextnprops->domain_name);

	return (0);
}

/*
 * Get the default domain directory set in the asenv.conf file.
 * The Defautl domain directory is set in the variable  AS_DEF_DOMAINS_PATH
 * variable.
 * Return 1 if it fails else return 0 for success.
 *
*/
int
get_default_domain_dir(char *install_dir, char **domain_dir,
					boolean_t print_messages)
{
	FILE	*fp = NULL;
	char    line[1024];
	char	*p = NULL, *val = NULL;
	char	*path_to_asenv_conf_file;
	char    asenv_conf[SCDS_ARRAY_SIZE];
	struct stat		statbuf;

	scds_syslog_debug(DBG_LEVEL_HIGH, "In "
			"get_default_domain_dir () function");

	/*
	 * Check to see if the version is pre-9.1. If stat fails,
	 * it could be application server 9.1. Else it could be
	 * Application server 8.*.
	 */
	pre_glassfish = B_TRUE;
	(void) sprintf(asenv_conf, "%s/appserver/config/asenv.conf",
		install_dir);
	if (stat(asenv_conf, &statbuf) != 0) {
		pre_glassfish = B_FALSE;
	}

	if (pre_glassfish) {
		path_to_asenv_conf_file =
			ds_string_format("%s/appserver/config/asenv.conf",
					install_dir);
	} else {
		path_to_asenv_conf_file =
			ds_string_format("%s/config/asenv.conf",
					install_dir);
	}
	scds_syslog_debug(DBG_LEVEL_HIGH,
		"The path to asenv.conf file is %s", path_to_asenv_conf_file);

	if (path_to_asenv_conf_file == NULL) {
		ds_internal_error("Out of Memory");
		return (1);
	}

	fp = fopen(path_to_asenv_conf_file, "r");

	if (fp == NULL) {
		scds_syslog(LOG_ERR,
		"Unable to open %s: %s.",
		path_to_asenv_conf_file, strerror(errno));

		if (print_messages) {
			(void) fprintf(stderr,
				gettext("Unable to open <%s>: %s."),
				path_to_asenv_conf_file, strerror(errno));
		}
		return (1);
	} else {
		while (fgets(line, 1024, fp) != NULL) {

			/* Skip white spaces */
			for (p = line; isspace(*p); p++)
				;
			/* Skip empty lines */
			if (*p == 0)
				continue;
			/* Skip comment lines */
			if (*p == '#')
				continue;
			if ((p = (char *)strtok(p, "=")) == NULL)
				continue;
			if ((val = (char *)strtok(NULL, "\n"))
				== NULL)
				continue;

			if (strcasecmp(p, "AS_DEF_DOMAINS_PATH") == 0) {

				if ((*domain_dir = strdup(val)) == NULL) {
					scds_syslog(LOG_ERR,
						"INTERNAL ERROR: %s.",
						"Failed to allocate "
						"space for %s", val);
					if (print_messages) {
						(void) fprintf(stderr,
						gettext("INTERNAL ERROR: "
						"Failed to allocate space "
						"for %s"), val);
					}
					(void) fclose(fp);
					return (1);
				}
				scds_syslog_debug(DBG_LEVEL_HIGH,
					"The Default Domain dir is %s",
					*domain_dir);
				(void) fclose(fp);
				return (0);
			}
		} /* End While loop */
		/*
		 * SCMSGS
		 * @explanation
		 * The variable AS_DEF_DOMAINS_PATH is not set in the
		 * asenv.conf configuration file. The path to the default
		 * domain directory is by default set in this variable in the
		 * asenv.conf file. If this is not available, the agent cannot
		 * know the default domain directory.
		 * @user_action
		 * Set the extension property domaindir while creating the
		 * resource.
		 */
		scds_syslog(LOG_ERR,
			"AS_DEF_DOMAINS_PATH not found in the JSAS "
			"config file %s",
			path_to_asenv_conf_file);
		(void) fclose(fp);
		return (1);
	} /* End else  */
}

/*
 * Function to get the status of the DAS .
 * We call jsas_utils script where the actual
 * asadmin command is run.
 * return 0 if DAS is running  or 1 if DAS is "not running"
 * or if the command times out.
 * We return 2 for "out of memory" errors.
 */

int
get_das_status(scds_handle_t scds_handle, int timeout,
		as_extn_props_t *asextnprops)
{
	int		rc;
	char		*cmd = NULL;
	const char	*rt_base_dir;
	int		exit_code = 0;


	scds_syslog_debug(DBG_LEVEL_HIGH, "In get_das_status() "
		"function");

	rt_base_dir = scds_get_rt_rt_basedir(scds_handle);

	cmd = ds_string_format("%s/jsas_utils %s %s %s",
		rt_base_dir,
		asextnprops->install_dir,
		asextnprops->domain_dir,
		asextnprops->domain_name);

	if (cmd == NULL) {
		ds_internal_error("Out of Memory");
		return (2);
	}

	scds_syslog_debug(DBG_LEVEL_HIGH, "The command used to "
		"get the status of DAS is %s", cmd);

	rc = scds_timerun(scds_handle, cmd, timeout,
			SIGKILL, &exit_code);

	if (rc != SCHA_ERR_NOERR) {
		if (rc == SCHA_ERR_TIMEOUT) {
			scds_syslog_debug(DBG_LEVEL_HIGH,
				"scds_timerun in get_das_status() "
				"returned SCHA_ERR_TIMEOUT");
			return (1);
		} else {
			scds_syslog_debug(DBG_LEVEL_HIGH,
				"Failed to run the asadmin command");
			return (1);
		}
	}

	return (exit_code);

}
