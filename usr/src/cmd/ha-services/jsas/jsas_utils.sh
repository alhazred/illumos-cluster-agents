#!/bin/sh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.

#pragma ident	"@(#)jsas_utils.sh	1.6	09/03/13 SMI"

#
# What:
#	This script is used to check for the status
#	of the DAS server .  
#
# The "asadmin list-domains" gives the status of the
# DAS process as shown below
# -------------------------------------------------------
# bash-2.05# ./asadmin list-domains --domaindir /dg1/domain
# List of domains:
# new-domain running
# -------------------------------------------------------
# We use /usr/bin/grep to get the status.
# return 0 if DAS is running  or 1 if DAS is "not running" or 3 if DAS "requires restart"
#
# Return Value:
#	0 If DAS is running
#	1 If DAS is not running
#	3 If DAS requires restart
#set -x

PATH=/bin:/usr/bin:/usr/cluster/bin:/usr/sbin:/usr/cluster/lib/sc:$PATH;export PATH
syslog_tag="SC[SUNW.jsas,jsas_utils]"

# Checking the command syntax

if [ $# -ne 3 ]
then
	# SCMSGS
	# @explanation
	# Need explanation of this message!
	# @user_action
	# Need a user action for this message.
	scds_syslog -p error -t $syslog_tag -m \
		 "INTERNAL ERROR usage:$0 install_dir domaindir domain_name"
	exit 1
fi


INSTALL_DIR=$1
DOMAIN_DIR=$2
DOMAIN_NAME=$3

if [ -f "$INSTALL_DIR/bin/asadmin" ]
then
	ASADMIN=$INSTALL_DIR/bin/asadmin
else
	ASADMIN=$INSTALL_DIR/appserver/bin/asadmin
fi

	## 'grep' would fail for locales other than C, so run this under C locale.
	LC_ALL=C $ASADMIN list-domains --domaindir $DOMAIN_DIR | /usr/bin/grep -i $DOMAIN_NAME | /usr/bin/grep -i running | grep -v not > /dev/null
	rc=$?

if [ $rc -ne 0 ]
then
#CR 6401735:Agent restarted DAS because list-domain status does not return running on ha-cluster creation
#If list-domains returns "requires restart" return 3.
	## 'grep' would fail for locales other than C, so run this under C locale.
	LC_ALL=C $ASADMIN list-domains --domaindir $DOMAIN_DIR | /usr/bin/grep -i $DOMAIN_NAME | /usr/bin/grep -i "requires restart" > /dev/null
	rc=$?
	if [ $rc -eq 0 ]
	then
		exit 3
	fi
	exit 1
else
	exit 0
fi
