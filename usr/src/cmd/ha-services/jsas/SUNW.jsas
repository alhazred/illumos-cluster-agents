#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#pragma ident	"@(#)SUNW.jsas	1.8	07/06/06 SMI"

# Registration information and Paramtable for s1as
#
# NOTE: Keywords are case insensitive, i.e. users may use any
# capitalization style they wish
#

RESOURCE_TYPE = "jsas";
VENDOR_ID = SUNW;
RT_DESCRIPTION = "HA Sun Java System Application Server";

RT_VERSION ="4";
API_VERSION = 5;
FAILOVER = TRUE;

INIT_NODES = RG_PRIMARIES;

RT_BASEDIR=/opt/SUNWscs1as/jsas/bin;

START				=	jsas_svc_start;
STOP				=	jsas_svc_stop;

VALIDATE			=	jsas_validate;
UPDATE	 			=	jsas_update;

MONITOR_START			=	jsas_monitor_start;
MONITOR_STOP			=	jsas_monitor_stop;
MONITOR_CHECK			=	jsas_monitor_check;

PKGLIST = SUNWscs1as;

#
# Upgrade directives
#
#$upgrade
#$upgrade_from "3.1" anytime
#$upgrade_from "3.2" anytime

# For package upgrade
#% SERVICE_NAME = "jsas"

# The paramtable is a list of bracketed resource property declarations
# that come after the resource-type declarations
# The property-name declaration must be the first attribute
# after the open curly of a paramtable entry
#
# The following are the system defined properties. Each of the system
# defined properties has a default value set for each of the
# attributes. Look at man rt_reg(4) for a detailed explanation. 
#
{
	PROPERTY = Start_timeout;
	MIN = 60;
	DEFAULT = 600;
}

{
	PROPERTY = Stop_timeout;
	MIN = 5;
	DEFAULT = 90;
}
{
	PROPERTY = Validate_timeout;
	MIN = 60;
	DEFAULT = 180;
}
{
        PROPERTY = Update_timeout;
	MIN = 60;
        DEFAULT = 180;
}
{
	PROPERTY = Monitor_Start_timeout;
	MIN = 60;
	DEFAULT = 180;
}
{
	PROPERTY = Monitor_Stop_timeout;
	MIN = 60;
	DEFAULT = 180;
}
{
	PROPERTY = Monitor_Check_timeout;
	MIN = 60;
	DEFAULT = 180;
}
{
        PROPERTY = FailOver_Mode;
        DEFAULT = SOFT;
        TUNABLE = ANYTIME;
}
{
        PROPERTY = Network_resources_used;
        TUNABLE = WHEN_DISABLED;
	DEFAULT = "";
}
{
	PROPERTY = Thorough_Probe_Interval;
	MAX = 3600;
	DEFAULT = 120;
	TUNABLE = ANYTIME;
}
{
	PROPERTY = Retry_Count;
	MAX = 10;
	DEFAULT = 2;
	TUNABLE = ANYTIME;
}
{
	PROPERTY = Retry_Interval;
	MAX = 3600;
	DEFAULT = 1220;
	TUNABLE = ANYTIME;
}

{
	PROPERTY = Port_list;
	DEFAULT = "";
	TUNABLE = ANYTIME;
}

#
# Extension Properties
#

{
	PROPERTY = Confdir_list;
	EXTENSION;
	STRINGARRAY;
	DEFAULT = "/opt/SUNWappserver";
	TUNABLE = AT_CREATION;
	DESCRIPTION = "The Configuration Directory Path";
}

# List of URIs to be probed.  The app server agent will send HTTP/1.1
# GET requests to each of the listed URIs.  
#
# The probe does look at the response code but the only one considered
# a "failure" that will result in failover of the resource is response
# code 500 (Internal Server Error).
{
	PROPERTY = Monitor_Uri_List;
	EXTENSION;
	STRINGARRAY;
	DEFAULT = "";
	TUNABLE = ANYTIME;
	DESCRIPTION = "URI(s) that this server hosts to be probed";
}

# These two control the restarting of the fault monitor itself
# (not the server daemon) by PMF.
{
	PROPERTY = Monitor_retry_count;
	EXTENSION;
	INT;
	DEFAULT = 4;
	TUNABLE = ANYTIME;
	DESCRIPTION = "Number of PMF restarts allowed for the fault monitor";
}

{
	PROPERTY = Monitor_retry_interval;
	EXTENSION;
	INT;
	DEFAULT = 2;
	TUNABLE = ANYTIME;
	DESCRIPTION = "Time window (minutes) for fault monitor restarts";
}

# Time out value for the probe.
#
# Probe_timeout should be increased based on the number of URIs probed
# in Monitor_Uri_List.  The amount of time to generate a response to a
# request of those URIs should also be considered.
{
	PROPERTY = Probe_timeout;
	EXTENSION;
	INT;
	MIN = 2;
	DEFAULT = 180;
	TUNABLE = ANYTIME;
	DESCRIPTION = "Time out value for the probe (seconds)";
}

# Extension Property to specify the DAS Admin User name
{
	PROPERTY = Adminuser;
	EXTENSION;
	STRING;
	TUNABLE = AT_CREATION;
	DESCRIPTION = "The DAS Admin User Name";
}

# Extension Property to specify the complete Path to the File Containing DAS Admin Password 
{
	PROPERTY = Passwordfile;
	EXTENSION;
	STRING;
	TUNABLE = AT_CREATION;
	DESCRIPTION = "The Complete Path to the File containing the DAS Admin Password";
}

# Extension Property to specify the Complete Path to the Domain Directory
{
	PROPERTY = Domaindir;
	EXTENSION;
	STRING;
	DEFAULT = "";
	TUNABLE = AT_CREATION;
	DESCRIPTION = "The complete Path to the Domain Directory ";
}
# Extension Property to specify the Domain name
{
	PROPERTY = Domain_name;
	EXTENSION;
	STRING;
	TUNABLE = AT_CREATION;
	DESCRIPTION = "The Domain Name";
}
