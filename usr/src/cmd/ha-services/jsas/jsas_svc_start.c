/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * jsas_svc_start.c - Start method for s1as
 */

#pragma ident	"@(#)jsas_svc_start.c	1.7	07/06/06 SMI"

#include <rgm/libdsdev.h>
#include "jsas.h"

/*
 * The start method for s1as. Does some sanity checks on
 * the resource settings then starts the s1as under PMF with
 * an action script.
 */

int
main(int argc, char *argv[])
{
	scds_handle_t	scds_handle;
	int		rc;
	char		*rgname, *rsname;
	as_extn_props_t	asextnprops;

	/*
	 * Process all the arguments that have been passed to us from RGM
	 * and do some initialization for syslog
	 */

	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR) {
		return (1);
	}

	rsname = (char *)scds_get_resource_name(scds_handle);
	rgname = (char *)scds_get_resource_group_name(scds_handle);

	/* Get the App Server extension properties */

	if (get_as_extn_props(scds_handle, &asextnprops, B_FALSE) != 0) {
		/* Error messages already logged */
		rc = 1;
		goto finished;
	}

	/* Validate the configuration and if there is an error return back */
	rc = svc_validate(scds_handle, &asextnprops, B_FALSE);
	if (rc != 0) {
		scds_syslog(LOG_ERR,
			"Failed to validate configuration.");
		goto finished;
	}

	rc = get_das_status(scds_handle,
		scds_get_rs_start_timeout(scds_handle),
		&asextnprops);

	if (rc == 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * It is detected that DAS is already running on this
		 * node outside of Sun Cluster. The start of DAS from
		 * Sun Cluster will be aborted.
		 * @user_action
		 * No user action is needed.
		 */
		scds_syslog(LOG_ERR,
			"Sun Java Systems Application Server "
			"<%s> is already running on this "
			"node outside of Sun Cluster. "
			"The start of <%s> from Sun Cluster will "
			"be aborted.",
			asextnprops.domain_name,
			asextnprops.domain_name);
		rc = scha_control(SCHA_IGNORE_FAILED_START,
				rgname, rsname);
		(void) scha_resource_setstatus(rsname, rgname,
			SCHA_RSSTATUS_FAULTED,
			"Offline - Domain running outside "
			"Sun Cluster.");
		return (2); /* Bail Out */
	}


	/* Start the data service, if it fails return with an error */
	rc = svc_start(scds_handle, &asextnprops);

	if (rc != 0) {
		goto finished;
	}

	/* Wait for the service to start up fully */
	rc = svc_wait(scds_handle, &asextnprops);
	scds_syslog_debug(DBG_LEVEL_HIGH, "Returned from svc_wait");

finished:
	if (rc != SCHA_ERR_NOERR)
		(void) scha_resource_setstatus(rsname, rgname,
			SCHA_RSSTATUS_FAULTED,
			"Failed to start the Server.");
	else
		(void) scha_resource_setstatus(rsname, rgname,
			SCHA_RSSTATUS_OK,
			"Successfully started Server.");

	/* Free up the Environment resources that were allocated */
	scds_close(&scds_handle);

	return (rc);
}
