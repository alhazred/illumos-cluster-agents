#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2002 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# Clear the liveCache database.
# Input: confdir_list (eg. /sapdb)
# Input: livecache name
# Input: logical host name for livecache
#
#pragma ident	"@(#)db_clear.ksh	1.5	07/06/06 SMI"

PGREP=/usr/bin/pgrep
WC=/usr/bin/wc
XARGS=/usr/bin/xargs

if [ $# = 3 ]; then
	CONFDIR=$1
	LC_NAME=$2
	LH_HOST=$3
else
	print "Invalid Invocation."
	print "Usage: $0 <Confdir_list> <Livecache_Name> <LH_HOSTNAME>"
	exit 1
fi

$CONFDIR/programs/bin/dbmcli -d $LC_NAME -n $LH_HOST db_clear

# need to remove all left over process, otherwise, restart would fail
if [[ `$PGREP -f "$CONFDIR/$LC_NAME/db/pgm/kernel $LC_NAME"|$WC -l` -gt 0 ]]; then
	exec $PGREP -f "$CONFDIR/$LC_NAME/db/pgm/kernel $LC_NAME"|$XARGS kill -9
fi
