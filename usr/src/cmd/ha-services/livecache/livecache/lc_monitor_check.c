/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2002 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * lc_monitor_check.c - Monitor Check method for livecache.
 */

#pragma ident	"@(#)lc_monitor_check.c	1.6	07/06/06 SMI"

#include "lc.h"

/*
 * Make sure SAP xserver is running on the node liveCache is going to
 * failover to.
 */

int
main(int argc, char *argv[])
{
	scds_handle_t   scds_handle;
	int	rc;
	lc_extprops_t	lcxpropsp;

	/* Process the arguments passed by RGM and initialize syslog */
	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR) {
		return (1);
	}

	if (get_lc_extension(scds_handle, &lcxpropsp, B_FALSE) != 0) {
		scds_close(&scds_handle);
		return (1);
	}

	rc = svc_validate(scds_handle, &lcxpropsp, B_FALSE);
	if (rc != 0) {
		scds_syslog(LOG_ERR, "Failed to validate configuration.");
		goto finished;
	}

	rc = svc_mon_check(scds_handle, lcxpropsp.ext_confdir_list);
	scds_syslog_debug(SCDS_DBG_HIGH,
	    "Monitor_check method was called and returned %d.", rc);

finished:

	scds_close(&scds_handle);

	/* Return the result of validate method as part of monitor check */
	return (rc);
}
