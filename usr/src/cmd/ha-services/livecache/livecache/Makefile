#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# Makefile for data service: SAP livecache database
#
#ident	"@(#)Makefile	1.10	07/06/06 SMI"
#
# cmd/ha-services/livecache/livecache/Makefile
#

COMMON_SRCS = lc.c
SCRIPTS = parse_action_file \
		db_clear \
		lccluster \
		check_db_state \
		check_lccluster_file

SRCS = lc_svc_start.c \
	lc_svc_stop.c \
	lc_validate.c \
	lc_update.c \
	lc_monitor_start.c \
	lc_monitor_stop.c \
	lc_monitor_check.c \
	lc_probe.c

# objects
COMMON_OBJS = $(COMMON_SRCS:%.c=%.o)
OBJECTS = $(SRCS:%.c=%.o) $(COMMON_OBJS)

PROG = $(SRCS:%.c=%) $(SCRIPTS)

include ../../../Makefile.cmd

#Packaging
PKGNAME = SUNWsclc/livecache
RTRFILE = SUNW.sap_livecache

# I18n stuff
TEXT_DOMAIN = SUNW_SC_SAPLC
MESSAGE_DIR = /opt/$(PKGNAME)/lib/locale
PIFILES = $(SRCS:%.c=%.pi) $(COMMON_SRCS:%.c=%.pi)
POFILE = saplc-ds.po

#library
LDLIBS  += -ldsdev -lscha

LINTFILES = $(SRCS:%.c=%.ln) $(COMMON_SRCS:%.c=%.ln)

.KEEP_STATE:

all: $(COMMON_OBJS) $(PROG) $(ROOTOPTETCRTR)

.PARALLEL: $(PROG)

install: all $(ROOTOPTBINPROG)

include ../../../Makefile.targ

$(SCRIPTS): $(SCRIPTS:%=%.ksh)
	$(CP) $(@:%=%.ksh) $@
	$(CHMOD) +x $@

$(SRCS:%.c=%): $(COMMON_OBJS) $$(@:%=%.o)
	$(LINK.c) -o $@ $(@:%=%.o) $(COMMON_OBJS) $(LDFLAGS) $(LDLIBS)
	$(POST_PROCESS)

clean:
	$(RM) -f $(OBJECTS) $(PROG) 
