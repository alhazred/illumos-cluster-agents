#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2002 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# Parse the lccluster output file (/sapdb/<LC_NAME>/db/sap/lccluster.out)
# and see if it contains certain actions.
#
# Input: file to parse
# Output: 0 Doesn't contain the stop nor init actions
#	1 Contains intended stop, or intended init actions
#	2 Contains start error
#	10 Usage error
#
#pragma ident	"@(#)parse_action_file.ksh	1.5	07/06/06 SMI"

EGREP="/usr/bin/egrep"

if [ $# = 1 ]; then
	FILE=$1
else
	print "Invalid Invocation."
	print "Usage: $0 <File to parse>"
	exit 10
fi

CNT=`$EGREP -c 'stopping req|stopping err|stopping ok|init req|init err' $FILE`

# egrep returns 2 for syntax errors or inaccessible files.
if [[ $? -eq 2 ]]; then
	exit 10
fi

if [[ $CNT -gt 0 ]]; then
	exit 1
else
	CNT=`$EGREP -c 'starting err' $FILE`
	if [[ $? -eq 2 ]]; then
		exit 10
	fi

	if [[ $CNT -gt 0 ]]; then
		exit 2
	else
		exit 0
	fi
fi
