/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2002 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_LIVECACHE_H
#define	_LIVECACHE_H

#pragma ident	"@(#)lc.h	1.6	07/06/06 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <scha.h>
#include <sys/time.h>
#include <signal.h>
#include <rgm/libdsdev.h>
#include <errno.h>
#include <synch.h>
#include <pwd.h>
#include <libgen.h>
#include <sys/wait.h>
#include <sys/utsname.h>
#include <unistd.h>
#include <dlfcn.h>
#include <ctype.h>
#include <libintl.h>
#include <locale.h>

/* extension property */
typedef struct lc_extprops
{
	char	*ext_confdir_list;
	char	*ext_lc_name;
	char	*ext_lccluster_file; /* this is not a real ext property */
				    /* /sapdb/<LC_NAME>/db/sap/lccluster.out */
	char	*ext_lc_state_file; /* this is not a real ext property */
				    /* /sapdb/<LC_NAME>/db/wrk/lc.state */
	char	*lc_lh;		/* this is not a real ext property */
	char	*lc_adm;	/* this is not a real ext property */
				/* <lc_name>adm */
} lc_extprops_t;

/* executable scripts */
#define	PARSE_ACTION_FILE	"parse_action_file"
#define	CHECK_DB_STATE 		"check_db_state"
#define	CHECK_LCCLUSTER_FILE	"check_lccluster_file"

/* executables */
#define	DBCLEAR			"db_clear"
#define	SU			"/usr/bin/su"

/* Extension property */
#define	CONFDIRLIST		"Confdir_list"
#define	LCNAME			"Livecache_Name"

/* return code */
#define	XSVR_UP			1
#define	XSVR_DOWN		2
#define	LC_ONLINE		3
#define	LC_NOT_FOUND		4
#define	INTENDED_STOP		5
#define	START_ERR		8
#define	UNINTENDED_STOP		9
#define	SYS_ERR			10
#define	PARENT_DIE		11
#define	DBMCLI_FAILED		12

#define	INT_ERR_MSG_SIZE	1024
#define	SCDS_CMD_SIZE		(8 * 1024)
#define	SCDS_DBG_HIGH		9


int svc_validate(scds_handle_t scds_handle, lc_extprops_t *lcxpropsp,
	boolean_t print_messages);

int svc_start(scds_handle_t scds_handle, lc_extprops_t *lcxpropsp);

int svc_stop(scds_handle_t scds_handle, lc_extprops_t *lcxpropsp);

int mon_start(scds_handle_t scds_handle);

int mon_stop(scds_handle_t scds_handle);

int svc_probe(scds_handle_t scds_handle, lc_extprops_t *lcxpropsp,
int timeout);

int svc_mon_check(scds_handle_t scds_handle, char *confdir_list);

int read_state_file(char *state_file);

int
update_intended_stop(char *state_file, int intended);

int
user_intended_stop(scds_handle_t scds_handle, lc_extprops_t *lcxpropsp,
int timeout);

int
db_state(scds_handle_t scds_handle, lc_extprops_t *lcxpropsp, int timeout);

int
get_lc_extension(scds_handle_t handle, lc_extprops_t *lcxpropsp,
	boolean_t print_messages);

int
get_one_lc_extension(scds_handle_t scds_handle, char *prop_name, char **prop,
	boolean_t print_messages);

int
svc_wait(scds_handle_t scds_handle, lc_extprops_t *lcxpropsp);

int
call_lccluster(scds_handle_t scds_handle, char *lc_adm, char *confdir,
char *lc_name, int start_timeout);

void free_lc_property(lc_extprops_t *lcxpropsp);

#ifdef __cplusplus
}
#endif

#endif	/* _LIVECACHE_H */
