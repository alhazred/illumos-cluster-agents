#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2002 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# Check the script lccluster for the followings:
#	exists
#	is executable
#	the livecache instance name is defined in it
#
# Input: Confdir_list
# Input: LC-NAME
# Output: 0 if real LC-NAME is defined in lccluster script
#	1 if real LC-NAME is not defined in lccluster script
#	2 if lccluster script doens't exit
#	3 if lccluster script is not executable
#	4 if real Confdir_list is not defined in lccluster script
#	10 Usage error
#
#pragma ident	"@(#)check_lccluster_file.ksh	1.6	07/06/06 SMI"

EGREP="/usr/bin/egrep"

if [ $# = 2 ]; then
	CONFDIR_LIST=$1
	LC_NAME=$2
else
	print "Invalid Invocation."
	print "Usage: $0 <Confdir_list> <Livecache_Name>"
	exit 10
fi

if [ ! -f $CONFDIR_LIST/$LC_NAME/db/sap/lccluster ]; then
	exit 2
elif [ ! -x $CONFDIR_LIST/$LC_NAME/db/sap/lccluster ]; then
	exit 3
fi

# check lccluster for LC_NAME
CNT=`$EGREP LC_NAME= $CONFDIR_LIST/$LC_NAME/db/sap/lccluster|$EGREP -c $LC_NAME`

# egrep returns 2 for syntax errors or inaccessible files.
if [[ $? -eq 2 ]]; then
	exit 10
fi

if [[ $CNT -ne 1 ]]; then
	exit 1
fi

# check lccluster for Confdir_list
CNT=`$EGREP CONFDIR_LIST= $CONFDIR_LIST/$LC_NAME/db/sap/lccluster|$EGREP -c \"$CONFDIR_LIST\"`

# egrep returns 2 for syntax errors or inaccessible files.
if [[ $? -eq 2 ]]; then
	exit 10
fi

if [[ $CNT -eq 1 ]]; then
	exit 0
else
	exit 4
fi
