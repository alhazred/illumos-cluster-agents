#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2002 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#pragma ident	"@(#)lccluster.ksh	1.6	07/06/06 SMI"
#
# This file is called by SAP program 'lcinit' with two parameters, when
# liveCache is started/stopped. lccluster <mode> <state>.
#
# For example, lcinit makes the following lccluster calls:
#	lccluster starting req
#	lccluster starting ok
#	lccluster stopping req
#	lccluster stopping ok
#
# This script will then save the mode and state (the two parameters)
# to a file, which will be used later by HA-liveCache.
#
# The file this script writes to is /sapdb/<LC_NAME>/db/sap/lccluster.out.
#
# Replace "put-LC_NAME-here" with the SAP liveCache instance name (upper case).
# It is the same value as extension property 'Livecache_Name'.
# 
# Replace "put-Confdir_list-here" with the liveCache software and instance
# directory. It has the same value as extension property 'Confdir_list'.
# 

LC_NAME="put-LC_NAME-here"
CONFDIR_LIST="put-Confdir_list-here"

# error out if the directory is not there
if [ ! -d $CONFDIR_LIST/$LC_NAME/db/sap ]; then
	exit 1
fi

# if it's requesting some action, delete the old actions, and save the new one
if [ $2 = "req" ]; then
	echo $1 $2 > $CONFDIR_LIST/$LC_NAME/db/sap/lccluster.out
	rc=$?
else 
# else, just save the result from the action
	echo $1 $2 >> $CONFDIR_LIST/$LC_NAME/db/sap/lccluster.out
	rc=$?
fi

exit $rc
