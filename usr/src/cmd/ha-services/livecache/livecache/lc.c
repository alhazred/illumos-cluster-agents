/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#pragma ident	"@(#)lc.c	1.20	07/06/06 SMI"

/*
 * lc.c - Common utilities for livecache.
 *
 * This utility has the methods for performing the validation, starting and
 * stopping the data service and the fault monitor. It also contains the
 * method to probe the health of the data service.  The probe just returns
 * either success or failure. Action is taken based on this returned value
 * in the method found in the file lc_probe.c
 *
 */

#include "lc.h"
#include <limits.h>

#define	SVC_SMOOTH_PCT		80
#define	SVC_WAIT_TIME		5 /* the time to wait b/t probes */

/*
 * The initial timeout allowed  for the livecache dataservice to
 * be fully up and running. We will wait for 1% (SVC_WAIT_PCT)
 * of the start_timeout time before probing the service.
 */
#define	SVC_WAIT_PCT		1

/*
 * db_state: get the livecache database status calling sap command
 *	su - <lc-amd> -c 'dbmcli -d <LC_NAME> -n <lc-hostname> db_state'
 *
 * Returns:
 *	XSVR_UP (XServer down)
 *	XSVR_DOWN (XServer up)
 * 	LC_NOT_FOUND (wrong LC-NAME)
 * 	LC_ONLINE (LC is WARM)
 *	SYS_ERR (system error)
 *
 */

int
db_state(scds_handle_t scds_handle, lc_extprops_t *lcxpropsp, int timeout)
{

	int 	rc = 0, exit_code = 0;
	char 	db_cmd[SCDS_CMD_SIZE],
		db_cmd_sh[SCDS_CMD_SIZE];
	char	internal_err_str[INT_ERR_MSG_SIZE];


	rc = snprintf(db_cmd, SCDS_CMD_SIZE,
		"%s - %s -c '%s/%s %s/programs/bin/dbmcli %s %s'>/dev/null",
		SU, lcxpropsp->lc_adm, scds_get_rt_rt_basedir(scds_handle),
		CHECK_DB_STATE, lcxpropsp->ext_confdir_list,
		lcxpropsp->ext_lc_name, lcxpropsp->lc_lh);

	if (rc == -1) {
		(void) snprintf(internal_err_str, INT_ERR_MSG_SIZE,
			"Failed to form %s: %s", db_cmd,
			strerror(errno)); /*lint !e746 */
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		return (SYS_ERR);
	} else {
		rc = 0;
	}

	rc = snprintf(db_cmd_sh, SCDS_CMD_SIZE,
		"/usr/bin/sh -c \"%s\" >/dev/null", db_cmd);

	if (rc == -1) {
		(void) snprintf(internal_err_str, INT_ERR_MSG_SIZE,
			"Failed to form %s: %s", db_cmd_sh,
			strerror(errno));
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		return (SYS_ERR);
	} else {
		rc = 0;
	}

	scds_syslog_debug(SCDS_DBG_HIGH, "db cmd = %s", db_cmd);

	rc = scds_timerun(scds_handle, db_cmd, timeout, SIGKILL,
		&exit_code);

	scds_syslog_debug(SCDS_DBG_HIGH, "rc from db_cmd=%d", rc);

	if (rc != 0) {
		if (rc == SCHA_ERR_TIMEOUT) {
			/*
			 * SCMSGS
			 * @explanation
			 * The SAP utility listed timed out.
			 * @user_action
			 * Make sure the logical host name resource is online.
			 */
			scds_syslog(LOG_ERR, "'dbmcli -d <LC_NAME> -n <logical"
				" host name> db_state' timed out.");
		}
		if (rc == SCHA_ERR_NOMEM) {
			scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
				scds_error_string(rc));
		}
		return (SYS_ERR);
	} else if (exit_code == DBMCLI_FAILED) {
		/*
		 * SCMSGS
		 * @explanation
		 * SAP utililty 'dbmcli -d <LC_NAME> -n <logical hostname>
		 * db_state' failed to complete as user <lc-name>adm.
		 * @user_action
		 * Check the SAP liveCache installation and SAP liveCache log
		 * files for reasons that might cause this. Make sure the
		 * cluster nodes are booted up in 64-bit since liveCache only
		 * runs on 64-bit.
		 *
		 * If this error caused the SAP liveCache resource to be in
		 * any error state, use SAP liveCache utility to stop and
		 * dbclean the SAP liveCache database first, before trying to
		 * start it up again.
		 */
		scds_syslog(LOG_ERR, "'dbmcli' failed in command %s.",
			db_cmd_sh);
		return (SYS_ERR);
	}

	scds_syslog_debug(SCDS_DBG_HIGH, "exit-code=%d", exit_code);
	scds_syslog_debug(SCDS_DBG_HIGH, "Done with db_state");
	return (exit_code);
}


/*
 * get_lc_admin: Retrive the admin name for liveCache. It's <lc_name>adm.
 * Caller of this routine needs to free the memory.
 */
char
*get_lc_admin(char *lc_name)
{
	char 	*admuser = NULL;
	char 	adm[4] = "adm";
	char	*p;
	char	internal_err_str[INT_ERR_MSG_SIZE];

	if (lc_name == NULL) {
		(void) snprintf(internal_err_str, INT_ERR_MSG_SIZE,
			"Null value is passed to get_lc_admin()");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		return (admuser);
	} else {
		admuser = calloc(1, strlen(lc_name) + sizeof (adm) + 1);
		if (admuser == NULL) {
			scds_syslog(LOG_ERR, "Out of memory.");
			return (admuser);
		}
		if (strcpy(admuser, lc_name) == NULL) {
			(void) snprintf(internal_err_str, INT_ERR_MSG_SIZE,
				"strcpy failed for user <%s>adm: %s", lc_name,
				strerror(errno));
			scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
				internal_err_str);
			return (admuser);
		}

		for (p = admuser; *p; p++) {
			if (isupper(*p))
				*p = (char)tolower(*p);
		}
		return (strcat(admuser, adm));
	}
}



/* Retrieve one livecache extension property. */
int
get_one_lc_extension(scds_handle_t scds_handle, char *prop_name, char **prop,
	boolean_t print_messages)
{
	scha_extprop_value_t	*extprop = NULL;
	int			rc = 0;

	rc = scds_get_ext_property(scds_handle, prop_name,
		SCHA_PTYPE_STRING, &extprop);
	if (rc != SCHA_ERR_NOERR || extprop == NULL) {
		scds_syslog(LOG_ERR,
			"Failed to retrieve the property %s: %s.",
			prop_name, scds_error_string(rc));
		if (print_messages) {
			(void) fprintf(stderr, gettext("Failed to retrieve "
				"the property %s: %s.\n"), prop_name,
				gettext(scds_error_string(rc)));
		}
		rc = 1;
	}

	if ((*prop = strdup(extprop->val.val_str)) == NULL) {
		scds_syslog(LOG_ERR, "Out of memory.");
		if (print_messages) {
			(void) fprintf(stderr, gettext("Out of memory.\n"));
		}
		rc = 1;
	}

	scds_free_ext_property(extprop);
	return (rc);
}


/* Retrieve the livecache extension properties */
int
get_lc_extension(scds_handle_t scds_handle, lc_extprops_t *lcxpropsp,
	boolean_t print_messages)
{

	int	rc;
	scds_net_resource_list_t	*snrlp = NULL;
	char		internal_err_str[INT_ERR_MSG_SIZE];
	char		state_file[MAXPATHLEN];
	char		lccluster_output_file[MAXPATHLEN];
	char		*lc_admin = NULL;

	if (lcxpropsp == NULL) {
		(void) snprintf(internal_err_str, INT_ERR_MSG_SIZE,
			"Livecache extension property structure was NULL");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		if (print_messages) {
			(void) fprintf(stderr, gettext("INTERNAL ERROR: %s.\n"),
				gettext(internal_err_str));
		}
		return (1);
	}

	/* retrive LCNAME */
	scds_syslog_debug(SCDS_DBG_HIGH, "-- before get LCNAME --");
	if ((rc = get_one_lc_extension(scds_handle, LCNAME,
		&(lcxpropsp->ext_lc_name), print_messages)) != 0) {
		return (1);
	}
	scds_syslog_debug(SCDS_DBG_HIGH, "rc=%d, LCNAME=%s",
		rc, lcxpropsp->ext_lc_name);

	/* retrieve  Confdir_list */
	if ((rc = get_one_lc_extension(scds_handle, CONFDIRLIST,
		&(lcxpropsp->ext_confdir_list), print_messages)) != 0) {
		return (1);
	}
	scds_syslog_debug(SCDS_DBG_HIGH, "rc=%d, confdir list =%s",
		rc, lcxpropsp->ext_confdir_list);

	/* set lccluster output file */
	rc = snprintf(lccluster_output_file, MAXPATHLEN,
		"%s/%s/db/sap/lccluster.out", lcxpropsp->ext_confdir_list,
		lcxpropsp->ext_lc_name);

	if (rc == -1) {
		(void) snprintf(internal_err_str, INT_ERR_MSG_SIZE,
			"Failed to form %s: %s", lccluster_output_file,
			strerror(errno));
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		if (print_messages) {
			(void) fprintf(stderr, gettext("INTERNAL ERROR: %s.\n"),
				gettext(internal_err_str));
		}
		return (1);
	} else {
		rc = 0;
	}

	if ((lcxpropsp->ext_lccluster_file =
		strdup(lccluster_output_file)) == NULL) {

		scds_syslog(LOG_ERR, "Out of memory.");
		if (print_messages) {
			(void) fprintf(stderr, gettext("Out of memory.\n"));
		}
		return (1);
	}

	scds_syslog_debug(SCDS_DBG_HIGH, "rc=%d, lccluster-out-file=%s",
		rc, lcxpropsp->ext_lccluster_file);

	/* set state file */
	rc = snprintf(state_file, MAXPATHLEN,
		"%s/%s/db/wrk/lc.state", lcxpropsp->ext_confdir_list,
		lcxpropsp->ext_lc_name);

	if (rc == -1) {
		(void) snprintf(internal_err_str, INT_ERR_MSG_SIZE,
			"Failed to form %s: %s", state_file,
			strerror(errno));
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		if (print_messages) {
			(void) fprintf(stderr, gettext("INTERNAL ERROR: %s.\n"),
				gettext(internal_err_str));
		}
		return (1);
	} else {
		rc = 0;
	}

	if ((lcxpropsp->ext_lc_state_file = strdup(state_file)) == NULL) {
		scds_syslog(LOG_ERR, "Out of memory.");
		if (print_messages) {
			(void) fprintf(stderr, gettext("Out of memory.\n"));
		}
		return (1);
	}

	scds_syslog_debug(SCDS_DBG_HIGH, "statedir = %s",
		lcxpropsp->ext_lc_state_file);

	scds_syslog_debug(SCDS_DBG_HIGH, "before scds_get_rs_hostnames()");

	/*
	 * Return an error if there is an error when trying to get the
	 * available network address resources for this resource
	 */
	if ((rc = scds_get_rs_hostnames(scds_handle, &snrlp))
		!= SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		    "Error in trying to access the configured network "
		    "resources : %s.", scds_error_string(rc));
		if (print_messages) {
			(void) fprintf(stderr, gettext("Error in trying to "
				"access the configured network resources : "
				"%s.\n"), gettext(scds_error_string(rc)));
		}
		return (1);
	}

	scds_syslog_debug(SCDS_DBG_HIGH, "after scds_get_rs_hostnames");

	/* Return an error if there are no network address resources */
	if (snrlp == NULL || snrlp->num_netresources == 0) {
		scds_syslog(LOG_ERR,
		    "No network address resource in resource group.");
		if (print_messages) {
			(void) fprintf(stderr, gettext("No network address "
				" resource in resource group.\n"));
		}
		return (1);
	}

	scds_syslog_debug(SCDS_DBG_HIGH, "snrlp != null");

	if ((lcxpropsp->lc_lh = strdup(snrlp->netresources->hostnames[0]))
		== NULL) {
		scds_syslog(LOG_ERR, "Out of memory.");
		if (print_messages) {
			(void) fprintf(stderr, gettext("Out of memory.\n"));
		}
		scds_free_net_list(snrlp);
		return (1);
	}

	scds_syslog_debug(SCDS_DBG_HIGH, "lc-lh=%s", lcxpropsp->lc_lh);

	if ((lc_admin = get_lc_admin(lcxpropsp->ext_lc_name)) == NULL) {
		(void) snprintf(internal_err_str, INT_ERR_MSG_SIZE,
			"<lc-name>adm is null.");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		if (print_messages) {
			(void) fprintf(stderr, gettext("INTERNAL ERROR: %s.\n"),
				gettext(internal_err_str));
		}
		scds_free_net_list(snrlp);
		return (1);
	}

	scds_syslog_debug(SCDS_DBG_HIGH, "lc-adm=%s", lc_admin);

	if ((lcxpropsp->lc_adm = strdup(lc_admin)) == NULL) {
		scds_syslog(LOG_ERR, "Out of memory.");
		if (print_messages) {
			(void) fprintf(stderr, gettext("Out of memory.\n"));
		}
		scds_free_net_list(snrlp);
		return (1);
	}

	/* free memory allocated by scds_get_rs_hostnames() */
	scds_free_net_list(snrlp);

	free(lc_admin);
	scds_syslog_debug(SCDS_DBG_HIGH, "done with get_lc_extension");
	return (0);
}


/*
 * Whether the global file says intended stop or not. Assume it's unintended
 * unless it says not.
 * Returns
 * 	INTENDED_STOP
 *	UNINTENDED_STOP
 * 	SYS_ERR
 */
int
read_state_file(char *state_file)
{
	struct stat	statbuf;
	FILE		*fp = NULL;
	int		intended_stop = UNINTENDED_STOP;
	char		internal_err_str[INT_ERR_MSG_SIZE];

	if (stat(state_file, &statbuf) == 0) {
		if ((fp = fopen(state_file, "r")) == NULL) {
			(void) snprintf(internal_err_str, INT_ERR_MSG_SIZE,
				"Failed to open %s: %s",
				state_file, strerror(errno));
			scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
				internal_err_str);
			return (SYS_ERR);
		}
		if (fscanf(fp, "%d", &intended_stop) != 1) {
			(void) snprintf(internal_err_str, INT_ERR_MSG_SIZE,
				"Failed to retrieve value from %s: %s",
				state_file, strerror(errno));
			scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
				internal_err_str);
			intended_stop = SYS_ERR;
		}

		(void) fclose(fp);
		return (intended_stop);

	} else if (errno == ENOENT) {
			return (UNINTENDED_STOP);
		} else {
			return (SYS_ERR);
		}
}



/*
 * svc_validate():
 *
 * Verify the followings:
 * 0) HAStoragePlus check
 * 1) livecache executables are available
 * 2) network address is available
 * 3) check lccluster file
 * 4) other extension properties are set
 * Note, don't check just the livecache instance name since that's checked
 * inexplicitely by checking the livecache scripts lcinit and lccluster.
 */
int
svc_validate(scds_handle_t scds_handle, lc_extprops_t *lcxpropsp,
	boolean_t print_messages)
{
	struct stat	statbuf;
	char		lcinit_cmd[SCDS_CMD_SIZE],
			dbmcli_cmd[SCDS_CMD_SIZE],
			check_lccluster_cmd[SCDS_CMD_SIZE];
	char		*confdir_list;
	int 		rc = 0;
	char		internal_err_str[INT_ERR_MSG_SIZE];
	int		timeout = 0, exit_code = 0;
	int		err;
	scds_hasp_status_t	hasp_status;
	scha_resource_t		local_handle;
	int			saved_errno;

	/*
	 * cannot get the validate timeout if the rs is not created yet
	 * use 120 as timeout in that case.
	 */
	rc = scha_resource_open(scds_get_resource_name(scds_handle),
		scds_get_resource_group_name(scds_handle), &local_handle);

	if (rc != SCHA_ERR_NOERR) {
		if (rc == SCHA_ERR_RSRC) { /* resource not created yet */
			/* reset rc so it won't carry SCHA_ERR_RSRC forward */
			rc = 0;
			timeout = 120;
			scds_syslog_debug(SCDS_DBG_HIGH,
				"use 120 as validate timeout");
		} else {
			scds_syslog(LOG_ERR,
				"Failed to retrieve the resource handle: %s.",
				scds_error_string(rc));
			if (print_messages) {
				(void) fprintf(stderr, gettext("Failed to "
					"retrieve the resource handle: %s.\n"),
					scds_error_string(rc));
			}
			rc = 1;
			goto finished;
		}

	} else {
		rc = scha_resource_get(local_handle, SCHA_VALIDATE_TIMEOUT,
			&timeout);
		if (rc != SCHA_ERR_NOERR) {
			(void) snprintf(internal_err_str,
				sizeof (internal_err_str),
				"Failed to retrieve %s",
				SCHA_VALIDATE_TIMEOUT);
			scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
				internal_err_str);
			if (print_messages) {
				(void) fprintf(stderr,
					gettext("INTERNAL ERROR: %s.\n"),
					gettext(internal_err_str));
			}
			(void) scha_resource_close(local_handle);
			rc = 1;
			goto finished;
		}
	}

	(void) scha_resource_close(local_handle);

	/* check for HAStoragePlus resources */
	err = scds_hasp_check(scds_handle, &hasp_status);

	if (err != SCHA_ERR_NOERR) {
		/* scha_hasp_check() logs msg when it fails */
		rc = 1;
		goto finished;
	}

	if (hasp_status == SCDS_HASP_NOT_ONLINE) {
		scds_syslog(LOG_ERR, "Resource depends on a "
			"SUNW.HAStoragePlus type resource that is "
			"not online anywhere.");
		if (print_messages) {
			(void) fprintf(stderr, gettext("Resource depends on "
				"a SUNW.HAStoragePlus type resource that is "
				"not online anywhere.\n"));
		}
		rc = 1;
		goto finished;
	} else if (hasp_status == SCDS_HASP_ERR_CONFIG) {
		/* problem syslogged by scds_hasp_check */
		if (print_messages) {
			(void) fprintf(stderr, gettext("This resource depends "
				"on a HAStoragePlus resouce that is in a "
				"different Resource Group. This configuration "
				"is not supported.\n"));
		}
		rc = 1;
		goto finished;
	}

	/* don't check on the global stuff if not local. */
	if (hasp_status == SCDS_HASP_ONLINE_NOT_LOCAL) {
		scds_syslog_debug(SCDS_DBG_HIGH, "scha_hasp_check() returns "
			"SCDS_HASP_ONLINE_NOT_LOCAL, rc = %d", rc);

		goto non_global_checks;
	}

	/* check Confdir_list is an absolute path or not */
	confdir_list = lcxpropsp->ext_confdir_list;

	if (confdir_list[0] != '/') {
		scds_syslog(LOG_ERR, "%s is not an absolute path.",
			lcxpropsp->ext_confdir_list);
		if (print_messages) {
			(void) fprintf(stderr, gettext("%s is not an absolute "
				"path.\n"), lcxpropsp->ext_confdir_list);
		}
		rc = 1;
		goto finished;
	}

	/*  check dbmcli */
	rc = snprintf(dbmcli_cmd, sizeof (dbmcli_cmd),
		"%s/programs/bin/dbmcli", lcxpropsp->ext_confdir_list);

	if (rc == -1) {
		(void) snprintf(internal_err_str, INT_ERR_MSG_SIZE,
			"Failed to form %s: %s", dbmcli_cmd,
			strerror(errno));
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		if (print_messages) {
			(void) fprintf(stderr, gettext("INTERNAL ERROR: %s.\n"),
				gettext(internal_err_str));
		}
		rc = 1;
	} else {
		rc = 0;
	}

	if (stat(dbmcli_cmd, &statbuf) != 0) {
		saved_errno = errno;
		/*
		 * SCMSGS
		 * @explanation
		 * HA-SAP failed to access to a file. The file in question is
		 * specified with the first '%s'. The reason it failed is
		 * provided with the second '%s'.
		 * @user_action
		 * Check and make sure the file is accessable via the path
		 * list.
		 */
		scds_syslog(LOG_ERR,
			"%s: %s.", dbmcli_cmd, strerror(saved_errno));
		if (print_messages) {
			(void) fprintf(stderr, gettext("%s: %s.\n"),
				dbmcli_cmd, gettext(strerror(saved_errno)));
		}
		rc = 1;
		goto finished;
	}

	if (!(statbuf.st_mode & S_IXUSR)) {
		scds_syslog(LOG_ERR, "Incorrect permissions set for %s.",
			dbmcli_cmd);
		if (print_messages) {
			(void) fprintf(stderr, gettext("Incorrect permissions "
				"permissions set for %s.\n"), dbmcli_cmd);
		}
		rc = 1;
		goto finished;
	}

	/*  check lcinit */
	rc = snprintf(lcinit_cmd, sizeof (lcinit_cmd),
		"%s/%s/db/sap/lcinit", lcxpropsp->ext_confdir_list,
		lcxpropsp->ext_lc_name);
	if (rc == -1) {
		(void) snprintf(internal_err_str, INT_ERR_MSG_SIZE,
			"Failed to form command %s", lcinit_cmd);
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
			internal_err_str);
		if (print_messages) {
			(void) fprintf(stderr, gettext("INTERNAL ERROR: %s.\n"),
				gettext(internal_err_str));
		}
		rc = 1;
		goto finished;
	} else {
		rc = 0;
	}

	if (stat(lcinit_cmd, &statbuf) != 0) {
		saved_errno = errno;
		scds_syslog(LOG_ERR, "%s: %s.", lcinit_cmd,
			strerror(saved_errno));
		if (print_messages) {
			(void) fprintf(stderr, gettext("%s: %s.\n"),
				lcinit_cmd, gettext(strerror(saved_errno)));
		}
		rc = 1;
		goto finished;
	}

	if (!(statbuf.st_mode & S_IXUSR)) {
		scds_syslog(LOG_ERR, "Incorrect permissions set for %s.",
			lcinit_cmd);

		if (print_messages) {
			(void) fprintf(stderr, gettext("Incorrect "
				"permissions set for %s.\n"), lcinit_cmd);
		}
		rc = 1;
		goto finished;
	}

	/*
	 * check lccluster file.
	 */
	rc = snprintf(check_lccluster_cmd, sizeof (check_lccluster_cmd),
		"%s/%s %s %s > /dev/null", scds_get_rt_rt_basedir(scds_handle),
		CHECK_LCCLUSTER_FILE, lcxpropsp->ext_confdir_list,
		lcxpropsp->ext_lc_name);

	if (rc == -1) {
		(void) snprintf(internal_err_str, sizeof (internal_err_str),
			"Failed to form %s: %s", check_lccluster_cmd,
			strerror(errno));
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
			internal_err_str);
		if (print_messages) {
			(void) fprintf(stderr, gettext("INTERNAL ERROR: %s.\n"),
				gettext(internal_err_str));
		}
		rc = 1;
		goto finished;
	} else {
		rc = 0;
	}

	if ((rc = scds_timerun(scds_handle, check_lccluster_cmd,
		timeout, SIGKILL, &exit_code)) != 0) {
		rc = 1;
		goto finished;
	}

	switch (exit_code) {
		case 0: /* lccluster file is good */
			goto non_global_checks;
		/*
		 * SCMSGS
		 * @explanation
		 * The livecache instance name which is listed in the message
		 * is not defined in the script 'lccluster' which is also
		 * listed in the message.
		 * @user_action
		 * Make sure livecache instance name which is defined in
		 * extension property 'Livecache_Name' is defined in script
		 * lccluster via the macro LC_NAME. See the instructions in
		 * script file lccluster for details.
		 */
		case 1: scds_syslog(LOG_ERR,
			"Livecache instance name %s is not defined via macro"
				" LC_NAME in script %s/%s/db/sap/lccluster.",
				lcxpropsp->ext_lc_name,
				lcxpropsp->ext_confdir_list,
				lcxpropsp->ext_lc_name);
			if (print_messages) {
				(void) fprintf(stderr, gettext("Livecache "
					"instance name %s is not defined via"
					" macro LC_NAME in script "
					"%s/%s/db/sap/lccluster.\n"),
					lcxpropsp->ext_lc_name,
					lcxpropsp->ext_confdir_list,
					lcxpropsp->ext_lc_name);
			}
			break;
		/*
		 * SCMSGS
		 * @explanation
		 * Script 'lccluster' is not found under
		 * /sapdb/<livecache_Name>/db/sap/.
		 * @user_action
		 * Follow the HA-liveCache installation guide to create
		 * 'lccluster'.
		 */
		case 2: scds_syslog(LOG_ERR,
			"Script lccluster does not exist.");
			if (print_messages) {
				(void) fprintf(stderr, gettext("Script "
					"lccluster does not exist.\n"));
			}
			break;
		/*
		 * SCMSGS
		 * @explanation
		 * Script 'lccluster' is not executable.
		 * @user_action
		 * Make sure 'lccluster' is executable.
		 */
		case 3: scds_syslog(LOG_ERR,
			"Script lccluster is not executable.");
			if (print_messages) {
				(void) fprintf(stderr, gettext("Script "
					"lccluster is not executable.\n"));
			}
			break;
		/*
		 * SCMSGS
		 * @explanation
		 * Confdir_list path which is listed in the message is not
		 * defined in the script 'lccluster' which is listed in the
		 * message.
		 * @user_action
		 * Make sure the path for Confdir_list is defined in the
		 * script lccluster using parameter 'CONFDIR_LIST'. The value
		 * should be defined inside the double quotes, and it is the
		 * same as what is defined for extension property
		 * 'Confdir_list'.
		 */
		case 4: scds_syslog(LOG_ERR,
				"Confdir_list %s is not defined via macro "
				"CONFDIR_LIST in script "
				"%s/%s/db/sap/lccluster.",
				lcxpropsp->ext_confdir_list,
				lcxpropsp->ext_confdir_list,
				lcxpropsp->ext_lc_name);

			if (print_messages) {
				(void) fprintf(stderr, gettext("Confdir_list "
					"is not defined correctly in script "
					"lccluster.\n"));
			}
			break;
		default: break;
	}
	rc = 1;
	goto finished;


non_global_checks:

	/* Check to make sure other important extension props are set */
	if (scds_get_ext_monitor_retry_count(scds_handle) <= 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * The resource properties Monitor_retry_count or
		 * Monitor_retry_interval has not set. These properties
		 * control the restarts of the fault monitor.
		 * @user_action
		 * Check whether the properties are set. If not, set these
		 * values using scrgadm(1M).
		 */
		scds_syslog(LOG_ERR,
		"Monitor_retry_count or Monitor_retry_interval is not set.");
		if (print_messages) {
			(void) fprintf(stderr, gettext("Monitor_retry_count "
				"or Monitor_retry_interval is not set.\n"));
		}
		rc = 1; /* Validation Failure */
		goto finished;
	}
	if (scds_get_ext_monitor_retry_interval(scds_handle) <= 0) {
		scds_syslog(LOG_ERR,
		"Monitor_retry_count or Monitor_retry_interval is not set.");
		if (print_messages) {
			(void) fprintf(stderr, gettext("Monitor_retry_count "
				"or Monitor_retry_interval is not set.\n"));
		}
		rc = 1; /* Validation Failure */
		goto finished;
	}

finished:
	scds_syslog_debug(SCDS_DBG_HIGH, "In svc_validate, rc=%d, done now",
		rc);

	return (rc); /* return result of validation */

}


/*
 * Check if the host where this program is executing is healthy.
 * Make sure XServer is running.
 */
int
svc_mon_check(scds_handle_t scds_handle, char *confdir_list)
{
	int	rc = 0, mon_check_timeout;
	char	internal_err_str[INT_ERR_MSG_SIZE];
	scha_resource_t		local_handle;
	char			probe_cmd[SCDS_CMD_SIZE];
	int			exit_code = 0;

retry_open:
	rc = scha_resource_open(scds_get_resource_name(scds_handle),
		scds_get_resource_group_name(scds_handle), &local_handle);

	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
			"Failed to retrieve the resource handle: %s.",
			scds_error_string(rc));
		return (1);
	}

	rc = scha_resource_get(local_handle, SCHA_MONITOR_CHECK_TIMEOUT,
		&mon_check_timeout);

	if (rc == SCHA_ERR_SEQID) {
		/*
		 * SCMSGS
		 * @explanation
		 * An update to cluster configuration occured while resource
		 * properties were being retrieved
		 * @user_action
		 * This is only an informational message.
		 */
		scds_syslog(LOG_INFO, "Retrying to retrieve the resource "
			"information: %s.", scds_error_string(rc));
		(void) scha_resource_close(local_handle);
		local_handle = NULL;
		(void) sleep(1);
		goto retry_open;
	} else if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to retrieve the resource "
			"property %s: %s.", SCHA_MONITOR_CHECK_TIMEOUT,
			scds_error_string(rc));
		(void) scha_resource_close(local_handle);
		return (1);
	}

	/* check whether xserver is running or not */
	rc = snprintf(probe_cmd, SCDS_CMD_SIZE,
		"/usr/bin/sh -c \"%s/programs/bin/dbmcli db_enum\" >/dev/null",
		confdir_list);
	if (rc == -1) {
		(void) snprintf(internal_err_str, INT_ERR_MSG_SIZE,
			"Failed to form %s: %s", probe_cmd, strerror(errno));
		return (1);
	} else {
		rc = 0;
	}

	rc = scds_timerun(scds_handle, probe_cmd, mon_check_timeout, SIGKILL,
		&exit_code);

	if (rc != 0) {
		if (rc == SCHA_ERR_TIMEOUT) {
			/*
			 * SCMSGS
			 * @explanation
			 * Probing the SAP xserver with the listed command
			 * timed out.
			 * @user_action
			 * Other syslog messages occurring just before this
			 * one might indicate the reason for the failure. You
			 * might consider increase the time out value for the
			 * method error was generated from.
			 */
			scds_syslog(LOG_ERR, "Probing SAP xserver timed out "
				"with command %s.", probe_cmd);
		}
		if (rc == SCHA_ERR_NOMEM) {
			scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
				scds_error_string(rc));
		}
		/* system error */
		return (1);
	}

	scds_syslog_debug(SCDS_DBG_HIGH, "db_state=%d", exit_code);

	if (exit_code != 0) {
		/* xserver is not running or other error while checking */
		/*
		 * SCMSGS
		 * @explanation
		 * SAP xserver is not running currently.
		 * @user_action
		 * Informative message, no action is required.
		 */
		scds_syslog(LOG_ERR, "SAP xserver is not available.");
		(void) scha_resource_close(local_handle);
		return (1);
	}

	/* SAP x-server is up and running */
	(void) scha_resource_close(local_handle);
	return (0);
}



/*
 * svc_start():
 * Return 0 if intended stop (w/o restart livecache)
 *		unintended stop and livecache is online already or
 *		unintended stop and livecache is restarted by svc_start()
 *	  1 if system error
 *	  2 if SAP xserver down
 */

int
svc_start(scds_handle_t scds_handle, lc_extprops_t *lcxpropsp)
{
	int 	rc = 0, exit_code = 0, rc_cmd = 0;
	char			db_clear_cmd[SCDS_CMD_SIZE],
				db_start_cmd[SCDS_CMD_SIZE],
				remove_sem[SCDS_CMD_SIZE];
	char			internal_err_str[INT_ERR_MSG_SIZE];
	int			start_timeout;


	scds_syslog_debug(SCDS_DBG_HIGH, "In svc_start");

	rc = read_state_file(lcxpropsp->ext_lc_state_file);

	if (rc == SYS_ERR) {
		return (1);
	} else if (rc == INTENDED_STOP) {
		/* return early if it was stopped intentionally before */
		/*
		 * SCMSGS
		 * @explanation
		 * When Sun Cluster tries to bring up liveCache, it detects
		 * that liveCache was brought down by user intendedly outside
		 * of Sun Cluster. Suu Cluster will not try to restart it
		 * under the control of Sun Cluster until liveCache is started
		 * up successfully again by the user.
		 *
		 * This behaviour is enforced across nodes in the cluster.
		 * @user_action
		 * Informative message. No action is needed.
		 */
		scds_syslog(LOG_NOTICE, "liveCache was stopped by the user "
			"outside of Sun Cluster. Sun Cluster "
			"will suspend monitoring until liveCache is again "
			"started up successfully outside of Sun Cluster.");
		/*
		 * don't set the status here since svc_probe() is called
		 * after svc_start() and it will set the status there.
		 */
		return (0);
	}

	start_timeout = scds_get_rs_start_timeout(scds_handle);

	scds_syslog_debug(SCDS_DBG_HIGH, "In svc_start, before db_state");

	rc = db_state(scds_handle, lcxpropsp, start_timeout);

	scds_syslog_debug(SCDS_DBG_HIGH, "in svc_start, after db_state");

	if (rc == XSVR_DOWN) {
		/*
		 * if xserver is not available, exit from svc_start(),
		 * probe will keep looping until xserver is available, and
		 * then start up liveCache.
		 */
		scds_syslog(LOG_NOTICE, "SAP xserver is not available.");
		(void) scha_resource_setstatus(
			scds_get_resource_name(scds_handle),
			scds_get_resource_group_name(scds_handle),
			SCHA_RSSTATUS_DEGRADED,
			"SAP xserver is not available.");
		/*
		 * need to update ACTION_FILE for this situation. Otherwise,
		 * after xserver is available again, probe will think it is
		 * intended stop since ACTION_FILE has the stopping record
		 * from before.
		 */
		if (call_lccluster(scds_handle, lcxpropsp->lc_adm,
			lcxpropsp->ext_confdir_list,
			lcxpropsp->ext_lc_name, start_timeout) != 0) {
			return (SYS_ERR);
		}
		return (XSVR_DOWN);
	} else if (rc == LC_ONLINE) {
		/* livecache is online already */
		/*
		 * SCMSGS
		 * @explanation
		 * liveCache was started up outside of Sun Cluster when Sun
		 * Cluster tries to start it up. In this case, Sun Cluster
		 * will just put the already started up liveCache under Sun
		 * Cluster's control.
		 * @user_action
		 * Informative message, no action is needed.
		 */
		scds_syslog(LOG_INFO, "liveCache is already online.");
		return (0);
	} else if (rc == SYS_ERR) {
		return (1); /* system error */
	}

	/* If it gets here, SAP xserver is up, livecache is not online */

	/* before starting up livecache, recommended to db_clear it first  */
	rc_cmd = snprintf(db_clear_cmd, SCDS_CMD_SIZE,
	    "%s - %s -c '%s/%s %s %s %s'",
	    SU, lcxpropsp->lc_adm,
	    scds_get_rt_rt_basedir(scds_handle), DBCLEAR,
	    lcxpropsp->ext_confdir_list, lcxpropsp->ext_lc_name,
	    lcxpropsp->lc_lh);
	if (rc_cmd == -1) {
		(void) snprintf(internal_err_str, INT_ERR_MSG_SIZE,
			"Failed to form the %s command.", db_clear_cmd);
			scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
			internal_err_str);
		return (1);
	} else {
		rc_cmd = 0;
	}

	scds_syslog_debug(SCDS_DBG_HIGH, "db-clear-cmd=%s", db_clear_cmd);

	rc = scds_timerun(scds_handle, db_clear_cmd, start_timeout,
		SIGKILL, &exit_code);

	/* if db_clear failed, not sure what to do, will still go ahead ? */
	scds_syslog_debug(SCDS_DBG_HIGH,
		"DB clear command %s return %d exit_code %d",
		db_clear_cmd, rc, exit_code);
	if (rc != 0) {
		if (rc == SCHA_ERR_TIMEOUT) {
			/*
			 * SCMSGS
			 * @explanation
			 * The listed command timed out. Will continue to
			 * start up liveCache.
			 * @user_action
			 * Informative message. HA-liveCache will continue to
			 * start up liveCache. No immediate action is
			 * required. This could be caused by heavy system
			 * load. However, if the system load is not heavy,
			 * user should check the installation and
			 * configuration of liveCache. Make sure the same
			 * listed command can be ran manually on the system.
			 */
			scds_syslog(LOG_ERR, "Command %s timed out. Will "
				"continue to start up liveCache.",
				db_clear_cmd);
		} else {
			/*
			 * SCMSGS
			 * @explanation
			 * The listed command failed to complete. HA-liveCache
			 * will continue to start up liveCache.
			 * @user_action
			 * Look for other syslog error messages on the same
			 * node. Save a copy of the /var/adm/messages files on
			 * all nodes, and report the problem to your
			 * authorized Sun service provider.
			 */
			scds_syslog(LOG_ERR, "Failed to complete command %s. "
			"Will continue to start up liveCache.", db_clear_cmd);
		}
	} else if (exit_code != 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * The listed command failed to complete with the listed
		 * return code. The return code is from the script db_clear.
		 * @user_action
		 * Look for other syslog error messages on the same node. Save
		 * a copy of the /var/adm/messages files on all nodes, and
		 * report the problem to your authorized Sun service provider.
		 */
		scds_syslog(LOG_ERR, "Command %s failed to complete. Return "
			"code is %d.", db_clear_cmd, exit_code);
	}

	/* cleanup directories which holds files named with Semaphores */
	rc = snprintf(remove_sem, SCDS_CMD_SIZE,
		"/bin/rm -rf /usr/spool/sql/ipc/*:%s > /dev/null",
		lcxpropsp->ext_lc_name);
	if (rc == -1) {
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", strerror(errno));
		return (1);
	} else {
		rc = 0;
	}

	scds_syslog_debug(SCDS_DBG_HIGH, "remove_sem =%s", remove_sem);
	rc = scds_timerun(scds_handle, remove_sem, start_timeout,
		SIGKILL, &exit_code);

	if (rc != SCHA_ERR_NOERR) {
		if (rc == SCHA_ERR_TIMEOUT) {
			scds_syslog(LOG_ERR, "Command %s timed out. Will "
				"continue to start up liveCache.",
				remove_sem);
		} else {
			scds_syslog(LOG_ERR, "Failed to complete command %s. "
				"Will continue to start up liveCache.",
				remove_sem);
		}
	} else if (exit_code != 0) {
		scds_syslog(LOG_ERR, "Command %s failed to complete. Return "
			"code is %d.", remove_sem, exit_code);
	}

	/* start LC with lcinit now */
	rc = snprintf(db_start_cmd, SCDS_CMD_SIZE,
		"%s - %s -c '%s/%s/db/sap/lcinit %s restart'",
		SU, lcxpropsp->lc_adm, lcxpropsp->ext_confdir_list,
		lcxpropsp->ext_lc_name, lcxpropsp->ext_lc_name);

	if (rc == -1) {
		(void) snprintf(internal_err_str, INT_ERR_MSG_SIZE,
			"Failed to form %s: %s", db_start_cmd,
			strerror(errno));
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		return (1);
	} else {
		rc = 0;
	}

	scds_syslog(LOG_NOTICE, "Starting %s with command %s.", "liveCache",
	    db_start_cmd);

	rc = scds_timerun(scds_handle, db_start_cmd, start_timeout,
		SIGKILL, &exit_code);

	if (rc != SCHA_ERR_NOERR) {
		if (rc == SCHA_ERR_TIMEOUT) {
			/*
			 * SCMSGS
			 * @explanation
			 * Starting liveCache timed out.
			 * @user_action
			 * Look for syslog error messages on the same node.
			 * Save a copy of the /var/adm/messages files on all
			 * nodes, and report the problem to your authorized
			 * Sun service provider.
			 */
			scds_syslog(LOG_ERR,
				"Starting liveCache timed out with command %s.",
				db_start_cmd);
			return (1);
		} else {
			(void) snprintf(internal_err_str, INT_ERR_MSG_SIZE,
				"Failed to start liveCache with command %s.",
				db_start_cmd);
			scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
				internal_err_str);
			return (1);
		}
	} else if (exit_code != 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * Starting liveCache failed.
		 * @user_action
		 * Check SAP liveCache log files and also look for syslog
		 * error messages on the same node for potential errors.
		 */
		scds_syslog(LOG_ERR, "Starting liveCache with command %s"
			" failed. Return code is %d.",
			db_start_cmd, exit_code);
		return (1);
	}

	scds_syslog_debug(SCDS_DBG_HIGH, "In svc_start, done now.");
	return (0);
}


int
call_lccluster(scds_handle_t scds_handle, char *lc_adm, char *confdir,
char *lc_name, int start_timeout)
{
	char	call_lccluster_cmd[SCDS_CMD_SIZE];
	char	starting[9] = "starting",
		request[4] = "req";
	char	internal_err_str[INT_ERR_MSG_SIZE];
	int	rc = 0, exit_code = 0;

	rc = snprintf(call_lccluster_cmd, sizeof (call_lccluster_cmd),
		"%s - %s -c '%s/%s/db/sap/lccluster %s %s'>dev/null",
		SU, lc_adm, confdir, lc_name, starting, request);
	if (rc == -1) {
		(void) snprintf(internal_err_str, INT_ERR_MSG_SIZE,
			"Failed to form %s: %s", call_lccluster_cmd,
			strerror(errno));
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		return (1);
	}

	scds_syslog_debug(SCDS_DBG_HIGH, "call_lccluster = %s",
		call_lccluster_cmd);

	rc = scds_timerun(scds_handle, call_lccluster_cmd, start_timeout,
		SIGKILL, &exit_code);
	if (rc != SCHA_ERR_NOERR) {
		if (rc == SCHA_ERR_TIMEOUT) {
			scds_syslog(LOG_ERR,
				"Starting liveCache timed out with command %s.",
				call_lccluster_cmd);
			return (1);
		}
	} else if (exit_code != 0) {
		scds_syslog(LOG_ERR, "Starting liveCache with command %s"
			" failed. Return code is %d.",
			call_lccluster_cmd, exit_code);
		return (1);
	}
	return (0);
}



/*
 * svc_wait():
 *
 * Waits for the data service is at a final state (either it's started up
 * fully or user stopped it outside of Sun Cluster).
 */
int
svc_wait(scds_handle_t scds_handle, lc_extprops_t *lcxpropsp)
{
	int	start_timeout, rc;

	start_timeout = scds_get_rs_start_timeout(scds_handle);
	if (scds_svc_wait(scds_handle, (start_timeout * SVC_WAIT_PCT)/100) !=
		SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * Specified data service failed to start.
		 * @user_action
		 * Look in /var/adm/messages for the cause of failure. Save a
		 * copy of the /var/adm/messages files on all nodes. Contact
		 * your authorized Sun service provider for assistance in
		 * diagnosing the problem.
		 */
		scds_syslog(LOG_ERR, "Failed to start the service %s.",
			"SAP liveCache");
		return (1);
	}

	for (;;) {
		rc = svc_probe(scds_handle, lcxpropsp, start_timeout);
		if (rc == 0) {
			scds_syslog(LOG_NOTICE, "Service is online.");
			return (0);
		} else if (rc == SCDS_PROBE_IMMEDIATE_FAILOVER) {
			return (0);
		}
		scds_syslog(LOG_NOTICE, "Waiting for %s to come up.",
		    "liveCache");
		(void) scds_svc_wait(scds_handle, SVC_WAIT_TIME);
	/* We rely on RGM to timeout and terminate the program */
	}
}


/*
 * Check on:
 * 	liveCache database status
 *	if ONLINE
 *		return 0
 *	else
 *		check and see it's intended stop or not
 *
 * Return:
 *	SCDS_PROBE_COMPLETE_FAILURE (unintended stop)
 *	SCDS_PROBE_IMMEDIATE_FAILOVER (start error)
 *	SCDS_PROBE_COMPLETE_FAILURE/4 (system error)
 *	0	(intended stop, online, SAP xserver problem)
 */
int
svc_probe(scds_handle_t handle, lc_extprops_t *lcxpropsp, int timeout)
{
	int	rc = 0, db_rc = 0;
	static int	old_db_state = -1;

	scds_syslog_debug(SCDS_DBG_HIGH, "In svc_probe");

	/* db status check */
	db_rc = db_state(handle, lcxpropsp, timeout);

	scds_syslog_debug(SCDS_DBG_HIGH,
		"In probe,db_state returns %d", db_rc);

	if (db_rc == LC_ONLINE) {
		scds_syslog_debug(SCDS_DBG_HIGH,
			"DB is online. old_db_state=%d, db_rc=%d",
			old_db_state, db_rc);
		if (db_rc != old_db_state) {
			/* reset global file */
			(void) update_intended_stop(
				lcxpropsp->ext_lc_state_file,
				UNINTENDED_STOP);
			(void) scha_resource_setstatus(
				scds_get_resource_name(handle),
				scds_get_resource_group_name(handle),
				SCHA_RSSTATUS_OK, "Completed sucessfully.");
			old_db_state = db_rc;
		}
		return (0);
	} else if (db_rc == PARENT_DIE) {
		/* liveCache parent died */
		return (SCDS_PROBE_COMPLETE_FAILURE);
	}

	/* DB not online, check whether intended stop or not  */
	rc = user_intended_stop(handle, lcxpropsp, timeout);
	scds_syslog_debug(SCDS_DBG_HIGH, "after user-intended return %d", rc);

	switch (rc) {
		case INTENDED_STOP:
			scds_syslog_debug(SCDS_DBG_HIGH, "intended stop"
				"old_db_state=%d", old_db_state);

			if (rc != old_db_state) {
				(void) scha_resource_setstatus(
					scds_get_resource_name(handle),
					scds_get_resource_group_name(handle),
					SCHA_RSSTATUS_UNKNOWN, "liveCache was"
					" brought down outside of Sun "
					"Cluster.");
				if (update_intended_stop(
					lcxpropsp->ext_lc_state_file,
					INTENDED_STOP) != 0) {
					return (SCDS_PROBE_COMPLETE_FAILURE/4);
				}
				old_db_state = rc;
			}
			return (SCHA_ERR_NOERR);
		case START_ERR:
			return (SCDS_PROBE_IMMEDIATE_FAILOVER);
		case UNINTENDED_STOP:
			scds_syslog_debug(SCDS_DBG_HIGH, "unintend stop. "
				"old_db_state=%d, db_rc=%d",
				old_db_state, db_rc);

			switch (db_rc) {
			    case XSVR_DOWN:
				if (db_rc != old_db_state) {
				    (void) scha_resource_setstatus(
				    scds_get_resource_name(handle),
				    scds_get_resource_group_name(handle),
				    SCHA_RSSTATUS_DEGRADED,
				    "SAP xserver is not available.");
					old_db_state = db_rc;
				}
				return (0);
			    case XSVR_UP:
				if (db_rc != old_db_state) {
				    (void) scha_resource_setstatus(
					scds_get_resource_name(handle),
					scds_get_resource_group_name(handle),
					SCHA_RSSTATUS_OK,
					"SAP xserver is up.");
					old_db_state = db_rc;
				}
				return (SCDS_PROBE_COMPLETE_FAILURE);
			    default: /* unintended stop */
				return (SCDS_PROBE_COMPLETE_FAILURE);
			}
		default: /* system error */
			return (SCDS_PROBE_COMPLETE_FAILURE/4);
	}
}


/*
 * user_intended_stop()
 * Finds out user stopped LC intendedly or not.
 * Returns  INTENDED_STOP (Can be detected from the global state file
 *			   which means it was stopped before,
 *			   or the user stopped it this time, which
 *			   is detected via the action file)
 *	    UNINTENDED_STOP (LC wasn't stopped outside of Sun Cluster by user)
 *	    START_ERR	(start error)
 *	    SYS_ERR	(system error)
 */
int
user_intended_stop(scds_handle_t scds_handle, lc_extprops_t *lcxpropsp,
int probe_timeout)
{
	int	intended_stop = UNINTENDED_STOP, rc = 0;
	char	internal_err_str[INT_ERR_MSG_SIZE];
	struct stat 	actionbuf;
	char		parse_cmd[SCDS_CMD_SIZE];
	int		exit_code;

	/* check global state file */
	intended_stop = read_state_file(lcxpropsp->ext_lc_state_file);

	if (intended_stop != UNINTENDED_STOP) {
		/* for INTENDED_STOP or SYS_ERR */
		return (intended_stop);
	}

	/*
	 * It's not intended stop from before, now need to check
	 * whether any action on LC is taken after START started it up
	 */

	/* check lccluster output file */
	if (stat(lcxpropsp->ext_lccluster_file, &actionbuf) != 0) {
		/* START should have created this file already, problem */
		scds_syslog(LOG_ERR, "%s: %s.", lcxpropsp->ext_lccluster_file,
			strerror(errno));
		return (START_ERR);
	}

	rc = snprintf(parse_cmd, SCDS_CMD_SIZE, "%s/%s %s>/dev/null",
			scds_get_rt_rt_basedir(scds_handle),
			PARSE_ACTION_FILE, lcxpropsp->ext_lccluster_file);
	if (rc == -1) {
		(void) snprintf(internal_err_str, INT_ERR_MSG_SIZE,
			"Failed to form %s: %s", parse_cmd,
			strerror(errno));
		return (SYS_ERR);
	} else {
		rc = 0;
	}

	if ((rc = scds_timerun(scds_handle, parse_cmd, probe_timeout,
		SIGKILL, &exit_code)) != 0) {
		return (SYS_ERR);
	}

	scds_syslog_debug(SCDS_DBG_HIGH, "parse-cmd = %s exit_code=%d.",
		parse_cmd, exit_code);

	switch (exit_code) {
		case 1: /* intended stop */
			scds_syslog_debug(SCDS_DBG_HIGH,
				"Intended stop NOW.");
			/*
			 * SCMSGS
			 * @explanation
			 * LiveCache fault monitor detects that liveCache was
			 * brought down by user intendedly outside of Sun
			 * Cluster. Suu Cluster will not take any action upon
			 * it until liveCache is started up successfully again
			 * by the user.
			 * @user_action
			 * No action is needed if the shutdown is intended. If
			 * not, start up liveCache again using LC10 or lcinit,
			 * so it can be under the monitoring of Sun Cluster.
			 */
			scds_syslog(LOG_INFO,
				"liveCache %s was brought down "
				"outside of Sun Cluster. Sun Cluster will "
				"suspend monitoring for it until it is "
				"started up successfully again by the user.",
				lcxpropsp->ext_lc_name);
			return (INTENDED_STOP);
		case 2: /* start error */
			/*
			 * SCMSGS
			 * @explanation
			 * liveCache started up with error.
			 * @user_action
			 * Sun Cluster will fail over the liveCache resource
			 * to another available node. No user action is
			 * needed.
			 */
			scds_syslog(LOG_ERR, "liveCache %s failed to start.",
				lcxpropsp->ext_lc_name);
			return (START_ERR);
		default: /* unintended stop */
				return (UNINTENDED_STOP);
	}
}


/*
 * Update the state file (/sapdb/<LC_NAME>/db/wrk/lc.state) with the new status.
 */
int
update_intended_stop(char *state_file, int intended)
{
	char	internal_err_str[INT_ERR_MSG_SIZE];
	FILE	*fp;

	scds_syslog_debug(SCDS_DBG_HIGH, "In update-intended-stop, will "
		"update file %s with %d", state_file, intended);

	/* update state */
	if ((fp = fopen(state_file, "w")) == NULL) {
		(void) snprintf(internal_err_str, INT_ERR_MSG_SIZE,
			"Failed to open file %s: %s", state_file,
			strerror(errno));
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		return (1);
	}

	if (fprintf(fp, "%d", intended) < 0) {
		(void) snprintf(internal_err_str, INT_ERR_MSG_SIZE,
			"Failed to write to %s: %s", state_file,
			strerror(errno));
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		(void) fclose(fp);
		return (1);
	}

	(void) fclose(fp);
	return (0);
}


/*
 * svc_stop():
 *
 * Stop the liveCache database.
 * Return 0 on success, > 0 on failures.
 *
 */
int
svc_stop(scds_handle_t scds_handle, lc_extprops_t *lcxpropsp)
{
	int rc = 0, exit_code = 0;
	char 	soft_stop_cmd[SCDS_CMD_SIZE],
		hard_stop_cmd[SCDS_CMD_SIZE];
	int 	stop_smooth_timeout = 0, stop_timeout = 0;
	char	internal_err_str[INT_ERR_MSG_SIZE];


	stop_timeout = scds_get_rs_stop_timeout(scds_handle);
	stop_smooth_timeout = (stop_timeout * SVC_SMOOTH_PCT) / 100;

	/* set it to 2 seconds at least */
	if (stop_smooth_timeout < 2) {
		stop_smooth_timeout = 2;
	}

	rc = snprintf(soft_stop_cmd, SCDS_CMD_SIZE,
		"%s - %s -c '%s/%s/db/sap/lcinit %s stop'",
		SU, lcxpropsp->lc_adm, lcxpropsp->ext_confdir_list,
		lcxpropsp->ext_lc_name, lcxpropsp->ext_lc_name);
	if (rc == -1) {
		(void) snprintf(internal_err_str, INT_ERR_MSG_SIZE,
			"Failed to form %s.", soft_stop_cmd);
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
			internal_err_str);
		return (1);
	} else {
		rc = 0;
	}

	scds_syslog(LOG_NOTICE, "Stopping %s.", "liveCache");
	rc = scds_timerun(scds_handle, soft_stop_cmd, stop_smooth_timeout,
		SIGKILL, &exit_code);

	scds_syslog_debug(SCDS_DBG_HIGH, "soft stop = %s, rc=%d, exit_code=%d",
		soft_stop_cmd, rc, exit_code);

	if ((rc != 0) || (exit_code != 0)) {
		/*
		 * SCMSGS
		 * @explanation
		 * Failed to stop liveCache with 'lcinit stop'. Will shutdown
		 * immediately with 'dbmcli db_stop'.
		 * @user_action
		 * Informative message. No user action is needed.
		 */
		scds_syslog(LOG_ERR, "Failed to stop liveCache gracefully with "
			"command %s. Will stop it immediately with db_stop.",
			soft_stop_cmd);

		rc = snprintf(hard_stop_cmd, SCDS_CMD_SIZE,
		    "%s - %s -c '%s/programs/bin/dbmcli -d %s -n %s db_stop'",
		    SU, lcxpropsp->lc_adm, lcxpropsp->ext_confdir_list,
		    lcxpropsp->ext_lc_name, lcxpropsp->lc_lh);
		if (rc == -1) {
			(void) snprintf(internal_err_str, INT_ERR_MSG_SIZE,
				"Failed to form %s.", soft_stop_cmd);
			scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
				internal_err_str);
			return (1);
		} else {
			rc = 0;
		}

		/* scds_timerun doesnt take INT_MAX, so we divide it by 4 */
		rc = scds_timerun(scds_handle, hard_stop_cmd,
			INT_MAX >> 2, SIGKILL, &exit_code);
		if (rc != SCHA_ERR_NOERR) {
			if (rc == SCHA_ERR_TIMEOUT) { /* shouldnt happen */
				/*
				 * SCMSGS
				 * @explanation
				 * Stopping liveCache timed out.
				 * @user_action
				 * Look for syslog error messages on the same
				 * node. Save a copy of the /var/adm/messages
				 * files on all nodes, and report the problem
				 * to your authorized Sun service provider.
				 */
				scds_syslog(LOG_ERR,
					"Stopping liveCache times out with "
					"command %s.", hard_stop_cmd);
				return (1);
			} else {
				/*
				 * SCMSGS
				 * @explanation
				 * Stopping SAP liveCache with the specified
				 * command failed to complete.
				 * @user_action
				 * Look for other syslog error messages on the
				 * same node. Save a copy of the
				 * /var/adm/messages files on all nodes, and
				 * report the problem to your authorized Sun
				 * service provider.
				 */
				scds_syslog(LOG_ERR, "Failed to shutdown "
					"liveCache immediately with command "
					"%s.", hard_stop_cmd);
				scds_syslog_debug(SCDS_DBG_HIGH,
					"db_stop failed, return code from "
					"scds_timerun is %d.", rc);
				return (1);
			}
		} else if (exit_code != 0) {
			/* hard stop failed, xserver could be down */
			/*
			 * SCMSGS
			 * @explanation
			 * Failed to shutdown liveCache immediately with
			 * listed command. The return code from the SAP
			 * command is listed.
			 * @user_action
			 * Check the return code for dbmcli command for
			 * reasons of failure. Also, check the SAP log files
			 * for more details.
			 */
			scds_syslog(LOG_ERR, "Failed to stop liveCache"
				" with command %s. Return code from SAP "
				"command is %d.",
				hard_stop_cmd, exit_code);
			return (exit_code);
		}
	}

	if (rc == 0) {
		scds_syslog(LOG_NOTICE, "Successfully stopped %s.",
		    "resource");
	}

	return (rc);
}


/*
 * This function starts the fault monitor for a livecache resource.
 * This is done by starting the probe under PMF. The PMF tag
 * is derived as <RG-name,RS-name,instance_number.mon>. The restart option
 * of PMF is used but not the "infinite restart". Instead
 * interval/retry_time is obtained from the RTR file.
 */

int
mon_start(scds_handle_t scds_handle)
{
	scha_err_t	err;

	scds_syslog_debug(SCDS_DBG_HIGH,
	    "Calling MONITOR_START method for resource <%s>.",
	    scds_get_resource_name(scds_handle));

	/*
	 * The probe lc_probe is assumed to be available in the same
	 * subdirectory where the other callback methods for the RT are
	 * installed. The last parameter to scds_pmf_start denotes the
	 * child monitor level. Since we are starting the probe under PMF
	 * we need to monitor the probe process only and hence we are using
	 * a value of 0.
	 */

	err = scds_pmf_start(scds_handle, SCDS_PMF_TYPE_MON,
	    SCDS_PMF_SINGLE_INSTANCE, "lc_probe", 0);

	if (err != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to start fault monitor.");
		return (err);
	}

	scds_syslog(LOG_INFO, "Started the fault monitor.");
	return (SCHA_ERR_NOERR); /* Successfully started Monitor */
}


/*
 * This function stops the fault monitor for a livecache resource.
 * This is done via PMF. The PMF tag for the fault monitor is
 * constructed based on <RG-name_RS-name,instance_number.mon>.
 */

int
mon_stop(scds_handle_t scds_handle)
{
	scds_syslog_debug(SCDS_DBG_HIGH, "Calling scds_pmf_stop method");
	if (scds_pmf_stop(scds_handle, SCDS_PMF_TYPE_MON, 0, SIGKILL, -1) !=
	    SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to stop fault monitor.");
		return (1);
	}

	scds_syslog(LOG_INFO, "Stopped the fault monitor.");

	return (SCHA_ERR_NOERR);
}

void
free_lc_property(lc_extprops_t *lcxpropsp)
{
	if (lcxpropsp == NULL)
		return;

	free(lcxpropsp->ext_confdir_list);
	free(lcxpropsp->ext_lc_name);
	free(lcxpropsp->ext_lccluster_file);
	free(lcxpropsp->ext_lc_state_file);
	free(lcxpropsp->lc_lh);
	free(lcxpropsp->lc_adm);

	free(lcxpropsp);
}
