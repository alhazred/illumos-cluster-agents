/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2002 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * lc_svc_start.c - Start method for liveCache
 */

#pragma ident	"@(#)lc_svc_start.c	1.6	07/06/06 SMI"

#include "lc.h"

/*
 * The start method for livecache. Does some sanity checks on
 * the resource settings then starts livecache using sap utility lcinit.
 * Check on livecache right after svc_start() since user can use LC10 to
 * stop it even when ha-livecache is trying to start it. In that case, we
 * should exit 0.
 */

int
main(int argc, char *argv[])
{
	scds_handle_t	scds_handle;
	int rc;
	lc_extprops_t	lcxpropsp;

	/*
	 * Process all the arguments that have been passed to us from RGM
	 * and do some initialization for syslog
	 */

	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR) {
		return (1);
	}

	if (get_lc_extension(scds_handle, &lcxpropsp, B_FALSE) != 0) {
		scds_close(&scds_handle);
		return (1);
	}

	/* Validate the configuration and if there is an error return back */
	rc = svc_validate(scds_handle, &lcxpropsp, B_FALSE);
	if (rc != 0) {
		scds_syslog(LOG_ERR,
		    "Failed to validate configuration.");
		goto finished;
	}

	/* Start the data service, if it fails return with an error */
	rc = svc_start(scds_handle, &lcxpropsp);

	scds_syslog_debug(SCDS_DBG_HIGH,
		"In START, svc_start() returns %d", rc);

	if (rc != 0) {
		/*
		 * if SAP xserver is down, set rc = 0 so start will succeed,
		 * and probe will keep polling it until xserver is available.
		 */
		if (rc == XSVR_DOWN) {
			rc = 0;
		}

		goto finished;
	}

	/* Wait for the service to come to a final state */
	rc = svc_wait(scds_handle, &lcxpropsp);
	scds_syslog_debug(SCDS_DBG_HIGH,
		"Returned from svc_wait rc=%d.", rc);

	if (rc == SCHA_ERR_NOERR) {
		scds_syslog(LOG_INFO, "Completed successfully.");
	} else {
		scds_syslog(LOG_ERR, "Failed to start %s.", "SAP liveCache");
	}

finished:
	/* free memory allocated by get_lc_extension */
	free_lc_property(&lcxpropsp);

	/* Free up the Environment resources that were allocated */
	scds_syslog_debug(SCDS_DBG_HIGH, "In START, done now.");
	scds_close(&scds_handle);
	return (rc);
}
