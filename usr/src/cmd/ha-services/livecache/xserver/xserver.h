/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2002-2003 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_XSERVER_H
#define	_XSERVER_H

#pragma ident	"@(#)xserver.h	1.8	07/06/06 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <scha.h>
#include <sys/time.h>
#include <signal.h>
#include <rgm/libdsdev.h>
#include <errno.h>
#include <synch.h>
#include <pwd.h>
#include <libgen.h>
#include <sys/wait.h>
#include <sys/utsname.h>
#include <unistd.h>
#include <dlfcn.h>
#include <ctype.h>
#include <locale.h>
#include <libintl.h>
#include <sys/systeminfo.h>
#include <project.h>

#define	INT_ERR_MSG_SIZE	1024
#define	SCDS_CMD_SIZE		(8 * 1024)
#define	SCDS_DBG_HIGH		9

/* executables */
#define	KILL_PROCESS		"kill_process"

/* extension property */
#define	INDEPPROGPATH		"Independent_Program_Path"
#define	CONFDIRLIST		"Confdir_list"
#define	SOFTSTOPPCT		"Soft_Stop_Pct"
#define	XSERVERUSER		"Xserver_User"

/* executables */
#define	XSERVER			"x_server"
#define	STOP			"stop"
#define	START			"start"
#define	DBMCLI			"dbmcli"
#define	NEWTASK			"/usr/bin/newtask"
#define	DEFAULT_PROJ		"default"	/* default project value */
#define	APP_NAME		"SAP xserver"

/* extension property */
typedef struct xsvr_extprops
{
	char	*ext_confdir_list;
	char	*ext_indep_prog_path;
	char	*ext_xsvr_user;
	int	ext_stop_pct;
	char	*proj_name;
} xsvr_extprops_t;

int svc_validate(scds_handle_t scds_handle, xsvr_extprops_t *xsvrpropsp,
	boolean_t print_messages);

int svc_start(scds_handle_t scds_handle, xsvr_extprops_t *xsvrpropsp);

int svc_stop(scds_handle_t scds_handle, xsvr_extprops_t *xsvrpropsp);

int svc_wait(scds_handle_t scds_handle, xsvr_extprops_t *xsvrpropsp);

int mon_start(scds_handle_t scds_handle);

int mon_stop(scds_handle_t scds_handle);

int
svc_probe(scds_handle_t scds_handle, xsvr_extprops_t *xsvrpropsp,
	int timeout, boolean_t print_msgs);

int
get_xserver_extension(scds_handle_t scds_handle, xsvr_extprops_t *xsvrpropsp,
	boolean_t print_messages);

void free_xsvr_property(xsvr_extprops_t *xsvrpropsp);

int get_project_name(scds_handle_t scds_handle, xsvr_extprops_t *xsvrpropsp);

void validate_proj_user(xsvr_extprops_t *xsvrpropsp);

#ifdef __cplusplus
}
#endif

#endif	/* _XSERVER_H */
