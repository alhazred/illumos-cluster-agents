/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)xserver.c	1.13	07/06/06 SMI"

/*
 * xserver.c - Common utilities for SAP xserver.
 *
 * This utility has the methods for performing the validation, starting and
 * stopping the data service and the fault monitor. It also contains the
 * method to probe the health of the data service.  The probe just returns
 * either success or failure. Action is taken based on this returned value
 * in the method found in the file xserver_probe.c
 *
 */
#include <limits.h>
#include <ds_common.h>
#include "xserver.h"

/*
 * The initial timeout allowed  for the xserver dataservice to
 * be fully up and running. We will wait for for 3 % (SVC_WAIT_PCT)
 * of the start_timeout time before probing the service.
 */
#define	SVC_WAIT_PCT		3

/*
 * We need to wait for SVC_WAIT_TIME ( 2 secs) for pmf
 * to send the failure message before probing the service
 */

#define	SVC_WAIT_TIME		2

/*
 * get_xserver_extension
 */
int
get_xserver_extension(scds_handle_t scds_handle, xsvr_extprops_t *xsvrpropsp,
	boolean_t print_messages)
{
	int	rc = 0;
	scha_extprop_value_t	*ext_prop = NULL;
	char	*bin_path = NULL;

	if (xsvrpropsp == NULL) {
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
			"SAP xserver extension property structure was null");
		if (print_messages) {
			(void) fprintf(stderr, gettext("INTERNAL ERROR: %s.\n"),
				gettext("SAP xserver extension property "
				"structure was null\n"));
		}
		return (1);
	}

	/* get Livecache_dir */
	rc = scds_get_ext_property(scds_handle, INDEPPROGPATH,
		SCHA_PTYPE_STRING, &ext_prop);
	if ((rc != SCHA_ERR_NOERR) && (rc != SCHA_ERR_PROP)) {
		scds_syslog(LOG_ERR,
			"Failed to retrieve the property %s: %s.",
			INDEPPROGPATH, scds_error_string(rc));
		if (print_messages) {
			(void) fprintf(stderr, gettext("Failed to retrieve "
				"the property %s: %s.\n"), INDEPPROGPATH,
				scds_error_string(rc));
		}
		return (1);
	}

	/*
	 * set ext_confdir_list to the value from Independent_Program_path
	 * if it's set, otherwise, set it to the value from Confdir_List.
	 */
	if ((ext_prop != NULL) && (ext_prop->val.val_str[0] != '\0')) {
		scds_syslog_debug(SCDS_DBG_HIGH, "set ext_confdir-list= %s.",
			INDEPPROGPATH);
		if ((xsvrpropsp->ext_confdir_list =
			strdup(ext_prop->val.val_str)) == NULL) {
			scds_syslog(LOG_ERR, "Out of memory.");
			if (print_messages) {
				(void) fprintf(stderr,
					gettext("Out of memory.\n"));
			}
			scds_free_ext_property(ext_prop);
			return (1);
		}
		scds_syslog_debug(SCDS_DBG_HIGH,
			"confdir_list(indep_prog-path) =%s",
			xsvrpropsp->ext_confdir_list);
		scds_free_ext_property(ext_prop);
		ext_prop = NULL;
	} else {
		rc = scds_get_ext_property(scds_handle, CONFDIRLIST,
			SCHA_PTYPE_STRING, &ext_prop);
		if (rc != SCHA_ERR_NOERR || ext_prop == NULL) {
			scds_syslog(LOG_ERR,
				"Failed to retrieve the property %s: %s.",
				CONFDIRLIST, scds_error_string(rc));
			if (print_messages) {
				(void) fprintf(stderr,
					gettext("Failed to retrieve "
					"the property %s: %s.\n"), CONFDIRLIST,
					scds_error_string(rc));
			}
			return (1);
		}

		/* allocate memory for "<confdir_list>/programs" */
		bin_path = malloc(strlen(ext_prop->val.val_str) + 10);
		if (bin_path == NULL) {
			scds_syslog(LOG_ERR, "Out of memory.");
			if (print_messages) {
				(void) fprintf(stderr,
					gettext("Out of memory.\n"));
			}
			scds_free_ext_property(ext_prop);
			return (1);
		}
		(void) strcpy(bin_path, ext_prop->val.val_str);

		/*
		 * Need to append 'programs' to the end since that was the
		 * old code used to do.
		 */
		(void) strcat(bin_path, "/programs");

		if ((xsvrpropsp->ext_confdir_list =
			strdup(bin_path)) == NULL) {
			scds_syslog(LOG_ERR, "Out of memory.");
			if (print_messages) {
				(void) fprintf(stderr,
					gettext("Out of memory.\n"));
			}
			free(bin_path);
			scds_free_ext_property(ext_prop);
			return (1);
		}

		scds_syslog_debug(SCDS_DBG_HIGH,
			"confdir_list (confdir-list)=%s",
			xsvrpropsp->ext_confdir_list);

		scds_free_ext_property(ext_prop);
		ext_prop = NULL;
	}

	/* get Xserver_User */
	rc = scds_get_ext_property(scds_handle, XSERVERUSER,
		SCHA_PTYPE_STRING, &ext_prop);
	if (rc != SCHA_ERR_NOERR || ext_prop == NULL) {
		scds_syslog(LOG_ERR,
			"Failed to retrieve the property %s: %s.",
			XSERVERUSER, scds_error_string(rc));
		if (print_messages) {
			(void) fprintf(stderr, gettext("Failed to retrieve "
				"the property %s: %s.\n"), XSERVERUSER,
				gettext(scds_error_string(rc)));
		}
		return (1);
	}

	if ((xsvrpropsp->ext_xsvr_user = strdup(ext_prop->val.val_str))
		== NULL) {
		scds_syslog(LOG_ERR, "Out of memory.");
		if (print_messages) {
			(void) fprintf(stderr, gettext("Out of memory.\n"));
		}
		scds_free_ext_property(ext_prop);
		return (1);
	}
	scds_syslog_debug(SCDS_DBG_HIGH, "xsvr-user=%s",
		xsvrpropsp->ext_xsvr_user);

	scds_free_ext_property(ext_prop);
	ext_prop = NULL;

	/* get Soft_Stop_Pct */
	if ((rc = scds_get_ext_property(scds_handle, SOFTSTOPPCT,
		SCHA_PTYPE_INT, &ext_prop)) != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
			"Failed to retrieve the property %s: %s.",
			SOFTSTOPPCT, scds_error_string(rc));
		if (print_messages) {
			(void) fprintf(stderr, gettext("Failed to retrieve "
				"the property %s: %s.\n"), SOFTSTOPPCT,
				gettext(scds_error_string(rc)));
		}
		return (1);
	}
	xsvrpropsp->ext_stop_pct = ext_prop->val.val_int;
	scds_syslog_debug(SCDS_DBG_HIGH, "soft-stop-pct=%d",
		xsvrpropsp->ext_stop_pct);
	scds_free_ext_property(ext_prop);
	ext_prop = NULL;
	return (0);
}



/*
 * svc_validate():
 *
 * Do SAP xserver specific validation of the resource configration.
 *
 */

int
svc_validate(scds_handle_t scds_handle, xsvr_extprops_t *xsvrpropsp,
	boolean_t print_messages)
{
	struct stat statbuf;
	char xserver_cmd_prog[SCDS_CMD_SIZE];
	char dbmcli_cmd_prog[SCDS_CMD_SIZE];
	int rc = 0;
	char internal_err_str[INT_ERR_MSG_SIZE];
	scds_hasp_status_t hasp_status;
	int	err;
	struct passwd	*xsvruserpwd;
	struct passwd	pwd;
	char		buf[MAXPATHLEN];
	char		*confdir_list;
	int		saved_errno;

	scds_syslog_debug(SCDS_DBG_HIGH, "In svc_validate.");

	/* check for HAStoragePlus resources */
	err = scds_hasp_check(scds_handle, &hasp_status);

	scds_syslog_debug(SCDS_DBG_HIGH, "After scds_hasp_check().");

	if (err != SCHA_ERR_NOERR) {
		/* scha_hasp_check() logs msg when it fails */
		rc = 1;
		scds_syslog_debug(SCDS_DBG_HIGH, "scha_hasp_check() failed");
		goto finished;
	}

	scds_syslog_debug(SCDS_DBG_HIGH, "scds_hasp_check() return ok");

	if (hasp_status == SCDS_HASP_NOT_ONLINE) {
		scds_syslog(LOG_ERR, "Resource depends on a "
			"SUNW.HAStoragePlus type resource that is "
			"not online anywhere.");
		if (print_messages) {
			(void) fprintf(stderr, gettext("Resource depends on "
				"a SUNW.HAStoragePlus type resource that is "
				"not online anywhere.\n"));
		}
		rc = 1;
		goto finished;
	} else if (hasp_status == SCDS_HASP_ERR_CONFIG) {
		/* problem syslogged by scds_hasp_check */
		scds_syslog_debug(SCDS_DBG_HIGH, "scha_hasp_check() returns"
			" SCDS_HASP_ERR_CONFIG");
		if (print_messages) {
			(void) fprintf(stderr, gettext("This resource depends "
				"on a HAStoragePlus resouce that is in a "
				"different Resource Group. This configuration "
				"is not supported.\n"));
		}
		rc = 1;
		goto finished;
	}

	/* don't check on the global stuff if not local. */
	if (hasp_status == SCDS_HASP_ONLINE_NOT_LOCAL) {
		scds_syslog_debug(SCDS_DBG_HIGH, "scha_hasp_check() returns"
			"SCDS_HASP_ONLINE_NOT_LOCAL");
		goto non_global_checks;
	}

	/* check xserver user */
	xsvruserpwd = getpwnam_r(xsvrpropsp->ext_xsvr_user, &pwd, buf,
		MAXPATHLEN);

	scds_syslog_debug(SCDS_DBG_HIGH,
		"errno from getpwnam_r =%d", errno); /*lint !e746 */

	if (xsvruserpwd == NULL) {
		scds_syslog_debug(SCDS_DBG_HIGH, "xsvruserpwd is null");

		if (errno == ERANGE) {
			(void) snprintf(internal_err_str,
				sizeof (internal_err_str),
				"Buffer for user %s data is not big enough",
				xsvrpropsp->ext_xsvr_user);
			scds_syslog(LOG_ERR,
				"INTERNAL ERROR: %s.", internal_err_str);
			if (print_messages) {
				(void) fprintf(stderr, gettext("INTERNAL "
					"ERROR: %s.\n"),
					gettext(internal_err_str));
			}
			rc = 1;
			goto finished;
		} else {
			/*
			 * SCMSGS
			 * @explanation
			 * The SAP xserver user is not found on the system.
			 * @user_action
			 * Make sure the user is available on the system.
			 */
			scds_syslog(LOG_ERR,
				"Failed to retrieve information for "
				"SAP xserver user %s.",
				xsvrpropsp->ext_xsvr_user);
			if (print_messages) {
				(void) fprintf(stderr, gettext("Failed to "
					"retrieve information for SAP xserver "
					"user %s.\n"),
					xsvrpropsp->ext_xsvr_user);
			}
			rc = 1;
			goto finished;
		}
	} else if ((pwd.pw_dir == NULL) || (strlen(pwd.pw_dir) == 0)) {
		/*
		 * SCMSGS
		 * @explanation
		 * Home directory for the specified user is not set in the
		 * system.
		 * @user_action
		 * Check and make sure the home directory is set up correctly
		 * for the specified user.
		 */
		scds_syslog(LOG_ERR, "Home dir is not set for user %s.",
			xsvrpropsp->ext_xsvr_user);
		if (print_messages) {
			(void) fprintf(stderr, gettext("Home dir is not set "
				"for user %s.\n"), xsvrpropsp->ext_xsvr_user);
		}
		rc = 1;
		goto finished;
	}

	/* check Confdir_list is absolute or not */
	confdir_list = xsvrpropsp->ext_confdir_list;
	if (confdir_list[0] != '/') {
		scds_syslog(LOG_ERR, "%s is not an absolute path.",
			xsvrpropsp->ext_confdir_list);
		if (print_messages) {
			(void) fprintf(stderr, gettext("%s is not an "
				"absolute path.\n"),
				xsvrpropsp->ext_confdir_list);
		}
		rc = 1;
		goto finished;
	}

	/*
	 * Make sure executable xserver exists and that the
	 * permissions are correct.
	 */
	rc = snprintf(xserver_cmd_prog, sizeof (xserver_cmd_prog),
		"%s/bin/%s", xsvrpropsp->ext_confdir_list, XSERVER);

	if (rc == -1) {
		(void) snprintf(internal_err_str, sizeof (internal_err_str),
			"Failed to form %s: %s.", xserver_cmd_prog,
			strerror(errno)); /*lint !e746 */
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		if (print_messages) {
			(void) fprintf(stderr, gettext("INTERNAL ERROR: %s.\n"),
				gettext(internal_err_str));
		}
		rc = 1;
		goto finished;
	} else { /* set return code to 0 */
		rc = 0;
	}

	if (stat(xserver_cmd_prog, &statbuf) != 0) {
		saved_errno = errno;
		scds_syslog(LOG_ERR,
		    "%s: %s.", xserver_cmd_prog, strerror(saved_errno));
		if (print_messages) {
			(void) fprintf(stderr, gettext("%s: %s.\n"),
				xserver_cmd_prog,
				gettext(strerror(saved_errno)));
		}
		rc = 1;
		goto finished;
	}

	if (!(statbuf.st_mode & S_IXUSR)) {
		scds_syslog(LOG_ERR, "Incorrect permissions set for %s.",
			xserver_cmd_prog);
		if (print_messages) {
			(void) fprintf(stderr, gettext("Incorrect permissions "
				"set for %s.\n"), xserver_cmd_prog);
		}
		rc = 1;
		goto finished;
	}

	/*
	 * Make sure dbmcli exists and the permissions are correct.
	 */
	(void) snprintf(dbmcli_cmd_prog, sizeof (dbmcli_cmd_prog),
		"%s/bin/%s", xsvrpropsp->ext_confdir_list, DBMCLI);

	scds_syslog_debug(SCDS_DBG_HIGH, "After snprintf(dbmcli-cmd)");

	if (rc == -1) {
		(void) snprintf(internal_err_str, sizeof (internal_err_str),
			"Failed to form %s: %s.", dbmcli_cmd_prog,
			strerror(errno));
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		if (print_messages) {
			(void) fprintf(stderr, gettext("INTERNAL ERROR: %s.\n"),
				gettext(internal_err_str));
		}
		rc = 1;
		goto finished;
	} else {
		rc = 0;
	}

	if (stat(dbmcli_cmd_prog, &statbuf) != 0) {
		saved_errno = errno;
		scds_syslog(LOG_ERR, "%s: %s.", dbmcli_cmd_prog,
			strerror(saved_errno));
		if (print_messages) {
			(void) fprintf(stderr, gettext("%s: %s.\n"),
				dbmcli_cmd_prog,
				gettext(strerror(saved_errno)));
		}
		rc = 1;
		goto finished;
	}

	if (!(statbuf.st_mode & S_IXUSR)) {
		scds_syslog(LOG_ERR, "Incorrect permissions set for %s.",
			dbmcli_cmd_prog);
		if (print_messages) {
			(void) fprintf(stderr, gettext("Incorrect permissions "
				"set for %s.\n"), dbmcli_cmd_prog);
		}
		rc = 1;
		goto finished;
	}

non_global_checks:

	/* Check to make sure other important extension props are set */
	if (scds_get_ext_monitor_retry_count(scds_handle) <= 0) {
		scds_syslog(LOG_ERR,
		"Monitor_retry_count or Monitor_retry_interval is not set.");
		if (print_messages) {
			(void) fprintf(stderr, gettext("Monitor_retry_count or "
				"Monitor_retry_interval is not set.\n"));
		}
		rc = 1; /* Validation Failure */
		goto finished;
	}

	if (scds_get_ext_monitor_retry_interval(scds_handle) <= 0) {
		scds_syslog(LOG_ERR,
		"Monitor_retry_count or Monitor_retry_interval is not set.");
		if (print_messages) {
			(void) fprintf(stderr, gettext("Monitor_retry_count or "
				"Monitor_retry_interval is not set.\n"));
		}
		rc = 1; /* Validation Failure */
	}

	/* All validation checks were successful */

finished:

	scds_syslog_debug(SCDS_DBG_HIGH, "Done w/ svc_validate, rc=%d", rc);
	return (rc); /* return result of validation */
}


/*
 * svc_start()
 *
 */
int
svc_start(scds_handle_t scds_handle, xsvr_extprops_t *xsvrpropsp)
{
	char	kill_cmd[SCDS_CMD_SIZE];
	char	service_start_cmd[SCDS_CMD_SIZE];
	char	internal_err_str[INT_ERR_MSG_SIZE];
	int	rc, rc_code = 0, exit_code;

	rc = snprintf(kill_cmd, SCDS_CMD_SIZE, "%s/%s > /dev/null",
		scds_get_rt_rt_basedir(scds_handle), KILL_PROCESS);
	if (rc == -1) {
		(void) snprintf(internal_err_str, INT_ERR_MSG_SIZE,
			"Failed to form the %s command.", kill_cmd);
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
			internal_err_str);
		return (1);
	} else {
		rc = 0;
	}

	/*
	 * SCMSGS
	 * @explanation
	 * Sun Cluster is preparing to start the specified application
	 * @user_action
	 * This is an informational message, no user action is needed.
	 */
	scds_syslog(LOG_NOTICE, "Preparing to start service %s.",
	    "SAP xserver");
	(void) scds_timerun(scds_handle, kill_cmd,
		scds_get_rs_start_timeout(scds_handle),
		SIGKILL, &exit_code);

	/* What to do if kill -9 failed? */
	scds_syslog_debug(SCDS_DBG_HIGH,
		"Done SIGKILL SAP xserver, exit_code=%d.", exit_code);

	/* need to include 'newtask -p project' if project is set */
	if (xsvrpropsp->proj_name != NULL)
		rc = snprintf(service_start_cmd, SCDS_CMD_SIZE,
			"/usr/bin/su - %s -c '%s -p %s %s/bin/%s %s'",
			xsvrpropsp->ext_xsvr_user, NEWTASK,
			xsvrpropsp->proj_name,
			xsvrpropsp->ext_confdir_list, XSERVER, START);
	else
		rc = snprintf(service_start_cmd, SCDS_CMD_SIZE,
			"/usr/bin/su - %s -c '%s/bin/%s %s'",
			xsvrpropsp->ext_xsvr_user,
			xsvrpropsp->ext_confdir_list, XSERVER, START);

	if (rc == -1) {
		(void) snprintf(internal_err_str, INT_ERR_MSG_SIZE,
			"Failed to form the %s command.", service_start_cmd);
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		rc_code = 1;
		goto finished;
	} else {
		rc = 0;
	}

	/* start it under PMF */
	scds_syslog(LOG_NOTICE, "Starting %s with command %s.", "SAP xserver",
	    service_start_cmd);
	rc = scds_pmf_start(scds_handle, SCDS_PMF_TYPE_SVC,
		SCDS_PMF_SINGLE_INSTANCE, service_start_cmd, 3);

	if (rc == SCHA_ERR_NOERR) {
		scds_syslog_debug(SCDS_DBG_HIGH, "Started under PMF.");
	} else {
		scds_syslog_debug(SCDS_DBG_HIGH,
			"Failed to start under PMF.");
		rc_code = 1;
	}

finished:
	return (rc_code);
}


/*
 * svc_stop():
 *
 * Stop the SAP xserver.
 * Return 0 on success, > 0 on failures.
 *
 */
int
svc_stop(scds_handle_t scds_handle, xsvr_extprops_t *xsvrpropsp)
{
	int rc = 0, cmd_exit_code = 0;
	char service_stop_cmd[SCDS_CMD_SIZE];
	int stop_smooth_timeout = 0;
	char			internal_err_str[INT_ERR_MSG_SIZE];
	char			kill_cmd[SCDS_CMD_SIZE];

	/*
	 * First take the command out of PMF monitoring,
	 * so that it doesn't keep restarting it.
	 */
	rc = scds_pmf_stop_monitoring(scds_handle, SCDS_PMF_TYPE_SVC,
			SCDS_PMF_SINGLE_INSTANCE);
	if (rc != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * Sun Cluster was unable to remove the resource out of PMF
		 * control. This may cause the service to keep restarting on
		 * an unhealthy node.
		 * @user_action
		 * Look in /var/adm/messages for the cause of failure. Save a
		 * copy of the /var/adm/messages files on all nodes. Contact
		 * your authorized Sun service provider for assistance in
		 * diagnosing the problem.
		 */
		scds_syslog(LOG_ERR, "Failed to take "
			"the resource out of PMF control.");
		goto send_kill;
	}

	/*
	 * First try to stop the application using the stop command
	 * provided.
	 */

	/* need to include 'newtask -p project' if project is set */
	if (xsvrpropsp->proj_name != NULL)
		rc = snprintf(service_stop_cmd, SCDS_CMD_SIZE,
			"/usr/bin/su - %s -c '%s -p %s %s/bin/%s %s'",
			xsvrpropsp->ext_xsvr_user, NEWTASK,
			xsvrpropsp->proj_name,
			xsvrpropsp->ext_confdir_list, XSERVER, STOP);
	else
		rc = snprintf(service_stop_cmd, SCDS_CMD_SIZE,
			"/usr/bin/su - %s -c '%s/bin/%s %s'",
			xsvrpropsp->ext_xsvr_user,
			xsvrpropsp->ext_confdir_list, XSERVER, STOP);

	if (rc == -1) {
		(void) snprintf(internal_err_str, INT_ERR_MSG_SIZE,
			"Failed to form the %s command.", service_stop_cmd);
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		/*
		 * We failed to preprocess the stop command so can't
		 * use that. We still proceed to send KILL signal and
		 * try to stop the application anyway.
		 */
		goto send_kill;
	} else {
		rc = 0;
	}

	stop_smooth_timeout = scds_get_rs_stop_timeout(scds_handle) *
		xsvrpropsp->ext_stop_pct / 100;

	/* set it to 2 seconds at least */
	if (stop_smooth_timeout < 2) {
		stop_smooth_timeout = 2;
	}

	/*
	 * SCMSGS
	 * @explanation
	 * Sun Cluster is stopping the specified application with the
	 * specified command.
	 * @user_action
	 * This is an informational message, no user action is needed.
	 */
	scds_syslog(LOG_NOTICE, "Stopping %s with command %s.", "SAP xserver",
			service_stop_cmd);
	rc = scds_timerun(scds_handle, service_stop_cmd, stop_smooth_timeout,
		SIGTERM, &cmd_exit_code);

	/*
	 * we have to ignore exit_code here since stopping a non-running
	 * SAP x-server will have error code 1
	 *
	 */
	if (rc != 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * Process monitor facility is failed to stop the data
		 * service. It is reattempting to stop the data service.
		 * @user_action
		 * This is informational message. Check the Stop_timeout and
		 * adjust it, if it is not appropriate value.
		 */
		scds_syslog(LOG_ERR, "Failed to stop the process with: %s. "
			"Retry with SIGKILL.", service_stop_cmd);
	}

send_kill:
	/*
	 * kill -9 all xserver processes since some xserver processes will
	 * not be under PMF after the xserver processes monitored under PMF
	 * are killed. So need to grab for all, and kill them all.
	 */
	rc = snprintf(kill_cmd, SCDS_CMD_SIZE, "%s/%s > /dev/null",
		scds_get_rt_rt_basedir(scds_handle), KILL_PROCESS);
	if (rc == -1) {
		(void) snprintf(internal_err_str, INT_ERR_MSG_SIZE,
			"Failed to form the %s command.", kill_cmd);
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
			internal_err_str);
		rc = 1;
		goto finished;
	} else {
		rc = 0;
	}

	rc = scds_timerun(scds_handle, kill_cmd, INT_MAX >> 2,
			SIGKILL, &cmd_exit_code);

finished:
	if (rc == 0) {
		scds_syslog(LOG_NOTICE, "Successfully stopped %s.",
		    "resource");
	}
	return (rc);
}


/*
 * svc_wait():
 *
 * wait for the data service to start up fully and make sure it is running
 * healthy
 */

int
svc_wait(scds_handle_t scds_handle, xsvr_extprops_t *xsvrpropsp)
{
	int 			rc, svc_start_timeout, probe_timeout;

	/*
	 * Get the Start method timeout, and the Probe timeout value
	 */
	svc_start_timeout = scds_get_rs_start_timeout(scds_handle);
	probe_timeout = scds_get_ext_probe_timeout(scds_handle);

	/*
	 * sleep for SVC_WAIT_PCT percentage of start_timeout time
	 * before actually probing the dataservice. This is to allow
	 * the dataservice to be fully up inorder to reply to the
	 * probe. NOTE: the value for SVC_WAIT_PCT could be different
	 * for different dataservices.
	 */
	if (scds_svc_wait(scds_handle, (svc_start_timeout * SVC_WAIT_PCT)/100)
	    != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to start service.");
		return (1);
	}

	do {
		rc = svc_probe(scds_handle, xsvrpropsp, probe_timeout,
			B_FALSE);
		if (rc == SCHA_ERR_NOERR) {
			scds_syslog(LOG_NOTICE, "Service is online.");
			return (SCHA_ERR_NOERR);
		}

		scds_syslog(LOG_NOTICE, "Waiting for %s to come up.",
		    "SAP xserver");
		if (scds_svc_wait(scds_handle, SVC_WAIT_TIME)
			!= SCHA_ERR_NOERR) {
			scds_syslog(LOG_ERR, "Failed to start service.");
			return (1);
		}
	/*
	* The probe was not successful, we will continue to probe the
	* application until the calling method times out.
	*/

	} while (1);
}

/*
 * This function starts the fault monitor for a xserver resource.
 * This is done by starting the probe under PMF. The PMF tag
 * is derived as <RG-name,RS-name,instance_number.mon>. The restart option
 * of PMF is used but not the "infinite restart". Instead
 * interval/retry_time is obtained from the RTR file.
 */

int
mon_start(scds_handle_t scds_handle)
{
	scha_err_t	err;

	scds_syslog_debug(SCDS_DBG_HIGH,
	    "Calling MONITOR_START method for resource <%s>.",
	    scds_get_resource_name(scds_handle));

	/*
	 * The probe xserver_probe is assumed to be available in the same
	 * subdirectory where the other callback methods for the RT are
	 * installed. The last parameter to scds_pmf_start denotes the
	 * child monitor level. Since we are starting the probe under PMF
	 * we need to monitor the probe process only and hence we are using
	 * a value of 0.
	 */

	err = scds_pmf_start(scds_handle, SCDS_PMF_TYPE_MON,
	    SCDS_PMF_SINGLE_INSTANCE, "xserver_probe", 0);

	if (err != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to start fault monitor.");
		return (err);
	}


	scds_syslog(LOG_INFO, "Started the fault monitor.");

	return (SCHA_ERR_NOERR); /* Successfully started Monitor */
}


/*
 * This function stops the fault monitor for a xserver resource.
 * This is done via PMF. The PMF tag for the fault monitor is
 * constructed based on <RG-name_RS-name,instance_number.mon>.
 */

int
mon_stop(scds_handle_t scds_handle)
{
	scds_syslog_debug(SCDS_DBG_HIGH, "Calling scds_pmf_stop method");
	if (scds_pmf_stop(scds_handle, SCDS_PMF_TYPE_MON, 0, SIGKILL, -1) !=
	    SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to stop fault monitor.");
		return (1);
	}

	scds_syslog(LOG_INFO, "Stopped the fault monitor.");

	return (SCHA_ERR_NOERR);
}


/*
 * svc_probe(): Do data service specific probing. Return a float value
 * between 0 (success) and 100(complete failure).
 *
 * The probe calls SAP utility 'dbmcli' to check on the health of the
 * SAP xserver.
 */
int
svc_probe(scds_handle_t scds_handle, xsvr_extprops_t *xsvrpropsp, int timeout,
boolean_t print_msgs)
{
	int 	rc, probe_status = 0;
	char	internal_err_str[INT_ERR_MSG_SIZE];
	char	probe_cmd[SCDS_CMD_SIZE];

	rc = snprintf(probe_cmd, SCDS_CMD_SIZE,
		"/usr/bin/sh -c \"%s/bin/%s db_enum\" >/dev/null",
		xsvrpropsp->ext_confdir_list, DBMCLI);
	if (rc == -1) {
		(void) snprintf(internal_err_str, INT_ERR_MSG_SIZE,
			"Failed to form %s: %s", probe_cmd, strerror(errno));
		return (1);
	} else {
		rc = 0;
	}

	rc = scds_timerun(scds_handle, probe_cmd, timeout,
		SIGKILL, &probe_status);

	if (rc != 0) {
		if (rc == SCHA_ERR_TIMEOUT) {
			scds_syslog(LOG_ERR, "Probing SAP xserver timed out "
				"with command %s.", probe_cmd);
		}
		if (rc == SCHA_ERR_NOMEM) {
			scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
				scds_error_string(rc));
		}

		/* system error */
		return (SCDS_PROBE_COMPLETE_FAILURE/4);
	}

	switch (probe_status) {
		case 0: /* SAP xserver is up */
			return (SCHA_ERR_NOERR);
		case 2: if (print_msgs)
				scds_syslog(LOG_ERR, "SAP xserver is "
					"not available.");
			return (SCDS_PROBE_COMPLETE_FAILURE);
		default: /* other error */
			(void) snprintf(internal_err_str, INT_ERR_MSG_SIZE,
				"Checking SAP xserver with dbmcli db_enum "
				"returns code = %d", probe_status);
			scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
				internal_err_str);
			return (SCDS_PROBE_COMPLETE_FAILURE/4);
	}
}

void
free_xsvr_property(xsvr_extprops_t *xsvrpropsp)
{

	if (xsvrpropsp == NULL)
		return;

	free(xsvrpropsp->ext_confdir_list);
	free(xsvrpropsp->ext_xsvr_user);

	free(xsvrpropsp);
}


/*
 * Get project name from system property Resource_project_name if it's set or
 * RG_project_name if Resource_project_name is not set.
 *
 * Return
 *	-1 	if system error
 * 	0	otherwise
 * project_name is set to
 *  	NULL  	if OS is not S9 or higher or
 *		if system property is not available (eg. running < sc3.1) or
 *		if project name is not set (having value of 'default')
 *	project_name otherwise.
 */
int
get_project_name(scds_handle_t scds_handle, xsvr_extprops_t *xsvrpropsp)
{
	scha_resource_t		local_handle;
	char	*proj_name = NULL;
	int	rc = 0;
	char	buf[SYS_NMLN];


	/* set project name in the structure first in case return early */
	xsvrpropsp->proj_name = NULL;

	/* check os, if not S9, return */
	if (sysinfo(SI_RELEASE, buf, SYS_NMLN) == -1) {
		ds_internal_error("sysinfo failed: %s", strerror(errno));
		return (-1);
	}

	if (strcmp(buf, "5.8") == 0) {
		scds_syslog_debug(SCDS_DBG_HIGH, "Project name is not "
			"supported on Solaris 8.");
		return (0);
	}

	/* get project name */
	rc = scha_resource_open(scds_get_resource_name(scds_handle),
		scds_get_resource_group_name(scds_handle), &local_handle);

	if (rc != SCHA_ERR_NOERR) {
			scds_syslog(LOG_ERR,
				"Failed to retrieve the resource handle: %s.",
				scds_error_string(rc));
			rc = -1;
			goto finished;
	}

	/*
	 * scha call will return the value for Resource_project_name if it's
	 * defined. Otherwise, it'll return the value for RG_project_name.
	 * If RG_project_name is not defined either, it'll return the value
	 * of 'default'.
	 */
	rc = scha_resource_get(local_handle,
		SCHA_RESOURCE_PROJECT_NAME, &proj_name);

	if (rc != SCHA_ERR_NOERR) {
		if (rc == SCHA_ERR_PROP) {
		/*
		 * If the project name is not available, that means
		 * it's running s9 on sc3.0, this shouldn't happen since
		 * this 3.1 code won't be running on 3.0, but just in case,
		 * project name is not saved in xsvr structure so that
		 * start/stop won't use it.
		 */
			scds_syslog_debug(SCDS_DBG_HIGH, "No system property"
				": %s", SCHA_RESOURCE_PROJECT_NAME);
			rc = 0;
			goto finished;
		} else {
			scds_syslog(LOG_INFO, "Failed to retrieve the "
				"resource property %s: %s.",
				SCHA_RESOURCE_PROJECT_NAME,
				scds_error_string(rc));
			rc = -1;
			goto finished;
		}
	}

	scds_syslog_debug(SCDS_DBG_HIGH, "rs-proj-name=<%s> rc=%d",
		proj_name, rc);

	/*
	 * if project_name is 'default' that means user didn't set it,
	 * will not save it in xsvr structure since 'default' is already
	 * set by default.
	 */
	if (strcmp(proj_name, DEFAULT_PROJ) == 0) {
		scds_syslog_debug(SCDS_DBG_HIGH, "Project name is not set. ");
		rc = 0;
		goto finished;
	} else {
		/* project_name is set, save it in the xserver structure */
		if ((xsvrpropsp->proj_name  = strdup(proj_name)) == NULL) {
			scds_syslog(LOG_ERR, "Out of memory.");
			rc = -1;
			goto finished;
		}
		scds_syslog_debug(SCDS_DBG_HIGH,
			"project name in structure = <%s>.",
			xsvrpropsp->proj_name);
	}

finished:

	(void) scha_resource_close(local_handle);
	return (rc);
}


/*
 * Check if user can use the specified project.
 * If the user can't use the project, then set the project_name in the
 * xserver structure to NULL, so start/stop won't use the invalid project.
 */
void
validate_proj_user(xsvr_extprops_t *xsvrpropsp)
{
	char	buff[PROJECT_BUFSZ];


	/* don't need to validate project name if it's not set */
	if (xsvrpropsp->proj_name == NULL) {
		scds_syslog_debug(SCDS_DBG_HIGH, "validate_proj_name, "
			"proj name is not set");
		return;
	}

	if (inproj(xsvrpropsp->ext_xsvr_user,
		xsvrpropsp->proj_name, buff, PROJECT_BUFSZ) == 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * Resource_project_name or RG_project_name property
		 * has been set to a project whose members do not
		 * include the specified user.
		 * @user_action
		 * Please set the Resource_project_name or the
		 * RG_project_name property to a valid project for the
		 * specified user. The project membership of a user is
		 * displayed by the command 'projects <user>'.
		 */
		scds_syslog(LOG_WARNING, "Validation failed. User %s does "
			"not belong to the project %s.",
			xsvrpropsp->ext_xsvr_user, xsvrpropsp->proj_name);
		/*
		 * SCMSGS
		 * @explanation
		 * The application which is listed in the message will be
		 * started, stopped using project 'default'.
		 * @user_action
		 * Informational message. No user action is needed.
		 */
		scds_syslog(LOG_NOTICE, "%s will be administrated with project"
			" 'default'.", APP_NAME);

		/*
		 * log these syslog msgs to console too so users will
		 * be alerted.
		 */
		(void) fprintf(stderr, gettext("\nValidation failed. User %s "
			"does not belong to the project %s. %s will be "
			"administrated with project 'default'\n."),
			xsvrpropsp->ext_xsvr_user,
			xsvrpropsp->proj_name, APP_NAME);

		/*
		 * need to reset the project_name in the structure so
		 * start/stop won't use it.
		 */
		xsvrpropsp->proj_name = NULL;
	} else {
		scds_syslog_debug(SCDS_DBG_HIGH,
			"User %s can use project %s", xsvrpropsp->ext_xsvr_user,
			xsvrpropsp->proj_name);
	}
}
