/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2004 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * enq_update.c - Update method for enq
 */

#pragma ident	"@(#)enq_update.c	1.4	07/06/06 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <rgm/libdsdev.h>

/*
 * Some of the resource properties might have been updated. All such
 * updatable properties are related to the fault monitor. Hence, just
 * restarting the monitor should be enough.
 */

int
main(int argc, char *argv[])
{
	scds_handle_t   scds_handle;
	scha_err_t	result;

	/* Process the arguments passed by RGM and initialize syslog */
	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR)
		return (1);

	/*
	 * check if the Fault monitor is already running and if so stop and
	 * restart it. The second parameter to scds_pmf_restart_fm() uniquely
	 * identifies the instance of the fault monitor that needs to be
	 * restarted.
	 */

	result = scds_pmf_restart_fm(scds_handle, 0);
	if (result != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		    "Failed to restart fault monitor.");
		/* Free up all the memory allocated by scds_initialize */
		scds_close(&scds_handle);
		return (1);
	}

	scds_syslog(LOG_INFO,
	    "Completed successfully.");

	/* Free up all the memory allocated by scds_initialize */
	scds_close(&scds_handle);

	return (0);
}
