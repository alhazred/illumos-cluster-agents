/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2004 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef	_ENQ_H
#define	_ENQ_H

#pragma ident	"@(#)enq.h	1.5	07/06/06 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#include <sap_common.h>

#define	REPLICA_RT_NAME		"SUNW.saprepl"
#define	ENQ_MON_OPCODE_1		"1"
#define	ENQ_MON_OPCODE_2		"2"

/* in bytes */
#define	MAXBUFSIZE		256
#define	MAX_ENQ_ENV		7

#define	APP_NAME		"SAP standalone enqueue server"
#define	SAPENQ			"enqueue"
#define	ENQ_MON			"ensmon"

/* if no replica res configured in the +affinity rg, used in svc_start */
#define	NO_REP_RS		2

/* ensmon status code */
#define	ENQ_REPL_OK		0
#define	ENQ_OK_REPL_NOTOK	4
#define	ENQ_DOWN		8
#define	WRONG_PARAM		12
#define	ENQ_OK_REPL_UNKNOWN	13

/* extension property */
typedef struct enq_extprops
{
	char	*enq_profile;
	char	*enq_instancenum;
	char	*enq_svr;
	char	*enq_svr_mon;
	char	*rep_rs;
	char	*enq_usr;
	char	*enq_logdir;
	char	*enq_home_dir;		/* not a real ext property */
	uid_t	enq_uid;		/* not a real ext property */
	gid_t	enq_gid;		/* not a real ext property */
	int	enq_port;		/* not a real ext property */
	char	*repl_rgname;	/* not a real ext property, +affinity rg */
	char	*ld_path;		/* not a real ext property */
} enq_extprops_t;

int svc_validate(scds_handle_t scds_handle, enq_extprops_t *enqxpropsp,
	boolean_t print_messages);

int svc_start(scds_handle_t scds_handle, enq_extprops_t *enqxpropsp);

int svc_stop(scds_handle_t scds_handle);

int svc_wait(scds_handle_t scds_handle, enq_extprops_t *enqxpropsp);

int mon_start(scds_handle_t scds_handle);

int mon_stop(scds_handle_t scds_handle);

int svc_probe(scds_handle_t scds_handle, enq_extprops_t *enqxpropsp,
	int probe_timeout, boolean_t print_msg, const char *rs_name);

int get_enq_extensions(scds_handle_t handle, enq_extprops_t *enqxpropsp,
	boolean_t print_messages);

int
retrieve_otherprops(enq_extprops_t *enqxpropsp, boolean_t print_messages);

int check_weak_pos_affinity(scds_handle_t scds_handle,
	enq_extprops_t *enqxpropsp, boolean_t print_messages);

int check_replica_rs(char *rgname, boolean_t *replica_remote,
	boolean_t print_msg);

#ifdef __cplusplus
}
#endif

#endif /* _ENQ_H */
