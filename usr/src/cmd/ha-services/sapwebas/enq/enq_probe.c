/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2004 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * enq_probe.c - Probe for enq
 */

#pragma ident	"@(#)enq_probe.c	1.4	07/06/06 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <nss_dbdefs.h>
#include <rgm/libdsdev.h>
#include <ds_common.h>
#include "enq.h"

int
main(int argc, char *argv[])
{
	scds_handle_t	scds_handle;
	enq_extprops_t	enqxpropsp;
	int		timeout;
	int		probe_result;
	hrtime_t	ht1, ht2;
	long		dt;
	const char	*rs_name = NULL;

	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR)
		return (1);

	if (get_enq_extensions(scds_handle, &enqxpropsp, B_FALSE)
		!= SCHA_ERR_NOERR)
		return (1);

	if (retrieve_otherprops(&enqxpropsp, B_FALSE)
		!= SCHA_ERR_NOERR) {
		return (1);
	}

	timeout = scds_get_ext_probe_timeout(scds_handle);

	rs_name = scds_get_resource_name(scds_handle);

	/* go to the log dir here so won't do that in a loop in svc_probe */
	scds_syslog_debug(SCDS_DBG_HIGH, "chdir to %s",
		enqxpropsp.enq_logdir);
	if (chdir(enqxpropsp.enq_logdir) != 0) {
		ds_internal_error("chdir: %s.",
			strerror(errno)); /*lint !e746 */
		return (1);
	}

	for (;;) {

		/*
		 * sleep for a duration of thorough_probe_interval between
		 *  successive probes.
		 */
		(void) scds_fm_sleep(scds_handle,
		    scds_get_rs_thorough_probe_interval(scds_handle));

		ht1 = gethrtime(); /* Latch probe start time */

		probe_result = svc_probe(scds_handle,
			&enqxpropsp, timeout, B_TRUE, rs_name);

		ht2 = gethrtime();

		/* Convert to milliseconds */
		dt = (long)((ht2 - ht1) / 1e6);

		/*
		 * Compute failure history and take action if needed
		 */
		(void) scds_fm_action(scds_handle, probe_result, dt);

	} 	/* Keep probing forever */
}
