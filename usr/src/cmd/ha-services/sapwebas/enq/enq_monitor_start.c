/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2004 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * enq_monitor_start.c - Monitor Start method for enq
 */

#pragma ident	"@(#)enq_monitor_start.c	1.5	07/06/06 SMI"

#include <rgm/libdsdev.h>
#include "enq.h"

/*
 * This method starts the fault monitor for a enq resource.
 * This is done by starting the probe under PMF. The PMF tag
 * is derived as RG-name,RS-name.mon. The restart option of PMF
 * is used but not the "infinite restart". Instead
 * interval/retry_time is obtained from the RTR file.
 */

int
main(int argc, char *argv[])
{
	scds_handle_t   scds_handle;
	enq_extprops_t	enqxpropsp;
	int	rc;

	/* Process arguments passed by RGM and initialize syslog */
	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR) {
		return (1);
	}

	scds_syslog_debug(SCDS_DBG_HIGH,
		"Calling MONITOR_START method for resource <%s>.",
		scds_get_resource_name(scds_handle));

	/*
	 * Even enq_probe.c retrieves the ext. properties and validate
	 * them, we want to get the ext. properties and validate them here
	 * first. So that if there is any configuration error, monitor_start
	 * can fail right away, instead of failing in the enq_probe.
	 * The benefit for that is users won't see msgs about the monitor is
	 * online, then later on seeing error msgs, and then waiting for pmf
	 * to restart the monitor, and eventually the monitor quits after
	 * exceeding the retry_count.
	 * If the monitor encounters some errors after it's up and running,
	 * which is a different issue, that will be handled in the DSDL.
	 * Before the monitor exists, it should set appropriate state and
	 * status to inform users.
	 */

	/* Retrieve SAP enq server extension properties */
	rc = get_enq_extensions(scds_handle, &enqxpropsp, B_TRUE);
	if (rc != 0) {
		/* Error msg has been logged */
		scds_syslog_debug(SCDS_DBG_HIGH,
			"Failed to get ext. property.");
		goto finished;
	}

	if ((rc = retrieve_otherprops(&enqxpropsp, B_TRUE))
		!= SCHA_ERR_NOERR) {
		scds_syslog_debug(SCDS_DBG_HIGH,
			"Failed to retrieve other property.");
		goto finished;
	}

	rc = svc_validate(scds_handle, &enqxpropsp, B_TRUE);
	if (rc != 0) {
		scds_syslog(LOG_ERR,
			"Failed to validate configuration.");
		goto finished;
	}

	rc = mon_start(scds_handle);

finished:
	/* Free up all the memory allocated by scds_initialize */
	scds_close(&scds_handle);

	/* Return the result of monitor_start method */
	return (rc);
}
