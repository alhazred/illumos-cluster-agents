/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * enq.c - Common utilities for standalone enqueue server
 *
 * This utility has the methods for performing the validation, starting and
 * stopping the data service and the fault monitor. It also contains the method
 * to probe the health of the data service.  The probe just returns either
 * success or failure. Action is taken based on this returned value in the
 * method found in the file enq_probe.c
 *
 */

#pragma ident	"@(#)enq.c	1.12	07/09/10 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <unistd.h>
#include <scha.h>
#include <errno.h>
#include <rgm/libdsdev.h>
#include <pwd.h>
#include <libgen.h>
#include <libintl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/statvfs.h>
#include <limits.h>
#include <nss_dbdefs.h>
#include <ds_common.h>
#include "enq.h"

/*
 * The initial timeout allowed  for the sap enqueue dataservice to
 * be fully up and running. We will wait for for 2% (SVC_WAIT_PCT)
 * of the start_timeout time before probing the service.
 */
#define	SVC_WAIT_PCT		2

/*
 * We need to wait for SVC_WAIT_TIME ( 2 secs) to wait between probing
 * the service when service is first started up.
 */
#define	SVC_WAIT_TIME		2

/*
 * svc_validate():
 *
 * Perform the following checks:
 *	HSP check
 *	if HSP is online-not-local, perform the first two checks and
 *		skip the rest. Will perform all the checks if HSP is online
 *		and local on this node:
 *
 *	1)weak positive affinity for replica rg is set
 *	2)logical host exists
 *	3)enqueue profile exists with correct permission
 *	4)enqueue server binary exists with correct permission
 *	5)enqueue monitor binary exists with correct permission
 *	6)admin user belongs to the specified project (if RG_project_name
 *		or Resource_project_name is set)
 *	7)enqueue log dir exists and it's an absolute path
 */
int
svc_validate(scds_handle_t scds_handle, enq_extprops_t *enqxpropsp,
	boolean_t print_messages)
{
	int 	rc = 0;
	int	hasprc;
	scds_net_resource_list_t *snrlp = NULL;

	/*
	 * Validate HAStoragePlus resources and determine if the
	 * storage/filesystems are online on this node.
	 *
	 * If they are online and local, will go thru all the checks.
	 * If online but not local, only check non-global stuff.
	 *
	 * validate_hasp() logs and prints appropriate messages already.
	 */
	hasprc = validate_hasp(print_messages, scds_handle);

	switch (hasprc) {
		case SCDS_HASP_ONLINE_NOT_LOCAL:goto online_not_local;
		case SCDS_HASP_ONLINE_LOCAL:	break;
		case SCDS_HASP_NO_RESOURCE:	break;
		default:	/* hasp error or system error */
				return (1);
	}

	/*
	 * Perform the following checks if HSP is online-local. If it
	 * is online but not local, then only perform the checks under
	 * online_not_local.
	 */

	/* check enq server executable exists w/ correct permission */
	rc = ds_validate_file(print_messages, S_IRUSR | S_IXUSR | S_IFREG,
		enqxpropsp->enq_svr);

	if (rc != SCHA_ERR_NOERR) {
		return (1);
	}

	/* check enq server binary is owned by sap_user */
	rc = ds_validate_file_owner(print_messages, enqxpropsp->enq_uid,
		enqxpropsp->enq_gid, "%s", enqxpropsp->enq_svr);

	if (rc != SCHA_ERR_NOERR) {
		return (1);
	}

	/* check enqueue profile exists w/ correct permission */
	rc = ds_validate_file(print_messages, S_IRUSR | S_IFREG,
		enqxpropsp->enq_profile);

	if (rc != SCHA_ERR_NOERR) {
		return (1);
	}

	/* admin user belongs to the specified project */
	if ((rc = validate_project_name(scds_handle, enqxpropsp->enq_usr))
		!= 0) {
		/* error msg has been logged */
		return (1);
	}

	/* make sure log dir is an absolute path */
	if (enqxpropsp->enq_logdir[0] != '/') {
		scds_syslog(LOG_ERR, "%s is not an absolute path.",
			enqxpropsp->enq_logdir);
		if (print_messages) {
			(void) fprintf(stderr, gettext("%s is not an "
				"absolute path.\n"), enqxpropsp->enq_logdir);
		}
		return (1);
	}

	/* make sure logdir exists */
	rc = ds_validate_file(print_messages, S_IRUSR | S_IXUSR | S_IFDIR,
		enqxpropsp->enq_logdir);

	if (rc != SCHA_ERR_NOERR) {
		return (1);
	}

	scds_syslog_debug(SCDS_DBG_HIGH, "after ds_validate_file for logdir");

	/* check logdir is owned by sap_user */
	rc = ds_validate_file_owner(print_messages, enqxpropsp->enq_uid,
		enqxpropsp->enq_gid, "%s", enqxpropsp->enq_logdir);

	if (rc != SCHA_ERR_NOERR) {
		return (1);
	}

	/* check enq server monitor executable exists w/ correct permission */
	rc = ds_validate_file(print_messages, S_IRUSR | S_IXUSR | S_IFREG,
		enqxpropsp->enq_svr_mon);

	if (rc != SCHA_ERR_NOERR) {
		return (1);
	}

	/* check enq server monitor is owned by sap_user */
	rc = ds_validate_file_owner(print_messages, enqxpropsp->enq_uid,
		enqxpropsp->enq_gid, "%s", enqxpropsp->enq_svr_mon);

	if (rc != SCHA_ERR_NOERR) {
		return (1);
	}

online_not_local:

	/*
	 * Check whether weak positive affinity w/ replica rg is set.
	 * Issue warning if the affinity is not set. Howerver, if the
	 * affinity is set, but retry_count is not 0, error.
	 */
	if ((rc = check_weak_pos_affinity(scds_handle,
		enqxpropsp, print_messages)) != 0)
		return (1);

	/* check network address resources for this resource */
	if ((rc = scds_get_rs_hostnames(scds_handle, &snrlp))
		!= SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		    "Error in trying to access the configured network "
		    "resources : %s.", scds_error_string(rc));
		if (print_messages) {
			(void) fprintf(stderr,
				gettext("Error in trying to access the "
				"configured network resources : %s."),
				scds_error_string(rc));
		}
		return (1);
	}

	/* return error if there is no network address resource */
	if (snrlp == NULL || snrlp->num_netresources == 0) {
		scds_syslog(LOG_ERR,
		    "No network address resource in resource group.");
		if (print_messages) {
			(void) fprintf(stderr,
				gettext("No network address resource in "
				"resource group."));
		}
		return (1);
	}

	return (0);
}


/*
 * Verify the weak positive affinity is set with the replica resource group.
 * Currently, the weak or strong affinity are represented by prefix
 * "++", "+", "--" or "-" of the resource group name. Needs to  parse the
 * resource gorup name to find out which flavour of the affinity it is.
 * This routine will save the +aff repl-rg name in the internal structure.
 * It also check Retry_count. If it's > 0, error out.
 *
 * Return 0 if affinity is set and retry_count=0. If affinity is not set,
 * log error/warning msg, return 0. If affinity is set, but retry_count
 * is not 0, return 1.
 */
int
check_weak_pos_affinity(scds_handle_t scds_handle, enq_extprops_t *enqxpropsp,
	boolean_t print_messages)
{
	int			i = 0, retry_count = 0;
	boolean_t		affinity_set = B_FALSE;
	char			*temp_rg = NULL;
	const scha_str_array_t	*afflist = NULL;

	/* retrieve the rg affinity */
	afflist = scds_get_rg_rg_affinities(scds_handle);

	/* log warning msg if +affinity is not set */
	if ((afflist == NULL) || (afflist->array_cnt < 1) ||
		((afflist->array_cnt > 0) && (afflist->str_array == NULL))) {
		/*
		 * SCMSGS
		 * @explanation
		 * The specified affinity has not been set for the specified
		 * resource group.
		 * @user_action
		 * Informational message, no action is needed if the SAP
		 * enqueue server is running without the SAP replica server.
		 * However, if the SAP replica server will also be running on
		 * the cluster, the specified affinity must be set on the
		 * specified resource group.
		 */
		scds_syslog(LOG_ERR, "WARNING: %s affinity property for "
			"%s resource group is not set.",
			"Weak positive", "replica server");
		if (print_messages)
			(void) fprintf(stderr,
				gettext("\nWARNING: Weak positive affinity "
					"property for the replica server "
					"resource group is not set."));
		enqxpropsp->repl_rgname = NULL;
		return (0);
	}

	/*
	 * rg_affinity property is set, need to check and see if it really
	 * contains weak positive affinity: for each rg name in the
	 * afflist array, if there is prefix "+", that means it's a
	 * weak positive affinity. If none of rgs in the afflist has
	 * "+" prefix, error out.
	 *
	 * Note, we can't do further check to verify the +affinity rg
	 * does indeed contain a rs that is the replica RT since at the
	 * validate time of the enqueue rs, the replica rg is created,
	 * but not the replica rs.
	 */
	for (i = 0; i < (int)afflist->array_cnt; i++) {
		if ((afflist->str_array[i][0] == '+') &&
			(afflist->str_array[i][1] != '+')) {

			/* save the +affinity rg name for START */
			temp_rg = afflist->str_array[i];
			temp_rg += 1;	/* skip the prefix "+" */

			if (temp_rg != NULL) {
				if ((enqxpropsp->repl_rgname =
					strdup(temp_rg)) == NULL) {
					scds_syslog(LOG_ERR, "Out of memory.");
					if (print_messages) {
						(void) fprintf(stderr,
						gettext("Out of memory.\n"));
					}
					return (1);
				} else {
					/* +affinity is set, return early */
					scds_syslog_debug(SCDS_DBG_HIGH,
						"+affinity=%s",
						enqxpropsp->repl_rgname);
					affinity_set = B_TRUE;
					break;
				}
			}
		} else { /* rg is not +affinity, keep looking. */
			scds_syslog_debug(SCDS_DBG_HIGH,
				"\nrg %s is not +affinity.",
				afflist->str_array[i]);
		}
	}

	if (affinity_set != B_TRUE) {
		scds_syslog(LOG_ERR, "WARNING: %s affinity property for %s "
			"resource group is not set.", "Weak positive",
			"replica server");
		if (print_messages)
			(void) fprintf(stderr,
				gettext("\nWARNING: Weak positive affinity "
					"property for replica server resource "
					"group is not set.\n"));
		enqxpropsp->repl_rgname = NULL;
		return (0);
	}

	/* weak positive affinity is set, check retry_count, it has to be 0 */
	retry_count = scds_get_rs_retry_count(scds_handle);
	if (retry_count > 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * The specified affinity has been set for the specified
		 * resource group. That means the SAP enqueue server is
		 * running with the replica server. Hence the retry_count for
		 * SAP enqueue server resource must not restart locally.
		 * Therefore, the Retry_count must be set to zero.
		 * @user_action
		 * Set the value for Retry_count to zero for the SAP enqueue
		 * server resource.
		 */
		scds_syslog(LOG_ERR, "%s affinity property for %s resource "
			"group is set. Retry_count cannot be greater than 0.",
			"Weak positive", "replica server");
		if (print_messages)
			(void) fprintf(stderr,
				gettext("Weak positive affinity property "
					"for replica server resource group "
					"is set. Retry_count cannot be "
					"greater than 0.\n"));
		return (1);
	}
	return (0);
}


/*
 * Sets the directory path for logging. It will be the home dir of the
 * admin user (if not defined specifically).
 * Sets the Enqueue-server-monitor path. It is a property w/ default "".
 * Sets the LD_LIBRARY_PATH environment variable.
 *
 * Return 0 if succeed, 1 otherwise.
 */
int
retrieve_otherprops(enq_extprops_t *enqxpropsp, boolean_t print_messages)
{
	char		*enq_mon = NULL, *bin_path = NULL, *sap_bin = NULL;
	char		*enq_ldpath = NULL;

	/*
	 * Set the log dir path.
	 * If no input value, set it to home dir of Enqueue_User.
	 */
	if (strcmp(enqxpropsp->enq_logdir, "") == 0) {
		enqxpropsp->enq_logdir = strdup(enqxpropsp->enq_home_dir);
		if (enqxpropsp->enq_logdir == NULL) {
			scds_syslog(LOG_ERR, "Out of memory.");
			if (print_messages)
				(void) fprintf(stderr,
					gettext("Out of memory.\n"));
			return (1);
		}
	}

	/*
	 * Set the Enqueue_Server_Monitor path.
	 * If no input value, set it to the same path for Enqueue_Server.
	 */
	sap_bin = strdup(enqxpropsp->enq_svr);
	if (sap_bin == NULL) {
		scds_syslog(LOG_ERR, "Out of memory.");
		if (print_messages)
			(void) fprintf(stderr, gettext("Out of memory.\n"));
		return (1);
	}

	if (strcmp(enqxpropsp->enq_svr_mon, "") == 0) {

		/* find out the dir of enqueue_server */
		bin_path = dirname(sap_bin);
		scds_syslog_debug(SCDS_DBG_HIGH, "bin-path=[%s]", bin_path);

		/* dirname() got a valid path */
		if (strcmp(bin_path, ".") != 0) {
			enq_mon = ds_string_format("%s/%s",
					bin_path, ENQ_MON);
			if (enq_mon == NULL) {
				scds_syslog(LOG_ERR, "Out of memory.");
				free(sap_bin);
				return (1);
			}
			scds_syslog_debug(SCDS_DBG_HIGH,
				"enq_mon = [%s]", enq_mon);
			/*
			 * LD_LIBRARY_PATH point to the same path as the
			 * enqueue binary
			 */
			enq_ldpath = ds_string_format("LD_LIBRARY_PATH=%s",
					bin_path);
			if (enq_ldpath == NULL) {
				scds_syslog(LOG_ERR, "Out of memory.");
				free(sap_bin);
				free(enq_mon);
				return (1);
			} else
				scds_syslog_debug(SCDS_DBG_HIGH,
				"enq_ld_path = [%s]", enq_ldpath);
		} else {
			ds_internal_error("dirname(%s) return w/ pointer "
				"to '.'", sap_bin);
			free(sap_bin);
			return (1);
		}

		enqxpropsp->enq_svr_mon = strdup(enq_mon);
		if (enqxpropsp->enq_svr_mon == NULL) {
			scds_syslog(LOG_ERR, "Out of memory.");
			if (print_messages)
				(void) fprintf(stderr,
					gettext("Out of memory.\n"));
			free(enq_mon);
			free(sap_bin);
			free(enq_ldpath);
			return (1);
		}
		enqxpropsp->ld_path = strdup(enq_ldpath);
		if (enqxpropsp->ld_path == NULL) {
			scds_syslog(LOG_ERR, "Out of memory.");
			if (print_messages)
				(void) fprintf(stderr,
					gettext("Out of memory.\n"));
			free(enq_ldpath);
			free(enq_mon);
			free(sap_bin);
			return (1);
		}
	}

	scds_syslog_debug(SCDS_DBG_HIGH, "enqsrv=%s, ensmon=%s, ldpath=%s",
		enqxpropsp->enq_svr, enqxpropsp->enq_svr_mon,
		enqxpropsp->ld_path);

	free(sap_bin);
	free(enq_ldpath);
	free(enq_mon);
	return (0);
}



/*
 * svc_start(): Start SAP enqueue server using the SAP utililty under
 * pmf after setgid/setuid to the enqueue admin user, if
 * the replica is running online locally or offline.
 *
 * If the weak positive affinity is set, but the affinity rg has no
 * replica resource, an warning msg is logged but the enqueue resource
 * will be started. Because if the replica resource is configured, the
 * replica validate will make sure the configuration towards the enqueue
 * dependency and affinity relationship is set correctly. However, if the
 * replica resource is not configured, and the affinity is set by mistake,
 * the enqueue will still be started up.
 *
 * Return 0 	if enqueue started up successfully
 *	  1	if replica is online remotely or system error
 *	call scha_control to bailout and also return 1
 *		if enqueue is already running outside of SC
 */
int
svc_start(scds_handle_t scds_handle, enq_extprops_t *enqxpropsp)
{
	int 		rc = 0;
	boolean_t	replica_remote;
	char		*pf_cmd = NULL;
	const char	*rgname = NULL, *rsname = NULL;
	int		child_mon_level = -1;
	scha_extprop_value_t	*child_mon_level_prop = NULL;
	char		*envp[MAX_ENQ_ENV];

	/*
	 * If the replica rgname is set, then go on to
	 * find out Replica resource is online on this node or not.
	 * This ensures users can't scswitch the enqueue resource to
	 * the wrong node by accident.
	 */
	if (enqxpropsp->repl_rgname != NULL) {
		rc = check_replica_rs(enqxpropsp->repl_rgname,
			&replica_remote, B_FALSE);
		switch (rc) {
			case 0:
				if (replica_remote == B_TRUE) {
				/* replica is running on some other node */
					/*
					 * SCMSGS
					 * @explanation
					 * The SAP enqueue server is trying to
					 * start up on a node where the SAP
					 * replica server has not been
					 * running. The SAP enqueue server
					 * must start on the node where the
					 * replica server has been running.
					 * @user_action
					 * No user action is needed.
					 */
					scds_syslog(LOG_ERR, "Replica is not "
						"running on this node, but is "
						"running remotely on some "
						"other node. %s cannot be "
						"started on this node.",
					APP_NAME);
					rc = 1;
					goto finished;
				}
				/*
				 * replica rg is not set; or
				 * replica is online on this node or offline.
				 */
				break;
			case NO_REP_RS:
				/*
				 * weak aff rg is set, but there is
				 * no repl rs configured in it. Warning msg
				 * was logged alreday. Won't fail start method.
				 * So in case the weak positive affinity rg
				 * is set by mistake or for something else
				 * other than replica rs, enqueue can still
				 * startup.
				 */
				break;
			case 1: /* system error */
			default: /* shouldn't have any other cases */
				scds_syslog_debug(SCDS_DBG_HIGH,
					"Error in check_replica_rs. rc=%d. "
					" Should already log msg.", rc);
				rc = 1;
				goto finished;
		}
	}

	/*
	 * replica rg is not set; or
	 * replica rg is set w/o replica rs in it; or
	 * replica is online on this node; or
	 * offline, can go ahead start enqueue.
	 *
	 * If enqueue is already running on this node, call scha_control
	 * with flag SCHA_IGNORE_FAILED_START so that the start method
	 * will abort w/o further failovers. The start method will return
	 * non-zero.
	 */

	rc = svc_probe(scds_handle, enqxpropsp, MAX_TIMEOUT,
		B_FALSE, scds_get_resource_name(scds_handle));

	if (rc == 0) {
		/*
		 * If enqueue is already running, log error, call scha_control
		 * w/ SCHA_IGNORE_FAILED_START flag. Then return 1
		 * so the start method will abort.
		 */
		scds_syslog(LOG_ERR,
			"%s is already running on this node outside of Sun "
			"Cluster. The start of %s from Sun Cluster will "
			"be aborted.", APP_NAME, APP_NAME);

		rsname = scds_get_resource_name(scds_handle);
		rgname = scds_get_resource_group_name(scds_handle);

		if (scha_control(SCHA_IGNORE_FAILED_START,
			rgname, rsname) == SCHA_ERR_TAG)

			scds_syslog_debug(SCDS_DBG_HIGH, "Invalid scha_control"
				" tag SCHA_IGNORE_FAILED_START. api-version "
				"running is older than 5. Start method won't "
				"abort, but fail. Failover will be carried "
				"out by RGM.");

		return (1);
	}

	/* Enqueue not running, start it under PMF */
	pf_cmd = ds_string_format("%s pf=%s", enqxpropsp->enq_svr,
		enqxpropsp->enq_profile);
	if (pf_cmd == NULL) {
		scds_syslog(LOG_ERR, "Out of memory.");
		return (1);
	}

	scds_syslog_debug(SCDS_DBG_HIGH, "start-cmd=%s, log-dir=%s ldpath=%s",
		pf_cmd, enqxpropsp->enq_logdir, enqxpropsp->ld_path);

	/* set environment LD_LIBRARY_PATH */
	envp[0] = enqxpropsp->ld_path;
	envp[1] = NULL;

	/* change to the log dir and setting the egid and euid before start */
	if ((rc = chdir(enqxpropsp->enq_logdir)) != 0) {
		ds_internal_error("chdir: %s", strerror(errno)); /*lint !e746 */
		goto finished;
	}

	scds_syslog_debug(SCDS_DBG_HIGH, " after chdir");

	if ((rc = setegid(enqxpropsp->enq_gid)) != 0) {
		ds_internal_error("setegid: %s", strerror(errno));
		goto finished;
	}

	scds_syslog_debug(SCDS_DBG_HIGH, "after setegid");

	if ((rc = seteuid(enqxpropsp->enq_uid)) != 0) {
		ds_internal_error("seteuid: %s", strerror(errno));
		goto finished;
	}

	scds_syslog_debug(SCDS_DBG_HIGH, " after seteuid");

	/* get child_mon_level */
	if (scds_get_ext_property(scds_handle, "Child_mon_level",
		SCHA_PTYPE_INT, &child_mon_level_prop) != SCHA_ERR_NOERR)
		scds_syslog(LOG_INFO,
			"Either extension property <Child_mon_level> is not "
			"defined, or an error occurred while retrieving this "
			"property. The default value of -1 is being used.");
	else {
		child_mon_level = child_mon_level_prop->val.val_int;
		scds_syslog(LOG_INFO,
			"Extension property <Child_mon_level> has a "
			"value of <%d>", child_mon_level);
	}

	/* start enq under pmf with the LD_LIBRARY_PATH set in envp */
	rc = scds_pmf_start_env(scds_handle, SCDS_PMF_TYPE_SVC, 0,
		pf_cmd, child_mon_level, envp);

	if (rc == SCHA_ERR_NOERR)
		scds_syslog(LOG_NOTICE,
			"Start of %s completed successfully.", APP_NAME);
	else
		scds_syslog(LOG_NOTICE,
			"Failed to start the service %s.", APP_NAME);

	if (seteuid(0) != 0) {
		ds_internal_error("seteuid(0): %s", strerror(errno));
		rc = 1;
	}

	if (setegid(0) != 0) {
		ds_internal_error("setegid(0): %s", strerror(errno));
		rc = 1;
	}

	scds_syslog_debug(SCDS_DBG_HIGH, "after reset uid=%d euid=%d",
		getuid(), geteuid());

finished:

	free(pf_cmd);
	return (rc); /* return Success/failure status */
}



/*
 * This function retrieves the replica rs from the +affinity rg. Then
 * retrieves the status of that replica rs. Returns error code NO_REP_RS
 * if there is no replica resource configured in the +affinity rg.
 *
 * replica_remote will be set to true if replica is running on a remote
 * note. Otherwise (either online local or offline), it'll be false.
 *
 * Start method will fail if replica_remote is true (to prevent
 * accidental scswitch to the wrong node).
 *
 * This function return 1 for system error.
 */
int
check_replica_rs(char *rgname, boolean_t *replica_remote, boolean_t print_msg)
{
	scha_resourcegroup_t	rghandle = NULL;
	scha_resource_t		rshandle = NULL;
	scha_str_array_t	*rss = NULL, *rgnodes = NULL;
	char			*rtname = NULL, *nodename = NULL;
	boolean_t		is_replica_rt = B_FALSE;
	scha_rsstate_t		node_rs_state;
	scha_err_t		rc = 0;
	int			i = 0, j = 0;

	/* set replica_remote to true for early return code */
	*replica_remote = B_TRUE;

	/* open the +affinity rg */
	scds_syslog_debug(SCDS_DBG_HIGH, "repl-rg=%s", rgname);

retry_rs_list:
	rc = scha_resourcegroup_open(rgname, &rghandle);

	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
			"Failed to retrieve the resource group handle: %s.",
			scds_error_string(rc));
		if (print_msg)
			(void) fprintf(stderr,
				gettext("Failed to retrieve the resource "
					"group handle: %s."),
				scds_error_string(rc));
		return (1);
	}

	/* retrieve all resources in the +affinity rg */
	rc = scha_resourcegroup_get(rghandle, SCHA_RESOURCE_LIST, &rss);
	if (rc == SCHA_ERR_SEQID) {
		/*
		 * SCMSGS
		 * @explanation
		 * An update to cluster configuration occurred while resource
		 * group properties were being retrieved.
		 * @user_action
		 * No user action is needed.
		 */
		scds_syslog(LOG_INFO,
			"Retrying to retrieve the resource group "
			"information: %s.", scds_error_string(rc));
		(void) scha_resourcegroup_close(rghandle);
		rghandle = NULL;
		rss = NULL;
		(void) sleep(1);
		goto retry_rs_list;
	} else if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
			"Failed to retrieve the resource group "
			"property %s: %s.", SCHA_RESOURCE_LIST,
			scds_error_string(rc));
		if (print_msg) {
			(void) fprintf(stderr,
				gettext("Failed to retrieve the "
					"resource group property %s: %s."),
				SCHA_RESOURCE_LIST, scds_error_string(rc));
		}
		(void) scha_resourcegroup_close(rghandle);
		return (1);
	}

	/*
	 * For each resource in the rg, find out what the RT it belongs to.
	 * If the first rs's RT is replica RT, it will stop further search.
	 */
	for (i = 0; i < (int)rss->array_cnt; i++) {
		scds_syslog_debug(SCDS_DBG_HIGH,
			"rs in repl-rg=%s", rss->str_array[i]);
retry_rt_type:
		rc = scha_resource_open(rss->str_array[i], rgname,
			&rshandle);
		if (rc != SCHA_ERR_NOERR) {
			scds_syslog(LOG_ERR, "Failed to retrieve the resource"
				" handle: %s.", scds_error_string(rc));
			if (print_msg)
				(void) fprintf(stderr,
					gettext("Failed to retrieve the "
						"resource handle: %s."),
					scds_error_string(rc));
			(void) scha_resourcegroup_close(rghandle);
			return (1);
		}

		/* retrieve the RT of the rs */
		rc = scha_resource_get(rshandle, SCHA_TYPE, &rtname);
		if (rc == SCHA_ERR_SEQID) {
			scds_syslog(LOG_INFO,
				"Retrying to retrieve the resource "
				"information: %s.", scds_error_string(rc));
			rtname = NULL;
			rshandle = NULL;
			(void) scha_resource_close(rshandle);
			(void) sleep(1);
			goto retry_rt_type;
		} else if (rc != SCHA_ERR_NOERR) {
			scds_syslog(LOG_ERR, "Failed to retrieve the resource "
				"property %s: %s.", SCHA_TYPE,
				scds_error_string(rc));
			if (print_msg)
				(void) fprintf(stderr,
					gettext("Failed to retrieve the "
						"resource property %s: %s.\n"),
					SCHA_TYPE,
					scds_error_string(rc));
			(void) scha_resource_close(rshandle);
			(void) scha_resourcegroup_close(rghandle);
			return (1);
		}

		scds_syslog_debug(SCDS_DBG_HIGH, "rt=%s", rtname);
		if (strncmp(rtname, REPLICA_RT_NAME,
			(sizeof (REPLICA_RT_NAME) -1)) == 0) {
			is_replica_rt = B_TRUE;
			break;
		} else { /* close this rs handle, go on to next rs */
			(void) scha_resource_close(rshandle);
		}
	} /* end of for loop */

	/* didn't find a resource that's from replica RT, error out */
	if (is_replica_rt != B_TRUE) {
		/*
		 * SCMSGS
		 * @explanation
		 * The weak positive affinity is set on the specified resource
		 * group (from the SAP enqueue server resource group).
		 * However, the specified resource group does not contain any
		 * SAP replica server resources.
		 * @user_action
		 * Create SAP replica server resource in the resource group
		 * specified in the error message.
		 */
		scds_syslog(LOG_WARNING, "There is no SAP replica resource in "
			"the weak positive affinity resource group %s.",
			rgname);
		/* print warning msg to console */
		(void) fprintf(stderr,
			gettext("There is no SAP replica resource in "
				"the weak positive affinity "
				"resource group %s.\n"), rgname);

		(void) scha_resourcegroup_close(rghandle);
		return (NO_REP_RS);
	}

	/*
	 * Find out the state of the replica rs on all nodes.
	 * If the node is the current node, replica_remote = false.
	 * If the node isn't the current one, replica_remote = true.
	 * If the replica rs is not online anywhere, replica_remote = false.
	 */

	/* retrieve the nodelist for the rg */
retry_rg_nodelist:

	rc = scha_resourcegroup_get(rghandle, SCHA_NODELIST, &rgnodes);
	if (rc == SCHA_ERR_SEQID) {
		scds_syslog(LOG_INFO,
			"Retrying to retrieve the resource group "
			"information: %s.", scds_error_string(rc));
		(void) scha_resourcegroup_close(rghandle);
		rghandle = NULL;
		rgnodes = NULL;
		(void) sleep(1);
		rc = scha_resourcegroup_open(rgname, &rghandle);
		if (rc != SCHA_ERR_NOERR) {
			scds_syslog(LOG_ERR,
				"Failed to retrieve the resource group handle:"
				" %s.", scds_error_string(rc));
			if (print_msg)
				(void) fprintf(stderr,
				gettext("Failed to retrieve the resource "
					"group handle: %s.\n"),
					scds_error_string(rc));
			return (1);
		}
		goto retry_rg_nodelist;
	} else if (rc != SCHA_ERR_NOERR || rgnodes == NULL) {
		scds_syslog(LOG_ERR,
			"Failed to retrieve the resource group "
			"property %s: %s.",
			SCHA_NODELIST, scds_error_string(rc));
		if (print_msg) {
			(void) fprintf(stderr,
				gettext("Failed to retrieve the "
					"resource group property %s: %s.\n"),
				SCHA_NODELIST, scds_error_string(rc));
		}
		(void) scha_resourcegroup_close(rghandle);
		return (1);
	}

	/* find out the current node name */
	rc = scha_cluster_getzone(&nodename);
	if (rc != SCHA_ERR_NOERR) {
		ds_internal_error("Unable to get the "
			"name of the local cluster node: %s.",
			scds_error_string(rc));
		(void) scha_resource_close(rshandle);
		(void) scha_resourcegroup_close(rghandle);
		return (1);
	}

	/* find out the repl rs state on all nodes */
	for (j = 0; j < (int)rgnodes->array_cnt; j++) {

retry_rs_state_node:
		rc = scha_resource_get(rshandle,
			SCHA_RESOURCE_STATE_NODE, rgnodes->str_array[j],
			&node_rs_state);
		if (rc == SCHA_ERR_SEQID) {
			scds_syslog(LOG_INFO,
				"Retrying to retrieve the resource "
				"information: %s.", scds_error_string(rc));
			/* close the resource handle */
			(void) scha_resource_close(rshandle);
			rshandle = NULL;
			(void) sleep(1);
			rc = scha_resource_open(rss->str_array[i], rgname,
				&rshandle);
			goto retry_rs_state_node;
		} else if (rc != SCHA_ERR_NOERR) {
			scds_syslog(LOG_ERR,
				"Failed to retrieve the resource "
				"property %s: %s.",
				SCHA_RESOURCE_STATE_NODE,
				scds_error_string(rc));
			(void) scha_resource_close(rshandle);
			(void) scha_resourcegroup_close(rghandle);
			return (1);
		} else if (node_rs_state == SCHA_RSSTATE_ONLINE) {
			/* online on the current node */
			if (strcmp(rgnodes->str_array[j], nodename) == 0)
				*replica_remote = B_FALSE;
			else
				*replica_remote = B_TRUE;

			(void) scha_resource_close(rshandle);
			(void) scha_resourcegroup_close(rghandle);
			return (0);
		} /* else, go on to the next node */
	} /* for all nodes in the rgnodes */

	/* if repl rs is not online on any node, it's offline */
	(void) scha_resource_close(rshandle);
	(void) scha_resourcegroup_close(rghandle);
	*replica_remote = B_FALSE;
	return (0);
}


/*
 * svc_stop():
 *
 * Return 0 on success, 1 on failures.
 */
int
svc_stop(scds_handle_t scds_handle)
{
	int 	rc = 0;
	int	stop_signal = SIGINT;
	scha_extprop_value_t	*stop_signal_prop = NULL;

	/* get stop_signal */
	rc = scds_get_ext_property(scds_handle, "Stop_signal", SCHA_PTYPE_INT,
		&stop_signal_prop);

	if (rc == SCHA_ERR_NOERR) {
		stop_signal = stop_signal_prop->val.val_int;
		/*
		 * SCMSGS
		 * @explanation
		 * Resource property stop_signal is set to a value or has a
		 * default value.
		 * @user_action
		 * No user action is needed.
		 */
		scds_syslog(LOG_INFO,
			"Extension property <Stop_signal> has a value of <%d>",
			stop_signal);
	} else {
		/*
		 * SCMSGS
		 * @explanation
		 * Property stop_signal may not be defined in RTR file.
		 * Continue the process with the default value of SIGINT.
		 * @user_action
		 * No user action is needed.
		 */
		scds_syslog(LOG_INFO,
			"Either extension property <Stop_signal> is not "
			"defined, or an error occurred while retrieving "
			"this property. The default value of SIGINT is "
			"being used.");
	}

	scds_syslog(LOG_NOTICE, "Stopping %s.", APP_NAME);

	if ((rc = scds_pmf_stop(scds_handle, SCDS_PMF_TYPE_SVC, 0, stop_signal,
		INT_MAX)) != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * The attempt to stop the specified application with signal
		 * SIGINT failed. Will attempt to stop it with signal SIGKILL.
		 * @user_action
		 * No user action is needed.
		 */
		scds_syslog(LOG_ERR, "Failed to stop %s with "
			"SIGINT. Will try to stop it with SIGKILL.",
			APP_NAME);
	}

	scds_syslog_debug(SCDS_DBG_HIGH, "after pmf-stop, rc = %d", rc);
	if (rc == SCHA_ERR_NOERR)
		scds_syslog(LOG_INFO, "Successfully stopped the application");

	return (rc);
}


/*
 * svc_wait():
 *
 * Wait for the data service to start up fully before return from start method
 * and report the appl is online.
 */
int
svc_wait(scds_handle_t scds_handle, enq_extprops_t *enqxpropsp)
{
	int		svc_start_timeout, probe_timeout;
	int		probe_rc = 0;
	const char	*rsname = NULL;

	scds_syslog_debug(SCDS_DBG_HIGH, "in svc_wait()... ");

	/* Get the Start timeout, Probe timeout and resource name */
	svc_start_timeout = scds_get_rs_start_timeout(scds_handle);
	probe_timeout = scds_get_ext_probe_timeout(scds_handle);
	rsname = scds_get_resource_name(scds_handle);

	/*
	 * sleep for SVC_WAIT_PCT percentage of start_timeout time
	 * before actually probing the dataservice. This is to allow
	 * the dataservice to be fully up in order to reply to the
	 * probe. NOTE: the value for SVC_WAIT_PCT could be different
	 * for different dataservices.
	 *
	 * Since SAP enqueue server didn't start under PMF, if it dies
	 * during the wait in scds_svc_wait(), it won't have the benefit of
	 * give up and return early.
	 */

	scds_syslog_debug(SCDS_DBG_HIGH, "initial wait ... before probe");

	if (scds_svc_wait(scds_handle, (svc_start_timeout * SVC_WAIT_PCT)/100)
	    != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to start service.");
		return (1);
	}

	scds_syslog_debug(SCDS_DBG_HIGH, "after inital wait, probe now");

	do {
		probe_rc = svc_probe(scds_handle, enqxpropsp,
			probe_timeout, B_FALSE, rsname);
		if (probe_rc == 0) {
			scds_syslog(LOG_NOTICE, "Service is online.");
			return (SCHA_ERR_NOERR);
		} else {	/* ensmon probe not getting thru */
			scds_syslog(LOG_NOTICE,
				"Waiting for %s to come up.", APP_NAME);

			if (scds_svc_wait(scds_handle, SVC_WAIT_TIME)
				!= SCHA_ERR_NOERR) {
				scds_syslog(LOG_ERR,
					"Failed to start service.");
				return (1);
			}
		}

		/* We rely on RGM to timeout and terminate the program */
	} while (1);
}


/*
 * This function starts the fault monitor for a enq resource.
 * This is done by starting the probe under PMF. The PMF tag
 * is derived as <RG-name,RS-name,instance_number.mon>. The restart option
 * of PMF is used but not the "infinite restart". Instead
 * interval/retry_time is obtained from the RTR file.
 */

int
mon_start(scds_handle_t scds_handle)
{
	scha_err_t	err;

	/*
	 * The probe enq_probe is assumed to be available in the same
	 * subdirectory where the other callback methods for the RT are
	 * installed. The last parameter to scds_pmf_start denotes the
	 * child monitor level. Since we are starting the probe under PMF
	 * we need to monitor the probe process only and hence we are using
	 * a value of 0.
	 */

	err = scds_pmf_start(scds_handle, SCDS_PMF_TYPE_MON,
	    SCDS_PMF_SINGLE_INSTANCE, "enq_probe", 0);

	if (err != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to start fault monitor.");
		return (err);
	}

	/*
	 * SCMSGS
	 * @explanation
	 * The fault monitor for this data service was started successfully.
	 * @user_action
	 * No user action is needed.
	 */
	scds_syslog(LOG_INFO, "Successfully started the fault monitor.");

	return (SCHA_ERR_NOERR); /* Successfully started Monitor */
}


/*
 * This function stops the fault monitor for a enq resource.
 * This is done via PMF. The PMF tag for the fault monitor is
 * constructed based on <RG-name_RS-name,instance_number.mon>.
 */

int
mon_stop(scds_handle_t scds_handle)
{
	scha_err_t	err;

	scds_syslog_debug(SCDS_DBG_HIGH, "Calling scds_pmf_stop method");

	err = scds_pmf_stop(scds_handle, SCDS_PMF_TYPE_MON,
	    SCDS_PMF_SINGLE_INSTANCE, SIGKILL,
	    scds_get_rs_monitor_stop_timeout(scds_handle));

	if (err != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to stop fault monitor.");
		return (err);
	}

	scds_syslog(LOG_INFO, "Stopped the fault monitor.");

	return (SCHA_ERR_NOERR); /* Successfully stopped monitor */
}


/*
 * svc_probe(): Do data service specific probing. Return a float value
 * between 0 (success) and 100(complete failure).
 *
 * The probe checks the health check using SAP * utility 'ensmon' with option
 * 2 first. Option 2 checks both enqueue and replica. If ensmon times out,
 * will check enqueue only with option 1 from ensmon.
 *
 * If the health check shows enqueue is not running, flag it as complete
 * failure. If 'ensmon' shows replica is not working (e.g. ensmon 2 times
 * out), only update status.
 *
 * All other system error, treat them as 1/4 of complete failure.
 */
int
svc_probe(scds_handle_t scds_handle, enq_extprops_t *enqxpropsp,
int probe_timeout, boolean_t print_msg, const char *rs_name)
{
	int	rc = 0, exit_code = 0;
	int	probe_status = 0;
	static char	*probe_cmd = NULL;
	static int	old_state = -1;

	scds_syslog_debug(SCDS_DBG_HIGH, "start ensmon check");

	/* ensmon -Hlocalhost -Sserverport 2 */
	probe_cmd = ds_string_format(
		"%s -Hlocalhost -S%d %s > %s/ensmon%s.out.%s",
		enqxpropsp->enq_svr_mon,
		enqxpropsp->enq_port,
		ENQ_MON_OPCODE_2, enqxpropsp->enq_logdir,
		ENQ_MON_OPCODE_2, rs_name);

	if (probe_cmd == NULL) {
		ds_internal_error("unable to create command string to "
			"probe SAP standalone enqueue server");
		return (SCDS_PROBE_COMPLETE_FAILURE/4);
	}

	/* set environment */
	scds_syslog_debug(SCDS_DBG_HIGH, "setting env %s", enqxpropsp->ld_path);
	if ((rc = putenv(enqxpropsp->ld_path)) != 0) {
		ds_internal_error("putenv: %s", strerror(errno));
		if (print_msg)
			(void) fprintf(stderr, gettext(
				"INTERNAL ERROR: putenv: %s.\n"),
				strerror(errno));
		return (SCDS_PROBE_COMPLETE_FAILURE/4);
	}

	/* execute the health check cmd under scds_timerun */
	scds_syslog_debug(SCDS_DBG_HIGH, "probe-cmd = %s", probe_cmd);
	rc = scds_timerun(scds_handle, probe_cmd, probe_timeout,
		SIGKILL, &exit_code);
	scds_syslog_debug(SCDS_DBG_HIGH, "rc from scds-timerun(probe_cmd)=%d",
		rc);

	if (rc != 0) {
		if (rc == SCHA_ERR_TIMEOUT) {
			/* check enqueue only with 'ensmon 1' */
			probe_cmd = ds_string_format(
				"%s -Hlocalhost -S%d %s > %s/ensmon%s.out.%s",
				enqxpropsp->enq_svr_mon,
				enqxpropsp->enq_port,
				ENQ_MON_OPCODE_1, enqxpropsp->enq_logdir,
				ENQ_MON_OPCODE_1,
				rs_name);

			if (probe_cmd == NULL) {
				ds_internal_error("unable to create command "
					"string to probe SAP standalone "
					"enqueue server");
				return (SCDS_PROBE_COMPLETE_FAILURE/4);
			}

			/* execute the health check cmd under scds_timeout */
			scds_syslog_debug(SCDS_DBG_HIGH, "probe-cmd = %s",
				probe_cmd);
			rc = scds_timerun(scds_handle, probe_cmd, probe_timeout,
				SIGKILL, &exit_code);
			scds_syslog_debug(SCDS_DBG_HIGH, "rc from "
				"scds-timerun(probe_cmd)=%d", rc);

			if (rc != 0) {
				if (rc == SCHA_ERR_TIMEOUT) {
					/*
					 * SCMSGS
					 * @explanation
					 * Probing with the specified command
					 * timed out.
					 * @user_action
					 * Other syslog messages occurring
					 * just before this one might indicate
					 * the reason for the failure. You
					 * might consider increasing the
					 * timeout value for the method that
					 * generated the error.
					 */
					scds_syslog(LOG_ERR,
						"Probe command '%s' timed out:"
						" %s.", probe_cmd,
						scds_error_string(rc));
					if (print_msg)
						(void) fprintf(stderr, gettext(
							"Probe command '%s' "
							"timed out: %s.\n"),
							probe_cmd,
							scds_error_string(rc));
					probe_status =
						SCDS_PROBE_COMPLETE_FAILURE/2;
				} else {
					scds_syslog(LOG_ERR,
						"Command %s failed to "
						"complete. Return code is %d.",
						probe_cmd, rc);
					if (print_msg)
						(void) fprintf(stderr, gettext(
						"Command %s failed to complete."
						" Return code is %d.\n"),
						probe_cmd, rc);
					probe_status =
						SCDS_PROBE_COMPLETE_FAILURE/4;
				}
			} else if ((exit_code) == 0) {
				/* enqueue ok, log msg for repl down */
				if (print_msg) {
					/*
					 * SCMSGS
					 * @explanation
					 * SAP utility 'ensmon' with option 2
					 * cannot be completed. However,
					 * 'ensmon' with option 1 finished
					 * successfully. This can happen if
					 * the network of the cluster node
					 * where the SAP replica server was
					 * running becomes unavailable.
					 * @user_action
					 * No user action is needed.
					 */
					scds_syslog(LOG_WARNING,
						"'ensmon 2' timed out. "
						"Enqueue server is "
						"running. But status "
						"for replica server is "
						"unknown. See "
						"%s/ensmon%s.out.%s "
						"for output.",
						enqxpropsp->enq_logdir,
						ENQ_MON_OPCODE_1, rs_name);

					if (old_state != ENQ_OK_REPL_UNKNOWN)
						(void) scha_resource_setstatus(
							scds_get_resource_name(
							scds_handle),
						scds_get_resource_group_name(
							scds_handle),
						SCHA_RSSTATUS_DEGRADED,
							"SAP Replica server "
							"status unknown");
					}
				probe_status = 0;
				old_state = ENQ_OK_REPL_UNKNOWN;
				} else { /* enqueue down */
					if (print_msg)
						/*
						 * SCMSGS
						 * @explanation
						 * The SAP enqueue server is
						 * down. See the output file
						 * listed in the error message
						 * for details. The data
						 * service will fail over the
						 * SAP enqueue server.
						 * @user_action
						 * No user action is needed.
						 */
						scds_syslog(LOG_ERR,
						"SAP enqueue server is not "
						"running. See "
						"%s/ensmon%s.out.%s"
						" for output.",
						enqxpropsp->enq_logdir,
						ENQ_MON_OPCODE_1, rs_name);

					probe_status =
						SCDS_PROBE_COMPLETE_FAILURE;
				}
		} else {
			scds_syslog(LOG_ERR, "Command %s failed to "
				"complete. Return code is %d.",
				probe_cmd, rc);
			if (print_msg)
				(void) fprintf(stderr, gettext(
					"Command %s failed to complete."
					" Return code is %d.\n"),
					probe_cmd, rc);
			probe_status = SCDS_PROBE_COMPLETE_FAILURE/4;
		}
	} else {	/* check return code from ensmon */
		scds_syslog_debug(SCDS_DBG_HIGH,
			"old-state=%d, new exit_code =%d",
			old_state, exit_code);

		switch (exit_code) {
			case ENQ_REPL_OK:
				if (exit_code != old_state) {
					(void) scha_resource_setstatus(
						scds_get_resource_name(
							scds_handle),
						scds_get_resource_group_name(
							scds_handle),
						SCHA_RSSTATUS_OK, "");
				}
				probe_status = 0;
				break;
			case ENQ_OK_REPL_NOTOK:	/* won't take action */
				if (print_msg) {
					/*
					 * SCMSGS
					 * @explanation
					 * Replication is configured to be
					 * enabled. However, the SAP replica
					 * server is not running on the
					 * cluster. The output file listed in
					 * the warning message shows the
					 * complete output from the SAP
					 * command 'ensmon'.
					 * @user_action
					 * This is an informational message,
					 * no user action is needed. The data
					 * service for SAP replica server is
					 * responsible for making the replica
					 * server highly available.
					 */
					scds_syslog(LOG_WARNING,
					"Replication is "
					"enabled in enqueue server, but the "
					"replica server is not running. "
					"See %s/ensmon%s.out.%s for output.",
					enqxpropsp->enq_logdir,
					ENQ_MON_OPCODE_2, rs_name);
					if (old_state != exit_code)
						(void) scha_resource_setstatus(
						scds_get_resource_name(
							scds_handle),
						scds_get_resource_group_name(
							scds_handle),
						SCHA_RSSTATUS_DEGRADED,
							"SAP Replica server is "
							"not running.");
				}
				probe_status = 0;
				break;
			case ENQ_DOWN:
				if (print_msg) {
					scds_syslog(LOG_ERR,
					"SAP enqueue server is not running. "
					"See %s/ensmon%s.out.%s for output.",
					enqxpropsp->enq_logdir,
					ENQ_MON_OPCODE_2, rs_name);
				}
				probe_status = SCDS_PROBE_COMPLETE_FAILURE;
				break;
			case WRONG_PARAM:
				if (print_msg) {
					ds_internal_error(
					"Wrong parameter for "
					"ensmon. See %s/ensmon%s.out.%s for "
					"output.", enqxpropsp->enq_logdir,
					ENQ_MON_OPCODE_2, rs_name);
				}
				probe_status = SCDS_PROBE_COMPLETE_FAILURE;
				break;
			default:	/* other ensmon errors */
				if (print_msg) {
					/*
					 * SCMSGS
					 * @explanation
					 * The specified probe command
					 * finished but the return code is not
					 * zero.
					 * @user_action
					 * See the output file specified in
					 * the error message for the return
					 * code and the detail error message
					 * from SAP utility ensmon.
					 */
					scds_syslog(LOG_ERR,
					"Probe command %s finished with return "
					"code %d. See %s/ensmon%s.out.%s for "
					"output.",
					probe_cmd, exit_code,
					enqxpropsp->enq_logdir,
					ENQ_MON_OPCODE_2, rs_name);
				}
				probe_status = SCDS_PROBE_COMPLETE_FAILURE/4;
				break;
		}
		old_state = exit_code;
	}

	free(probe_cmd);
	return (probe_status);
}


/*
 * This function retrieves the extension properties and save them
 * in a structure.
 * It also checks the sap port.
 */
int
get_enq_extensions(scds_handle_t handle, enq_extprops_t *enqxpropsp,
	boolean_t print_messages)
{
	int			rc = 0;
	char			*service = NULL;
	struct servent		result;
	char			buf[NSS_BUFSIZ];
	struct passwd		*enq_usr_pwd = NULL;
	struct passwd		pwd;
	char			buffer[SCDS_ARRAY_SIZE];

	if (enqxpropsp == NULL) {
		ds_internal_error("Extension property structure was NULL");

		if (print_messages)
			ds_internal_error("Extension property structure "
				"was NULL");
		return (1);
	}

	rc = get_string_ext_property(handle, "SAP_User",
		&enqxpropsp->enq_usr);

	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to retrieve the extension "
			"property %s: %s.", "SAP_User",
			scds_error_string(rc));

		if (print_messages)
			(void) fprintf(stderr,
				gettext("Failed to retrieve the extension "
				"property %s: %s.\n"), "SAP_User",
				scds_error_string(rc));
		return (1);
	}

	/* Retrieve enqueue admin user and retrieve uid and gid of the user */
	enq_usr_pwd = getpwnam_r(enqxpropsp->enq_usr, &pwd, buffer,
		SCDS_ARRAY_SIZE);

	if (enq_usr_pwd == NULL) {
		if (errno == ERANGE) {
			ds_internal_error("Data buffer for user %s "
				"is not big enough",
				enqxpropsp->enq_usr);
			if (print_messages)
				(void) fprintf(stderr,
					gettext("Data buffer for user %s "
						"is not big enough.\n"),
						enqxpropsp->enq_usr);
		} else {
			scds_syslog(LOG_ERR,
				"Failed to retrieve information for user %s.",
				enqxpropsp->enq_usr);
			if (print_messages)
				(void) fprintf(stderr,
					gettext("Failed to retrieve "
					"information for user %s.\n"),
					enqxpropsp->enq_usr);
		}
		return (1);
	} else if ((pwd.pw_dir == NULL) || (strlen(pwd.pw_dir) == 0)) {
		scds_syslog(LOG_ERR, "Home dir is not set for user %s.",
			enqxpropsp->enq_usr);
		if (print_messages) {
			(void) fprintf(stderr, gettext("Home dir is not set"
				" for user %s.\n"), enqxpropsp->enq_usr);
		}
		return (1);
	} else if ((enqxpropsp->enq_home_dir = strdup(pwd.pw_dir)) == NULL) {
			scds_syslog(LOG_ERR, "Out of memory.");
			if (print_messages)
				(void) fprintf(stderr,
					gettext("Out of memory.\n"));
			return (1);
	}

	/* set enqueue gid and uid */
	enqxpropsp->enq_uid = pwd.pw_uid;
	enqxpropsp->enq_gid = pwd.pw_gid;

	rc = get_string_ext_property(handle, "Enqueue_Server",
		&enqxpropsp->enq_svr);

	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to retrieve the extension "
			"property %s: %s.", "Enqueue_Server",
			scds_error_string(rc));

		if (print_messages)
			(void) fprintf(stderr,
				gettext("Failed to retrieve the extension "
				"property %s: %s.\n"), "Enqueue_Server",
				scds_error_string(rc));
		return (1);
	}

	rc = get_string_ext_property(handle, "Enqueue_Profile",
		&enqxpropsp->enq_profile);

	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to retrieve the extension "
			"property %s: %s.", "Enqueue_Profile",
			scds_error_string(rc));

		if (print_messages)
			(void) fprintf(stderr,
				gettext("Failed to retrieve the extension "
				"property %s: %s.\n"), "Enqueue_Profile",
				scds_error_string(rc));
		return (1);
	}

	rc = get_string_ext_property(handle, "Enqueue_Server_Monitor",
		&enqxpropsp->enq_svr_mon);

	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to retrieve the extension "
			"property %s: %s.", "Enqueue_Server_Monitor",
			scds_error_string(rc));

		if (print_messages)
			(void) fprintf(stderr,
				gettext("Failed to retrieve the extension "
				"property %s: %s.\n"),
				"Enqueue_Server_Monitor",
				scds_error_string(rc));
		return (1);
	}

	rc = get_string_ext_property(handle, "Enqueue_Instance_Number",
		&enqxpropsp->enq_instancenum);

	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to retrieve the extension "
			"property %s: %s.", "Enqueue_Instance_Number",
			scds_error_string(rc));

		if (print_messages)
			(void) fprintf(stderr,
				gettext("Failed to retrieve the extension "
				"property %s: %s.\n"),
				"Enqueue_Instance_Number",
				scds_error_string(rc));
		return (1);
	}

	rc = get_string_ext_property(handle, "Log_Directory",
		&enqxpropsp->enq_logdir);

	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to retrieve the extension "
			"property %s: %s.", "Log_Directory",
			scds_error_string(rc));

		if (print_messages)
			(void) fprintf(stderr,
				gettext("Failed to retrieve the extension "
				"property %s: %s.\n"), "Log_Directory",
				scds_error_string(rc));
		return (1);
	}

	/*
	 * print warning msg if sap service sapdpxx is not defined.
	 * xx being the enqueue-intance-number.
	 */
	service = ds_string_format("sapdp%s", enqxpropsp->enq_instancenum);
	if (service == NULL) {
		scds_syslog(LOG_ERR, "Out of memory.");
		return (1);
	} else
		scds_syslog_debug(SCDS_DBG_HIGH, "service=%s", service);

	/* didn't define enqueue port in /etc/services file or nis etc */
	if (getservbyname_r(service, "tcp", &result, buf, NSS_BUFSIZ)
		== NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * The SAP service listed in the message is not defined on the
		 * system.
		 * @user_action
		 * Define the SAP service.
		 */
		scds_syslog(LOG_ERR,
			"Service %s is not defined. ", service);
		if (print_messages)
			(void) fprintf(stderr,
				gettext("Service %s is not defined.\n"),
					service);
		free(service);
		return (1);
	}

	/* port defined */
	enqxpropsp->enq_port = ntohs(result.s_port);
	scds_syslog_debug(SCDS_DBG_HIGH, "service found, port=%d ",
		enqxpropsp->enq_port);

	free(service);

	/*
	 * Retrieve/set logdir path and enqueue_monitor_server path.
	 */
	if ((rc = retrieve_otherprops(enqxpropsp, print_messages))
		!= SCHA_ERR_NOERR) {
		return (1);
	}
	return (rc);
}
