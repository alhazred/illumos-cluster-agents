/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2004 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * enq_monitor_check.c - Monitor Check method for enq
 */

#pragma ident	"@(#)enq_monitor_check.c	1.5	07/06/06 SMI"

#include <rgm/libdsdev.h>
#include "enq.h"

/*
 * Validate check on the service.
 * Added the same replica check logic here as in svc_start() to prevent
 * enqueue being failover to the wrong node.
 */

int
main(int argc, char *argv[])
{
	scds_handle_t   scds_handle;
	enq_extprops_t	enqxpropsp;
	int		rc;
	boolean_t	replica_remote;

	/* Process the arguments passed by RGM and initialize syslog */
	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR) {
		return (1);
	}

	/* Retrieve SAP enq server extension properties */
	rc = get_enq_extensions(scds_handle, &enqxpropsp, B_FALSE);
	if (rc != 0) {
		/* Error msg has been logged */
		return (1);
	}

	rc = svc_validate(scds_handle, &enqxpropsp, B_FALSE);
	scds_syslog_debug(SCDS_DBG_HIGH,
	    "monitor_check method "
	    "was called and returned <%d>.", rc);

	/*
	 * Find out Replica resource is online on a remote node or not.
	 * Currently RGM will not failover enqueue to a node if enqueue
	 * has been failed to start on that node twice w/i pingpong interval,
	 * even replica is online on that node. So to make sure RGM will
	 * not fail over to a node where replica is not running (given it
	 * is online somewhere), check the replica state here like we do
	 * in the start method if the replica-rg is defined (which means
	 * the weak positive affinity is set from the enqueue rg to the
	 * replica rg).
	 */
	if (enqxpropsp.repl_rgname != NULL) {
		if ((rc = check_replica_rs(enqxpropsp.repl_rgname,
			&replica_remote, B_FALSE)) != 0) {
			scds_syslog_debug(SCDS_DBG_HIGH,
				"check-repl rc=%d", rc);
			/*
			 * will not fail monitor_check if there is no
			 * replica rs defined in the affinity rg.
			 */
			if (rc != NO_REP_RS) {
				scds_syslog_debug(SCDS_DBG_HIGH,
					"Error in check_replica_rs. "
					"Should already log msg.");
				rc = 1;
			} else { /* it's ok not to have repl rs */
				rc = 0;
			}
		} else if (replica_remote == B_TRUE) {
			/* replica is running on some other node */
			scds_syslog(LOG_ERR, "Replica is not running on "
				"this node, but is running remotely on "
				"some other node. %s cannot be started "
				"on this node.", APP_NAME);
			rc = 1;
		}
	}

	/*
	 * else either replica affinity is not set, or
	 * replica rg is set w/o replica rs in it; or
	 * the replica rg is not online or online on this node.
	 */

	/* Free up all the memory allocated by scds_initialize */
	scds_close(&scds_handle);

	return (rc);
}
