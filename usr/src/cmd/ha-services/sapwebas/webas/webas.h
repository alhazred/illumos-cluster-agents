/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * webas.h - for Netweaver data services.
 */

#ifndef	_SAP_AS_H
#define	_SAP_AS_H

#pragma ident	"@(#)webas.h	1.6	08/02/13 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <strings.h>
#include <unistd.h>
#include <libintl.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <ctype.h>
#include <scha.h>
#include <rgm/libdsdev.h>
#include <errno.h>
#include <dlfcn.h>
#include <pwd.h>
#include <ds_common.h>
#include <nss_dbdefs.h>
#include "sap_common.h"

#ifdef	__cplusplus
extern "C" {
#endif

#ifndef	SAP_WEBAS_J2EE
#define	SAP_WEBAS_J2EE
#endif

#define	SAP_SVC_NAME		"webas"

/* SAP extensions */
#define	SAP_EXT_SAPSID		"SAP_SID"
#define	SAP_EXT_SAPSYSNUM_AS	"SAP_Instance_Number"
#define	SAP_EXT_SERVICES	"SAP_Instance_Name"
#define	SAP_EXT_USER		"SAP_User"
#define	SAP_EXT_USEPMF		"Webas_Use_Pmf"
#define	SAP_EXT_LOGDIR		"SAP_Logdir"
#define	SAP_EXT_INST_TYPE	"SAP_Instance_Type"
#if 0
#define	SAP_EXT_PROBEDISP 	"Webas_Probe_Dispatcher"
#else
#define	SAP_EXT_PROBEDISP 	"Webas_Enable_Probe"
#endif
#define	SAP_EXT_STOPSAPPCT	"Stop_sap_pct"
#define	SAP_EXT_STARTSAP	"Webas_startup_script"
#define	SAP_EXT_STOPSAP		"Webas_shutdown_script"

/* Data service provided binary to start R3 */
#define	SAP_CMD_STARTR3		"webas_startR3"
#define	SAP_CMD_CLEANIPC	"cleanipc"
#define	SAP_CMD_CLEANIPC_REM	"remove"
#define	SID_LEN 		3
#define	INSTANCE_LEN 		2

#define	SCDS_PATHLEN		(MAXPATHLEN+1)

/* extension property */
typedef struct sap_extprops
{
	char		*sapsid;		/* SAPSID */
	char		*assvcstring;	/* As_services_string */
	char		*asinstancenum;	/* As_instance_id */
	char		*as_start;		/* As_startup_script */
	char		*as_stop;		/* As_shutdown_script */
	char		*user;		/* SAP user */
	boolean_t	use_pmf;
	char		*sap_logdir;	/* SAP Log dir */
	char 		*sap_instance_type;	/* SAP Instance Type */
	boolean_t	probe_disp;
	int		pscheckretry;	/* Process_check_retry */
	int		stopsappct;		/* Stop_sap_pct */
/* just set in validate, no real extensions */
	uid_t		userid;
	gid_t		grpid;
	char		as_hmdir[SCDS_PATHLEN];
	char		*lhost;		/* Logical Host */
	scha_rgmode_t	rg_mode;
	int		pre_sap_kernel7;

} sap_extprops_t;

/* Function to fill in SAP specific properties */
int sap_get_extensions(scds_handle_t handle, sap_extprops_t *sapxprops,
		boolean_t print_messages);

/* Validation and update */
int svc_validate(scds_handle_t handle, sap_extprops_t *sapxprops,
		boolean_t print_messages);
int svc_update(scds_handle_t scds_handle, sap_extprops_t *sapxprops);

/* Basic start and stop functionality */
int svc_start(scds_handle_t handle, sap_extprops_t *sapxprops);
int webas_startR3(scds_handle_t handle, sap_extprops_t *sapxprops);
int svc_stop(scds_handle_t handle, sap_extprops_t *sapxprops, boolean_t val);

/* Fault monitor functions */
int svc_fm_start(scds_handle_t handle);
int svc_fm_stop(scds_handle_t handle);
int svc_fm_check(scds_handle_t handle, sap_extprops_t *sapxprops);

/* Probe related functions */
int svc_probe(scds_handle_t handle, sap_extprops_t *sapxprops,
	int probe_timeout);
int probe_j2ee(scds_handle_t handle, int *probe_rc,
	sap_extprops_t *sapxprops, int timeout);
int probe_disp(scds_handle_t, int *probe_rc,
	sap_extprops_t *sapxprops, int timeout);

int svc_wait(scds_handle_t handle, sap_extprops_t *sapxprops);
int run_cleanipc(scds_handle_t, int *ret, sap_extprops_t *sapxprops);

#define	NOT_RUNNING	0
#define	RUNNING_LOCALLY	1
#define	RUNNING_REMOTLY	2

#define	DEFAULT_NONE		0
#define	DEFAULT_STARTSAP	1
#define	DEFAULT_STOPSAP		2

#define	SAP_PROBE_OK		0
#define	SAP_PROBE_FAILED	1

#define	PROBEPERCENTAGE		0.9
#define	CONVERT2MS			1000.0

#define	HTTP_HEAD		"HTTP/1.1"

#ifdef	__cplusplus
}
#endif

#endif /* _SAP_AS_H */
