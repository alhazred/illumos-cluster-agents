/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2004 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * webas_svc_start.c - for Netweaver data services.
 */

#pragma ident	"@(#)webas_svc_start.c	1.4	07/06/06 SMI"

#include "webas.h"

int
main(int argc, char *argv[])
{
	scds_handle_t		scds_handle;
	sap_extprops_t		sapxprops;
	int 			rc;

	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR)
		exit(1);

	if (sap_get_extensions(scds_handle, &sapxprops, B_FALSE)
		!= SCHA_ERR_NOERR) {
		scds_close(&scds_handle);
		exit(1);
	}

	/* make sure everything looks ok */
	if (svc_validate(scds_handle, &sapxprops, B_FALSE) != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
			"Failed to validate configuration.");
		exit(1);
	}

	/* start SAP application server */
	rc = svc_start(scds_handle, &sapxprops);
	if (rc != 0) {
		scds_close(&scds_handle);
		return (rc);
	}
	scds_syslog_debug(DBG_LEVEL_HIGH,
		"Done with svc_start, calling svc_wait.");

	/*
	 * wait until sap is actually up and running which indicated by
	 * its ability to probe
	 */

	rc = svc_wait(scds_handle, &sapxprops);

	scds_syslog_debug(DBG_LEVEL_HIGH,
		"Done with svc_wait which returned <%d>.", rc);
	if (rc == 0) {
		scds_syslog(LOG_INFO, "Successfully started the service.");
	} else {
		scds_syslog(LOG_ERR, "Failed to start service.");
	}

	/* Free up the Environment resources that were allocated */
	scds_close(&scds_handle);
	return (rc);
}
