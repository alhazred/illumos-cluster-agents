/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * webas.c - for Netweaver data services.
 */

#pragma ident	"@(#)webas.c	1.23	08/10/14 SMI"

#include "webas.h"


#define	SCDS_CMD_SIZE			2048
#define	DISCONNECT_TO			5

const char	probe_xml[] =
"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
"       <scenario>"
"           <scenname>Server testing</scenname>"
"           <scenversion>001 </scenversion>"
"           <sceninst>001</sceninst>"
"               <component>"
"                    <compname>HTTP</compname>"
"                    <compversion>001</compversion>"
"                 </component>"
"       </scenario>";

/*
 * Do SAP specific validation of a given resource configuration
 * Called by start/validate/update/monitor methods.
 * Return 0 on success, > 0 on failures.
 *
 * svc_validate checks the following:
 * - check for dependency on HASP resource
 * - check SAP extension properties are set correctly:
 * 	o SID, turns lower to upper case, length 3 digit!
 *	o check if start/stop scripts are set,
 *	  if not, set them to default /usr/sap/SID/SYS/exe/run/sapstart/stop
 *	o check for sap admin user, if not set to SIDadm (lower case)
 * 	o check for cleanipc, dpmon
 * - monitor retry count / interval and probe timeout
 */

int
svc_validate(scds_handle_t scds_handle, sap_extprops_t *sapxprops,
		boolean_t print_messages)
{
	int			i, j;
	int			hasprc;
	char			executable[SCDS_PATHLEN];
	char			*utils[] = { "cleanipc", "dpmon", NULL };
	struct passwd		pwd;
	char			getxbyzbuffer[NSS_BUFSIZ];
	char			*slash;
	char			service[NSS_BUFSIZ];
	char			sap_profile[SCDS_CMD_SIZE];
	struct servent		serv;
	struct stat		sbuf;
	int			rc = 0;
	scds_net_resource_list_t *snrlp = NULL;
	int			flag = 0;

	/*
	* Check if the Resource Group is in scalable mode
	*/
	sapxprops->rg_mode = scds_get_rg_rg_mode(scds_handle);
	if (sapxprops->rg_mode == RGMODE_SCALABLE) {
		/* Skip logical hostname validation */
		sapxprops->lhost = strdup("localhost");
		goto shared_ip_not_required;
	}
	/*
	 * check network address resources for this resource
	 * Return an error if there is an error when trying to get the
	 * available network address resources for this resource
	 * and if no Failover IP resources are created in this RG.
	 */
	if ((rc = scds_get_rs_hostnames(scds_handle, &snrlp))
		!= SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		    "Error in trying to access the configured network "
		    "resources : %s.", scds_error_string(rc));
		if (print_messages) {
			(void) fprintf(stderr,
				gettext("Error in trying to access the "
				"configured network resources : %s."),
				scds_error_string(rc));
		}
		return (1);
	}

	/* return error if there is no network address resource */
	if (snrlp == NULL || snrlp->num_netresources == 0) {
		scds_syslog(LOG_ERR,
		    "No network address resource in resource group.");
		if (print_messages) {
			(void) fprintf(stderr,
				gettext("No network address resource in "
				"resource group."));
		}
		return (1);
	}

shared_ip_not_required:

	/* check SAPSID */

	if (strlen(sapxprops->sapsid) != SID_LEN) {
		scds_syslog(LOG_ERR,
			"SID <%s> is smaller than or exeeds 3 digits.",
			sapxprops->sapsid);
		if (print_messages) {
			(void) fprintf(stderr,
					gettext("%s is not a valid "
						"SAP system id.\n"),
					sapxprops->sapsid);
		}
		return (1);
	} else {
		for (i = 0; i < SID_LEN; i++) {
			sapxprops->sapsid[i] =
				(char)toupper(sapxprops->sapsid[i]);
		}
	}

	if (sapxprops->asinstancenum) {
		if (strlen(sapxprops->asinstancenum) != INSTANCE_LEN) {
			scds_syslog(LOG_ERR,
				"Instance number <%s> does not consist of "
				"2 numeric characters.",
				sapxprops->asinstancenum);
			if (print_messages) {
				(void) fprintf(stderr, gettext("%s is not "
					"a valid SAP instance number.\n"),
					sapxprops->asinstancenum);
			}
			return (1);
		}
		if (!isdigit(sapxprops->asinstancenum[0]) ||
			!isdigit(sapxprops->asinstancenum[1])) {
			scds_syslog(LOG_ERR,
				"Instance number <%s> contains non-numeric "
				"character.",
				sapxprops->asinstancenum);
			if (print_messages) {
				(void) fprintf(stderr, gettext("%s is not "
					"a valid SAP instance number"),
					sapxprops->asinstancenum);
			}
			return (1);
		}
	} else {
		scds_syslog(LOG_ERR,
			"Internal error; SAP instance id set to NULL.");
		return (1);
	}

	if (strcmp(sapxprops->sap_instance_type, "ABAP") == 0) {
		(void) snprintf(service, NSS_BUFSIZ, "sapdp%s",
				sapxprops->asinstancenum);

		if (getservbyname_r(service, "tcp",
			&serv, getxbyzbuffer, NSS_BUFSIZ) == NULL) {
			/*
			 * SCMSGS
			 * @explanation
			 * The data service cannot retrieve the required
			 * service.
			 * @user_action
			 * Check the service entries on all relevant cluster
			 * nodes.
			 */
			scds_syslog(LOG_ERR,
				"Cannot retrieve service <%s>.", service);
			if (print_messages) {
				(void) fprintf(stderr, gettext("Cannot "
					"retrieve service sapdp%s"),
					sapxprops->asinstancenum);
			}
			return (1);
		}
	}

	if (sapxprops->user != NULL) {
		if (strlen(sapxprops->user) == 0) {
			free(sapxprops->user); /* has been "" */
			/*
			 * if the user is not set, we set it to
			 * sidadm, where sid is the lowercase of
			 * SAP_SID extension property.
			 */
			sapxprops->user = strdup("SIDadm");
			if (sapxprops->as_start == NULL) {
				ds_internal_error("strdup(): %s",
					strerror(errno)); /*lint !e746 */
				return (1);
			}
			for (i = 0; i < SID_LEN; i++) {
				sapxprops->user[i] =
					(char)tolower(sapxprops->sapsid[i]);
			}
		}
		if (getpwnam_r(sapxprops->user,
					&pwd,
					getxbyzbuffer,
					NSS_BUFLEN_PASSWD) == 0) {
			scds_syslog(LOG_ERR,
				"Cannot retrieve user id %s.",
				sapxprops->user);
			if (print_messages) {
				(void) fprintf(stderr,
					gettext("Cannot retrieve "
						"user id %s.\n"),
					sapxprops->user);
			}
			return (1);
		} else {
			sapxprops->userid = pwd.pw_uid;
			sapxprops->grpid = pwd.pw_gid;
			(void) snprintf(sapxprops->as_hmdir,
				SCDS_PATHLEN, pwd.pw_dir);
		}
	} else {
		scds_syslog(LOG_ERR,
			"Internal error; SAP User set to NULL.");
		return (1);
	}

	if (validate_project_name(scds_handle, sapxprops->user) != 0) {
		return (1);
	}


	/* check for a HA StoragePlus resource */
	hasprc = validate_hasp(print_messages, scds_handle);

	scds_syslog_debug(DBG_LEVEL_HIGH,
		"validate_hasp returned <%d>\n", hasprc);

	switch (hasprc) {
		case SCDS_HASP_ONLINE_NOT_LOCAL:
			goto online_not_local;
		case SCDS_HASP_ONLINE_LOCAL:
			break;
		case SCDS_HASP_NO_RESOURCE:
			break;
		default:
			return (1);
	}

	/*
	 * Check the SAP users home dir
	 */

	if ((stat(sapxprops->as_hmdir, &sbuf) != 0) ||
			(!S_ISDIR(sbuf.st_mode))) {
		scds_syslog(LOG_ERR,
			"Cannot stat() home directory: %s",
			sapxprops->as_hmdir);
		return (1);
	}


	/*
	 * start and stop script, if not set use
	 * /usr/sap/<SID>/SYS/exe/run/startsap / stopsap
	 */

	if (!strlen(sapxprops->as_start)) {
		free(sapxprops->as_start);
		sapxprops->as_start = (char *)malloc(SCDS_PATHLEN);
		if (sapxprops->as_start == NULL) {
			ds_internal_error("malloc(): %s",
				strerror(errno));
			return (1);
		}
		(void) snprintf(sapxprops->as_start, SCDS_PATHLEN,
			"/usr/sap/%s/SYS/exe/run/startsap",
			sapxprops->sapsid);
	}

	slash = strrchr(sapxprops->as_start, '/');
	if (slash == NULL || strlen(slash) == 1) {
		scds_syslog(LOG_ERR,
			"%s is not an absolute path.",
			sapxprops->as_start);
		if (print_messages) {
			(void) fprintf(stderr,
				gettext("%s is not an absolute path."),
				sapxprops->as_start);
		}
		return (1);
	}

	if (ds_validate_file(print_messages, S_IRUSR | S_IXUSR,
			sapxprops->as_start) != SCHA_ERR_NOERR)
		return (1);

	if (!strlen(sapxprops->as_stop)) {
		free(sapxprops->as_stop);
		sapxprops->as_stop = (char *)malloc(SCDS_PATHLEN);
		if (sapxprops->as_start == NULL) {
			ds_internal_error("malloc(): %s",
				strerror(errno));
			return (1);
		}
		(void) snprintf(sapxprops->as_stop, SCDS_PATHLEN,
			"/usr/sap/%s/SYS/exe/run/stopsap",
			sapxprops->sapsid);
	}

	slash = strrchr(sapxprops->as_stop, '/');
	if (slash == NULL || strlen(slash) == 1) {
		scds_syslog(LOG_ERR,
			"%s is not an absolute path.",
			sapxprops->as_stop);
		if (print_messages) {
			(void) fprintf(stderr,
				gettext("%s is not an absolute path."),
				sapxprops->as_stop);
		}
		return (1);
	}

	if (ds_validate_file(print_messages, S_IRUSR | S_IXUSR,
			sapxprops->as_stop) != SCHA_ERR_NOERR)
		return (1);

	/* /usr/sap/<SID>/SYS/exe/run/cleanipc */
	/* /usr/sap/<SID>/SYS/exe/run/dpmon */

	for (i = 0; utils[i] != NULL; i++) {
		/* run this part only if validation worked */
		if ((strcmp(sapxprops->sap_instance_type, "J2EE") == 0) &&
					(strcmp(utils[i], "dpmon") == 0)) {
			continue;
		}

		(void) snprintf(executable, SCDS_PATHLEN,
			"/usr/sap/%s/SYS/exe/run/%s",
			sapxprops->sapsid,
			utils[i]);

		if (ds_validate_file(print_messages, S_IRUSR | S_IXUSR,
			executable) != SCHA_ERR_NOERR)
			return (1);

	}

	if (sapxprops->rg_mode == RGMODE_SCALABLE) {
		/* Skip profile name validation */
		goto skip_profile_validation;
	}
	/*
	 * Validate SAP ABAP/J2EE instance profiles
	 * Search for the profiles under /usr/sap/{SAPID}/SYS/profile/
	 * The validation succeeds if a profile
	 * START_{abap/j2ee_instance}_{$LHOSTNAME} is found.
	 */
	for (i = 0; i < snrlp->num_netresources; i++) {
		for (j = 0;
			j < snrlp->netresources[i].num_hostnames; j++) {

			/* Frame the profile name */
			(void) snprintf(sap_profile, SCDS_CMD_SIZE,
				"/usr/sap/%s/SYS/profile/START_%s_%s",
				sapxprops->sapsid, sapxprops->assvcstring,
				snrlp->netresources[i].hostnames[j]);

			/* make sure instance file exists */
			if (stat(sap_profile, &sbuf) != 0) {
				if (errno == ENOENT) {
					(void) snprintf(sap_profile,
					SCDS_CMD_SIZE,
					"/usr/sap/%s/SYS/profile/%s_%s_%s",
					sapxprops->sapsid,
					sapxprops->sapsid,
					sapxprops->assvcstring,
					snrlp->netresources[i].hostnames[j]);
					if (stat(sap_profile, &sbuf) != 0) {
						if (errno == ENOENT) {
							continue;
						}
					}
				}
			}
			flag = 1;
			sapxprops->lhost=
			strdup(snrlp->netresources[i].hostnames[j]);
		}
		/* profile is found */
		if (flag)
			break;
	}
	/* The SAP profile doesn't exist */
	if (flag == 0) {
		/*
		* SCMSGS
		* @explanation
		* The check for SAP profile failed. The start up of
		* sap instance requires the profile to be present.
		* @user_action
		* Make sure the sap profile is present under
		* /usr/sap/SAPSID/SYS/profile/ directory.
		*/
		scds_syslog(LOG_ERR,
				"The profile for SAP %s instance "
				"%s does not exist", sapxprops->sapsid,
				sap_profile);
		if (print_messages) {
			(void) fprintf(stderr, gettext(
				"The profile for SAP %s instance "
				"%s does not exist"), sapxprops->sapsid,
				sap_profile);
		}
		return (1);
	}
	/* check instance file is owned by sap_user */
	if (ds_validate_file_owner(print_messages, sapxprops->userid,
		sapxprops->grpid, "%s", sap_profile))
		return (1);

skip_profile_validation:
	/* set the log dir path */
	if (strcmp(sapxprops->sap_logdir, "") == 0) {
	/* no input value, set it to home dir of SAP User */
		sapxprops->sap_logdir = strdup(sapxprops->as_hmdir);
		if (sapxprops->sap_logdir == NULL) {
			scds_syslog(LOG_ERR, "Out of memory.");
			if (print_messages)
				(void) fprintf(stderr,
					gettext("Out of memory.\n"));
			return (1);
		}
	}

	scds_syslog_debug(SCDS_DBG_HIGH, "logdir=%s, homedir=%s",
		sapxprops->sap_logdir, sapxprops->as_hmdir);

	/* log dir is an absolute path */
	if (sapxprops->sap_logdir[0] != '/') {
		scds_syslog(LOG_ERR, "%s is not an absolute path.",
			sapxprops->sap_logdir);
	if (print_messages) {
		(void) fprintf(stderr, gettext("%s is not an absolute "
			"path.\n"), sapxprops->sap_logdir);
		}
		return (1);
	}

	/* make sure logdir exists */
	if (ds_validate_file(print_messages, S_IRUSR | S_IXUSR | S_IFDIR,
		sapxprops->sap_logdir))
		return (1);

	/* check logdir is owned by sap_user */
	if (ds_validate_file_owner(print_messages, sapxprops->userid,
		sapxprops->grpid, "%s", sapxprops->sap_logdir))
		return (1);

online_not_local:

	/* check monitor retry count */
	if (scds_get_ext_monitor_retry_count(scds_handle) <= 0) {
		scds_syslog(LOG_ERR, "Property %s is not set.",
			"Monitor_retry_count");
		if (print_messages) {
			(void) fprintf(stderr, gettext("Property %s is not "
				"set.\n"), "Monitor_retry_count");
		}
		return (1);
	}
	if (scds_get_ext_monitor_retry_interval(scds_handle) <= 0) {
		scds_syslog(LOG_ERR, "Property %s is not set.",
			"Monitor_retry_interval");
		if (print_messages) {
			(void) fprintf(stderr, gettext("Property %s is not "
				"set.\n"), "Monitor_retry_interval");
		}
		return (1);
	}

	if (scds_get_ext_probe_timeout(scds_handle) <= 0) {
		scds_syslog(LOG_ERR,
		    "Property %s is not set.", "Probe_timeout");
		if (print_messages) {
			(void) fprintf(stderr, gettext("Property %s is not "
				"set.\n"), "Probe_timeout");
		}
		return (1);
	}

	return (0);
}

/*
 * Start the SAP application server processes either under pmf or not,
 * just start monitoring it if it's already up and running.
 *
 * The first thing to check is whether sap is already up and running. If sap
 * is not up, then go ahead with the starting up. Otherwise, depending on
 * the value of use_pmf, either just start monitoring it (FALSE) or bail
 * out (TRUE).
 * If pmf is used it's important to remove the saposcol from the startup
 * script sapstart.
 *
 * Again, depending on the setting of use_pmf this routine either calls
 * scds_pmf_start() on a further executable ''webas_startR3'' or directly
 * calls into routine webas_startR3 in order to run the r3/j2ee start script.
 */

int
svc_start(scds_handle_t scds_handle, sap_extprops_t *sapxprops)
{
	int			rc;
	char			sapstart[SCDS_ARRAY_SIZE];

	scds_syslog_debug(DBG_LEVEL_HIGH, "Calling START method.");


	rc = svc_probe(scds_handle, sapxprops,
		scds_get_rs_start_timeout(scds_handle));
	if (rc == SCHA_ERR_NOERR) {
		if (sapxprops->use_pmf == B_FALSE)
			return (0);
		/*
		 * SCMSGS
		 * @explanation
		 * The data service detected a running SAP instance.
		 * Webas_Use_Pmf is specified, thus the data service cannot
		 * start probing the instance.
		 * @user_action
		 * Either set Webas_Use_Pmf to False, or stop the service.
		 */
		scds_syslog(LOG_NOTICE, "Service is already "
			"running. Cannot use PMF.");
		rc = scha_control(SCHA_IGNORE_FAILED_START,
			scds_get_resource_group_name(scds_handle),
			scds_get_resource_name(scds_handle));
		/* fail regardless */
		return (1);
	}

	scds_syslog_debug(DBG_LEVEL_HIGH,
		"svc_probe() returned %d", rc);


	/*
	 * now start the other group of sap processes by calling
	 * subroutine sap_ci_startR3().
	 * change this coding if scds_pmf_start_env becomes
	 * available.
	 */

	if (sapxprops->use_pmf == B_TRUE) {

		/* form path to start SAP start up command */
		(void) snprintf(sapstart, sizeof (sapstart),
			"%s/%s -R %s -T %s -G %s",
			scds_get_rt_rt_basedir(scds_handle),
			SAP_CMD_STARTR3,
			scds_get_resource_name(scds_handle),
			scds_get_resource_type_name(scds_handle),
			scds_get_resource_group_name(scds_handle));

		/* start the startup program under pmf */
		rc = scds_pmf_start(scds_handle, SCDS_PMF_TYPE_SVC,
			SCDS_PMF_SINGLE_INSTANCE, sapstart, -1);

		if (rc == SCHA_ERR_NOERR) {
			/*
			 * SCMSGS
			 * @explanation
			 * Informational message. SAP is being started under
			 * the control of Process Monitoring Facility (PMF).
			 * @user_action
			 * No action needed.
			 */
			scds_syslog(LOG_INFO,
				"Started SAP processes under PMF "
				"successfully.");
		} else {
			/*
			 * SCMSGS
			 * @explanation
			 * The data service failed to start the SAP processes.
			 * @user_action
			 * Check the script and execute manually.
			 */
			scds_syslog(LOG_ERR,
				"Failed to start SAP processes with %s.",
				sapstart);
		}

	} else {

		/* start the script w/o pmf */
		rc = webas_startR3(scds_handle, sapxprops);
		if (rc == SCHA_ERR_NOERR) {
			/*
			 * SCMSGS
			 * @explanation
			 * The data service started the processes w/o PMF.
			 * @user_action
			 * Informational message. No user action is needed.
			 */
			scds_syslog(LOG_INFO,
				"Started SAP processes successfully w/o PMF.");
		}
		/* error messages should be done by webas_startR3, fall thru */

	}

	/* return success/failure status */
	return (rc);
}


int
svc_wait(scds_handle_t scds_handle, sap_extprops_t *sapxprops)
{
	int	probe_timeout;
	int	rc = 0;

	/* Get Probe timeout value */
	probe_timeout = scds_get_ext_probe_timeout(scds_handle);

	do {
		rc = svc_probe(scds_handle, sapxprops, probe_timeout);

		if (rc == SCHA_ERR_NOERR) {
			scds_syslog_debug(DBG_LEVEL_HIGH,
				"Service is online.");
			return (0);
		}

		scds_syslog_debug(DBG_LEVEL_HIGH,
			"Waiting for dispatcher to come up.");

		/*
		 * SCMSGS
		 * @explanation
		 * Waiting for the SAP application to startup.
		 * @user_action
		 * Informational message. No user action is needed.
		 */
		scds_syslog(LOG_NOTICE, "Waiting for the SAP Application"
			" server to come online.");

		/* wait for 10 seconds if probe isn't there yet! */
		if (scds_svc_wait(scds_handle, 10) != SCHA_ERR_NOERR) {
			scds_syslog(LOG_ERR, "Failed to start service.");
		}

	/* we rely on rgm to timeout */
	} while (1);
}



/*
 * run cleanipc command with system number and remove as
 * arguments. Can be run under scds_timerun, it doesn't require
 * a specific user/environment.
 */
int
run_cleanipc(scds_handle_t handle,
	int *cipc_rc, sap_extprops_t *sapxprops)
{
	char	cleanipccmd[SCDS_ARRAY_SIZE];
	char	*cmd_string;
	int	rc;
	int	cmd_exit_status;

	/*
	 * Form command to cleanipc's
	 * /usr/sap/<SAPSID>/SYS/exe/run/cleanipc <sapsysnum> remove
	 * run cleanipc i/o to have the shm segs removed
	 */
	(void) snprintf(cleanipccmd, SCDS_ARRAY_SIZE,
		"/usr/sap/%s/SYS/exe/run/%s",
		sapxprops->sapsid,
		SAP_CMD_CLEANIPC);

	/*
	 * Use the cleanipc program from the SAP installation.
	 */
	scds_syslog_debug(DBG_LEVEL_HIGH,
		"Running cleanipc with command %s.", cleanipccmd);

	cmd_string = ds_string_format("%s %s %s",
			cleanipccmd, sapxprops->asinstancenum,
			SAP_CMD_CLEANIPC_REM);

	if (cmd_string == NULL) {
		ds_internal_error("Unable to allocate string for "
			"cleanipc command.");
		return (SCHA_ERR_INTERNAL);
	}

	rc = timeout_run(handle, cmd_string, B_FALSE,
			5, &cmd_exit_status,
			sapxprops->sapsid,
			sapxprops->as_hmdir,
			sapxprops->user,
			sapxprops->userid,
			sapxprops->grpid,
			sapxprops->asinstancenum,
			sapxprops->sap_logdir);

	switch (rc) {
		case SCHA_ERR_TIMEOUT:
			scds_syslog(LOG_ERR,
				"Command %s failed to complete. "
				"HA-SAP will continue to start SAP.",
				cleanipccmd);
			break;
		case SCHA_ERR_INTERNAL:
			/*
			 * SCMSGS
			 * @explanation
			 * The data service detected an internal error from
			 * SCDS.
			 * @user_action
			 * Informational message. No user action is needed.
			 */
			scds_syslog(LOG_ERR,
				"INTERNAL ERROR: error occurred while "
				"launching command <%s>", cleanipccmd);
			break;
		case SCHA_ERR_INVAL:
			scds_syslog(LOG_ERR,
				"Cannot execute %s: %s.",
				cleanipccmd, cmd_exit_status);
			break;
		case SCHA_ERR_NOERR:

			/*
			 * handle return value from cleanipc
			 * 0 if segments were there, 1 if not, anything
			 * else unknown
			 */

			*cipc_rc = cmd_exit_status;

			if (cmd_exit_status == 0 || cmd_exit_status == 1) {
				scds_syslog_debug(DBG_LEVEL_HIGH,
					"cleanipc successfully run, "
					"returned %d.",
					cmd_exit_status);
			} else {
				scds_syslog_debug(LOG_ERR,
					"cleanipc returned %d.",
					cmd_exit_status);
			}
			break;
		default:
			/*
			 * SCMSGS
			 * @explanation
			 * The data service detected an error from
			 * scds_timerun().
			 * @user_action
			 * Informational message. No user action is needed.
			 */
			scds_syslog(LOG_ERR,
				"Unhandled return code from "
				"scds_timerun() for %s",
				cleanipccmd);
	}

	free(cmd_string);

	return (rc);

}

/* Actually startup SAP R/3 */

int
webas_startR3(scds_handle_t handle, sap_extprops_t *sapxprops)
{
	int 		rc;
	int		probe_rc;
	int		status;
	char		*start_cmd;

	/*
	 * activate this part if we want to enable J2EE standalone probe
	 * if (sapxprops->probe_disp == B_TRUE) {
	 */

	/* run cleanipc to remove left over shm segs */
	rc = run_cleanipc(handle, &probe_rc, sapxprops);
	if (rc != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * The data service could not execute cleanipc command.
		 * @user_action
		 * Ensure cleanipc is in the expected path
		 * (/usr/sap/<SID>/SYS/exe/run) and is executable.
		 */
		scds_syslog(LOG_ERR,
			"Failure running cleanipc; %d.",
			rc);
		/* continue even if it failed */
	}

	/*
	 * run either the configured script or the default startup script
	 * with the appropriate args (startsap <instance name> <logical-host>)
	 */

	if (sapxprops->pre_sap_kernel7 > 0) {
		if (sapxprops->rg_mode != RGMODE_SCALABLE) {
			start_cmd = ds_string_format("%s %s %s %s",
					sapxprops->as_start, "r3",
					sapxprops->assvcstring,
					sapxprops->lhost);
		} else {
			start_cmd = ds_string_format("%s %s %s %s",
					sapxprops->as_start, "r3",
					sapxprops->assvcstring,
					"localhost");
		}
	} else {
		start_cmd = ds_string_format("%s %s %s",
			sapxprops->as_start, "r3", sapxprops->assvcstring);
	}
	if (start_cmd == NULL) {
		ds_internal_error("Unable to allocate string for "
			"start command.");
		return (SCHA_ERR_INTERNAL);
	}

	scds_syslog_debug(DBG_LEVEL_HIGH,
		"Trying to start SAP with command %s.", sapxprops->as_start);

	rc = timeout_run(handle, start_cmd, B_TRUE,
		0, &status,
		sapxprops->sapsid,
		sapxprops->as_hmdir,
		sapxprops->user,
		sapxprops->userid,
		sapxprops->grpid,
		sapxprops->asinstancenum,
		sapxprops->sap_logdir);

	switch (rc) {
		case SCHA_ERR_TIMEOUT:
			scds_syslog(LOG_ERR,
				"Start command %s timed out.",
				start_cmd);
			free(start_cmd);
			return (SCHA_ERR_TIMEOUT);
		case SCHA_ERR_NOERR:
			scds_syslog_debug(DBG_LEVEL_HIGH,
				"Start command %s succeeded, returned %d.",
				start_cmd, status);
			if (status) {
				/*
				 * SCMSGS
				 * @explanation
				 * The data service failed to run the start
				 * command.
				 * @user_action
				 * Ensure that the start command is in the
				 * expected path (/usr/sap/<SID>/SYS/exe/run)
				 * and is executable.
				 */
				scds_syslog(LOG_ERR,
					"Start command %s returned error, %d."
					"Aborting startup of resource.",
					start_cmd, status);
				rc = scha_control(SCHA_IGNORE_FAILED_START,
					scds_get_resource_group_name(handle),
					scds_get_resource_name(handle));
				if (rc != SCHA_ERR_NOERR) {
					/*
					 * SCMSGS
					 * @explanation
					 * The data service detected an
					 * internal error from scds.
					 * @user_action
					 * Informational message. No user
					 * action is needed.
					 */
					scds_syslog(LOG_ERR,
						"scha_control() returned "
						"error %d for %s in %s.",
						rc,
						scds_get_resource_name(handle),
						scds_get_resource_group_name
							(handle));
					free(start_cmd);
					return (rc);
				}
				return (1);
			}
			break;
		default:
			scds_syslog(LOG_ERR,
				"Start command %s failed with error %s.",
				start_cmd, scds_error_string(rc));
			break;
	}

	free(start_cmd);
	return (rc);
}

/*
 * Stop SAP processes which started up under PMF with tag index 0. This
 * also include the OS collector process saposcol, if it's not removed
 * from the start-up sequence.
 * If pmf is not used, just run the stop script.
 */

#define	STOPPERCENTAGE	85
int
svc_stop(scds_handle_t scds_handle, sap_extprops_t *sapxprops,
	boolean_t failed_validate)
{
	int 		rc = 0;
	int		stopsap_to; /* timeouts */
	int		probe_rc;
	char		*stop_cmd = NULL;
	int		status = 0;

	/* Now stop the processes */
	scds_syslog_debug(DBG_LEVEL_HIGH, "Stopping WebAS.");

	/* tell pmf to stop monitoring the instance */
	if (sapxprops->use_pmf == B_TRUE) {
		stopsap_to = (scds_get_rs_stop_timeout(scds_handle) *
				STOPPERCENTAGE) / 100;
		rc = scds_pmf_stop_monitoring(scds_handle, SCDS_PMF_TYPE_SVC,
			SCDS_PMF_SINGLE_INSTANCE);
		if (rc != SCHA_ERR_NOERR) {
			scds_syslog(LOG_ERR,
				"Failed to stop fault monitor, %s.",
				scds_error_string(rc));
			/* continue even if this fails */
		}
	} else {
		stopsap_to = (scds_get_rs_stop_timeout(scds_handle) *
				STOPPERCENTAGE) / 100;
	}

	if (!failed_validate) {
		if (sapxprops->pre_sap_kernel7 > 0) {
			if (sapxprops->rg_mode != RGMODE_SCALABLE) {
				stop_cmd = ds_string_format("%s %s %s %s",
					sapxprops->as_stop, "r3",
					sapxprops->assvcstring,
					sapxprops->lhost);
			} else {
				stop_cmd = ds_string_format("%s %s %s %s",
					sapxprops->as_stop, "r3",
					sapxprops->assvcstring,
					"localhost");
			}
		} else {
			stop_cmd = ds_string_format("%s %s %s",
					sapxprops->as_stop, "r3",
					sapxprops->assvcstring);
		}
		if (stop_cmd == NULL) {
			ds_internal_error("Unable to allocate string for "
				"stop command.");
			/* let pmf do the work, if configured */
			goto pmf_only;
		}

		scds_syslog_debug(DBG_LEVEL_HIGH,
			"Trying to stop SAP with command %s", stop_cmd);

		rc = timeout_run(scds_handle, stop_cmd, B_TRUE,
				stopsap_to, &status,
				sapxprops->sapsid,
				sapxprops->as_hmdir,
				sapxprops->user,
				sapxprops->userid,
				sapxprops->grpid,
				sapxprops->asinstancenum,
				sapxprops->sap_logdir);

		switch (rc) {
			case SCHA_ERR_TIMEOUT:
				/*
				 * stopsap timed out, depending on usage of
				 * pmf we have to continue, though.
				 */
				scds_syslog(LOG_ERR,
					"Stop command %s timed out.",
					stop_cmd);
				if (sapxprops->use_pmf == B_TRUE) {
					break;
				} else {
					free(stop_cmd);
					return (SCHA_ERR_TIMEOUT);
				}
			case SCHA_ERR_NOERR:
				scds_syslog_debug(DBG_LEVEL_HIGH,
					"Stop command %s succeeded, "
					"returned %d.", stop_cmd, status);
				if (status) {
					scds_syslog(LOG_ERR,
						"Stop command %s returned "
						"error, %d.", stop_cmd, status);
				}
				break;
			default:
				/*
				 * SCMSGS
				 * @explanation
				 * The data service detected an error running
				 * the stop command.
				 * @user_action
				 * Ensure that the stop command is in the
				 * expected path (/usr/sap/<SID>/SYS/exe/run)
				 * and is executable.
				 */
				scds_syslog(LOG_ERR,
					"Stop command for %s failed with "
					"error %s.", stop_cmd,
					scds_error_string(rc));
				/*
				 * hatimerun must have failed, depending
				 * on usage of pmf we have to continue.
				 */
				if (sapxprops->use_pmf == B_TRUE) {
					break;
				} else {
					free(stop_cmd);
					return (rc);
				}
		}
		free(stop_cmd);
	} else {
		if (sapxprops->use_pmf == B_FALSE) {
			/*
			 * SCMSGS
			 * @explanation
			 * Validation failed while PMF is not configured.
			 * Termination of the SAP processes might not be
			 * complete.
			 * @user_action
			 * Informational message. No user action is needed.
			 */
			scds_syslog(LOG_ERR,
				"PMF is not used, but validation failed."
				" Cannot run stopsap command.");
			free(stop_cmd);
			return (SCHA_ERR_INTERNAL);
		}
	}

	scds_syslog_debug(DBG_LEVEL_HIGH,
		"timeout_run() returned %d, cmd return value %d",
		rc, status);

pmf_only:

	if (sapxprops->use_pmf == B_TRUE) {
		stopsap_to = (scds_get_rs_stop_timeout(scds_handle) *
				(100 - STOPPERCENTAGE)) / 100;

		rc = scds_pmf_stop(scds_handle, SCDS_PMF_TYPE_SVC,
				0, SIGKILL, stopsap_to);
		switch (rc) {
			case SCHA_ERR_TIMEOUT:
				scds_syslog(LOG_ERR,
					"scds_pmf_stop() timed out.");
				break;
			case SCHA_ERR_NOERR:
				scds_syslog_debug(DBG_LEVEL_HIGH,
					"scds_pmf_stop() succeeded.");
				break;
			default:
				scds_syslog(LOG_ERR,
					"scds_pmf_stop() failed with error %s.",
					scds_error_string(rc));
				break;
		}
	}

	if (!failed_validate) {
		rc = run_cleanipc(scds_handle, &probe_rc, sapxprops);
		if (rc != SCHA_ERR_NOERR) {
			scds_syslog(LOG_ERR,
				"Failure running cleanipc; %d.",
				rc);
		}
	}

	return (0);
}


/* Start the fault monitor under PMF */
int
svc_fm_start(scds_handle_t scds_handle)
{
	if (scds_pmf_start(scds_handle,
			SCDS_PMF_TYPE_MON,
			SCDS_PMF_SINGLE_INSTANCE,
			"webas_probe", 0) != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to start fault monitor.");
		return (1);
	}
	scds_syslog(LOG_INFO, "Started the fault monitor.");
	return (SCHA_ERR_NOERR);
}



/* Stop the fault monitor */
int
svc_fm_stop(scds_handle_t scds_handle)
{
	if (scds_pmf_stop(scds_handle, SCDS_PMF_TYPE_MON, 0, SIGKILL, -1) !=
	    SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to stop fault monitor.");
		return (1);
	}

	scds_syslog(LOG_INFO, "Stopped the fault monitor.");

	return (SCHA_ERR_NOERR);
}


/*
 * Check if the host where this program is executing is healthy.
 * Currently, we just call validate and return success if validate passes
 */
int
svc_fm_check(scds_handle_t scds_handle, sap_extprops_t *sapxprops)
{

	if (svc_validate(scds_handle, sapxprops, B_FALSE) != SCHA_ERR_NOERR) {
		return (1);		/* Validation failure */
	}

	scds_syslog(LOG_INFO, "Completed successfully.");

	return (SCHA_ERR_NOERR);
}



/*
 * svc_probe(): Do data service specific probing.
 * The probe functionality has to meet these requirements
 * ABAP-only system: {Instance_type = ABAP}
 *   1) main dispatcher processes
 * Java-only system: {Instance_type = J2EE}
 *   1) j2ee engine, by sending xml/http requests.
 * ABAP + Java system:{Instance_type = ABAP_J2EE}
 * It checks two things in the following sequence:
 *   1) main dispatcher processes
 *   2) j2ee engine, if required
 *
 * If any error (complete failure or partial failure) happens during those
 * three checks, this routine return with that error. If finishes successfully,
 * it returns SAP_PROBE_OK.
 */
int
svc_probe(scds_handle_t scds_handle, sap_extprops_t *sapxprops,
	int probe_timeout)
{
	int		disp_rc = SAP_PROBE_OK;
	int		j2ee_rc = SAP_PROBE_OK;
	int		err = 0;
	time_t		secs_before = 0, secs_after = 0;

	/*
	 * Probe the SAP dispatcher processes.
	 */
	if (strcmp(sapxprops->sap_instance_type, "ABAP") == 0) {
		secs_before = time((time_t *)NULL);
		err += probe_disp(scds_handle, &disp_rc, sapxprops,
			probe_timeout);
		if (err == SCDS_PROBE_COMPLETE_FAILURE)
			return (err);
		secs_after = time((time_t *)NULL);
	}

	/*
	 * Probe the SAP J2EE engine.
	 */

	if (strcmp(sapxprops->sap_instance_type, "ABAP") != 0) {
		probe_timeout = probe_timeout - (secs_after - secs_before);
		err += probe_j2ee(scds_handle, &j2ee_rc, sapxprops,
			probe_timeout);
		if (err >= SCDS_PROBE_COMPLETE_FAILURE)
			return (SCDS_PROBE_COMPLETE_FAILURE);
	}

	return (err);
}


/*
 * Check for the J2EE engine
 * connect to port 50000 + instance number * 100, http post an xml ''document''
 * and read the answer.
 */
#define	MAX_RETRY	10

int
probe_j2ee(scds_handle_t scds_handle, int *probe_rc, sap_extprops_t *sapxprops,
	int timeout)
{
	scha_err_t	rc;
	int		connectfd;
	int		port;
	char		buf[SCDS_ARRAY_SIZE];

	/* for writing the probe */
	size_t		n_write, bytes_written;

	/* readin and parsing the probe */
	int		header_rcvd = 0;
	char		*content_string = NULL,
			*content_string_end = NULL,
			*head_end = NULL;
	int		content_length = 0;
	char		*message_alert;
	size_t		n_read, bytes_recvd, header_size = 0;

	time_t		pit1, pit2;

	int		retries;

	*probe_rc = SAP_PROBE_FAILED;

	/* J2EE engine listen()s on 50000 + system number * 100 */
	(void) snprintf(buf, 6, "5%s00", sapxprops->asinstancenum);

	port = atoi(buf);

	if (port < 50000) {
		/*
		 * SCMSGS
		 * @explanation
		 * The data service detected an invalid port number for the
		 * J2EE engine probe.
		 * @user_action
		 * Informational message. No user action is needed.
		 */
		scds_syslog(LOG_ERR, "J2EE probe determined wrong port "
					"number, %d.",
			port);
		return (SCDS_PROBE_COMPLETE_FAILURE);
	}

	pit1 = time((time_t *)NULL);
	rc = scds_fm_tcp_connect(scds_handle, &connectfd, sapxprops->lhost,
				port, timeout);
	pit2 = time((time_t *)NULL);
	timeout = timeout - (pit2 - pit1);

	if (rc == SCHA_ERR_TIMEOUT) {
		/*
		 * SCMSGS
		 * @explanation
		 * The data service timed out while connecting to the J2EE
		 * engine port.
		 * @user_action
		 * Informational message. No user action is needed.
		 */
		scds_syslog(LOG_NOTICE, "Probe for J2EE engine timed out "
				"in scds_fm_tcp_connect().");
		goto part_fail;
	}

	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to connect to the host <%s> "
				"and port <%d>.", sapxprops->lhost, port);
		(void) scds_fm_tcp_disconnect(scds_handle,
			connectfd, DISCONNECT_TO);
		return (SCDS_PROBE_COMPLETE_FAILURE);
	}

	(void) snprintf(buf, SCDS_ARRAY_SIZE,
		"%s\r\nHost: %s:%d\r\nContent-Length: %d\r\n\r\n%s",
		"POST /GRMGHeartBeat/EntryPoint HTTP/1.1",
		"localhost", port,
		strlen(probe_xml),
		probe_xml);

	scds_syslog_debug(DBG_LEVEL_HIGH,
		"J2EE engine probe sending %s.", buf);

	retries = 0;
	n_write = strlen(buf);
	bytes_written = 0;
	while (n_write && retries < MAX_RETRY)  {
		/*
		 * try to write bytes left, initially all of them
		 * n_bytes is n_write on entry of this loop,
		 * scds_fm_tcp_write() returns the bytes written,
		 * so, n_write is reduced by n_bytes and bytes_written
		 * is increased by it at the end of the loop.
		 */
		size_t	n_bytes = n_write;

		if (timeout <= 0)
			goto part_fail;

		pit1 = time((time_t *)NULL);

		rc = scds_fm_tcp_write(scds_handle, connectfd,
				&(buf[bytes_written]),
				&n_bytes, timeout);
		pit2 = time((time_t *)NULL);
		timeout = timeout - (pit2 - pit1);

		scds_syslog_debug(DBG_LEVEL_HIGH,
			"J2EE probe: scds_fm_tcp_write() %d bytes of %s.",
			n_bytes, &buf[n_write]);

		if (rc == SCHA_ERR_TIMEOUT) {
			/*
			 * SCMSGS
			 * @explanation
			 * The data service could not send to the J2EE engine
			 * port.
			 * @user_action
			 * Informational message. No user action is needed.
			 */
			scds_syslog(LOG_NOTICE, "Probe for J2EE engine timed "
					"out in scds_fm_tcp_write().");
			goto part_fail;
		}
		if (rc != SCHA_ERR_NOERR) {
			/*
			 * SCMSGS
			 * @explanation
			 * The data service could not write to the J2EE engine
			 * port.
			 * @user_action
			 * Informational message. No user action is needed.
			 */
			scds_syslog(LOG_ERR, "INTERNAL ERROR in J2EE probe "
				"calling scds_fm_tcp_write(): %s.",
				scha_strerror(rc));
			(void) scds_fm_tcp_disconnect(scds_handle,
				connectfd, DISCONNECT_TO);
			return (SCDS_PROBE_COMPLETE_FAILURE);
		}
		if (n_bytes > 0) {
			n_write -= n_bytes;
			bytes_written += n_bytes;
		}

		retries += 1;
	}

	scds_syslog_debug(DBG_LEVEL_HIGH,
		"J2EE probe done sending probe.");

	retries = 0;
	n_read = 0;

	do {
		/* this loops reads the entire reply */

		bytes_recvd = SCDS_ARRAY_SIZE - n_read;

		if (timeout <= 0)
			goto part_fail;

		pit1 = time((time_t *)NULL);
		rc = scds_fm_tcp_read(scds_handle, connectfd, &buf[n_read],
				&bytes_recvd, timeout);
		pit2 = time((time_t *)NULL);
		timeout = timeout - (pit2 - pit1);

		if (rc == SCHA_ERR_TIMEOUT) {
			/*
			 * SCMSGS
			 * @explanation
			 * The data service timed out while reading J2EE probe
			 * reply.
			 * @user_action
			 * Informational message. No user action is needed.
			 */
			scds_syslog(LOG_NOTICE, "Probe for J2EE engine timed "
					"out in scds_fm_tcp_read().");
			goto part_fail;
		}
		if (rc != SCHA_ERR_NOERR) {
			/*
			 * SCMSGS
			 * @explanation
			 * The data service could not read the J2EE probe
			 * reply.
			 * @user_action
			 * Informational message. No user action is needed.
			 */
			scds_syslog(LOG_ERR, "INTERNAL ERROR in J2EE probe "
				"calling scds_fm_tcp_read(): %s.",
				scha_strerror(rc));
			(void) scds_fm_tcp_disconnect(scds_handle,
				connectfd, DISCONNECT_TO);
			return (SCDS_PROBE_COMPLETE_FAILURE);
		}

		n_read += bytes_recvd;

		buf[n_read] = '\0';

		scds_syslog_debug(DBG_LEVEL_HIGH,
			"J2EE probe: scds_fm_tcp_read() %d bytes of %s.",
			bytes_recvd, buf);

		if (!header_rcvd) {
			if ((head_end = strstr(buf, "\r\n\r\n")) != NULL) {
				/* found a headedr */
				header_rcvd = 1;
				if (strncmp(buf, HTTP_HEAD,
						strlen(HTTP_HEAD))) {
					/* no HTTP head */
					/*
					 * SCMSGS
					 * @explanation
					 * The data service could not find an
					 * http header in reply from J2EE
					 * engine.
					 * @user_action
					 * Informational message. No user
					 * action is needed.
					 */
					scds_syslog(LOG_ERR,
						"In J2EE probe, failed to find "
						"http header in %s.", buf);
					goto part_fail;
				}
				if ((strncmp(buf + strlen(HTTP_HEAD) + 1,
					"200 OK", strlen("200 OK")) == 0) ||
					(strncmp(buf + strlen(HTTP_HEAD) + 1,
					"503", strlen("503")) == 0)) {
					/* no HTTP OK found */
					scds_syslog_debug(DBG_LEVEL_HIGH,
						"J2EE probe returned"
						"HTTP OK.");
					if (strncmp(buf + strlen(HTTP_HEAD) + 1,
						"503", strlen("503")) == 0) {
						/*
						 * SCMSGS
						 * @explanation
						 * The data service received
						 * http code 503, which
						 * indicates that the service
						 * is temporarily not
						 * availble.
						 * @user_action
						 * Informational message. No
						 * user action is needed.
						 */
						scds_syslog(LOG_NOTICE,
							"J2EE engine probe "
							"returned http code "
							"503. "
							"Temp. not available.");
						(void) scds_fm_tcp_disconnect(
							scds_handle, connectfd,
							DISCONNECT_TO);
						return (0);
					}
				} else {
					*head_end = '\0';
					/*
					 * SCMSGS
					 * @explanation
					 * ???
					 * @user_action
					 * Informational message. No user
					 * action is needed.
					 */
					scds_syslog(LOG_ERR,
						"J2EE probe returned %s",
						buf);
					*head_end = '\r';
					goto part_fail;
				}
				content_string =
					strstr(buf, "Content-Length:");
				if (content_string == NULL) {
					content_string =
						strstr(buf, "content-length:");
					if (content_string == NULL) {
						/* no content entry in header */
						/*
						* SCMSGS
						* @explanation
						* The reply from the J2EE
						* engine did not contain
						* a Content-Length: entry
						* in the http header.
						* @user_action
						* Informational message. No
						* user action is needed.
						*/
						scds_syslog(LOG_ERR,
							"In J2EE probe, failed "
							"to find Content-Length"
							": in %s.", buf);
						goto part_fail;
					}
				}
				content_string_end =
					strstr(content_string, "\r\n");
				if (content_string_end == NULL) {
					/* no correct termination */
					scds_syslog(LOG_ERR,
						"In J2EE probe, failed to find "
						"Content-Length: in %s.", buf);
					goto part_fail;
				}
				/* determine the content length as int */
				*content_string_end = '\0';
				content_length =
					atoi(content_string +
					strlen("Content-Length:"));
				*content_string_end = '\r';
				if (content_length <= 0) {
					/*
					 * SCMSGS
					 * @explanation
					 * The reply from the J2EE engine did
					 * not contain a detectable contrnt
					 * length value in the http header.
					 * @user_action
					 * Informational message. No user
					 * action is needed.
					 */
					scds_syslog(LOG_ERR,
						"In J2EE probe, failed to "
						"determine Content-Length: "
						"in %s.", buf);
					goto part_fail;
				}
				/* end of header is  \r\n\r\n */
				header_size = (size_t)head_end
					- (size_t)buf + 4;
			}
		}

		retries += 1;

	/*
	 * keep reading until we either received the entire header
	 * or (after having the header) we got the entire content.
	 */
	} while (retries < MAX_RETRY && (!header_rcvd ||
		(n_read < header_size + (size_t)content_length)));

	if (timeout <= 0)
		goto part_fail;

	pit1 = time((time_t *)NULL);
	rc = scds_fm_tcp_disconnect(scds_handle, connectfd, timeout);
	pit2 = time((time_t *)NULL);
	timeout = timeout - (pit2 - pit1);

	if (rc == SCHA_ERR_TIMEOUT) {
		/*
		 * SCMSGS
		 * @explanation
		 * The data service timed out while disconnecting the socket.
		 * @user_action
		 * Informational message. No user action is needed.
		 */
		scds_syslog(LOG_NOTICE, "Probe for J2EE engine timed "
				"out in scds_fm_tcp_disconnect().");
		return (SCDS_PROBE_COMPLETE_FAILURE/2);
	}
	if (rc != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * The data service detected an internal error from scds.
		 * @user_action
		 * Informational message. No user action is needed.
		 */
		scds_syslog(LOG_ERR, "INTERNAL ERROR in J2EE probe "
			"calling scds_fm_tcp_disconnect(): %s.",
			scha_strerror(rc));
		return (SCDS_PROBE_COMPLETE_FAILURE/4);
	}

	/* head_end is !before! \r\n\r\n, need the stuff behind that */
	head_end += 4;

	scds_syslog_debug(DBG_LEVEL_HIGH,
		"J2EE probe: detected content %s of length %d.",
		head_end, content_length);
	if ((message_alert =
		strstr(head_end, "<messalert>")) != NULL) {
		if (strncmp(message_alert,
			"<messalert>OKAY</messalert>",
			strlen("<messalert>OKAY</messalert>")) == 0) {
			/* bail out successfully! */
			*probe_rc = SAP_PROBE_OK;
			return (0);
		}
	}

	/* some kind of failure, print the received message */
	/*
	 * SCMSGS
	 * @explanation
	 * The data service received an unexpected reply from the J2EE engine.
	 * @user_action
	 * Informational message. No user action is needed.
	 */
	scds_syslog(LOG_ERR, "J2EE probe didn't return expected message: %s",
		head_end);

	return (SCDS_PROBE_COMPLETE_FAILURE/2);

part_fail:
	(void) scds_fm_tcp_disconnect(scds_handle, connectfd, DISCONNECT_TO);
	return (SCDS_PROBE_COMPLETE_FAILURE/2);
}


/* Check for the SAP AS dispatcher process */
int
probe_disp(scds_handle_t handle, int *probe_rc,
	sap_extprops_t *sapxprops, int timeout)
{
	int		rc;
	char	internal_err_str[INT_ERR_MSG_SIZE];
	char	dpmon_cmd[SCDS_CMD_SIZE];
	char	*probe_cmd;
	int		cmd_exit_status;
	int		probe_status = 0;
	float	probe_time = 0.0; /* Timeout for dispatcher probe CLI */

	if (probe_rc == NULL) {
		(void) sprintf(internal_err_str,
			"No space was allocated for probe result");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		return (1);
	}

	*probe_rc = SAP_PROBE_FAILED;

	/*
	 * Adjust probe_timeout to be 90% of total timeout
	 * and convert to millisecs.
	 */
	probe_time = ((float)timeout * PROBEPERCENTAGE) * CONVERT2MS;

	(void) snprintf(dpmon_cmd, SCDS_CMD_SIZE,
		"/usr/sap/%s/SYS/exe/run/dpmon",
		sapxprops->sapsid);
	probe_cmd = ds_string_format("%s %s %d",
	    dpmon_cmd, "-p -T", (int)probe_time);

	if (probe_cmd == NULL) {
		ds_internal_error("Unable to allocate string for "
			"probe command.");
		return (SCDS_PROBE_COMPLETE_FAILURE);
	}

	scds_syslog_debug(DBG_LEVEL_HIGH,
	    "probe_disp() calling %s with total timeout %d",
	    probe_cmd, timeout);

	rc = timeout_run(handle, probe_cmd, B_FALSE,
		timeout, &cmd_exit_status,
		sapxprops->sapsid,
		sapxprops->as_hmdir,
		sapxprops->user,
		sapxprops->userid,
		sapxprops->grpid,
		sapxprops->asinstancenum,
		sapxprops->sap_logdir);

	switch (rc) {
		case SCHA_ERR_TIMEOUT:
			scds_syslog(LOG_ERR,
				"The probe command <%s> timed out", dpmon_cmd);
			probe_status = SCDS_PROBE_COMPLETE_FAILURE/2;
			goto finished;
		case SCHA_ERR_INTERNAL:
			(void) snprintf(internal_err_str,
				INT_ERR_MSG_SIZE,
				"error occurred while launching "
				"the probe command <%s>", dpmon_cmd);
			scds_syslog(LOG_ERR,
				"INTERNAL ERROR: %s.", internal_err_str);
			probe_status = SCDS_PROBE_COMPLETE_FAILURE/2;
			goto finished;
		case SCHA_ERR_INVAL:
			scds_syslog(LOG_ERR,
				"Cannot execute %s: %s.", dpmon_cmd,
				cmd_exit_status);
			probe_status = SCDS_PROBE_COMPLETE_FAILURE;
			/* fall through */
		case SCHA_ERR_NOERR:
			scds_syslog_debug(DBG_LEVEL_HIGH,
				"dpmon returned %d", cmd_exit_status);
			break;
		default:
			scds_syslog(LOG_ERR,
				"Unhandled error from probe command %s: %d",
				dpmon_cmd, rc);
			goto finished;
	}

	if (cmd_exit_status == 0) {
		probe_status = 0;
	} else {
		/*
		 * SCMSGS
		 * @explanation
		 * The data service received the displayed return code from
		 * the dpmon command.
		 * @user_action
		 * Informational message. No user action is needed.
		 */
		scds_syslog(LOG_ERR,
			"Probe for dispatcher returned %d", cmd_exit_status);
		probe_status = SCDS_PROBE_COMPLETE_FAILURE;
	}

finished:
	free(probe_cmd);
	return (probe_status);
}



/* Get the extension properties from the SAP config that aren't in the ptable */
int
sap_get_extensions(scds_handle_t scds_handle, sap_extprops_t *sapxprops,
		boolean_t print_messages)
{
	int				rc;
	scha_extprop_value_t 		*extprop = NULL;
	char				internal_err_str[INT_ERR_MSG_SIZE];

	if (sapxprops == NULL) {
		(void) sprintf(internal_err_str,
			"SAP extension property structure was NULL");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		if (print_messages) {
			(void) fprintf(stderr, gettext("INTERNAL ERROR: %s.\n"),
				gettext(internal_err_str));
		}
		return (1);
	}

	/* Start filling in the sap_extpropt_t struct */

	/* SAP_Instance_Type */

	rc = scds_get_ext_property(scds_handle, SAP_EXT_INST_TYPE,
		SCHA_PTYPE_ENUM, &extprop);
	if (rc != SCHA_ERR_NOERR || extprop == NULL) {
		scds_syslog(LOG_ERR, "Failed to retrieve the property %s: %s.",
			SAP_EXT_INST_TYPE, scds_error_string(rc));
		if (print_messages) {
			(void) fprintf(stderr, gettext("Failed to retrieve the "
				"property %s: %s.\n"), SAP_EXT_INST_TYPE,
				gettext(scds_error_string(rc)));
		}
		return (1);
	}
	sapxprops->sap_instance_type = strdup(extprop->val.val_enum);
	extprop = NULL;

	/* SAPSID */

	rc = get_string_ext_property(scds_handle, SAP_EXT_SAPSID,
			&sapxprops->sapsid);

	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to retrieve the extension "
			"property %s: %s", SAP_EXT_SAPSID,
			scds_error_string(rc));
	}

	/* SAP_Instance_Name */

	rc = get_string_ext_property(scds_handle, SAP_EXT_SERVICES,
			&sapxprops->assvcstring);

	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to retrieve the extension "
			"property %s: %s", SAP_EXT_SERVICES,
			scds_error_string(rc));
	}

	/* SAP_Instance_No */

	rc = get_string_ext_property(scds_handle, SAP_EXT_SAPSYSNUM_AS,
			&sapxprops->asinstancenum);

	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to retrieve the extension "
			"property %s: %s", SAP_EXT_SAPSYSNUM_AS,
			scds_error_string(rc));
	}

	/* As_startup_script */

	rc = get_string_ext_property(scds_handle, SAP_EXT_STARTSAP,
			&sapxprops->as_start);

	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to retrieve the extension "
			"property %s: %s", SAP_EXT_STARTSAP,
			scds_error_string(rc));
	}

	/* As_shutdown_script */

	rc = get_string_ext_property(scds_handle, SAP_EXT_STOPSAP,
			&sapxprops->as_stop);

	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to retrieve the extension "
			"property %s: %s", SAP_EXT_STOPSAP,
			scds_error_string(rc));
	}

	/* SAP_User */

	rc = get_string_ext_property(scds_handle, SAP_EXT_USER,
			&sapxprops->user);

	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to retrieve the extension "
			"property %s: %s", SAP_EXT_USER,
			scds_error_string(rc));
	}

	/* SAP_Logdir */

	rc = get_string_ext_property(scds_handle, SAP_EXT_LOGDIR,
			&sapxprops->sap_logdir);

	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to retrieve the extension "
			"property %s: %s", SAP_EXT_LOGDIR,
			scds_error_string(rc));
	}
	/* Webas_Use_Pmf */

	rc = scds_get_ext_property(scds_handle, SAP_EXT_USEPMF,
		SCHA_PTYPE_BOOLEAN, &extprop);
	if (rc != SCHA_ERR_NOERR || extprop == NULL) {
		scds_syslog(LOG_ERR, "Failed to retrieve the property %s: %s.",
			SAP_EXT_USEPMF, scds_error_string(rc));
		if (print_messages) {
			(void) fprintf(stderr, gettext("Failed to retrieve the "
				"property %s: %s.\n"), SAP_EXT_USEPMF,
				gettext(scds_error_string(rc)));
		}
		return (1);
	}
	sapxprops->use_pmf = extprop->val.val_boolean;
	extprop = NULL;

	/* check if sap version is > 6 */
	rc = get_version(sapxprops->sap_instance_type, sapxprops->sapsid,
		&sapxprops->pre_sap_kernel7);
	if (rc == -1) {
		/*
		 * SCMSGS
		 * @explanation
		 * The data service cannot retrieve the sap version.
		 * @user_action
		 * Ensure disp+work/jlaunch is in the expected path
		 * (/usr/sap/<SID>/SYS/exe/run) and is executable.
		 */
		scds_syslog(LOG_ERR, "Failed to retrieve the sap version");
		if (print_messages) {
			(void) fprintf(stderr, gettext("Failed to retrieve the "
				"sap version\n"));
		}
		return (1);
	}

	return (0);
}
