#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

# Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# NOTE: Keywords are case insensitive, i.e. users may use any
# capitalization style they wish.
#
#pragma	ident	"@(#)SUNW.sapwebas	1.9	07/06/06 SMI"

RESOURCE_TYPE = "sapwebas";
VENDOR_ID = SUNW;
RT_DESCRIPTION = "SAP Web Application Server on Sun Cluster";

RT_VERSION ="5";

API_VERSION = 5;	 

INIT_NODES = RG_PRIMARIES;

RT_BASEDIR=/opt/SUNWscsapwebas/webas/bin;

START				=	webas_svc_start;
STOP				=	webas_svc_stop;

VALIDATE			=	webas_validate;
UPDATE	 			=	webas_update;

MONITOR_START			=	webas_monitor_start;
MONITOR_STOP			=	webas_monitor_stop;
MONITOR_CHECK			=	webas_monitor_check;

PKGLIST = SUNWscsapwebas;

#
# Upgrade directives
#
#$upgrade
#$upgrade_from "1.0" anytime
#$upgrade_from "2" anytime
#$upgrade_from "4" anytime

#for pkg upgrade
#% SERVICE_NAME = "sapwebas"

# The paramtable is a list of bracketed resource property declarations 
# that come after the resource-type declarations
# The property-name declaration must be the first attribute
# after the open curly of a paramtable entry
#
# The following are the system defined properties. Each of the system defined
# properties have a default value set for each of the attributes. Look at 
# man rt_reg(4) for a detailed explanation.
#
{  
	PROPERTY = Start_timeout; 
	MIN = 60;
	DEFAULT = 300;
}
{
	PROPERTY = Stop_timeout; 
	MIN = 60;
	DEFAULT = 300;
}
{ 
	PROPERTY = Validate_timeout; 
	MIN = 60;
	DEFAULT = 120; 
}
{
        PROPERTY = Update_timeout;
	MIN = 60;
        DEFAULT = 120;
}
{ 
	PROPERTY = Monitor_Start_timeout; 
	MIN = 60;
	DEFAULT = 120;
}
{ 
	PROPERTY = Monitor_Stop_timeout; 
	MIN = 60;
	DEFAULT = 120;
}
{ 
	PROPERTY = Monitor_Check_timeout; 
	MIN = 60;
	DEFAULT = 120; 
}
{
        PROPERTY = FailOver_Mode;
        DEFAULT = SOFT;
        TUNABLE = ANYTIME;
}
{
        PROPERTY = Network_resources_used;
        TUNABLE = WHEN_DISABLED;
	DEFAULT = "";
}
{ 
	PROPERTY = Thorough_Probe_Interval; 
	MAX = 3600; 
	DEFAULT = 120; 
	TUNABLE = ANYTIME;
}
{ 
	PROPERTY = Retry_Count; 
	MAX = 5; 
	DEFAULT = 2; 
	TUNABLE = ANYTIME;
}
{ 
	PROPERTY = Retry_Interval; 
	MAX = 4600; 
	DEFAULT = 4320; 
	TUNABLE = ANYTIME;
}

#
# Extension Properties
#

# These two control the restarting of the fault monitor itself
# (not the server daemon) by PMF.
{
	PROPERTY = Monitor_retry_count;
	EXTENSION;
	INT;
	DEFAULT = 4;
	TUNABLE = ANYTIME;
	DESCRIPTION = "Number of PMF restarts allowed for the fault monitor";
}
{
	PROPERTY = Monitor_retry_interval;
	EXTENSION;
	INT;
	DEFAULT = 2;
	TUNABLE = ANYTIME;
	DESCRIPTION = "Time window (minutes) for fault monitor restarts";
}

# Time out value for the probe
{
	PROPERTY = Probe_timeout;
	EXTENSION;
	INT;
	MIN = 2;
	DEFAULT = 120;
	TUNABLE = ANYTIME;
	DESCRIPTION = "Time out value for the probe (seconds)";
}
{
	PROPERTY = SAP_SID;
	EXTENSION;
	STRING;
	TUNABLE = WHEN_DISABLED;
	DESCRIPTION = "SAP system ID (SAPSYSTEMNAME in SAP profile)";
}	
{
	PROPERTY = SAP_Instance_Type;
	EXTENSION;
	Enum  { ABAP, J2EE, ABAP_J2EE};
	Default= "ABAP";
	TUNABLE = WHEN_DISABLED;
	DESCRIPTION = "The Instance type on the specified Host";
}
{
	PROPERTY = SAP_Instance_Name;
	EXTENSION;
	STRING;
	TUNABLE = WHEN_DISABLED;
	DESCRIPTION = "String of Application Server services (INSTANCE_NAME in SAP start profile)";
}
{
	PROPERTY = SAP_Instance_Number;
	EXTENSION;
	STRING;
	TUNABLE = WHEN_DISABLED;
	DESCRIPTION = "The two-digit SAP System number for Application Server (SAPSYSTEM in SAP profile)";
}	
{
	PROPERTY = SAP_User;
	EXTENSION;
	STRING;
	DEFAULT = "";
	TUNABLE = WHEN_DISABLED;
	DESCRIPTION = "The administration user for the SAP application server. Defaults to <SAP_SID>adm";
}
{
	PROPERTY = SAP_Logdir;
	EXTENSION;
	STRING;
	DEFAULT = "";
	TUNABLE = ANYTIME;
	DESCRIPTION = "Optional, where to put the logfiles from startsap and stopsap.Default is the home directory of the administration user. The administration user is defined with extension property SAP_User";
}
{
	PROPERTY = Webas_Startup_Script;
	EXTENSION;
	STRING;
	DEFAULT = "";
	TUNABLE = WHEN_DISABLED;
	DESCRIPTION = "Startup script for the instance. Defaults to /usr/sap/<SAP_SID>/SYS/exe/run/startsap (see documentation) or specify different script (eg. startsap_hostname_00)";	
}
{
	PROPERTY = Webas_Shutdown_Script;
	EXTENSION;
	STRING;
	DEFAULT = "";
	TUNABLE = WHEN_DISABLED;
	DESCRIPTION = "Shutdown script for the instance. Defaults to /usr/sap/<SAP_SID>/SYS/exe/run/stopsap (see documentation) or specify different script (eg. stopsap_hostname_00)";
}
{
	PROPERTY = Webas_Use_Pmf;
	EXTENSION;
	BOOLEAN;
	DEFAULT = TRUE;
	TUNABLE = WHEN_DISABLED;
	DESCRIPTION = "Determines if the startup script process tree is run under PMF.";
}
