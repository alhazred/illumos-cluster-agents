/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * sap_common.h - Common utilities for SAP Netweaver data services.
 */


#ifndef	_SAP_COMMON_H
#define	_SAP_COMMON_H

#pragma ident	"@(#)sap_common.h	1.12	07/06/06 SMI"

#ifdef __cplusplus
extern "C" {
#endif

/* Debug levels for error messages */
#define	SCDS_DBG_HIGH		9

/* buffer size for getpwdnam_r */
#define	SCDS_ARRAY_SIZE		1024

#define	DBG_LEVEL_HIGH		9

#define	INT_ERR_MSG_SIZE	2048

/* wait for an hour */
#define	DEFAULT_HATIMERUN_TIMEOUT	3600

/* INT_MAX divided by 4 since scds_timerun doesn't take INT_MAX */
#define	MAX_TIMEOUT		(INT_MAX >> 2)

/* PATH for the commands */
#define	PATH_ENV		"/usr/sbin:/usr/bin:/usr/cluster/bin"
/* max arg for execv */
#define	MAX_ARGV		16

#define	TEMP_DIR		"/var/run/cluster/"

/* project name in S9 */
#define	DEFAULT_PROJ		"default"

int validate_hasp(int print, scds_handle_t handle);

scha_err_t get_string_ext_property(scds_handle_t scds_handle,
	char *prop_name, char **stringval);

int validate_project_name(scds_handle_t handle, char *user);

#ifdef  SAP_WEBAS_J2EE
int
timeout_run(scds_handle_t handle,
	char *argvs,
	boolean_t log_console,
	int timeout,
	int *ret_val,
	char *sid,
	char *homedir,
	char *username,
	uid_t uid,
	gid_t gid,
	char *sysn,
	char *logdir);
#else
int
timeout_run(scds_handle_t handle,
	char *argvs,
	boolean_t log_console,
	int timeout,
	int *ret_val,
	char *sid,
	char *homedir,
	char *username,
	uid_t uid,
	gid_t gid,
	char *sysn);
#endif

#ifdef __cplusplus
}
#endif

#endif /* _SAP_COMMON_H */
