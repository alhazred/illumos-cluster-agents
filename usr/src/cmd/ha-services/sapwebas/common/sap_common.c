/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * sap_common.c - Common utilities for SAP Netweaver data services.
 */

#pragma ident	"@(#)sap_common.c	1.17	08/10/14 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <grp.h>
#include <scha.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <rgm/libdsdev.h>
#include <libintl.h>		/* for gettext */
#include <libgen.h>		/* for mkdirp */
#include <sys/types.h>		/* for open */
#include <project.h>
#include <fcntl.h>
#include <ds_common.h>
#include "sap_common.h"

/*
 * Do HASP validation.
 *
 * Returns
 *	-1			system error
 *	hasp error codes	otherwise.
 */
int
validate_hasp(int print, scds_handle_t handle)
{
	int				err, rc = 0;
	scds_hasp_status_t		hasp_status;

	err = scds_hasp_check(handle, &hasp_status);

	if (err != SCHA_ERR_NOERR) {
		/*
		 * scha_hasp_check() logs a message to syslog when it
		 * fails
		 */
		if (print) {
			(void) fprintf(stderr,
				gettext("INTERNAL ERROR: %s.\n"),
				gettext("scds_hasp_check failed"));
		}
		/*
		 * validation has failed for this resource
		 */
		return (-1);
	}

	switch (hasp_status) {

	case SCDS_HASP_NO_RESOURCE:
		/*
		 * We do not depend on any SUNW.HAStoragePlus resources
		 */
		scds_syslog(LOG_INFO,
			"This resource does not depend on any "
			"SUNW.HAStoragePlus resources. Proceeding with "
			"normal checks.");
		if (print) {
			(void) fprintf(stderr,
				gettext("This resource does not depend "
					"on any SUNW.HAStoragePlus "
					"resources. Proceeding with "
					"normal checks.\n"));
		}
		rc = SCDS_HASP_NO_RESOURCE;
		break;

	case SCDS_HASP_ERR_CONFIG:
		/*
		 * Configuration error, SUNW.HAStoragePlus resource is
		 * in a different RG. Fail the validation.
		 */
		scds_syslog(LOG_ERR,
			"One or more of the SUNW.HAStoragePlus resources "
			"that this resource depends on is in a different "
			"resource group. Failing validate method "
			"configuration checks.");
		if (print) {
			(void) fprintf(stderr,
				gettext("One or more of the "
					"SUNW.HAStoragePlus resources that "
					"this resource depends on is in a "
					"different resource "
					"group. Failing validate method "
					"configuration checks.\n"));
		}
		rc = SCDS_HASP_ERR_CONFIG;
		break;

	case SCDS_HASP_NOT_ONLINE:
		/*
		 * There is at least one SUNW.HAStoragePlus resource not
		 * online anywhere.
		 */
		scds_syslog(LOG_ERR,
			"One or more of the SUNW.HAStoragePlus resources "
			"that this resource depends on is not online "
			"anywhere. Failing validate method.");
		if (print) {
			(void) fprintf(stderr,
				gettext("One or more of the "
					"SUNW.HAStoragePlus resources that "
					"this resource depends on is not "
					"online anywhere. "
					"Failing validate method.\n"));
		}
		rc = SCDS_HASP_NOT_ONLINE;
		break;

	case SCDS_HASP_ONLINE_NOT_LOCAL:
		/*
		 * Not all SUNW.HAStoragePlus we need, are online locally.
		 */
		scds_syslog(LOG_INFO,
			"All the SUNW.HAStoragePlus resources that this "
			"resource depends on are not online on the local "
			"node. Skipping the checks for the existence "
			"and permissions of the start/stop/probe commands.");
		if (print) {
			(void) fprintf(stderr,
				gettext("All the SUNW.HAStoragePlus "
					"resources that this resource "
					"depends on are not online on "
					"the local node. "
					"Skipping the checks for the "
					"existence and permissions "
					"of the start/stop/probe "
					"commands.\n"));
		}
		rc = SCDS_HASP_ONLINE_NOT_LOCAL;
		break;

	case SCDS_HASP_ONLINE_LOCAL:
		/*
		 * All SUNW.HAStoragePlus resources we need are available on
		 * this node.
		 */
		scds_syslog(LOG_INFO,
			"All the SUNW.HAStoragePlus resources that this "
			"resource depends on are online on the local node. "
			"Proceeding with the checks for the existence and "
			"permissions of the start/stop/probe commands.");
		if (print) {
			(void) fprintf(stderr,
				gettext("All the SUNW.HAStoragePlus "
					"resources that this resource "
					"depends on are online on the "
					"local node. "
					"Proceeding with the checks for "
					"the existence and "
					"permissions of the "
					"start/stop/probe commands.\n"));
		}
		rc = SCDS_HASP_ONLINE_LOCAL;
		break;

	default:
		/* Unknown status code */
		scds_syslog(LOG_ERR, "Unknown status code %d.", hasp_status);
		if (print) {
			(void) fprintf(stderr,
				gettext("Unknown status code %d.\n"),
				hasp_status);
		}
		/*
		 * Since the scds_hasp_check() returned an unknown
		 * status code we return 0 which is "OK, but not
		 * online".
		 */
		rc = -1;
		break;
	}

	return (rc);
}


/*
 * Retrieve a string extension property.  On success return
 * SCHA_ERR_NOERR.  The returned string value may be NULL on success,
 * it will always be NULL if an error occured.
 *
 * The caller is responsible for freeing the returned string which is
 * duplicated from the underlying extension property.
 *
 * Please note the underlying extension property is not freed.  This
 * is a memory leak  but freeing it caused a problem on a previous
 * version of cluster.This is normally OK since methods are short
 * lived so the leak is manageable.  The exception is the probe -
 * be careful not to call this function in the probe's loop.
 */
scha_err_t
get_string_ext_property(scds_handle_t scds_handle, char *prop_name,
	char **stringval)
{
	int			rc;
	scha_extprop_value_t	*extprop = NULL;

	rc = scds_get_ext_property(scds_handle, prop_name,
		SCHA_PTYPE_STRING, &extprop);

	if (rc != SCHA_ERR_NOERR || extprop == NULL) {
		scds_syslog(LOG_ERR,
			"Failed to retrieve the property %s: %s.",
			prop_name, scds_error_string(rc));
		*stringval = NULL;
		return (rc);
	}

	if (extprop->val.val_str == NULL) {
		scds_syslog(LOG_INFO,
			"Extension property <%s> has a value of <%s>",
			prop_name, "NULL");
		*stringval = NULL;
	} else {
		*stringval = strdup(extprop->val.val_str);
		if (*stringval == NULL) {
			rc = SCHA_ERR_NOMEM;
			scds_syslog(LOG_ERR, "Out of memory.");
		} else
			scds_syslog(LOG_INFO,
				"Extension property <%s> has a value of "
				"<%s>", prop_name, *stringval);
	}

	/*
	 * Don't run this function in loop.
	 */
	return (rc);
}



/*
 * Retrieve the project_name if it is set via Resource_project_name
 * or RG_project_name property. Then validate it against the user.
 *
 * If the user belongs to the specified project, return 0. Otherwise
 * return 1.
 */
int
validate_project_name(scds_handle_t handle, char *user)
{

	char	*proj = NULL;
	char	buff[PROJECT_BUFSZ];

	scds_syslog_debug(SCDS_DBG_HIGH, "Validate project name.");

	/* get resouce_project_name or RG_project_name */
	proj = (char *)scds_get_rs_resource_project_name(handle);
	if ((proj == NULL) || (strcmp(proj, DEFAULT_PROJ) == 0) ||
		(strcmp(proj, "") == 0)) {
		if (proj != NULL)
			scds_syslog_debug(SCDS_DBG_HIGH, "rs-proj=[%s]", proj);
		scds_syslog_debug(SCDS_DBG_HIGH, "Project name is "
			"not set via resource_project_name.");
		proj = (char *)scds_get_rg_rg_project_name(handle);
		if ((proj == NULL) || (strcmp(proj, DEFAULT_PROJ) == 0) ||
			(strcmp(proj, "")) == 0) {
			if (proj != NULL)
				scds_syslog_debug(SCDS_DBG_HIGH,
					"rg-proj=[%s]", proj);
			scds_syslog_debug(SCDS_DBG_HIGH,
				"Project name is not set "
				"via RG_project_name either."
				"No need to validate user.");
			return (0);
		} else { /* need to validate user */
			scds_syslog_debug(SCDS_DBG_HIGH,
				"rg_project_name=[%s]", proj);
		} /* endif for RG_project_name */

	} else { /* need to validate user */
		scds_syslog_debug(SCDS_DBG_HIGH,
			"resource_project_name=[%s]", proj);
	} /* endif for resource_project_name */

	/* validate user */
	if (inproj(user, proj, buff, PROJECT_BUFSZ) == 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * The specified user does not belong to the project that is
		 * specified in the error message.
		 * @user_action
		 * Use the projadd(1M) command to add the specified user to
		 * the specified project.
		 */
		scds_syslog(LOG_ERR, "User %s does "
			"not belong to the project %s: %s.", user, proj,
			strerror(errno)); /*lint !e746 */
		return (1);
	}

	scds_syslog_debug(SCDS_DBG_HIGH, "User %s can use project %s",
			user, proj);
	return (0);
}



#define	MAX_ENV			8
#define	LONG_INT_PRINT_WIDTH	15
#define	MAX_HATIMEOUT		3660
#define	SCDS_HATIMERUN		"/usr/cluster/bin/hatimerun"
#define	MINUSt			"-t"
#define	MINUSk			"-k"
#define	BINSH			"/bin/sh"
#define	MINUSc			"-c"

#ifdef  SAP_WEBAS_J2EE
int
timeout_run(scds_handle_t handle,
	char *cmd,
	boolean_t log_console,
	int timeout,
	int *ret_val,
	char *sid,
	char *homedir,
	char *username,
	uid_t uid,
	gid_t gid,
	char *inst,
	char *logdir)
#else
int
timeout_run(scds_handle_t handle,
	char *cmd,
	boolean_t log_console,
	int timeout,
	int *ret_val,
	char *sid,
	char *homedir,
	char *username,
	uid_t uid,
	gid_t gid,
	char *inst)
#endif
{
	char		*envp[MAX_ENV];
	char		envhome[SCDS_ARRAY_SIZE];
	char		envlog[SCDS_ARRAY_SIZE];
	char		envsid[SCDS_ARRAY_SIZE];
	char		envinst[SCDS_ARRAY_SIZE];
	char		envpath[SCDS_ARRAY_SIZE];
	char		envldpath[SCDS_ARRAY_SIZE];
	char		signalstr[LONG_INT_PRINT_WIDTH];
	char		timeoutstr[LONG_INT_PRINT_WIDTH];
	char		logstderr[SCDS_ARRAY_SIZE];
	char		logstdout[SCDS_ARRAY_SIZE];
	char		*cmd_string;
	char		*execvargv[MAX_ARGV];
	int		status;
	pid_t		pid;
	struct stat	sbuf;
#ifdef  SAP_WEBAS_J2EE
	char		envlogdir[SCDS_ARRAY_SIZE];
#endif


	(void) snprintf(envsid, SCDS_ARRAY_SIZE,
			"SAPSYSTEMNAME=%s", sid);

	(void) snprintf(envhome, SCDS_ARRAY_SIZE,
			"HOME=%s", homedir);

	(void) snprintf(envinst, SCDS_ARRAY_SIZE,
			"SAPSYSTEM=%s", inst);

	(void) snprintf(envlog, SCDS_ARRAY_SIZE,
			"LOGNAME=root");

	(void) snprintf(envpath, SCDS_ARRAY_SIZE,
			"PATH=%s", PATH_ENV);

	(void) snprintf(envldpath, SCDS_ARRAY_SIZE,
			"LD_LIBRARY_PATH=/sapmnt/%s/exe:"
			"/usr/sap/%s/SYS/exe/uc/sunx86_64:"
			"/usr/sap/%s/SYS/exe/uc/sun_64:"
			"/usr/sap/%s/SYS/exe/nuc/sunx86_64:"
			"/usr/sap/%s/SYS/exe/nuc/sun_64",
			sid, sid, sid, sid, sid);

#ifdef  SAP_WEBAS_J2EE
	(void) snprintf(envlogdir, SCDS_ARRAY_SIZE,
			"R3S_LOGDIR=%s", logdir);

	scds_syslog_debug(DBG_LEVEL_HIGH,
		"timeout_run() starting command %s with timeout %s"
		" and env. set %s / %s / %s / %s",
		cmd, timeoutstr, envhome, envsid, envinst, envlogdir);
#else
	scds_syslog_debug(DBG_LEVEL_HIGH,
		"timeout_run() starting command %s with timeout %s"
		" and env. set %s / %s / %s ",
		cmd, timeoutstr, envhome, envsid, envinst);

#endif

	(void) snprintf(signalstr, LONG_INT_PRINT_WIDTH, "%d", SIGKILL);

	if ((timeout <= 0) || (timeout > MAX_HATIMEOUT))
		(void) snprintf(timeoutstr,
			LONG_INT_PRINT_WIDTH, "%ld", MAX_HATIMEOUT);
	else
		(void) snprintf(timeoutstr,
			LONG_INT_PRINT_WIDTH, "%ld", timeout);


	envp[0] = envhome;
	envp[1] = envsid;
	envp[2] = envinst;
	envp[3] = envlog;
	envp[4] = envpath;
	envp[5] = envldpath;
#ifdef  SAP_WEBAS_J2EE
	envp[6] = envlogdir;
	envp[7] = NULL;
#else
	envp[6] = NULL;
#endif

	execvargv[0] = SCDS_HATIMERUN;
	execvargv[1] = MINUSt;
	execvargv[2] = timeoutstr;
	execvargv[3] = MINUSk;
	execvargv[4] = signalstr;
	execvargv[5] = BINSH;
	execvargv[6] = MINUSc;

	(void) snprintf(logstderr, SCDS_ARRAY_SIZE, "%s%s", TEMP_DIR,
		scds_get_resource_name(handle));


	switch (pid = fork1()) {
		case -1:
			ds_internal_error("fork1(): %s",
				strerror(errno));
			return (SCHA_ERR_INTERNAL);
		case 0:
			if (setgid(gid) < 0) {
				ds_internal_error("setgid(): %s",
					strerror(errno));
				exit(-1);
			}

			/*
			* initialize the supplementary group access list
			* this is required to allow the file/resource access
			* of other groups by the service
			*/

			if (initgroups(username, gid) == -1) {
				ds_internal_error("initgroups(): %s",
					strerror(errno));
				exit(-1);
			}
			if (setuid(uid) < 0) {
				ds_internal_error("setuid(): %s",
					strerror(errno));
				exit(-1);
			}
			if ((stat(logstderr, &sbuf) == 0) &&
				(S_ISDIR(sbuf.st_mode)) &&
				(access(logstderr, W_OK) == 0)) {
				(void) snprintf(logstderr, SCDS_ARRAY_SIZE,
					"2>%s%s/stderr.%d",
					TEMP_DIR,
					scds_get_resource_name(handle),
					getpid());
				(void) snprintf(logstdout, SCDS_ARRAY_SIZE,
					">%s%s/stdout.%d",
					TEMP_DIR,
					scds_get_resource_name(handle),
					getpid());
			} else {
				if (!log_console) {
					(void) snprintf(logstderr,
						SCDS_ARRAY_SIZE,
						"2>/dev/null");
					(void) snprintf(logstdout,
						SCDS_ARRAY_SIZE,
						">/dev/null");
				} else {
					(void) snprintf(logstderr,
						SCDS_ARRAY_SIZE, "");
					(void) snprintf(logstdout,
						SCDS_ARRAY_SIZE, "");
				}
			}
			if (strlen(logstdout) && strlen(logstderr)) {
				cmd_string = ds_string_format("%s %s %s",
						cmd, logstdout, logstderr);
				execvargv[7] = cmd_string;
			}
			else
				execvargv[7] = cmd;

			execvargv[8] = NULL;

			if (chdir(homedir) < 0) {
				ds_internal_error("chdir(): %s",
					strerror(errno));
				exit(-1);
			}

			if (execve(SCDS_HATIMERUN, execvargv, envp) < 0) {
				ds_internal_error("execv(): %s",
					strerror(errno));
				exit(-1);
			}
			break;
		default:
			scds_syslog_debug(DBG_LEVEL_HIGH,
				"timeout_run() fork1()ed pid %d", pid);
			break;
	}

	scds_syslog_debug(DBG_LEVEL_HIGH,
		"waitpid() for pid %d", pid);

	while (waitpid(pid, &status, 0) < 0) {
		if (errno != EINTR) {
			scds_syslog_debug(DBG_LEVEL_HIGH,
				"timeout_run() got %s in waitpid.",
				strerror(errno));
			break;
		}
	}

	if (WIFEXITED(status)) {
		/* normal exit from child */
		scds_syslog_debug(DBG_LEVEL_HIGH,
			"timeout_run(): WIFEXITED() = %d",
			WEXITSTATUS(status)); /*lint !e702 */
		*ret_val = WEXITSTATUS(status); /*lint !e702 */
		if (*ret_val == 99)
			return (SCHA_ERR_TIMEOUT);
		if (*ret_val == 98) {
			ds_internal_error("hatimerun returned 98");
			return (SCHA_ERR_INTERNAL);
		}
	}

	if (WIFSIGNALED(status)) {
		/* signal got send to the process */
		scds_syslog_debug(DBG_LEVEL_HIGH,
			"timeout_run(): WTERMSIG() = %d",
			WTERMSIG(status));
		*ret_val = WTERMSIG(status);
		return (SCHA_ERR_TIMEOUT);
	}

	return (SCHA_ERR_NOERR);
}

/*
 * Get SAP version.
 *
 * If the sap version is >= 7 , return 1. Otherwise
 * return 0. For error return -1.
 */
int
get_version(char *sap_instance_type, char *sapsid, int *ver)
{
	char	*cmd;
	char	*s;
	char	line[SCDS_ARRAY_SIZE];
	char	internal_err_str[SCDS_ARRAY_SIZE];
	FILE    *in = NULL;
	int	rc = 0;

	if (strcmp(sap_instance_type, "J2EE") == 0) {
		cmd = ds_string_format("LC_ALL=\"C\""
			" LD_LIBRARY_PATH=/usr/sap/%s/SYS/exe/run/:"
			"/usr/sap/%s/SYS/exe/uc/sunx86_64:"
			"/usr/sap/%s/SYS/exe/uc/sun_64:"
			"/usr/sap/%s/SYS/exe/nuc/sunx86_64:"
			"/usr/sap/%s/SYS/exe/nuc/sun_64:"
			" /usr/sap/%s/SYS/exe/run/jlaunch",
			sapsid, sapsid, sapsid, sapsid, sapsid, sapsid);
	} else {
		cmd = ds_string_format("LC_ALL=\"C\""
			" LD_LIBRARY_PATH=/usr/sap/%s/SYS/exe/run/:"
			"/usr/sap/%s/SYS/exe/uc/sunx86_64:"
			"/usr/sap/%s/SYS/exe/uc/sun_64:"
			"/usr/sap/%s/SYS/exe/nuc/sunx86_64:"
			"/usr/sap/%s/SYS/exe/nuc/sun_64:"
			" /usr/sap/%s/SYS/exe/run/disp+work",
			sapsid, sapsid, sapsid, sapsid,	sapsid, sapsid);
	}
	*ver = 0;
	if ((in = popen(cmd, "r")) != NULL) {
		while (!feof(in)) {
			s = fgets(line, sizeof (line), in);
			if (s == NULL && !feof(in)) {
			/*
			 * SCMSGS
			 * @explanation
			 * An error was encountered while reading the output
			 * from the specified disp+work/jlaunch command.
			 * @user_action
			 * Examine other syslog messages occurring around the
			 * same time on the same node, to see if the source of
			 * the problem can be identified.
			 */
				scds_syslog(LOG_ERR, "Error reading line from "
					"%s command: %s.", cmd,
					strerror(errno));
				rc = SCHA_ERR_INVAL;
				return (-1);
			}

			if (feof(in)) {
				rc = -1;
				break;
			}
			if (strstr(line, "kernel") && strstr(line, "release")) {
				while (*s != '\0' && !isdigit(*s))
					s++;
				if (*s-'0' >= 7) {
					*ver = 1;
					break;
				}
			}
		}
		rc = pclose(in);
		if (WIFEXITED((uint_t)rc) && (WEXITSTATUS((uint_t)rc) != 0)) {
			(void) sprintf(internal_err_str,
				"Failed to close pipe with pclose. "
				"Output error encountered");
			scds_syslog(LOG_ERR,
				"INTERNAL ERROR: %s.", internal_err_str);
			rc = -1;
		}
	} else {
		(void) sprintf(internal_err_str,
			"Failed to open pipe with popen. "
			"Output error encountered");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		rc = -1;
	}
	return (rc);
}
