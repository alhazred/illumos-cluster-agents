/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * repl.c - Common utilities for replica server
 *
 * This utility has the methods for performing the validation, starting and
 * stopping the data service and the fault monitor. It also contains the method
 * to probe the health of the data service.  The probe just returns either
 * success or failure. Action is taken based on this returned value in the
 * method found in the file repl_probe.c
 *
 */

#pragma ident	"@(#)repl.c	1.9	07/06/06 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <strings.h>
#include <sys/stat.h>
#include <scha.h>
#include <errno.h>
#include <unistd.h>
#include <libgen.h>
#include <rgm/libdsdev.h>
#include <pwd.h>
#include <limits.h>
#include <libintl.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/statvfs.h>
#include <ds_common.h>
#include <sap_common.h>
#include "repl.h"

/*
 * The initial timeout allowed  for the repl dataservice to
 * be fully up and running. We will wait for for 2 % (SVC_WAIT_PCT)
 * of the start_timeout time before probing the service.
 */
#define	SVC_WAIT_PCT		1

/*
 * svc_validate():
 *
 * Perform the follow checks:
 *	HSP check
 *	if HSP is online-not-local, perform the first two checks and skip the
 *		rest. Will perform all the checks if HSP is online local:
 *
 *	1) strong negative affinity for enqueue rg is set
 *	2) resource dependency on enqueue rs is set
 *	3) replica profile exists with correct permission
 *	4) replica server binary exists with correct permission
 *	5) admin user belongs to the specified project (if RG_project_name
 *		or Resource_project_name is set)
 *	6) replica log dir exists and it's an absolute path
 */

int
svc_validate(scds_handle_t scds_handle, repl_extprops_t *replxpropsp,
	boolean_t print_messages)
{
	int	hasprc;
	int	rc = 0;

	/*
	 * Validate HAStoragePlus resources and determine if the
	 * storage/filesystems are online on this node.
	 *
	 * If they are online and local, will go thru all the checks.
	 * If online but not local, only check non-global stuff.
	 *
	 * validate_hasp() logs and prints appropriate messages already.
	 */
	hasprc = validate_hasp(print_messages, scds_handle);

	switch (hasprc) {
		case SCDS_HASP_ONLINE_NOT_LOCAL:goto online_not_local;
		case SCDS_HASP_ONLINE_LOCAL:	break;
		case SCDS_HASP_NO_RESOURCE:	break;
		default:	/* hasp error or system error */
				return (1);
	}

	/*
	 * Perform the following checks if HSP is online-local. If it
	 * is online but not local, then only perform the checks under
	 * online_not_local.
	 */

	/* check repl profile exists w/ the correct permissions */
	rc = ds_validate_file(print_messages, S_IRUSR | S_IFREG,
		replxpropsp->repl_profile);

	if (rc != SCHA_ERR_NOERR) {
		return (1);
	}

	/* check repl server executable exists w/ correct permissions */
	rc = ds_validate_file(print_messages, S_IRUSR | S_IXUSR | S_IFREG,
		replxpropsp->repl_svr);

	if (rc != SCHA_ERR_NOERR) {
		return (1);
	}

	/* check repl server is owned by sap_user */
	rc = ds_validate_file_owner(print_messages, replxpropsp->repl_uid,
		replxpropsp->repl_gid, "%s", replxpropsp->repl_svr);

	if (rc != SCHA_ERR_NOERR)
		return (1);

	/* check and set the log dir */
	if ((rc = retrieve_logdir(replxpropsp, print_messages))
		!= SCHA_ERR_NOERR) {
		return (1);
	}

	/* admin user belongs to the specified project */
	if ((rc = validate_project_name(scds_handle, replxpropsp->repl_usr))
		!= 0) {
		/* error msg has been logged */
		return (1);
	}

	/* log dir is an absolute path */
	if (replxpropsp->repl_logdir[0] != '/') {
		scds_syslog(LOG_ERR, "%s is not an absolute path.",
			replxpropsp->repl_logdir);
		if (print_messages) {
			(void) fprintf(stderr, gettext("%s is not an absolute "
				"path.\n"), replxpropsp->repl_logdir);
		}
		return (1);
	}

	/* make sure logdir exists */
	rc = ds_validate_file(print_messages, S_IRUSR | S_IXUSR | S_IFDIR,
		replxpropsp->repl_logdir);

	if (rc != SCHA_ERR_NOERR)
		return (1);

	/* check logdir is owned by sap_user */
	rc = ds_validate_file_owner(print_messages, replxpropsp->repl_uid,
		replxpropsp->repl_gid, "%s", replxpropsp->repl_logdir);

	if (rc != SCHA_ERR_NOERR)
		return (1);

online_not_local:

	/*
	 * Check whether strong negative affinity w/ enqueue rg is set.
	 * Check the strong resource dependency on enqueue rs is set.
	 * Also make sure the enq-rg (from rs-dependency setting) is one
	 * of the rgs in the affinity-rg list.
	 */
	if ((rc = check_dependency_n_affinity(scds_handle,
		print_messages)) != 0) {

		/*
		 * affinity or dependency is not set or system error,
		 * msg already logged.
		 */
		return (1);
	}

	return (0);
}



/*
 * Convenient routine to find out whether there is a strong resource
 * dependency set on a enqueue rs.
 *
 * Return the enqueue rs name if found one. Otherwise return NULL.
 * It's the caller's responsibility to free the memory.
 */
char *
get_rs_dep_on_enq(scds_handle_t scds_handle, boolean_t print_msg)
{

	const scha_str_array_t	*rsdep = NULL;
	char			*rs_dep_enq = NULL;
	boolean_t		got_enq_rs = B_FALSE, is_enq = B_FALSE;
	uint_t			i = 0;

	/* get strong rs dependency on enqueue */
	rsdep = scds_get_rs_resource_dependencies(scds_handle);
	scds_syslog_debug(SCDS_DBG_HIGH,
		"-- after scds_get_rs_resource_dependencies");

	if ((rsdep == NULL) || (rsdep->array_cnt < 1)) {
		/*
		 * SCMSGS
		 * @explanation
		 * The resource dependency on the resource listed in the error
		 * message is not set.
		 * @user_action
		 * Set the resource dependency on the appropriate resource.
		 */
		scds_syslog(LOG_ERR, "Resource dependency on %s "
			"is not set.", "SAP enqueue server");
		if (print_msg)
			(void) fprintf(stderr,
				gettext("Resource dependency on SAP Enqueue "
					"server is not set.\n"));
		return (NULL);
	}

	scds_syslog_debug(SCDS_DBG_HIGH, "== before for loop ");

	/* for each rs, find out it's enqueue rt or not */
	for (i = 0; i < rsdep->array_cnt; i++) {
		is_enq = is_enq_rs(rsdep->str_array[i], print_msg);
		if (is_enq == B_TRUE) {
			scds_syslog_debug(SCDS_DBG_HIGH,
				"got enq-rs=[%s]", rsdep->str_array[i]);
			got_enq_rs = B_TRUE;
			if ((rs_dep_enq = strdup(rsdep->str_array[i]))
				== NULL) {
				scds_syslog(LOG_ERR, "Out of memory.");
				if (print_msg)
					(void) fprintf(stderr,
						gettext("Out of memory.\n"));
				return (NULL);
			}
			break;
		} /* else not a enqueue rt, continue to next */
	} /* end of for loop */

	if (!got_enq_rs) {
		/* didn't have depedency set on enqueue */
		scds_syslog(LOG_ERR, "Resource dependency on %s is not set.",
			"SAP Enqueue server");
		if (print_msg)
			(void) fprintf(stderr,
				gettext("Resource dependency on SAP Enqueue "
					"server is not set."));
		return (NULL);
	}
	return (rs_dep_enq);
}



/*
 * Verify the strong resource dependency is set on enqueue resource.
 * Also, verify the strong negative affinity is set with an enqueue server
 * resource group. The rg of the enq-rs defined in the rs-dependency list
 * should be one of the strong neg affinity rg defined in the rg-affinity list.
 *
 * Return 0 if all checks succeed, 1 otherwise.
 */
int
check_dependency_n_affinity(scds_handle_t scds_handle, boolean_t print_msg)
{
	int			i = 0, rc = 0;
	scha_resource_t		enq_rshandle = NULL;
	char			*enq_rg = NULL;
	const scha_str_array_t	*afflist = NULL;
	char			*temp_rg = NULL;
	char			*rs_dep_on_enq = NULL;

	/* find the strong rs dependency on enqueue */
	if ((rs_dep_on_enq = get_rs_dep_on_enq(scds_handle, print_msg))
		== NULL)
		/* no rs dependency set on enq rs. msg already logged */
		return (1);

	/*
	 * Got the enqueue rs. Need to find out its
	 * rg so we can verify against rg-affinity setting later on.
	 */
retry_rg:
	rc = scha_resource_open(rs_dep_on_enq, NULL, &enq_rshandle);
	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
			"Failed to retrieve the resource handle: %s.",
			scds_error_string(rc));
		free(rs_dep_on_enq);
		return (1);
	}

	rc = scha_resource_get(enq_rshandle, SCHA_GROUP, &enq_rg);
	if (rc == SCHA_ERR_SEQID) {
		scds_syslog(LOG_INFO,
			"Retrying to retrieve the resource information: %s.",
			scds_error_string(rc));
		(void) scha_resource_close(enq_rshandle);
		enq_rshandle = NULL;
		enq_rg = NULL;
		(void) sleep(1);
		goto retry_rg;
	} else if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
			"Failed to retrieve the resource property %s: %s.",
			SCHA_GROUP, scds_error_string(rc));
	}

	scds_syslog_debug(SCDS_DBG_HIGH, "enq-rg=[%s]", enq_rg);

	/* retrieve property rg-affinity */
	afflist = scds_get_rg_rg_affinities(scds_handle);

	scds_syslog_debug(SCDS_DBG_HIGH, "after DSDL call");

	/* fail validate if +affinity is not set */
	if ((afflist == NULL) || (afflist->array_cnt < 1) ||
		((afflist->array_cnt > 0) && (afflist->str_array == NULL))) {
		/*
		 * SCMSGS
		 * @explanation
		 * The specified affinity has not been set for the specified
		 * resource group.
		 * @user_action
		 * Set the specified affinity on the specified resource group.
		 */
		scds_syslog(LOG_ERR, "%s affinity property for "
			"%s resource group is not set.",
			"Strong negative", "Enqueue server");
		if (print_msg)
			(void) fprintf(stderr,
				gettext("Strong negative affinity "
					"for Enqueue server resource group "
					"is not set with the Replica server "
					"resource group."));
		free(rs_dep_on_enq);
		return (1);
	}

	scds_syslog_debug(SCDS_DBG_HIGH, "afflist is not null");

	/*
	 * Does the affinity list contain the enq-rg from the rs-depdency
	 * property ?
	 */
	for (i = 0; i < (int)afflist->array_cnt; i++) {
		/* strong negative affinity ? */
		scds_syslog_debug(SCDS_DBG_HIGH,
			"rg-aff list = %s, enq-rg=%s",
			afflist->str_array[i], enq_rg);

		if ((afflist->str_array[i][0] == '-') &&
			(afflist->str_array[i][1] == '-')) {

			/* it's --aff rg, skip prefix "--" for rg name */
			temp_rg = afflist->str_array[i];
			temp_rg += 2;

			if (strcmp(temp_rg, enq_rg) == 0) {
				/*
				 * rg of the enq-rs from rs-dependency list
				 * is on the rg-affinity list and it's
				 * the correct affinity setting (strong
				 * negative). Return early.
				 */
				free(rs_dep_on_enq);
				return (0);
			} /* else rg-name doesn't match, go to next */
		} /* else not --affinity, go to next on the list */
	} /* end of for loop */

	/*
	 * No rg from rg-affinity list matches the rg of the enq-rs which
	 * defined in the rs-dependency list. Error.
	 */
	/*
	 * SCMSGS
	 * @explanation
	 * Both the dependency on a SAP enqueue server resource and the
	 * affinity on a SAP enqueue server resource group are set. However,
	 * the resource groups specified in the affinity property does not
	 * correspond to the resource group that the SAP enqueue server
	 * resource (specified in the resource dependency) belongs to.
	 * @user_action
	 * Specify a SAP enqueue server resource group in the affinity
	 * property. Make sure that SAP enqueue server resource group contains
	 * the SAP enqueue server resource that is specified in the resource
	 * dependency property.
	 */
	scds_syslog(LOG_ERR, "Resource dependency on SAP enqueue server "
		"does not match the affinity setting on SAP enqueue server "
		"resource group.");
	free(rs_dep_on_enq);
	return (1);
}



/*
 * This convenient routine determines the specified rs is of enqueue RT.
 * Returns TRUE if the rs is an enqueue rs. Otherwise FALSE (which includes
 * system error).
 */
boolean_t
is_enq_rs(char *rsname, boolean_t print_messages)
{
	int	rc = 0;
	scha_resource_t		rshandle = NULL;
	char			*rtname = NULL;

retry_rt_type:
	rc = scha_resource_open(rsname, NULL, &rshandle);
	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
			"Failed to retrieve the resource handle: %s.",
			scds_error_string(rc));
		if (print_messages)
			(void) fprintf(stderr,
				gettext("Failed to retrieve the "
					"resource handle: %s."),
				scds_error_string(rc));
		return (B_FALSE);
	}

	/* is the rs'RT = sap enqueue */
	rc = scha_resource_get(rshandle, SCHA_TYPE, &rtname);
	if (rc == SCHA_ERR_SEQID) {
		scds_syslog(LOG_INFO,
			"Retrying to retrieve the resource information: %s.",
			scds_error_string(rc));
		(void) scha_resource_close(rshandle);
		rshandle = NULL;
		rtname = NULL;
		(void) sleep(1);
		goto retry_rt_type;
	} else if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
			"Failed to retrieve the resource property"
			" %s: %s.", SCHA_TYPE,
			scds_error_string(rc));
		if (print_messages)
			(void) fprintf(stderr,
				gettext("Failed to retrieve the "
					"resource property %s: %s."),
				SCHA_TYPE, scds_error_string(rc));
		(void) scha_resource_close(rshandle);
		return (B_FALSE);
	}

	if (strncmp(rtname, ENQUEUE_RT_NAME, (sizeof (ENQUEUE_RT_NAME) - 1))
		== 0) {
		/* the rs is an enqueue rs, return */
		(void) scha_resource_close(rshandle);
		return (B_TRUE);
	}

	(void) scha_resource_close(rshandle);
	return (B_FALSE);
}


/* Set the log dir path */
int
retrieve_logdir(repl_extprops_t *replxpropsp, boolean_t print_messages)
{

	/* set the log dir path */
	if (strcmp(replxpropsp->repl_logdir, "") == 0) {
		/* no input value, set it to home dir of Replica_User */
		replxpropsp->repl_logdir = strdup(replxpropsp->repl_home_dir);
		if (replxpropsp->repl_logdir == NULL) {
			scds_syslog(LOG_ERR, "Out of memory.");
			if (print_messages)
				(void) fprintf(stderr,
					gettext("Out of memory.\n"));
			return (1);
		}
	}

	scds_syslog_debug(SCDS_DBG_HIGH, "logdir=%s, homedir=%s",
		replxpropsp->repl_home_dir,
		replxpropsp->repl_logdir);
	return (0);
}


/*
 * svc_start(): Start SAP replica server using the SAP utililty under
 * PMF. If the process tree dies, PMF will restart the replica server.
 */

int
svc_start(scds_handle_t scds_handle, repl_extprops_t *replxpropsp)
{
	int 	rc = 0;
	char	*start_repl_cmd = NULL;
	int	child_mon_level = -1;
	scha_extprop_value_t	*child_mon_level_prop = NULL;
	char			*envp[MAX_REPL_ENV];

	scds_syslog(LOG_NOTICE, "Starting %s.", "SAP replica server");

	/* build the start command, then start it under PMF */
	start_repl_cmd = ds_string_format("%s pf=%s",
		replxpropsp->repl_svr,
		replxpropsp->repl_profile);

	if (start_repl_cmd == NULL) {
		ds_internal_error("Failed to create command to start %s"
			APP_NAME);
		return (1);
	}
	scds_syslog_debug(SCDS_DBG_HIGH, "start-cmd=%s", start_repl_cmd);

	/* set environment LD_LIBRARY_PATH */
	envp[0] = replxpropsp->ld_path;
	envp[1] = NULL;

	/*
	 * go to the home dir for user <repl-user> since the log
	 * files will be created at wherever the dir the startup
	 * command is ran.
	 */
	scds_syslog_debug(SCDS_DBG_HIGH, "chdir to %s",
		replxpropsp->repl_logdir);

	if ((rc = chdir(replxpropsp->repl_logdir)) != 0) {
		ds_internal_error("chdir: %s", strerror(errno)); /*lint !e746 */
		goto finished;
	}

	/* change the gid and uid to the ones for <repl-user> */
	if ((rc = setegid(replxpropsp->repl_gid)) != 0) {
		ds_internal_error("setegid: %s", strerror(errno));
		goto finished;
	}

	if ((rc = seteuid(replxpropsp->repl_uid)) != 0) {
		ds_internal_error("seteuid: %s", strerror(errno));
		goto finished;
	}

	scds_syslog_debug(SCDS_DBG_HIGH, "before pmf start uid=%d euid=%d",
		getuid(), geteuid());

	/* startup replica under pmf */
	/* get child_mon_level */
	if (scds_get_ext_property(scds_handle, "Child_mon_level",
		SCHA_PTYPE_INT, &child_mon_level_prop) == SCHA_ERR_NOERR) {

		child_mon_level = child_mon_level_prop->val.val_int;
		scds_syslog(LOG_INFO,
			"Extension property <Child_mon_level> has a "
			"value of <%d>", child_mon_level);
	} else
		scds_syslog(LOG_INFO,
			"Either extension property <Child_mon_level> is not "
			"defined, or an error occurred while retrieving this "
			"property. The default value of -1 is being used.");

	rc = scds_pmf_start_env(scds_handle, SCDS_PMF_TYPE_SVC, 0,
		start_repl_cmd, child_mon_level, envp);

	if (rc == SCHA_ERR_NOERR)
		scds_syslog(LOG_NOTICE,
			"Start of %s completed successfully.", APP_NAME);
	else
		scds_syslog(LOG_NOTICE,
			"Failed to start the service %s.", APP_NAME);

	/*
	 * Set the gid and uid back to 0 otherwise, will have problem w/
	 * system calls in svc_wait().
	 */
	if (seteuid(0) != 0) {
		ds_internal_error("seteuid(0): %s", strerror(errno));
		rc = 1;
	}

	if (setegid(0) != 0) {
		ds_internal_error("setegid(0): %s", strerror(errno));
		rc = 1;
	}

	scds_syslog_debug(SCDS_DBG_HIGH, "after reset uid=%d euid=%d",
		getuid(), geteuid());

finished:

	free(start_repl_cmd);
	return (rc);
}


/*
 * svc_stop():
 *
 * Stop the replica server via PMF.
 * Return 0 on success, > 0 on failures.
 *
 */
int
svc_stop(scds_handle_t scds_handle)
{
	int 	rc = 0;
	int	stop_signal = SIGINT;
	scha_extprop_value_t	*stop_signal_prop = NULL;

	/* get stop_signal */
	rc = scds_get_ext_property(scds_handle, "Stop_signal", SCHA_PTYPE_INT,
		&stop_signal_prop);
	if (rc == SCHA_ERR_NOERR) {
		stop_signal = stop_signal_prop->val.val_int;
		scds_syslog(LOG_INFO,
			"Extension property <Stop_signal> has a value of <%d>",
			stop_signal);
	} else {
		scds_syslog(LOG_INFO,
			"Either extension property <Stop_signal> is not "
			"defined, or an error occurred while retrieving "
			"this property. The default value of SIGINT is "
			"being used.");
	}

	scds_syslog(LOG_NOTICE, "Stopping %s.", APP_NAME);

	/*
	 * No need to stop monitoring it first since scds_pmf_stop()
	 * will do the followings:
	 *	pmfadm -k SIGCONT
	 *	pmfadm -s -w timeout signal.
	 * PMF will first use 80% of the timeout to stop the appl w/
	 * the specified signal. If that didn't work, PMF will use
	 * the rest of the timeout to stop the appl w/ SIGKILL.
	 *
	 * Can't use -1 as timeout since scds_pmf_stop will error out
	 * if timeout is -1. So use a big timeout like INT_MAX.
	 */

	/*
	 * There is a signal handler in the sap software for SIGINT,
	 * hence send SIGINT first, if that doesn't work, SIGKILL
	 * will be sent.
	 */
	if ((rc = scds_pmf_stop(scds_handle, SCDS_PMF_TYPE_SVC, 0, stop_signal,
		INT_MAX)) != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to stop %s with "
			"SIGINT. Will try to stop it with SIGKILL.");
	}

	scds_syslog_debug(SCDS_DBG_HIGH, "after pmf-stop, rc = %d", rc);

	if (rc == SCHA_ERR_NOERR)
		scds_syslog(LOG_INFO, "Successfully stopped the application");

	return (rc); /* Successfully stopped */
}


/*
 * svc_wait():
 *
 * Wait for the data service to start up fully before return from start method
 * and report the appl is online.
 */
int
svc_wait(scds_handle_t scds_handle)
{
	int		svc_start_timeout;

	/* Get the Start timeout, Probe timeout and resource name */
	svc_start_timeout = scds_get_rs_start_timeout(scds_handle);

	/*
	 * sleep for SVC_WAIT_PCT percentage of start_timeout time
	 * so this data service is fully up.
	 * If there is a probe, will probe and wait until probe reports
	 * the server is up.
	 */
	scds_syslog(LOG_NOTICE, "Waiting for %s to come up.", APP_NAME);
	if (scds_svc_wait(scds_handle, (svc_start_timeout * SVC_WAIT_PCT)/100)
	    != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to start service.");
		return (1);
	}
	return (SCHA_ERR_NOERR);
}


/*
 * This function starts the fault monitor for a repl resource.
 * This is done by starting the probe under PMF. The PMF tag
 * is derived as <RG-name,RS-name,instance_number.mon>. The restart option
 * of PMF is used but not the "infinite restart". Instead
 * interval/retry_time is obtained from the RTR file.
 */

int
mon_start(scds_handle_t scds_handle)
{
	scha_err_t	err;

	scds_syslog_debug(SCDS_DBG_HIGH,
	    "Calling MONITOR_START method for resource <%s>.",
	    scds_get_resource_name(scds_handle));

	/*
	 * The probe repl_probe is assumed to be available in the same
	 * subdirectory where the other callback methods for the RT are
	 * installed. The last parameter to scds_pmf_start denotes the
	 * child monitor level. Since we are starting the probe under PMF
	 * we need to monitor the probe process only and hence we are using
	 * a value of 0.
	 */

	err = scds_pmf_start(scds_handle, SCDS_PMF_TYPE_MON,
	    SCDS_PMF_SINGLE_INSTANCE, "repl_probe", 0);

	if (err != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to start fault monitor.");
		return (err);
	}

	scds_syslog(LOG_INFO, "Started the fault monitor.");

	return (SCHA_ERR_NOERR); /* Successfully started Monitor */
}


/*
 * This function stops the fault monitor for a repl resource.
 * This is done via PMF. The PMF tag for the fault monitor is
 * constructed based on <RG-name_RS-name,instance_number.mon>.
 */

int
mon_stop(scds_handle_t scds_handle)
{
	scha_err_t	err;

	scds_syslog_debug(SCDS_DBG_HIGH, "Calling scds_pmf_stop method");

	err = scds_pmf_stop(scds_handle, SCDS_PMF_TYPE_MON,
	    SCDS_PMF_SINGLE_INSTANCE, SIGKILL,
	    scds_get_rs_monitor_stop_timeout(scds_handle));

	if (err != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to stop fault monitor.");
		return (err);
	}

	scds_syslog(LOG_INFO, "Stopped the fault monitor.");

	return (SCHA_ERR_NOERR); /* Successfully stopped monitor */
}



/*
 * Check whether the enqueue rg is online/pending online on any cluster
 * node(s) or not. This routine is called in boot method.
 *
 * If enqueue rg is online/pending online on some node, return 0.
 * If not, wait and check again in an inifite loop.
 * Return 1 for system error.
 */
int
wait_enq_rg_online(char *rg_name)
{
	int		rc = 0;
	int		i = 0;

	scha_resourcegroup_t	rg_handle = NULL;
	scha_str_array_t	*rgnodes = NULL;
	scha_rgstate_t		node_rg_state = SCHA_RGSTATE_UNMANAGED;

retry_rg_nodelist:
	rc = scha_resourcegroup_open(rg_name, &rg_handle);
	if (rc != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * An attempt to retrieve a resource group handle has failed.
		 * @user_action
		 * If the failure is cased by insufficient memory, reboot. If
		 * the problem recurs after rebooting, consider increasing
		 * swap space by configuring additional swap devices. If the
		 * failure is caused by an API call, check the syslog messages
		 * for the possible cause.
		 */
		scds_syslog(LOG_ERR,
			"Failed to open the resource group handle: %s.",
			scds_error_string(rc));
		goto finished;
	}

	/* retrieve the nodelist where the repl resource can be on  */
	rc = scha_resourcegroup_get(rg_handle, SCHA_NODELIST, &rgnodes);
	if (rc == SCHA_ERR_SEQID) {
		scds_syslog(LOG_INFO,
			"Retrying to retrieve the resource group "
			"information: %s.", scds_error_string(rc));
		(void) scha_resourcegroup_close(rg_handle);
		rg_handle = NULL;
		(void) sleep(1);
		goto retry_rg_nodelist;
	} else if (rc != SCHA_ERR_NOERR || rgnodes == NULL) {
		scds_syslog(LOG_ERR,
			"Failed to retrieve the resource group "
			"property %s: %s.",
			SCHA_NODELIST, scds_error_string(rc));
		goto finished;
	}

	/* wait forever if enq rg is not online */
	for (;;) {

		/* find out the enq rg state on all nodes */
		for (i = 0; i < (int)rgnodes->array_cnt; i++) {

retry_rg_state_node:
			rc = scha_resourcegroup_get(rg_handle,
				SCHA_RG_STATE_NODE,
				rgnodes->str_array[i], &node_rg_state);
			if (rc == SCHA_ERR_SEQID) {
				scds_syslog(LOG_INFO,
					"Retrying to retrieve the resource "
					"group information: %s.",
					scds_error_string(rc));
				/* close the resource group handle */
				(void) scha_resourcegroup_close(rg_handle);
				rg_handle = NULL;
				(void) sleep(1);
				goto retry_rg_state_node;
			} else if (rc != SCHA_ERR_NOERR) {
				scds_syslog(LOG_ERR,
					"Failed to retrieve the resource group"
					" property %s: %s.",
					SCHA_RG_STATE_NODE,
					scds_error_string(rc));
				goto finished;
			/* if it's online/pending online, get out early */
			} else if ((node_rg_state == SCHA_RGSTATE_ONLINE) ||
				(node_rg_state ==
					SCHA_RGSTATE_PENDING_ONLINE)) {
				rc = 0;
				goto finished;
			}
		} /* for all nodes in the rgnodes */

		/* sleep 1 before check again */
		(void) sleep(1);
	}

finished:
	(void) scha_resourcegroup_close(rg_handle);
	return (rc);
}



/*
 * This function retrieves the extension properties and save them
 * in a structure.
 */
int
get_repl_extensions(scds_handle_t handle, repl_extprops_t *replxpropsp,
	boolean_t print_messages)
{
	int		rc = 0;
	struct passwd	*repl_usr_pwd;
	struct passwd	pwd;
	char		buf[MAXPATHLEN];
	char		*repl_ldpath = NULL;
	char		*bin_path = NULL, *sap_bin = NULL;

	if (replxpropsp == NULL) {
		ds_internal_error("Extension property structure "
				"was NULL");
		if (print_messages)
			(void) fprintf(stderr, gettext("INTERNAL ERROR: "
				"Extension property structure was NULL."));
		return (1);
	}

	rc = get_string_ext_property(handle, "Replica_Profile",
		&replxpropsp->repl_profile);

	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to retrieve the extension "
			"property %s: %s.", "Replica_Profile",
			scds_error_string(rc));

		if (print_messages)
			(void) fprintf(stderr,
				gettext("Failed to retrieve the extension "
				"property %s: %s."), "Replica_Profile",
				scds_error_string(rc));
		return (1);
	}

	rc = get_string_ext_property(handle, "Replica_Server",
		&replxpropsp->repl_svr);

	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to retrieve the extension "
			"property %s: %s.", "Replica_Server",
			scds_error_string(rc));

		if (print_messages)
			(void) fprintf(stderr,
				gettext("Failed to retrieve the extension "
				"property %s: %s."), "Replica_Server",
				scds_error_string(rc));
		return (1);
	}

	rc = get_string_ext_property(handle, "SAP_User",
		&replxpropsp->repl_usr);

	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to retrieve the extension "
			"property %s: %s.", "SAP_User",
			scds_error_string(rc));

		if (print_messages)
			(void) fprintf(stderr,
				gettext("Failed to retrieve the extension "
				"property %s: %s."), "SAP_User",
				scds_error_string(rc));
		return (1);
	}

	/* validate replica admin user, retrieve uid and gid of the user. */
	repl_usr_pwd = getpwnam_r(replxpropsp->repl_usr, &pwd, buf, MAXPATHLEN);

	if (repl_usr_pwd == NULL) {
		if (errno == ERANGE) {
			ds_internal_error("Data buffer for user %s "
				"is not big enough",
				replxpropsp->repl_usr);
			if (print_messages)
				(void) fprintf(stderr,
					gettext("Data buffer for user %s "
						"is not big enough.\n"),
						replxpropsp->repl_usr);
		} else {
			scds_syslog(LOG_ERR,
				"Failed to retrieve information for "
				"user %s.", replxpropsp->repl_usr);
			if (print_messages)
				(void) fprintf(stderr,
					gettext("Failed to retrieve information"
					" for user %s.\n"),
					replxpropsp->repl_usr);
		}
		return (1);
	} else if ((pwd.pw_dir == NULL) || (strlen(pwd.pw_dir) == 0)) {
		scds_syslog(LOG_ERR, "Home dir is not set for user %s.",
			replxpropsp->repl_usr);
		if (print_messages) {
			(void) fprintf(stderr, gettext("Home dir is not set"
				" for user %s."), replxpropsp->repl_usr);
		}
		return (1);
	} else if ((replxpropsp->repl_home_dir = strdup(pwd.pw_dir)) == NULL) {
			scds_syslog(LOG_ERR, "Out of memory.");
			if (print_messages)
				(void) fprintf(stderr,
					gettext("Out of memory.\n"));
			return (1);
	}

	/* set replica gid/uid */
	replxpropsp->repl_uid = pwd.pw_uid;
	replxpropsp->repl_gid = pwd.pw_gid;

	rc = get_string_ext_property(handle, "Log_Directory",
		&replxpropsp->repl_logdir);

	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to retrieve the extension "
			"property %s: %s.", "Log_Directory",
			scds_error_string(rc));

		if (print_messages)
			(void) fprintf(stderr,
				gettext("Failed to retrieve the extension "
				"property %s: %s."), "Log_Directory",
				scds_error_string(rc));
		return (1);
	}

	/* save the bin path */
	sap_bin = strdup(replxpropsp->repl_svr);
	if (sap_bin == NULL) {
		scds_syslog(LOG_ERR, "Out of memory.");
		if (print_messages)
			(void) fprintf(stderr,
				gettext("Out of memory.\n"));
		return (1);
	}
	bin_path = dirname(sap_bin);
	scds_syslog_debug(SCDS_DBG_HIGH, "bin-path = %s", bin_path);

	/* dirname() didn't get a valid path */
	if (strcmp(bin_path, ".") == 0) {
		ds_internal_error("dirname(%s) return w/ pointer "
			"to '.'", sap_bin);
		free(sap_bin);
		return (1);
	}

	/* LD_LIBRARY_PATH point to the same path as the repl binary */
	repl_ldpath = ds_string_format("LD_LIBRARY_PATH=%s", bin_path);
	if (repl_ldpath == NULL) {
		scds_syslog(LOG_ERR, "Out of memory.");
		if (print_messages)
			(void) fprintf(stderr,
				gettext("Out of memory.\n"));
		free(sap_bin);
		return (1);
	}
	scds_syslog_debug(SCDS_DBG_HIGH, "repl_ld_path = [%s]", repl_ldpath);

	replxpropsp->ld_path = strdup(repl_ldpath);
	if (replxpropsp->ld_path == NULL) {
		scds_syslog(LOG_ERR, "Out of memory.");
		if (print_messages)
			(void) fprintf(stderr,
				gettext("Out of memory.\n"));
		free(repl_ldpath);
		free(sap_bin);
		return (1);
	}

	free(sap_bin);
	free(repl_ldpath);
	return (0);
}
