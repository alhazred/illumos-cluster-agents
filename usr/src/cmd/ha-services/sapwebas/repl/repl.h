/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2004 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef	_REPL_H
#define	_REPL_H

#pragma ident	"@(#)repl.h	1.5	07/06/06 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#include <sap_common.h>

#define	ENQUEUE_RT_NAME		"SUNW.sapenq"
#define	SAPREPL			"replica"

#define	APP_NAME		"SAP replica server"
#define	MAX_REPL_ENV		7

/* extension property */
typedef struct repl_extprops
{
	char	*repl_profile;
	char	*repl_svr;
	char	*repl_usr;
	char	*repl_home_dir;		/* not a real ext property */
	char	*repl_logdir;		/* not a real ext property */
	uid_t	repl_uid;		/* not a real ext property */
	gid_t	repl_gid;		/* not a real ext property */
	char	*ld_path;		/* not a real ext property */
} repl_extprops_t;

int svc_validate(scds_handle_t scds_handle, repl_extprops_t *replxpropsp,
	boolean_t print_messages);

int svc_start(scds_handle_t scds_handle, repl_extprops_t *replxpropsp);

int svc_stop(scds_handle_t scds_handle);

int svc_wait(scds_handle_t scds_handle);

int mon_start(scds_handle_t scds_handle);

int mon_stop(scds_handle_t scds_handle);

int get_repl_extensions(scds_handle_t handle, repl_extprops_t *replxpropsp,
	boolean_t print_messages);

int retrieve_logdir(repl_extprops_t *replxpropsp, boolean_t print_messages);

int check_dependency_n_affinity(scds_handle_t scds_handle, boolean_t print_msg);

boolean_t is_enq_rs(char *rsname, boolean_t print_messages);

int wait_enq_rg_online(char *rg_name);

char *get_rs_dep_on_enq(scds_handle_t scds_handle, boolean_t print_msg);

#ifdef __cplusplus
}
#endif

#endif /* _REPL_H */
