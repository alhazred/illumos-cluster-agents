/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2004 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * repl_boot.c - Boot method for replica server.
 */

#pragma ident	"@(#)repl_boot.c	1.4	07/06/06 SMI"

#include <unistd.h>
#include <stdlib.h>
#include <rgm/libdsdev.h>
#include "repl.h"

/*
 * The boot method for replica server. It'll return 0 if the standalone
 * enqueue server rg is online or pending online. Otherwise, it'll wait.
 * This check will prevent the extra cost of moving replica_rg to a different
 * node in case the boot method of the replica rs finished earlier than the
 * enqueue rs.
 */

int
main(int argc, char *argv[])
{
	scds_handle_t	scds_handle;
	int		rc = 0;
	scha_resource_t	enq_rshandle = NULL;
	char		*enq_rs = NULL, *enq_rg = NULL;

	/*
	 * Process all the arguments that have been passed to us from RGM
	 * and do some initialization for syslog
	 */
	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR) {
		return (1);
	}

	/* retrieve the enqueue rs which the replica rs is depending on */
	if ((enq_rs = get_rs_dep_on_enq(scds_handle, B_FALSE)) == NULL) {
		/* no rs dependency set on enq rs. msg already logged */
		return (1);
	}

retry_rg:
	/* find out the rg of the enqueue resource */
	rc = scha_resource_open(enq_rs, NULL, &enq_rshandle);
	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
			"Failed to retrieve the resource handle: %s.",
			scds_error_string(rc));
		free(enq_rs);
		return (rc);
	}

	rc = scha_resource_get(enq_rshandle, SCHA_GROUP, &enq_rg);
	if (rc == SCHA_ERR_SEQID) {
		scds_syslog(LOG_INFO,
			"Retrying to retrieve the resource information: %s.",
			scds_error_string(rc));
		(void) scha_resource_close(enq_rshandle);
		enq_rshandle = NULL;
		enq_rg = NULL;
		(void) sleep(1);
		goto retry_rg;
	} else if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
			"Failed to retrieve the resource property %s: %s.",
			SCHA_GROUP, scds_error_string(rc));
		(void) scha_resource_close(enq_rshandle);
		free(enq_rs);
		return (1);
	}

	/* Wait for enqueue rg to come up */
	rc = wait_enq_rg_online(enq_rg);
	return (rc);
}
