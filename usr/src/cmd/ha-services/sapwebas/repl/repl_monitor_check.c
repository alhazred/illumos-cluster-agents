/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2004 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * repl_monitor_check.c - Monitor Check method for replica server
 */

#pragma ident	"@(#)repl_monitor_check.c	1.4	07/06/06 SMI"

#include <rgm/libdsdev.h>
#include "repl.h"

/*
 * just make a simple validate check on the service
 */

int
main(int argc, char *argv[])
{
	scds_handle_t   scds_handle;
	repl_extprops_t	replxpropsp;
	int	rc;

	/* Process the arguments passed by RGM and initialize syslog */
	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR) {
		return (1);
	}

	/* Retrieve SAP replica server extension properties */
	rc = get_repl_extensions(scds_handle, &replxpropsp, B_FALSE);
	if (rc != 0) {
		/* Error msg has been logged */
		return (1);
	}

	rc =  svc_validate(scds_handle, &replxpropsp, B_FALSE);
	scds_syslog_debug(SCDS_DBG_HIGH,
	    "monitor_check method "
	    "was called and returned <%d>.", rc);

	/* Free up all the memory allocated by scds_initialize */
	scds_close(&scds_handle);

	/* Return the result of validate method run as part of monitor check */
	return (rc);
}
