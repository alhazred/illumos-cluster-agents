/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2004 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * repl_probe.c - Probe for replica server
 */

#pragma ident	"@(#)repl_probe.c	1.4	07/06/06 SMI"

#include <rgm/libdsdev.h>

int
main(int argc, char *argv[])
{
	scds_handle_t	scds_handle;
	int		probe_int = 0;

	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR)
		return (1);

	probe_int = scds_get_rs_thorough_probe_interval(scds_handle);

	for (;;) {

		/*
		 * sleep for a duration of thorough_probe_interval between
		 * successive probes. If replica process dies, PMF will
		 * restart it.
		 */
		(void) scds_fm_sleep(scds_handle, probe_int);

		/*
		 * If there is a real probe later on, need to add
		 * it here and call scds_fm_action().
		 */

	} 	/* Keep probing forever */
}
