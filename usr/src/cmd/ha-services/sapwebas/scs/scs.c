/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * scs.c - for Netweaver data services.
 */

#pragma ident	"@(#)scs.c	1.17	08/10/14 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <strings.h>
#include <unistd.h>
#include <libintl.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <ctype.h>
#include <scha.h>
#include <rgm/libdsdev.h>
#include <dlfcn.h>
#include <pwd.h>
#include <ds_common.h>
#include <nss_dbdefs.h>
#include "sap_common.h"
#include "scs.h"

/* #define	HAVE_SCDS_PMF_START_ENV		0 */


/* formerly define in common.h */
#define	SCDS_CMD_SIZE		(8*1024)
/*
 * The initial timeout allowed  for the sapscssvc dataservice to
 * be fully up and running. We will wait for for 2 % (SVC_WAIT_PCT)
 * of the start_timeout time before probing the service.
 */
#define	SVC_WAIT_PCT		2

/*
 * We need to wait for SVC_WAIT_TIME ( 2 secs) for pmf
 * to send the failure message before probing the service
 */

#define	SVC_WAIT_TIME		2

/*
 * svc_validate():
 *
 * Perform the following checks:
 * if HSP is online-not-local, perform the first check, skip the rest
 *
 * 1) logical host exists
 * 2) check for scs_server / monitor
 * 3) profile
 *
 */

int
svc_validate(scds_handle_t scds_handle,
		scs_extprops_t	*scsxprops,
		boolean_t	print_messages)
{
	int		rc = 0;
	struct passwd	pwd;
	char		getpwnam_buffer[NSS_BUFSIZ];
	int		i, hasprc;
	char		*slash = NULL;
	struct stat	sbuf;

	scds_net_resource_list_t *snrlp = NULL;

	/* check SAPSID */

	if (strlen(scsxprops->sapsid) != SID_LEN) {
		/*
		 * SCMSGS
		 * @explanation
		 * The specified SAP SID is not a valid SID. 3 digit SID,
		 * starting with none-numeric digit, needs to be specified.
		 * @user_action
		 * Specifiy a valid SID.
		 */
		scds_syslog(LOG_ERR,
			"SID <%s> is smaller than or exeeds 3 digits.",
			scsxprops->sapsid ? scsxprops->sapsid : "");
		if (print_messages) {
			(void) fprintf(stderr,
					gettext("%s is not a valid "
						"SAP system id.\n"),
					scsxprops->sapsid);
		}
		return (1);
	} else {
		for (i = 0; i < SID_LEN; i++) {
			scsxprops->sapsid[i] =
				(char)toupper(scsxprops->sapsid[i]);
		}
	}

	/* check for the user */

	if (scsxprops->scs_user != NULL) {
		if (strlen(scsxprops->scs_user) == 0) {
			free(scsxprops->scs_user); /* has been "" */
			scsxprops->scs_user = strdup("SIDadm");
			if (scsxprops->scs_user == NULL) {
				ds_internal_error("strdup(): %s",
					strerror(errno)); /*lint !e746 */
				return (1);
			}
			for (i = 0; i < SID_LEN; i++) {
				scsxprops->scs_user[i] =
					(char)tolower(scsxprops->sapsid[i]);
			}
		}
		if (getpwnam_r(scsxprops->scs_user,
					&pwd,
					getpwnam_buffer,
					NSS_BUFLEN_PASSWD) == 0) {
			/*
			 * SCMSGS
			 * @explanation
			 * The data service is not able to retrieve the user
			 * id of the SAP admin user under which SAP is
			 * started.
			 * @user_action
			 * Ensure that the user was created on all relevant
			 * cluster nodes.
			 */
			scds_syslog(LOG_ERR,
				"Cannot retrieve user id %s.",
				scsxprops->scs_user);
			if (print_messages) {
				(void) fprintf(stderr,
					gettext("Cannot retrieve "
						"user id %s.\n"),
					scsxprops->scs_user);
			}
			return (1);
		} else {
			scsxprops->scs_userid = pwd.pw_uid;
			scsxprops->scs_grpid = pwd.pw_gid;
			(void) snprintf(scsxprops->scs_hmdir,
				SCDS_PATHLEN, pwd.pw_dir);
		}
	} else {
		/*
		 * SCMSGS
		 * @explanation
		 * Extension property could not be retrieved and is set to
		 * NULL. Internal error.
		 * @user_action
		 * No user action needed.
		 */
		scds_syslog(LOG_ERR,
			"Internal error; SAP User set to NULL.");
		return (1);
	}

	/* check if instance number is 2 digits and upper case */

	if (scsxprops->scs_instid != NULL) {
		if (strlen(scsxprops->scs_instid) != INSTANCE_LEN) {
			/*
			 * SCMSGS
			 * @explanation
			 * The instance number specified is not a valid SAP
			 * instance number.
			 * @user_action
			 * Specify a valid instance number.
			 */
			scds_syslog(LOG_ERR,
				"Instance number <%s> does not consist "
				"of 2 numeric characters.",
				scsxprops->scs_instid);
			if (print_messages) {
				(void) fprintf(stderr, gettext("%s is not "
					"a valid SAP instance number"),
					scsxprops->scs_instid);
			}
			return (1);
		}
		if (!isdigit(scsxprops->scs_instid[0]) ||
			!isdigit(scsxprops->scs_instid[1])) {
			/*
			 * SCMSGS
			 * @explanation
			 * The instance number specified is not a valid SAP
			 * instance number.
			 * @user_action
			 * Specify a valid instance number, consisting of 2
			 * numeric character.
			 */
			scds_syslog(LOG_ERR,
				"Instance number <%s> contains non-numeric "
				"character.",
				scsxprops->scs_instid);
			if (print_messages) {
				(void) fprintf(stderr, gettext("%s is not "
					"a valid SAP instance number"),
					scsxprops->scs_instid);
			}
			return (1);
		}
	} else {
		/*
		 * SCMSGS
		 * @explanation
		 * Extension property could not be retrieved and is set to
		 * NULL. Internal error.
		 * @user_action
		 * No user action needed.
		 */
		scds_syslog(LOG_ERR,
			"Internal error; SAP instance id set to NULL.");
		return (1);
	}

	/* should we cover privileged and ephemaral ? */
	if (scsxprops->msg_port == 0) {
		char	port_buf[5];
		(void) sprintf(port_buf, "36%s", scsxprops->scs_instid);
		scsxprops->msg_port = atoi(port_buf);
	}

	if (validate_project_name(scds_handle, scsxprops->scs_user) != 0) {
		return (1);
	}

	/*
	 * Validate HAStoragePlus resources and determine if the
	 * storage/filesystems are online on this node.
	 *
	 * If they are online and local, will go thru all the checks.
	 * If online but not local, only check non-global stuff.
	 *
	 * validate_hasp() logs and prints appropriate messages already.
	 */
	hasprc = validate_hasp(print_messages, scds_handle);

	scds_syslog_debug(DBG_LEVEL_HIGH,
		"validate_hasp returned <%d>\n", hasprc);

	switch (hasprc) {
		case SCDS_HASP_ONLINE_NOT_LOCAL:
			goto online_not_local;
		case SCDS_HASP_ONLINE_LOCAL:
			break;
		case SCDS_HASP_NO_RESOURCE:
			break;
		default:	/* hasp error or system error */
			return (1);
	}

	if ((stat(scsxprops->scs_hmdir, &sbuf) != 0) ||
			(!S_ISDIR(sbuf.st_mode))) {
		/*
		 * SCMSGS
		 * @explanation
		 * Home directory of the SAP admin user could not be checked.
		 * @user_action
		 * Ensure that the directory is accessible from all relevant
		 * cluster nodes.
		 */
		scds_syslog(LOG_ERR,
			"Cannot stat() home directory: %s",
			scsxprops->scs_hmdir);
		return (1);
	}

	/* start/stop script */

	if (!strlen(scsxprops->scs_start)) {
		free(scsxprops->scs_start);
		scsxprops->scs_start = (char*) malloc(SCDS_PATHLEN);
		if (scsxprops->scs_start == NULL) {
			ds_internal_error("malloc(): %s",
				strerror(errno));
			return (1);
		}
		(void) snprintf(scsxprops->scs_start, SCDS_PATHLEN,
			"/usr/sap/%s/SYS/exe/run/startsap",
			scsxprops->sapsid);
	}

	slash = strrchr(scsxprops->scs_start, '/');
	if (slash == NULL || strlen(slash) == 1) {
		scds_syslog(LOG_ERR,
			"%s is not an absolute path.",
			scsxprops->scs_start);
		if (print_messages) {
			(void) fprintf(stderr,
				gettext("%s is not an absolute path.\n"),
				scsxprops->scs_start);
		}
		return (1);
	}

	if (ds_validate_file(print_messages, S_IRUSR | S_IXUSR,
			scsxprops->scs_start) != SCHA_ERR_NOERR)
		return (1);

	if (!strlen(scsxprops->scs_stop)) {
		free(scsxprops->scs_stop);
		scsxprops->scs_stop = (char*) malloc(SCDS_PATHLEN);
		if (scsxprops->scs_stop == NULL) {
			ds_internal_error("malloc(): %s",
				strerror(errno));
			return (1);
		}
		(void) snprintf(scsxprops->scs_stop, SCDS_PATHLEN,
			"/usr/sap/%s/SYS/exe/run/stopsap",
			scsxprops->sapsid);
	}

	slash = strrchr(scsxprops->scs_stop, '/');
	if (slash == NULL || strlen(slash) == 1) {
		scds_syslog(LOG_ERR,
			"%s is not an absolute path.",
			scsxprops->scs_stop);
		if (print_messages) {
			(void) fprintf(stderr,
				gettext("%s is not an absolute path.\n"),
				scsxprops->scs_stop);
		}
		return (1);
	}

	if (ds_validate_file(print_messages, S_IRUSR | S_IXUSR,
			scsxprops->scs_stop) != SCHA_ERR_NOERR)
		return (1);

	/* msg_mon */

	if (strlen(scsxprops->msg_mon) == 0) {
		free(scsxprops->msg_mon); /* was "" */
		scsxprops->msg_mon =
			(char*) malloc(MAXPATHLEN + 1);
		if (scsxprops->msg_mon == NULL) {
			ds_internal_error("malloc(): %s",
				strerror(errno));
			return (1);
		}
		(void) snprintf(scsxprops->msg_mon, MAXPATHLEN,
			"/usr/sap/%s/SYS/exe/run/msprot",
			scsxprops->sapsid);
	}

	if (ds_validate_file(print_messages, S_IRUSR | S_IXUSR,
			scsxprops->msg_mon) != SCHA_ERR_NOERR)
		return (1);

online_not_local:

	/* check for network resource */

	if ((rc = scds_get_rs_hostnames(scds_handle, &snrlp))
		!= SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
		    "Error in trying to access the configured network "
		    "resources : %s.", scds_error_string(rc));
		return (1);
	}

	if (snrlp == NULL || snrlp->num_netresources == 0) {
		scds_syslog(LOG_ERR,
		    "No network address resource in resource group.");
		if (print_messages) {
			(void) fprintf(stderr, gettext("No network "
				"address resource in resource group "
				"configured.\n"));
		}
		return (1);
	}

	if (snrlp)
		scds_free_net_list(snrlp);

	return (rc); /* return result of validation */
}


/*
 *
 * get_scs_extensions()
 *
 */

int
get_scs_extensions(scds_handle_t handle, scs_extprops_t *scsxprops)
{
	int			rc = 0;
	scha_extprop_value_t	*extprop = NULL;

	if (scsxprops == NULL) {
		ds_internal_error("Extension property structure "
				"NULL ptr");
		return (1);
	}

	/* SID */

	rc = get_string_ext_property(handle, SCS_EXT_SAPSID,
			&scsxprops->sapsid);

	if (rc != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * The data service could not retrieve the extension property.
		 * @user_action
		 * No user action needed.
		 */
		scds_syslog(LOG_ERR, "Failed to retrieve the extension "
				"property %s: %s", SCS_EXT_SAPSID,
				scds_error_string(rc));

		return (1);
	}

	/* sap user */

	rc = get_string_ext_property(handle, SCS_EXT_SAPUSER,
			&scsxprops->scs_user);

	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to retrieve the extension "
				"property %s: %s", SCS_EXT_SAPUSER,
				scds_error_string(rc));

		return (1);
	}

	/* message server port */

	rc = scds_get_ext_property(handle, MSG_EXT_PORT,
		SCHA_PTYPE_INT, &extprop);

	if (rc != SCHA_ERR_NOERR || extprop == NULL) {
		scds_syslog(LOG_ERR, "Failed to retrieve the property %s: %s.",
			MSG_EXT_PORT, scds_error_string(rc));
		return (1);
	}
	scsxprops->msg_port = extprop->val.val_int;
	extprop = NULL;

	/* startup script */

	rc = get_string_ext_property(handle, SCS_EXT_STARTSAP,
		&scsxprops->scs_start);

	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to retrieve the extension "
			"property %s: %s", SCS_EXT_STARTSAP,
			scds_error_string(rc));
	}

	/* shutdown script */

	rc = get_string_ext_property(handle, SCS_EXT_STOPSAP,
		&scsxprops->scs_stop);

	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to retrieve the extension "
			"property %s: %s", SCS_EXT_STOPSAP,
			scds_error_string(rc));
	}

	/* msg monitor */

	rc = get_string_ext_property(handle, MSG_EXT_SRVMON,
			&scsxprops->msg_mon);

	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to retrieve the extension "
				"property %s: %s", MSG_EXT_SRVMON,
				scds_error_string(rc));

		return (1);
	}

	/* sap instance number */

	rc = get_string_ext_property(handle, SCS_EXT_SYSNR,
			&scsxprops->scs_instid);

	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to retrieve the extension "
				"property %s: %s", SCS_EXT_SYSNR,
				scds_error_string(rc));

		return (1);
	}

	/* sap instance name */

	rc = get_string_ext_property(handle, SCS_EXT_INSTNAME,
			&scsxprops->scs_svcstring);

	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to retrieve the extension "
				"property %s: %s", SCS_EXT_INSTNAME,
				scds_error_string(rc));

		return (1);
	}

	return (0);

}


/*
 * svc_start():
 *
 */

int
svc_start(scds_handle_t scds_handle, scs_extprops_t *xprops)
{
	int	rc = 0;
	char	cmd[SCDS_CMD_SIZE];

	/*
	 * SCMSGS
	 * @explanation
	 * This message indicates that the data service is starting the SCS.
	 * @user_action
	 * No user action needed.
	 */
	scds_syslog(LOG_NOTICE, "Starting SAP Central Service.");

#ifdef	HAVE_SCDS_PMF_START_ENV
	/* if scds_pmf_start_env() ever becomes public */

	rc = scs_start(scds_handle, xprops);

#else
	(void) snprintf(cmd, SCDS_CMD_SIZE,
		"%s/%s -R %s -T %s -G %s",
		scds_get_rt_rt_basedir(scds_handle),
		SCS_START_COMMAND,
		scds_get_resource_name(scds_handle),
		scds_get_resource_type_name(scds_handle),
		scds_get_resource_group_name(scds_handle));

	rc = scds_pmf_start(scds_handle, SCDS_PMF_TYPE_SVC,
			SCDS_PMF_SINGLE_INSTANCE, cmd, -1);
#endif

	if (rc == SCHA_ERR_NOERR) {
		scds_syslog(LOG_INFO,
		    "Start of %s completed successfully.", cmd);
	} else {
		scds_syslog(LOG_ERR,
		    "Failed to start %s.", cmd);
	}

	return (rc); /* return Success/failure status */
} /*lint !e715 */



/*
 * svc_stop():
 *
 * Stop the sapscssvc server
 * RETURN 0 on success, > 0 on failures.
 *
 */

#define	STOPPERCENTAGE	85
int
svc_stop(scds_handle_t scds_handle, scs_extprops_t *scsxprops,
	boolean_t validate_failed)
{
	int 		rc;
	int		stopsap_to; /* timeouts */
	char 		*stop_cmd;
	int		status;

	/* Now stop the processes */
	/*
	 * SCMSGS
	 * @explanation
	 * This message indicates that the data service is stoping the SCS.
	 * @user_action
	 * No user action needed.
	 */
	scds_syslog(LOG_NOTICE, "Stopping SAP Central Service.");

	/* tell pmf to stop monitoring the instance */
	rc = scds_pmf_stop_monitoring(scds_handle, SCDS_PMF_TYPE_SVC,
		SCDS_PMF_SINGLE_INSTANCE);
	if (rc != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * Error in stopping the fault monitor.
		 * @user_action
		 * No user action needed.
		 */
		scds_syslog(LOG_ERR,
			"Failed to stop fault monitor, %s.",
			scds_error_string(rc));
		/* continue, anyway */
	}

	if (validate_failed == B_FALSE) {

		/*
		 * run this part only if validate succeeded, otherwise
		 * it'll fail.
		 */

		stop_cmd = ds_string_format("%s %s %s",
			scsxprops->scs_stop,
			"r3",
			scsxprops->scs_svcstring);

		if (stop_cmd == NULL) {
			ds_internal_error("Unable to allocate string "
				"for stop command.");
			goto pmf_only;
		}

		scds_syslog_debug(DBG_LEVEL_HIGH,
			"Trying to stop SCS with command %s", stop_cmd);

		stopsap_to = (scds_get_rs_stop_timeout(scds_handle) *
			STOPPERCENTAGE) / 100;

		rc = timeout_run(scds_handle, stop_cmd, B_TRUE,
			stopsap_to, &status,
			scsxprops->sapsid,
			scsxprops->scs_hmdir,
			scsxprops->scs_user,
			scsxprops->scs_userid,
			scsxprops->scs_grpid,
			scsxprops->scs_instid);

		switch (rc) {
			case SCHA_ERR_TIMEOUT:
				/*
				 * SCMSGS
				 * @explanation
				 * The command for stopping the data service
				 * timed out.
				 * @user_action
				 * No user action needed.
				 */
				scds_syslog(LOG_ERR,
					"Stop command %s timed out.",
					stop_cmd);
				break; /* fall through, pmf will take care */
			case SCHA_ERR_NOERR:
				scds_syslog_debug(DBG_LEVEL_HIGH,
					"Stop command %s "
					"returned %d.", stop_cmd, status);
				if (status) {
					/*
					 * SCMSGS
					 * @explanation
					 * The command for stopping the data
					 * service returned an error.
					 * @user_action
					 * No user action needed.
					 */
					scds_syslog(LOG_ERR,
						"Stop command %s returned "
						"error, %d.", stop_cmd, status);
					/* just fall thru to scds_pmf_stop() */
				}
				break;
			default:
				/*
				 * SCMSGS
				 * @explanation
				 * The execution of the command for stopping
				 * the data service failed.
				 * @user_action
				 * No user action needed.
				 */
				scds_syslog(LOG_ERR,
					"Stop command %s failed with error %s.",
					stop_cmd,
					scds_error_string(rc));
				break;
		}
		free(stop_cmd);
	}

pmf_only:

	stopsap_to = (scds_get_rs_stop_timeout(scds_handle) *
			100 - STOPPERCENTAGE) / 100;
	rc = scds_pmf_stop(scds_handle, SCDS_PMF_TYPE_SVC,
			0, SIGTERM, stopsap_to);
	switch (rc) {
		case SCHA_ERR_TIMEOUT:
			/*
			 * SCMSGS
			 * @explanation
			 * Shutdown through PMF timed out.
			 * @user_action
			 * No user action needed.
			 */
			scds_syslog(LOG_ERR,
				"scds_pmf_stop() timed out.");
			break;
		case SCHA_ERR_NOERR:
			scds_syslog_debug(DBG_LEVEL_HIGH,
				"scds_pmf_stop() succeeded.");
			break;
		default:
			/*
			 * SCMSGS
			 * @explanation
			 * Shutdown through PMF returned an error.
			 * @user_action
			 * No user action needed.
			 */
			scds_syslog(LOG_ERR,
				"scds_pmf_stop() failed with error %s.",
				scds_error_string(rc));
			break;
	}

	return (rc);
}

/*
 * svc_wait():
 *
 * wait for the data service to start up fully and make sure it is running
 * healthy
 */

int
svc_wait(scds_handle_t scds_handle, scs_extprops_t *scsxprops)
{
	int			rc,
				svc_start_timeout,
				probe_timeout;
	scds_pmf_status_t	status;
	scha_err_t		err;

	/*
	 * Get the Start method timeout, port number on which to probe,
	 * the Probe timeout value
	 */
	svc_start_timeout = scds_get_rs_start_timeout(scds_handle);
	probe_timeout = scds_get_ext_probe_timeout(scds_handle);

	/*
	 * sleep for SVC_WAIT_PCT percentage of start_timeout time
	 * before actually probing the dataservice. This is to allow
	 * the dataservice to be fully up in order to reply to the
	 * probe. NOTE: the value for SVC_WAIT_PCT could be different
	 * for different dataservices.
	 */
	if (scds_svc_wait(scds_handle, (svc_start_timeout * SVC_WAIT_PCT)/100)
	    != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to start service.");
		return (1);
	}

	scds_syslog_debug(DBG_LEVEL_HIGH,
		"Using port %d for probe.", scsxprops->msg_port);

	do {
		/*
		 * probe the data service on the IP address of the
		 * network resource and the portname
		 */
		rc = svc_probe(scds_handle, scsxprops, probe_timeout);
		if (rc == SCHA_ERR_NOERR) {
			/* Success. Free up resources and return */
			return (SCHA_ERR_NOERR);
		}

		err = scds_pmf_get_status(scds_handle,
		    SCDS_PMF_TYPE_SVC, SCDS_PMF_SINGLE_INSTANCE, &status);
		if (err != SCHA_ERR_NOERR) {
			scds_syslog(LOG_ERR,
			    "Failed to retrieve process monitor facility tag.");
			return (1);
		}

		/* Check if the dataservice is still up and running */
		if (status != SCDS_PMF_MONITORED) {
			scds_syslog(LOG_ERR,
			    "Application failed to stay up. "
			    "Start method Failure.");
			return (1);
		}

		/*
		 * Dataservice is still trying to come up. Sleep for a while
		 * before probing again.
		 */
		(void) scds_svc_wait(scds_handle, SVC_WAIT_TIME);

	} while (1);

}



int
mon_start(scds_handle_t scds_handle)
{
	scha_err_t	err;

	scds_syslog_debug(DBG_LEVEL_HIGH,
		"Calling MONITOR_START method for resource <%s>.",
		scds_get_resource_name(scds_handle));

	err = scds_pmf_start(scds_handle, SCDS_PMF_TYPE_MON,
		SCDS_PMF_SINGLE_INSTANCE, "scs_probe", -1);

	if (err != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to start fault monitor.");
		return (err);
	}

	scds_syslog(LOG_INFO, "Started the fault monitor.");

	return (SCHA_ERR_NOERR); /* Successfully started Monitor */
}


/*
 * This function stops the fault monitor for a sapscssvc resource.
 * This is done via PMF. The PMF tag for the fault monitor is
 * constructed based on <RG-name_RS-name,instance_number.mon>.
 */

int
mon_stop(scds_handle_t scds_handle)
{
	scha_err_t	err;

	scds_syslog_debug(DBG_LEVEL_HIGH, "Calling scds_pmf_stop method");

	err = scds_pmf_stop(scds_handle, SCDS_PMF_TYPE_MON,
		SCDS_PMF_SINGLE_INSTANCE, SIGKILL,
		scds_get_rs_monitor_stop_timeout(scds_handle));

	if (err != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to stop fault monitor.");
		return (err);
	}

	scds_syslog(LOG_INFO, "Stopped the fault monitor.");

	return (SCHA_ERR_NOERR); /* Successfully stopped monitor */
}


/*
 * svc_probe(): Do data service specific probing. Return a float value
 * between 0 (success) and 100(complete failure).
 *
 * This function uses dpmon -info to connect to the dispatchers
 * task handler layer.
 */
int
svc_probe(scds_handle_t scds_handle,
		scs_extprops_t *scsxprops,
		int timeout)
{
	int		rc = 0;
	int		probe_status = 0;
	int 		cmd_exit_status = 0;
	char		cmd[SCDS_CMD_SIZE];


	(void) snprintf(cmd, SCDS_CMD_SIZE,
		"%s -mshost localhost -msserv %d -r %d",
		scsxprops->msg_mon,
		scsxprops->msg_port,
		(timeout / 2) - 2);

	scds_syslog_debug(DBG_LEVEL_HIGH,
		"svc_probe calling %s.",
		cmd);

	rc = timeout_run(scds_handle, cmd, B_FALSE, timeout,
		&cmd_exit_status,
		scsxprops->sapsid,
		scsxprops->scs_hmdir,
		scsxprops->scs_user,
		scsxprops->scs_userid,
		scsxprops->scs_grpid,
		scsxprops->scs_instid);

	switch (rc) {
		char internal_err_str[SCDS_CMD_SIZE];

		case SCHA_ERR_TIMEOUT:
			scds_syslog(LOG_ERR,
			    "The probe command <%s> timed out", cmd);
			probe_status = SCDS_PROBE_COMPLETE_FAILURE/2;
			break;

		case SCHA_ERR_INTERNAL:
			(void) snprintf(internal_err_str,
			    SCDS_CMD_SIZE,
			    "error occurred while launching "
			    "the probe command <%s>", cmd);
			scds_syslog(LOG_ERR,
			    "INTERNAL ERROR: %s.",
			    internal_err_str);

			probe_status = SCDS_PROBE_COMPLETE_FAILURE/2;
			break;

		case SCHA_ERR_NOERR:
			switch (cmd_exit_status) {
				case MSPROT_RET_OK:
					probe_status = 0;
					break;
				case MSPROT_RET_NOREP:
					/*
					 * may be it is a load issue, that's
					 * why it's only FAILURE/2.
					 */
					probe_status =
						SCDS_PROBE_COMPLETE_FAILURE/2;
					/*
					 * SCMSGS
					 * @explanation
					 * Probe did not get a response from
					 * the SAP message server.
					 * @user_action
					 * No user action needed.
					 */
					scds_syslog(LOG_ERR,
						"No reply from message "
						"server.");
					break;
				default:
					probe_status =
						SCDS_PROBE_COMPLETE_FAILURE;
					/*
					 * SCMSGS
					 * @explanation
					 * Probe could not connect to the SAP
					 * message server.
					 * @user_action
					 * No user action needed.
					 */
					scds_syslog(LOG_ERR,
						"Cannot connect to message "
						"server on port <%d>."
						" Return code %d.",
						scsxprops->msg_port,
						cmd_exit_status);
					break;
			}
			break;
		default:
			scds_syslog(LOG_ERR,
			    "Cannot execute %s: %s.", cmd, cmd_exit_status);
			probe_status = SCDS_PROBE_COMPLETE_FAILURE;
			break;

	}

	return (probe_status);
}


/*
 * scs_svc_start in just pmf_starts scs_start, because pmf_start then
 * does the setuid, setgid etc stuff. Will lead to problems writing to the
 * /tmp subdirs for domain sockets if executed from w/ scs_svc_start.
 */

int
scs_start(scds_handle_t handle, scs_extprops_t *scsxprops)
{
	int 		rc;
	int		status = 0;
	char 		*start_cmd;
#ifdef HAVE_SCDS_PMF_START_ENV
	char		*envp[7];
	char		shell_cmd[SCDS_ARRAY_SIZE];
	char		envsname[SCDS_ARRAY_SIZE];
	char		envhome[SCDS_ARRAY_SIZE];
	char		envsys[SCDS_ARRAY_SIZE];
	char		envlog[SCDS_ARRAY_SIZE];
	char		envld[SCDS_ARRAY_SIZE];
	char		envpath[SCDS_ARRAY_SIZE];

	uid_t		prev_uid = geteuid();
	gid_t		prev_gid = getegid();
#endif


	start_cmd = ds_string_format("%s %s %s",
		scsxprops->scs_start, "r3",
		scsxprops->scs_svcstring);

	if (start_cmd == NULL) {
		ds_internal_error("Unable to allocate string "
			"for start command.");
		return (SCHA_ERR_INTERNAL);
	}

	scds_syslog_debug(DBG_LEVEL_HIGH,
		"Trying to start SCS with command %s", start_cmd);

#ifdef HAVE_SCDS_PMF_START_ENV

	(void) snprintf(envsname, SCDS_ARRAY_SIZE,
		"SAPSYSTEMNAME=%s", scsxprops->sapsid);
	envp[0] = envsname;

	(void) snprintf(envhome, SCDS_ARRAY_SIZE,
		"HOME=%s", scsxprops->scs_hmdir);
	envp[1] = envhome;

	(void) snprintf(envsys, SCDS_ARRAY_SIZE,
		"SAPSYSTEM=%s", scsxprops->scs_instid);
	envp[2] = envsys;

	(void) snprintf(envlog, SCDS_ARRAY_SIZE,
		"LOGNAME=root");
	envp[3] = envlog;

	(void) snprintf(envpath, SCDS_ARRAY_SIZE,
		"PATH=%s", PATH_ENV);
	envp[4] = envpath;

	(void) snprintf(envpath, SCDS_ARRAY_SIZE,
		"LD_LIBRARY_PATH=/sapmnt/%s/exe:"
		"/usr/sap/%s/SYS/exe/uc/sunx86_64:"
		"/usr/sap/%s/SYS/exe/uc/sun_64:"
		"/usr/sap/%s/SYS/exe/nuc/sunx86_64:"
		"/usr/sap/%s/SYS/exe/nuc/sun_64",
		scsxprops->sapsid, scsxprops->sapsid,
		scsxprops->sapsid, scsxprops->sapsid, scsxprops->sapsid);
	envp[5] = envld;

	(void) snprintf(shell_cmd, SCDS_ARRAY_SIZE,
		"%s r3 %s",
		scsxprops->scs_start,
		scsxprops->scs_svcstring);

	envp[6] = NULL;

	if (seteuid(scsxprops->scs_userid) != 0) {
		ds_internal_error("seteuid(): %s",
			strerror(errno));
		return (SCHA_ERR_INTERNAL);
	}
	if (setegid(scsxprops->scs_grpid) != 0) {
		ds_internal_error("setegid(): %s",
			strerror(errno));
		return (SCHA_ERR_INTERNAL);
	}

	rc = scds_pmf_start_env(handle, SCDS_PMF_TYPE_SVC,
		SCDS_PMF_SINGLE_INSTANCE, shell_cmd, -1,
		envp);

	if (seteuid(prev_uid) != 0) {
		ds_internal_error("seteuid(): %s",
			strerror(errno));
		return (SCHA_ERR_INTERNAL);
	}
	if (setegid(prev_gid) != 0) {
		ds_internal_error("setegid(): %s",
			strerror(errno));
		return (SCHA_ERR_INTERNAL);
	}

#else

	rc = timeout_run(handle, start_cmd, B_TRUE, 0, &status,
		scsxprops->sapsid,
		scsxprops->scs_hmdir,
		scsxprops->scs_user,
		scsxprops->scs_userid,
		scsxprops->scs_grpid,
		scsxprops->scs_instid);

#endif

	switch (rc) {
		case SCHA_ERR_TIMEOUT:
			/*
			 * SCMSGS
			 * @explanation
			 * Start-up of the data service timed out.
			 * @user_action
			 * No user action needed.
			 */
			scds_syslog(LOG_ERR,
				"Start command %s timed out.",
				start_cmd);
			free(start_cmd);
			return (SCHA_ERR_TIMEOUT);
		case SCHA_ERR_NOERR:
			scds_syslog_debug(DBG_LEVEL_HIGH,
				"Start command %s returned %d.",
				start_cmd,
				status);
			if (status) {
				/*
				 * SCMSGS
				 * @explanation
				 * The command for starting the data service
				 * returned an error.
				 * @user_action
				 * No user action needed.
				 */
				scds_syslog(LOG_ERR,
					"Start command %s returned error, %d.",
					start_cmd, status);
				rc = scha_control(SCHA_IGNORE_FAILED_START,
					scds_get_resource_group_name(handle),
					scds_get_resource_name(handle));
				if (rc != SCHA_ERR_NOERR) {
					/*
					 * SCMSGS
					 * @explanation
					 * scha_control() failed to set
					 * resource to
					 * SCHA_IGNORE_FAILED_START.
					 * @user_action
					 * No user action needed.
					 */
					scds_syslog(LOG_ERR,
						"Error from scha_control() "
						"cannot bail out.");
					free(start_cmd);
					return (SCHA_ERR_INTERNAL);
				}
				return (1);
			}
			break;
		default:
			/*
			 * SCMSGS
			 * @explanation
			 * The execution of the command for starting the data
			 * service failed.
			 * @user_action
			 * No user action needed.
			 */
			scds_syslog(LOG_ERR,
				"Start command %s failed with "
				"error %s.",
				start_cmd,
				scds_error_string(rc));
			break;
	}
	scds_syslog_debug(DBG_LEVEL_HIGH,
		"Start command call returned %d, cmd returned %d",
		rc, status);

	free(start_cmd);
	return (rc);
}
