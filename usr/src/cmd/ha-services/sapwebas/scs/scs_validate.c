/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2004 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * scs_validate.c - for Netweaver data services.
 */

#pragma ident	"@(#)scs_validate.c	1.5	07/06/06 SMI"

#include <locale.h>
#include <rgm/libdsdev.h>
#include "scs.h"

/*
 * Check to make sure that the properties have been set properly.
 */

int
main(int argc, char *argv[])
{
	scds_handle_t   scds_handle;
	scs_extprops_t	scsxpropsp;
	int		rc = 0;

	/* I18N stuff */
	(void) setlocale(LC_ALL, "");
	(void) textdomain(TEXT_DOMAIN);
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);


	/* Process arguments passed by RGM and initialize syslog */
	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR) {
		scds_syslog_debug(DBG_LEVEL_HIGH,
			"%s failed to scds_initialize ().",
			argv[0]);
		return (1);
	}

	if ((rc = get_scs_extensions(scds_handle, &scsxpropsp)) != 0) {
		scds_syslog_debug(DBG_LEVEL_HIGH,
			"%s failed in get_scs_extensions ().",
			argv[0]);
		goto done;
	}

	if ((rc = svc_validate(scds_handle, &scsxpropsp, B_TRUE)) != 0) {
		scds_syslog_debug(DBG_LEVEL_HIGH,
			"%s failed in svc_validate ().",
			argv[0]);
	}

done:

	scds_close(&scds_handle);

	return (rc);

}
