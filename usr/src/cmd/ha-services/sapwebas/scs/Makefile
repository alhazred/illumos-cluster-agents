#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#pragma ident	"@(#)Makefile	1.10	07/06/06 SMI"
#
# cmd/ha-services/sapwebas/scs/Makefile
#


DS_COMMON = $(SRC)/cmd/ha-services/common
DS_COMMON_SRC = $(DS_COMMON)/ds_utils.c
LIBDS_COMMON_ARCH = $(MACH)

SAP_COMMON = $(SRC)/cmd/ha-services/sapwebas/common
SAP_COMMON_SRC = $(SAP_COMMON)/sap_common.c

COMMON_SRCS = scs.c

SRCS = scs_svc_start.c \
	scs_svc_stop.c \
	scs_validate.c \
	scs_update.c \
	scs_monitor_start.c \
	scs_monitor_stop.c \
	scs_monitor_check.c \
	scs_start.c \
	scs_probe.c 

# objects
COMMON_OBJS = $(COMMON_SRCS:%.c=%.o)
SAP_COMMON_OBJ = $(SAP_COMMON_SRC:$(SAP_COMMON)/%.c=%.o)

OBJECTS = $(SRCS:%.c=%.o) $(COMMON_OBJS) $(SAP_COMMON_OBJ)

PROG = $(SRCS:%.c=%)

include ../../../Makefile.cmd

# Packaging
PKGNAME = SUNWscsapwebas/scs
RTRFILE = SUNW.sapscs 

# I18N stuff
TEXT_DOMAIN = SUNW_SC_SAPSCS
MESSAGE_DIR = /opt/$(PKGNAME)/lib/locale
PIFILES = $(SRCS:%.c=%.pi) $(COMMON_SRCS:%.c=%.pi) $(DS_COMMON_SRCS:%.c=%.pi) \
 $(SAP_COMMON_SRC:%.c=%.pi)

POFILE = scs-ds.po

CFLAGS += -I$(DS_COMMON)
CFLAGS += -I$(SAP_COMMON)

# Library
LDLIBS	+= -ldsdev -lscha -lgen -lresolv -lsocket -lproject -lnsl 
LDLIBS  += -L$(DS_COMMON)/$(LIBDS_COMMON_ARCH) -lds_common 

LINTFLAGS +=  -I$(DS_COMMON)
LINTFLAGS +=  -I$(SAP_COMMON)
LINTFILES= $(SRCS:%.c=%.ln) $(COMMON_SRCS:%.c=%.ln) $(SAP_COMMON_SRC:%.c=%.ln)

.KEEP_STATE:

all: $(SAP_COMMON_OBJ) $(COMMON_OBJS) $(PROG) $(ROOTOPTETCRTR)

.PARALLEL: $(PROG)

install: all $(ROOTOPTBINPROG)

include ../../../Makefile.targ

$(SAP_COMMON_OBJ):      $(SAP_COMMON_SRC)
	$(COMPILE.c) -o $(SAP_COMMON_OBJ) -c $(SAP_COMMON_SRC) 
	$(POST_PROCESS_O)

$(PROG): $(SAP_COMMON_OBJ) $(COMMON_OBJS) $$@.o
	$(LINK.c) -o $@ $(@:%=%.o) $(SAP_COMMON_OBJ) $(COMMON_OBJS) \
                $(LDFLAGS) $(LDLIBS)
	$(POST_PROCESS)

clean:
	$(RM) $(PROG) $(OBJECTS)
