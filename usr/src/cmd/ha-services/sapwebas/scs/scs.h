/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2004 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * scs.h - for Netweaver data services.
 */

#ifndef	_SCS_H
#define	_SCS_H

#pragma ident	"@(#)scs.h	1.5	07/06/06 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#include "../common/sap_common.h"

#define	SCDS_PATHLEN		(MAXPATHLEN+1)

/* Message server extension properties */
typedef struct scs_extprops {
	char	*sapsid;
	char	*scs_user;
	char	*scs_svcstring;
	char	*scs_instancenum;
	char	*scs_start;
	char	*scs_stop;
	char	*msg_mon;
	int	msg_port;
	char	*scs_instid;
/* just set in validate, no real extension properties */
	short	use_default_scripts;
	uid_t	scs_userid;
	gid_t	scs_grpid;
	char	scs_hmdir[SCDS_PATHLEN];
} scs_extprops_t;

/* #define	SCDS_CMD_SIZE		(8 * 1024) */

/* #define	SCDS_ARRAY_SIZE		1024 */

int svc_validate(scds_handle_t, scs_extprops_t*, boolean_t);

int svc_start(scds_handle_t, scs_extprops_t*);

int svc_stop(scds_handle_t,  scs_extprops_t*, boolean_t);

int svc_wait(scds_handle_t, scs_extprops_t*);

int mon_start(scds_handle_t);

int mon_stop(scds_handle_t);

int svc_probe(scds_handle_t,
		scs_extprops_t*,
		int);

int scs_start(scds_handle_t, scs_extprops_t*);


int preprocess_cmd(scds_handle_t, char*);

int get_scs_extensions(scds_handle_t, scs_extprops_t*);

#define	SCS_EXT_SAPSID	"SAP_SID"
#define	SCS_EXT_SAPUSER	"SAP_User"
#define	SCS_EXT_SYSNR	"SAP_Instance_Number"
#define	SCS_EXT_INSTNAME	"SAP_Instance_Name"
#define	SCS_EXT_STARTSAP	"Scs_Startup_Script"
#define	SCS_EXT_STOPSAP	"Scs_Shutdown_Script"
#define	MSG_EXT_SRVMON	"Msg_Server_Monitor"
#define	MSG_EXT_PORT	"Msg_Server_Port"

#define	SID_LEN		3
#define	INSTANCE_LEN	2

#define	MSPROT_RET_OK		0
#define	MSPROT_RET_NOREP	7

#define	DEFAULT_NONE		0
#define	DEFAULT_STARTSAP	1
#define	DEFAULT_STOPSAP		2

#define	SCS_START_COMMAND	"scs_start"

#ifdef __cplusplus
}
#endif

#endif /* _SCS_H */
