/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * scs_svc_stop.c for Netweaver data services.
 */

#pragma ident	"@(#)scs_svc_stop.c	1.6	07/06/06 SMI"

#include <rgm/libdsdev.h>
#include "scs.h"

/*
 * The stop method for sapscssvc. Does some sanity checks on
 * the resource settings then stops the sapscssvc under PMF with
 * an action script.
 */

int
main(int argc, char *argv[])
{
	scds_handle_t	scds_handle;
	scs_extprops_t	scsxprops;
	int	rc;
	boolean_t	validate_failed = B_FALSE;

	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR) {
		scds_syslog_debug(DBG_LEVEL_HIGH,
			"%s failed to scds_initialize ().",
			argv[0]);
		return (1);
	}

	if ((rc = get_scs_extensions(scds_handle, &scsxprops))
			!= SCHA_ERR_NOERR) {
		scds_syslog_debug(DBG_LEVEL_HIGH,
			"%s failed in get_scs_extensions ().",
			argv[0]);
		validate_failed = B_TRUE;
	}

	if (validate_failed == B_FALSE) {
		if ((rc = svc_validate(scds_handle, &scsxprops, B_TRUE))
				!= SCHA_ERR_NOERR) {
			scds_syslog_debug(DBG_LEVEL_HIGH,
				"%s failed in svc_validate ().",
				argv[0]);
			validate_failed = B_TRUE;
		}
	}

	/*
	 * we call svc_stop, no matter if validate succeeded,
	 * to let pmf do the job.
	 */

	if ((rc = svc_stop(scds_handle, &scsxprops, validate_failed))
			!= SCHA_ERR_NOERR) {
		scds_syslog_debug(DBG_LEVEL_HIGH,
			"%s failed in svc_start ().",
			argv[0]);
		goto done;
	}

done:

	scds_close(&scds_handle);
	return (rc);
}
