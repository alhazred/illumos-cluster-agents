/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2004 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * scs_update.c - for Netweaver data services.
 */

#pragma ident	"@(#)scs_update.c	1.5	07/06/06 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <rgm/libdsdev.h>
#include "scs.h"
/*
 * Some of the resource properties might have been updated. All such
 * updatable properties are related to the fault monitor. Hence, just
 * restarting the monitor should be enough.
 */

int
main(int argc, char *argv[])
{
	scds_handle_t   scds_handle;
	scha_err_t	result;

	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR) {
		scds_syslog_debug(DBG_LEVEL_HIGH,
			"%s failed to scds_initialize ().",
			argv[0]);
		return (1);
	}

	result = scds_pmf_restart_fm(scds_handle, 0);
	if (result != SCHA_ERR_NOERR) {
		scds_syslog_debug(DBG_LEVEL_HIGH,
			"%s failed to restart fault monitor",
			argv[0]);
	}

	scds_close(&scds_handle);

	return (result);
}
