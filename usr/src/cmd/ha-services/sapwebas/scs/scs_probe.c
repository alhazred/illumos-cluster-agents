/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2004 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * scs_probe.c - for Netweaver data services.
 */

#pragma ident	"@(#)scs_probe.c	1.8	07/06/06 SMI"

#include <stdio.h>
#include <stdlib.h>
#include <nss_dbdefs.h>
#include <sys/time.h>
#include <rgm/libdsdev.h>
#include "scs.h"

int
main(int argc, char *argv[])
{
	scds_handle_t	scds_handle;
	scha_err_t 	rc;
	scs_extprops_t	scsxprops;

	int		timeout;
	int		probe_result;
	hrtime_t	ht1, ht2;
	long		dt;

	/*
	 * struct servent	serv;
	 * char		service[NSS_BUFSIZ];
	 */

	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR)
		return (1);

	timeout = scds_get_ext_probe_timeout(scds_handle);

	if (get_scs_extensions(scds_handle, &scsxprops) != 0) {
		scds_close(&scds_handle);
		return (1);
	}

	if ((rc = svc_validate(scds_handle, &scsxprops, B_FALSE)) != 0) {
		scds_syslog_debug(DBG_LEVEL_HIGH,
			"%s failed in svc_validate ().",
			argv[0]);
		return (rc);
	}

	/*
	 * remove this, will not work with J2EE SCS
	 *
	 * (void) snprintf(service, NSS_BUFSIZ, "sapms%s", scsxprops.sapsid);

	 * if (getservbyname_r(service, "tcp",
	 *	&serv, service, NSS_BUFSIZ) == NULL) {
	 */

	scds_syslog_debug(DBG_LEVEL_HIGH,
			"Using port %d for probe.", scsxprops.msg_port);
	/*
	 *	} else {
	 *	port = serv.s_port;
	 *}
	 */

	for (;;) {

		(void) scds_fm_sleep(scds_handle,
		    scds_get_rs_thorough_probe_interval(scds_handle));

		probe_result = 0;

		ht1 = gethrtime(); /* Latch probe start time */

		probe_result = svc_probe(scds_handle, &scsxprops, timeout);

		ht2 = gethrtime();

		/* Convert to milliseconds */
		dt = (long)((ht2 - ht1) / 1e6);

		scds_syslog_debug(DBG_LEVEL_HIGH,
			"%s svc_probe returned <%d>",
			argv[0], probe_result);

		(void) scds_fm_action(scds_handle, probe_result, dt);

	} 	/* Keep probing forever */
}
