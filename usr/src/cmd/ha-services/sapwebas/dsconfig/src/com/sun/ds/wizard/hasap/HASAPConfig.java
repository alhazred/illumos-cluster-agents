/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * ident	"@(#)HASAPConfig.java	1.11	07/06/06 SMI"
 */

package com.sun.ds.wizard.hasap;
import java.io.*;
import java.util.regex.*;
import java.util.*;
import java.lang.*;
import java.util.StringTokenizer;
import com.sun.cluster.agent.dataservices.common.ErrorValue;
import com.sun.cluster.agent.dataservices.common.ServiceConfig;

/**
 * This class implements the ServiceConfigInterface to provide auto-discovery
 * and validation implmentation for HA-SAP
 */

public class HASAPConfig implements  ServiceConfig {

    public static final String SID = "SAP_SID";
    public static final String ENQ_PROF = "Enqueue_Profile";
    public static final String ENQ_LOC = "Enqueue_Server";
    public static final String SAP_USER = "SAP_User";
    public static final String ENQ_I_NO = "Enqueue_Instance_Number";
    public static final String SCS_I_NO = "SAP_Instance_Number";
    public static final String SCS_NAME = "SAP_Instance_Name";
    public static final String REP_PROF = "Replica_Profile";
    public static final String REP_LOC = "Replica_Server";
    public static final String ZONE_ENABLE = "ZONE_ENABLE";
    public static final String SAP_LOC = "/usr/sap/";

    /**
     * This function getDiscoverableProperties will be called to get the names
     * of the properties that could be discovered from a particular dataservice.
     * @return String Array of all the discoverable properties.
     */
    public  String[] getDiscoverableProperties() {
	String[] properties = {
	    SID,
	    ENQ_PROF,
	    ENQ_LOC,
	    SAP_USER,
	    ENQ_I_NO,
	    SCS_I_NO,
	    SCS_NAME,
	    REP_PROF,
	    REP_LOC,
	    ZONE_ENABLE
	};

	return properties;
    }

    /**
     * This function getAllProperties will be give properies to be used
     * in HA SAP
     * @return return a set of discoverable properties
     */
    public String[] getAllProperties() {

	String[] properties = {
	    SID,
	    ENQ_PROF,
	    ENQ_LOC,
	    SAP_USER,
	    ENQ_I_NO,
	    SCS_I_NO,
	    SCS_NAME,
	    REP_PROF,
	    REP_LOC
	};

	return properties;
    }

    /**
     * This function will return all the possible values for a given array of
     * property names.
     * Inputs: The propertyNames string array is given as input to this method.
     * Optionally, a helperData Object can be provided to aid in the discovery
     * process.
     * Outputs: The method returns HashMap of array of strings indexed by the
     * property
     * name, containing all the possible values of the property in the local
     * cluster node.
     * @param propertyNames The propertyNames string array is given as input
     * to this function
     * @param helperData helperData Object to aid in the discovery process.
     * @return Hash map of array of strings containing possible values
     */

    public HashMap discoverPossibilities(
			String[] propertyNames, Object helperData) {

	HashMap properties = new HashMap();
	List strList = Arrays.asList(propertyNames);

	if (strList.contains(ZONE_ENABLE)) {
		// SAP dataservice supports zones. Return true to
		// enable zones support.
		properties.put(ZONE_ENABLE, new Boolean(true));
	}

	if (strList.contains(SID))
	    properties.put(SID, getSAPID());

	if (strList.contains(ENQ_PROF))
	    properties.put(ENQ_PROF,
			 getEnqueueServerProfile(helperData.toString()));

	if (strList.contains(ENQ_LOC))
	    properties.put(ENQ_LOC,
			 getEnqueueServerLocation((String)helperData));

	if (strList.contains(SAP_USER))
	    properties.put(SAP_USER,
			 getSAPUserName((String)helperData));

	if (strList.contains(ENQ_I_NO))
	    properties.put(ENQ_I_NO,
			 getEnqueueInstanceNumber((String)helperData));

	if (strList.contains(SCS_I_NO))
	    properties.put(SCS_I_NO,
			 getSCSInstanceNumber((String)helperData));

	if (strList.contains(SCS_NAME))
	    properties.put(SCS_NAME,
			 getSCSInstanceName((String)helperData));

	if (strList.contains(REP_PROF))
	    properties.put(REP_PROF,
			 getReplicaServerProfile((String)helperData));

	if (strList.contains(REP_LOC))
	    properties.put(REP_LOC,
			 getReplicaServerLocation((String)helperData));

	if (properties.isEmpty())
	    return null;
	else
	    return properties;
    }

    /**
     * This function will validate the user given value from the wizard and
     * return a ErrorValue object.
     * Inputs: The propertyNames string array is given as input to this method.
     * userInputs contains a hash map of the values that the user has chosen to
     * set indexed by the property.
     * Optionally, a helperData Object can be provided to aid in the validation
     * process.
     * Outputs: The method returns ErrorValue object that contains the result of
     * the validation
     * @param propertyNames string array is given as input to this method.
     * @param userInputs hash map of the values that the user has chosen to set
     * indexed by the property
     * The userInputs are the below values :
     * SAP_SID
     * Enqueue_Profile
     * Enqueue_Server
     * SAP_User
     * Enqueue_Instance_Number
     * SAP_Instance_Number
     * SAP_Instance_Name
     * Replica_Profile
     *
     * @param helperData provided to aid in the validation process
     * @return Error object containing the result of validation
     */

    public ErrorValue validateInput(String[] propertyNames, HashMap userInputs,
	    Object helperData) {
	ErrorValue error[] = new ErrorValue[propertyNames.length];
	int i = 0;
	try {
	    List strList = Arrays.asList(propertyNames);
	    if (strList.contains(SID))
		error[i++] = ValidateSapSid(userInputs.get(SID).toString());

	    if (strList.contains(ENQ_PROF))
		error[i++] = ValidateEnqueueServerProfile(
				userInputs.get(ENQ_PROF).toString());

	    if (strList.contains(ENQ_LOC))
		error[i++] = ValidateEnqueueServerLocation(
				userInputs.get(ENQ_LOC).toString());

	    if (strList.contains(SAP_USER))
		error[i++] = ValidateSAPUserName(
				userInputs.get(SAP_USER).toString());

	    if (strList.contains(REP_PROF))
		error[i++] = ValidateReplicaServerProfile(
				userInputs.get(REP_PROF).toString());

	    if (strList.contains(REP_LOC))
		error[i++] = ValidateReplicaServerLocation(
				userInputs.get(REP_LOC).toString());

	}catch (Exception ex) {
	    error[i-1] = new ErrorValue();
	    error[i-1].setErrorString(ex.toString());
	    error[i-1].setReturnVal(new Boolean(false));
	}finally {

	    ErrorValue gerror = new ErrorValue();
	    gerror.setReturnVal(new Boolean(true));

	    String errStr = "";

	    for (int j = 0; j < i; j++) {
		if (error[j].getReturnValue().booleanValue() == false) {
		    gerror.setReturnVal(error[j].getReturnValue());
		    errStr += error[j].getErrorString()+" ";
		}
	    }
	    gerror.setErrorString(errStr);
	    return gerror;
	}
    }

    /**
     * get the SAPID's
     * @return String array of SAPID's
     */
    String[] getSAPID() {

	ArrayList availbleSAPID = new ArrayList();

	File file = new File(SAP_LOC);
	if (file.exists()) {

	    String[] filesList = file.list();
	    if (filesList != null && filesList.length > 0) {
		for (int i = 0; i <  filesList.length; i++) {
		    File sapFile = new File(SAP_LOC+filesList[i]);
		    if (sapFile.isDirectory())
			if (filesList[i].length() == 3)
			    if (filesList[i].equals(filesList[i].toUpperCase()))
				availbleSAPID.add(filesList[i]);
		}

		String[] str = new String [availbleSAPID.size()];

		availbleSAPID.toArray(str);

		return str;
	    }
	}
	return null;
    }

    /**
     * This function returns possible enq location, if it exists
     * @param helperData SAP SID
     * @return Enq Server location, if it exists or else null
     */
    String getEnqueueServerLocation(String helperData)  {

	String availEqueueLocation;

	File file = new File(SAP_LOC+helperData+"/SYS/exe/run/enserver");

	if (file.exists())
	    return file.toString();

	return null;
    }

    /**
     * This function returns possible enq profiles, if it exists
     * @param helperData SAP SID
     * @return Array of Enq Server profile, if it exists or else null
     */
    String[] getEnqueueServerProfile(String helperData) {

	String enqProfileLoc =
		new String(SAP_LOC+helperData+"/SYS/profile/");

	File file = new File(enqProfileLoc);

	if (file.exists()) {
	    String[] profilesList = file.list();
	    if (profilesList != null && profilesList.length > 0) {

		ArrayList enqProfiles = new ArrayList();

		for (int i = 0; i < profilesList.length; i++) {
		    if (profilesList[i].indexOf(helperData+"_SCS") >= 0
			|| profilesList[i].indexOf(helperData+"_ASCS")
			>= 0)
			enqProfiles.add(enqProfileLoc+profilesList[i]);
		}

		if (enqProfiles.size() > 0) {
		    String[] profiles = new String[enqProfiles.size()];
		    enqProfiles.toArray(profiles);
		    return profiles;
		}
	    }
	}
	return null;
    }

    /**
     * This function returns sap user id.
     * @param helperData SAP SID
     * @return User name
     */
    String getSAPUserName(String helperData)  {

	if (!helperData.equals("null"))
	    return  helperData.toLowerCase()+"adm";

	return null;
    }

    /**
     * This function returns the value matching the key.
     * @param file name
     * @return value
     */
    String valueSolver(String fileName, String key)  {
	Pattern exp = Pattern.compile(key);
	Matcher matcher = exp.matcher(key);

	try {
		final File fp = new File(fileName);
		LineNumberReader lineReader = null;
		lineReader = new LineNumberReader(new FileReader(fp));
		String line = null;

		while ((line = lineReader.readLine()) != null) {
			matcher.reset(line); // reset the input
			if (matcher.find()) {
				StringTokenizer tokenizer =
					new StringTokenizer(line, "=");
                                String token = null;
                                while (tokenizer.hasMoreTokens()) {
					token = tokenizer.nextToken();
                                }
				return token.trim();
			}
		}
		if (lineReader != null) {
			lineReader.close();
		}
	} catch (Exception ex) {
	    return null;
	}finally {
	}

	return null;
    }

    /**
     * This function returns possible enq instance numbers, if exists.
     * @param helperData SAP SID
     * @return Array of Enq instance numbers, if it exists or else null
     */
    String[]  getEnqueueInstanceNumber(String helperData)  {

	ArrayList enqIntNumList = new ArrayList();
        String enqProfileLoc = SAP_LOC+helperData+"/SYS/profile/";
	File file = new File(enqProfileLoc);

	if (file.exists()) {
	    String[]  profiles = file.list();
	    if (profiles.length > 0 && profiles != null) {
		System.arraycopy(file.list(), 0, profiles, 0, profiles.length);

		for (int i = 0; i < profiles.length; i++) {
		    if ((profiles[i].indexOf(helperData+"_SCS") >= 0)
			|| profiles[i].indexOf(helperData+"_ASCS") >= 0) {
			String enqNo = valueSolver(enqProfileLoc+profiles[i],
					"SAPSYSTEM[^\\w]");
			if (enqNo != null)
				enqIntNumList.add(enqNo);
		    }
		}
		if (enqIntNumList.size() > 0) {
		    String[] enqIntNum = new String[enqIntNumList.size()];
		    enqIntNumList.toArray(enqIntNum);
		    return enqIntNum;
		}
	    }
	}
	return null;
    }

    /**
     *  This function returns possible scs instance numbers, if exists.
     * @param helperData SAP SID
     * @return Array of SCS instance numbers, if it exists or else null
     */
    String[] getSCSInstanceNumber(String helperData)  {

	ArrayList scsIntNumList = new ArrayList();
	String scsProfileLoc = SAP_LOC+helperData+"/SYS/profile/";
	File file = new File(scsProfileLoc);
	if (file.exists()) {
	    String[] profiles = file.list();
	    if (profiles.length > 0 && profiles != null) {
		for (int i = 0; i < profiles.length; i++) {
		    if (profiles[i].indexOf(helperData+"_SCS") >= 0
			|| profiles[i].indexOf(helperData+"_ASCS") >= 0) {
			String scsNo = valueSolver(scsProfileLoc+profiles[i],
					"SAPSYSTEM[^\\w]");
			if (scsNo != null)
				scsIntNumList.add(scsNo);
                    }
		}
	    }
	    if (scsIntNumList.size() > 0) {
		String[] SCSprofiles = new String[scsIntNumList.size()];
		scsIntNumList.toArray(SCSprofiles);
		return SCSprofiles;
	    }
	}
	return null;
    }

    /**
     * This function returns possible scs instance numbers, if exists.
     * @param helperData SAP SID
     * @return Array of SCS instance numbers, if it exists or else null
     */
    String[] getSCSInstanceName(String helperData) {

	String[] profiles = null;
	try {
	    ArrayList scsNameList = new ArrayList();
	    String scsProfileLoc = SAP_LOC+helperData+"/SYS/profile/";
	    File file = new File(scsProfileLoc);
	    if (file.exists()) {
		profiles = file.list();
		if (profiles.length > 0 && profiles != null) {
		    for (int i = 0; i < profiles.length; i++) {
			if (profiles[i].indexOf(helperData+"_SCS") >= 0
				|| profiles[i].indexOf(helperData+"_ASCS")
				>= 0) {
				String scsInstName = valueSolver(
						scsProfileLoc+profiles[i],
						"INSTANCE_NAME[^\\w]");
				if (scsInstName != null)
					scsNameList.add(scsInstName);
                        }
		    }
		    if (scsNameList.size() > 0) {
			String[] SCSprofiles = new String[scsNameList.size()];
			scsNameList.toArray(SCSprofiles);
			return SCSprofiles;
		    }
		}
	    }
	}catch (StringIndexOutOfBoundsException str) {
	    System.out.println("StringIndexOutOfBoundsException");
	}
	return null;
    }

    /**
     *  This function returns replica server profiles, if it exists
     * @param helperData SAP SID
     * @return Array of Replica Server profile, if it exists or else null
     */
    String[] getReplicaServerProfile(String helperData)  {

	ArrayList replicaProfiles = new ArrayList();
	String repProfileLoc = SAP_LOC+helperData+"/SYS/profile/";
	File file = new File(repProfileLoc);
	if (file.exists()) {
	    String[] profiles = file.list();
	    if (profiles.length > 0 && profiles != null) {
		for (int i = 0;  i < profiles.length; i++) {
		    if ((profiles[i].indexOf(helperData+"_REP") >= 0)
			|| (profiles[i].indexOf(helperData+"_ENR") >= 0)
			|| (profiles[i].indexOf(helperData+"_ERS") >= 0)
			|| (profiles[i].indexOf(helperData+"_RENQ") >= 0)
			|| (profiles[i].indexOf(helperData+"_REN") >= 0))
			replicaProfiles.add(repProfileLoc+profiles[i]);
		}
		if (replicaProfiles.size() > 0) {
		    String[] profile = new String[replicaProfiles.size()];
		    replicaProfiles.toArray(profile);
		    return profile;
		}
	    }
	}

	return null;
    }

    /**
     * This function returns possible Replica server location, if it exists
     * @param helperData SAP SID
     * @return Replica Server location, if it exists or else null
     */
    String getReplicaServerLocation(String helperData)  {

	File file = new File(SAP_LOC+helperData+"/SYS/exe/run/enrepserver");
	if (file.exists())
	    return (file.toString());
	return null;
    }

    /**
     * This function validates the SAP Sid against standard location
     * @param helperData SAP SID
     * @return ErrorValue
     */
    ErrorValue ValidateSapSid(String helperData) {

	ErrorValue error = new ErrorValue();
	error.setReturnVal(new Boolean(true));
	try {
	    File file = new File(SAP_LOC+helperData.toUpperCase());
	    if (file.exists() && file.isDirectory()) {
		return null;
	    } else {
		error.setErrorString("No file present :"+file.toString());
		error.setReturnVal(new Boolean(false));
	    }
	} catch (Exception ex) {
	    error.setErrorString(ex.getMessage());
	    error.setReturnVal(new Boolean(false));
	}finally {
	    return error;
	}
    }

    /**
     * This function validates the SAP enq server profile
     * @param userinput Profile Location
     * @return ErrorValue
     */
    ErrorValue  ValidateEnqueueServerProfile(String userinput) {

	ErrorValue error = new ErrorValue();
	error.setReturnVal(new Boolean(true));
	try {
	    if ((userinput != null) && (userinput.length() >= 0)) {
		File file = new File(userinput);
		if (file.exists() && file.canRead() && file.isFile()) {
		    error.setErrorString("Checked Path is :"+file.toString());
		    error.setReturnVal(new Boolean(true));
		} else {
		    error.setErrorString(userinput+" profile not exists");
		    error.setReturnVal(new Boolean(false));
		}
	    } else {
		error.setErrorString("profile not exists");
		error.setReturnVal(new Boolean(false));
	    }
	} catch (Exception ex) {
	    error.setErrorString(ex.getMessage());
	    error.setReturnVal(new Boolean(false));
	}finally {
	    return error;
	}
    }

    /**
     * This function validates the enq server location
     * @param userinput Enq server Location
     * @return ErrorValue
     */
    ErrorValue ValidateEnqueueServerLocation(String userinput) {

	ErrorValue error = new ErrorValue();
	error.setReturnVal(new Boolean(true));

	try {
	    if ((userinput != null) && (userinput.length() >= 0)) {
		File file = new File(userinput);
		if (file.exists() && file.canRead() && file.isFile()) {
		    error.setErrorString(userinput);
		    error.setReturnVal(new Boolean(true));
		} else {
		    error.setErrorString("Enqueue Server Location is invalid "
					+ userinput);
		    error.setReturnVal(new Boolean(false));
		}
	    }
	}catch (Exception ex) {
	    error.setErrorString(ex.getMessage());
	    error.setReturnVal(new Boolean(false));
	}finally {
	    return error;
	}
    }

    /**
     *  This function validates the SAP user name
     * @param helperData null
     * @param userinput user name/id
     * @return ErrorValue
     */
    ErrorValue ValidateSAPUserName(String userinput) {

	int validate = 0;
	ErrorValue error = new ErrorValue();
	error.setReturnVal(new Boolean(true));

	try {
	    String test = null;
	    if ((userinput != null) && (userinput.length() >= 0)) {

		File file = new File("/etc/passwd");

		if (file.exists() && file.canRead()) {

		    Pattern exp = Pattern.compile(userinput);
		    Matcher matcher = exp.matcher(userinput);

		    final File fp = new File("/etc/passwd");
		    LineNumberReader lineReader = null;
		    lineReader = new LineNumberReader(new FileReader(fp));
		    String line = null;

		    while ((line = lineReader.readLine()) != null) {
			matcher.reset(line); // reset the input
			if (matcher.find()) {
			    validate = 1;
			    break;
			}
		    }
		    if (lineReader != null)
			lineReader.close();
		}
	    }
	    if (validate == 1) {
		error.setErrorString("User is  Valid "+userinput);
		error.setReturnVal(new Boolean(true));
	    }
	} catch (Exception ex) {
	    error.setErrorString(ex.getMessage());
	    error.setReturnVal(new Boolean(false));
	}finally {
	    return error;
	}
    }

    /**
     *  This function validates the SAP replica server profile
     * @param userinput replica server location
     * @return ErrorValue
     */
    ErrorValue    ValidateReplicaServerProfile(String userinput) {

	ErrorValue error = new ErrorValue();
	error.setReturnVal(new Boolean(true));

	try {
	    if ((userinput != null) && (userinput.length() >= 0)) {
		File file = null;
		file = new File(userinput);
		if (file.exists() && file.canRead() && file.isFile()) {
		    error.setErrorString("Replica Server exists : "+userinput);
		} else {
		    error.setErrorString("Replica Server does "
					+ "not Exists :"
					+ userinput);
		    error.setReturnVal(new Boolean(false));
		}
	    }

	} catch (Exception ex) {
	    error.setErrorString(ex.getMessage());
	    error.setReturnVal(new Boolean(false));

	}finally {
	    return error;
	}
    }

    /**
     *  This function validates the replica server location
     * @param userinput replica location
     * @return ErrorValue
     */
    ErrorValue ValidateReplicaServerLocation(String userinput) {

	ErrorValue error = new ErrorValue();
	error.setReturnVal(new Boolean(true));

	try {
	    if ((userinput != null) && (userinput.length() >= 0)) {
		File file = new File(userinput);
		if (file.exists() && file.canRead() && file.isFile()) {
		    error.setErrorString("Replica Server Exists :"+userinput);
		} else {
		    error.setErrorString("Replica Server Location does "
					+ "not Exists :"
					+ userinput);
		    error.setReturnVal(new Boolean(false));
		}
	    }
	} catch (Exception ex) {
	    error.setErrorString(ex.getMessage());
	    error.setReturnVal(new Boolean(false));
	}finally {
	    return error;
	}
    }

    /**
     * This method will filter a list of discovered values to be presented for
     * user selection.
     * Inputs: The propertyName string is given as input to this method.
     * unfilteredValues contains the hash map of the nodenames and a array of
     * discovered values on that particular node.
     * helperData Object can be provided to aid in the filtering process.
     * Outputs: The method returns a array of strings containing all the
     * possible values of the property in all of cluster.
     *
     * This is a dummy implementation and returns null
     * @param propertyName string[] is given as input to this method
     * @param unfilteredValues hash map of the nodenames and a array of
     * discovered values on that particular node
     * @param helperData provided to aid in the filtering process
     * @return Array of String containing all the possible values of the
     * propertyin all of cluster
     */
    public String[] aggregateValues(String propertyName,
	    Map unfilteredValues, Object helperData) {
	return null;
    }

    /**
     * This method would perform the data service specific configuration
     * operation on the cluster. A example of this configuration would be,
     * creating the fault monitoring user in oracle, creating dfstab in nfs etc.
     * Inputs: The helperData Object can be provided to aid in the configuration
     * process.
     * Outputs: The method returns ErrorValue object that contains the result
     * of the configuration(returnValue) and a error string(errorStr) if the
     * configuration fails.
     *
     * This is a dummy implementation and returns null
     * @param helperData provided to aid in the configuration process
     * @return null
     */
    public ErrorValue applicationConfiguration(Object helperData) {
	return null;
    }

    /**
     * Discovers the possible values for the given set of properties
     * and Helper Data
     *
     * @param nodeList String array of nodes on which the discovery has to run.
     * @param propertyNames String array of names of properties whose values
     * 		are to be discovered.
     * @param helperData  The HelperData that would be used for discovering
     * 		the property values.
     *
     * @return A Map containing propertyNames as Key with their discovered
     *		values as Values.
     */
    public HashMap discoverPossibilities(String[] nodeList,
	    String[] propertyNames, Object helperData) { return null; }

    /**
     *
     * @param args
     */
    public static void main(String args[]) throws Exception {
	HASAPConfig sapw = new HASAPConfig();
	String[] property = { SID, ENQ_I_NO, SCS_NAME
//			,ENQ_PROF,ENQ_LOC,
//			SAP_USER,ENQ_I_NO,
//			SCS_NAME,REP_PROF,REP_LOC
		  };

	HashMap properties = sapw.discoverPossibilities(property, "PEP");
	System.out.println("hashMap"+properties.values());
	System.out.println(properties);
	properties.put(SID, new String("PEP"));
//	properties.put(ENQ_PROF,new String("enQprofile"));
//	properties.put(ENQ_LOC,new String("enQLocation"));
//	properties.put(REP_PROF,new String("replicaProfile"));
//	properties.put(REP_LOC,new String("replicaLocation"));
	ErrorValue err = sapw.validateInput(property, properties, null);
	System.out.println(err.getErrorString());
    }
}
