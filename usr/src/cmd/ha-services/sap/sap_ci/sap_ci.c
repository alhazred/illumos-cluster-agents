/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)sap_ci.c	1.33	07/06/06 SMI"

#include "sap_ci.h"

/* extern int errno; */

#define	SVC_WAIT_SLEEP_INTERVAL		5

/*
 * Do SAP specific validation of a given resource configuration
 * Called by start/validate/update/monitor methods.
 * Return 0 upon success, otherwise return 1.
 *
 * svc_validate checks the following:
 * - check SAP extension properties are set correctly:
 * 	SAPSID, SAP CI system num, CI service list, startup/stop scripts
 * - check sap startup and shutdown scripts are there and executable
 * - check SAP programs saposcol, sapstart, lgtst, R3trans, cleanipc exist and
 * 	are executable
 * - check that extension property x_pmf_arg_n is set
 * - check that extension property x_pmf_arg_t is set
 * - check that extension property x_probe_timeout set
 * - don't need to check network resources since sap_get_extensions() did that
 *
 * If user configure a development system on the Central instance cluster,
 * and configure 'Shutdown_dev' to be 'TRUE', then need to do:
 * - check development system startup and shutdown script are there and
 * 	executable.
 */

int
svc_validate(scds_handle_t handle, sap_extprops_t *sapxprops,
		boolean_t print_messages)
{

	int 		rc;
	boolean_t	script = B_TRUE;
	int		stop_dev_time = 0;
	scds_hasp_status_t	hasp_status;

	if (sapxprops->se_sapsid == NULL) {
		scds_syslog(LOG_ERR,
			"Property %s is not set.",
			SAP_EXT_SAPSID);
		if (print_messages) {
			(void) fprintf(stderr, gettext("Property %s is not "
				"set.\n"), SAP_EXT_SAPSID);
		}
		return (1);
	}

	if (sapxprops->se_sapsysnum == NULL) {
		scds_syslog(LOG_ERR,
			"Property %s is not set.",
			SAP_EXT_SAPSYSNUM);
		if (print_messages) {
			(void) fprintf(stderr, gettext("Property %s is not "
				"set.\n"), SAP_EXT_SAPSYSNUM);
		}
		return (1);
	}

	if (sapxprops->se_startsap == NULL) {
		scds_syslog(LOG_ERR,
			"Property %s is not set.",
			SAP_EXT_STARTSAP);
		if (print_messages) {
			(void) fprintf(stderr, gettext("Property %s is not "
				"set.\n"), SAP_EXT_STARTSAP);
		}
		return (1);
	}

	if (sapxprops->se_stopsap == NULL) {
		scds_syslog(LOG_ERR,
			"Property %s is not set.", SAP_EXT_STOPSAP);
		if (print_messages) {
			(void) fprintf(stderr, gettext("Property %s is not "
				"set.\n"), SAP_EXT_STOPSAP);
		}
		return (1);
	}


	if (sapxprops->se_services == NULL) {
		scds_syslog(LOG_ERR,
			"Property %s is not set.", SAP_EXT_SERVICES);
		if (print_messages) {
			(void) fprintf(stderr, gettext("Property %s is not "
				"set.\n"), SAP_EXT_SERVICES);
		}
		return (1);
	}

	rc = scds_hasp_check(handle, &hasp_status);
	if (rc != SCHA_ERR_NOERR) {
		/* scha_hasp_check() logs everytime it fails */
		if (print_messages) {
			(void) fprintf(stderr, gettext("INTERNAL ERROR: %s.\n"),
				gettext("scds_hasp_check failed"));
		}
		return (1);
	}

	/* hasp resources must be online (somewhere) and in our RG */
	if (hasp_status == SCDS_HASP_NOT_ONLINE) {
		scds_syslog(LOG_ERR, "Resource depends on a "
			"SUNW.HAStoragePlus type resource that is "
			"not online anywhere.");
		if (print_messages) {
			(void) fprintf(stderr, gettext("Resource depends on a "
				"SUNW.HAStoragePlus type resource that is "
				"not online anywhere.\n"));
		}
		return (1);
	} else if (hasp_status == SCDS_HASP_ERR_CONFIG) {
		/* problem syslogged by scds_hasp_check */
		if (print_messages) {
			(void) fprintf(stderr, gettext("This resource depends "
				"on a HAStoragePlus resouce that is in a "
				"different Resource Group. This configuration "
				"is not supported.\n"));
		}
		return (1);
	}

	/* check SAP scripts are there */
	rc = check_sap_file(script, sapxprops->se_sapsid,
		sapxprops->se_startsap, hasp_status, print_messages);
	if (rc != 0)
		return (1);

	rc = check_sap_file(script, sapxprops->se_sapsid,
		sapxprops->se_stopsap, hasp_status, print_messages);
	if (rc != 0)
		return (1);

	/* Check SAP binary programs are there and executable */
	script = B_FALSE;

	/* SAP startup program */
	rc = check_sap_file(script, sapxprops->se_sapsid,
		SAP_PRG_SAPOSCOL, hasp_status, print_messages);
	if (rc != 0)
		return (1);

	/*
	 * only need to check if it's either SCDS_HASP_NO_RESOURCE or
	 * SCDS_HASP_ONLINE_LOCAL which is !SCDS_HASP_ONLINE_NOT_LOCAL
	 */
	if (hasp_status != SCDS_HASP_ONLINE_NOT_LOCAL) {
		/* Make sure saposcol is the right one */
		if ((rc = correct_saposcol(sapxprops->se_sapsid,
						print_messages)) != 0) {
			return (1);
		}
	}

	rc = check_sap_file(script, sapxprops->se_sapsid,
		SAP_PRG_STARTSAP, hasp_status, print_messages);
	if (rc != 0)
		return (1);

	/* SAP test program for LG-Layer */
	rc = check_sap_file(script, sapxprops->se_sapsid,
		SAP_CMD_LGTST, hasp_status, print_messages);
	if (rc != 0)
		return (1);

	/* SAP transport program - used to check connection to Message Server */
	rc = check_sap_file(script, sapxprops->se_sapsid,
		SAP_PRG_R3TRANS, hasp_status, print_messages);
	if (rc != 0)
		return (1);

	/* SAP cleanup ipc program */
	rc = check_sap_file(script, sapxprops->se_sapsid,
		SAP_CMD_CLEANIPC, hasp_status, print_messages);
	if (rc != 0)
		return (1);

	/* check monitor retry count */
	if (scds_get_ext_monitor_retry_count(handle) <= 0) {
		scds_syslog(LOG_ERR, "Property %s is not set.",
			"Monitor_retry_count");
		if (print_messages) {
			(void) fprintf(stderr, gettext("Property %s is not "
				"set.\n"), "Monitor_retry_count");
		}
		return (1);
	}
	if (scds_get_ext_monitor_retry_interval(handle) <= 0) {
		scds_syslog(LOG_ERR, "Property %s is not set.",
			"Monitor_retry_interval");
		if (print_messages) {
			(void) fprintf(stderr, gettext("Property %s is not "
				"set.\n"), "Monitor_retry_interval");
		}
		return (1);
	}

	if (scds_get_ext_probe_timeout(handle) <= 0) {
		scds_syslog(LOG_ERR,
		    "Property %s is not set.", SAP_EXT_PROBE_TIMEOUT);
		if (print_messages) {
			(void) fprintf(stderr, gettext("Property %s is not "
				"set.\n"), SAP_EXT_PROBE_TIMEOUT);
		}
		return (1);
	}

	/* Check for shutdown dev system script */
	if (sapxprops->se_shutdowndev) {
		script = B_TRUE;
		rc = check_sap_file(script, sapxprops->se_devsid,
				sapxprops->se_stopsap_dev, hasp_status,
				print_messages);
		if (rc != 0)
			return (1);

		/*
		 * make sure the time to stop dev system is sufficient.
		 * 5 seconds for now.
		 */
		stop_dev_time = sapxprops->se_stopdevpct *
			scds_get_rs_start_timeout(handle)/100;
		if (stop_dev_time < 5) {
			/*
			 * SCMSGS
			 * @explanation
			 * Time allocated to stop the development system is
			 * too small.
			 * @user_action
			 * The time for stopping the development system is a
			 * percentage of the total Start_timeout. Increase the
			 * value for property Start_timeout or the value for
			 * propety Dev_stop_pct.
			 */
			scds_syslog(LOG_ERR,
				"Time allocated to stop development system is "
				"too small (less than 5 seconds).");
			if (print_messages) {
				(void) fprintf(stderr, gettext("Time allocated "
					"to stop development system is too "
					"small (less than 5 seconds).\n"));
			}
			return (1);
		}
	}
	return (0);
}


/*
 * Start SAP processes under pmf.
 * First thing is to check whether sap is already up and running by checking
 * on the lock file. If sap is not up, then go ahead with the followings.
 * Otherwise, return 1, so RGM knows start failed. Currently, there is no
 * way of "return-early" from the data service start method: set the resource
 * state to "start-failed", so that RGM will not keep trying to restart and
 * failover the resource. So until that RFE is done, checking upfront will
 * sap is up or not, will put the resource in "start-failed" state in a very
 * short time, instead of taking a could be very long time, eg.
 * start-timeout * retry_count.
 *
 * SAP processes are divided into two groups. The first group contains the
 * saposcol process. It will be started under PMF along with a tag as
 * saposcol.svc, in this subroutine.
 *
 * The other group contains the message server, main dispatcher and all other
 * processes that's created by the main dispatcher. This group of processes
 * will be started under PMF with a tag index 0, in another subroutine
 * sap_ci_startR3(). The reason for not starting up sap in this routine is
 * before we can start up the second group of sap processes, we need to make
 * sure the DB is already up and running. And if the attempt to start up
 * sap failed again in start method, we need to pull on DB again, and we
 * won't be able to do that if we start sap under PMF in this subroutine.
 *
 * This routine returns 0 upon success, otherwise, return 1.
 */
int
svc_start(scds_handle_t handle, char *sapsid, char *sapservice, char *sapsysnum)
{
	int			rc;
	scha_err_t		pmf_rc;
	char			sapstart[SCDS_ARRAY_SIZE];
	char 			internal_err_str[INT_ERR_MSG_SIZE];
	char			lockfile[MAXPATHLEN];


	scds_syslog_debug(DBG_LEVEL_HIGH, "Calling START method.");

	/* check sap is up or not */
	rc = snprintf(lockfile, MAXPATHLEN, "%s/%s/%s%s/%s/%s",
		SAP_DIR_USRSAP,
		sapsid,
		sapservice,
		sapsysnum,
		SAP_DIR_WORK,
		SAP_LOCK_FILE);
	if (rc == -1) {
		(void) sprintf(internal_err_str,
			"Failed to form sap lock file path");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		return (1);
	}

	rc = is_sap_running(lockfile, scds_get_resource_name(handle));

	switch (rc) {
		case 1:
			scds_syslog(LOG_ERR, "SAP is already running.");
			return (1);
		case 0:
			/* sap is not up yet */
			break;
		case -1:
			scds_syslog(LOG_ERR, "Failed to start %s.", "SAP");
			return (1);
		default:
			break;
	}

	/* otherwise, sap was not up, so continue with the starting up */

	/* startup saposcol process under pmf tag 'saposcol.svc'. */
	rc = start_saposcol(handle, sapsid);
	if (rc != 0) {
		/* error when try to bring up saposcol, return 1 */
		return (1);
	}

	/*
	 * now start the other group of sap processes by calling
	 * subroutine sap_ci_startR3().
	 */

	/* form path to start SAP start up command */
	rc = snprintf(sapstart, sizeof (sapstart), "%s/%s -R %s -T %s -G %s",
	    scds_get_rt_rt_basedir(handle),
	    SAP_CMD_STARTR3,
	    scds_get_resource_name(handle),
	    scds_get_resource_type_name(handle),
	    scds_get_resource_group_name(handle));

	if (rc == -1) {
		(void) sprintf(internal_err_str,
			"Failed to form start command %s", sapstart);
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		return (1);
	}

	scds_syslog(LOG_NOTICE, "Starting %s with command %s.",
	    "SAP Central Instance", sapstart);

	/*
	 * Do not check whether this sap is already running locally or
	 * remotely since it won't get here if sap is
	 * already started either locally or remotely.
	 */

	/* start it under PMF with tag index 0 */
	pmf_rc = scds_pmf_start(handle, SCDS_PMF_TYPE_SVC,
		SCDS_PMF_SINGLE_INSTANCE, sapstart, -1);
	if (pmf_rc == SCHA_ERR_NOERR) {
		scds_syslog(LOG_INFO,
			"Started sap processes under PMF successfully.");
	} else {
		scds_syslog(LOG_ERR,
		    "Failed to start sap processes with command %s.", sapstart);
	}
	/* return success/failure status */
	return (pmf_rc);
}


int
svc_wait(scds_handle_t scds_handle, sap_extprops_t *sapxprops)
{
	int   	probe_result;
	int	probe_timeout;
	int	ms_pid = -1, disp_pid = -1, rc = 0;

	/* Get Probe timeout value */
	probe_timeout = scds_get_ext_probe_timeout(scds_handle);

	/*
	 * Instead of calling sleep(), call scds_svc_wait() so that if
	 * service fails too many times (for case that sap is already up and
	 * running on a local/remote node), we give up and return early.
	 */

	/*
	 * first retrieve the pid for message server and dispatcher
	 * and then write them to files.
	 */
	do { /* will keep looping until get it or timeout eventually */
		rc = retrieve_ms_pid(sapxprops->se_sapsid,
			scds_get_resource_name(scds_handle));
		if (rc == 0)
			break;

		scds_syslog(LOG_NOTICE, "Waiting for %s to come up.",
		    "message server");

		/* wait for 3 seconds if pid could not be retrieved */
		if (scds_svc_wait(scds_handle, 3) != SCHA_ERR_NOERR) {
			scds_syslog(LOG_ERR, "Failed to start service.");
		}
	} while (1);

	do {
		rc = retrieve_disp_pid(scds_get_rt_rt_basedir(scds_handle),
					sapxprops->se_sapsid,
					sapxprops->se_services,
					sapxprops->se_sapsysnum,
					scds_get_resource_name(scds_handle));
		if (rc == 0)
			break;

		scds_syslog(LOG_NOTICE, "Waiting for %s to come up.",
		    "SAP Central Instance main dispatcher");

		/* wait for 3 seconds if pid could not be retrieved */
		if (scds_svc_wait(scds_handle, 3) != SCHA_ERR_NOERR) {
			scds_syslog(LOG_ERR, "Failed to start service.");
		}
	} while (1);

	/* then read the pids from files to pass to svc_probe() */
	rc = read_in_ms_pid(&ms_pid, scds_get_resource_name(scds_handle));
	rc = read_in_disp_pid(&disp_pid, scds_get_resource_name(scds_handle));

	/*
	 * probe the data service SAP with the pids for the message
	 * server and the dispatcher.
	 */
	do {
		probe_result = svc_probe(scds_handle, sapxprops, probe_timeout,
					&ms_pid, &disp_pid);
		if (probe_result == SCHA_ERR_NOERR) {
			/*
			 * If we get here, the application is up,
			 * and we return success
			 */
			scds_syslog(LOG_NOTICE, "Service is online.");
			return (0);
		}

		scds_syslog(LOG_NOTICE, "Waiting for %s to come up.",
		    "SAP Central Instance");

		/*
		 * Dataservice is still trying to come up. Sleep for a while
		 * before probing again. Instead of calling sleep(),
		 * call scds_svc_wait() so that if service fails too
		 * many times, we give up and return early.
		 */
		if (scds_svc_wait(scds_handle, SVC_WAIT_SLEEP_INTERVAL)
			!= SCHA_ERR_NOERR) {
			scds_syslog(LOG_ERR, "Failed to start service.");
		}

		/*
		 * the probe was not successful, we will continue to probe the
		 * application if there's still time.
		 */
		/* We rely on RGM to timeout and terminate the program */
	} while (1);
}

/*
 * This function actually startup SAP R/3, after making sure the DB is
 * available. Then it'll start up sap calling startsap script user provides.
 * The main startup program stays up after SAP is brought up, which means
 * even sap_ci_startR3() returns, PMF won't repeatedly restart SAP.
 */
int
sap_ci_startR3(scds_handle_t handle, sap_extprops_t *sapxprops)
{
	int 		rc, probe_rc;
	int		err;
	int		retry_interval;
	char		startsapcmd[SCDS_ARRAY_SIZE];
	char		cleanipccmd[SCDS_ARRAY_SIZE];
	boolean_t	giveup = B_FALSE;
	char		*admuser;
	char		internal_err_str[INT_ERR_MSG_SIZE];

	/*
	 * get retry interval value in case DB connection is not working
	 * the first time.
	 */
	retry_interval = sapxprops->se_startinterval;

	/* get admuser */
	admuser = get_admuser_name(sapxprops->se_sapsid);

	/*
	 * Form command to start SAP
	 * 	su - <sid>adm -c '<startup script> r3'
	 */
	rc = snprintf(startsapcmd, sizeof (startsapcmd),
	    "/usr/bin/su - %s -c '%s r3'",
	    admuser,
	    sapxprops->se_startsap);

	if (rc == -1) {
		(void) sprintf(internal_err_str,
			"Failed to form SAP start command: "
			"Output error encounted");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		return (rc);
	}

	/*
	 * Form command to cleanipc's
	 * 	/usr/sap/<SAPSID>/SYS/exe/run/cleanipc <sapsysnum> remove'
	 */
	rc = snprintf(cleanipccmd, sizeof (cleanipccmd),
	    "%s/%s/%s/%s %s %s'",
	    SAP_DIR_USRSAP,
	    sapxprops->se_sapsid,
	    SAP_DIR_SYS,
	    SAP_CMD_CLEANIPC,
	    sapxprops->se_sapsysnum,
	    SAP_CMD_CLEANIPC_REM);

	if (rc == -1) {
		(void) sprintf(internal_err_str,
			"Failed to form cleanipc command: "
			"Output error encounted");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		return (rc);
	}

	/*
	 * Big loop to try to start SAP.
	 */
	while (giveup == B_FALSE) {
		/*
		 * Can only start SAP if the database is up
		 */
		err = probe_db(handle, sapxprops->se_sapsid,
				PROBE_DB_TIMEOUT, &probe_rc);
		if (err != SAP_OK) {
			/* some internal error, exit */
			return (err);
		} else if (probe_rc != SAP_PROBE_OK) {
			scds_syslog(LOG_ERR,
			    "Database might be down, HA-SAP won't take any "
			    "action. Will check again in %d seconds.",
			    retry_interval);
			(void) sleep((unsigned int)retry_interval);

		/*
		 * It'll be nice to set start_fail and exit here
		 * instead waiting for next circle.
		 * But since can't do interRG dependency, can't really check
		 * state of oracle resource which is in a different RG. Also
		 * we hope the DB is not available yet, but after waiting for
		 * a while, it will come up and SAP will connect at that point.
		 * So it has value to wait, instead of just quit right away.
		 */
		} else /* DB is up */
			giveup = B_TRUE;
	} /* while */

	/*
	 * Use the cleanipc program from the SAP installation.
	 */
	scds_syslog_debug(DBG_LEVEL_HIGH,
		    "Running cleanipc with command %s.",
		    cleanipccmd);

	rc = system(cleanipccmd);
	if (rc == -1) {

		/*
		 * SCMSGS
		 * @explanation
		 * system(3C) call returned an error, vfork or exec failed
		 * for example.
		 * @user_action
		 * This may be due to lack of system resources. Check whether
		 * the system is low on memory, also check /var/adm/messages.
		 */
		scds_syslog(LOG_ERR,
		    "Unable to execute %s : %s (%d),"
		    " HA-SAP will continue to start SAP.",
		    cleanipccmd, strerror(errno), errno);

	}

	/* Deal with termination of child in system(3C) call above */
	if (WIFSIGNALED((uint_t)rc)) {

		/*
		 * SCMSGS
		 * @explanation
		 * Command exited abnormally as process was not able to catch
		 * a particular signal.
		 * @user_action
		 * Contact your authorized Sun service provider.
		 */
		scds_syslog(LOG_ERR,
		    "Command %s failed to complete"
		    " - unable to catch signal: %d,"
		    " HA-SAP will continue to start SAP.",
		    cleanipccmd, WTERMSIG((uint_t)rc));

	} else if (WIFEXITED((uint_t)rc) && WEXITSTATUS((uint_t)rc) != 0) {

		/*
		 * SCMSGS
		 * @explanation
		 * Command exited normally but the exit status was was set to a
		 * non-zero value.
		 * @user_action
		 * Contact your authorized Sun service provider.
		 */
		scds_syslog(LOG_ERR,
		    "Command %s returned with non-zero exit status %d,"
		    " HA-SAP will continue to start SAP.",
		    cleanipccmd, WEXITSTATUS((uint_t)rc));

	} else {
		scds_syslog_debug(DBG_LEVEL_LOW,
		    "Command %s completed successfully. (rc=%d)",
		    cleanipccmd, rc);
	}

	/*
	 * We have cleaned up as much as possible. Now start SAP
	 * using startsap script user provided.
	 */
	scds_syslog_debug(DBG_LEVEL_LOW, "Trying to start SAP with command %s.",
		    startsapcmd);

	rc = system(startsapcmd);

	if (rc == -1) {
		(void) sprintf(internal_err_str,
			"Cannot execute %s: %s",
			startsapcmd, strerror(errno));	/*lint !e746 */
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		return (rc);
	}
	if (WIFSIGNALED((uint_t)rc) || (WIFEXITED((uint_t)rc) &&
		WEXITSTATUS((uint_t)rc) != 0)) {
		(void) sprintf(internal_err_str, "%s failed to complete",
			startsapcmd);
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		return (1);
	}
	return (SCHA_ERR_NOERR);
}


/*
 * Stop SAP processes which started up under PMF with tag index 0. This
 * also include the OS collector process saposcol.
 * It first checks and see if sap was up and stop method should not shut
 * it down or not, by checking the state file. If state file is there, then
 * just exit(0). Otherwise, go on to shutdown sap processes. Then it'll
 * check and see if this is the last resource that uses saposcol, if yes,
 * it will stop saposcol process also. Otherwise, just decreases the lock
 * count for saposcol.
 */
int
svc_stop(scds_handle_t handle, sap_extprops_t *sapxprops)
{
	int 		rc, exit_code;
	char		cmd[SCDS_ARRAY_SIZE];
	int		stopsap_to;  /* timeouts */
	char		*admuser = NULL, *cleanipc_path = NULL;
	char		internal_err_str[INT_ERR_MSG_SIZE];

	scds_syslog_debug(DBG_LEVEL_LOW, "Calling svc_stop.");

	/* check state file */
	rc = check_state_file(scds_get_resource_name(handle));
	if (rc == 0) { /* state file exit, no need to stop sap, just exit(0) */
		scds_syslog(LOG_NOTICE,
			"SAP was brought up outside of HA-SAP, "
			"HA-SAP will not shut it down.");
		return (0);
	} else if (rc > 0) { /* state file doesn't exit, go ahead to stop sap */
		scds_syslog_debug(DBG_LEVEL_HIGH,
			"State file doesn't exit, go ahead with stop.");
	    } else { /* internal error from check_state_file */
		(void) sprintf(internal_err_str,
			"Failed to check state file");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		return (1);
	    }

	/* Now stop the processes */
	scds_syslog(LOG_NOTICE, "Stopping %s.", "SAP Central Instance");

	/* get admuser */
	admuser = get_admuser_name(sapxprops->se_sapsid);

	stopsap_to = (scds_get_rs_stop_timeout(handle) *
			sapxprops->se_stopsappct) / 100;
	/*
	 * form the command to stop SAP gracefully:
	 * 	su - <sid>adm -c '<stopsap script> r3'
	 */
	rc = snprintf(cmd, sizeof (cmd), "/usr/bin/su - %s -c '%s r3'",
		admuser, sapxprops->se_stopsap);

	if (rc == -1) {
		(void) sprintf(internal_err_str,
			"Failed to form stop command");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		return (1);
	}

	/*
	 * tell pmf to stop monitoring sap processes (excluding saposcol)
	 * if the tag is not there, scds_pmf_stop_monitoring() returns
	 * SCHA_ERR_NOERR.
	 */
	rc = scds_pmf_stop_monitoring(handle, SCDS_PMF_TYPE_SVC,
			SCDS_PMF_SINGLE_INSTANCE);
	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
			"Failed to stop fault monitor.");
		free(admuser);
		return (1);
	}

	scds_syslog_debug(DBG_LEVEL_HIGH,
		"Stopping SAP processes with command %s.", cmd);

	/*
	 * scds_timerun() will run the cmd with hatimerun
	 * will ignore the exit_code since stopsap script will exit 7 if
	 * if couldn't find the kill.sap file (which was created after
	 * sap is brought on-line). And for case like sap was never brought
	 * on-line, kill.sap file won't be found. And we still want to just
	 * exit 0 from the stop method.
	 * For case that sap was indeed brought on-line, and had problem
	 * stopping it via stopsap script, well, we'll call PMF to kill
	 * all the processes later anyway. So still want to exit 0 for
	 * stop method.
	 */
	rc = scds_timerun(handle, cmd, stopsap_to, SIGTERM, &exit_code);

	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
			"Failed to stop the process with: %s. "
			"Retry with SIGKILL.", cmd);
	}

	/*
	 * call PMF to terminate processes that might be still running
	 * with SIGKILL. If they are not running anymore, scds_pmf_stop()
	 * would do no harm.
	 */
	rc = scds_pmf_stop(handle, SCDS_PMF_TYPE_SVC,
		SCDS_PMF_SINGLE_INSTANCE, SIGKILL, -1);
	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
			"Failed to stop SAP processes under PMF with SIGKILL.");
	}

	/*
	 * cleanipc cleans up shared memory/semaphores that might got left
	 * behind from unkilled sap processes.
	 * Form command to cleanipc's
	 * 	/usr/sap/<SAPSID>/SYS/exe/run/cleanipc <sapsysnum> remove
	 */

	cleanipc_path = get_bin_path(sapxprops->se_sapsid, B_FALSE);

	if (cleanipc_path == NULL) {
		scds_syslog(LOG_ERR, "Failed to retrieve SAP binary path.");
		return (1);
	}

	rc = snprintf(cmd, sizeof (cmd),
		"%s/%s %s %s",
		cleanipc_path,
		SAP_CMD_CLEANIPC,
		sapxprops->se_sapsysnum,
		SAP_CMD_CLEANIPC_REM);

	if (rc == -1) {
		(void) sprintf(internal_err_str,
			"Failed to form cleanipc command");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		if (cleanipc_path != NULL) free(cleanipc_path);
		return (1);
	}

	scds_syslog_debug(DBG_LEVEL_LOW, "Running cleanipc with command %s.",
	    cmd);
	scds_syslog(LOG_NOTICE, "Cleaning up IPC facilities.");
	rc = system(cmd);

	if (rc != 0) {
		scds_syslog_debug(DBG_LEVEL_LOW,
		    "Cannot execute %s: %s.", cmd, strerror(errno));
		if (cleanipc_path != NULL) free(cleanipc_path);
		return (1);
	}
	if (WIFSIGNALED((uint_t)rc) || (WIFEXITED((uint_t)rc) &&
		(rc = WEXITSTATUS((uint_t)rc)) != 0)) {
		(void) sprintf(internal_err_str, "%s failed to complete", cmd);
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		if (cleanipc_path != NULL) free(cleanipc_path);
		return (1);
	}

	/* stop saposcol */
	if (stop_saposcol(handle, sapxprops->se_sapsid) != 0) {
		scds_syslog(LOG_ERR, "Failed to stop %s.", "saposcol");
		if (cleanipc_path != NULL) free(cleanipc_path);
		return (1);
	}

	scds_syslog(LOG_NOTICE, "Successfully stopped %s.", "resource");

	if (cleanipc_path != NULL) free(cleanipc_path);
	return (0);
}


/* Start the fault monitor under PMF */
int
svc_fm_start(scds_handle_t handle)
{


	scds_syslog_debug(DBG_LEVEL_LOW, "Calling svc_fm_start.");

	if (scds_pmf_start(handle,
	    SCDS_PMF_TYPE_MON,
	    SCDS_PMF_SINGLE_INSTANCE,
	    "sap_ci_probe", 0) != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to start fault monitor.");
		return (1);
	}

	scds_syslog(LOG_INFO,
	    "Started the fault monitor.");
	return (SCHA_ERR_NOERR);
}



/* Stop the fault monitor */
int
svc_fm_stop(scds_handle_t scds_handle)
{
	scds_syslog_debug(DBG_LEVEL_LOW, "Calling svc_fm_stop.");

	if (scds_pmf_stop(scds_handle, SCDS_PMF_TYPE_MON, 0, SIGKILL, -1) !=
	    SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to stop fault monitor.");
		return (1);
	}

	scds_syslog(LOG_INFO, "Stopped the fault monitor.");

	return (SCHA_ERR_NOERR);

}



/*
 * Check if the host where this program is executing is healthy.
 * Currently, we just call validate and return success if validate passes
 */
int
svc_fm_check(scds_handle_t handle, sap_extprops_t *sapxprops)
{

	scds_syslog_debug(DBG_LEVEL_LOW, "Calling svc_fm_check.");

	if (svc_validate(handle, sapxprops, B_FALSE) != SCHA_ERR_NOERR) {
		return (1);		/* Validation failure */
	}

	scds_syslog(LOG_INFO, "Completed successfully.");
	return (SCHA_ERR_NOERR);
}


/*
 * svc_probe(): Do data service specific probing.
 * It checks three things in the following sequence:
 * 1) message server and main dispatcher processes
 * 2) message server connection via lgtst
 * 3) connection to DB
 *
 * If any error (complete failure or partial failure) happens during those
 * three checks, this routine return with that error. If finishes successfully,
 * it returns SAP_PROBE_OK.
 */
int
svc_probe(scds_handle_t scds_handle, sap_extprops_t *sapxprops,
	int probe_timeout, int *ms_pid, int *disp_pid)
{
	int		sap_rc = SAP_PROBE_OK;
	int		probe_rc = SAP_PROBE_ERROR;
	int		err = SAP_OK;

	scds_syslog_debug(DBG_LEVEL_LOW, "Calling svc_probe.");

	/*
	 * The order that these probes are done was determined due to the
	 * possibility of probe timeout. We want to do the probes that catch
	 * the more serious errors first (ie dispatcher not running.) If
	 * there is time, then we do the other probes.
	 */

	/*
	 * Check the processes for SAP message server and dispatcher.
	 */
	err = probe_proc(&probe_rc, scds_get_rt_rt_basedir(scds_handle),
			sapxprops->se_sapsid,
			sapxprops->se_services, sapxprops->se_sapsysnum,
			scds_get_resource_name(scds_handle), ms_pid, disp_pid);

	if (err != SAP_OK) {
		/*
		 * system problem, not probing error, so don't want to rush to
		 * judgement. Only set the error to a quarter. Because for a
		 * loaded system, setting to total failure might cause restart
		 * unnecessarily.
		 */
		scds_syslog_debug(DBG_LEVEL_LOW,
		    "An error occured while probing the "
		    "SAP Critical Processes.");
		sap_rc = SCDS_PROBE_COMPLETE_FAILURE/4;
		return (sap_rc);
	} else if (probe_rc != SAP_PROBE_OK) {
		scds_syslog_debug(DBG_LEVEL_LOW,
		    "SAP Critical Processes are not "
		    "running.");
		sap_rc = SCDS_PROBE_COMPLETE_FAILURE;
		return (sap_rc);
	} else {
		scds_syslog_debug(DBG_LEVEL_LOW,
		    "SAP Critical Processes are running.");
	}

	/*
	 * Probe the SAP message server connection using the SAP test program
	 * for LG-Layer "lgtst". The hostname used to run lgtst is the logical
	 * host in the SAP rg, by default. But user can configure to use the
	 * local host instead.
	 *
	 * Also, user can specify how many times that lgtst check failed,
	 * before it reach a total failure. The default is 2 in the RTR file.
	 */
	if (sapxprops->se_mscheckretry > 0) {
	    err = probe_ms(scds_handle, sapxprops, probe_timeout, &probe_rc);

	    if (err != SAP_OK) {
		/*
		 * system problem, not probing error, so don't want to rush to
		 * judgement. Only set the error to a quarter. Because for a
		 * loaded system, setting to total failure might cause restart
		 * unnecessarily.
		 */
		scds_syslog_debug(DBG_LEVEL_LOW,
		    "An error occured while probing the "
		    "SAP Message Server.");
		sap_rc = SCDS_PROBE_COMPLETE_FAILURE/4;
		return (sap_rc);
	    } else if (probe_rc != SAP_PROBE_OK) {
			if (probe_rc == SAP_PROBE_TIMEOUT) {
				sap_rc = SCDS_PROBE_COMPLETE_FAILURE/2;
				return (sap_rc);
			} else {
				/*
				 * extension property "se_mscheckretry"
				 * controls how many times probe_ms failed
				 * before it becomes a complete failure.
				 */
				sap_rc = SCDS_PROBE_COMPLETE_FAILURE /
					sapxprops->se_mscheckretry;
				return (sap_rc);
			    }
		    } else {
				scds_syslog_debug(DBG_LEVEL_LOW,
					"SAP message server is reachable.");
		    }
	} /* if mscheckretry > 0, else don't need to probe ms */

	/*
	 * Probe the SAP Database using SAP transport program R3trans.
	 * If the DB check failed, set the SAP rg status to DEGRADED, but
	 * HA-SAP will not take any action.
	 */

	err = probe_db(scds_handle, sapxprops->se_sapsid, probe_timeout,
		    &probe_rc);

	if (err != SAP_OK) {
		/*
		 * system problem, not probing error, so don't want to rush to
		 * judgement. Only set the error to a quarter. Because for a
		 * loaded system, setting to total failure might cause restart
		 * unnecessarily.
		 */
		scds_syslog_debug(DBG_LEVEL_HIGH,
			    "An error occured while probing the "
			    "SAP Database.");
		sap_rc = SCDS_PROBE_COMPLETE_FAILURE/4;
		return (sap_rc);
	}
	return (sap_rc);
}


/*
 * Probe the SAP Message Server
 * This function checks the health of the message server by using the SAP
 * utility 'lgtst'. If the user didn't change the default for the extension
 * property 'Lgtst_ms_with_logicalhostname', then it will call lgtst with the
 * logical host name. Otherwise, it will call lgtst with the local host name.
 *
 * This routine returns 0 upon success, otherwise 1.
 * probe_rc returns SAP_PROBE_ERROR if lgtst test failed. Returns SAP_PROBE_OK
 * if succeeded, returns SAP_PROBE_TIMEOUT if hatimerun times out.
 */
int
probe_ms(scds_handle_t handle, sap_extprops_t *sapxprops,
	int probe_time, int *probe_rc)
{
	int	rc, exit_code = 0, logical;
	char	ms_cmd[SCDS_ARRAY_SIZE];
	char	internal_err_str[INT_ERR_MSG_SIZE];

	if (probe_rc == NULL) {
		(void) sprintf(internal_err_str,
		    "No space was allocated for probe result");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		return (1);
	}

	if (probe_time < SAP_PROBE_MIN_TIMEOUT) {
		probe_time = SAP_PROBE_MIN_TIMEOUT;
	}

	/* initialize to ERROR just in case we bailout early */
	*probe_rc = SAP_PROBE_ERROR;

	/*
	 * SAP Message Server is probed by using sap utility lgtst. The
	 * command is ran under hatimerun.
	 *	ms_cmd = /usr/sap/<SAPSID>/SYS/exe/run/lgtst
	 *		-H <CI> -S sapms<SID>|grep ENQ > /dev/null 2>&1
	 * 	hatimerun -t probe_time ms_cmd
	 */

	if (sapxprops->se_domsprobe == B_TRUE) {
		logical = B_TRUE;
	} else logical = B_FALSE;

	if (logical)
		rc = snprintf(ms_cmd, sizeof (ms_cmd),
			"%s/%s/%s/%s -H %s -S %s |grep ENQ %s",
			SAP_DIR_USRSAP,
			sapxprops->se_sapsid,
			SAP_DIR_SYS,
			SAP_CMD_LGTST,
			sapxprops->se_ciloghost,
			sapxprops->se_msname,
			SAP_STDOUT_REDIR);
	else
		rc = snprintf(ms_cmd, sizeof (ms_cmd),
			"%s/%s/%s/%s -H %s -S %s |grep ENQ %s",
			SAP_DIR_USRSAP,
			sapxprops->se_sapsid,
			SAP_DIR_SYS,
			SAP_CMD_LGTST,
			SAP_LOCAL_HOST,
			sapxprops->se_msname,
			SAP_STDOUT_REDIR);

	if (rc == -1) {
		(void) sprintf(internal_err_str,
			"Failed to form message server probe command");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
			return (1);
	}

	/* scds_timerun() will run the cmd with hatimerun */
	rc = scds_timerun(handle, ms_cmd, probe_time, SIGTERM, &exit_code);

	if (rc != SCHA_ERR_NOERR) {
		if (rc == SCHA_ERR_TIMEOUT) {
			/* hatimerun time out */
			/*
			 * SCMSGS
			 * @explanation
			 * Checking SAP message server with utility lgtst
			 * times out. This may happen under heavy system load.
			 * @user_action
			 * You might consider increasing the Probe_timeout
			 * property. Try switching the resource group to
			 * another node using scswitch (1M).
			 */
			scds_syslog(LOG_ERR,
				"Probing SAP Message Server times out with "
				"command %s.", ms_cmd);
			*probe_rc = SAP_PROBE_TIMEOUT;
		} else {
			(void) sprintf(internal_err_str,
				"Failed to probe SAP Mesasge Server with "
				"command %s", ms_cmd);
			scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
						internal_err_str);
			return (1);
		}
	} else if (exit_code == 0) {
		*probe_rc = SAP_PROBE_OK;
		} else {
			/*
			 * lgtst command itself executed successfully, but
			 * the checking failed.
			 */
			*probe_rc = SAP_PROBE_ERROR;
		}

	scds_syslog_debug(DBG_LEVEL_LOW,
	    "SAP Message Server probe result: %s.",
	    (*probe_rc == SAP_PROBE_OK) ?
	    SAP_PROBE_SUCCESS_STRING :
	    SAP_PROBE_FAILURE_STRING);

	return (SAP_PROBE_OK);
}


/*
 * Check for the SAP Critical Processes
 * by doing a kill(pid,0). Return 0 upon success, otherwise, return 1.
 */
int
probe_proc(int *probe_rc, const char *rtbase, char *sapsid, char *service,
		char *sysnum, const char *rname, int *ms_pid, int *disp_pid)
{
	int	rc;
	char	internal_err_str[INT_ERR_MSG_SIZE];

	if (probe_rc == NULL) {
		(void) sprintf(internal_err_str,
		    "No space was allocated for probe result");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		return (1);
	}

	*probe_rc = SAP_PROBE_OK;

	/*
	 * check the message server process and the dispatcher process and
	 * see if they are still there.
	 * kill() will return 0 if successful, -1 if not. If not successful,
	 * errno is set to indicate the error: EINVAL, EPERM or ESRCH. Only
	 * ESRCH should trigger probe error. Treat other ones as non-sap
	 * related internal error, and shouldn't trigger probe error.
	 */
	rc = kill(*ms_pid, 0);
	if (rc != 0) {
		if (errno == ESRCH) {
			scds_syslog_debug(DBG_LEVEL_HIGH,
				"Message server process is not "
				"running. Check again.");
			/*
			 * sap might be restarted, so the pid is no longer
			 * valid. Try to read them in again, and kill it
			 * again. If that fails, flag error.
			 */
			if ((rc = retrieve_ms_pid(sapsid, rname)) != 0) {
			    /* BEGIN CSTYLED */
			    /*
			     * CSTYLED is used to prevent cstyle
			     * complaints about block comments
			     * indented with spaces.
			     */
			    /*
			     * SCMSGS
			     * @explanation
			     * Failed to retrieve the process ID for the
			     * message server indicating the message server
			     * process is not running.
			     * @user_action
			     * No action needed. The fault monitor will detect
			     * this and take appropriate action.
			     */
			    /* END CSTYLED */
			    scds_syslog(LOG_ERR,
				"Failed to retrieve Message server pid.");
			    *probe_rc = SAP_PROBE_ERROR;
			    return (0);
			}

			(void) read_in_ms_pid(ms_pid, rname);
			(void) read_in_disp_pid(disp_pid, rname);

			rc = kill(*ms_pid, 0);
			if (rc != 0) {
			    if (errno == ESRCH) {
				/*
				 * SCMSGS
				 * @explanation
				 * Message server process is not present on
				 * the process list indicating message server
				 * process is not running on this node.
				 * @user_action
				 * No action needed. Fault monitor will detect
				 * that message server process is not running,
				 * and take appropriate action.
				 */
				scds_syslog(LOG_ERR,
				    "Message Server Process is not running. "
				    "pid was %d.", *ms_pid);
				*probe_rc = SAP_PROBE_ERROR;
				return (SCHA_ERR_NOERR);
			    } else {
				(void) sprintf(internal_err_str,
				    "Failed to check message server process "
				    "status with kill(pid,0) command. pid was "
				    "%d", *ms_pid);
				scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
							internal_err_str);
				return (1);
			    }
			}
		} else {
			(void) sprintf(internal_err_str,
			    "Failed to check message server process "
			    "status with kill(pid,0) command. pid was "
			    "%d", *ms_pid);
			scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
							internal_err_str);
			return (1);
		}
	}

	rc = kill(*disp_pid, 0);
	if (rc != 0) {
		if (errno == ESRCH) {
			/*
			 * sap might be restarted, so the pid is no longer
			 * valid. Try to read them in again, and kill it
			 * again. If that fails, flag error.
			 */
			if ((rc = retrieve_disp_pid(rtbase,
						    sapsid,
						    service,
						    sysnum,
						    rname)) != 0) {
				scds_syslog(LOG_ERR,
				    "Failed to retrieve main dispatcher pid.");
				*probe_rc = SAP_PROBE_ERROR;
				return (0);
			}
			(void) read_in_ms_pid(ms_pid, rname);
			(void) read_in_disp_pid(disp_pid, rname);

			rc = kill(*disp_pid, 0);
			if (rc != 0) {
			    if (errno == ESRCH) {
				scds_syslog(LOG_ERR,
					"Dispatcher Process is not running."
					" pid was %d", *disp_pid);
				*probe_rc = SAP_PROBE_ERROR;
				return (SCHA_ERR_NOERR);
			    } else {
				(void) sprintf(internal_err_str,
				    "Failed to check main dispatcher process "
				    "status with kill(pid,0) command. pid was "
				    "%d", *disp_pid);
				scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
							internal_err_str);
				return (1);
			    }
			}
		} else {
			(void) sprintf(internal_err_str,
				"Failed to check main dispatcher process "
				"status with kill(pid,0) command. pid was "
				"%d", *disp_pid);
			scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
							internal_err_str);
			return (1);
		}
	}
	return (SCHA_ERR_NOERR);
}


/*
 * This routine stops the develop system using the script that is
 * passed in to the routine under hatimerun with option '-a'.
 * Return 0 upon success, otherwise (including hatimerun times out), returns 1.
 */
int
stop_dev_system(char *devsid, int stop_dev_time, char *stopsap_dev_script)
{

	char	stopcmd[SCDS_ARRAY_SIZE];
	int	rc = 0;
	char	*devadm;
	char	internal_err_str[INT_ERR_MSG_SIZE];

	/* only shutdown dev system if it's running, otherwise, do nothing. */

	/*
	 * well, checking the sap processes are not always reliable since
	 * the dev system could be just starting up, and at the steps of
	 * starting up the database. The check for the sap processes will
	 * fail and the code will not shutdown the dev system.
	 * It's safer to always shutdown the dev system if user specified
	 * the shutdown script.
	 */
	devadm = get_admuser_name(devsid);
	rc = snprintf(stopcmd, SCDS_ARRAY_SIZE,
		"/usr/cluster/bin/hatimerun -t %d -a su - %s -c '%s all'",
		stop_dev_time,
		devadm,
		stopsap_dev_script);

	if (rc == -1) {
		(void) sprintf(internal_err_str,
			"Failed to form stop development system command."
			" Output error encounted");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		return (1);
	}

	scds_syslog(LOG_NOTICE, "Stopping %s with command %s.",
	    "the Development system", stopcmd);

	rc = system(stopcmd);
	if (rc == -1) {
		(void) sprintf(internal_err_str, "Cannot execute %s: %s",
			stopcmd, strerror(errno));
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		return (1);
	}

	/*
	 * if hatimerun timeout, hatimerun will exit 99, but stopsap will
	 * continue asynchronously, which will be on best-effort base.
	 */
	if (WIFSIGNALED((uint_t)rc) || (WIFEXITED((uint_t)rc) &&
		WEXITSTATUS((uint_t)rc) != 0)) {
		if (WIFEXITED((uint_t)rc) && (WEXITSTATUS((uint_t)rc) == 99)) {
			/* hatimerun time out */
			/*
			 * SCMSGS
			 * @explanation
			 * Failed to shutdown the development system within
			 * the timeout period. It will be continuously
			 * shutting down in the background. Meanwhile, the
			 * Central instance will be started up.
			 * @user_action
			 * No action needed. You might consider increasing the
			 * Dev_stop_pct property or Start_timeout property.
			 */
			scds_syslog(LOG_ERR,
				"Failed to stop development system within %d "
				"seconds. Will continue to stop the development"
				" system in the background. Meanwhile, the "
				"production system Central Instance is started"
				" up now.", stop_dev_time);
			return (1);
		}
	} else {
		/*
		 * SCMSGS
		 * @explanation
		 * Informational message.
		 * @user_action
		 * No action needed.
		 */
		scds_syslog(LOG_INFO,
			"Development system shut down successfully.");
		return (0);
	}
	return (0);
}


/* Debugging function to print out the SAP extension properties. */
static void
print_xprops(int dlevel, scds_handle_t handle, sap_extprops_t *sapxprops)
{

	if (sapxprops == NULL) {
		scds_syslog_debug(dlevel,
		    "NULL value passed to ",
		    "extension property print function\n");
	}

	scds_syslog_debug(dlevel,
	    "The extension properties for SAP "
	    "resource <%s> in group <%s> are:\n",
	    scds_get_resource_name(handle),
	    scds_get_resource_group_name(handle));

	scds_syslog_debug(dlevel,
	    "<%s> = <%s>\n",
	    SAP_EXT_SAPSID,
	    (sapxprops->se_sapsid == NULL ? "NULL" : sapxprops->se_sapsid));

	scds_syslog_debug(dlevel,
	    "<%s> = <%s>\n",
	    SAP_EXT_SAPSYSNUM,
	    (sapxprops->se_sapsysnum == NULL ?
		"NULL" :
		sapxprops->se_sapsysnum));

	scds_syslog_debug(dlevel,
	    "<%s> = <%s>\n", SAP_EXT_STARTSAP,
	    (sapxprops->se_startsap == NULL ? "NULL" : sapxprops->se_startsap));

	scds_syslog_debug(dlevel,
	    "<%s> = <%s>\n", SAP_EXT_STOPSAP,
	    (sapxprops->se_stopsap == NULL ? "NULL" : sapxprops->se_stopsap));

	scds_syslog_debug(dlevel,
	    "<%s> = <%s>\n",
	    SAP_EXT_SERVICES,
	    (sapxprops->se_services == NULL ? "NULL" : sapxprops->se_services));

	scds_syslog_debug(dlevel,
	    "<%s> = <%d>\n",
	    SAP_EXT_STARTINTERVAL,
	    sapxprops->se_startinterval);

	scds_syslog_debug(dlevel,
	    "<%s> = <%s>\n", SAP_EXT_MSNAME,
	    (sapxprops->se_msname == NULL ? "NULL" : sapxprops->se_msname));

	scds_syslog_debug(dlevel,
	    "<%s> = <%d>\n", SAP_EXT_STOPSAPPCT,
	    sapxprops->se_stopsappct);

	scds_syslog_debug(dlevel,
	    "<%s> = <%s>\n",
	    SAP_EXT_DOMSPROBE,
	    (sapxprops->se_domsprobe == B_TRUE ? "TRUE" : "FALSE"));

	scds_syslog_debug(dlevel,
	    "<%s> = <%d>\n",
	    SAP_EXT_MSCHECKRETRY,
	    sapxprops->se_mscheckretry);
}



/*
 * Get the extension properties from the SAP config that aren't in handle
 * Return 0 upon success, otherwise, return 1.
 */
int
sap_get_extensions(scds_handle_t handle, sap_extprops_t *sapxpropsp,
		boolean_t print_messages)
{
	int				err;
	scha_extprop_value_t 		*extprop = NULL;
	scds_net_resource_list_t	*snrl = NULL;
	char				*ms_name = NULL;
	char				ms_prefix[6] = "sapms";
	char				internal_err_str[INT_ERR_MSG_SIZE];



	if (sapxpropsp == NULL) {
		(void) sprintf(internal_err_str,
			"SAP extension property structure was NULL");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		if (print_messages) {
			(void) fprintf(stderr, gettext("INTERNAL ERROR: %s.\n"),
				gettext(internal_err_str));
		}
		return (1);
	}

	/* Start filling in the sap_extpropt_t struct */
	err = scds_get_ext_property(handle, SAP_EXT_SAPSID, SCHA_PTYPE_STRING,
	    &extprop);
	if (err != SCHA_ERR_NOERR || extprop == NULL) {
		scds_syslog(LOG_ERR,
			"Failed to retrieve the property %s: %s.",
			SAP_EXT_SAPSID, scds_error_string(err));
		if (print_messages) {
			(void) fprintf(stderr, gettext("Failed to retrieve the "
				"property %s: %s.\n"), SAP_EXT_SAPSID,
				gettext(scds_error_string(err)));
		}
		return (1);
	}

	if (extprop->val.val_str[0] == '\0') {
		scds_syslog(LOG_ERR, "Property %s is not set.", SAP_EXT_SAPSID);
		if (print_messages) {
			(void) fprintf(stderr, gettext("Property %s is not "
				"set.\n"), SAP_EXT_SAPSID);
		}
		if (extprop) scds_free_ext_property(extprop);
		return (1);
	}

	if ((sapxpropsp->se_sapsid = strdup(extprop->val.val_str)) == NULL) {
		(void) sprintf(internal_err_str,
			"Failed to allocate space for %s", SAP_EXT_SAPSID);
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		if (print_messages) {
			(void) fprintf(stderr, gettext("INTERNAL ERROR: %s.\n"),
				gettext(internal_err_str));
		}
		if (extprop) scds_free_ext_property(extprop);
		return (1);
	}

	if (extprop) scds_free_ext_property(extprop);
	extprop = NULL;

	err = scds_get_ext_property(handle, SAP_EXT_SAPSYSNUM,
	    SCHA_PTYPE_STRING, &extprop);
	if (err != SCHA_ERR_NOERR || extprop == NULL) {
		scds_syslog(LOG_ERR,
			"Failed to retrieve the property %s: %s.",
			SAP_EXT_SAPSYSNUM, scds_error_string(err));
		if (print_messages) {
			(void) fprintf(stderr, gettext("Failed to retrieve the "
				"property %s: %s.\n"), SAP_EXT_SAPSYSNUM,
				gettext(scds_error_string(err)));
		}
		return (1);
	}
	if ((sapxpropsp->se_sapsysnum = strdup(extprop->val.val_str)) == NULL) {
		(void) sprintf(internal_err_str,
			"Failed to allocate space for %s", SAP_EXT_SAPSYSNUM);
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		if (print_messages) {
			(void) fprintf(stderr, gettext("INTERNAL ERROR: %s.\n"),
				gettext(internal_err_str));
		}
		if (extprop) scds_free_ext_property(extprop);
		return (1);
	}

	if (extprop) scds_free_ext_property(extprop);
	extprop = NULL;

	err = scds_get_ext_property(handle, SAP_EXT_SERVICES, SCHA_PTYPE_STRING,
	    &extprop);
	if (err != SCHA_ERR_NOERR || extprop == NULL) {
		scds_syslog(LOG_ERR,
			"Failed to retrieve the property %s: %s.",
			SAP_EXT_SERVICES, scds_error_string(err));
		if (print_messages) {
			(void) fprintf(stderr, gettext("Failed to retrieve the "
				"property %s: %s.\n"), SAP_EXT_SERVICES,
				gettext(scds_error_string(err)));
		}
		return (1);
	}
	if ((sapxpropsp->se_services = strdup(extprop->val.val_str)) == NULL) {
		(void) sprintf(internal_err_str,
			"Failed to allocate space for %s", SAP_EXT_SERVICES);
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		if (print_messages) {
			(void) fprintf(stderr, gettext("INTERNAL ERROR: %s.\n"),
				gettext(internal_err_str));
		}
		if (extprop) scds_free_ext_property(extprop);
		return (1);
	}
	if (extprop) scds_free_ext_property(extprop);
	extprop = NULL;

	err = scds_get_ext_property(handle, SAP_EXT_STARTSAP, SCHA_PTYPE_STRING,
		&extprop);
	if (err != SCHA_ERR_NOERR || extprop == NULL) {
		scds_syslog(LOG_ERR,
			"Failed to retrieve the property %s: %s.",
			SAP_EXT_STARTSAP, scds_error_string(err));
		if (print_messages) {
			(void) fprintf(stderr, gettext("Failed to retrieve the "
				"property %s: %s.\n"), SAP_EXT_STARTSAP,
				gettext(scds_error_string(err)));
		}
		return (1);
	}
	if (extprop->val.val_str[0] == '\0') {
		scds_syslog(LOG_ERR,
			"Property %s is not set.", SAP_EXT_STARTSAP);
		if (print_messages) {
			(void) fprintf(stderr, gettext("Property %s is not "
				"set.\n"), SAP_EXT_STARTSAP);
		}
		if (extprop) scds_free_ext_property(extprop);
		return (1);
	}

	if ((sapxpropsp->se_startsap = strdup(extprop->val.val_str)) == NULL) {
		(void) sprintf(internal_err_str,
			"Failed to allocate space for %s", SAP_EXT_STARTSAP);
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		if (print_messages) {
			(void) fprintf(stderr, gettext("INTERNAL ERROR: %s.\n"),
				gettext(internal_err_str));
		}
		if (extprop) scds_free_ext_property(extprop);
		return (1);
	}

	if (extprop) scds_free_ext_property(extprop);
	extprop = NULL;

	err = scds_get_ext_property(handle, SAP_EXT_STOPSAP, SCHA_PTYPE_STRING,
		&extprop);
	if (err != SCHA_ERR_NOERR || extprop == NULL) {
		scds_syslog(LOG_ERR,
			"Failed to retrieve the property %s: %s.",
			SAP_EXT_STOPSAP, scds_error_string(err));
		if (print_messages) {
			(void) fprintf(stderr, gettext("Failed to retrieve the "
				"property %s: %s.\n"), SAP_EXT_STOPSAP,
				gettext(scds_error_string(err)));
		}
		return (1);
	}

	if (extprop->val.val_str[0] == '\0') {
		scds_syslog(LOG_ERR, "Property %s is not set.",
			SAP_EXT_STOPSAP);
		if (print_messages) {
			(void) fprintf(stderr, gettext("Property %s is not "
				"set.\n"), SAP_EXT_STOPSAP);
		}
		if (extprop) scds_free_ext_property(extprop);
		return (1);
	}

	if ((sapxpropsp->se_stopsap = strdup(extprop->val.val_str)) == NULL) {
		(void) sprintf(internal_err_str,
			"Failed to allocate space for %s", SAP_EXT_STOPSAP);
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		if (print_messages) {
			(void) fprintf(stderr, gettext("INTERNAL ERROR: %s.\n"),
				gettext(internal_err_str));
		}
		if (extprop) scds_free_ext_property(extprop);
		return (1);
	}

	if (extprop) scds_free_ext_property(extprop);
	extprop = NULL;

	/* RGM sets Ci_start_retry_interval to 1 if not set in RTR file ??? */
	err = scds_get_ext_property(handle, SAP_EXT_STARTINTERVAL,
	    SCHA_PTYPE_INT, &extprop);
	sapxpropsp->se_startinterval = extprop->val.val_int;
	extprop = NULL;

	err = scds_get_ext_property(handle, SAP_EXT_MSNAME,
		SCHA_PTYPE_STRING, &extprop);
	if (err != SCHA_ERR_NOERR || extprop == NULL) {
		scds_syslog(LOG_ERR,
			"Failed to retrieve the property %s: %s.",
			SAP_EXT_MSNAME, scds_error_string(err));
		if (print_messages) {
			(void) fprintf(stderr, gettext("Failed to retrieve the "
				"property %s: %s.\n"), SAP_EXT_MSNAME,
				gettext(scds_error_string(err)));
		}
		return (1);
	}
	if (extprop->val.val_str[0] == '\0') {
		ms_name = calloc(1, (sizeof (ms_prefix)
				+ strlen(sapxpropsp->se_sapsid) + 1));
		if (ms_name == NULL) {
			scds_syslog(LOG_ERR, "Out of memory.");
			if (print_messages) {
				(void) fprintf(stderr,
					gettext("Out of memory.\n"));
			}
			return (1);
		}
		if (strcpy(ms_name, ms_prefix) == NULL) {
			(void) sprintf(internal_err_str,
				"Failed to allocate space for %s",
				SAP_EXT_MSNAME);
			scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
							internal_err_str);
			if (print_messages) {
				(void) fprintf(stderr,
					gettext("INTERNAL ERROR: %s.\n"),
					gettext(internal_err_str));
			}
			return (1);
		}

		if (strcat(ms_name, sapxpropsp->se_sapsid) == NULL) {
			(void) sprintf(internal_err_str,
				"Failed to allocate space for %s",
				SAP_EXT_MSNAME);
			scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
							internal_err_str);
			if (print_messages) {
				(void) fprintf(stderr,
					gettext("INTERNAL ERROR: %s.\n"),
					gettext(internal_err_str));
			}
			if (ms_name != NULL) free(ms_name);
			return (1);
		}
		if ((sapxpropsp->se_msname = strdup(ms_name)) == NULL) {
			(void) sprintf(internal_err_str,
				"Failed to allocate space for %s",
				SAP_EXT_MSNAME);
			scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
							internal_err_str);
			if (print_messages) {
				(void) fprintf(stderr,
					gettext("INTERNAL ERROR: %s.\n"),
					gettext(internal_err_str));
			}
			if (extprop) scds_free_ext_property(extprop);
			if (ms_name != NULL) free(ms_name);
			return (1);
		}
	} else {
		if ((sapxpropsp->se_msname =
				strdup(extprop->val.val_str)) == NULL) {
			(void) sprintf(internal_err_str,
				"Failed to allocate space for %s",
				SAP_EXT_MSNAME);
			scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
							internal_err_str);
			if (print_messages) {
				(void) fprintf(stderr,
					gettext("INTERNAL ERROR: %s.\n"),
					gettext(internal_err_str));
			}
			if (extprop) scds_free_ext_property(extprop);
			if (ms_name != NULL) free(ms_name);
			return (1);
		}

	}
	if (extprop) scds_free_ext_property(extprop);
	extprop = NULL;

	/* set Stop_sap_pct with MIN=1 in RTR */
	err = scds_get_ext_property(handle, SAP_EXT_STOPSAPPCT,
		SCHA_PTYPE_INT, &extprop);
	sapxpropsp->se_stopsappct = extprop->val.val_int;
	extprop = NULL;

	err = scds_get_ext_property(handle, SAP_EXT_DOMSPROBE,
	    SCHA_PTYPE_BOOLEAN, &extprop);
	if (err != SCHA_ERR_NOERR || extprop == NULL) {
		return (1);
	}
	sapxpropsp->se_domsprobe = extprop->val.val_boolean;
	extprop = NULL;

	err = scds_get_ext_property(handle, SAP_EXT_MSCHECKRETRY,
		SCHA_PTYPE_INT, &extprop);
	if (err != SCHA_ERR_NOERR || extprop == NULL) {
		return (1);
	}
	if (extprop->val.val_int < 0) {
		sapxpropsp->se_mscheckretry = 0;
	} else {
		sapxpropsp->se_mscheckretry = extprop->val.val_int;
	}
	extprop = NULL;

	/* Get the logical host address of the central instance */
	if ((err = scds_get_rs_hostnames(handle, &snrl)) != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
			"No network address resource in resource group.");
		if (print_messages) {
			(void) fprintf(stderr, gettext("No network address "
				"resource in resource group.\n"));
		}
		return (1);
	}

	if (snrl == NULL || snrl->num_netresources == 0) {
		scds_syslog(LOG_ERR,
		    "No network address resource in resource group.");
		if (print_messages) {
			(void) fprintf(stderr, gettext("No network address "
				"resource in resource group.\n"));
		}
		return (1);
	}

	/*
	 * get the IP address for the network resource (needed for probing)
	 * Will get the first one if there are more than 1 network resource
	 * configured
	 */
	if ((sapxpropsp->se_ciloghost =
			strdup(snrl->netresources->hostnames[0])) == NULL) {
		(void) sprintf(internal_err_str,
			"Failed to allocate space for %s", SAP_EXT_CILOGHOST);
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		if (print_messages) {
			(void) fprintf(stderr, gettext("INTERNAL ERROR: %s.\n"),
				gettext(internal_err_str));
		}
		return (1);
	}

	/* The followings are for shutdown/startup development system */
	err = scds_get_ext_property(handle, SAP_EXT_SHUTDOWN,
		SCHA_PTYPE_BOOLEAN, &extprop);
	if (err != SCHA_ERR_NOERR || extprop == NULL) {
		return (1);
	}
	sapxpropsp->se_shutdowndev = extprop->val.val_boolean;
	extprop = NULL;

	/*
	 * only get the extension properties for shutting down dev system
	 * if configured so.
	 */
	if (sapxpropsp->se_shutdowndev) {
		err = scds_get_ext_property(handle, SAP_EXT_DEVSID,
			SCHA_PTYPE_STRING, &extprop);
		if (err != SCHA_ERR_NOERR || extprop == NULL) {
			scds_syslog(LOG_ERR,
				"Failed to retrieve the property %s: %s.",
					SAP_EXT_DEVSID, scds_error_string(err));
			if (print_messages) {
				(void) fprintf(stderr, gettext("Failed to "
					"retrieve the property %s: %s.\n"),
					SAP_EXT_DEVSID,
					gettext(scds_error_string(err)));
			}
			return (1);
		}
		if (extprop->val.val_str[0] == '\0') {
			scds_syslog(LOG_ERR,
				"Property %s is not set.", SAP_EXT_DEVSID);
			if (print_messages) {
				(void) fprintf(stderr, gettext("Property %s is "
					"not set.\n"), SAP_EXT_DEVSID);
			}
			if (extprop) scds_free_ext_property(extprop);
			return (1);
		}

		if ((sapxpropsp->se_devsid =
				strdup(extprop->val.val_str)) == NULL) {
			(void) sprintf(internal_err_str,
				"Failed to allocate space for %s",
				SAP_EXT_DEVSID);
			scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
							internal_err_str);
			if (print_messages) {
				(void) fprintf(stderr,
					gettext("INTERNAL ERROR: %s.\n"),
					gettext(internal_err_str));
			}
			if (extprop) scds_free_ext_property(extprop);
			return (1);
		}
		if (extprop) scds_free_ext_property(extprop);
		extprop = NULL;

		err = scds_get_ext_property(handle, SAP_EXT_DEV_STOPSAP,
			SCHA_PTYPE_STRING, &extprop);
		if (err != SCHA_ERR_NOERR || extprop == NULL) {
			scds_syslog(LOG_ERR,
				"Failed to retrieve the property %s: %s.",
				SAP_EXT_DEV_STOPSAP, scds_error_string(err));
			if (print_messages) {
				(void) fprintf(stderr, gettext("Failed to "
					"retrieve the property %s: %s.\n"),
					SAP_EXT_DEV_STOPSAP,
					gettext(scds_error_string(err)));
			}
			return (1);
		}

		if (extprop->val.val_str[0] == '\0') {
			scds_syslog(LOG_ERR,
				"Property %s is not set.", SAP_EXT_DEV_STOPSAP);
			if (print_messages) {
				(void) fprintf(stderr, gettext("Property %s is "
					"not set.\n"), SAP_EXT_DEV_STOPSAP);
			}
			if (extprop) scds_free_ext_property(extprop);
			return (1);
		}

		if ((sapxpropsp->se_stopsap_dev =
				strdup(extprop->val.val_str)) == NULL) {
			(void) sprintf(internal_err_str,
				"Failed to allocate space for %s",
				SAP_EXT_DEV_STOPSAP);
			scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
						internal_err_str);
			if (print_messages) {
				(void) fprintf(stderr,
					gettext("INTERNAL ERROR: %s.\n"),
					gettext(internal_err_str));
			}
			if (extprop) scds_free_ext_property(extprop);
			return (1);
		}

		if (extprop) scds_free_ext_property(extprop);
		extprop = NULL;

		err = scds_get_ext_property(handle, SAP_EXE_DEV_STOPSAPPCT,
			SCHA_PTYPE_INT, &extprop);

		sapxpropsp->se_stopdevpct = extprop->val.val_int;
		extprop = NULL;
	}
	/* end of stuff for developement */

	print_xprops(DBG_LEVEL_HIGH, handle, sapxpropsp);
	return (0);
}
