/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 1998-2002 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_SAP_CI_H
#define	_SAP_CI_H

#pragma ident	"@(#)sap_ci.h	1.14	07/06/06 SMI"


#ifdef	__cplusplus
extern "C" {
#endif

#include	"../util/sap_util.h"

#define	SAP_SVC_NAME		"sap_ci"

/* SAP extensions */
#define	SAP_EXT_SAPSID		"SAPSID"
#define	SAP_EXT_SAPSYSNUM	"Ci_instance_id"
#define	SAP_EXT_STARTSAP	"Ci_startup_script"
#define	SAP_EXT_STOPSAP		"Ci_shutdown_script"
#define	SAP_EXT_SERVICES	"Ci_services_string"
#define	SAP_EXT_STARTINTERVAL	"Ci_start_retry_interval"
#define	SAP_EXT_MSNAME		"Message_server_name"
#define	SAP_EXT_PSCHECKRETRY	"Process_check_retry"
#define	SAP_EXT_MSCHECKRETRY	"Check_ms_retry"
#define	SAP_EXT_STOPSAPPCT	"Stop_sap_pct"
#define	SAP_EXT_DOMSPROBE	"Lgtst_ms_with_logicalhostname"
#define	SAP_EXT_DODBPROBE	"Do_db_probe"
#define	SAP_EXT_CILOGHOST	"CI_LOGICAL_HOST"

/* Data service provided binary to start R3 */
#define	SAP_CMD_STARTR3		"sap_ci_startR3"

/* temp files for storing validate results */
#define	SAP_TMP_COUNT_FILE	"/tmp/sapscript_count"
#define	SAP_MS_COUNT_FILE	"/tmp/ms_count"

/* used with lgtst */
#define	SAP_LOCAL_HOST		"127.0.0.1"

/* used for env variable fm_on for shutdown/start dev system */
/* #define FM_ON			"fm_on=1" */
#define	SAP_EXT_SHUTDOWN	"Shutdown_dev"
#define	SAP_EXT_DEVSID		"Dev_sapsid"
#define	SAP_EXT_DEV_STARTSAP	"Dev_startup_script"
#define	SAP_EXT_DEV_STOPSAP	"Dev_shutdown_script"
#define	SAP_EXE_DEV_STOPSAPPCT	"Dev_stop_pct"

/* extension property */
typedef struct sap_extprops
{
	char		*se_sapsid;
	char		*se_sapsysnum;	/* Ci_instance_id */
	char		*se_startsap;	/* Ci_startup_script */
	char		*se_services;	/* Ci_services_string */
	char		*se_msname;	/* Message_server_name */
	int		se_mscheckretry;	/* Check_ms_retry */
	int		se_startinterval;	/* Ci_start_retry_interval */
	char		*se_stopsap;	/* Ci_shutdown_script */
	boolean_t	se_domsprobe;	/* Lgtst_ms_with_logicalhost */
	int		se_stopsappct;	/* Stop_sap_pct */
	char		*se_ciloghost;	/* This is not a real xprop */
	/* these are for shuting down the dev system */
	boolean_t	se_shutdowndev;	/* Shutdown_dev */
	char		*se_devsid;	/* Dev_sapsid */
	char		*se_startsap_dev;	/* Dev_startup_script */
	char		*se_stopsap_dev;	/* Dev_shutdown_script */
	int		se_stopdevpct;	/* Stop_dev_pct */
} sap_extprops_t;

/* Function to fill in SAP specific properties */
int sap_get_extensions(scds_handle_t scds_handle, sap_extprops_t *sapxpropsp,
		boolean_t print_messages);

/* Validation and update */
int svc_validate(scds_handle_t scds_handle, sap_extprops_t *sapxprops,
		boolean_t print_messages);
int svc_update(scds_handle_t scds_handle, sap_extprops_t *sapxprops);

/* Basic start and stop functionality */
int
svc_start(scds_handle_t scds_handle, char *sapsid, char *sapservice,
	char *sapsysnum);
int sap_ci_startR3(scds_handle_t scds_handle, sap_extprops_t *sapxprops);
int svc_stop(scds_handle_t scds_handle, sap_extprops_t *sapxprops);
int stop_dev_system(char *devsid, int stop_dev_time, char *stopsap_dev_script);

/* Fault monitor functions */
int svc_fm_start(scds_handle_t scds_handle);
int svc_fm_stop(scds_handle_t scds_handle);
int svc_fm_check(scds_handle_t scds_handle, sap_extprops_t *sapxprops);

/* Probe related functions */
int svc_probe(scds_handle_t scds_handle, sap_extprops_t *sapxprops,
	int probe_to, int *ms_pid, int *disp_pid);
int probe_ms(scds_handle_t handle, sap_extprops_t *sapxprops,
	int probe_time, int *probe_rc);
int probe_proc(int *probe_rc, const char *rtbase, char *sapsid, char *service,
	char *sysnum, const char *rname, int *ms_pid, int *disp_pid);
int svc_wait(scds_handle_t scds_handle, sap_extprops_t *sapxprops);

/* No-op functions */
int svc_postnet_stop(scds_handle_t scds_handle, sap_extprops_t *sapxprops);
int svc_prenet_start(scds_handle_t scds_handle, sap_extprops_t *sapxprops);
int svc_boot(scds_handle_t scds_handle, sap_extprops_t *sapxprops);
int svc_init(scds_handle_t scds_handle, sap_extprops_t *sapxprops);
int svc_fini(scds_handle_t scds_handle, sap_extprops_t *sapxprops);

#ifdef	__cplusplus
}
#endif

#endif /* _SAP_CI_H */
