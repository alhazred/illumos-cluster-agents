/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)sap_ci_svc_start.c	1.12	07/06/06 SMI"

#include "sap_ci.h"

int
main(int argc, char *argv[])
{
	scds_handle_t   	scds_handle;
	sap_extprops_t		sapxprops;
	int 			rc;
	int			stop_dev_time = 0;


	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR)
		exit(1);

	if (sap_get_extensions(scds_handle, &sapxprops, B_FALSE) != SAP_OK) {
		scds_close(&scds_handle);
		exit(1);
	}

	/* make sure everything looks ok */
	if (svc_validate(scds_handle, &sapxprops, B_FALSE) != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
			"Failed to validate configuration.");
		exit(1);
	}

	/*
	 * stop dev system if user configured a dev system under hatimerun
	 * for maximum a percentage of the starttimeout. If hatimerun times
	 * out, the stop dev process will run asynchronously and go ahead
	 * with the starting up of the Central instance.
	 */

	if (sapxprops.se_shutdowndev) {
		stop_dev_time = sapxprops.se_stopdevpct *
				scds_get_rs_start_timeout(scds_handle)/100;
		if ((rc = stop_dev_system(sapxprops.se_devsid,
			stop_dev_time, sapxprops.se_stopsap_dev)) != 0) {
			/*
			 * SCMSGS
			 * @explanation
			 * Stopping the development system failed.
			 * @user_action
			 * Informational message. Check previous messages in
			 * the system log for more details regarding why it
			 * failed.
			 */
			scds_syslog(LOG_ERR,
				"Failed to stop development system.");
		}

	}

	/* start SAP */
	rc = svc_start(scds_handle, sapxprops.se_sapsid,
			sapxprops.se_services, sapxprops.se_sapsysnum);
	if (rc != 0) {
		scds_close(&scds_handle);
		return (rc);
	}
	scds_syslog_debug(DBG_LEVEL_HIGH,
		"Done with svc_start, calling svc_wait.");

	/*
	 * wait until sap is actually up and running which indicated by
	 * its ability to probe
	 */
	rc = svc_wait(scds_handle, &sapxprops);

	scds_syslog_debug(DBG_LEVEL_HIGH,
		"Returning from svc_wait.");

	if (rc == 0) {
		scds_syslog(LOG_INFO, "Successfully started the service.");
	} else {
		scds_syslog(LOG_ERR, "Failed to start service.");
	}

	/* Free up the Environment resources that were allocated */
	scds_close(&scds_handle);

	return (rc);
}
