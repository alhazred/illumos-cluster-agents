#! /bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2001 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#


#pragma ident	"@(#)sap_get_disp_pid.sh	1.6	07/06/06 SMI"
#
# Usage: sap_ci_get_disp_pid SAPSID CI_SERVICES_STRING CI_INSTANCE_ID
#
#
# Return:
#	PID of the dispatcher or 1 if the dispatcher is not found
#
# Main

if [ -z "${1}" -o -z "${2}" -o -z "${3}" ]; then
	# didn't get all input parameter exit
	echo 1
	exit 0
fi

SAPSID="${1}"
CI_SERVICES_STRING="${2}"
CI_INSTANCE_ID="${3}"

DISP_PROC="dw.sap${SAPSID}_${CI_SERVICES_STRING}${CI_INSTANCE_ID}"

# Get a list of the dialog work processes including the dispatcher
PIDLIST=`pgrep -f "${DISP_PROC}"`

# Form a list of the parent PIDs for the dw processes in the PIDLIST.
# This list will contain the dispatcher PID and the
# PID of the sapstart process (the parent of the dispatcher).
PPIDS=
for pid in $PIDLIST
	do
		# The output of ps includes a header.
		# The egrep command removes any lines with letters.
		ps_out=`ps -p $pid -o ppid | egrep -v '[a-zA-Z]'`

		# Add the new parent PID to the list so far
		PPIDS="${ps_out} ${PPIDS}"
	done

# PPIDS now has the parent PIDs of all the dialog work processes
# (which are all the dispatcher PID) and the parent PID of the
# dispatcher (which is the sapstart PID).
# Given this list, we just need to find the PID that matches
# DISP_PROC, and this will be the dispatcher PID.
# When we find this PID, we return immediately.
for pid in $PPIDS
	do
		ps -p $pid -o comm= | grep "${DISP_PROC}" >/dev/null 2>&1
		if [ $? -eq 0 ]; then
			echo $pid
			exit 0
		fi
	done
	echo 1
