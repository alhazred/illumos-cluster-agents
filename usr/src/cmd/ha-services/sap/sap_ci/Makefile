#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
#pragma ident	"@(#)Makefile	1.25	07/06/06 SMI"
#
# cmd/ha-services/sap/Makefile
#

SRCS =	sap_ci_svc_start.c \
	sap_ci_svc_stop.c \
	sap_ci_startR3.c \
	sap_ci_validate.c \
	sap_ci_update.c \
	sap_ci_monitor_start.c \
	sap_ci_monitor_stop.c \
	sap_ci_monitor_check.c \
	sap_ci_probe.c \
	sap_ci_boot.c

CPROG = $(SRCS:%.c=%)

SHSCRIPTS = sap_get_disp_pid.sh
SHPROG = $(SHSCRIPTS:%.sh=%)

PROG = $(CPROG) $(SHPROG)

OBJS = $(SRCS:%.c=%.o)

COMMON_SRCS = 	sap_ci.c 
COMMON_OBJS = $(COMMON_SRCS:%.c=%.o)

UTIL_SRC = ../util/sap_util.c
UTIL_OBJ = ../util/sap_util.o

OBJECTS = $(OBJS) $(COMMON_OBJS) $(UTIL_OBJ)

include ../../../Makefile.cmd

# Packaging
PKGNAME = SUNWscsap/sap_ci
RTRFILE = SUNW.sap_ci \
	  SUNW.sap_ci_v2

TEXT_DOMAIN = SUNW_SC_SAPCI
MESSAGE_DIR = /opt/$(PKGNAME)/lib/locale
PIFILES = $(SRCS:%.c=%.pi) $(COMMON_SRCS:%.c=%.pi) $(UTIL_SRC:%.c=%.pi)
POFILE = sapci-ds.po

LDLIBS	+= -lc -lnsl -lelf -lsocket -ldsdev -lscha -lgen
LDLIBS	+= $(CL_REF_PROTO)/usr/cluster/lib/libsecurity.so.1
LDLIBS	+= $(CL_REF_PROTO)/usr/cluster/lib/libclos.so.1 -lresolv

LINTFILES= $(SRCS:%.c=%.ln) $(COMMON_SRCS:%.c=%.ln) $(UTIL_SRC:%.c=%.ln)

.KEEP_STATE:

all: $(UTIL_OBJ) $(COMMON_OBJS) $(PROG) $(ROOTOPTETCRTR)

.PARALLEL: $(PROG)

install: all $(ROOTOPTBINPROG)

include ../../../Makefile.targ

$(UTIL_OBJ):	$(UTIL_SRC)
	$(COMPILE.c) -o $(UTIL_OBJ) -c $(UTIL_SRC)
	$(POST_PROCESS_O)

$(SRCS:%.c=%): $(UTIL_OBJ) $(COMMON_OBJS) $$(@:%=%.o)
	$(LINK.c) -o $@ $(@:%=%.c) \
		$(UTIL_OBJ) $(COMMON_OBJS) $(LDFLAGS) $(LDLIBS)
	$(POST_PROCESS)

$(SHSCRIPTS:%.sh=%): $(SHSCRIPTS)
	cp  $(@:%=%.sh) $@
	chmod +x $@

clean:
	$(RM) $(PROG) $(OBJS) $(COMMON_OBJS) $(UTIL_OBJ) 
