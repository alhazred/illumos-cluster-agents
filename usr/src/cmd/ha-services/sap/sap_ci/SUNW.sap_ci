#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2002 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#pragma ident	"@(#)SUNW.sap_ci	1.18	07/06/06 SMI"

# Registration information and Paramtable for SAP
#
# NOTE: Keywords are case insensitive, i.e. users may use any
# capitalization style they wish
# 

RESOURCE_TYPE = "sap_ci";
VENDOR_ID = SUNW;
RT_DESCRIPTION = "SAP R/3 Central Instance on Sun Cluster";

RT_VERSION ="3.1"; 
API_VERSION = 2;	 


INIT_NODES = RG_PRIMARIES;

FAILOVER = TRUE;

RT_BASEDIR=/opt/SUNWscsap/sap_ci/bin;

START				=	sap_ci_svc_start;
STOP				=	sap_ci_svc_stop;

VALIDATE			=	sap_ci_validate;
UPDATE				=	sap_ci_update;

MONITOR_START			=	sap_ci_monitor_start;
MONITOR_STOP			=	sap_ci_monitor_stop;
MONITOR_CHECK			=	sap_ci_monitor_check;

BOOT				=	sap_ci_boot;

PKGLIST = SUNWscsap;

#
# Upgrade directives
#
#$upgrade
#$upgrade_from "1.0" anytime

# The paramtable is a list of bracketed resource property declarations 
# that come after the resource-type declarations
# The property-name declaration must be the first attribute
# after the open curly of a paramtable entry
#

# SAP may have to wait for the database, so have a larger default
{  
	PROPERTY = Start_timeout; 
	DEFAULT=600;
}
{
	PROPERTY = Stop_timeout; 
	DEFAULT=300;
}
{ 
	PROPERTY = Validate_timeout; 
	DEFAULT=300; 
}
{ 
	PROPERTY = Update_timeout; 
	DEFAULT=300; 
}
{ 
	PROPERTY = Monitor_Start_timeout; 
	DEFAULT=300; 
}
{ 
	PROPERTY = Monitor_Stop_timeout; 
	DEFAULT=300; 
}
{ 
	PROPERTY = Monitor_Check_timeout; 
	DEFAULT=300; 
}
{ 
	PROPERTY = Boot_timeout; 
	DEFAULT=60; 
}
{ 
	PROPERTY = Thorough_Probe_Interval; 
	DEFAULT=60; 
	TUNABLE = ANYTIME;
}

##########################################################
# will local restart twice. If it fails the third time,
# a failover is triggered.
{ 
	PROPERTY = Retry_Count; 
	DEFAULT=3; 
	TUNABLE = ANYTIME;
}

{ 
	PROPERTY = Retry_Interval; 
	DEFAULT=3600; 
	TUNABLE = ANYTIME;
}
{ 
	PROPERTY = FailOver_Mode;
	DEFAULT = SOFT; 
	TUNABLE = ANYTIME;
}

#########################################################################
# Extension Properties
#

# Not to be edited by end user
{
	PROPERTY = ParamtableVersion;
	EXTENSION;
	STRING;
	DEFAULT = "1.0";
	DESCRIPTION = "The Paramtable Version for this Resource";
}

#########################################################################
# Extension Properties that describe the SAP configuration
#
{

	PROPERTY = SAPSID;
	EXTENSION;
	STRING;
	TUNABLE = WHEN_DISABLED;
	DESCRIPTION = "The SAP System Name or <SAPSID>";
}

{
	PROPERTY = Ci_instance_id;
	EXTENSION;
	STRING;
	DEFAULT = "00";
	TUNABLE = WHEN_DISABLED;
	DESCRIPTION = "The two-digit SAP System Number or Instance ID";
}

{
	PROPERTY = Ci_services_string;
	EXTENSION;
	STRING;
	DEFAULT = "DVEBMGS";
	TUNABLE = WHEN_DISABLED;
	DESCRIPTION = "String of CI services";
}


{
        PROPERTY = Message_server_name;
        EXTENSION;
        STRING;
        DEFAULT = "";
        TUNABLE = WHEN_DISABLED;
        DESCRIPTION = "Message server name (default to sapms<SAPSID>";
}

###########################################################################
# General Probing Extension Properties
#
{
	PROPERTY = Probe_timeout;
	EXTENSION;
	INT;
	DEFAULT = 120;
	TUNABLE = ANYTIME;
	DESCRIPTION = "Time out value for the probe (in seconds)";
}

# These two control the restarting of the fault monitor itself
{
	PROPERTY = Monitor_retry_count;
	EXTENSION;
	INT;
	DEFAULT = 4;
	TUNABLE = ANYTIME;
	DESCRIPTION = "Number of PMF restarts allowed for the fault monitor";
}

{
	PROPERTY = Monitor_retry_interval;
	EXTENSION;
	INT;
	DEFAULT = 2;
	TUNABLE = ANYTIME;
	DESCRIPTION = "Time window (in minutes) for fault monitor restarts";
}

##########################################################################
# SAP specific probe extension property
#
{
        PROPERTY = Check_ms_retry;
        EXTENSION;
        INT;
        DEFAULT = 2;
        TUNABLE = ANYTIME;
        DESCRIPTION = "Maximum number of times SAP Message Server check fails before reporting a total failure and triggers a restart";
}

###########################################################################
# Extension Properties that affect the start up of SAP
{
	PROPERTY = Ci_start_retry_interval;
	EXTENSION;
	INT;
	DEFAULT = 30;
	TUNABLE = WHEN_DISABLED;
	DESCRIPTION = "The interval in seconds to wait between attempts to connect to the database before starting the Central Instance.";
}

{ 
	PROPERTY = Ci_startup_script;
        EXTENSION;
        STRING;
	DEFAULT = "";
        TUNABLE = WHEN_DISABLED;
        DESCRIPTION = "Startup script under <sapsid>adm home directory (eg. startsap_hostname_00)";
}
###########################################################################
# Extension Properties that affect the stopping of SAP

{
	PROPERTY = Stop_sap_pct;
	EXTENSION;
        INT;
        DEFAULT = 95;
	TUNABLE = WHEN_DISABLED;
        DESCRIPTION = "Percentage of Stop_timeout used to stop SAP processes using SAP script before calls PMF to terminate the processes";
}

{ 
        PROPERTY = Ci_shutdown_script;
        EXTENSION;
        STRING;
	DEFAULT = "";
        TUNABLE = WHEN_DISABLED;
        DESCRIPTION = "Shutdown script under <sapsid>adm home directory (eg. stopsap_hostname_00)";
}

###########################################################################
# SAP Probing Extension Properties
#
{
	PROPERTY = Lgtst_ms_with_logicalhostname;
	EXTENSION;
	BOOLEAN;
	DEFAULT = TRUE;
	TUNABLE = ANYTIME;
	DESCRIPTION = "Probe Message Server with lgtst <logicalhost> during a normal probing cycle if set to TRUE otherwise use localhost";
}

###############################################
# For stop/start development system
#
{
	PROPERTY = Shutdown_dev;
	EXTENSION;
        BOOLEAN;
        DEFAULT = FALSE;
        TUNABLE = WHEN_DISABLED;
	DESCRIPTION = "Whether HA-SAP should shut down the development system before starting up the Central Instance.";
}

{
	PROPERTY = Dev_sapsid;
	EXTENSION;
	STRING;
	DEFAULT = "";
	TUNABLE = WHEN_DISABLED;
        DESCRIPTION = "Development system name or <SAPSID>";
}



{
        PROPERTY = Dev_shutdown_script; 
        EXTENSION;     
        STRING;
        DEFAULT = "";  
        TUNABLE = WHEN_DISABLED;       
        DESCRIPTION = "Development system shutdown script name";
}


{
        PROPERTY = Dev_stop_pct; 
        EXTENSION;     
	INT;
	DEFAULT = 20;
        TUNABLE = WHEN_DISABLED;       
        DESCRIPTION = "Percentage of Start timeout HA-SAP uses to shut down the development system before starting the Central Instance.";
}
	
