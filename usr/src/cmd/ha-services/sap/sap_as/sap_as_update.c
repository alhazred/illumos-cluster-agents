/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * sap_as_update.c - Update method for highly available SAP
 * R/3 Application Server
 */

#pragma ident	"@(#)sap_as_update.c	1.11	07/06/06 SMI"

#include "sap_as.h"

int
main(int argc, char *argv[])
{
	scds_handle_t		scds_handle;
	sap_extprops_t		sapxprops;
	int result;

	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR)
		exit(1);

	result = scds_pmf_restart_fm(scds_handle, 0);
	if (result != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
			"Failed to restart fault monitor.");
		scds_close(&scds_handle);
		return (1);
	}

	scds_syslog(LOG_INFO,
		"Completed successfully.");

	scds_close(&scds_handle);
	return (0);
}
