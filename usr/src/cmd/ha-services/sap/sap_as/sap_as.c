/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)sap_as.c	1.29	07/06/06 SMI"

#include "sap_as.h"

#define	SVC_WAIT_SLEEP_INTERVAL 	5

/*
 * Do SAP specific validation of a given resource configuration
 * Called by start/validate/update/monitor methods.
 * Return 0 on success, > 0 on failures.
 *
 * svc_validate checks the following:
 * - check SAP extension properties are set correctly:
 * 	SID for CI, AS system num, AS service string.
 * - check the start/stop are there and are executable
 * - check SAP programs sapstart, R3trans, cleanipc exist and are executable
 * - check that extension property x_pmf_arg_n is set
 * - check that extension property x_pmf_arg_t is set
 * - check that extension property x_probe_timeout set
 */

int
svc_validate(scds_handle_t handle, sap_extprops_t *sapxprops,
		boolean_t print_messages)
{

	int 		rc;
	boolean_t	script = B_TRUE;
	scds_hasp_status_t	hasp_status;

	if (sapxprops->se_sapsid == NULL) {
		scds_syslog(LOG_ERR,
			"Property %s is not set.", SAP_EXT_SAPSID);
		if (print_messages) {
			(void) fprintf(stderr, gettext("Property %s is not "
				"set.\n"), SAP_EXT_SAPSID);
		}
		return (1);
		}

	if (sapxprops->se_asinstancenum == NULL) {
		scds_syslog(LOG_ERR,
			"Property %s is not set.", SAP_EXT_SAPSYSNUM_AS);
		if (print_messages) {
			(void) fprintf(stderr, gettext("Property %s is not "
				"set.\n"), SAP_EXT_SAPSYSNUM_AS);
		}
		return (1);
	}

	rc = scds_hasp_check(handle, &hasp_status);
	if (rc != SCHA_ERR_NOERR) {
		/* scha_hasp_check() logs everytime it fails */
		if (print_messages) {
			(void) fprintf(stderr, gettext("INTERNAL ERROR: %s.\n"),
				gettext("scds_hasp_check failed"));
		}
		return (1);
	}

	/* hasp resources must be online (somewhere) and in our RG */
	if (hasp_status == SCDS_HASP_NOT_ONLINE) {
		scds_syslog(LOG_ERR, "Resource depends on a "
			"SUNW.HAStoragePlus type resource that is "
			"not online anywhere.");
		if (print_messages) {
			(void) fprintf(stderr, gettext("Resource depends on a "
				"SUNW.HAStoragePlus type resource that is "
				"not online anywhere.\n"));
		}
		return (1);
	} else if (hasp_status == SCDS_HASP_ERR_CONFIG) {
		/* problem syslogged by scds_hasp_check */
		if (print_messages) {
			(void) fprintf(stderr, gettext("This resource depends "
				"on a HAStoragePlus resouce that is in a "
				"different Resource Group. This configuration "
				"is not supported.\n"));
		}
		return (1);
	}

	/* check SAP scripts are there */
	rc = check_sap_file(script, sapxprops->se_sapsid,
		sapxprops->se_as_start, hasp_status, print_messages);
	if (rc != 0)
		return (1);

	rc = check_sap_file(script, sapxprops->se_sapsid,
		sapxprops->se_as_stop, hasp_status, print_messages);
	if (rc != 0)
		return (1);

	/* Check SAP binary programs are there and executable */
	script = B_FALSE;

	/* SAP startup program */
	rc = check_sap_file(script, sapxprops->se_sapsid,
		SAP_PRG_SAPOSCOL, hasp_status, print_messages);
	if (rc != 0)
		return (1);

	/*
	 * only need to check if it's either SCDS_HASP_NO_RESOURCE or
	 * SCDS_HASP_ONLINE_LOCAL which is !SCDS_HASP_ONLINE_NOT_LOCAL
	 */
	if (hasp_status != SCDS_HASP_ONLINE_NOT_LOCAL) {
		/* Make sure saposcol is the right one */
		if ((rc = correct_saposcol(sapxprops->se_sapsid,
						print_messages)) != 0) {
			return (1);
		}
	}

	rc = check_sap_file(script, sapxprops->se_sapsid,
		SAP_PRG_STARTSAP, hasp_status, print_messages);
	if (rc != 0)
		return (1);

	/* SAP transport program - used to check connection to Message Server */
	rc = check_sap_file(script, sapxprops->se_sapsid,
		SAP_PRG_R3TRANS, hasp_status, print_messages);
	if (rc != 0)
		return (1);

	/* SAP cleanup ipc program */
	rc = check_sap_file(script, sapxprops->se_sapsid,
		SAP_CMD_CLEANIPC, hasp_status, print_messages);
	if (rc != 0)
		return (1);

	/* check monitor retry count */
	if (scds_get_ext_monitor_retry_count(handle) <= 0) {
		scds_syslog(LOG_ERR, "Property %s is not set.",
			"Monitor_retry_count");
		if (print_messages) {
			(void) fprintf(stderr, gettext("Property %s is not "
				"set.\n"), "Monitor_retry_count");
		}
		return (1);
	}
	if (scds_get_ext_monitor_retry_interval(handle) <= 0) {
		scds_syslog(LOG_ERR, "Property %s is not set.",
			"Monitor_retry_interval");
		if (print_messages) {
			(void) fprintf(stderr, gettext("Property %s is not "
				"set.\n"), "Monitor_retry_interval");
		}
		return (1);
	}

	if (scds_get_ext_probe_timeout(handle) <= 0) {
		scds_syslog(LOG_ERR,
		    "Property %s is not set.", SAP_EXT_PROBE_TIMEOUT);
		if (print_messages) {
			(void) fprintf(stderr, gettext("Property %s is not "
				"set.\n"), SAP_EXT_PROBE_TIMEOUT);
		}
		return (1);
	}

	return (0);
}


/*
 * Start the SAP application server processes under pmf.
 *
 * The first thing to check is whether sap is already up and running. If sap
 * is not up, then go ahead with the starting up. Otherwise, return 1, so
 * RGM knows start method failed. Currently, there is no
 * way of "return-early" from the data service start method: set the resource
 * state to "start-failed", so that RGM will not keep trying to restart and
 * failover the resource. So until that RFE is done, checking upfront will
 * sap is up or not, will put the resource in "start-failed" state in a very
 * short time, instead of taking a could be very long time, eg.
 * start-timeout * retry_count.
 *
 * SAP processes are divided into two groups. The first group contains the
 * saposcol process. It will be started under PMF along with a tag as
 * saposcol.svc, in this subroutine. If the saposcol is already started up
 * (could be by the central instance, if this is an internal app server),
 * then this routine will not start it up.
 *
 * The other group contains the main dispatcher and all other processes
 * that's created by the main dispatcher. This group of processes
 * will be started under PMF with a tag index 0, in another subroutine
 * sap_as_startR3(). The reason for not starting up sap in this routine is
 * before we can start up the second group of sap processes, we need to make
 * sure the DB is already up and running. And if the attemp to start up
 * sap failed again in start method, we need to pull on DB again, and we
 * won't be able to do that if we start sap under PMF in this subroutine.
 *
 * This routine returns 0 upon success, otherwise, return 1.
 */

int
svc_start(scds_handle_t handle, char *sapsid, char *sapservice, char *sapsysnum)
{
	int			rc;
	scha_err_t		pmf_rc;
	char			sapstart[SCDS_ARRAY_SIZE];
	char 			internal_err_str[INT_ERR_MSG_SIZE];
	char			lockfile[MAXPATHLEN];

	scds_syslog_debug(DBG_LEVEL_HIGH, "Calling START method.");

	/* check sap is up or not */
	rc = snprintf(lockfile, MAXPATHLEN, "%s/%s/%s%s/%s/%s",
		SAP_DIR_USRSAP,
		sapsid,
		sapservice,
		sapsysnum,
		SAP_DIR_WORK,
		SAP_LOCK_FILE);
	if (rc == -1) {
		(void) sprintf(internal_err_str,
			"Failed to form sap lock file path");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		return (1);
	}

	rc = is_sap_running(lockfile, scds_get_resource_name(handle));

	switch (rc) {
		case 1:
			/*
			 * SCMSGS
			 * @explanation
			 * SAP is already running either locally on this node
			 * or remotely on a different node in the cluster
			 * outside of the control of the Sun Cluster.
			 * @user_action
			 * Need to shut down SAP first, before start up SAP
			 * under the control of Sun Cluster.
			 */
			scds_syslog(LOG_ERR, "SAP is already running.");
			return (1);
		case 0:
			/* sap is not up yet */
			break;
		case -1:
			scds_syslog(LOG_ERR, "Failed to start %s.", "SAP");
			return (1);
		default:
			break;
	}

	/* otherwise, sap was not up, so continue with the starting up */
	scds_syslog(LOG_NOTICE, "Starting %s.", "SAP OS Collector");
	/* startup saposcol process under pmf tag 'saposcol.svc'. */
	rc = start_saposcol(handle, sapsid);
	if (rc != 0) {
		/* error when try to bring up saposcol, return 1 */
		return (1);
	}

	/*
	 * now start the other group of sap processes by calling
	 * subroutine sap_ci_startR3().
	 */

	/* form path to start SAP start up command */
	rc = snprintf(sapstart, sizeof (sapstart), "%s/%s -R %s -T %s -G %s",
	    scds_get_rt_rt_basedir(handle),
	    SAP_CMD_STARTR3,
	    scds_get_resource_name(handle),
	    scds_get_resource_type_name(handle),
	    scds_get_resource_group_name(handle));

	if (rc == -1) {
		(void) snprintf(internal_err_str, sizeof (internal_err_str),
			"Failed to form start command %s", sapstart);
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		return (1);
	}

	scds_syslog(LOG_NOTICE, "Starting %s with command %s.", "service",
	    sapstart);

	/*
	 * Do not check whether this sap is already running locally or
	 * remotely since it won't get here  if sap is already started
	 * either locally or remotely.
	 */
	/* start the startup program under pmf */
	pmf_rc = scds_pmf_start(handle, SCDS_PMF_TYPE_SVC,
		SCDS_PMF_SINGLE_INSTANCE, sapstart, -1);
	if (pmf_rc == SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * Need explanation of this message!
		 * @user_action
		 * Need a user action for this message.
		 */
		scds_syslog(LOG_INFO,
			"Started sap processes under PMF successfully.");
	} else {
		/*
		 * SCMSGS
		 * @explanation
		 * Failed to start up SAP with the specified command.
		 * @user_action
		 * SAP Application Server failed to start on this cluster node.
		 * It would be started on some other cluster node provided
		 * there is another cluster node available. If the Application
		 * Server failed to start on any other node, disable the SAP
		 * Application Server resource, then try to run the same command
		 * manually, and fix any problem found. Save the
		 * /var/adm/messages from all nodes. Contact your authorized
		 * Sun service provider.
		 */
		scds_syslog(LOG_ERR,
		    "Failed to start sap processes with command %s.", sapstart);
	}

	/* return success/failure status */
	return (pmf_rc);
}


int
svc_wait(scds_handle_t handle, sap_extprops_t *sapxprops)
{
	float   probe_result;
	int	probe_timeout;
	int	disp_pid = -1;
	int	rc = 0;

	/* Get Probe timeout value */
	probe_timeout = scds_get_ext_probe_timeout(handle);

	/*
	 * Instead of calling sleep(), call scds_svc_wait() so that if
	 * service fails too many times (for case that sap is already up and
	 * running on a local/remote node), we give up and return early.
	 */

	/* first retrieve the pid for main dispatcher and write it to file. */
	do { /* will keep looping until get it or timeout eventually */
		rc = retrieve_disp_pid(scds_get_rt_rt_basedir(handle),
			sapxprops->se_sapsid,
			sapxprops->se_assvcstring,
			sapxprops->se_asinstancenum,
			scds_get_resource_name(handle));
		if (rc == 0)
			break;

		scds_syslog(LOG_NOTICE, "Waiting for %s to come up.",
		    "dispatcher");

		/* wait for 3 seconds if pid could not be retrieved */
		if (scds_svc_wait(handle, 3) != SCHA_ERR_NOERR) {
			scds_syslog(LOG_ERR, "Failed to start service.");
		}
	} while (1);

	/* then read the dispatcher pid to pass to svc_probe() */
	rc = read_in_disp_pid(&disp_pid, scds_get_resource_name(handle));

	/* probe the data service SAP with the pid for dispatcher */
	do {
		probe_result = svc_probe(handle, sapxprops, probe_timeout,
				&disp_pid);

		if (probe_result == SCHA_ERR_NOERR) {
			/*
			 * If we get here, the application is up,
			 * and we return success
			 */
			scds_syslog(LOG_NOTICE, "Service is online.");
			return (0);
		}

		scds_syslog(LOG_NOTICE, "Waiting for %s to come up.",
		    "service");
		/*
		 * Dataservice is still trying to come up. Sleep for a while
		 * before probing again. Instead of calling sleep(),
		 * call scds_svc_wait() so that if service fails too
		 * many times, we give up and return early.
		 */
		if (scds_svc_wait(handle, SVC_WAIT_SLEEP_INTERVAL)
			!= SCHA_ERR_NOERR) {
			scds_syslog(LOG_ERR, "Failed to start service.");
		}

		/*
		 * the probe was not successful, we will continue to probe the
		 * application if there's still time.
		 */
		/* We rely on RGM to timeout and terminate the program */

	} while (1);
}



/* Actually startup SAP R/3, but only after the DB is available */
int
sap_as_startR3(scds_handle_t handle, sap_extprops_t *sapxprops)
{
	int 		rc;
	int		probe_rc;
	int		err;
	int		retry_interval;
	char		startsapcmd[SCDS_ARRAY_SIZE];
	char		cleanipccmd[SCDS_ARRAY_SIZE];
	boolean_t	giveup = B_FALSE;
	char		*admuser;
	char		internal_err_str[INT_ERR_MSG_SIZE];

	/*
	 * get retry interval value in case DB connection is not working
	 * the first time.
	 */
	retry_interval = sapxprops->se_dbretryinterval;

	/* get SAP admin user */
	admuser = get_admuser_name(sapxprops->se_sapsid);

	/*
	 * Form command to start SAP
	 * 	su - <sid>adm -c <startup script> r3'
	 */
	rc = snprintf(startsapcmd, sizeof (startsapcmd),
		"/usr/bin/su - %s -c '%s r3'", admuser,
		sapxprops->se_as_start);

	if (rc == -1) {
		(void) sprintf(internal_err_str,
			"Failed to form SAP start command: "
			"Output error encounted");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		return (rc);
	}


	/*
	 * Form command to cleanipc's
	 * 	/usr/sap/<SAPSID>/SYS/exe/run/cleanipc <sapsysnum> remove
	 */
	rc = snprintf(cleanipccmd, sizeof (cleanipccmd),
	    "%s/%s/%s/%s %s %s'",
	    SAP_DIR_USRSAP,
	    sapxprops->se_sapsid,
	    SAP_DIR_SYS,
	    SAP_CMD_CLEANIPC,
	    sapxprops->se_asinstancenum,
	    SAP_CMD_CLEANIPC_REM);

	if (rc == -1) {
		(void) sprintf(internal_err_str,
			"Failed to form cleanipc command: "
			"Output error encounted");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		return (rc);
	}

	/*
	 * Big loop to try to start SAP.
	 */

	/*
	 * Want to start up AS as soon as the process died, regardless
	 * whether CI is up or not to save time.
	 */
	while (giveup == B_FALSE) {
		/*
		 * Can only start SAP if the database is up
		 */

		err = probe_db(handle, sapxprops->se_sapsid,
				PROBE_DB_TIMEOUT, &probe_rc);

		if (err != SAP_OK) {
			/* some internal error, exit */
			return (err);
		} else {
			if (probe_rc != SAP_PROBE_OK) {
				/*
				 * SCMSGS
				 * @explanation
				 * Database connection check failed indicating
				 * the database might be down. HA-SAP will not
				 * take any action, but will check the
				 * database connection again after the time
				 * specified.
				 * @user_action
				 * Make sure the database and the HA software
				 * for the database are functioning properly.
				 */
				scds_syslog(LOG_ERR,
				    "Database might be down, HA-SAP won't take "
				    "any action. Will check again in %d "
				    "seconds.", retry_interval);
				(void) sleep((unsigned int)retry_interval);
			} else { /* DB is up */
				giveup = B_TRUE;
			}
		}
	} /* while */


	/*
	 * Use the SAP-supplied cleanipc program
	 */
	scds_syslog_debug(DBG_LEVEL_HIGH,
		"Running cleanipc with command %s.", cleanipccmd);
	rc = system(cleanipccmd);
	rc = WEXITSTATUS((uint_t)rc);

	if (rc != 0) {
		/*
		 * SCMSGS
		 * @explanation
		 * The command to cleanipc failed to complete.
		 * @user_action
		 * This is an internal error. No user action needed. Save the
		 * /var/adm/messages from all nodes. Contact your authorized
		 * Sun service provider.
		 */
		scds_syslog(LOG_ERR, "Command %s failed to complete. "
			"HA-SAP will continue to start SAP.", rc);
	}

	/*
	 * We have cleaned up as much as possible. Now start SAP
	 */
	scds_syslog_debug(DBG_LEVEL_HIGH,
		    "Trying to start SAP with command %s.",
		    startsapcmd);

	rc = system(startsapcmd);

	if (rc == -1) {
		(void) sprintf(internal_err_str,
			"Cannot execute %s: %s", startsapcmd,
			strerror(errno));	/*lint !e746 */
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		return (rc);
	}
	if (WIFSIGNALED((uint_t)rc) || (WIFEXITED((uint_t)rc) &&
		WEXITSTATUS((uint_t)rc) != 0)) {
		(void) sprintf(internal_err_str, "%s failed to complete",
			startsapcmd);
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		return (1);
	}
	return (SCHA_ERR_NOERR);
}


/*
 * Stop SAP processes which started up under PMF with tag index 0. This
 * also include the OS collector process saposcol.
 * It first checks and see if sap was up and stop method should not shut
 * it down or not, by checking the state file. If state file is there, then
 * just exit(0). Otherwise, go on to shutdown sap processes. Then it'll
 * check and see if this is the last resource that uses saposcol, if yes,
 * it will stop saposcol process also. Otherwise, just decreases the lock
 * count for saposcol.
 */
int
svc_stop(scds_handle_t handle, sap_extprops_t *sapxprops)
{
	int 		rc, exit_code;
	char		cmd[SCDS_ARRAY_SIZE];
	char		*cleanipc_path = NULL;
	int		stopsap_to; /* timeouts */
	char    	internal_err_str[INT_ERR_MSG_SIZE];
	char		*admuser;


	/* check state file */
	rc = check_state_file(scds_get_resource_name(handle));
	if (rc == 0) { /* state file exit, no need to stop sap, just exit(0) */
		/*
		 * SCMSGS
		 * @explanation
		 * SAP was started up outside of the control of Sun cluster.
		 * It will not be shutdown automatically.
		 * @user_action
		 * Need to shut down SAP, before trying to start up SAP under
		 * the control of Sun Cluster.
		 */
		scds_syslog(LOG_NOTICE,
			"SAP was brought up outside of HA-SAP, "
			"HA-SAP will not shut it down.");
		return (0);
	} else if (rc > 0) { /* state file doesn't exit,continue to stop sap */
			scds_syslog_debug(DBG_LEVEL_HIGH,
				"State file doesn't exit, go ahead with stop.");
		} else { /* internal error from check_state_file */
			(void) sprintf(internal_err_str,
				"Failed to check state file");
			scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
				internal_err_str);
			return (1);
		}

	/* Now stop the processes */
	scds_syslog(LOG_NOTICE, "Stopping %s.", "service");

	/* get admuser */
	admuser = get_admuser_name(sapxprops->se_sapsid);

	/*
	 * Allocate STOP_SAP_PCT * stop_timeout to stop all SAP processes
	 * using SAP script. It that doesn't work, call PMF to kill the
	 * processes.
	 */
	stopsap_to = (scds_get_rs_stop_timeout(handle) *
			sapxprops->se_stopsappct) / 100;

	/* tell pmf to stop monitoring the instance (excluding saposcol) */
	rc = scds_pmf_stop_monitoring(handle, SCDS_PMF_TYPE_SVC,
		SCDS_PMF_SINGLE_INSTANCE);
	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
			"Failed to stop fault monitor.");
		free(admuser);
		return (1);
	}

	/*
	 * form the command to stop SAP gracefully:
	 * 	su - <sid>adm -c <stopsap script> r3'
	 */
	rc = snprintf(cmd, sizeof (cmd), "/usr/bin/su - %s -c '%s r3'",
		admuser, sapxprops->se_as_stop);

	if (rc == -1) {
		(void) sprintf(internal_err_str,
			"Failed to form stop command");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		return (1);
	}

	scds_syslog_debug(DBG_LEVEL_HIGH, "Stopping SAP with command %s.", cmd);

	/*
	 * scds_timerun() will run the cmd with hatimerun
	 * will ignore the exit_code since stopsap script will exit 7 if
	 * if couldn't find the kill.sap file (which was created after
	 * sap is brought on-line). And for case like sap was never brought
	 * on-line, kill.sap file won't be found. And we still want to just
	 * exit 0 from the stop method.
	 * For case that sap was indeed brought on-line, and had problem
	 * stopping it via stopsap script, well, we'll call PMF to kill
	 * all the processes later anyway. So still want to exit 0 for
	 * stop method.
	 */
	rc = scds_timerun(handle, cmd, stopsap_to, SIGTERM, &exit_code);

	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
			"Failed to stop the process with: %s. "
			"Retry with SIGKILL.", cmd);
	}

	/*
	 * call PMF to terminate processes that might be still running
	 * with SIGKILL. If they are not running anymore, scds_pmf_stop()
	 * would do no harm.
	 */
	rc = scds_pmf_stop(handle, SCDS_PMF_TYPE_SVC,
		SCDS_PMF_SINGLE_INSTANCE, SIGKILL, -1);

	if (rc != SCHA_ERR_NOERR) {
		/*
		 * SCMSGS
		 * @explanation
		 * Failed to stop SAP processes with Process Monitor
		 * Facility(PMF) with signal.
		 * @user_action
		 * This is an internal error. No user action needed. Save the
		 * /var/adm/messages from all nodes. Contact your authorized
		 * Sun service provider.
		 */
		scds_syslog(LOG_ERR,
			"Failed to stop SAP processes under PMF with SIGKILL.");
	}

	/*
	 * If SAP processes aren't dead, cleaning the ipcs would abort them.
	 * Form command to cleanipc's
	 * 	/usr/sap/<SAPSID>/SYS/exe/run/cleanipc <sapsysnum> remove
	 */

	cleanipc_path = get_bin_path(sapxprops->se_sapsid, B_FALSE);

	if (cleanipc_path == NULL) {
		/*
		 * SCMSGS
		 * @explanation
		 * Cannot retrieve the path to SAP binaries.
		 * @user_action
		 * This is an internal error. There may be prior messages in
		 * syslog indicating specific problems. Make sure that the
		 * system has enough memory and swap space available. Save the
		 * /var/adm/messages from all nodes. Contact your authorized
		 * Sun service provider.
		 */
		scds_syslog(LOG_ERR, "Failed to retrieve SAP binary path.");
		return (1);
	}

	rc = snprintf(cmd, sizeof (cmd),
		"%s/%s %s %s",
		cleanipc_path,
		SAP_CMD_CLEANIPC,
		sapxprops->se_asinstancenum,
		SAP_CMD_CLEANIPC_REM);

	if (rc == -1) {
		(void) sprintf(internal_err_str,
			"Failed to form cleanipc command");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		if (cleanipc_path != NULL) free(cleanipc_path);
		return (1);
	}

	scds_syslog_debug(DBG_LEVEL_HIGH, "Running cleanipc with command %s.",
	    cmd);
	/*
	 * SCMSGS
	 * @explanation
	 * Sun Cluster is cleaning up the IPC facilities used by the
	 * application.
	 * @user_action
	 * This is an informational message, no user action is needed.
	 */
	scds_syslog(LOG_NOTICE, "Cleaning up IPC facilities.");
	rc = system(cmd);

	if (rc != 0) {
		scds_syslog(LOG_ERR,
			"Cannot execute %s: %s.", cmd, strerror(errno));
		if (cleanipc_path != NULL) free(cleanipc_path);
		return (1);
	}

	if (WIFSIGNALED((uint_t)rc) || (WIFEXITED((uint_t)rc) &&
		(rc = WEXITSTATUS((uint_t)rc)) != 0)) {
	    (void) sprintf(internal_err_str, "%s failed to complete", cmd);
	    scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
	    if (cleanipc_path != NULL) free(cleanipc_path);
	    return (1);
	}

	/* stop saposcol */
	scds_syslog(LOG_NOTICE, "Stopping %s.", "SAP OS Collector");
	if (stop_saposcol(handle, sapxprops->se_sapsid) != 0) {
		scds_syslog(LOG_ERR, "Failed to stop %s.", "saposcol");
		if (cleanipc_path != NULL) free(cleanipc_path);
		return (1);
	}

	scds_syslog(LOG_NOTICE, "Successfully stopped %s.", "resource");

	if (cleanipc_path != NULL)
		free(cleanipc_path);
	return (0);
}


/* Start the fault monitor under PMF */
int
svc_fm_start(scds_handle_t handle)
{


	if (scds_pmf_start(handle,
	    SCDS_PMF_TYPE_MON,
	    SCDS_PMF_SINGLE_INSTANCE,
	    "sap_as_probe", 0) != SCHA_ERR_NOERR) {
	    scds_syslog(LOG_ERR, "Failed to start fault monitor.");
	    return (1);
	}
	scds_syslog(LOG_INFO, "Started the fault monitor.");
	return (SCHA_ERR_NOERR);
}



/* Stop the fault monitor */
int
svc_fm_stop(scds_handle_t handle)
{
	if (scds_pmf_stop(handle, SCDS_PMF_TYPE_MON, 0, SIGKILL, -1) !=
	    SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to stop fault monitor.");
		return (1);
	}

	scds_syslog(LOG_INFO, "Stopped the fault monitor.");

	return (SCHA_ERR_NOERR);
}


/*
 * Check if the host where this program is executing is healthy.
 * Currently, we just call validate and return success if validate passes
 */
int
svc_fm_check(scds_handle_t handle, sap_extprops_t *sapxprops)
{

	if (svc_validate(handle, sapxprops, B_FALSE) != SCHA_ERR_NOERR) {
		return (1);		/* Validation failure */
	}

	scds_syslog(LOG_INFO, "Completed successfully.");

	return (SCHA_ERR_NOERR);
}



/*
 * svc_probe(): Do data service specific probing.
 * It checks two things in the following sequence:
 * 1) main dispatcher processes
 * 2) connection to DB
 *
 * If any error (complete failure or partial failure) happens during those
 * three checks, this routine return with that error. If finishes successfully,
 * it returns SAP_PROBE_OK.
 */
int
svc_probe(scds_handle_t handle, sap_extprops_t *sapxprops, int probe_timeout,
	int *disp_pid)
{
	int		sap_rc = SAP_PROBE_OK;
	int		probe_rc = SAP_PROBE_ERROR;
	int		err = SAP_OK;


	/*
	 * Probe the SAP dispatcher processes.
	 */
	err = probe_proc(&probe_rc, scds_get_rt_rt_basedir(handle),
		sapxprops->se_sapsid,
		sapxprops->se_assvcstring,
		sapxprops->se_asinstancenum,
		scds_get_resource_name(handle), disp_pid);

	if (err != SAP_OK) {
		/*
		 * system problem, not probing error, so don't want to rush to
		 * judgement. Only set the error to a quarter. Because for a
		 * loaded system, setting to total failure might cause restart
		 * unnecessarily.
		 */
		scds_syslog_debug(LOG_ERR,
		    "An error occured while probing the "
		    "SAP Critical Processes.");
		sap_rc = SCDS_PROBE_COMPLETE_FAILURE/4;
		return (sap_rc);
	} else if (probe_rc != SAP_PROBE_OK) {
			scds_syslog_debug(DBG_LEVEL_LOW,
				"SAP Critical Processes are not "
				"running.");
			sap_rc = SCDS_PROBE_COMPLETE_FAILURE;
			return (sap_rc);
	} else {
		scds_syslog_debug(DBG_LEVEL_HIGH,
			"SAP Critical Processes are running.");
		}
	/*
	 * Doesn't have to probe the SAP message server since the
	 * AS reattach mechanism should reattach ASs to CI once CI is
	 * available again.
	 */
	/*
	 * Probe the SAP Database
	 */

	err = probe_db(handle, sapxprops->se_sapsid, probe_timeout, &probe_rc);

	if (err != SAP_OK) {
		/*
		 * system problem, not probing error, so don't want to rush to
		 * judgement. Only set the error to a quarter. Because for a
		 * loaded system, setting to total failure might cause restart
		 * unnecessarily.
		 */
		scds_syslog_debug(LOG_ERR,
			"An error occured while probing the SAP Database.");
		sap_rc = SCDS_PROBE_COMPLETE_FAILURE/4;
		return (sap_rc);
	}
	return (sap_rc);
}


/* Check for the SAP Critical Processes */
int
probe_proc(int *probe_rc, const char *rtbase, char *sapsid, char *service,
	char *sysnum, const char *rname, int *disp_pid)
{
	int	rc;
	char	internal_err_str[INT_ERR_MSG_SIZE];


	if (probe_rc == NULL) {
		(void) sprintf(internal_err_str,
			"No space was allocated for probe result");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		return (1);
	}

	*probe_rc = SAP_PROBE_OK;

	/*
	 * check the dispatcher process and see if it's still there.
	 * kill() will return 0 if successful, -1 if not. If not successful,
	 * errno is set to indicate the error: EINVAL, EPERM or ESRCH. Only
	 * ESRCH should trigger probe error. Treat other ones as non-sap
	 * related internal error, and shouldn't trigger probe error.
	 */
	rc = kill(*disp_pid, 0);
	if (rc != 0) {
		if (errno == ESRCH) {
			/*
			 * sap might be restarted, so the pid is no longer
			 * valid. Try to read them in again, and kill it
			 * again. If that fails, flag error.
			 */
			if ((rc = retrieve_disp_pid(rtbase, sapsid, service,
					sysnum, rname)) != 0) {
			    /* BEGIN CSTYLED */
			    /*
			     * CSTYLED is used to prevent cstyle
			     * complaints about block comments
			     * indented with spaces.
			     */
			    /*
			     * SCMSGS
			     * @explanation
			     * Failed to retrieve the process ID for the main
			     * dispatcher process indicating the main
			     * dispatcher process is not running.
			     * @user_action
			     * No action needed. The fault monitor will detect
			     * this and take appropriate action.
			     */
			    /* END CSTYLED */
			    scds_syslog(LOG_ERR,
				"Failed to retrieve main dispatcher pid.");
			    *probe_rc = SAP_PROBE_ERROR;
			    return (0);
			}
			(void) read_in_disp_pid(disp_pid, rname);

			rc = kill(*disp_pid, 0);
			if (rc != 0) {
			    if (errno == ESRCH) {
				/*
				 * SCMSGS
				 * @explanation
				 * The main dispatcher process is not present
				 * in the process list indicating the main
				 * dispatcher is not running on this node.
				 * @user_action
				 * No action needed. Fault monitor will detect
				 * that the main dispatcher process is not
				 * running, and take appropriate action.
				 */
				scds_syslog(LOG_ERR,
					"Dispatcher Process is not running."
					" pid was %d", *disp_pid);
				*probe_rc = SAP_PROBE_ERROR;
				return (SCHA_ERR_NOERR);
			    } else {
				(void) sprintf(internal_err_str,
				    "Failed to check main dispatcher process "
				    "status with kill(pid,0) command. pid was "
				    "%d", *disp_pid);
				scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
					internal_err_str);
				return (1);
			    }
			}
		} else {
			(void) sprintf(internal_err_str,
				"Failed to check main dispatcher process "
				"status with kill(pid,0) command. pid was "
				"%d", *disp_pid);
			scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
				internal_err_str);
			return (1);
		}
	}
	return (SCHA_ERR_NOERR);
}



/* Get the extension properties from the SAP config that aren't in the ptable */
int
sap_get_extensions(scds_handle_t handle, sap_extprops_t *sapxprops,
		boolean_t print_messages)
{
	int				err;
	scha_extprop_value_t 		*extprop = NULL;
	char				internal_err_str[INT_ERR_MSG_SIZE];

	if (sapxprops == NULL) {
		(void) sprintf(internal_err_str,
			"SAP extension property structure was NULL");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		if (print_messages) {
			(void) fprintf(stderr, gettext("INTERNAL ERROR: %s.\n"),
				gettext(internal_err_str));
		}
		return (1);
	}

	/* Start filling in the sap_extpropt_t struct */
	/* SAPSID */
	err = scds_get_ext_property(handle, SAP_EXT_SAPSID, SCHA_PTYPE_STRING,
	    &extprop);
	if (err != SCHA_ERR_NOERR || extprop == NULL) {
		scds_syslog(LOG_ERR,
			"Failed to retrieve the property %s: %s.",
			SAP_EXT_SAPSID, scds_error_string(err));
		if (print_messages) {
			(void) fprintf(stderr, gettext("Failed to retrieve the "
				"property %s: %s.\n"), SAP_EXT_SAPSID,
				gettext(scds_error_string(err)));
		}
		return (1);
	}

	if (extprop->val.val_str[0] == '\0') {
		scds_syslog(LOG_ERR, "Property %s is not set.", SAP_EXT_SAPSID);
		if (print_messages) {
			(void) fprintf(stderr, gettext("Property %s is not "
				"set.\n"), SAP_EXT_SAPSID);
		}
		if (extprop) scds_free_ext_property(extprop);
		return (1);
	}

	if ((sapxprops->se_sapsid = strdup(extprop->val.val_str)) == NULL) {
		(void) sprintf(internal_err_str,
			"Failed to allocate space for %s", SAP_EXT_SAPSID);
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		if (print_messages) {
			(void) fprintf(stderr, gettext("INTERNAL ERROR: %s.\n"),
				gettext(internal_err_str));
		}
		if (extprop) scds_free_ext_property(extprop);
		return (1);
	}

	if (extprop) scds_free_ext_property(extprop);
	extprop = NULL;

	/* As_services_string */
	err = scds_get_ext_property(handle, SAP_EXT_SERVICES, SCHA_PTYPE_STRING,
		&extprop);
	if (err != SCHA_ERR_NOERR || extprop == NULL) {
		scds_syslog(LOG_ERR, "Failed to retrieve the property %s: %s.",
			SAP_EXT_SERVICES, scds_error_string(err));
		if (print_messages) {
			(void) fprintf(stderr, gettext("Failed to retrieve the "
				"property %s: %s.\n"), SAP_EXT_SERVICES,
				gettext(scds_error_string(err)));
		}
		return (1);
	}
	if ((sapxprops->se_assvcstring = strdup(extprop->val.val_str))
		== NULL) {
		(void) sprintf(internal_err_str,
			"Failed to allocate space for %s", SAP_EXT_SERVICES);
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		if (print_messages) {
			(void) fprintf(stderr, gettext("INTERNAL ERROR: %s.\n"),
				gettext(internal_err_str));
		}
		if (extprop) scds_free_ext_property(extprop);
		return (1);
	}
	if (extprop) scds_free_ext_property(extprop);
	extprop = NULL;

	/* As_instance_id */
	err = scds_get_ext_property(handle, SAP_EXT_SAPSYSNUM_AS,
		SCHA_PTYPE_STRING, &extprop);
	if (err != SCHA_ERR_NOERR || extprop == NULL) {
		scds_syslog(LOG_ERR,
			"Failed to retrieve the property %s: %s.",
			SAP_EXT_SAPSYSNUM_AS, scds_error_string(err));
		if (print_messages) {
			(void) fprintf(stderr, gettext("Failed to retrieve the "
				"property %s: %s.\n"), SAP_EXT_SAPSYSNUM_AS,
				gettext(scds_error_string(err)));
		}
		return (1);
	}

	if (extprop->val.val_str[0] == '\0') {
		scds_syslog(LOG_ERR, "Property %s is not set.",
			SAP_EXT_SAPSYSNUM_AS);
		if (print_messages) {
			(void) fprintf(stderr, gettext("Property %s is not "
				"set.\n"), SAP_EXT_SAPSYSNUM_AS);
		}
		if (extprop) scds_free_ext_property(extprop);
		return (1);
	}

	if ((sapxprops->se_asinstancenum = strdup(extprop->val.val_str))
		== NULL) {
		(void) sprintf(internal_err_str,
		    "Failed to allocate space for %s", SAP_EXT_SAPSYSNUM_AS);
		    scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
			internal_err_str);
		if (print_messages) {
			(void) fprintf(stderr, gettext("INTERNAL ERROR: %s.\n"),
				gettext(internal_err_str));
		}
		if (extprop) scds_free_ext_property(extprop);
		return (1);
	}
	if (extprop) scds_free_ext_property(extprop);
	extprop = NULL;

	/* As_startup_script */
	err = scds_get_ext_property(handle, SAP_EXT_STARTSAP, SCHA_PTYPE_STRING,
		&extprop);
	if (err != SCHA_ERR_NOERR || extprop == NULL) {
		scds_syslog(LOG_ERR,
			"Failed to retrieve the property %s: %s.",
			SAP_EXT_STARTSAP, scds_error_string(err));
		if (print_messages) {
			(void) fprintf(stderr, gettext("Failed to retrieve the "
				"property %s: %s.\n"), SAP_EXT_STARTSAP,
				gettext(scds_error_string(err)));
		}
		return (1);
	}
	if (extprop->val.val_str[0] == '\0') {
		scds_syslog(LOG_ERR, "Property %s is not set.",
			SAP_EXT_STARTSAP);
		if (print_messages) {
			(void) fprintf(stderr, gettext("Property %s is not "
				"set.\n"), SAP_EXT_STARTSAP);
		}
		if (extprop) scds_free_ext_property(extprop);
		return (1);
	}

	if ((sapxprops->se_as_start = strdup(extprop->val.val_str)) == NULL) {
		(void) sprintf(internal_err_str,
			"Failed to allocate space for %s", SAP_EXT_STARTSAP);
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		if (print_messages) {
			(void) fprintf(stderr, gettext("INTERNAL ERROR: %s.\n"),
				gettext(internal_err_str));
		}
		if (extprop) scds_free_ext_property(extprop);
		return (1);
	}

	if (extprop) scds_free_ext_property(extprop);
	extprop = NULL;

	/* As_shutdown_script */
	err = scds_get_ext_property(handle, SAP_EXT_STOPSAP, SCHA_PTYPE_STRING,
		&extprop);
	if (err != SCHA_ERR_NOERR || extprop == NULL) {
		scds_syslog(LOG_ERR,
			"Failed to retrieve the property %s: %s.",
			SAP_EXT_STOPSAP, scds_error_string(err));
		if (print_messages) {
			(void) fprintf(stderr, gettext("Failed to retrieve the "
				"property %s: %s.\n"), SAP_EXT_STOPSAP,
				gettext(scds_error_string(err)));
		}
		return (1);
	}

	if (extprop->val.val_str[0] == '\0') {
		scds_syslog(LOG_ERR, "Property %s is not set.",
			SAP_EXT_STOPSAP);
		if (print_messages) {
			(void) fprintf(stderr, gettext("Property %s is not "
				"set.\n"), SAP_EXT_STOPSAP);
		}
		if (extprop) scds_free_ext_property(extprop);
		return (1);
	}

	if ((sapxprops->se_as_stop = strdup(extprop->val.val_str)) == NULL) {
		(void) sprintf(internal_err_str,
			"Failed to allocate space for %s", SAP_EXT_STOPSAP);
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		if (print_messages) {
			(void) fprintf(stderr, gettext("INTERNAL ERROR: %s.\n"),
				gettext(internal_err_str));
		}
		if (extprop) scds_free_ext_property(extprop);
		return (1);
	}

	if (extprop) scds_free_ext_property(extprop);
	extprop = NULL;

	/* As_db_retry_interval */
	err = scds_get_ext_property(handle, SAP_EXT_DBRETRYINTERVAL,
		SCHA_PTYPE_INT, &extprop);
	if (err != SCHA_ERR_NOERR || extprop == NULL) {
		scds_syslog(LOG_ERR, "Failed to retrieve the property %s: %s.",
			SAP_EXT_DBRETRYINTERVAL, scds_error_string(err));
		if (print_messages) {
			(void) fprintf(stderr, gettext("Failed to retrieve the "
				"property %s: %s.\n"), SAP_EXT_DBRETRYINTERVAL,
				gettext(scds_error_string(err)));
		}
		return (1);
	}
	sapxprops->se_dbretryinterval = extprop->val.val_int;
	extprop = NULL;

	/* Stop_sap_pct */
	err = scds_get_ext_property(handle, SAP_EXT_STOPSAPPCT,
		SCHA_PTYPE_INT, &extprop);
	sapxprops->se_stopsappct = extprop->val.val_int;
	extprop = NULL;
	return (0);
}
