/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 1998-2002 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)sap_as_validate.c	1.10	07/06/06 SMI"

#include "sap_as.h"

int
main(int argc, char *argv[])
{
	scds_handle_t		scds_handle;
	sap_extprops_t		sapxprops;
	int			rc;

	/* I18N stuff */
	(void) setlocale(LC_ALL, "");
	(void) textdomain(TEXT_DOMAIN);
	(void) bindtextdomain(TEXT_DOMAIN, MESSAGE_DIR);

	/* Process arguments passed by RGM and initialize syslog */
	if ((rc = scds_initialize(&scds_handle, argc, argv)) !=
			SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR, "Failed to retrieve the resource "
			"handle: %s", scds_error_string(rc));
		(void) fprintf(stderr, gettext("Failed to retrieve the "
			"resource handle: %s\n"),
			gettext(scds_error_string(rc)));
		return (1);
	}

	if (sap_get_extensions(scds_handle, &sapxprops, B_TRUE) != SAP_OK) {
		scds_close(&scds_handle);
		exit(1);
	}

	rc = svc_validate(scds_handle, &sapxprops, B_TRUE);

	/* Free up all the memory allocated by scds_initialize */
	scds_close(&scds_handle);
	return (rc);
}
