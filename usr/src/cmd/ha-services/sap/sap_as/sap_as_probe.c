/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 1998-2002 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#pragma ident	"@(#)sap_as_probe.c	1.14	07/06/06 SMI"

#include "sap_as.h"

/*
 * Just an infinite loop which waits for the probe_interval.
 * If the PMF action script contacts the probe, then we count that
 * as a total failure. Otherwise, do the AS probes.
 */
int
main(int argc, char *argv[])
{
	scds_handle_t 			scds_handle;
	sap_extprops_t			sapxprops;

	int				doexit = 0;
	hrtime_t			ht1, ht2;
	int				probe_to, probe_timeout;
	int				probe_result;
	unsigned long			dt;
	int				disp_pid = 0;

	/* So printout from R3trans etc won't show on console */
	(void) close(1);
	(void) close(2);

	/* Parse and verify the command line args and open syslog */
	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR)
		exit(1);

	if (sap_get_extensions(scds_handle, &sapxprops, B_FALSE) != SAP_OK) {
		scds_close(&scds_handle);
		exit(1);
	}

	/* setup probe interval = Thorough_Probe_Interval */
	probe_to = scds_get_rs_thorough_probe_interval(scds_handle);
	probe_timeout = scds_get_ext_probe_timeout(scds_handle);

	/* get the pids for dispatcher */
	(void) read_in_disp_pid(&disp_pid, scds_get_resource_name(scds_handle));

	while (doexit == 0) {
	/*
	 * sleep for a duration of thorough_probe_interval between
	 *  successive probes.
	 */
		(void) scds_fm_sleep(scds_handle,
			scds_get_rs_thorough_probe_interval(scds_handle));

		ht1 = gethrtime();
		probe_result = svc_probe(scds_handle, &sapxprops,
			probe_timeout, &disp_pid);

		ht2 = gethrtime();

		/* Convert to milliseconds */
		dt = (ulong_t)((ht2 - ht1) / 1e6);

		/*
		 * Evaluate and act from probe.
		 * scds_fm_action set the status for the resource.
		 */
		(void) scds_fm_action(scds_handle, probe_result, (long)dt);

		scds_syslog_debug(DBG_LEVEL_HIGH,
			    "Probe is sleeping for %d seconds.",
			    probe_to);
	} /* while */
	return (0);
}
