/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 1998-2002 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 */

#ifndef	_SAP_AS_H
#define	_SAP_AS_H

#pragma ident	"@(#)sap_as.h	1.13	07/06/06 SMI"


#ifdef	__cplusplus
extern "C" {
#endif

#include "../util/sap_util.h"

#define	SAP_SVC_NAME		"sap_as"

/* SAP extensions */
#define	SAP_EXT_SAPSID		"SAPSID"
#define	SAP_EXT_SAPSYSNUM_AS	"As_instance_id"
#define	SAP_EXT_SERVICES	"As_services_string"
#define	SAP_EXT_PSCHECKRETRY	"Process_check_retry"
#define	SAP_EXT_DBRETRYINTERVAL	"As_db_retry_interval"
#define	SAP_EXT_STOPSAPPCT	"Stop_sap_pct"
#define	SAP_EXT_STARTSAP	"As_startup_script"
#define	SAP_EXT_STOPSAP		"As_shutdown_script"

/* Data service provided binary to start R3 */
#define	SAP_CMD_STARTR3		"sap_as_startR3"

/* temp files for storing validate results */
#define	SAP_TMP_COUNT_FILE	"/tmp/sapscript_count"

/* XXX */
/* #define	SAP_STDOUT_REDIR	"> /dev/null 2>&1" */
#define	SAP_CMD_LOGFILE		"/tmp/sap.out"

/*
 * Probe functionality
 */

/* extension property */
typedef struct sap_extprops
{
	char		*se_sapsid;		/* SAPSID */
	char		*se_assvcstring;	/* As_services_string */
	char		*se_asinstancenum;	/* As_instance_id */
	char		*se_as_start;		/* As_startup_script */
	char		*se_as_stop;		/* As_shutdown_script */
	int		se_pscheckretry;	/* Process_check_retry */
	int		se_dbretryinterval;	/* As_db_retry_interval */
	int		se_stopsappct;		/* Stop_sap_pct */
} sap_extprops_t;

/* Function to fill in SAP specific properties */
int sap_get_extensions(scds_handle_t handle, sap_extprops_t *sapxprops,
		boolean_t print_messages);

/* Validation and update */
int svc_validate(scds_handle_t handle, sap_extprops_t *sapxprops,
		boolean_t print_messages);
int svc_update(scds_handle_t scds_handle, sap_extprops_t *sapxprops);

/* Basic start and stop functionality */
int svc_start(scds_handle_t handle, char *sapsid, char *sapservice,
	char *sapsysnum);
int sap_as_startR3(scds_handle_t handle, sap_extprops_t *sapxprops);
int svc_stop(scds_handle_t handle, sap_extprops_t *sapxprops);

/* Fault monitor functions */
int svc_fm_start(scds_handle_t handle);
int svc_fm_stop(scds_handle_t handle);
int svc_fm_check(scds_handle_t handle, sap_extprops_t *sapxprops);

/* Probe related functions */
int
svc_probe(scds_handle_t handle, sap_extprops_t *sapxprops, int probe_timeout,
	int *disp_pid);
int
probe_proc(int *probe_rc, const char *rtbase, char *sapsid, char *service,
	char *sysnum, const char *rname, int *disp_pid);
int
svc_wait(scds_handle_t handle, sap_extprops_t *sapxprops);

#ifdef	__cplusplus
}
#endif

#endif /* _SAP_AS_H */
