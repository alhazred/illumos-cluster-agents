/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * sap_util.c - Util methods for highly available SAP
 */

#pragma ident	"@(#)sap_util.c	1.36	07/06/06 SMI"

#include "sap_util.h"

/*
 * This routine returns the instruction set architecture.
 * Returns 32:	32-bit
 *	   64: 	64-bit
 *	  -1: 	error
 *
 */
int
machine_architecture()
{
	char 	isalist[ISALIST_SIZE];
	const char	sparcv9_char[ARCH_ARRAY_SIZE] = "sparcv9";
	const char	amd64_char[ARCH_ARRAY_SIZE] = "amd64";

	if (sysinfo(SI_ISALIST, isalist, sizeof (isalist)) == -1) {
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
			strerror(errno)); /*lint !e746 */
		return (-1);
	}
	if (strstr(isalist, sparcv9_char) == NULL &&
			strstr(isalist, amd64_char) == NULL) {
		/* 32-bit */
		return (32);
	}
	return (64);
}


/*
 * This routine returns the elf class of the file.
 * Returns: 32 32-bit
 *          64 64-bit
 *          -1 error
 *
 */
int
file_elf_class(char *file)
{
	int	ifd, rc;
	Elf	*elf;
	GElf_Ehdr	ehdr;
	char		internal_err_str[INT_ERR_MSG_SIZE];

	ifd = open(file, O_RDONLY);
	if (ifd < 0) {
		(void) sprintf(internal_err_str,
			"Cannot open %s:%s", file, strerror(errno));
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		return (-1);
	}

	if (elf_version(EV_CURRENT) == EV_NONE) {
		(void) sprintf(internal_err_str,
			"ELF library out of date");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		(void) close(ifd);
		return (-1);
	}

	/* make sure file descriptor describes an ELF file */
	if ((elf = elf_begin(ifd, ELF_C_READ, (Elf *)0)) == NULL) {
		(void) snprintf(internal_err_str, sizeof (internal_err_str),
			"Failed to allocate an ELF descriptor");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		rc = -1;
	}
	switch (elf_kind(elf)) {
	case ELF_K_ELF:
		break;
	default:
		(void) snprintf(internal_err_str, sizeof (internal_err_str),
			"%s is not an ELF file", file);
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		rc = -1;
	}

	if (gelf_getehdr(elf, &ehdr) == (GElf_Ehdr *)0) {
		(void) snprintf(internal_err_str, sizeof (internal_err_str),
			"Cannot read ELF header.");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		rc = -1;
	}

	switch (gelf_getclass(elf)) {
		case ELFCLASS32:
			rc = 32;
			break;
		case ELFCLASS64:
			rc = 64;
			break;
		default:  /* ELFCLASSNONE, assume 32-bit */
			rc = 32;
			break;
	}
	(void) elf_end(elf);
	(void) close(ifd);
	return (rc);
}

/*
 * This routine checks whether the saposcol process is the right one for
 * this environment.
 * Returns 0:	correct file type for this environment
 *	   1:	error
 *
 */
int
correct_saposcol(char *sapsid, boolean_t print_messages)
{
	int	machine_arch = 0, file_arch = 0, rc = 0;
	char	internal_err_str[INT_ERR_MSG_SIZE];
	char	*bin_path = NULL;
	char	saposcol[SCDS_ARRAY_SIZE];

	if ((machine_arch = machine_architecture()) == -1) {
		(void) sprintf(internal_err_str,
			"Cannot retrieve machine architecture.");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		if (print_messages) {
			(void) fprintf(stderr, gettext("INTERNAL ERROR: %s.\n"),
				gettext(internal_err_str));
		}
		return (1);
	}

	if ((bin_path = get_bin_path(sapsid, print_messages)) == NULL) {
		(void) sprintf(internal_err_str,
			"Failed to retrieve sap bin path: "
			"Output error encounted");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
				internal_err_str);
		if (print_messages) {
			(void) fprintf(stderr, gettext("INTERNAL ERROR: %s.\n"),
				gettext(internal_err_str));
		}
		return (1);
	}

	rc = snprintf(saposcol, sizeof (saposcol),
			"%s/%s", bin_path, SAP_PRG_SAPOSCOL);

	free(bin_path);

	if ((rc < 0) || (rc > (int)sizeof (saposcol))) {
		(void) sprintf(internal_err_str,
			"Failed to form command %s to start saposcol: "
			"Output error encounted", saposcol);
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		if (print_messages) {
			(void) fprintf(stderr, gettext("INTERNAL ERROR: %s.\n"),
				gettext(internal_err_str));
		}
		return (1);
	}

	if ((file_arch = file_elf_class(saposcol)) == -1) {
		(void) snprintf(internal_err_str, sizeof (internal_err_str),
			"Cannot retrieve architecture for file %s.", saposcol);
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		if (print_messages) {
			(void) fprintf(stderr, gettext("INTERNAL ERROR: %s.\n"),
				gettext(internal_err_str));
		}
		return (1);
	}

	if (file_arch == machine_arch) {
		/*
		 * 32-bit saposcol runs on 32-bit machine or 64-bit
		 * saposcol runs on 64-bit machine
		 */
		return (0);
	} else {
		/*
		 * SCMSGS
		 * @explanation
		 * The architecture of saposcol is not compatable to the
		 * current running Solaris version. For example, you have a
		 * 64-bit saposcol running on a 32-bit Solaris machine or vice
		 * verse.
		 * @user_action
		 * Make sure the correct saposcol is installed on the cluster.
		 */
		scds_syslog(LOG_ERR, "%d-bit saposcol is running on"
			" %d-bit Solaris.", file_arch, machine_arch);
		if (print_messages) {
			(void) fprintf(stderr, gettext("%d-bit saposcol is "
				"running on %d-bit Solaris.\n"), file_arch,
				machine_arch);
		}
		return (1);
	}
}


/*
 * This routine returns the SAP admin. user name.
 * It takes the SAPSID string and converts it to lower case, then
 * add 'adm' to the end of the sapsid string.
 * Caller of this routine needs to free the space.
 */

char
*get_admuser_name(char *sapsid)
{
	char 	*admuser = NULL;
	char 	adm[4] = "adm";
	int 	count = 0;
	char	internal_err_str[INT_ERR_MSG_SIZE];

	if (sapsid == NULL) {
		(void) sprintf(internal_err_str,
			"Null value is passed to get_admuser_name()");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		return (admuser);
	} else {
		admuser = calloc(1, strlen(sapsid) + sizeof (adm) + 1);
		if (admuser == NULL) {
			scds_syslog(LOG_ERR, "Out of memory.");
			return (admuser);
		}
		if (strcat(admuser, sapsid) == NULL) {
			(void) sprintf(internal_err_str,
				"Failed to allocate space for user <%s>adm",
				sapsid);
			scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
				internal_err_str);
			return (admuser);
		}

		for (; count < (int)strlen(admuser); count++) {
			admuser[count] = (char)tolower(admuser[count]);
		}
		return (strcat(admuser, adm));
	}
}


/*
 * Probe the SAP database
 * We probe the DB using a SAP utility 'R3trans' with option '-d' to check
 * whether can connect to DB with user <sapsid>adm.
 *
 * This routine returns 0 if no internal error occurs, and probe_db
 * finished, otherwise return 1 for internal error.
 * probe_rc return SAP_PROBE_ERROR if connect to DB failed, return
 * SAP_PROBE_TIMEOUT if R3trans command timesout, returns SAP_PROBE_OK if
 * DB is up and running.
 *
 * This routine only checks the connection to DB, and it will not be called
 * if any of the sap checkings failed. Inside this routine, status of the
 * sap resource is being set depending on whether DB connection is successful
 * or not. This might be out of sync with what the failure history is (for
 * the case that if sap checkings are all fine, but DB connection failed).
 * This routine will set the sap resource status to be degraded, but the
 * failure history from scds_fm_action() still thinks it's ok. Once the
 * outcome of any sap checkings changes, then scds_fm_action() will set
 * the status of the sap resource, and that will "overwrite" whatever status
 * this routine sets.
 *
 * Should revisit this now that we have the capability for inter-rg
 * dependencies.
 */
int
probe_db(scds_handle_t handle, char *sapsid, int probe_to, int *probe_rc)
{
	int		exit_code = 0;
	scha_err_t	rc;
	char    	cmd[SCDS_ARRAY_SIZE];
	char    	*admuser;
	scha_resource_t	local_handle;
	scha_status_value_t	*status;
	char		internal_err_str[INT_ERR_MSG_SIZE];

	if (probe_rc == NULL) {
		(void) sprintf(internal_err_str,
			"Failed to allocate space for probe result");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		return (1);
	}

	/* get user <sapsid>adm */
	if ((admuser = get_admuser_name(sapsid)) == NULL) {
		return (1);
	}

	/* initialize to ERROR just in case we bailout early */
	*probe_rc = SAP_PROBE_ERROR;

	/*
	 * SAP Database is probed by doing:
	 *	hatimerun -t <to> su - <sidadm> -c 'R3trans -d' >/dev/null
	 * 'su' sets the neccessary environment for the command R3trans. The
	 * return code for 'R3trans -d' can be 0|4|8, which means DB is up.
	 * Codes other than that, means DB is not reachable.
	 */
	rc = snprintf(cmd, sizeof (cmd), "/usr/bin/su - %s -c '%s %s'",
	    admuser,
	    SAP_CMD_R3TRANS,
	    SAP_STDOUT_REDIR);

	if (rc == -1) {
		(void) sprintf(internal_err_str,
			"Unable to form database probe command");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		free(admuser);
		return (1);
	}

	/*
	 * get the current status of rs, set the new status after probing DB
	 * only if there is a change on the status.
	 */
	rc = scha_resource_open(scds_get_resource_name(handle),
		scds_get_resource_group_name(handle), &local_handle);
	if (rc != SCHA_ERR_NOERR) {
		scds_syslog(LOG_ERR,
			"Failed to retrieve the resource handle: %s.",
			scds_error_string(rc));
			free(admuser);
			return (rc);
	}

	rc = scha_resource_get(local_handle, SCHA_STATUS, &status);
	if (rc != SCHA_ERR_NOERR) {
		(void) sprintf(internal_err_str,
			"Failed to retrieve the resource status: %s",
			scds_error_string(rc));
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
			free(admuser);
		return (rc);
	}

	scds_syslog_debug(DBG_LEVEL_HIGH,
		"Probing SAP Database with command %s under hatimerun.", cmd);

	/* scds_timerun() will run the cmd with hatimerun */
	rc = scds_timerun(handle, cmd, probe_to, SIGTERM, &exit_code);

	scds_syslog_debug(DBG_LEVEL_HIGH,
		"Command to probe SAP Database returned %d with exit code %d.",
		rc, exit_code);

	scds_syslog_debug(DBG_LEVEL_HIGH,
		"Status in probe is %d", status->status);

	if (rc == SCHA_ERR_NOERR) {
		/* return code 0|4|8 means DB is up */
		if ((exit_code == 0) || (exit_code == 4) || (exit_code == 8)) {
			*probe_rc = SAP_PROBE_OK;
			if (status->status != SCHA_RSSTATUS_OK) {
				(void) scha_resource_setstatus(
					scds_get_resource_name(handle),
					scds_get_resource_group_name(handle),
					SCHA_RSSTATUS_OK,
					"Database is up.");
			}

		} else { /* DB is not up */
			*probe_rc = SAP_PROBE_ERROR;
			if (status->status != SCHA_RSSTATUS_DEGRADED) {
				(void) scha_resource_setstatus(
					scds_get_resource_name(handle),
					scds_get_resource_group_name(handle),
					SCHA_RSSTATUS_DEGRADED,
					"Database might be down.");
			}
		}
	} else if (rc == SCHA_ERR_TIMEOUT) {
		*probe_rc = SAP_PROBE_ERROR;
		(void) scha_resource_setstatus(
			scds_get_resource_name(handle),
			scds_get_resource_group_name(handle),
			SCHA_RSSTATUS_DEGRADED,
			"Database might be down.");
	    } else {	/* internal error from scds_timerun() */
			(void) sprintf(internal_err_str,
				"Failed to check on the database"
				" with command %s", cmd);
			scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
				internal_err_str);
			free(admuser);
			return (1);
	    }

	rc = scha_resource_close(local_handle);
	if (rc != SCHA_ERR_NOERR) {
		(void) sprintf(internal_err_str,
			"Failed to close the resource: %s",
			scds_error_string(rc));
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
			free(admuser);
		return (rc);
	}

	free(admuser);
	return (0);
}


/*
 * This routine takes the SAP program file and then check whether it
 * exists and is executable or not.
 * Return 0 if file exists and is executable, otherwise, return 1.
 */

int
check_sap_file(boolean_t script, char *sapsid, char *progname,
	scds_hasp_status_t hasp_status, boolean_t print_messages)
{
	char	*sap_path, sap_program[SCDS_ARRAY_SIZE];
	int	rc = 0, check_exec_perm;
	struct stat statbuf;
	char	internal_err_str[INT_ERR_MSG_SIZE];

	if (script) {
		/* check script name contain '/' or not */
		if ((strchr(progname, '/')) != NULL) {
			/*
			 * SCMSGS
			 * @explanation
			 * The script name should be just the script name, no
			 * path is needed.
			 * @user_action
			 * Specify just the script name without any path.
			 */
			scds_syslog(LOG_ERR, "Invalid script name %s. It "
				"cannot contain any '/'.", progname);
			if (print_messages) {
				(void) fprintf(stderr, gettext("Invalid script "
					"name %s. It cannot contain any "
					"'/'.\n"), progname);
			}
			return (1);
		}

		/* get the path for the script under user <sapsid>adm */
		sap_path = get_script_path(sapsid, print_messages);
	} else {	/* get the path for the binary */
		sap_path = get_bin_path(sapsid, print_messages);
	}

	if (sap_path == NULL) {
		return (1);
	}

	rc = snprintf(sap_program, MAXPATHLEN, "%s/%s", sap_path, progname);

	if (rc == -1) {
		/* ignore err return code above */
		(void) sprintf(internal_err_str,
		    "Failed to form path %s with program %s. "
		    "The path may be too long", sap_path, progname);
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		if (print_messages) {
			(void) fprintf(stderr, gettext("INTERNAL ERROR: %s.\n"),
				gettext(internal_err_str));
		}
		rc = 1;
		goto finished;
	}
	/* Reset rc */
	rc = 0;
	/* By default, we will check if the file is executable */
	check_exec_perm = 1;
	if (stat(sap_program, &statbuf) != 0) {
		/*
		 * If the file doesnt exist, its ok only for
		 * SCDS_HASP_ONLINE_NOT_LOCAL. If theres an error other
		 * than ENOENT, thats definitely a sign of trouble.
		 */
		if ((errno == ENOENT) &&	/*lint !e746 */
			(hasp_status == SCDS_HASP_ONLINE_NOT_LOCAL)) {
			/* file's not here, but its ok */
			check_exec_perm = 0;
		} else {
			rc = errno;
			scds_syslog(LOG_ERR, "%s: %s.",
			sap_program, strerror(rc));
			if (print_messages) {
				(void) fprintf(stderr, "%s: %s.\n", sap_program,
					gettext(strerror(rc)));
			}
			rc = 1;
			goto finished;
		}
	}

	/* check permisions only if the file is around */
	if (check_exec_perm) {
		/* check that the binary is executable */
		if ((statbuf.st_mode & S_IXUSR) != S_IXUSR) {
			scds_syslog(LOG_ERR, "Incorrect permissions "
				"set for %s.", sap_program);
			if (print_messages) {
				(void) fprintf(stderr, gettext("Incorrect "
					"permissions set for %s.\n"),
					sap_program);
			}
			rc = 1;
			goto finished;
		}
	}
finished:

	if (sap_path) free(sap_path);
	return (rc);
}

/*
 * This routine retrieves the user home directory path for user sapsid.
 * Default is /usr/sap/<SAPSID>/home/<sapsid>adm.
 * Caller of this routine should free the space for the user home dir path.
 * Return a pointer to the path upon success, otherwise, return NULL.
 */
char
*get_script_path(char *sapsid, boolean_t print_messages)
{

	char		*admuser;
	struct passwd   *admuserpwd;
	struct passwd	pwd;
	char		internal_err_str[INT_ERR_MSG_SIZE];
	char		buffer[MAXPATHLEN*2];
	char		*dirpath;

	admuser = get_admuser_name(sapsid);
	if (admuser == NULL) {
		(void) sprintf(internal_err_str,
			"Failed to retrieve SAP admin user <%s>adm", sapsid);
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		if (print_messages) {
			(void) fprintf(stderr, gettext("INTERNAL ERROR: %s.\n"),
				gettext(internal_err_str));
		}
		return (NULL);
	}

	/*
	 * MT safe routine getpwnam_r instead of getpwnam(). Resulting
	 * pinter is the same as the pointer to pwd.
	 */

	admuserpwd = getpwnam_r(admuser, &pwd, buffer, MAXPATHLEN);
	if (admuserpwd == NULL) {
	    if (errno == ERANGE) {
		(void) sprintf(internal_err_str,
			"Buffer for user %s data is not big enough", admuser);
		scds_syslog(LOG_ERR,
		    "INTERNAL ERROR: %s.", internal_err_str);
		if (print_messages) {
			(void) fprintf(stderr, gettext("INTERNAL ERROR: %s.\n"),
				gettext(internal_err_str));
		}
		return (NULL);
	    } else {
			/*
			 * SCMSGS
			 * @explanation
			 * Failed to retrieve home directory for the specified
			 * SAP user for the specified system ID.
			 * @user_action
			 * Check the system ID for SAP. SAPSID is case
			 * sensitive.
			 */
			scds_syslog(LOG_ERR,
				"Failed to retrieve information for user %s "
				"for SAP system %s.", admuser, sapsid);
			if (print_messages) {
				(void) fprintf(stderr, gettext("Failed to "
					"retrieve information for user %s for "
					"SAP system %s.\n"), admuser, sapsid);
			}
			return (NULL);
	    }
	} else if (pwd.pw_dir == NULL) {
			scds_syslog(LOG_ERR, "Home dir is not set for user %s.",
				admuser);
			if (print_messages) {
				(void) fprintf(stderr, gettext("Home dir is "
					"not set for user %s.\n"), admuser);
			}
			return (NULL);
		} else if (strlen(pwd.pw_dir) == 0) {
			scds_syslog(LOG_ERR, "Home dir is not set for user %s.",
				admuser);
			if (print_messages) {
				(void) fprintf(stderr, gettext("Home dir is "
					"not set for user %s.\n"), admuser);
			}
			return (NULL);
		}

	if ((dirpath = strdup(pwd.pw_dir)) == NULL) {
		(void) sprintf(internal_err_str, "Out of memory");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		if (print_messages) {
			(void) fprintf(stderr, gettext("INTERNAL ERROR: %s.\n"),
				gettext(internal_err_str));
		}
	}

	free(admuser);
	return (dirpath);
}


/*
 * This routine creates the SAP binary path. The default is
 * /usr/sap/<SAPSID>/SYS/exe/run.
 * Returns a pointer to the path upon success, otherwise, return NULL.
 * Caller of this routine has to free the memory.
 */
char
*get_bin_path(char *sapsid, boolean_t print_messages)
{

	char	*bin_path = NULL;
	char	internal_err_str[INT_ERR_MSG_SIZE];
	int	rc;

	bin_path = (char *)malloc(MAXPATHLEN);
	if (bin_path == NULL) {
		scds_syslog(LOG_ERR, "Out of memory.");
		if (print_messages) {
			(void) fprintf(stderr, gettext("Out of memory.\n"));
		}
		return (bin_path);
	}
	rc = snprintf(bin_path, MAXPATHLEN, "%s/%s/%s",
		SAP_DIR_USRSAP,
		sapsid,
		SAP_DIR_SYS);
	if (rc == -1) {
		(void) sprintf(internal_err_str,
			"Failed to form SAP bin path command: "
			"Output error encountered");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		if (print_messages) {
			(void) fprintf(stderr, gettext("INTERNAL ERROR: %s.\n"),
				gettext(internal_err_str));
		}
		return (NULL);
	}
	return (bin_path);
}


/*
 * This routine retrieve the pid of the message server and write it to a file
 * Returns 0 upon success, otherwise, return 1.
 */
int
retrieve_ms_pid(char *sapsid, const char *rname)
{
	int	rc = 0;
	char	pidcmd[SCDS_ARRAY_SIZE];
	char	internal_err_str[INT_ERR_MSG_SIZE];

	rc = snprintf(pidcmd, sizeof (pidcmd),
			"/bin/pgrep -f \\^ms.sap%s > %s/%s.ms 2>/dev/null",
			sapsid, SAP_PID_FILE_DIR, rname);
	if (rc == -1) {
		(void) sprintf(internal_err_str,
			"Failed to form command to retrieve pid "
			"for message server");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		return (1);
	}
	rc = system(pidcmd);
	if (rc == -1) {
		scds_syslog(LOG_ERR, "Cannot execute %s: %s.",
			pidcmd, strerror(errno));
		return (1);
	}
	if (WIFSIGNALED((uint_t)rc) || (WIFEXITED((uint_t)rc) &&
		WEXITSTATUS((uint_t)rc) != 0)) {
		scds_syslog_debug(DBG_LEVEL_HIGH,
			"%s failed to complete.", pidcmd);
		return (1);
	}
	return (0);
}


/*
 * This routine retrieve the pid of the dispatcher and write it to a file
 * Returns 0 upon success, otherwise, return 1.
 */
int
retrieve_disp_pid(const char *rtbasedir, char *sapsid, char *service,
	char *sysnum, const char *rname)
{

	int	rc = 0;
	char	pidcmd[SCDS_ARRAY_SIZE];
	int	disp_pid = 0;
	FILE	*fp, *fpdisp;
	char	internal_err_str[INT_ERR_MSG_SIZE];
	char	dispfile[MAXPATHLEN];

	/*
	 * form the command to retrieve pid for the main dispatcher:
	 * /rtbasedir/sap_get_disp_pid sapsid sap_service_string sapsysnum
	 */
	rc = snprintf(pidcmd, sizeof (pidcmd),
				"%s/%s %s %s %s  2>/dev/null ",
				rtbasedir, SAP_CMD_PROCPROBE, sapsid,
				service, sysnum);
	if (rc == -1) {
		(void) sprintf(internal_err_str,
			"Failed to form command to retrieve pid "
			"for the dispatcher");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		return (1);
	}

	if ((fp = popen(pidcmd, "r")) != NULL) {
		(void) fscanf(fp, "%d", &disp_pid);
		rc = pclose(fp);
		if (WIFEXITED((uint_t)rc) && (WEXITSTATUS((uint_t)rc) != 0)) {
			(void) sprintf(internal_err_str,
			    "Failed to close pipe with pclose. "
			    "Output error encountered");
			scds_syslog(LOG_ERR,
				"INTERNAL ERROR: %s.", internal_err_str);
			return (1);
		}
	} else {
		(void) sprintf(internal_err_str,
			"Failed to open pipe with popen. "
			"Output error encountered");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		return (1);
	}

	/*
	 * sap main dispatcher is started the startup script, so its pid
	 * won't be 1. If the pid was never retrieved, it has an initial
	 * value of 0. So consider the pid is good if the value is
	 * greater than 1.
	 */
	if (disp_pid <= 1) {
		scds_syslog_debug(DBG_LEVEL_HIGH,
			"Cannot find the pid of the dispatcher");
		return (1);
	}

	pidcmd[0] = NULL;

	/* write the dispatcher pid to file named /tmp/<rsname>.disp. */
	rc = snprintf(dispfile, sizeof (dispfile),
		"%s/%s.disp", SAP_PID_FILE_DIR, rname);
	if (rc == -1) {
		(void) sprintf(internal_err_str,
			"Failed to form dispatcher pid file path. "
			"Output error encountered");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		return (1);
	}
	if ((fpdisp = fopen(dispfile, "w")) == NULL) {
		(void) sprintf(internal_err_str,
		    "Failed to open dispatcher pid file. "
		    "Output error encountered");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		return (1);
	}
	if (fprintf(fpdisp, "%d", disp_pid) < 0) {
		(void) sprintf(internal_err_str,
			"Failed to write dispatcher pid to file. "
			"Output error encountered");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		(void) fclose(fpdisp);
		return (1);
	}
	(void) fclose(fpdisp);
	return (0);
}


/*
 * This routine read the pid file for message server and
 * passes the pid for message server to the caller.
 * Return 0 upon success, otherwise, return 1.
 */
int
read_in_ms_pid(int *ms_pid, const char *rname)
{
	int 	rc = 0;
	char	msfile[MAXPATHLEN];
	FILE	*fpms;
	char	internal_err_str[INT_ERR_MSG_SIZE];


	/*
	 * read in pid of message server from file that was created in
	 * svc_wait()
	 */
	rc = snprintf(msfile, sizeof (msfile),
		"%s/%s.ms", SAP_PID_FILE_DIR, rname);
	if (rc == -1) {
		(void) sprintf(internal_err_str,
		    "Failed to form message server pid file path. "
		    "Output error encountered");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		return (1);
	}
	if ((fpms = fopen(msfile, "r")) == NULL) {
		(void) sprintf(internal_err_str,
			"Failed to open message server pid file. "
			"Output error encountered");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		return (1);
	}
	if (fscanf(fpms, "%d", ms_pid) != 1) {
		(void) sprintf(internal_err_str,
			"Failed to retrieve message server pid from file. "
			"Output error encountered");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		(void) fclose(fpms);
		return (1);
	}

	(void) fclose(fpms);
	return (0);
}


/*
 * This routine read the pid file for dispatcher and
 * passes the pid for dispatcher to the caller.
 * Return 0 upon success, otherwise, return 1.
 */
int
read_in_disp_pid(int *disp_pid, const char *rname)
{

	int		rc = 0;
	char		dispfile[MAXPATHLEN];
	FILE		*fpdisp;
	char    	internal_err_str[INT_ERR_MSG_SIZE];


	/* read in pid of dispatcher from file that was created in svc_wait() */
	rc = snprintf(dispfile, sizeof (dispfile),
		"%s/%s.disp", SAP_PID_FILE_DIR, rname);
	if (rc == -1) {
		(void) sprintf(internal_err_str,
		    "Failed to form dispatcher pid file path. "
		    "Output error encountered");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		return (1);
	}
	if ((fpdisp = fopen(dispfile, "r")) == NULL) {
		(void) sprintf(internal_err_str,
			"Failed to open dispatcher pid file. "
			"Output error encountered");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		return (1);
	}
	if (fscanf(fpdisp, "%d", disp_pid) != 1) {
		(void) sprintf(internal_err_str,
			"Failed to retrieve dispatcher pid from file. "
			"Output error encountered");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		(void) fclose(fpdisp);
		return (1);
	}
	(void) fclose(fpdisp);
	return (0);
}

/*
 * This routine checks the lock file which was created when sap is
 * brought up. If can't put a lock on the lock file(which means sap is up
 * and running), then create a state file to signal the stop method that
 * sap is brought up already, don't try to stop it.
 * Returns 0 if sap is not up, returns 1 is sap is up, returns -1 if some
 * internal error occured.
 */
int
is_sap_running(char *lock_file, const char *rs_name)
{
	flock_t fl;
	int	lockfd;
	int 	rc = 1, n, m;
	char    statefile[MAXPATHLEN];
	char	internal_err_str[INT_ERR_MSG_SIZE];

	if (mkdirp(TEST_LOCK_DIR, 0755) != 0) {
		if (errno != EEXIST) {
			(void) sprintf(internal_err_str,
				"Failed to create directory: %s",
				TEST_LOCK_DIR);
			scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
				internal_err_str);
			return (-1);
		}
		scds_syslog_debug(DBG_LEVEL_HIGH,
			"LOCK dir <%s> existed.", TEST_LOCK_DIR);
	}
	rc = sprintf(statefile, "%s/running.%s", TEST_LOCK_DIR, rs_name);
	if (rc == -1) {
		(void) sprintf(internal_err_str,
			"Failed to form state file path");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
	}

	if (unlink(statefile) != 0) {
		if (errno != ENOENT) {
			(void) sprintf(internal_err_str,
				"Failed to delete state file %s",
				statefile);
			scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
				internal_err_str);
			return (-1);
		}
	}
	scds_syslog_debug(DBG_LEVEL_HIGH, "Lockfile=%s.", lock_file);

	lockfd = open(lock_file, O_RDWR);
	if (lockfd < 0) {
		scds_syslog_debug(DBG_LEVEL_HIGH,
			"Open lock file return %d.", lockfd);

		/* lock file was not there, assume sap is not started */
		if (errno == ENOENT) {
			scds_syslog_debug(DBG_LEVEL_HIGH,
				"Lock file is not there.");
			return (0);
		}
		(void) sprintf(internal_err_str,
			"Failed to open SAP lock file %s", lock_file);
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		return (-1);
	}
	scds_syslog_debug(DBG_LEVEL_HIGH,
		"Open lock file return %d.", lockfd);

	(void) memset((char *)&fl, 0, sizeof (fl));
	fl.l_type = F_WRLCK;
	n = fcntl(lockfd, F_SETLK, &fl);
	if (n != 0) { /* can't lock the lock file */
		scds_syslog_debug(DBG_LEVEL_HIGH,
			"Failed to lock sap lock file with fcntl(). "
			"SAP is probably already up.");
		m = creat(statefile, 0600);
		scds_syslog_debug(DBG_LEVEL_HIGH,
			"Create state file return %d.", m);

		if (m < 0) {
			/*
			 * SCMSGS
			 * @explanation
			 * If SAP is brought up outside the control of Sun
			 * Cluster, HA-SAP will create the state file to
			 * signal the stop method not to try to stop sap via
			 * Sun Cluster. Now if SAP was brought up outside of
			 * Sun Cluster, and the state file creation failed,
			 * then the SAP resource might end in the stop-failed
			 * state when Sun Cluster tries to stop SAP.
			 * @user_action
			 * This is an internal error. No user action needed.
			 * Save the /var/adm/messages from all nodes. Contact
			 * your authorized Sun service provider.
			 */
			scds_syslog(LOG_ERR,
				"Failed to create sap state file %s:%s "
				"Might put sap resource in stop-failed state.",
				statefile, strerror(errno));
		}
		scds_syslog_debug(DBG_LEVEL_HIGH,
			"SAP is up, created state file %s.", statefile);
		rc = 1;
	} else {
		scds_syslog_debug(DBG_LEVEL_HIGH,
			"Can lock the sap lock file which "
			"means sap is not running.");
		rc = 0;
	}
	(void) close(lockfd);
	return (rc);
}


/*
 * This routine checks the existence of the state file.
 * Returns 0 if state file exists, returns 1 if state file doesn't exit.
 * Returns -1 if internal error occurred.
 */

int
check_state_file(const char *rs_name)
{
	char    	statefile[MAXPATHLEN];
	struct	stat	statbuf;
	int		rc;
	char		internal_err_str[INT_ERR_MSG_SIZE];

	rc = sprintf(statefile, "%s/running.%s", TEST_LOCK_DIR, rs_name);
	if (rc == -1) {
		(void) sprintf(internal_err_str,
			"Failed to form state file path");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		return (rc);
	}

	if (stat(statefile, &statbuf) == 0) {
		/* State file exists */
		(void) unlink(statefile);
		rc = 0;
	} else { /* state file doesn't exist */
		rc = 1;
	}
	return (rc);
}


/*
 * This routine starts up saposcol process under PMF with tag 'saposcol.svc'.
 * If the process is already started up under PMF, then it will exit 0.
 * Otherwise, if saposcol is started up outside of PMF, then this routine
 * will first terminate the saposcol process, then starts it up under PMF.
 * This routine will first lock the saposcol lock file '/tmp/sap/lock.saposcol'
 * before it does any checking and starting up the saposcol process. Also,
 * it will increase the counter in the lock file as a way to know how many
 * sap resource needs the saposcol process.
 * Return 0 if saposcol is brought up under PMF or is already brought up under
 * PMF by some other resource. Return 1 if error occured.
 */
int
start_saposcol(scds_handle_t handle, char *sapsid)
{
	int			rc, exit_code;
	char			saposcolcmd[SCDS_ARRAY_SIZE],
				killsaposcol[SCDS_ARRAY_SIZE],
				saposcolpmf[SCDS_ARRAY_SIZE],
				checkcmd[SCDS_ARRAY_SIZE];
	char			*bin_path = NULL;
	int			start_timeout;
	char			internal_err_str[INT_ERR_MSG_SIZE];
	flock_t			fl;
	int			lockfd;
	char			oscolfile[MAXPATHLEN];
	int			oscol_num;
	char			oscol_num_buf[10];

	start_timeout = scds_get_rs_start_timeout(handle);

	/* First check/start saposcol process under PMF tag 'saposcol.svc'. */
	rc = snprintf(saposcolcmd, MAXPATHLEN,
		"/usr/cluster/bin/pmfadm -q %s",
		SAPOSCOL_PMF_TAG);
	if (rc == -1) {
		(void) sprintf(internal_err_str,
			"Failed to form pmfadm -q command");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		return (1);
	}

	/*
	 * Since different sap instances from multiple RGs could be trying
	 * to start up saposcol at the same time, need to put locks on
	 * this so no two pmfadm can check and startup saposcol
	 * process at the same time.
	 */
	if (mkdirp(TEST_LOCK_DIR, 0755) != 0) {
		if (errno != EEXIST) {
			(void) sprintf(internal_err_str,
				"Failed to create directory %s",
				TEST_LOCK_DIR);
			scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
				internal_err_str);
			return (1);
		}
	}
	rc = sprintf(oscolfile, "%s/lock.saposcol", TEST_LOCK_DIR);
	if (rc == -1) {
		(void) sprintf(internal_err_str,
			"Failed to form saposcol lock file");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		return (1);
	}

	lockfd = open(oscolfile, O_RDWR|O_CREAT, 0755);
	scds_syslog_debug(DBG_LEVEL_HIGH,
		"Open lock file %s return %d.", oscolfile, lockfd);
	if (lockfd < 0) {
		(void) sprintf(internal_err_str,
			"Failed to open saposcol lock file %s:%s",
			oscolfile, strerror(errno));
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
			internal_err_str);
		return (1);
	}

	/* lock the lock file */
	(void) memset((char *)&fl, 0, sizeof (fl));
	fl.l_type = F_WRLCK;
	if (fcntl(lockfd, F_SETLKW, &fl) < 0) {
		(void) sprintf(internal_err_str,
			"Failed to lock file %s:%s", oscolfile,
			strerror(errno));
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		(void) close(lockfd);
		return (1);
	}

	/* Now check and start saposcol process */
	rc = system(saposcolcmd);
	if (rc == -1) {
		scds_syslog(LOG_ERR, "Cannot execute %s: %s.",
			saposcolcmd, strerror(errno));
		(void) close(lockfd);
		return (1);
	}

	if (WIFEXITED((uint_t)rc) && (WEXITSTATUS((uint_t)rc) == 0)) {
		/* saposcol is registered and running under PMF */
		scds_syslog_debug(DBG_LEVEL_LOW,
		    "saposcol is running under pmf with tag saposcol.svc.");
	} else if (WIFEXITED((uint_t)rc) && (WEXITSTATUS((uint_t)rc) == 1)) {
		/* saposcol is not registered and running under PMF */
		if ((bin_path = get_bin_path(sapsid, B_FALSE)) == NULL) {
			(void) sprintf(internal_err_str,
			"Failed to retrieve sap bin path: "
			"Output error encounted");
		    scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
				internal_err_str);
		    (void) close(lockfd);
		    return (1);
		}
		/*
		 * form command to check and see if saposcol is already
		 * running outside of PMF, if yes, kill it first
		 */
		rc = snprintf(checkcmd, sizeof (checkcmd),
			"/bin/pgrep -x saposcol > /dev/null");
		if (rc == -1) {
			(void) sprintf(internal_err_str,
				"Failed to form command to check on saposcol"
				"process: Output error encounted");
			scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
				internal_err_str);
			if (bin_path != NULL) free(bin_path);
			(void) close(lockfd);
			return (1);
		}

		rc = system(checkcmd);

		/* Kill saposcol first if it already runs on this node. */
		if (WIFEXITED((uint_t)rc) && (WEXITSTATUS((uint_t)rc) != 1)) {
			/*
			 * SCMSGS
			 * @explanation
			 * The SAP OS collector process is running outside of
			 * the control of Sun Cluster. HA-SAP will terminate
			 * it and restart it under the control of Sun Cluster.
			 * @user_action
			 * Informational message. No user action needed.
			 */
			scds_syslog(LOG_INFO, "Process sapsocol is already "
				"running outside of Sun Cluster. Will terminate"
				" it now, and restart it under Sun Cluster.");

			/* form command to kill saposcol under hatimerun. */
			rc = snprintf(killsaposcol, sizeof (killsaposcol),
				"%s/%s -k", bin_path, SAP_PRG_SAPOSCOL);

			if (rc == -1) {
			    (void) sprintf(internal_err_str,
				"Failed to form command to terminate saposcol"
				" process: Output error encountered");
			    scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
				internal_err_str);
			    if (bin_path != NULL) free(bin_path);
			    (void) close(lockfd);
			    return (1);
			}

			rc = scds_timerun(handle, killsaposcol,
				start_timeout, SIGKILL, &exit_code);

			switch (rc) {
				case SCHA_ERR_NOERR: break;
				case SCHA_ERR_INVAL:
				    (void) sprintf(internal_err_str,
					"Failed to terminate saposcol process"
					" under hatimerun: "
					"Output error encounted");
				    scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
					internal_err_str);
				    if (bin_path != NULL) free(bin_path);
				    (void) close(lockfd);
				    return (1);
				case SCHA_ERR_TIMEOUT:
				    (void) sprintf(internal_err_str,
					"Terminating saposcol process"
					" under hatimerun times out");
				    scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
					internal_err_str);
				    if (bin_path != NULL) free(bin_path);
				    (void) close(lockfd);
				    return (1);
				default: break;
			}
		}

		saposcolcmd[0] = '\0';
		/* form command to start saposcol process */
		rc = snprintf(saposcolcmd, sizeof (saposcolcmd),
			"%s/%s", bin_path, SAP_PRG_SAPOSCOL);

		if (rc == -1) {
			(void) sprintf(internal_err_str,
				"Failed to form command %s to start saposcol:"
				"Output error encounted", saposcolcmd);
			scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
				internal_err_str);
			if (bin_path != NULL) free(bin_path);
			(void) close(lockfd);
			return (1);
		}

		/*
		 * start saposcol under PMF with tag saposcol.svc
		 * Will retry infinite times if the process died.
		 */
		rc = snprintf(saposcolpmf, MAXPATHLEN,
			"/usr/cluster/bin/pmfadm -c %s -n -1 %s",
			SAPOSCOL_PMF_TAG, saposcolcmd);
		if (rc == -1) {
			(void) sprintf(internal_err_str,
				"Failed to form pmfadm -c command");
			scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
				internal_err_str);
			if (bin_path != NULL) free(bin_path);
			(void) close(lockfd);
			return (1);
		}

		scds_syslog(LOG_NOTICE, "Starting %s.", "SAP OS Collector");
		rc = scds_timerun(handle, saposcolpmf,
			start_timeout, SIGTERM, &exit_code);
		if (rc != SCHA_ERR_NOERR) {
			if (rc == SCHA_ERR_TIMEOUT) {
				/* hatimerun time out */
				/*
				 * SCMSGS
				 * @explanation
				 * Starting up the SAP OS collector process
				 * under the control of Process Monitor
				 * facility times out. This might happen under
				 * heavy system load.
				 * @user_action
				 * You might consider increase the start
				 * timeout value.
				 */
				scds_syslog(LOG_ERR,
					"Starting up saposcol process under "
					"PMF times out.");
			} else {
				(void) sprintf(internal_err_str,
				    "Failed to start saposcol process under "
				    "PMF with command %s", saposcolpmf);
				scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
					internal_err_str);
				if (bin_path != NULL) free(bin_path);
				(void) close(lockfd);
				return (1);
			}
		} else {
			/*
			 * SCMSGS
			 * @explanation
			 * The SAP OS collector process is started
			 * successfully under the control of the Process
			 * monitor facility.
			 * @user_action
			 * Informational message. No user action needed.
			 */
			scds_syslog(LOG_INFO,
			    "Started saposcol process under PMF successfully.");
		}
	    } /* saposcol was not running under PMF */
	    else { /* pmfadm -q went wrong */
		scds_syslog(LOG_ERR, "Cannot execute %s: %s.", saposcolcmd,
			strerror(errno));
		return (1);
	    }

	/*
	 * update number of locks and release the lock on the lock_file
	 * so that other resource can check and startup saposcol process
	 */
	scds_syslog_debug(DBG_LEVEL_HIGH,
		"saposcol file descriptor = %d", lockfd);

	/* increase the lock counter by 1 */
	oscol_num_buf[0] = '\0';
	rc = read(lockfd, oscol_num_buf, sizeof (oscol_num_buf));
	scds_syslog_debug(DBG_LEVEL_HIGH,
		"read from lock file return <%d> <%s>",
		rc, oscol_num_buf);

	if (rc == -1) { /* read() should return positive number */
		(void) sprintf(internal_err_str,
			"Failed to read saposcol lock file %s:%s",
			oscolfile, strerror(errno));
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		if (bin_path != NULL) free(bin_path);
		(void) close(lockfd);
		return (1);
	} else if (rc < 1) { /* first one to bring saposcol up */
		scds_syslog_debug(DBG_LEVEL_LOW, "first one starting saposcol");
		oscol_num = 0;
		} else { /* read in the lock counter */
			oscol_num = atoi(oscol_num_buf);
		}

	/* now add i to lock counter */
	oscol_num = oscol_num + 1;

	rc = sprintf(oscol_num_buf, "%d", oscol_num);
	scds_syslog_debug(DBG_LEVEL_HIGH, "oscol-num-buf=%s,"
		" strlen(oscol_num_buf)=%d", oscol_num_buf, oscol_num);

	if (pwrite(lockfd, oscol_num_buf, (strlen(oscol_num_buf)+1), 0) < 0) {
		(void) sprintf(internal_err_str,
			"Failed to write to saposcol lock file");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		if (bin_path != NULL) free(bin_path);
		(void) close(lockfd);
		return (1);
	}

	/* closing the lock file will also unlock it */
	if (bin_path != NULL) free(bin_path);
	(void) close(lockfd);
	return (0);
}


/*
 * This routine decreases the number of locks on the saposcol lock file.
 * Stop saposcol process if this is the last instance that uses it (if
 * number of locks on the saposcol lock file = 1). If not the last
 * one that uses saposcol, then just decreases the number of locks on
 * the lock file.
 * Return 1 if error occured, otherwise return 0
 */
int
stop_saposcol(scds_handle_t handle, char *sapsid)
{

	char	oscolfile[MAXPATHLEN];
	int	rc, lockfd, oscol_num, n;
	char	saposcolcmd[SCDS_ARRAY_SIZE],
		saposcolpmf[SCDS_ARRAY_SIZE];
	char	internal_err_str[INT_ERR_MSG_SIZE];
	flock_t	fl;
	char	*bin_path = NULL;
	int	exit_code = 0;
	char	oscol_num_buf[10];

	rc = sprintf(oscolfile, "%s/lock.saposcol", TEST_LOCK_DIR);
	if (rc == -1) {
		(void) sprintf(internal_err_str,
			"Failed to form saposcol lock file");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		return (1);
	}

	lockfd = open(oscolfile, O_RDWR);
	if (lockfd < 0) {
		if (lockfd == ENOENT) {
			scds_syslog_debug(DBG_LEVEL_LOW,
				"saposcol lock file %s not found.", oscolfile);
		} else {
			scds_syslog_debug(DBG_LEVEL_LOW,
				"Failed to open file %s:%s.",
				oscolfile, strerror(errno));
		}
		return (0);
	}

	(void) memset((char *)&fl, 0, sizeof (fl));
	fl.l_type = F_WRLCK;
	n = fcntl(lockfd, F_SETLKW, &fl);
	if (n != 0) { /* can't lock the lock file */
		(void) sprintf(internal_err_str,
			"Failed to lock file %s with fcntl():%s",
			oscolfile, strerror(errno));
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		(void) close(lockfd);
		return (1);
	}

	rc = read(lockfd, oscol_num_buf, sizeof (oscol_num_buf));
	if (rc == 0) {
		/*
		 * Usually it's something wrong, should at least be 1
		 * but if the lock file was never created (start didn't
		 * work, then this can happen.
		 */
		scds_syslog_debug(DBG_LEVEL_HIGH, "read %s return %d",
			oscolfile, rc);

		(void) sprintf(internal_err_str,
			"Failed to determine whether saposcol "
			"process is running or not");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		(void) close(lockfd);
		return (0);
	} else if (rc < 0) {
		/* something is wrong */
		(void) sprintf(internal_err_str,
			"Failed to open lock file %s:%s",
			oscolfile, strerror(errno));
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		(void) close(lockfd);
		return (1);
	    }

	oscol_num = atoi(oscol_num_buf);
	if (oscol_num < 1) {
		/*
		 * something wrong, should at least be 1,
		 * could be start method failed and saposcol didn't get
		 * started. Just print out msg under debug.
		 */
		scds_syslog_debug(DBG_LEVEL_HIGH,
			"No saposcol process running.");
		(void) close(lockfd);
		return (0);
	} else if (oscol_num == 1) {
		/* stop saposcol process if this is the last lock */

		/* stop monitoring the process from PMF */
		rc = snprintf(saposcolpmf, sizeof (saposcolpmf),
			"/usr/cluster/bin/pmfadm -s %s", SAPOSCOL_PMF_TAG);
		if (rc == -1) {
			(void) sprintf(internal_err_str,
				"Failed to form pmfadm -s command");
			scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
				internal_err_str);
			(void) close(lockfd);
			return (1);
		}
		rc = scds_timerun(handle, saposcolpmf,
			scds_get_rs_stop_timeout(handle), SIGTERM, &exit_code);

		switch (rc) {
			case SCHA_ERR_NOERR: break;
			case SCHA_ERR_INVAL:
				(void) sprintf(internal_err_str,
				    "Failed to stop monitoring saposcol "
				    "process with PMF");
				scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
					internal_err_str);
				(void) close(lockfd);
				return (1);
			case SCHA_ERR_TIMEOUT:
				/*
				 * SCMSGS
				 * @explanation
				 * Stopping monitoring the SAP OS collector
				 * process under the control of Process
				 * Monitor facility times out. This might
				 * happen under heavy system load.
				 * @user_action
				 * You might consider increase the monitor
				 * stop time out value.
				 */
				scds_syslog(LOG_ERR,
				    "Stop monitoring saposcol under PMF "
				    "times out.");
				(void) close(lockfd);
				return (1);
			default: break;
		}

		/* now stop saposcol process */
		bin_path = get_bin_path(sapsid, B_FALSE);
		rc = snprintf(saposcolcmd, sizeof (saposcolcmd),
			"%s/%s -k %s", bin_path,
			SAP_PRG_SAPOSCOL, SAP_STDOUT_REDIR);

		if (rc == -1) {
			(void) sprintf(internal_err_str,
				"Failed to form command %s to stop saposcol:"
				"Output error encounted", saposcolcmd);
			scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
				internal_err_str);
			if (bin_path != NULL) free(bin_path);
			(void) close(lockfd);
			return (1);
		}

		scds_syslog(LOG_NOTICE, "Stopping %s.", "SAP OS Collector");
		rc = scds_timerun(handle, saposcolcmd,
			scds_get_rs_stop_timeout(handle),
			SIGKILL, &exit_code);
		switch (rc) {
			case SCHA_ERR_NOERR:
				if (exit_code != 0) {
				    (void) sprintf(internal_err_str,
					"Failed to terminate "
					"saposcol process via SAP utility "
					"saposcol");
				    scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
					internal_err_str);
				}
				break;
			case SCHA_ERR_INVAL:
				(void) sprintf(internal_err_str,
				    "Failed to terminate saposcol process via "
				    "hatimerun");
				scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.",
					internal_err_str);
				break;
			case SCHA_ERR_TIMEOUT:
				/*
				 * SCMSGS
				 * @explanation
				 * Stopping the SAP OS collector process under
				 * the control of Process Monitor facility
				 * times out. This might happen under heavy
				 * system load.
				 * @user_action
				 * You might consider increase the stop time
				 * out value.
				 */
				scds_syslog(LOG_ERR,
				    "Stop saposcol under PMF times out.");
				break;
			default: break;
		}
	    }

	/*
	 * call pmfadm with SIGKILL to stop saposcol process. If it's already
	 * stopped, this has no side effect.
	 */
	saposcolpmf[0] = '\0';
	rc = snprintf(saposcolpmf, sizeof (saposcolpmf),
		"/usr/cluster/bin/pmfadm -s %s SIGKILL",
		SAPOSCOL_PMF_TAG);
	if (rc == -1) {
		(void) sprintf(internal_err_str,
			"Failed to form pmfadm -s <tag> SIGKILL command");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		(void) close(lockfd);
		return (1);
	}

	/* now decrease the saposcol counter in lock file */
	oscol_num = oscol_num - 1;
	rc = sprintf(oscol_num_buf, "%d", oscol_num);
	if (rc == -1) {
		(void) sprintf(internal_err_str,
			"Failed to convert saposcol lock counter to ascii");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		(void) close(lockfd);
		if (bin_path != NULL) free(bin_path);
		return (1);
	}

	if (pwrite(lockfd, oscol_num_buf, (strlen(oscol_num_buf)+1), 0) < 0) {
		(void) sprintf(internal_err_str,
			"Failed to write to saposcol lock file");
		scds_syslog(LOG_ERR, "INTERNAL ERROR: %s.", internal_err_str);
		if (bin_path != NULL) free(bin_path);
		(void) close(lockfd);
		return (1);
	}

	/* closing the lock file will also unlock it */
	(void) close(lockfd);
	return (0);
}
