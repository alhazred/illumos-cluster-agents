/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 */

#ifndef	_SAP_UTIL_H
#define	_SAP_UTIL_H

#pragma ident	"@(#)sap_util.h	1.24	07/06/06 SMI"

#ifdef __cplusplus
extern "C" {
#endif

#include <sys/wait.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <syslog.h>
#include <stdlib.h>
#include <pwd.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <errno.h>
#include <signal.h>
#include <sys/time.h>
#include <sys/un.h>
#include <netdb.h>
#include <libgen.h>
#include <scha.h>
#include <libdsdev.h>
#include <sys/param.h>
#include <sys/fcntl.h>
#include <rgm/libdsdev.h>
#include <fcntl.h>
#include <sys/elf.h>
#include <sys/utsname.h>
#include <libelf.h>
#include <gelf.h>
#include <sys/systeminfo.h>
#include <locale.h>
#include <libintl.h>

/* debug level */
#define	DBG_LEVEL_HIGH		9
#define	DBG_LEVEL_MED		5
#define	DBG_LEVEL_LOW		1

/* Return codes from the main sap methods */
#define	SAP_OK			0
#define	SAP_ERROR		1

#define	SCDS_ARRAY_SIZE		1024
#define	INT_ERR_MSG_SIZE	1024
#define	ISALIST_SIZE		1024
#define	ARCH_ARRAY_SIZE		8

/* An individual probe binary return status */
#define	SAP_PROBE_OK		0
#define	SAP_PROBE_ERROR		1
#define	SAP_PROBE_TIMEOUT		2

#define	SAP_PROBE_SUCCESS_STRING	"SUCCESS"
#define	SAP_PROBE_FAILURE_STRING	"FAILURE"

/* Data service provided binary to start R3 */
#define	SAP_CMD_R3TRANS		"R3trans -d"
#define	SAP_CMD_LGTST		"lgtst"
#define	SAP_CMD_CLEANIPC	"cleanipc"
#define	SAP_CMD_CLEANIPC_REM	"remove"
#define	SAP_CMD_STARTSAP	"startsap"

#define	SAP_PRG_R3TRANS		"R3trans"
#define	SAP_PRG_SAPOSCOL	"saposcol"
#define	SAP_PRG_STARTSAP	"sapstart"
#define	SAP_PRG_STOPSAP		"stopsap"
#define	SAP_PRG_WRAPPER		"sapstart_wrapper"

/* SAP directory */
#define	SAP_DIR_USRSAP		"/usr/sap"
#define	SAP_DIR_WORK		"work"
#define	SAP_DIR_SYS		"SYS/exe/run"
#define	SAP_LOCK_FILE		"startup_lockfile"

/* output redirect */
/* #define SAP_STDOUT_REDIR	">/dev/null 2>&1"  */
#define	SAP_STDOUT_REDIR	">/dev/null"

/* in seconds */
#define	SAP_PROBE_MIN_TIMEOUT		3
#define	PROBE_DB_TIMEOUT		30
#define	SAP_EXT_PROBE_TIMEOUT   "Probe_timeout"

/* dir for files that has the pid for message server and dispatcher */
#define	SAP_PID_FILE_DIR	"/var/run/cluster"

/* dir for sap state file */
/* #define TEST_LOCK_DIR		"/tmp/sap" */
#define	TEST_LOCK_DIR 		"/var/run/cluster"

/* script to retrieve dispatcher pid */
#define	SAP_CMD_PROCPROBE	"sap_get_disp_pid"

/* PMF tag name for saposcol */
#define	SAPOSCOL_PMF_TAG	"saposcol.svc"

char
*get_admuser_name(char *sapsid);

char
*get_bin_path(char *sapsid, boolean_t print_messages);

char
*get_script_path(char *sapsid, boolean_t print_messages);

int
probe_db(scds_handle_t handle, char *sapsid, int probe_to, int *probe_rc);

int
check_sap_file(boolean_t script, char *sapsid, char *progname,
	scds_hasp_status_t hasp_status, boolean_t print_messages);

void
start_dev_system(char *devsid, char *startsap_dev_script);

int
start_saposcol(scds_handle_t handle, char *sapsid);

int
stop_saposcol(scds_handle_t handle, char *sapsid);

int
is_sap_running(char *lock_file, const char *rs_name);

int
read_in_ms_pid(int *ms_pid, const char *rname);

int
read_in_disp_pid(int *disp_pid, const char *rname);

int
retrieve_ms_pid(char *sapsid, const char *rname);

int
retrieve_disp_pid(const char *rtbasedir, char *sapsid, char *service,
	char *sysnum, const char *rname);
int
check_state_file(const char *rs_name);

int machine_architecture();

int file_elf_class(char *file);

int correct_saposcol(char *sapsid, boolean_t print_messages);

#ifdef __cplusplus
}
#endif

#endif /* _SAP_UTIL_H */
