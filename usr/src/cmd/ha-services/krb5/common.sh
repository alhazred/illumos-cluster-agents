#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

# ident	"@(#)common.sh	1.7	07/06/06 SMI"
#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

# capture the validate's method directory
BINDIR=`/bin/dirname $0`

# default kdc.conf file path
KDCCONF=/etc/krb5/kdc.conf

# default db path for krb5kdc
PRINCIPALDB=/var/krb5/principal

#default resolv.conf path
RESOLVCONF=/etc/resolv.conf

# default smf service names for krb5kdc, kadmind
FMRI[0]=svc:/network/security/krb5kdc:default
FMRI[1]=svc:/network/security/kadmin:default
fmri_cnt=2

syslog_tag="SC[SUNW.krb5,$0]"

export PATH=/usr/cluster/lib/sc:/usr/cluster/bin:/usr/sbin:/usr/proc/bin:$PATH

#
# Function for parsing arguments
#
function parse_args # [args ...]
{
	typeset opt

	while getopts 'cur:x:g:R:T:G:' opt
	do
		case "$opt" in

		R)
		# Name of the sckrb5 resource.
		RESOURCE_NAME=$OPTARG
		;;

		G)
		# Name of the resource group in which the resource is
		# configured.
		RESOURCEGROUP_NAME=$OPTARG
		;;

		T)
		# Name of the resource type.
		RESOURCETYPE_NAME=$OPTARG
		;;

		r)      
		# We are not accessing any system defined property.
		# So, this is a no-op
		;;

		g)
		# This is a no-op as we are not bothered about 
		# Resource group properties
		;;

		c)
		# This is a no-op as this is just a flag which indicates
		# that the validate method is being called while
		# creating the resource.
		;;

		u)
		# This is a flag to indicate the updating of property 
		# of an existing resource.
		UPDATE_PROPERTY=1
		;;

		x)
		;;

		*)
		# SCMSGS
		# @explanation
		# The option provided is invalid.
		# @user_action
		# Possible options are: c, u, r <arg>, x <arg>, g <arg>,
		# R <arg>, T <arg>, and G <arg>.
		scds_syslog -p error -t $syslog_tag -m \
			"ERROR: Option -%s unknown" "${OPTARG}"
		exit 1
		;;

		esac
	done
}

#
# This function will check and rely on the presence or absence
# of /lib/svc/share/smf_include.sh, since it contains an smf_present
# check.
#
function smf_running
{
	SMFLIB=/lib/svc/share/smf_include.sh
	if [ -r $SMFLIB ]; then
		. $SMFLIB
		smf_present
		return $?
	else
		return 1
	fi
}

#
# This function sets the resource status for the start and stop methods.
#
function set_status
{
	typeset rstatus="${1:-UNKNOWN}"
	typeset rmsg=${2:-""}
	typeset prev_status

	prev_status=`scha_resource_get -O $rstatus -R "$RESOURCE_NAME" -G "$RESOURCEGROUP_NAME"`

	# WARNING: Assumption here is that all previous scha_resource_setstatus
	# commands did NOT use the -m option. If it was ever used, more
	# parsing of prev_status is required here

	if [ "$prev_status" = "$rstatus" ]; then
		return 0
	fi

	if [ -n "${rmsg}" ]; then
		scha_resource_setstatus -s $rstatus -m "$rmsg" \
			-R $RESOURCE_NAME -G $RESOURCEGROUP_NAME
	else
		scha_resource_setstatus -s $rstatus \
			-R $RESOURCE_NAME -G $RESOURCEGROUP_NAME
	fi
}

# Parse the arguments that have been passed to this method
parse_args "$@"

# We need to know the full path for the validate method which resides
# in the directory <RT_BASEDIR>. Get this from the RT_BASEDIR property of the
# resource type.
RT_BASEDIR=`scha_resource_get -O RT_BASEDIR -R $RESOURCE_NAME \
	-G $RESOURCEGROUP_NAME`

