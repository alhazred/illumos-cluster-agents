#!/bin/ksh -p
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)krb5_validate.ksh	1.8	07/06/06 SMI"
#
# Validate method for HA Kerberos KDC server.
#
# ex: When the resource is being created command args will be
#   
# krb5_validate -c -R <..> -G <...> -T <..> -r <sysdef-prop=value>... 
#       -x <extension-prop=value>.... -g <resourcegroup-prop=value>....
#
# when the resource property is being updated
#
# krb5_validate -u -R <..> -G <...> -T <..> -r <sys-prop_being_updated=value>
#   OR
# krb5_validate -u -R <..> -G <...> -T <..> -x <extn-prop_being_updated=value>
#
#include_common_lib
#

#############################
# MAIN
#############################

NSSCONF=/etc/nsswitch.conf

# Set lang to standard english to avoid confusion
export LC_ALL=C

UPDATE_PROPERTY=0

# Check presence of kdc start/stop script
start_stop_cmd_prog=$(echo "$BINDIR/kdc")
if [[ ! -f $start_stop_cmd_prog || ! -x $start_stop_cmd_prog ]]; then
	scds_syslog -p error -t $syslog_tag -m \
		"File %s is missing or not executable." "${start_stop_cmd_prog}"
	exit 1
fi

# Check presence of kdc daemon probe script
probe_cmd_prog=$(echo "$BINDIR/complete_probe")
if [[ ! -z $probe_cmd_prog ]]; then
	if [[ ! -f $probe_cmd_prog || ! -x $probe_cmd_prog ]]; then
		scds_syslog -p error -t $syslog_tag -m \
			"File %s is missing or not executable." \
			"${probe_cmd_prog}"
		exit 1
	fi
fi

# Check presence of resolv.conf
if [[ ! -s $RESOLVCONF || ! -r $RESOLVCONF ]]; then
	# SCMSGS
	# @explanation
	# The specified file does not exist.
	# @user_action
	# Refer to the file's or associated service's man page on how to create
	# this file.
	scds_syslog -p error -t $syslog_tag -m \
		"File %s is missing, exiting." "${RESOLVCONF}"
	exit 1
fi

# Check for valid nsswitch.conf file
if [[ ! -s $NSSCONF || ! -r $NSSCONF ]]; then
	scds_syslog -p error -t $syslog_tag -m \
		"File %s is missing, exiting." "${NSSCONF}"
	exit 1
fi
found=0
while read db source
do
	case "$db" in
		'hosts:'|'ipnodes:')
			echo $source | grep dns > /dev/null 2>&1
			if [[ $? -eq 0 ]]; then
				found=1
				break
			fi
			;;
	esac
done <$NSSCONF
if [[ $found -ne 1 ]]; then
	# SCMSGS
	# @explanation
	# The /etc/nsswitch.conf(4) file does not have a valid "dns" source
	# specified.
	# @user_action
	# /etc/nsswitch.conf(4) must have a "dns" source under the "hosts" or
	# "ipnodes" database entries.
	scds_syslog -p error -t $syslog_tag -m \
		"Must have a valid %s file with dns specified." "${NSSCONF}"
	exit 1
fi

#
# Check if kerberos daemons have been DISABLED under smf(5).
# We want to the run the smf(5) check only after the resource
# has been created/added. The smf(5) check is bypassed the very
# first time the resource is created, so that INIT has had a
# chance to run and disable the daemons under smf(5), first.
#
resource_state=`scha_resource_get -O RESOURCE_STATE -R $RESOURCE_NAME`
if [[ $? -eq 0 ]]; then
	if smf_running; then
		i=0
		while ((i < fmri_cnt)); do
			STATUS=`svcs -H -o STATE ${FMRI[i]}`
			if [[ $STATUS != disabled ]]; then
				# SCMSGS
				# @explanation
				# The HA-KDC service has not been disabled in
				# SMF.  It cannot remain in SMF control because
				# of fault management conflict.
				# @user_action
				# This is an internal error.  No user action
				# needed.  Save the /var/adm/messages from all
				# nodes.  Contact your authorized Sun service
				# provider.
				scds_syslog -p error -t $syslog_tag -m \
					"%s has not been disabled under smf(5), exiting." "${FMRI[i]}"
				exit 1
			fi
			((i += 1))
		done
	fi
fi

# Check presence of hasp_check binary
hasp_check_prog=$(echo "$BINDIR/hasp_check")
if [[ -f $hasp_check_prog && -x $hasp_check_prog ]]; then
	$BINDIR/hasp_check "$@"
	hasp_status=$?
else
	# the binary doesn't exist so we cannot call it
	hasp_status=2
fi

case "$hasp_status" in

	1) 
	# SCMSGS
	# @explanation
	# Was not able to determine the cause of failure due to an internal
	# error to hasp_check. 
	# @user_action
	# This is an internal error.  No user action needed.  Save the
	# /var/adm/messages from all nodes.  Contact your authorized Sun
	# service provider.
	scds_syslog -p error -t $syslog_tag -m \
		"Internal Error. Failed to check status of SUNW.HAStoragePlus resource."
	exit 1
	;;

	2)
	scds_syslog -p info -t $syslog_tag -m \
		"This resource does not depend on any SUNW.HAStoragePlus resources. Proceeding with normal checks."
	;;

	3)
	scds_syslog -p error -t $syslog_tag -m \
		"One or more of the SUNW.HAStoragePlus resources that this resource depends on is in a different resource group. Failing validate method configuration checks."
	exit 1
	;;

	4)
	scds_syslog -p error -t $syslog_tag -m \
		"One or more of the SUNW.HAStoragePlus resources that this resource depends on is not online anywhere. Failing validate method."
	exit 1
	;;

	5)
	scds_syslog -p info -t $syslog_tag -m \
		"All the SUNW.HAStoragePlus resources that this resource depends on are not online on the local node. Skipping the checks for the existence and permissions of the start/stop/probe commands."
	exit 0
	;;

	6)
	scds_syslog -p info -t $syslog_tag -m \
		"All the SUNW.HAStoragePlus resources that this resource depends on are online on the local node. Proceeding with the checks for the existence and permissions of the start/stop/probe commands."
	;;

	*)
	# SCMSGS
	# @explanation
	# hasp_check returned an unknown status code.
	# @user_action
	# This is an internal error.  No user action needed.  Save the
	# /var/adm/messages from all nodes.  Contact your authorized Sun
	# service provider.
	scds_syslog -p error -t $syslog_tag -m \
		"Unknown status code %s." "${hasp_status}"
	exit 1
	;;

esac

# Check presence of valid kdc.conf
if [[ -s $KDCCONF ]]; then
	# Make sure kdc.conf is configured -
	# We want to check for "_default_realm_"
  	# with any number of preceeeding or
	# trailing underscores or whitespaces.
	grep '^[ 	]*_[_]*default_realm_' \
		$KDC_CONF_DIR/kdc.conf > /dev/null 2>&1

	kdc_configured=$?
	if [[ $kdc_configured -eq 0 ]]; then
		# SCMSGS
		# @explanation
		# The specified file has not been configured.
		# @user_action
		# Look at the file's man page to determine how to properly
		# configure the file.
		scds_syslog -p error -t $syslog_tag -m \
			"File %s is not configured, exiting." "${KDCCONF}"
		exit 1
	fi
else
	scds_syslog -p error -t $syslog_tag -m \
		"File %s is missing, exiting." "${KDCCONF}"
	exit 1
fi

# Check presence of kerberos database
if [[ ! -s $PRINCIPALDB ]]; then
	# SCMSGS
	# @explanation
	# The /var/krb5/principal database is missing or of zero length.
	# @user_action
	# Verify that the principal database has been properly configured and
	# that the /etc/krb5/krb5.conf and /etc/krb5/kdc.conf files have been
	# specified correctly.
	scds_syslog -p error -t $syslog_tag -m \
		"Kerberos database %s is missing, exiting." "${PRINCIPALDB}"
	exit 1
fi

#
# Ensure that the logical host names have been added to the server's
# keytab file
#
kadm5_keytab=$(echo "/etc/krb5/kadm5.keytab")
if [[ ! -s $kadm5_keytab ]]; then
	scds_syslog -p error -t $syslog_tag -m \
		"File %s is missing, exiting." "${kadm5_keytab}"
	exit 1
else

	hostname=$(echo `$BINDIR/gethostnames $@`)

	if [[ -z $hostname ]]; then
		scds_syslog -p error -t $syslog_tag -m \
			"The network resource has not been configured."
		exit 1
	else
		hostname=$(echo "$hostname"|cut -d "," -f1|tr '[A-Z]' '[a-z]')
	fi
	KLIST_OUT=`klist -k $kadm5_keytab`

	# Determine if the logical kadmin principal exists in $kadm5_keytab
	echo $KLIST_OUT | grep "kadmin/$hostname" > /dev/null 2>&1
	if [[ $? -ne 0 ]]; then
		# SCMSGS
		# @explanation
		# The logical host name associated with the "kadmin" service
		# principal does not exist in the /etc/krb5/kadm5.keytab file.
		# @user_action
		# Create and/or add the "kadmin/<logical host name>" service
		# principal's keys in the /etc/krb5/kadm5.keytab file.  Note:
		# that the logical host name must be fully qualified.
		scds_syslog -p error -t $syslog_tag -m \
			"Logical kadmin principal does not exist in %s." \
			"${kadm5_keytab}"
		exit 1
	fi

	# Determine if the logical changepw principal exists in $kadm5_keytab
	echo $KLIST_OUT | grep "changepw/$hostname" > /dev/null 2>&1
	if [[ $? -ne 0 ]]; then
		# SCMSGS
		# @explanation
		# The logical host name associated with the "changepw" service
		# principal does not exist in the /etc/krb5/kadm5.keytab file.
		# @user_action
		# Create and/or add the "changepw/<logical host name>" service
		# principal's keys in the /etc/krb5/kadm5.keytab file.  Note:
		# that the logical host name must be fully qualified.
		scds_syslog -p error -t $syslog_tag -m \
			"Logical changepw principal does not exist in %s." \
			"${kadm5_keytab}"
		exit 1
	fi
fi

# Log a message indicating that validate method was successful.

# SCMSGS
# @explanation
# HA-KDC's validate method has successfully completed.
# @user_action
# This is for informational purposes only, no action is required.
scds_syslog -p info -t $syslog_tag -m \
	"Validate method for resource %s completed successfully." \
	"${RESOURCE_NAME}"

exit 0
