#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)krb5_probe.ksh	1.8	07/06/06 SMI"
#
# Probe method for HA Kerberos KDC server.
# This method checks the health of the data service.
#
#include_common_lib
#

###############################################################################
# decide_restart_or_failover ()
#
# This function decides the action to be taken upon the failure of a probe 
# The action could be to restart the data service locally or it could be to
# failover the data service to another node in the cluster. 
#
function decide_restart_or_failover
{

	typeset rc=0
	# Get the resource number of restarts
	num_resource_restart=`scha_resource_get -O NUM_RESOURCE_RESTARTS \
		-R $RESOURCE_NAME -G $RESOURCEGROUP_NAME`
	if [[ $? -ne 0 ]]; then
		scds_syslog -p error -t $syslog_tag -m \
		    "scha_resource_get operation %s failed for Resource %s" \
		    "NUM_RESOURCE_RESTARTS" "${RESOURCE_NAME}"

		# set the status to faulted state.
		scha_resource_setstatus -G $RESOURCEGROUP_NAME \
		    -R $RESOURCE_NAME -s FAULTED -m \
		    "scha_resource_get operation failed."
		exit 1
	fi

	# Get the Retry count value from the system defined property Retry_count
	retry_count=`scha_resource_get -O RETRY_COUNT -G $RESOURCEGROUP_NAME \
	    -R "${RESOURCE_NAME}"`
	if [[ $? -ne 0 ]]; then
		scds_syslog -p error -t $syslog_tag -m \
		    "scha_resource_get operation %s failed for Resource %s" \
		    "RETRY_COUNT" "${RESOURCE_NAME}"

		# set the status to faulted state.
		scha_resource_setstatus -G $RESOURCEGROUP_NAME \
		    -R $RESOURCE_NAME -s FAULTED \
		    -m "scha_resource_get operation failed."
		exit 1
	fi

	# Initiate a local restart if the number of resource restarts
	# is less than the retry count in the retry time interval window.
	if [[ $num_resource_restart -lt $retry_count ]]; then

		scha_control -O RESOURCE_RESTART -G $RESOURCEGROUP_NAME \
		    -R $RESOURCE_NAME
		rc=$?

		if [[ $rc -ne 0 ]]; then
			# don't log a trace if the error is SCHA_ERR_CHECKS
			# Probe may fail repeatedly with Failover_mode setting
			# preventing giveover [or restart], and we don't want
			# to fill up syslog
			if [[ $rc -ne 16 ]]; then
				# SCMSGS
				# @explanation
				# This message indicated that the RGM did not
				# process a restart request, most likely due to
				# the configuration settings.
				# @user_action
				# This is an informational message.
				scds_syslog -p error -t $syslog_tag -m \
				    "Restart operation failed for Resource %s" \
				    "${RESOURCE_NAME}"
			elif [[ $rc -eq 16 ]]; then
				# set rc = 0 to keep the probe running
				rc=0
			fi
			# set the status to faulted state.
			scha_resource_setstatus -G $RESOURCEGROUP_NAME \
			    -R $RESOURCE_NAME -s FAULTED -m "Restart failed."
		else
			# SCMSGS
			# @explanation
			# The HA-KDC service has been restarted.
			# @user_action
			# This is an informational message.
			scds_syslog -p info -t $syslog_tag -m \
			    "Successfully restarted service."
		fi
	else
		# the fact that the number of resource restarts have exceeded
		# implies that the dataservice has already passed the max no
		# of retries allowed. Hence there is no point in trying to
		# restart the dataservice again. We might as well try to
		# failover to another node in the cluster.

		scha_control -O GIVEOVER -G $RESOURCEGROUP_NAME \
		    -R $RESOURCE_NAME
		rc=$?
		# if the resource is not able to failover set it to
		# faulted state

		if [[ $rc -ne 0 ]]; then
			scha_resource_setstatus -G $RESOURCEGROUP_NAME \
			    -R $RESOURCE_NAME -s FAULTED -m \
			    "Unable to fail over to other node."
			# SCMSGS
			# @explanation
			# The number of restarts of services under the resource
			# has reached the max. Hence a fail over was attempted,
			# but failed and the resource group remains in faulted
			# status on its current master.
			# @user_action
			# Examine the syslog to determine the cause of the
			# failures and execute "clresourcegroup switch" to
			# attempt to switch the resource group to a different
			# node, or "clresourcegroup restart" to restart it.
			scds_syslog -p error -t $syslog_tab -m \
			    "Error in failing over the resource group:%s" \
			    "${RESOURCE_NAME}"
		fi
	fi
return $rc
}

###############################################################################
# MAIN
###############################################################################

# The interval at which probing is to to be done is set in the system defined
# properties CHEAP_PROBE_INTERVAL, THOROUGH_PROBE_INTERVAL. Obtain this 
# information using scha_resource_get 
COMPLETE_PROBE_INTERVAL=`scha_resource_get -O THOROUGH_PROBE_INTERVAL \
	-R $RESOURCE_NAME -G $RESOURCEGROUP_NAME`
SIMPLE_PROBE_INTERVAL=`scha_resource_get -O CHEAP_PROBE_INTERVAL \
	-R $RESOURCE_NAME -G $RESOURCEGROUP_NAME`

# Obtain the time-out value allowed for the probe. 
# This value is set in the extension property probe_timeout of the data
# service.
probe_timeout_info=`scha_resource_get -O Extension -R $RESOURCE_NAME \
	-G $RESOURCEGROUP_NAME Probe_timeout`
PROBE_TIMEOUT=`echo $probe_timeout_info | awk '{print $2}'`
((SPROBE_TIMEOUT = PROBE_TIMEOUT/4))

probe_cmd_prog=$(echo "$RT_BASEDIR/complete_probe")

#
# Initialize the sleep counter
#
((sleep_cnt = 0))

while :
do
	status=0
	#
	# We keep sleeping for <CHEAP_PROBE_INTERVAL> and doing
	# a simple_probe, but we also simultaneously maintain
	# a sleep counter. When the sleep counter reaches
	# <THOROUGH_PROBE_INTERVAL>, we do a complete_probe,
	# which essentially adds an admin princ, does a kinit
	# and also sets up a kadmin connection with kadmind.
	#
	sleep $SIMPLE_PROBE_INTERVAL
	((sleep_cnt = sleep_cnt + SIMPLE_PROBE_INTERVAL))

	if [[ $sleep_cnt -ge $COMPLETE_PROBE_INTERVAL ]]; then
		((sleep_cnt = 0))
		if [[ -f $probe_cmd_prog && -x $probe_cmd_prog ]]; then
			hatimerun -t $PROBE_TIMEOUT $probe_cmd_prog \
			-R $RESOURCE_NAME -G $RESOURCEGROUP_NAME \
			-T $RESOURCETYPE_NAME
			status=$?

			#
			# Probe a second time if failure is detected.  PMF may
			# be in the process of restarting in the case that the
			# process was killed.
			#
			if [[ $status != 0 ]]; then
				sleep 1
				hatimerun -t $PROBE_TIMEOUT $probe_cmd_prog \
					-R $RESOURCE_NAME \
					-G $RESOURCEGROUP_NAME \
					-T $RESOURCETYPE_NAME
			fi
		else
			# SCMSGS
			# @explanation
			# The complete_probe file could not be found or is not
			# executable.
			# @user_action
			# Ensure that the file exists and is executable.  By
			# default the file can be found at
			# /opt/SUNWsckrb5/bin/complete_probe.
			scds_syslog -p error -t $syslog_tag -m \
				"Could not find complete_probe for sckrb5."
			status=1
		fi
	else
		#
		# We haven't reached COMPLETE_PROBE_INTERVAL, so we do
		# a simple_probe in the meanwhile.
		#
		hatimerun -t $SPROBE_TIMEOUT $RT_BASEDIR/simple_probe \
			-R $RESOURCE_NAME -G $RESOURCEGROUP_NAME \
			-T $RESOURCETYPE_NAME
		status=$?

		#
		# Probe a second time if failure is detected.  PMF may be
		# in the process of restarting in the case that the process
		# was killed.
		#
		if [[ $status != 0 ]]; then
			sleep 1
			hatimerun -t $SPROBE_TIMEOUT $RT_BASEDIR/simple_probe \
				-R $RESOURCE_NAME -G $RESOURCEGROUP_NAME \
				-T $RESOURCETYPE_NAME
		fi
	fi

	if [[ $status != 0 ]]; then

		decide_restart_or_failover
		if [[ $? -ne 0 ]]; then
			# SCMSGS
			# @explanation
			# The HA-KDC service could not be restarted or perform
			# a fail over.
			# @user_action
			# To determine what action to take, look at the
			# previous syslog messages for more specific error
			# information.
			scds_syslog -p error -t $syslog_tag -m \
				"Could not Restart/Failover the dataservice."
		fi
	else
		# SCMSGS
		# @explanation
		# The complete or simple probe for the HA-KDC service has
		# completed successfully.
		# @user_action
		# This is for informational purposes only, no action is
		# required.
		scds_syslog -p info -t $syslog_tag -m \
			"Probe for resource sckrb5 successful."
	fi
done
