#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)krb5_pmf_action.ksh	1.4	07/06/06 SMI"
#
# PMF action script for KRB5.
#
# This action script is invoked by PMF when the data service dies. The job
# of this script is to 
# 1) initiate a local restart if the number of resource
# restarts is less than the retry count in the retry time interval window.
# This would call scha_control with SCHA_RESOURCE_RESTART tag to request
# the rgmd to execute STOP followed by START method.
# 2) if The number of restarts of services under the resource has reached
# the max. the resource is failover over by calling scha_control with
# SCHA_GIVEOVER
#
#include_common_lib

###############################################################################
# MAIN
##############################################################################

rc=1
parse_args "$@"

# Get the resource number of restarts
num_resource_restart=`scha_resource_get -O NUM_RESOURCE_RESTARTS \
	-R $RESOURCE_NAME -G $RESOURCEGROUP_NAME`
if [[ $? -ne 0 ]]; then
	# SCMSGS
	# @explanation
	# The Resource is left in a faulted status and is not 
	# restarted.
	# @user_action
	# Examine the /var/adm/messages to determine why the
	# resource is failing, and restart it after taking
	# corrective action. The resource can be restarted by
	# the sequence "clresource disable <resource>; clresource
	# enable <resource>". Or, the whole resource group can be
	# restarted using "clresourcegroup restart <group>". If
	# problem persists, contact your Sun service representative.
	scds_syslog -p error -t syslog_tag -m \
		"scha_resource_get operation %s failed for Resource %s" \
		"NUM_RESOURCE_RESTARTS" "${RESOURCE_NAME}"

	# set the status to faulted state.
	scha_resource_setstatus -G "${RESOURCEGROUP_NAME}" \
		-R $RESOURCE_NAME \
		-s FAULTED \
		-m "scha_resource_get operation failed."
	exit 1
fi
# Get the Retry count value from the system defined property Retry_count
retry_count=`scha_resource_get -O RETRY_COUNT \
		-G "${RESOURCEGROUP_NAME}" \
		-R "${RESOURCE_NAME}"`
if [[ $? -ne 0 ]]; then
	scds_syslog -p error -t $syslog_tag -m \
		"scha_resource_get operation %s failed for Resource %s" \
		"RETRY_COUNT" "${RESOURCE_NAME}"
	# set the status to faulted state.
	scha_resource_setstatus -G "${RESOURCEGROUP_NAME}" \
		-R $RESOURCE_NAME \
		-s FAULTED \
		-m "scha_resource_get operation failed."
	exit 1
fi

# Initiate a local restart if the number of resource restarts
# is less than the retry count in the retry time interval window.
if [[ $num_resource_restart -lt $retry_count ]]; then

	scha_control -O RESOURCE_RESTART -G "${RESOURCEGROUP_NAME}" \
			-R "${RESOURCE_NAME}"
	rc=$?

	if [[ $rc -ne 0 ]]; then
		# don't log a trace if the error is SCHA_ERR_CHECKS
		# Probe may fail repeatedly with Failover_mode setting
		# preventing giveover [or restart], and we don't want
		# to fill up syslog
		if [[ $rc -ne 16 ]]; then
			scds_syslog -p error -t $syslog_tag -m \
				"Restart operation failed for Resource %s" \
				"${RESOURCE_NAME}"
		fi
			# set the status to faulted state.
			scha_resource_setstatus -G "${RESOURCEGROUP_NAME}" \
				-R $RESOURCE_NAME \
				-s FAULTED \
				-m "Restart failed."
	else
			scds_syslog -p info -t $syslog_tag -m \
				"Successfully restarted service."
	fi
else
	# the fact that the number of resource restarts have exceeded
	# implies that the dataservice has already passed the max no
	# of retries allowed. Hence there is no point in trying to restart
	# the dataservice again. We might as well try to failover
	# to another node in the cluster.
	scha_control -O GIVEOVER -G $RESOURCEGROUP_NAME \
		-R $RESOURCE_NAME
	rc=$?
	# if the resource is not able to failover set it to 
	# faulted state

	if [ $rc -ne 0 ]; then
		scha_resource_setstatus -G "${RESOURCEGROUP_NAME}" \
			-R $RESOURCE_NAME \
			-s FAULTED \
			-m "Unable to Failover to other node."
		scds_syslog -p error -t $syslog_tag -m \
			"Error in failing over the resource group:%s" \
			"${RESOURCEGROUP_NAME}"
	fi
fi
exit 1
