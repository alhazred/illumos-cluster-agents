#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)krb5_monitor_stop.ksh	1.6	07/06/06 SMI"
#
# Monitor stop method for HA Kerberos KDC server.
# Stops the monitor that is running. This is done via PMF.
#
# cmd/ha-services/krb5/krb5_monitor_stop.ksh
#
#include_common_lib
#

#############################
# MAIN
#############################

PMF_TAG=$RESOURCEGROUP_NAME,$RESOURCE_NAME,0.mon

# See if the monitor is running, and if so, kill it. 
if pmfadm -q $PMF_TAG; then 

	pmfadm -s $PMF_TAG KILL
	if [ $? -ne 0 ]; then 
		# SCMSGS
		# @explanation
		# The monitor for HA-KDC is running and could not be stopped.
		# @user_action
		# Check to see if the monitor is still running and try to
		# manually stop the monitor.  Observe the behavior and take
		# the appropriate action.
		scds_syslog -p error -t $syslog_tag -m \
			"Could not stop monitor for resource %s." \
			"${RESOURCE_NAME}"
		exit 1
	else
		# could successfully stop the monitor. Log a message.

		# SCMSGS
		# @explanation
		# The monitor for HA-KDC has been stopped.
		# @user_action
		# This is for informational purposes, no action is required.
		scds_syslog -p info -t $syslog_tag -m \
			"Monitor for resource %s successfully stopped." \
			"${RESOURCE_NAME}"
	fi

fi

exit 0
