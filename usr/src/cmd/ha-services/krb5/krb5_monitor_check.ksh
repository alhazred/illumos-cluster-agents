#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
# 
# ident	"@(#)krb5_monitor_check.ksh	1.6	07/06/06 SMI"
# 
# Monitor check method for HA Kerberos KDC server.
#
# This method is called by the RGM whenever there is a failover to be performed
# by the fault monitor. This method is called to ensure that the new node that
# dataservice will be failing over to is actually healthy enough to host the  
# dataservice. This method makes a call to the validate method that has been
# registered in order to achieve this.
#
# cmd/ha-services/krb5/krb5_monitor_check.ksh
#
#include_common_lib
#

#############################
# MAIN
#############################

# Obtain the name of the validate method for this resource.
VALIDATE_METHOD=`scha_resource_get -O VALIDATE \
	-R $RESOURCE_NAME -G $RESOURCEGROUP_NAME`

# Call the validate method so that the dataservice can be failed over 
# successfully to the new node.
$RT_BASEDIR/$VALIDATE_METHOD -R $RESOURCE_NAME -G $RESOURCEGROUP_NAME \
	-T $RESOURCETYPE_NAME

# Log a message indicating that monitor check was successful.
if [ $? -eq 0 ]; then
	# SCMSGS
	# @explanation
	# The node to be failed over to for HA-KDC is ready.
	# @user_action
	# This is for informational purposes, no action is required.
	scds_syslog -p info -t $syslog_tag -m \
		"Monitor check for sckrb5 successful."
	exit 0
else
	# SCMSGS
	# @explanation
	# The node to be failed over to for HA-KDC is not ready.
	# @user_action
	# Use scstat to help determine the reason why the other node can not
	# be failed over to.
	scds_syslog -p error -t $syslog_tag -m \
		"Monitor check for sckrb5 failed."
	exit 1
fi
