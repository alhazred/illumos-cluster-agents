/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */


/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * gettime.c - This is a utility program used by the probe method of
 * the Kerberos KDC data service. It provides the time in seconds from a
 * known reference point (epoch point).
 *
 */

#pragma ident	"@(#)gettime.c	1.4	07/06/06 SMI"

#include <stdio.h>
#include <sys/types.h>
#include <time.h>

int
main()
{
	(void) printf("%d\n", time(0));
	return (0);
}
