#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)krb5_update.ksh	1.6	07/06/06 SMI"
#
# Update method for HA Kerberos KDC server.
#
# This method only restarts the fault monitor.
#
# cmd/ha-services/krb5/krb5_update.ksh
# 
#include_common_lib
#

#############################
# MAIN
#############################

PMF_TAG=$RESOURCEGROUP_NAME,$RESOURCE_NAME,0.mon

pmfadm -q $PMF_TAG
if [[ $? == 0 ]]; then
	# kill the monitor that is running already
	pmfadm -k $PMF_TAG KILL
	if [[ $? != 0 ]]; then
		# SCMSGS
		# @explanation
		# The monitor for HA-KDC could not be killed with a SIGKILL.
		# @user_action
		# This is an internal error.  No user action needed.  Save the
		# /var/adm/messages from all nodes.  Contact your authorized
		# Sun service provider.
		scds_syslog -p error -t $syslog_tag -m \
			"Could not kill the monitor."
		exit 1
	else
		# could successfully stop sckrb5. Log a message.

		# SCMSGS
		# @explanation
		# The HA-KDC's update method was able to stop the monitor.
		# @user_action
		# PMF will restart the monitor, so no intervention is required.
		scds_syslog -p info -t $syslog_tag -m \
			"Monitor for sckrb5 successfully stopped.  PMF will restart it."
	fi
fi

exit 0
