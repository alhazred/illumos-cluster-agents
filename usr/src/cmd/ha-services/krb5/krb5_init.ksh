#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)krb5_init.ksh	1.6	07/06/06 SMI"
#
# An init script which essentially checks to see if the Kerberos
# KDC daemons are already running under smf(5). If yes, then we
# disable the daemons under smf and restart them under the
# SunCluster HA services framework.
#
# cmd/ha-services/krb5/krb5_init.ksh
#
#include_common_lib
#

###############################################################################
# MAIN
###############################################################################

#
# Check to see if the system is a S10 (or pre-S10) system,
# so we can disable the Kerberos KDC daemons, if running
# under smf(5). 
#
if smf_running; then
	((i = 0))
	while ((i < fmri_cnt)); do
		# Use the -s option to explicitly request a synchronous
		# call to svcadm
		svcadm disable -s ${FMRI[i]} > /dev/null 2>&1
		if [ $? -ne 0 ]; then
			# SCMSGS
			# @explanation
			# The initialization script was not able to disable
			# krb5kdc or kadmind under SMF.
			# @user_action
			# Check the state of krb5kdc or kadmind using "svcs -x".
			scds_syslog -p error -t $syslog_tag -m \
				"Disabling of %s under smf(5), failed. Admin intervention may be required." "${FMRI[i]}"
			exit 1
		fi
	((i = i + 1))
	done # while
fi
