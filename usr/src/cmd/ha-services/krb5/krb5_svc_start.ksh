#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)krb5_svc_start.ksh	1.9	08/09/02 SMI"
#
# Start Method for HA Kerberos KDC server.
#
# The data service is started under the control of PMF. Prior to starting the 
# process(es) for sckrb5 some sanity checks are done. PMF tries to start the
# service specified number of times and if the number of attempts exceeds this 
# value within a specified interval of time, PMF reports a failure to start
# the service. The number of times to retry and the interval in which it is to
# be tried are both properties of the resource set in RTR file. 
#
#include_common_lib
#

#############################
# MAIN
#############################

set_status UNKNOWN

PMF_TAG[0]=$RESOURCEGROUP_NAME,$RESOURCE_NAME,0.svc
PMF_TAG[1]=$RESOURCEGROUP_NAME,$RESOURCE_NAME,1.svc

hostnames=`$RT_BASEDIR/gethostnames -R $RESOURCE_NAME -G $RESOURCEGROUP_NAME \
	-T $RESOURCETYPE_NAME`

start_cmd_args[0]="$RT_BASEDIR/kdc daemon0 start"
start_cmd_args[1]="$RT_BASEDIR/kdc daemon1 start"

start_cmd_prog=`echo ${start_cmd_args[0]} | nawk '{print $1}'`

if [[ ! -f $start_cmd_prog || ! -x $start_cmd_prog ]]; then
	# SCMSGS
	# @explanation
	# The specified file does not exist or does not have it's executable
	# bit set.
	# @user_action
	# Ensure that the file exists and is executable.
	scds_syslog -p error -t $syslog_tag -m \
		"File %s is missing or not executable." "${start_cmd_prog}"
	set_status FAULTED

	exit 1
fi

# Get the value for retry count from the RTR file.
RETRY_CNT=`scha_resource_get -O Retry_Count -R $RESOURCE_NAME \
	-G $RESOURCEGROUP_NAME`

# Get the value for retry interval from the RTR file. The value for the 
# RETRY_INTERVAL in the RTR file will be in seconds. Convert this value from 
# seconds to minutes for passing on to pmfadm. Note that this is necessarily
# a conversion with round-up, eg. 59 seconds --> 1 minute.
((RETRY_INTRVAL = (`scha_resource_get -O Retry_Interval -R $RESOURCE_NAME -G \
	$RESOURCEGROUP_NAME` + 59) / 60 ))


# Obtain the name of the validate method for this resource.
VALIDATE_METHOD=`scha_resource_get -O VALIDATE \
	-R $RESOURCE_NAME -G $RESOURCEGROUP_NAME`

# Always do validation first, before starting up the dataservice
$RT_BASEDIR/$VALIDATE_METHOD -R $RESOURCE_NAME -G $RESOURCEGROUP_NAME \
	-T $RESOURCETYPE_NAME
if [[ $? -ne 0 ]]; then
	# SCMSGS
	# @explanation
	# The validation test failed before the HA-KDC service started.
	# @user_action
	# To determine what action to take, look at the previous syslog
	# messages for more specific error information.
	scds_syslog -p error -t $syslog_tag -m \
		"Validate check for sckrb5 failed."
	set_status FAULTED

	exit 1
fi

# Action script to be passed to pmfadm
action=$RT_BASEDIR/krb5_pmf_action

# start the daemon under the control of Sun Cluster Process Monitor
# Facility. Let it crash and invoke the action script provided. The
# action script will do a resource restart/failover and will return
# non zero exit status to cease the process monitor facility restarts.

pmfadm -c ${PMF_TAG[0]} -a "$action -R $RESOURCE_NAME -G $RESOURCEGROUP_NAME \
-T $RESOURCETYPE_NAME" ${start_cmd_args[0]} > /dev/null 2>&1
daemon0_status=$?
pmfadm -c ${PMF_TAG[1]} -a "$action -R $RESOURCE_NAME -G $RESOURCEGROUP_NAME \
-T $RESOURCETYPE_NAME" ${start_cmd_args[1]} > /dev/null 2>&1
daemon1_status=$?

if [[ $daemon0_status -eq 0 && $daemon1_status -eq 0 ]]; then

	# Do a simple probe to make sure the daemons have successfully started
	# before we declare victory for the start method.
	# Start out by briefly waiting for the services to come up while in
	# a loop for MAX_RETRY times. We wait 10% of start time for the probe
	# to complete.
	SLEEP_WAIT_PCT=1
	WAIT_PCT=10
	MAX_RETRY=5
	i=0

	START_TIMEOUT=`scha_resource_get -O START_TIMEOUT -R $RESOURCE_NAME \
		-G $RESOURCEGROUP_NAME`
	((SLEEP_TIMEOUT = (START_TIMEOUT*SLEEP_WAIT_PCT)/100))
	((PROBE_TIMER = (START_TIMEOUT*WAIT_PCT)/100))
	if [[ $SLEEP_TIMEOUT -eq 0 || $PROBE_TIMER -eq 0 ]]; then
		SLEEP_TIMEOUT=3
		PROBE_TIMER=30
	fi

	while ((i < MAX_RETRY)); do
		sleep $SLEEP_TIMEOUT

		hatimerun -t $PROBE_TIMER $RT_BASEDIR/simple_probe \
			-R $RESOURCE_NAME -G $RESOURCEGROUP_NAME \
			-T $RESOURCETYPE_NAME
		status=$?

		# Log a message indicating that sckrb5 has been started.
		if [[ $status -eq 0 ]]; then
			# SCMSGS
			# @explanation
			# Both the krb5kdc and kadmind daemons started
			# successfully under PMF.
			# @user_action
			# This is for informational purposes only, no action
			# is required.
			scds_syslog -p notice -t $syslog_tag -m \
				"Kerberos daemons started."
			set_status OK "Kerberos daemons started."

			exit 0
		else
			# SCMSGS
			# @explanation
			# Waiting for both the krb5kdc and kadmind daemons
			# to start successfully under PMF.
			# @user_action
			# This is for informational purposes only, no action
			# is required.
			scds_syslog -p notice -t $syslog_tag -m \
				"Waiting for Kerberos daemons to startup."
			set_status DEGRADED \
				"Waiting for Kerberos daemons to startup."
		fi
		((i += 1))
	done
fi

# At this point we've either returned daemon status failure or we've timed-out
# out waiting for the simple probe to return success, so we return failure.
# SCMSGS
# @explanation
# The krb5kdc and kadmind daemons had failed to come up.
# @user_action
# Check the syslog messages for further information on specific configuration
# issues of Kerberos.
scds_syslog -p error -t $syslog_tag -m "Failed to start Kerberos daemons."
set_status FAULTED

exit 1
