#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)krb5_svc_stop.ksh	1.7	07/06/06 SMI"
#
# Stop method for HA Kerberos KDC server.
#
# cmd/ha-services/krb5/krb5_svc_stop.ksh
#
#include_common_lib
#

#############################
# MAIN
#############################

set_status UNKNOWN

# get the Timeout value allowed for stop method from the RTR file
STOP_TIMEOUT=`scha_resource_get -O STOP_TIMEOUT -R $RESOURCE_NAME \
	-G $RESOURCEGROUP_NAME`

# We will try to wait wait for 80% of the stop_method_timeout value when we
# send a SIGTERM through PMF to the Data service. This is to make sure that
# the application stops in a decent manner. If the application does not
# respond favourably to this then we use SIGKILL to stop the data service.
((SMOOTH_TIMEOUT=$STOP_TIMEOUT * 80/100))

PMF_TAG[0]=$RESOURCEGROUP_NAME,$RESOURCE_NAME,0.svc
PMF_TAG[1]=$RESOURCEGROUP_NAME,$RESOURCE_NAME,1.svc
pmf_tag_cnt=2

hostnames=`$RT_BASEDIR/gethostnames -R $RESOURCE_NAME -G $RESOURCEGROUP_NAME \
        -T $RESOURCETYPE_NAME`

stop_cmd_args[0]="$RT_BASEDIR/kdc daemon0 stop"
stop_cmd_args[1]="$RT_BASEDIR/kdc daemon1 stop"

stop_cmd_prog=`echo ${stop_cmd_args[0]} | nawk '{print $1}'`

typeset -i SEND_KILL=0

((i = 0))
while ((i < pmf_tag_cnt)); do

pmfadm -q ${PMF_TAG[i]}

if [[ $? == 0 ]]; then
	if [[ -f $stop_cmd_prog && -x $stop_cmd_prog ]]; then

		pmfadm -s ${PMF_TAG[i]}
		if [[ $? != 0  ]]; then
			# SCMSGS
			# @explanation
			# HA-KDC's stop method was not able to stop krb5kdc or
			# kadmind through PMF.
			# @user_action
			# This is for informational purposes, no action is
			# required.  The stop method will subsequently try to
			# forcefully kill these processes through SIGKILL.
			scds_syslog -p info -t $syslog_tag -m \
				"Failed to take sckrb5 out of PMF control; trying to send SIGKILL now."
			SEND_KILL=1
		else

			# execute the user specified stop_cmd using hatimerun
			hatimerun -k KILL -t $SMOOTH_TIMEOUT \
				${stop_cmd_args[i]}
			if [[ $? != 0 ]]; then
				# SCMSGS
				# @explanation
				# HA-KDC's stop method was not able to
				# gracefully terminate krb5kdc or kadmind
				# within SMOOTH_TIMEOUT.
				# @user_action
				# This is for informational purposes, no action
				# is required.  The stop method will
				# subsequently try to forcefully kill these
				# processes through SIGKILL.
				scds_syslog -p error -t $syslog_tag -m \
					"Failed to stop sckrb5 using the custom stop command; trying SIGKILL now."
			fi

			# Regardless of whether the command succeeded or not we
			# send KILL signal to the pmf tag. This will ensure
			# that the process tree goes away if it still exists.
			# If it doesn't exist by then, we return NOERR.
			SEND_KILL=1
		fi
	else

		# Send a SIGTERM signal to the Data service and wait for
		# 80% of the total timeout value.
		pmfadm -s ${PMF_TAG[i]} -w $SMOOTH_TIMEOUT TERM

		# We compare the exit status of pmfadm to be greater than 2
		# because "exit status = 1" means nametag doesn't exist, which
		# is a OK, for the stop method has to be idempotent.
		if [[ $? -ge 2 ]]; then 
			# SCMSGS
			# @explanation
			# HA-KDC's stop method was not able to stop krb5kdc or
			# kadmind by using the SIGTERM signal.
			# @user_action
			# This is for informational purposes, no action is
			# required.  The stop method will subsequently try to
			# forcefully kill these processes through SIGKILL.
			scds_syslog -p error -t $syslog_tag -m \
				"Failed to stop sckrb5 with SIGTERM; retry with SIGKILL."
			SEND_KILL=1;
		fi
	fi

	pmfadm -q ${PMF_TAG[i]}
	if [[ $SEND_KILL == 1 && $? == 0 ]]; then 

		# Since the Data service did not stop with a SIGTERM we will
		# use a SIGKILL now.
		pmfadm -s ${PMF_TAG[i]} KILL

		# We compare the exit status of pmfadm to be greater than 2
		# because "exit status = 1" means nametag doesn't exist, which
		# is a OK, for the stop method has to be idempotent.
		if [[ $? -ge 2 ]]; then
			# SCMSGS
			# @explanation
			# Was not able to stop the HA-KDC service through a
			# SIGKILL.
			# @user_action
			# This is an internal error.  No user action needed. 
			# Save the /var/adm/messages from all nodes.  Contact
			# your authorized Sun service provider.
			scds_syslog -p error -t $syslog_tag -m \
				"Failed to stop sckrb5; exiting UNSUCCESSFUL."
			set_status FAULTED

			exit 1
		fi
	fi
else
	((last_svc_index = pmf_tag_cnt - 1))
	if [ $i -eq $last_svc_index ]; then
		# The Data service is not running as of now. Log a message and 
		# exit success.

		# SCMSGS
		# @explanation
		# HA-KDC's stop method was called but the service was not
		# running.
		# @user_action
		# This is for informational purposes only, no action is
		# required.
		scds_syslog -p info -t $syslog_tag -m \
			"sckrb5 not running."
		set_status OFFLINE "sckrb5 not running."

		# Even if the sckrb5 is not running, we exit success,
		# for idempotency of the STOP method.
		exit 0
	fi
fi

# Increment counter
((i = i + 1))
done  #while ((i < pmf_tag_cnt)); 


# Successfully stopped sckrb5. Log a message and exit success.

# SCMSGS
# @explanation
# The HA-KDC's stop method had completed with out error.
# @user_action
# This is for informational purposes only, no action is required.
scds_syslog -p info -t $syslog_tag -m \
	"sckrb5 successfully stopped."
set_status OFFLINE "Kerberos daemons stopped."

exit 0
