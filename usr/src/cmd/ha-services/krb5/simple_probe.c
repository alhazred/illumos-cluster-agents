/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * simple_probe.c - This is a utility program used by the probe method of
 * the Kerberos KDC data service. This utility probes the dataservice that
 * calls this binary. The probing done is a simple connect and disconnect
 * to the IP address and port on which the dataservice has been configured.
 *
 * The usage for this binary is
 *
 * simple_probe -R <resource_name>
 * 		-G <resourcegroup_name>
 * 		-T <resourcetype_name>
 *
 */


#pragma ident	"@(#)simple_probe.c	1.4	07/06/06 SMI"

#include <stdio.h>
#include <sys/types.h>
#include <time.h>
#include <rgm/libdsdev.h>

int
main(int argc, char *argv[])
{
	scds_netaddr_list_t 	*netaddr;
	int 			err;
	scds_handle_t 		scds_handle;
	char 		*hostname;
	int		port, ip, timeout;


	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR) {
		return (1);
	}

	if (scds_get_netaddr_list(scds_handle, &netaddr)) {
		scds_syslog(LOG_ERR,
		    "No network address resource in resource group.");
		scds_close(&scds_handle);
		return (1);
	}

	timeout = scds_get_ext_probe_timeout(scds_handle);

	for (ip = 0; ip < netaddr->num_netaddrs; ip++) {
		/* Get the host IP address and the port number */
		hostname = netaddr->netaddrs[ip].hostname;
		port = netaddr->netaddrs[ip].port_proto.port;

		err = scds_simple_probe(scds_handle, hostname, port, timeout);
		if (err != 0) {
			return (err);
		}
	}

	scds_free_netaddr_list(netaddr);
	scds_close(&scds_handle);
	return (0);
}
