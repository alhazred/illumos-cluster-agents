/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * hasp_check.c - utility routine for checking the status of the HAStorage
 *		  Plus resources.
 *
 * Since this utility is typically called from the VALIDATE method, the
 * resource for which this is being called hasn't yet been created.
 * Therefore, this utility must be called with all the arguments as they were
 * passed to the VALIDATE method. Failing to do this would result in the
 * failure of scds_initialize and, consequently, of hasp_check itself.
 *
 */


#pragma ident	"@(#)hasp_check.c	1.4	07/06/06 SMI"

#include <stdio.h>
#include <rgm/libdsdev.h>

int
main(int argc, char *argv[])
{
	int 		rc;
	scds_handle_t 	scds_handle;
	scds_hasp_status_t hasp_status;

	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR) {
		return (1);
	}

	/* Check availability of SUNW.HAStoragePlus resources */
	if (scds_hasp_check(scds_handle, &hasp_status) != SCHA_ERR_NOERR) {
		/* Call failed, status is unknown */
		rc = 1;
		goto finished; /* validation has failed for this resource */
	}

	switch (hasp_status) {

	case SCDS_HASP_NO_RESOURCE:
		/*
		 * We do not depend on any SUNW.HAStoragePlus resources
		 */
		rc = 2;
		break;

	case SCDS_HASP_ERR_CONFIG:
		/*
		 * Configuration error, SUNW.HAStoragePlus resource is
		 * in a different RG.
		 */
		rc = 3;
		break;

	case SCDS_HASP_NOT_ONLINE:
		/*
		 * There is at least one SUNW.HAStoragePlus resource not
		 * online anywhere.
		 */
		rc = 4;
		break;

	case SCDS_HASP_ONLINE_NOT_LOCAL:
		/*
		 * Not all SUNW.HAStoragePlus we need, are online locally.
		 */
		rc = 5;
		break;

	case SCDS_HASP_ONLINE_LOCAL:
		/*
		 * All SUNW.HAStoragePlus resources we need are available on
		 * this node.
		 */
		rc = 6;
		break;

	default:
		/* Unknown status code */
		scds_syslog(LOG_ERR, "Unknown status code %d.", hasp_status);
		rc = 1;
		break;
	}

finished:
	scds_close(&scds_handle);
	return (rc);
}
