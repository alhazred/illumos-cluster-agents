#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)complete_probe.ksh	1.9	08/09/02 SMI"
#
# A 'complete_probe' program for Kerberos daemons. Invoked every
# <THOROUGH_PROBE_INTERVAL> seconds.
#
#include_common_lib
#

#
# This function does some cleanup, before returning error.
#
function error_return
{
	# SCMSGS
	# @explanation
	# Sun Cluster could not complete the thorough probing of HA-KDC.
	# @user_action
	# Check for other error messages logged before this one to determine
	# the specific cause of failure.
	scds_syslog -p error -t $syslog_tag -m \
		"Complete_Probe for resource sckrb5 failed."
	kadmin.local -q "delprinc -force $ADMIN_PRINC" >$TMP_FILE 2>&1
	rm -f $TMP_FILE $TMP_KEYTAB
	exit 1
}

###############################################################################
# MAIN
###############################################################################

ADMIN_PRINC="sckrb5-probe/admin"

# Set lang to standard english to avoid confusion
export LC_ALL=C

if [ -x /usr/bin/mktemp ]; then
	TMP_FILE=$(/usr/bin/mktemp /etc/krb5/krb5tmpfile.XXXXXX)
	TMP_KEYTAB=$(/usr/bin/mktemp /etc/krb5/sckrb5keytab.XXXXXX)
else
	TMP_FILE="/etc/krb5/krb5tmpfile.$$"
	TMP_KEYTAB="/etc/krb5/sckrb5keytab.$$"
fi

if [[ -z "$TMP_FILE" || -z "$TMP_KEYTAB" ]]; then
	# SCMSGS
	# @explanation
	# Was not able to create a temporary keytab file used in thorough
	# probing of the HA-KDC service.
	# @user_action
	# Check the state of /etc/krb5.
	scds_syslog -p error -t $syslog_tag -m \
		"Creation of temporary file failed."
	error_return
fi

#
# If we are interrupted, cleanup after ourselves
#
trap "error_return" HUP INT QUIT TERM

hostname=$(echo `$RT_BASEDIR/gethostnames -R $RESOURCE_NAME -G $RESOURCEGROUP_NAME -T $RESOURCETYPE_NAME`)

if [ -z "$hostname" ]; then
	# SCMSGS
	# @explanation
	# The network address resource have not been configured.
	# @user_action
	# Add the network resource to the associated resource group.
	scds_syslog -p error -t $syslog_tag -m \
		"The network resource has not been configured."
	error_return
else
	hostname=$(echo "$hostname"|cut -d "," -f1|tr '[A-Z]' '[a-z]')
fi

#
# Check for /etc/resolv.conf
#
if [ ! -r $RESOLVCONF ]; then
	#
	# /etc/resolv.conf not present, cannot proceed
	#

	# SCMSGS
	# @explanation
	# The /etc/resolv.conf file is missing.
	# @user_action
	# Create the /etc/resolv.conf file.  Kerberos is dependent upon DNS
	# to canonicalize service principal names.
	scds_syslog -p error -t $syslog_tag -m \
		"File %s is missing." "${RESOLVCONF}"
	error_return
fi

if ! ping $hostname > /dev/null 2>&1; then
	# SCMSGS
	# @explanation
	# Did not receive a response from the specified logical host name's
	# interface.
	# @user_action
	# Check to make sure that the interface associated with the logical
	# host name is up and configured correctly.
	scds_syslog -p error -t $syslog_tag -m \
		"Logical nodename '%s', unreachable." "${hostname}"
	error_return
fi

#
# Add a dummmy principal to the KDC database, delete it first
# so you dont have an existing one with the same name
#
kadmin.local -q "delprinc -force $ADMIN_PRINC" >$TMP_FILE 2>&1
kadmin.local -q "addprinc -randkey $ADMIN_PRINC" >$TMP_FILE 2>&1
kadmin.local -q "ktadd -k $TMP_KEYTAB $ADMIN_PRINC" >$TMP_FILE 2>&1

#
# Open a kadmin connection and do a getprinc on
# the newly added dummy principal.
#
kadmin -k -t $TMP_KEYTAB -p $ADMIN_PRINC -q "getprinc $ADMIN_PRINC" >$TMP_FILE 2>&1
egrep -s "get_principal: Principal does not exist" $TMP_FILE
if [ $? -eq 0 ]; then
	# SCMSGS
	# @explanation
	# An attempt was made to add a test principal to the Kerberos database,
	# but this failed during a complete probe for the HA-KDC service.
	# @user_action
	# Check the state of the /var/krb5/principal database.
	scds_syslog -p error -t $syslog_tag -m \
		"getprinc of %s failed." "${ADMIN_PRINC}"
	error_return
else
	# SCMSGS
	# @explanation
	# The complete probe of Kerberos received no error.
	# @user_action
	# This is for informational purposes, no action is required.
	scds_syslog -p info -t $syslog_tag -m \
		"Complete_Probe for resource sckrb5 successful."
fi

#
# We are done running the probe checks, delete the
# dummy principal and do cleanup.
#
kadmin.local -q "delprinc -force $ADMIN_PRINC" >$TMP_FILE 2>&1
rm -f $TMP_FILE $TMP_KEYTAB
return 0
