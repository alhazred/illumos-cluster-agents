#!/bin/sh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)kdc.sh	1.8	08/09/02 SMI"
#
# Startup script for Kerberos daemons.
#

BINDIR=/usr/lib/krb5
KDC_CONF_DIR=/etc/krb5

# default kadm5.acl file path
KADM5ACL=/etc/krb5/kadm5.acl

# krb5kdc's default db path
PRINCIPALDB=/var/krb5/principal

# Set PATH and set lang to standard english to avoid confusion
PATH=/usr/cluster/lib/sc:/usr/cluster/bin:/usr/sbin:/usr/proc/bin:$PATH
LC_ALL=C

syslog_tag="SC[SUNW.krb5,$0]"

if [ ! -d $BINDIR ]; then			# /usr not mounted
	# SCMSGS
	# @explanation
	# The /usr/lib/krb5 path is not accessible.
	# @user_action
	# Check this path and determine if /usr needs to be mounted.
	scds_syslog -p error -t $syslog_tag -m \
		"%s not mounted, exiting" "${BINDIR}"
	exit 1
fi


#return success (0) if a db exists in a path specified in kdc.conf or
#if none are specified in the kdc.conf, check the default path.  else
#return failure (1).
db_exists() {

	#check db names specified in the kdc.conf
	DBNAMES=`awk -F= '/^[ 	]*database_name/ \
		    { printf("%s", $2)}' $KDC_CONF_DIR/kdc.conf`

	if [ -n "$DBNAMES" ]; then
		for DB in $DBNAMES; do
			if [ -s $DB ]; then
				return 0
			elif  [ -s ${DB}.db ]; then
				mv ${DB}.db  $DB #db suffix no longer needed
				if [ $? = 0 ]; then
					# SCMSGS
					# @explanation
					# The principal database was renamed
					# from the old name "principal.db" to
					# the new name "principal".
					# @user_action
					# This is for informational purposes,
					# no action is required.
					scds_syslog -p info -t $syslog_tag \
					-m "%s: renamed %s.db to %s" "${0}" \
					"${DB}" "${DB}"
					return 0
				else
					# SCMSGS
					# @explanation
					# The "principal.db" database file could
					# not be renamed to "principal".
					# @user_action
					# Check the state of the "principal" and
					# "principal.db" files under /var/krb5.
					scds_syslog -p error -t $syslog_tag \
					-m "%s: rename of %s.db to %s FAILED" \
					"${0}" "${DB}" "${DB}"
					return 1 #failure
				fi
			fi
		done
	else
		#check default db path
		if [ -s $PRINCIPALDB ]; then
			return 0
		elif [ -s ${PRINCIPALDB}.db ]; then
			#db suffix no longer needed
			mv ${PRINCIPALDB}.db $PRINCIPALDB
			if [ $? = 0 ]; then
				scds_syslog -p info -t $syslog_tag -m \
				"%s: renamed %s.db to %s" "${0}" "${DB}" "${DB}"
				return 0
			else
				scds_syslog -p error -t $syslog_tag -m \
				"%s: rename of %s.db to %s FAILED" "${0}" \
				"${DB}" "${DB}"
				return 1 #failure
			fi
		fi
	fi

	return 1  #failure
}

#
# Returns success (0) if an acl_file that is specified in the kdc.conf
# (or if not specified, the default one), is configured
# ("_default_realm_" is replaced with the local realm name),
# else returns failure (1).
#
kadm5_acl_configured() {

	#check acl_file relation values in the kdc.conf
	ACLFILES=`awk -F= '/^[ 	]*acl_file[ 	=]/ \
		    { printf("%s", $2)}' $KDC_CONF_DIR/kdc.conf`

	if [ -n "$ACLFILES" ]; then
		for FILE in $ACLFILES; do
			if [ -s $FILE ]; then
				grep -v '^[ 	]*#' $FILE | \
				    grep  '_default_realm_' > /dev/null 2>&1
				if [ $? -gt 0 ]; then
					return 0 #success
				fi
			fi
		done
	else
		#check the default kadm5.acl file
		if [ -s $KADM5ACL ]; then
			grep -v '^[ 	]*#' $KADM5ACL | \
			    grep  '_default_realm_' > /dev/null 2>&1
			if [ $? -gt 0 ]; then
				return 0 #success
			fi
		fi

	fi

	#
	# If we reached this far, there is no kadm5.acl configured,
	# we return failure.
	#
	return 1  #failure
}


#
# MAIN - Start/stop processes required for KDC. This will start
# up krb5kdc if arg1 is 'daemon0', and will start up kadmind if
# arg1 is 'daemon1' on the master kdc.
#

zonename=`/bin/zonename`

case "$1" in

'daemon0')
	case "$2" in

	'start')
	if [ -s $KDC_CONF_DIR/kdc.conf ]; then

		# Make sure kdc.conf is configured -
		# We want to check for "_default_realm_"
	  	# with any number of preceeeding or
		# trailing underscores or whitespaces.
		grep '^[ 	]*_[_]*default_realm_' \
		    $KDC_CONF_DIR/kdc.conf > /dev/null 2>&1

		kdc_configured=$?

		if [ $kdc_configured -gt 0 ] && db_exists \
			&& [ -x $BINDIR/krb5kdc ]; then
				$BINDIR/krb5kdc
				/usr/bin/pgrep -z $zonename krb5kdc > \
					/dev/null 2>&1
				if [ $? -eq 0 ]; then
					exit 0
				else
					# SCMSGS
					# @explanation
					# krb5kdc is no longer running just
					# after Sun Cluster tried to start it.
					# @user_action
					# This problem could be caused by a
					# number of issues.  Check the
					# krb5.conf(4) and kdc.conf(4) files for
					# correctness.  Ensure that the master
					# key exists:
					# /etc/krb5/.k5.<realm name>.  Check
					# that the binary is executable and
					# that the /var/krb5/principal database
					# file exists.
					scds_syslog -p error -t $syslog_tag \
					-m "%s/krb5kdc not started, exiting" \
					"${BINDIR}"
					exit 1
				fi
		else
			scds_syslog -p error -t $syslog_tag -m \
			"%s/krb5kdc not started, exiting" "${BINDIR}"
			exit 1
		fi
	fi
	exit 0
	;;


	'stop')
	/usr/bin/pkill -x -u 0 -z $zonename krb5kdc > /dev/null 2>&1
	exit 0
	;;


	*)
	echo "Usage: $0 { daemon0 | daemon1 } { start | stop }"
	exit 1
	;;


	esac
	;;


'daemon1')
	case "$2" in

	'start')
        if [ -s $KDC_CONF_DIR/kdc.conf ]; then

		# Make sure kdc.conf is configured -
		# We want to check for "_default_realm_"
	  	# with any number of preceeeding or
		# trailing underscores or whitespaces.
                grep '^[        ]*_[_]*default_realm_' \
                    $KDC_CONF_DIR/kdc.conf > /dev/null 2>&1

                kdc_configured=$?
                if [ $kdc_configured -gt 0 ]; then
                        if kadm5_acl_configured && db_exists \
			    && [ -x $BINDIR/kadmind ]; then
                        	$BINDIR/kadmind
				/usr/bin/pgrep -z $zonename kadmind > \
					/dev/null 2>&1
				if [ $? -eq 0 ]; then
					exit 0
				else
					# SCMSGS
					# @explanation
					# kadmind is no longer running just
					# after Sun Cluster tried to start it.
					# @user_action
					# This problem could be caused by a
					# number of issues.  Check the
					# krb5.conf(4), kdc.conf(4), and
					# kadm5.acl(4) files for correctness.
					# Ensure that the master key exists:
					# /etc/krb5/.k5.<realm name>.  Check
					# that the binary is executable and
					# that the /var/krb5/principal database
					# file exists.
					scds_syslog -p error -t $syslog_tag \
					-m "%s/kadmind not started, exiting" \
					"${BINDIR}"
					exit 1
				fi
			else
				scds_syslog -p error -t $syslog_tag -m \
				"%s/kadmind not started, exiting" "${BINDIR}"
				exit 1
			fi
		else
			scds_syslog -p error -t $syslog_tag -m \
			"%s/kadmind not started, exiting" "${BINDIR}"
			exit 1
                fi
        fi
        ;;


	'stop')
        /usr/bin/pkill -x -u 0 -z $zonename kadmind > /dev/null 2>&1
	exit 0
        ;;


	*)
        echo "Usage: $0 { daemon0 | daemon1 } { start | stop }"
	exit 1
        ;;


	esac
	;;

esac
