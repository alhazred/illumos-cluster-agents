/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the License).
 * You may not use this file except in compliance with the License.
 *
 * You can obtain a copy of the license at usr/src/CDDL.txt
 * or http://www.opensolaris.org/os/licensing.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file at usr/src/CDDL.txt.
 * If applicable, add the following below this CDDL HEADER, with the
 * fields enclosed by brackets [] replaced with your own identifying
 * information: Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */

/*
 * Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
 * Use is subject to license terms.
 *
 * gethostnames.c - This is a utility program used by the probe method
 * of the Kerberos KDC data service. This utility prints the list of all
 * the network address resources that are being used by the dataservice.
 *
 * The usage for this binary is
 *
 * gethostnames -R <resource_name>
 * 		-G <resourcegroup_name>
 * 		-T <resourcetype_name>
 *
 */

#pragma ident	"@(#)gethostnames.c	1.4	07/06/06 SMI"


#include <stdio.h>
#include <sys/types.h>
#include <time.h>
#include <rgm/libdsdev.h>

int
main(int argc, char *argv[])
{
	scds_net_resource_list_t 	*netres;
	int 		rs, ip;
	scds_handle_t 	scds_handle;

	if (scds_initialize(&scds_handle, argc, argv) != SCHA_ERR_NOERR) {
		return (1);
	}

	if (scds_get_rs_hostnames(scds_handle, &netres)) {
		scds_syslog(LOG_ERR,
		    "No network address resource in resource group.");
		scds_free_net_list(netres);
		scds_close(&scds_handle);
		return (1);
	}

	if ((netres->num_netresources == 1) &&
	    (netres->netresources[0].num_hostnames == 1)) {
		(void) printf("%s", netres->netresources[0].hostnames[0]);
	} else {
		for (rs = 0; rs < netres->num_netresources; rs++) {
			for (ip = 0;
			    ip < netres->netresources[rs].num_hostnames;
			    ip++) {
				(void) printf("%s",
				    netres->netresources[rs].hostnames[ip]);
				(void) printf("%s", ",");
			}
		}
	}

	scds_free_net_list(netres);

	scds_close(&scds_handle);

	return (0);
}
