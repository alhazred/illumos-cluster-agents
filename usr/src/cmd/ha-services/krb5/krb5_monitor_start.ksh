#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2006 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)krb5_monitor_start.ksh	1.7	07/06/06 SMI"
#
# Monitor start method for HA Kerberos KDC server.
#
#include_common_lib
#

#############################
# MAIN
#############################

PMF_TAG=$RESOURCEGROUP_NAME,$RESOURCE_NAME,0.mon

# Get the value for monitor retry count from the RTR file.
mon_retry_cnt_info=`scha_resource_get -O Extension -R $RESOURCE_NAME \
	-G $RESOURCEGROUP_NAME Monitor_retry_count`
MON_RETRY_CNT=`echo $mon_retry_cnt_info | awk '{print $2}'`

# Get the value for monitor retry interval from the RTR file.
mon_retry_intrval_info=`scha_resource_get -O Extension -R $RESOURCE_NAME \
	-G $RESOURCEGROUP_NAME Monitor_retry_interval`
MON_RETRY_INTRVAL=`echo $mon_retry_intrval_info | awk '{print $2}'`

# Start the probe for the data service under PMF. Use the Monitor_retry_count
# and Monitor_retry_interval options to start the probe. Pass the resource
# name, type, and group to the probe method. 
pmfadm -c $PMF_TAG -n $MON_RETRY_CNT -t $MON_RETRY_INTRVAL \
	$RT_BASEDIR/krb5_probe -R $RESOURCE_NAME -G $RESOURCEGROUP_NAME \
	-T $RESOURCETYPE_NAME

# Log a message indicating that the monitor for sckrb5 has been started.
if [ $? -eq 0 ]; then
	# SCMSGS
	# @explanation
	# The probe for HA-KDC has been started.
	# @user_action
	# This message is for informational purposes only, no action is
	# required.
	scds_syslog -p info -t $syslog_tag -m \
		"Monitor for sckrb5 successfully started."
fi

exit 0
