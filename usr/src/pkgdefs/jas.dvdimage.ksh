#!/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)jas.dvdimage.ksh	1.45	09/02/19 SMI"
#
# This script is used by the build_dvds script to build the pieces
# of the DVD image that this workspace contributes to it. The name of
# the DVD image is obtained from the prefix of this script's name:
# <dvd image name>.dvdimage.ksh
#
# This script should contain a function named build_image_<ARCH>_<OS>
# for every ARCH/OS combination supported by the build, so for sparc
# Solaris 9 the function would be named build_image_sparc_9. These ARCH/OS
# specific functions typically call other functions to handle the areas of
# the DVD that are common to multiple OSes, like the installer and
# external shared components. These individual functions should be made
# robust enough to handle the situation where they are called more than once,
# which is highly likely if each OS function calls a common function.
#
# See build_dvds for the list of predefined variables and functions
# available to this script.
#

#
# Variables
#
AGENTS_DIR=${DVD}/Solaris_${ARCH}/Product/sun_cluster_agents
CLOSED_BUILD=$([ ! -d "${WS}/usr/closed" ] && echo "#")
MACH=$([ "${ARCH}" = "sparc" ] && { echo ${ARCH}; } || { echo "i386"; })
ND=$([ "${DEBUG}" -eq 0 ] && echo "-nd")
PKGARCH=${WS}/packages/${MACH}/Sol_${OS}${ND}
PKGDEFS_DIR=${WS}/usr/src/pkgdefs
SOL=Solaris_${OS}

# DVD image of external products
DVDIMAGE_DIR=${PKGDEFS_DIR}/dvdimages/$(${BASENAME} ${DVD})

# Agent packages
PACKAGES_AGFA_IMPAX=SUNWscpax

PACKAGES_APACHE=SUNWscapc

PACKAGES_APACHE_TC=SUNWsctomcat

PACKAGES_DHC=SUNWscdhc

PACKAGES_DNS=SUNWscdns

PACKAGES_EBS=SUNWscebs

PACKAGES_HADB=SUNWschadb

PACKAGES_IDS=SUNWscids

PACKAGES_KERBEROS=SUNWsckrb5

PACKAGES_L10N="\
	SUNWcscapc \
	SUNWcscdns \
	SUNWcschadb \
	SUNWcschtt \
	SUNWcsclc \
	SUNWcscnfs \
	SUNWcscor \
	SUNWcscs1as \
	SUNWcscs1mq \
	SUNWcscsap \
	SUNWcscsapdb \
	SUNWcscsapenq \
	SUNWcscsaprepl \
	SUNWcscsapscs \
	SUNWcscsapwebas \
	SUNWcscsbl \
	SUNWcscsyb \
	SUNWcscwls \
	SUNWdschadb \
	SUNWdschtt \
	SUNWdscs1as \
	SUNWdscs1mq \
	SUNWeschadb \
	SUNWeschtt \
	SUNWescs1as \
	SUNWescs1mq \
	SUNWfscapc \
	SUNWfscdns \
	SUNWfschadb \
	SUNWfschtt \
	SUNWfsclc \
	SUNWfscnfs \
	SUNWfscs1as \
	SUNWfscs1mq \
	SUNWfscsap \
	SUNWfscsapdb \
	SUNWfscsbl \
	SUNWfscwls \
	SUNWhschadb \
	SUNWhschtt \
	SUNWhscs1as \
	SUNWhscs1mq \
	SUNWjscapc \
	SUNWjscdns \
	SUNWjschadb \
	SUNWjschtt \
	SUNWjsclc \
	SUNWjscnfs \
	SUNWjscor \
	SUNWjscs1as \
	SUNWjscs1mq \
	SUNWjscsap \
	SUNWjscsapdb \
	SUNWjscsapenq \
	SUNWjscsaprepl \
	SUNWjscsapscs \
	SUNWjscsapwebas \
	SUNWjscsbl \
	SUNWjscsyb \
	SUNWjscwls \
	SUNWkschadb \
	SUNWkschtt \
	SUNWkscs1as \
	SUNWkscs1mq"

PACKAGES_LIVECACHE=SUNWsclc

PACKAGES_MQI=SUNWscmqi

PACKAGES_MQS=SUNWscmqs

PACKAGES_MYS=SUNWscmys

PACKAGES_N1G_SPS=SUNWscsps

PACKAGES_NFS=SUNWscnfs

PACKAGES_ORACLE=$(eval ${CLOSED_BUILD} echo SUNWscor)

PACKAGES_ORACLE_9IAS=SUNWsc9ias

PACKAGES_POSTGRESQL=SUNWscPostgreSQL

PACKAGES_S1AS=SUNWscs1as

PACKAGES_S1MQ=SUNWscs1mq

PACKAGES_S1WS=SUNWschtt

PACKAGES_SAA=SUNWscsaa

PACKAGES_SAP=SUNWscsap

PACKAGES_SAPDB=SUNWscsapdb

PACKAGES_SAPWEBAS="\
	SUNWscsapenq \
	SUNWscsaprepl \
	SUNWscsapscs \
	SUNWscsapwebas"

PACKAGES_SIEBEL=SUNWscsbl

PACKAGES_SMB=SUNWscsmb

PACKAGES_SOLARIS_ZONES=SUNWsczone

PACKAGES_SUN_GRID_ENG=SUNWscsge

PACKAGES_SWIFT_GWAY=SUNWscsag

PACKAGES_SYBASE=$(eval ${CLOSED_BUILD} echo SUNWscsyb)

PACKAGES_WLS=SUNWscwls

PACKAGES_XVM=SUNWscxvm

# Specify which agents are supported on which platforms
sparc_9_AGENTS="\
	${PACKAGES_AGFA_IMPAX} \
	${PACKAGES_APACHE} \
	${PACKAGES_APACHE_TC} \
	${PACKAGES_DHC} \
	${PACKAGES_DNS} \
	${PACKAGES_EBS} \
	${PACKAGES_HADB} \
	${PACKAGES_LIVECACHE} \
	${PACKAGES_MQI} \
	${PACKAGES_MQS} \
	${PACKAGES_MYS} \
	${PACKAGES_N1G_SPS} \
	${PACKAGES_NFS} \
	${PACKAGES_ORACLE} \
	${PACKAGES_ORACLE_9IAS} \
	${PACKAGES_POSTGRESQL} \
	${PACKAGES_S1AS} \
	${PACKAGES_S1MQ} \
	${PACKAGES_S1WS} \
	${PACKAGES_SAA} \
	${PACKAGES_SAP} \
	${PACKAGES_SAPDB} \
	${PACKAGES_SAPWEBAS} \
	${PACKAGES_SIEBEL} \
	${PACKAGES_SMB} \
	${PACKAGES_SUN_GRID_ENG} \
	${PACKAGES_SWIFT_GWAY} \
	${PACKAGES_SYBASE} \
	${PACKAGES_WLS}"

sparc_10_AGENTS="\
	${PACKAGES_AGFA_IMPAX} \
	${PACKAGES_APACHE} \
	${PACKAGES_APACHE_TC} \
	${PACKAGES_DHC} \
	${PACKAGES_DNS} \
	${PACKAGES_EBS} \
	${PACKAGES_HADB} \
	${PACKAGES_IDS} \
	${PACKAGES_KERBEROS} \
	${PACKAGES_LIVECACHE} \
	${PACKAGES_MQI} \
	${PACKAGES_MQS} \
	${PACKAGES_MYS} \
	${PACKAGES_N1G_SPS} \
	${PACKAGES_NFS} \
	${PACKAGES_ORACLE} \
	${PACKAGES_ORACLE_9IAS} \
	${PACKAGES_POSTGRESQL} \
	${PACKAGES_S1AS} \
	${PACKAGES_S1MQ} \
	${PACKAGES_S1WS} \
	${PACKAGES_SAA} \
	${PACKAGES_SAP} \
	${PACKAGES_SAPDB} \
	${PACKAGES_SAPWEBAS} \
	${PACKAGES_SIEBEL} \
	${PACKAGES_SMB} \
	${PACKAGES_SOLARIS_ZONES} \
	${PACKAGES_SUN_GRID_ENG} \
	${PACKAGES_SWIFT_GWAY} \
	${PACKAGES_SYBASE} \
	${PACKAGES_WLS}"

# Support for S11 is unknown at this time, so include all agents
sparc_11_AGENTS="\
	${PACKAGES_AGFA_IMPAX} \
	${PACKAGES_APACHE} \
	${PACKAGES_APACHE_TC} \
	${PACKAGES_DHC} \
	${PACKAGES_DNS} \
	${PACKAGES_EBS} \
	${PACKAGES_HADB} \
	${PACKAGES_IDS} \
	${PACKAGES_KERBEROS} \
	${PACKAGES_L10N} \
	${PACKAGES_LIVECACHE} \
	${PACKAGES_MQI} \
	${PACKAGES_MQS} \
	${PACKAGES_MYS} \
	${PACKAGES_N1G_SPS} \
	${PACKAGES_NFS} \
	${PACKAGES_ORACLE} \
	${PACKAGES_ORACLE_9IAS} \
	${PACKAGES_POSTGRESQL} \
	${PACKAGES_S1AS} \
	${PACKAGES_S1MQ} \
	${PACKAGES_S1WS} \
	${PACKAGES_SAA} \
	${PACKAGES_SAP} \
	${PACKAGES_SAPDB} \
	${PACKAGES_SAPWEBAS} \
	${PACKAGES_SIEBEL} \
	${PACKAGES_SMB} \
	${PACKAGES_SOLARIS_ZONES} \
	${PACKAGES_SUN_GRID_ENG} \
	${PACKAGES_SWIFT_GWAY} \
	${PACKAGES_SYBASE} \
	${PACKAGES_WLS}"

i386_10_AGENTS="\
	${PACKAGES_APACHE} \
	${PACKAGES_APACHE_TC} \
	${PACKAGES_DHC} \
	${PACKAGES_DNS} \
	${PACKAGES_HADB} \
	${PACKAGES_IDS} \
	${PACKAGES_KERBEROS} \
	${PACKAGES_LIVECACHE} \
	${PACKAGES_MQI} \
	${PACKAGES_MQS} \
	${PACKAGES_MYS} \
	${PACKAGES_N1G_SPS} \
	${PACKAGES_NFS} \
	${PACKAGES_ORACLE} \
	${PACKAGES_ORACLE_9IAS} \
	${PACKAGES_POSTGRESQL} \
	${PACKAGES_S1AS} \
	${PACKAGES_S1MQ} \
	${PACKAGES_S1WS} \
	${PACKAGES_SAP} \
	${PACKAGES_SAPDB} \
	${PACKAGES_SAPWEBAS} \
	${PACKAGES_SMB} \
	${PACKAGES_SOLARIS_ZONES} \
	${PACKAGES_SUN_GRID_ENG} \
	${PACKAGES_SYBASE} \
	${PACKAGES_WLS}"

i386_11_AGENTS="\
	${sparc_11_AGENTS} \
	${PACKAGES_XVM}"

AGENTS=$(eval echo \${${MACH}_${OS}_AGENTS})

#
# Functions
#
# Builds the external pieces of the image
# build_image_external()
build_image_external() {
	# Check if this piece of the image has already been built
	[ -e "${DVD}/.cdtoc" ] && return

	echo "Adding the external pieces to the image"

	# Create directories
	${MKDIR} ${DVD} || error "Can't create directory ${DVD}"

	# Add dvdimage of symlinks to external products
	(cd ${DVDIMAGE_DIR} && ${FIND} . -print | ${CPIO} -oc) | (cd ${DVD} &&
${CPIO} -icdum)
}

# Builds the common pieces of the image
# build_image_common()
build_image_common() {
	# Check if this piece of the image has already been built
	[ -f "${AGENTS_DIR}/.producttoc" -a ! -h "${AGENTS_DIR}/.producttoc" ] && return

	# Build the external pieces of the image
	build_image_external

	echo "Adding the common ${MACH} pieces to the image"

	# Create directories
	${MKDIR} ${AGENTS_DIR} || error "Can't create directory ${AGENTS_DIR}"

	# Add .producttoc to top level directory
	copy ${PKGDEFS_DIR}/dot.producttoc.sun_cluster_agents.${MACH} ${AGENTS_DIR}/.producttoc
}

# Builds the solaris pieces of the image
# build_image_solaris()
build_image_solaris() {
	# Check if this piece of the image has already been built
	[ -f "${AGENTS_DIR}/${SOL}/Packages/.clustertoc" -a ! -h "${AGENTS_DIR}/${SOL}/Packages/.clustertoc" ] && return

	# Build the common pieces of the image
	build_image_common

	echo "Adding the ${SOL} ${MACH} pieces to the image"

	# Create directories
	${MKDIR} ${AGENTS_DIR}/${SOL}
	${MKDIR} ${AGENTS_DIR}/${SOL}/Packages

	# Add .clustertoc to agents Packages directory
	copy ${PKGDEFS_DIR}/dot.clustertoc ${AGENTS_DIR}/${SOL}/Packages/.clustertoc

	# Add .order to agents Packages directory
	copy ${PKGDEFS_DIR}/dot.order ${AGENTS_DIR}/${SOL}/Packages/.order

	# Copy packages from package archive to agents Packages directory
	[ -d "${PKGARCH}" ] || error "Package archive not found"

	for pkg in ${AGENTS}
	do
		[ -h "${AGENTS_DIR}/${SOL}/Packages/${pkg}" ] && ${RM} ${AGENTS_DIR}/${SOL}/Packages/${pkg}
	done

	${PKGTRANS} ${PKGARCH} ${AGENTS_DIR}/${SOL}/Packages ${AGENTS} || error "pkgtrans to ${AGENTS_DIR}/${SOL}/Packages failed"
}

# Builds the sparc solaris 9 pieces of the image
# build_image_sparc_9()
build_image_sparc_9() {
	# Build the solaris pieces of the image
	build_image_solaris
}

# Builds the sparc solaris 10 pieces of the image
# build_image_sparc_10()
build_image_sparc_10() {
	# Build the solaris pieces of the image
	build_image_solaris
}

# Builds the x86 solaris 10 pieces of the image
# build_image_x86_10()
build_image_x86_10() {
	# Build the solaris pieces of the image
	build_image_solaris
}

# Builds the sparc solaris 11 pieces of the image
# build_image_sparc_11()
build_image_sparc_11() {
	# Build the solaris pieces of the image
	build_image_solaris
}

# Builds the x86 solaris 11 pieces of the image
# build_image_x86_11()
build_image_x86_11() {
	# Build the solaris pieces of the image
	build_image_solaris
}
