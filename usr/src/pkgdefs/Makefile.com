#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#ident	"@(#)Makefile.com	1.64	09/05/04 SMI"
#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

include $(SRC)/Makefile.master

PKGARCHIVE=$(CODEMGR_WS)/packages/$(NATIVE_MACH)
PKGARCH=$(PKGARCHIVE)/$(TARGET_ROOT_PROTO)
PKGPROTOTYPE=prototype_$(NATIVE_MACH)
COPYRIGHTFILE= echo $(SRC)/pkgdefs/common_files/copyright
COPYRIGHT_FILES= $(COPYRIGHTFILE:sh)
COPYRIGHT= copyright
DATAFILES=
FILES=$(COPYRIGHT) $(DATAFILES) pkginfo
PACKAGE:sh= basename `pwd`
BASEDIR=
CLOBBERFILES= $(FILES)
PO_LABEL=' The following lines are contents of '

# Localization packages
PACKAGES_L10N = \
		SUNWcscapc SUNWcscdns SUNWcschadb SUNWcschtt \
		SUNWcsclc SUNWcscnfs SUNWcscor SUNWcscs1as \
		SUNWcscs1mq SUNWcscsap SUNWcscsapdb SUNWcscsapenq \
		SUNWcscsaprepl SUNWcscsapscs SUNWcscsapwebas SUNWcscsbl \
		SUNWcscsyb SUNWcscwls \
		SUNWdschadb SUNWdschtt SUNWdscs1as SUNWdscs1mq \
		SUNWeschadb SUNWeschtt SUNWescs1as SUNWescs1mq \
		SUNWfscapc SUNWfscdns SUNWfschadb SUNWfschtt \
		SUNWfsclc SUNWfscnfs SUNWfscs1as SUNWfscs1mq \
		SUNWfscsap SUNWfscsapdb SUNWfscsbl SUNWfscwls \
		SUNWhschadb SUNWhschtt SUNWhscs1as SUNWhscs1mq \
		SUNWjscapc SUNWjscdns SUNWjschadb SUNWjschtt \
		SUNWjsclc SUNWjscnfs SUNWjscor SUNWjscs1as \
		SUNWjscs1mq SUNWjscsap SUNWjscsapdb SUNWjscsapenq \
		SUNWjscsaprepl SUNWjscsapscs SUNWjscsapwebas SUNWjscsbl \
		SUNWjscsyb SUNWjscwls \
		SUNWkschadb SUNWkschtt SUNWkscs1as SUNWkscs1mq
