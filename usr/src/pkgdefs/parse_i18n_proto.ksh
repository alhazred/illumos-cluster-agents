#! /usr/bin/ksh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#ident	"@(#)parse_i18n_proto.ksh	1.9	08/06/09 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

CLOSED_MSG_CATALOGS=$*

nawk -v closed_msg_catalogs="${CLOSED_MSG_CATALOGS}" '
   BEGIN {
	printf("include ../Makefile.com\n\n");
   }
   /^f / {
	if (NF == 6) {
		path = $3;
		mode = $4;
		owner = $5;
		group = $6;
		n = split(path, paths, "/");
		split(paths[n], domain, ".po");
		catalogs[domain[1]] = 1;
		printf("%s := CATPATH = %s\n", domain[1], path);
	}
   }
   END {
	printf("MSG_CATALOGS = ");
	for (catalog in catalogs) {
		if (! match(closed_msg_catalogs, catalog)) {
			printf("\t%s", catalog);
		}
		else {
			closed_catalogs[catalog] = 1;
		}
	}
	printf("\n\n$(CLOSED_BUILD)MSG_CATALOGS += ");
	for (catalog in closed_catalogs) {
		printf("\t%s", catalog);
	}
	printf("\n\n_msg: $(MSG_CATALOGS)\n\ninclude ../Makefile.targ\n");
   }'
