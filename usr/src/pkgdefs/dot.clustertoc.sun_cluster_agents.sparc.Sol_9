#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# ident	"@(#)dot.clustertoc.sun_cluster_agents.sparc.Sol_9	1.4	07/08/07 SMI"
#

#
# Sun Cluster HA for Agfa IMPAX
#
CLUSTER=SUNWC_DS_pax
NAME=Sun Cluster HA for AGFA IMPAX
DESC=Sun Cluster HA for AGFA IMPAX
VENDOR=Sun Microsystems, Inc.
VERSION=3.1
SUNW_CSRMEMBER=SUNWscpax
END

#
# Sun Cluster HA for Apache
#
CLUSTER=SUNWC_DS_apache
NAME=Sun Cluster HA for Apache
DESC=Sun Cluster HA for Apache
VENDOR=Sun Microsystems, Inc.
VERSION=3.0
SUNW_CSRMEMBER=SUNWscapc
END

#
# Sun Cluster HA for Apache Tomcat
#
CLUSTER=SUNWC_DS_tomcat
NAME=Sun Cluster HA for Apache Tomcat
DESC=Sun Cluster HA for Apache Tomcat
VENDOR=Sun Microsystems, Inc.
VERSION=3.1
SUNW_CSRMEMBER=SUNWsctomcat
END

#
# Sun Cluster HA for DHCP
#
CLUSTER=SUNWC_DS_dhcp
NAME=Sun Cluster HA for DHCP
DESC=Sun Cluster HA for DHCP
VENDOR=Sun Microsystems, Inc.
VERSION=3.1
SUNW_CSRMEMBER=SUNWscdhc
END

#
# Sun Cluster HA for Domain Name Server (DNS)
#
CLUSTER=SUNWC_DS_dns
NAME=Sun Cluster HA for DNS
DESC=Sun Cluster HA for DNS
VENDOR=Sun Microsystems, Inc.
VERSION=1.0
SUNW_CSRMEMBER=SUNWscdns
END

#
# Sun Cluster HA for Oracle E-Business suite
#
CLUSTER=SUNWC_DS_ebs
NAME=Sun Cluster HA for Oracle E-Business Suite
DESC=Sun Cluster HA for Oracle E-Business Suite
VENDOR=Sun Microsystems, Inc.
VERSION=3.1
SUNW_CSRMEMBER=SUNWscebs
END

#
# Sun Cluster HA Sun Java System App Server EE (HADB)
#
CLUSTER=SUNWC_DS_hadb
NAME=Sun Cluster HA Sun Java System App Server EE (HADB)
DESC=Sun Cluster HA Sun Java System App Server EE (HADB)
VENDOR=Sun Microsystems, Inc.
VERSION=1.0
SUNW_CSRMEMBER=SUNWschadb
END

#
# Sun Cluster HA for SAP liveCache
#
CLUSTER=SUNWC_DS_livecache
NAME=Sun Cluster HA for SAP liveCache
DESC=Sun Cluster HA for SAP liveCache
VENDOR=Sun Microsystems, Inc.
VERSION=1.0
SUNW_CSRMEMBER=SUNWsclc
END

#
# Sun Cluster HA for WebSphere MQ Broker
#
CLUSTER=SUNWC_DS_mqi
NAME=Sun Cluster HA for WebSphere MQ Broker 
DESC=Sun Cluster HA for WebSphere MQ Broker
VENDOR=Sun Microsystems, Inc.
VERSION=3.1
SUNW_CSRMEMBER=SUNWscmqi
END

#
# Sun Cluster HA for WebSphere MQ
#
CLUSTER=SUNWC_DS_mqs
NAME=Sun Cluster HA for WebSphere MQ
DESC=Sun Cluster HA for WebSphere MQ
VENDOR=Sun Microsystems, Inc.
VERSION=3.1
SUNW_CSRMEMBER=SUNWscmqs
END

#
# Sun Cluster HA for MySQL
#
CLUSTER=SUNWC_DS_mys
NAME=Sun Cluster HA for MySQL
DESC=Sun Cluster HA for MySQL
VENDOR=Sun Microsystems, Inc.
VERSION=3.1
SUNW_CSRMEMBER=SUNWscmys
END

#
# Sun Cluster HA for N1 Grid Provisioning System
#
CLUSTER=SUNWC_DS_n1sps
NAME=Sun Cluster HA for N1 Grid Service Provisioning
DESC=Sun Cluster HA for N1 Grid Service Provisioning
VENDOR=Sun Microsystems, Inc.
VERSION=3.1
SUNW_CSRMEMBER=SUNWscsps
END

#
# Sun Cluster HA for Network File System (NFS)
#
CLUSTER=SUNWC_DS_nfs
NAME=Sun Cluster HA for NFS
DESC=Sun Cluster HA for NFS
VENDOR=Sun Microsystems, Inc.
VERSION=1.0
SUNW_CSRMEMBER=SUNWscnfs
END

#
# Sun Cluster HA for Oracle
#
CLUSTER=SUNWC_DS_oracle
NAME=Sun Cluster HA for Oracle
DESC=Sun Cluster HA for Oracle
VENDOR=Sun Microsystems, Inc.
VERSION=3.1
SUNW_CSRMEMBER=SUNWscor
END

#
# Sun Cluster HA for Oracle Application Server
#
CLUSTER=SUNWC_DS_9ias
NAME=Sun Cluster HA for Oracle Application Server
DESC=Sun Cluster HA for Oracle Application Server
VENDOR=Sun Microsystems, Inc.
VERSION=3.1
SUNW_CSRMEMBER=SUNWsc9ias
END

#
# Sun Cluster Oracle RAC CVM
#
CLUSTER=SUNWC_RAC_rac_cvm
NAME=Sun Cluster Oracle RAC CVM
DESC=Sun Cluster Oracle RAC CVM
VENDOR=Sun Microsystems, Inc.
VERSION=3.1
SUNW_CSRMEMBER=SUNWcvmr
SUNW_CSRMEMBER=SUNWcvm
END

#
# Sun Cluster Oracle RAC Framework
#
CLUSTER=SUNWC_RAC_oracle_rac
NAME=Sun Cluster Oracle RAC
DESC=Sun Cluster Oracle RAC
VENDOR=Sun Microsystems, Inc.
VERSION=3.1
SUNW_CSRMEMBER=SUNWscucm
SUNW_CSRMEMBER=SUNWudlmr
SUNW_CSRMEMBER=SUNWudlm
SUNW_CSRMEMBER=SUNWscor
END

#
# Sun Cluster Oracle RAC SVM
#
CLUSTER=SUNWC_RAC_rac_svm
NAME=Sun Cluster Oracle RAC SVM
DESC=Sun Cluster Oracle RAC SVM
VENDOR=Sun Microsystems, Inc.
VERSION=3.1
SUNW_CSRMEMBER=SUNWscmd
END

#
# Sun Cluster HA for PostgreSQL
#
CLUSTER=SUNWC_DS_PostgreSQL
NAME=Sun Cluster HA for PostgreSQL
DESC=Sun Cluster HA for PostgreSQL
VENDOR=Sun Microsystems, Inc.
VERSION=3.2
SUNW_CSRMEMBER=SUNWscPostgreSQL
END

#
# Sun Cluster HA Sun Java System Application Server
#
CLUSTER=SUNWC_DS_s1as
NAME=Sun Cluster HA Sun Java System Application Server
DESC=Sun Cluster HA Sun Java System Application Server
VENDOR=Sun Microsystems, Inc.
VERSION=1.0
SUNW_CSRMEMBER=SUNWscs1as
END

#
# Sun Cluster HA Sun Java System Message Queue
#
CLUSTER=SUNWC_DS_s1mq
NAME=Sun Cluster HA Sun Java System Message Queue
DESC=Sun Cluster HA Sun Java System Message Queue
VENDOR=Sun Microsystems, Inc.
VERSION=1.0
SUNW_CSRMEMBER=SUNWscs1mq
END

#
# Sun Cluster HA Sun Java System Web Server
#
CLUSTER=SUNWC_DS_iws
NAME=Sun Cluster HA Sun Java System Web Server
DESC=Sun Cluster HA Sun Java System Web Server
VENDOR=Sun Microsystems, Inc.
VERSION=1.0
SUNW_CSRMEMBER=SUNWschtt
END

#
# Sun Cluster HA for SWIFTAlliance Access
#
CLUSTER=SUNWC_DS_saa
NAME=Sun Cluster HA for SWIFTAlliance Access
DESC=Sun Cluster HA for SWIFTAlliance Access
VENDOR=Sun Microsystems, Inc.
VERSION=3.1
SUNW_CSRMEMBER=SUNWscsaa
END

#
# Sun Cluster HA for SAP
#
CLUSTER=SUNWC_DS_sap
NAME=Sun Cluster HA for SAP
DESC=Sun Cluster HA for SAP
VENDOR=Sun Microsystems, Inc.
VERSION=1.0
SUNW_CSRMEMBER=SUNWscsap
END

#
# Sun Cluster SAPDB Data Service
#
CLUSTER=SUNWC_DS_sapdb
NAME=Sun Cluster HA for SAPDB
DESC=Sun Cluster HA for SAPDB
VENDOR=Sun Microsystems, Inc.
VERSION=1.0
SUNW_CSRMEMBER=SUNWsclc
SUNW_CSRMEMBER=SUNWscsapdb
END

#
# Sun Cluster SAP Web Application Server Data Service
#
CLUSTER=SUNWC_DS_sapwebas
NAME=Sun Cluster HA for SAP Web Application Server
DESC=Sun Cluster HA for SAP Web Application Server
VENDOR=Sun Microsystems, Inc.
VERSION=1.0
SUNW_CSRMEMBER=SUNWscsapenq
SUNW_CSRMEMBER=SUNWscsaprepl
SUNW_CSRMEMBER=SUNWscsapwebas
SUNW_CSRMEMBER=SUNWscsapscs
END

#
# Sun Cluster HA for Siebel
#
CLUSTER=SUNWC_DS_siebel
NAME=Sun Cluster HA for Siebel
DESC=Sun Cluster HA for Siebel
VENDOR=Sun Microsystems, Inc.
VERSION=1.0
SUNW_CSRMEMBER=SUNWscsbl
END

#
# Sun Cluster HA for Samba
#
CLUSTER=SUNWC_DS_smb
NAME=Sun Cluster HA for Samba
DESC=Sun Cluster HA for Samba
VENDOR=Sun Microsystems, Inc.
VERSION=3.1
SUNW_CSRMEMBER=SUNWscsmb
END

#
# Sun Cluster HA for Sun Grid Engine
#
CLUSTER=SUNWC_DS_n1ge
NAME=Sun Cluster HA for Sun Grid Engine
DESC=Sun Cluster HA for Sun Grid Engine
VENDOR=Sun Microsystems, Inc.
VERSION=3.1
SUNW_CSRMEMBER=SUNWscsge
END

#
# Sun Cluster HA for Swift Alliance Gateway
#
CLUSTER=SUNWC_DS_sag
NAME=Sun Cluster HA for SWIFT Alliance Gateway
DESC=Sun Cluster HA for SWIFT Alliance Gateway
VENDOR=Sun Microsystems, Inc.
VERSION=3.1
SUNW_CSRMEMBER=SUNWscsag
END

#
# Sun Cluster HA for Sybase ASE
#
CLUSTER=SUNWC_DS_sybase
NAME=Sun Cluster HA for Sybase ASE
DESC=Sun Cluster HA for Sybase ASE
VENDOR=Sun Microsystems, Inc.
VERSION=3.0
SUNW_CSRMEMBER=SUNWscsyb
END

#
# Sun Cluster HA for BEA WebLogic Server
#
CLUSTER=SUNWC_DS_wls
NAME=Sun Cluster HA for BEA WebLogic Server
DESC=Sun Cluster HA for BEA WebLogic Server
VENDOR=Sun Microsystems, Inc.
VERSION=3.1
SUNW_CSRMEMBER=SUNWscwls
END
