#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
#ident	"@(#)Makefile	1.132	09/04/15 SMI"
#
# Copyright 2009 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# pkgdefs/Makefile

include Makefile.com

sparc_NONSHIP_SUBDIRS =
i386_NONSHIP_SUBDIRS =

I18N_SUBDIRS =
#	SUNW0scds

NONSHIP_SUBDIRS = \
	$($(MACH)_NONSHIP_SUBDIRS)

PRODUCT_SUBDIRS = \
	SUNWscapc \
	SUNWscnfs \
	SUNWscdhc \
	SUNWscdns

s9_PRODUCT_SUBDIRS += 

$(S9_BUILD)PRODUCT_SUBDIRS += $(s9_PRODUCT_SUBDIRS)

post_s9_PRODUCT_SUBDIRS += \
	SUNWsczone

$(POST_S9_BUILD)PRODUCT_SUBDIRS += $(post_s9_PRODUCT_SUBDIRS)

sparc_PRODUCT_SUBDIRS +=
#	SUNWscebs \
#	SUNWscpax \
#	SUNWscsbl

i386_PRODUCT_SUBDIRS +=
#SUNWscxvm

PRODUCT_SUBDIRS += $($(MACH)_PRODUCT_SUBDIRS)

SUBDIRS= \
	$(PRODUCT_SUBDIRS) \
	$(NONSHIP_SUBDIRS)
#	$(I18N_SUBDIRS)

# Targetdirs needs to process all package subdirs, as we build all the code,
# even if the agent isn't supported/packaged on a particular platform
$(POST_S9_BUILD)ALLPKG_SUBDIRS += $(s9_PRODUCT_SUBDIRS)
$(S9_BUILD)ALLPKG_SUBDIRS += $(post_s9_PRODUCT_SUBDIRS)
i386_ALLPKG_SUBDIRS += $(sparc_PRODUCT_SUBDIRS)
sparc_ALLPKG_SUBDIRS += $(i386_PRODUCT_SUBDIRS)
ALLPKG_SUBDIRS += $(SUBDIRS) $($(MACH)_ALLPKG_SUBDIRS)

# Support for S11 is unknown at this time, so include all agents
$(S11_BUILD)SUBDIRS += $(s9_PRODUCT_SUBDIRS) $($(MACH)_ALLPKG_SUBDIRS)

sparc_XMODS=

# L10N support dropped from 3.2 due to resource constraints
# L10N support is included in open source builds
$(S11_BUILD)L10N_XMODS= l10n

DVDIMAGES_XMODS= \
	dvdimages

XMODS=	$($(MACH)_XMODS)
#$(L10N_XMODS) $(DVDIMAGES_XMODS)

DOT_FILES= dot.clustertoc dot.order

all :=         		TARGET= all
install := 		TARGET= install
clean :=                TARGET= clean
clobber :=              TARGET= clobber
_msg :=                 TARGET= _msg
patch_build :=		TARGET= patch_build

.KEEP_STATE:

.PARALLEL: $(SUBDIRS) $(XMODS)

install: awk_pkginfo $(SUBDIRS) $(XMODS)

_msg: ./parse_i18n_proto $(I18N_SUBDIRS)

all: awk_pkginfo $(SUBDIRS) $(XMODS) $(DOT_FILES)

clean clobber: $(SUBDIRS) $(XMODS)
	$(RM) awk_pkginfo Targetdirs $(DOT_FILES)

patch_build: awk_pkginfo $(PRODUCT_SUBDIRS) patch_manpages

$(SUBDIRS): FRC
	@cd $@; pwd; $(MAKE) $(TARGET)

$(XMODS):       FRC
	@if [ -f $@/Makefile  ]; then \
		cd $@; pwd; $(MAKE) $(TARGET); \
	else \
		true; \
	fi

DOT_CLUSTERTOC_SRC = dot.clustertoc.sun_cluster_agents.$(NATIVE_MACH)
$(S9_BUILD)DOT_CLUSTERTOC_SRC = dot.clustertoc.sun_cluster_agents.$(NATIVE_MACH).Sol_9

DOT_ORDER_SRC= dot.order.sun_cluster_agents.$(NATIVE_MACH)
$(S9_BUILD)DOT_ORDER_SRC= dot.order.sun_cluster_agents.$(NATIVE_MACH).Sol_9

dot.clustertoc: $(DOT_CLUSTERTOC_SRC)
	$(RM) $@
	cp $(DOT_CLUSTERTOC_SRC) $@

dot.order: $(DOT_ORDER_SRC)
	$(RM) $@
	cp $(DOT_ORDER_SRC) $@

$(NOT_RELEASE_BUILD)AWK_PKGINFO_DFLAG= -d

awk_pkginfo: ./bld_awk_pkginfo
	./bld_awk_pkginfo -m $(MACH) -p "$(RELEASE)/$(VERSION)" ${AWK_PKGINFO_DFLAG} -o $@

# Copy the package contents to the proto area
patch_manpages: $(DOCS_DELIVERY)
	for pkg in SUNWscdsman; \
	do \
		basedir=`$(EGREP) '^BASEDIR=' $(DOCS_DELIVERY)/$$pkg/pkginfo | $(SED) -e 's:.*=::'`; \
		[ -d $(VROOT)$$basedir ] || /bin/mkdir -p $(VROOT)$$basedir; \
		(cd $(DOCS_DELIVERY)/$$pkg/reloc && /bin/tar cf - .) | (cd $(VROOT)$$basedir && /bin/tar xf -); \
	done

ALL_PROTOTYPES = 	$(ALLPKG_SUBDIRS:%=%/pkginfo.tmpl %/prototype_com) \
			$(ALLPKG_SUBDIRS:%=%/pkginfo.tmpl %/prototype_$(MACH))

makefiles:	Targetdirs parse_i18n_proto

# generate Targetdirs and use that to create all the proto directories
Targetdirs:	./pkgdefs_to_targetdirs $(ALL_PROTOTYPES)
		./pkgdefs_to_targetdirs $(ALL_PROTOTYPES) > Targetdirs

FRC:
