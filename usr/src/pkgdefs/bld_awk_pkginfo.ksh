#!/usr/bin/ksh -p
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# ident	"@(#)bld_awk_pkginfo.ksh	1.19	08/09/22 SMI"
#
# Copyright 2008 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# Simple script which builds the awk_pkginfo awk script.  This awk script
# is used to convert the pkginfo.tmpl files into pkginfo files
# for the build.
#


usage()
{
   echo "usage: bld_awk_pkginfo [-d] -p <prodver> -m <mach> -o <awk_script>"
}

MACH=`uname -p`

#
# Awk strings
#
# VERSION1 for Dewey decimal plus ,REV=n that has two or more '=', and is used
# for new packages whose REV is "0.0.0".
# VERSION_32 is for the packages that shipped in SC3.2.
#
VERSION1="VERSION=[^=]*,REV=0\.0\.0\"$"
VERSION_32="VERSION=[^=]*,REV=3\.2\.0\"$"
VERSION_IDS="VERSION=[^=]*,REV=IDS\"$"
PRODVERS="^SUNW_PRODVERS="
ARCH='ARCH=\"ISA\"'

# Remove zones variables from S9 packages
#
ZONE="^SUNW_PKG_ALLZONES=|^SUNW_PKG_HOLLOW=|^SUNW_PKG_THISZONE="

#
#
# parse command line
#
mach=""
prodver=""
awk_script=""
debug=""

while getopts o:p:m:d c
do
   case $c in
   o)
      awk_script=$OPTARG
      ;;
   m)
      mach=$OPTARG
      ;;
   p)
      prodver=$OPTARG
      ;;
   d)
      debug=",debug"
      ;;
   \?)
      usage
      exit 1
      ;;
   esac
done

if [[ ( -z "$prodver" ) || ( -z "$mach" ) || ( -z "$awk_script" ) ]]
then
   usage
   exit 1
fi

#
# Build REV= field based on date. Used for new packages.
#
rev=$(date "+%Y.%m.%d.%H.%M")

#
# Rev for different OS build. Used for packages that have already shipped.
#
if [[ ( "${OS}" == "5.9" ) && ( "${MACH}" == "sparc" ) ]]; then
	rev_32="2006.12.06.18.31"
	rev_ids=$rev
elif [[ ( "${OS}" == "5.10" ) && ( "${MACH}" == "sparc" ) ]]; then
	rev_32="2006.12.06.18.32"
	rev_ids="2008.08.25.16.43"
elif [[ ( "${OS}" == "5.10" ) && ( "${MACH}" == "i386" ) ]]; then
	rev_32="2006.12.06.18.33"
	rev_ids="2008.08.25.16.41"
else
	rev_32=$rev
	rev_ids=$rev
fi

# Remove zones variables from S9 packages
#
zone_awkcmd=
if [[ ${OS} != "5.9" ]]; then
	zone_awkcmd="print"
fi

#
#
# Build awk script which will process all the
# pkginfo.tmpl files.
#
rm -f $awk_script
cat << EOF > $awk_script
/$VERSION1/ {
      sub(/\=[^=]*$/,"=${rev}${debug}\"")
      print
      next
   }
/$VERSION_32/ {
      sub(/\=[^=]*$/,"=${rev_32}${debug}\"")
      print
      next
   }
/$VERSION_IDS/ {
      sub(/\=[^=]*$/,"=${rev_ids}${debug}\"")
      print
      next
   }
/$PRODVERS/ { 
      printf "SUNW_PRODVERS=\"%s\"\n", "$prodver" 
      next
   }
/$ARCH/ {
      printf "ARCH=\"%s\"\n", "$mach"
      next
   }
/$ZONE/ {
      $zone_awkcmd
      next
   }
{ print }
EOF

