#! /usr/bin/sh
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the License).
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/CDDL.txt
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/CDDL.txt.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets [] replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# ident	"@(#)pkgdefs_to_targetdirs.sh	1.13	07/06/06 SMI"
#
# Copyright 1998-2002 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#
# script to generate TARGETDIRS file from pkg definitions
#
# syntax: $0 list of prototype files to scan for directory entries
#
#
# outputs Makefile-form Targetdirs file to drive creation of
# directories directly from package specs
#

MACH=`mach`

/bin/nawk '
           /^BASEDIR/ {
			split($1, basearray, "=");
			basedir=basearray[2];
			}
           /^#/ { continue}
	   /^i/ { continue}
	   /^!/ { continue}
	   /^d/ { 
		$4 = $4 + 0; # remove leading 0
		file = $3;
		name = toupper($5) "." toupper($6)
		mode = $4;
		check = name mode;

		# prepend basedir sans leading / if it is not empty

		if (basedir != "/")
		    file = substr(basedir,2) "/" file;

		
		if (files[file] != "") {
			if (files[file] != check) {
				printf("\n\nPackages %s and %s not consistent for %s: %s %s\n\n",
					filenames[file], FILENAME, file, files[file], check) > "/dev/tty"
				error_happened = 1;
				exit 1
			}
		} else {
			files[file] = check;
			filenames[file] = FILENAME;
			name_index = name_list[name] = name_list[name] + 1;
			mode_index = mode_list[mode] = mode_list[mode] + 1;
			place[name_index, name] = sprintf(" \\\n\t/%s", file);
			modes[mode_index, mode] = sprintf(" \\\n\t$(VROOT)/%s", file);
		}
		}
	   END {
	        if (error_happened != 0)
		  exit error_happened

		printf("# Machine generated - do not edit\n");

		printf("TARGETDIRS = ");

		for (name in name_list) {
			printf("\\\n\t$(%s)", name);
		}
		printf("\n");
		for (name in name_list) {
			printf("\n%s = ", name);
			for (i = 1; i <= name_list[name]; i++)
				printf("%s", place[i, name]);
			printf("\n\n");
		}
		for (name in name_list) {
			split(tolower(name),og, ".");
			printf("$(%s:%%=$(VROOT)%%):=	OWNER = %s\n",
				name, og[1]);
			printf("$(%s:%%=$(VROOT)%%):=	GROUP = %s\n",
				name, og[2]);
		}
		for (mode in mode_list) {
			if (mode != 755) {  # skip default
				printf("DIRMODE_%s += ",mode);
				for (i = 1; i <= mode_list[mode]; i++)
					printf("%s", modes[i,mode]);
				printf("\n\n");
			}
		}
		for (mode in mode_list) {
			if (mode != 755)  # skip default
				printf("$(DIRMODE_%s):=\tDIRMODE = %s\n", mode, mode);
		}
		exit 0
	}'  $*
